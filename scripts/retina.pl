#!/usr/bin/env perl

######
## retina.pl - Retina image generator
## 2021-05

use strict;
use warnings;
no feature qw(indirect);
use open qw(:std :utf8);
use utf8;
use Image::Magick;

use constant FUZZ => 5.0;          ## % fuzz for trimming edges
use constant QUALITY => 97;        ## max quality % for lossy formats
use constant RETINA_WIDTH => 1000; ## desired Retina image width

###########################################################################

my $source = shift or die "Usage: retina.pl <IMAGE> [<WIDTH>]";
my $width = shift // RETINA_WIDTH;

exit(&main());

###########################################################################

sub jpegoptim {
    foreach my $file (@_) {
    	system("jpegoptim --preserve --totals \"$file\"");
    }
}

sub pngcrush {
    foreach my $file (@_) {
    	system("pngcrush -blacken -ow -reduce -v \"$file\"");
    }
}

###########################################################################

sub main {
    my $image = Image::Magick->new();
    my $status = $image->Read($source);
    die $status if $status;

    ## Get source file modtime
    my $mtime = (stat $source)[9];

    ## Grab extension, create target filenames, and convert gif to png
    my $extension = $image->Get("%e");
    my $source = $image->Get("%t");

    $extension =~ s/gif/png/;
    my $x2 = $source. '@2x.'. $extension;
    my $x1 = $source. '@1x.'. $extension;
    
    if (($extension =~ m/jpe*g/) && ($image->Get('quality') > QUALITY)) {
        $image->Set(quality => QUALITY);
    }

    ## Light processing
    $image->ContrastStretch();
    $image->Set(interlace => 'None');
    $image->Set(comment => "From Rubenerd.com!");
    $image->Set(label => "From Rubenerd.com!");

    ## Remove white edges
    $image->Set(fuzz => FUZZ);
    $image->Trim();
    $image->Set(page =>'0x0+0+0');  ## Repage

    ## Create Retina image, and crop to even height if needed
    $image->Resize(geometry => $width. "x\>");
    $image->Crop(geometry => '0x0+0-1') if ($image->Get('height') % 2);
    $image->Write("$x2");

    ## Create standard image
    $image->Resize(geometry => "50%x50%");
    $image->Write("$x1");

    ## Donezo!
    print "Processed $source in ". $image->Get('elapsed-time'). "\n";

    ## Final compression
    if ($extension =~ m/jpe*g/) {
		&jpegoptim($x2, $x1);
    }
    elsif ($extension eq "png") {
        &pngcrush($x2, $x1);
    }

    ## Set modtimes to match source file
    utime(time, $mtime, $x2);
    utime(time, $mtime, $x1);

    return 0;
}

