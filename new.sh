#!/bin/sh

_EDITOR="gvim"
_YEAR="$(date +%Y)"
_FOLDER="./content/post/$_YEAR"

[ ! -d "$_FOLDER" ] && mkdir "$_FOLDER"

## macOS chokes on this
## hugo new --editor="$_EDITOR" "$_FOLDER/$1.md"

hugo new "$_FOLDER/$1.md"
gvim "$_FOLDER/$1.md"
