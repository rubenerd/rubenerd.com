55:49 – In part one, your sophisticated host attempts to make an Aeropress coffee live on air, namedrops his friend, mentions some Minecraft LEGO adventures, and discusses some listener mail from Oskar Sharipov regarding audio recording, OGG files, and independent production. Then we timeskip a week to when your aforementioned host walks around an abandoned Chatswood street late at night to find which coffee shops are open during the Sydney lockdowns. Mmm, coffee.

Recorded in Sydney, Australia. Licence for this track: Creative Commons Attribution 3.0. Attribution: Ruben Schade.

Released July 2021 on Rubenerd and The Overnightscape Underground, an Internet talk radio channel focusing on a freeform monologue style, with diverse and fascinating hosts; this one notwithstanding.

