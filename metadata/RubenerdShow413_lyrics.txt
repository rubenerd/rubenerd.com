13:57 – Jim Kloss returned to the Whole Wheat Radio airwaves again for a chat! I was not prepared, and even started typing over it towards the end when work pinged me, but I couldn’t let this moment of Internet audio history go unrecorded.

Recorded in Sydney, Australia. Licence for this track: Creative Commons Attribution 3.0. Attribution: Ruben Schade.

Released January 2021 on Rubenerd and The Overnightscape Underground, an Internet talk radio channel focusing on a freeform monologue style, with diverse and fascinating hosts; this one notwithstanding.

