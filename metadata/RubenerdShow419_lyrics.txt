53:50 – Join your host as he enjoys some milder Sydney weather! Talking about monsoons in Singapore and Hong Kong, wanting to live in the latter after it blew me away (metaphorically, not in a monsoon), slim Japanese clothing, water-retaining socks, always feeling the need to be productive, embarrassing nerd magazines on the train, old-school MP3 players, downtime, meditation, a rant about music streaming platforms, and collecting music again.

Recorded in Sydney, Australia. Licence for this track: Creative Commons Attribution 3.0. Attribution: Ruben Schade.

Released September 2021 on Rubenerd and The Overnightscape Underground, an Internet talk radio channel focusing on a freeform monologue style, with diverse and fascinating hosts; this one notwithstanding.

