14:00 – A discussion about how my fervour for independent writing hasn’t translated into improving podcast imposter syndrome! Also RSS, distribution, how social networks have convinced people to only write for them, and related ruminations. Thanks for your patience while I produced this one :).

Recorded in Sydney, Australia. Licence for this track: Creative Commons Attribution 3.0. Attribution: Ruben Schade.

Released May 2021 on Rubenerd and The Overnightscape Underground, an Internet talk radio channel focusing on a freeform monologue style, with diverse and fascinating hosts; this one notwithstanding.

