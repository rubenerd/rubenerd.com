---
layout: page
title: Pages
---
### Blog 

* [Home](https://rubenerd.com/), the first page of my blog, where you can read the latest entries and browse the archives.

* [Categories](https://rubenerd.com/category/), of categories!

* [Locations](https://rubenerd.com/location/) where I've written posts, still incomplete.

* [Tags](https://rubenerd.com/tag/), which is, quite frankly, a huge mess.


### Other goodies

* [About](https://rubenerd.com/about/): Information about me, the site, my mascot Rubi, and how you can contact me via email and social media.

* [Blogroll](https://rubenerd.com/blogroll.opml): People, news services, video channels, and podcasters I read, watch, and listen to.

* [Buttons](https://rubenerd.com/buttons/): Remember those?

* [Now](https://rubenerd.com/now/): Stuff I'm working on *now*. It's like a living projects page.

* [Omake](https://rubenerd.com/omake.xml): My personal cardfile database of everything, written in my XML format.

* [Pages](https://rubenerd.com/pages/), which is this page. How delightfully redundant!


### Elsewhere on the server

* [Retro Corner](http://retro.rubenerd.com/): My retro site of old computer hardware, using my 1998 GeoCities theme.

* [Ruben.Coffee](http://ruben.coffee/): My Omake page about my favourite beverage.

* [Sasara.moe](https://sasara.moe/): Clara's and my shared wiki of collections, stuff, and travel.

