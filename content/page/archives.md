---
layout: page
title: "Archives"
---
This blog has 8,000+ posts and pages, including this one. This page lists how you can view and find them by [searching](#search), [years](#years), [categories](#categories), [tags](#tags), [lists](#lists), [repositories](#repositories), and [locations](#locations).


<h3 id="search">Search</h3>

<p></p>
<form role="search" method="get" action="https://duckduckgo.com/">
<fieldset>
<legend>Search blog posts with DuckDuckGo.</legend>
<input type="text" size="40" name="q" placeholder="Search with DuckDuck…" />
<input type="hidden" name="ia" value="web" />
<input type="hidden" name="kaj" value="m" />
<input type="hidden" name="k7" value="w" />
<input type="hidden" name="k9" value="b" />
<input type="hidden" name="ks" value="l" />
<input type="hidden" name="sites" value="https://rubenerd.com/" />
<input type="submit" value="Go!" />
</fieldset>
</form>
<p></p>


<h3 id="years">Years</h3>

* **202x**: [2023](/year/2023/) [2022](/year/2022) [2021](/year/2021/) [2020](/year/2020/)
* **201x**: [2019](/year/2019/) [2018](/year/2018/) [2017](/year/2017/) [2016](/year/2016/) [2015](/year/2015/) [2014](/year/2014/) [2013](/year/2013/) [2012](/year/2012/) [2011](/year/2011/) [2010](/year/2010/)
* **200x**: [2009](/year/2009/) [2008](/year/2008/) [2007](/year/2007/) [2006](/year/2006/) [2005](/year/2005/) [2004](/year/2004/)


<h3 id="categories">Categories</h3>

* [Anime](/anime/), now mostly fig reviews
* [Annexe](/annexe/), imports from side blogs and closed sites
* [Hardware](/hardware/), computers, servers, phones, jaffle irons
* [Internet](/internet/), websites, online privacy and security, standards
* [Media](/media/), photography, journalism, music
* [Rubenerd Show](/show/), my long-running podcast of sillyness
* [Software](/software/), mostly the BSDs and macOS
* [Thoughts](/thoughts/), for other random stuff
* [Travel](/travel/), avaiation, public transport, and exploring


<h3 id="tags">Tags</h3>

You can view the [entire tag list here]. If you see a recurring topic, chances are there's a tag. For example, here's [coffee], [FreeBSD], and [privacy].

[coffee]: https://rubenerd.com/tag/coffee/
[FreeBSD]: https://rubenerd.com/tag/freebsd/
[privacy]: https://rubenerd.com/tag/privacy/
[entire tag list here]: /tag/ 


<h3 id="lists">Lists</h3>

Lists and other pages are now in the [Omake](/omake/) section. I'd link you to this archive page, but that'd be a bit redundant.


<h3 id="repositories">Repositories</h3>

* [Rubenerd.com], site source that I occasionally keep current!
* [Lunchbox], my consolidated repo for helper scripts, etc
* [Pinboard], my external link blog, previously [del.icio.us]

[Rubenerd.com]: https://gitlab.com/rubenerd/rubenerd.com
[Lunchbox]: https://gitlab.com/rubenerd/lunchbox
[Pinboard]: https://pinboard.in/u:Rubenerd
[del.icio.us]: https://del.icio.us/rubenerd


<h3 id="locations">Locations</h3>

I love sitting at coffee shops in random places and writing. I only started geo-tagging posts a few years ago and have yet to finish backfilling, so consider these archives incomplete.

* 🇦🇺 [Adelaide](/location/adelaide/)
* 🇦🇺 [Blue Mountains](/location/blue-mountains/)
* 🇭🇰 [Hong Kong](/location/hong-kong/)
* 🇲🇾 [Kuala Lumpur](/location/kuala-lumpur/)
* 🇯🇵 [Kyoto](/location/kyoto/)
* 🇺🇸 [Los Angeles](/location/los-angeles/)
* 🇦🇺 [Melbourne](/location/melbourne/)
* 🇺🇸 [New York](/location/new-york/)
* 🇺🇸 [Oakland](/location/oakland/)
* 🇯🇵 [Odawara](/location/odawara)
* 🇯🇵 [Osaka](/location/osaka/)
* 🇺🇸 [Philadelphia](/location/philadelphia)
* 🇺🇸 [San Francisco](/location/san-francisco/)
* 🇸🇬 [Singapore](/location/singapore/)
* 🇦🇺 [Sydney](/location/sydney/)
* 🇯🇵 [Tokyo](/location/tokyo/)


<h3 id="pages">Pages</h3>

Pages that don't belong anywhere else:

* [Archives](/archives/), how meta!
* [Help](/help/) for people who are unsure how to browse a weblog
* [Omake](/omake/) that has bonus and extra content
* [Terms](/terms/) for comments and usage, and general disclaimers

