---
layout: page
title: "Archives"
---
This blog has **[OVER 9000](https://rubenerd.com/over-9000/)** posts and pages, *including this one*. This page lists various ways you can find them.

<h3 id="search">Search</h3>

<p></p>
<form role="search" method="get" action="https://duckduckgo.com/">
<fieldset>
<legend>Search blog posts with DuckDuckGo.</legend>
<input type="text" size="40" name="q" placeholder="Search with DuckDuck…" />
<input type="hidden" name="ia" value="web" />
<input type="hidden" name="kaj" value="m" />
<input type="hidden" name="k7" value="w" />
<input type="hidden" name="k9" value="b" />
<input type="hidden" name="ks" value="l" />
<input type="hidden" name="sites" value="https://rubenerd.com/" />
<input type="submit" value="Go" />
</fieldset>
</form>
<p></p>


<h3 id="categories">Categories</h3>

Every post is filed under one of these, though I don't think they're especially useful anymore. Maybe I should convert them to tags?

* [Anime](/anime/)
* [Annexe](/annexe/)
* [Ethics](/ethics/)
* [Hardware](/hardware/)
* [Internet](/internet/)
* [Media](/media/)
* [Rubenerd Show](/show/)
* [Software](/software/)
* [Thoughts](/thoughts/)
* [Travel](/travel/)


<h3 id="tags">Tags</h3>

There are too many to mention here. You can view the full list below, or some examples:

* [Entire tag archive](/tag/)
* [#coffee](https://rubenerd.com/tag/coffee/)
* [#freebsd](https://rubenerd.com/tag/freebsd/)
* [#retrocomputing](https://rubenerd.com/tag/retrocomputing/)


<h3 id="years">Years</h3>

* [2024](/year/2024/)
* [2023](/year/2023/)
* [2022](/year/2022/)
* [2021](/year/2021/)
* [2020](/year/2020/)
* [2019](/year/2019/)
* [2018](/year/2018/)
* [2017](/year/2017/)
* [2016](/year/2016/)
* [2015](/year/2015/)
* [2014](/year/2014/)
* [2013](/year/2013/)
* [2012](/year/2012/)
* [2011](/year/2011/)
* [2010](/year/2010/)
* [2009](/year/2009/)
* [2008](/year/2008/)
* [2007](/year/2007/)
* [2006](/year/2006/)
* [2005](/year/2005/)
* [2004](/year/2004/) 


<h3 id="repositories">Repositories</h3>

* [Rubenerd.com site source](https://codeberg.org/rubenerd/rubenerd.com)
* [Lunchbox of useful scripts](https://codeberg.org/rubenerd/lunchbox)


<h3 id="locations">Locations</h3>

I love sitting at coffee shops in random places and writing. I only started geo-tagging posts a few years ago and have yet to finish backfilling, so consider these archives incomplete. Or not, I can't tell you how to live your life. Or... can I? How should I handle this newfound power?

* [Adelaide](/location/adelaide/)
* [Blue Mountains](/location/blue-mountains/)
* [Hong Kong](/location/hong-kong/)
* [Kuala Lumpur](/location/kuala-lumpur/)
* [Kyoto](/location/kyoto/)
* [Los Angeles](/location/los-angeles/)
* [Melbourne](/location/melbourne/)
* [New York](/location/new-york/)
* [Oakland](/location/oakland/)
* [Odawara](/location/odawara)
* [Osaka](/location/osaka/)
* [Philadelphia](/location/philadelphia)
* [San Francisco](/location/san-francisco/)
* [Singapore](/location/singapore/)
* [Sydney](/location/sydney/)
* [Tokyo](/location/tokyo/)

