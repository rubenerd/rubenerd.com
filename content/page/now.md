---
layout: page
title: "Now"
---
What I'm thinking about and working on... *now!* Projects may disappear and pop back as priorities change.

* Moving into our new apartment we just bought. We're still waiting to get the floors done and a final clean, but we're moving valuables into the laundry ourselves in advance. We also need to shift other valuables and old computers from the storage locker over.

* Blogging! It's my favourite daily thing.

* Evaluating NetBSD as a daily driver for my primary non-Mac laptop. I'm thinking of getting another second-hand ThinkPad.

* Putting together a nice home coffee setup. Saving money for a good grinder, and figuring out which light roasted beans I like, and medium roast beans Clara likes.

* \>10,000 steps a day, with at least 2,000 being "brisk". It's become rote now, and Clara and I have a healthy new baseline weight which is great.


### Credits

With [special thanks](https://nownownow.com/about) to Derek Sivers for the idea, and to [James](https://jamesg.blog/) for reminding me of it. These are all wonderful people. 
