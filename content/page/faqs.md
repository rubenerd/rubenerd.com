---
layout: page
title: FAQs
---
A frequently asked question is a question one may be asked frequently. These are some of the questions I'm frequently asked... frequently.

* [Me](#me)
* [Reading](#reading)
* [Writing](#writing)
* [Comments](#comments)
* [Cash Money](#cash-money)
* [Advice](#advice)
* [Lightning Round](#lightning-round)
* [Grab Bag](#grab-bag)


<h3 id="me"><a style="text-decoration:none" href="#me">🧑‍💻 &nbsp;Me</a></h3>

* **What’s your favourite kuih?** Talam kacang merah, served with a teh halia or kopi o kosong. Bagus!

* **Have you ever sneezed riding a bicycle?** Unfortunately.

* **Anything else?** The [about page](/about/) and [omake](/omake.opml) have more.


<h3 id="reading"><a style="text-decoration:none" href="#reading">📖 &nbsp;Reading</a></h3>

* **How do I read your site?** This site is a blog, which is like a public journal. Most days you'll see a couple of new posts appear on the [home page](https://rubenerd.com/), and the older ones move down and off to the [archives](https://rubenerd.com/archives/), so they can be read and be embarrassing years later.

* **What does the name mean?** It's been my unfortunate Internet handle since I was a kid, when I thought it was a clever concatenation of *Ruben* and *nerd*. Now I'm too attached to do the logical thing and replace it.

* **When did you start?** The end of December 2004, when I was in high school. Taipei 101 opened at the same time, though only one of us needed a tuned-mass damper.

* **What's RSS?** It's a format you can use to subscribe to this blog in an external reader, similar to how you might receive an email newsletter. I recommend [FeedLand](http://feedland.org/).

* **Why are your topics so random?** I post what interests me, or is on my mind. I read corporate and project blogs for my job, and you can tell when someone's heart isn't in it. Or on it, or underneath.

* **Does your blog have any outright lies or falsehoods?** Only one that I'm aware of, and it's in the next question.

* **Is this the best blog ever?** *&hellip;absolutely!*

* **Why do you have an anime mascot?** Because I love my partner's illustrations. The fact it infuriates some vocal people is only a bonus.


<h3 id="writing"><a style="text-decoration:none" href="#writing">⌨️ &nbsp;Writing</a></h3>

* **Why did you start?** My late mum Debra got me into journaling, so this was the natural progression. It's also a way to scratch my itch in a less formal setting, especially after a day of technical writing.

* **What do you use to write?** My [omake](/omake.opml) page lists this under *Rubenerd.com &rarr; How I Write*. I also employ my brain, both for formatting ideas, and interfacing with the keyboard.

* **How do you write regularly?** I don't have the mental space not to, if that makes sense. Don't look at me like that.

* **Have you ever written something you're not proud of?** At the time, no. Afterwards, yes. I guess that's how we grow.

* **Did other bloggers inspire you to start?** Too many to mention. But in 2004 my favourites were Jonathan Schwartz, John Walkenbach's J-Walk Blog, Om Malik, Dave Winer, Merlin Mann, and Doc Searls. Danny Choo and Dark Mirage got me into anime blogging, which one day I might even get back into properly.

* **Why aren't you more serious?** I've [answered that here](https://rubenerd.com/why-arent-you-more-serious/).

* **What's the best thing you've written?** Probably [this post from 2009](https://rubenerd.com/shower-philosophy-quandary/) about showers. It even has a *Split Enz* reference.


<h3 id="comments"><a style="text-decoration:none" href="#comments">🙋 &nbsp;Comments</a></h3>

* **Can I send comments?** Sure, the [about](https://rubenerd.com/about/#contact) page has my contact details. Thanks for taking the time, it does mean a lot.

* **Why don't you allow comments inline on posts?** Spam, simplicity, and because this is my site. I do publish the best ones people email.

* **Hacker News/Lobsters/Reddit linked to you again, will you read the comments/respond?** I'm flattered, but I'm good thanks.

* **Why didn't you [just] do, mention, consider, use, address, explain, contrast, compare...?** My interests, circumstances, preferences, use cases, locale, constraints, experiences, motives, priorities, and/or tastes may differ from yours. I apologise if this sounds flippant; those of you responding in good faith like this have long been drowned out by those who don't.

* **Why would you *want* to do foo?** As above.

* **Why do you suck?** To balance you being a legend, clearly!


<h3 id="cash-money"><a style="text-decoration:none" href="#cash-money">💰 &nbsp;Cash Money</a></h3>

* **Can I buy you a coffee?** [You sure can](https://www.ko-fi.com/A625HL)!

* **Can I advertise on your site?** I charge SG$9,001 a character. I'll even throw in the punctuation for free.

* **Can I hire you to write some docs?** Might be able to, [send me an email](https://rubenerd.com/about/#contact) and let's have a chat. 

* **Can I guest write, or link to my commercial site that your readers might find useful?** In the words of a ship's rigger, I'm a frayed knot.

* **I'm just following up in regards to my previous...** spam filter?


<h3 id="advice"><a style="text-decoration:none" href="#advice">😈 &nbsp;Advice</a></h3>

* **How do I start blogging?** Don't worry about the infrastructure, just write, in whatever time you have. A free Tumblr, WordPress.com, or Drummer account is a great place to start. We need your voice, and I'd love you on my [FeedLand river](http://my.feedland.org/Rubenerd).

* **I want to get into anime, where should I start?** [Barakamon](https://myanimelist.net/anime/22789/Barakamon).

* **I run Linux, should I try FreeBSD or NetBSD?** Absolutely! There's probably enough for you to feel at home, and the tooling is such a pleasure to use. It's also a great gateway to OpenZFS and the wider \*nix ecosystem. I warn you though, once you use a system that gives you full control, going back can be tough.

* **What's the best advice you've ever received?** [Merlin Mann](https://github.com/merlinmann/wisdom/blob/master/wisdom.md)\: Start acting like your life matters.

* **Hey that was good, have another?** [Jim Kloss](http://jimkloss.com/): Invest and save a bit each month that you wouldn't even notice, because in the future you will.


<h3 id="lightning-round"><a style="text-decoration:none" href="#lightning-round">⚡️ &nbsp;Lightning Round</a></h3>

* **Coffee or tea?** That'd be great, please.

* **Vim or Emacs?** Depends.

* **YAML or JSON?** XML.

* **Led Zep or Pink Floyd?** The Beatles, especially George and Paul.

* **DEB or RPM?** That's a weird way to spell pkgsrc.

* **DD-MM-YYYY or MM-DD-YYYY?** ISO 8601.

* **Do you not understand this format?** Reticulating splines.


<h3 id="grab-bag"><a style="text-decoration:none" href="#grab-bag">📎 &nbsp;Grab Bag</a></h3>

* **If you could travel back in time...** I'd have that [Aqua song](https://www.youtube.com/watch?v=Ls0WfopgR9k) stuck in my head.

* **What's your favourite song and album?** *Tiger in the Rain*, and *Tiger in the Rain*, by Michael Franks.

* **What's the most devices you've ever plugged into a USB hub?** Six, including the power brick.

* **What's the least-used app on your phone?** The phone app.

* **Have you ever boiled eggs to the point where all the water evaporated, the kitchen filled with smoke, and the apartment alarms alerted the local fire department, much to your embarrassment?** Not since last time.

* **How much wood would a woodchuck chuck if a woodchuck could chuck wood?** It's *chop*.

* **Do you observe the 5 second rule?** Not if I can avoid it.

* **What was your mother's maiden name? Where was your first primary school? What was your first brand of...** I use pseudo-random gibberish for secret questions too.

* **Can I ask you one last question?** I see what you did there.

