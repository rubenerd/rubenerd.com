---
layout: page
title: "Buttons"
---
<p>These graphics were a staple of early blogs, and even earlier websites. I missed them, but it soon became apparent the homepage sidebar would be overloaded if I included them all. Even more are on my <a href="http://retro.rubenerd.com/">retro site</a>.</p>

<p class="pixelart" style="margin-top:1.2em; line-height:1em">
<img src="https://rubenerd.com/files/buttons/8015ani.gif" alt="I love anime" />
<a href="https://www.archive.org/"><img src="https://rubenerd.com/files/buttons/8015archive.png" alt="The Internet Archive" /></a>
<img src="https://rubenerd.com/files/buttons/8015aldi.gif" alt="Aldi" />
<img src="https://rubenerd.com/files/buttons/8015australia.png" alt="Geo Australia" />
<a href="https://rubenerd.com/goodbye-bloglines-ill-miss-you/"><img src="https://rubenerd.com/files/buttons/8015bloglines.png" alt="Bloglines, RIP" /></a>
<img src="https://rubenerd.com/files/buttons/8015base.gif" alt="All your base are belong to us" />
<img src="https://rubenerd.com/files/buttons/8015beat.gif" alt="Beatles" />
<a href="https://opensource.org/license/bsd-3-clause/"><img src="https://rubenerd.com/files/buttons/8015bsdlicenced.gif" alt="BSD Licenced" /></a>
<img src="https://rubenerd.com/files/buttons/8015bi.gif" alt="Bisexual" />
<img src="https://rubenerd.com/files/buttons/8015camino.gif" alt="Camino, RIP" />
<img src="https://rubenerd.com/files/buttons/8015creative.gif" alt="Creative" />
<a href="https://creativecommons.org/licenses/by/3.0/"><img src="https://rubenerd.com/files/buttons/8015ccby.svg" alt="Creative Commons Attribution 3.0" /></a>
<img src="https://rubenerd.com/files/buttons/8015coffeegeek.png" alt="Coffee Geek" />
<img src="https://rubenerd.com/files/buttons/8015cfe.gif" alt="Coffee Powered" />
<a href="https://www.dublincore.org/"><img src="https://rubenerd.com/files/buttons/8015dublincore.png" alt="Dublin Core Metadata Initiative" /></a>
<img src="https://rubenerd.com/files/buttons/8015emacs.png" alt="Emacs" />
<img src="https://rubenerd.com/files/buttons/8015empty.png" alt="Empty" />
<img src="https://rubenerd.com/files/buttons/8015nerv.png" alt="Evangelion" />
<a href="https://fedoraproject.org/"><img src="https://rubenerd.com/files/buttons/8015fedora.png" alt="Fedora Powered" /></a>
<a href="https://getfirefox.com/"><img src="https://rubenerd.com/files/buttons/8015firefox.gif" alt="Firefox" /></a>
<a href="http://fluxbox.org/"><img src="https://rubenerd.com/files/buttons/8015fluxbox.png" alt="Fluxbox" /></a>
<img src="https://rubenerd.com/files/buttons/8015haruhi.png" alt="Haruhiism!" />
<img src="https://rubenerd.com/files/buttons/8015goldfish.gif" alt="Goldfish" />
<a href="https://rubenerd.com/"><img src="https://rubenerd.com/files/buttons/8015home.gif" alt="Home" /></a>
<a href="https://validator.w3.org/nu/?doc=https%3A%2F%2Frubenerd.com%2Fbuttons%2F"><img src="https://rubenerd.com/files/buttons/8015html.png" alt="Valid HTML" /></a>
<a href="https://portswigger.net/research/http2"><img src="https://rubenerd.com/files/buttons/8015http11.png" alt="HTTP 1.1" /></a>
<img src="https://rubenerd.com/files/buttons/8015ibmthink.gif" alt="IBM ThinkPad" />
<img src="https://rubenerd.com/files/buttons/8015ibook2002.gif" alt="iBook Dual USB, RIP" />
<a href="https://indieweb.org/"><img src="https://rubenerd.com/files/buttons/8015indieweb.svg" alt="IndieWeb" style="width:80px;" /></a>
<a href="https://rubenerd.com/about/"><img src="https://rubenerd.com/files/buttons/8015info.png" alt="Info: About"></a>
<a href="https://www.inkscape.org/"><img src="https://rubenerd.com/files/buttons/8015inkscape.png" alt="Inkscape" /></a>
<img src="https://rubenerd.com/files/buttons/8015ipod.gif" alt="iPod 3G, RIP" />
<a href="https://www.libreoffice.org/"><img src="https://rubenerd.com/files/buttons/8015lo.png" alt="LibreOffice" /></a>
<img src="https://rubenerd.com/files/buttons/8015mean.gif" alt="Mean People Suck" />
<img src="https://rubenerd.com/files/buttons/8015mikuru.png" alt="Mikuruism!" />
<img src="https://rubenerd.com/files/buttons/8015nobuttons.gif" alt="No Buttons" />
<img src="https://rubenerd.com/files/buttons/8015nycpizza.png" alt="NYC Pizza" />
<a href="https://rubenerd.com/omake.xml"><img src="https://rubenerd.com/files/buttons/8015omake.png" alt="Omake" /></a>
<a href="https://rubenerd.com/blogroll.opml"><img src="https://rubenerd.com/files/buttons/8015opml.png" alt="OPML: Share It!" /></a>
<a href="https://openzfs.org/"><img src="https://rubenerd.com/files/buttons/8015openzfs.png" alt="OpenZFS" /></a>
<img src="https://rubenerd.com/files/buttons/8015ourmedia.gif" alt="Ourmedia (RIP)" />
<a href="https://www.perl.org/"><img src="https://rubenerd.com/files/buttons/8015perl.gif" alt="Perl" /></a>
<img src="https://rubenerd.com/files/buttons/8015phoenix.gif" alt="Mozilla Phoenix, we remember thee" />
<img src="https://rubenerd.com/files/buttons/8015pie.png" alt="Mmm, pie" />
<a href="https://github.com/ibara/oksh"><img src="https://rubenerd.com/files/buttons/8015oksh.png" alt="oksh" /></a>
<img src="https://rubenerd.com/files/buttons/8015oss.png" alt="Open Source" style="width:80px;" />
<img src="https://rubenerd.com/files/buttons/8015otaku.gif" alt="Otaku" />
<a href="https://postgres.org/"><img src="https://rubenerd.com/files/buttons/8015pg.png" alt="Postgres" /></a>
<img src="https://rubenerd.com/files/buttons/8015pride.gif" alt="Pride!" />
<a href="http://showfeed.rubenerd.com/"><img src="https://rubenerd.com/files/buttons/8015pod.png" alt="RSS Podcast feed" /></a>
<img src="https://rubenerd.com/files/buttons/8015rssvalid.gif" alt="RSS Valid" />
<a href="https://sqlite.org/"><img src="https://rubenerd.com/files/buttons/8015selfhosted.gif" alt="Self hosted" /></a>
<img src="https://rubenerd.com/files/buttons/8015singapore.gif" alt="Singapore" />
<a href="https://sqlite.org/"><img src="https://rubenerd.com/files/buttons/8015sqlite.png" alt="SQLite" /></a>
<img src="https://rubenerd.com/files/buttons/8015startrek.gif" alt="Star Trek" />
<a href="http://tribblix.org/"><img src="https://rubenerd.com/files/buttons/8015sunsolaris.gif" alt="Sun Solaris" /></a>
<img src="https://rubenerd.com/files/buttons/8015svg1.gif" alt="SVG" />
<img src="https://rubenerd.com/files/buttons/8015tea.gif" alt="Tea Powered" />
<a href="https://rubenerd.com/technorati/"><img src="https://rubenerd.com/files/buttons/8015tnrt.gif" alt="Technorati, RIP" /></a>
<img src="https://rubenerd.com/files/buttons/8015tintin.gif" alt="Tintin" />
<a href="http://thunderbird.org/"><img src="https://rubenerd.com/files/buttons/8015thunderbird.gif" alt="Thunderbird" /></a>
<img src="https://rubenerd.com/files/buttons/8015t3.png" alt="Tungsten T3 powered, RIP" />
<a href="https://home.unicode.org/"><img src="https://rubenerd.com/files/buttons/8015unicode.png" alt="Unicode" /></a>
<a href="https://www.vim.org/"><img src="https://rubenerd.com/files/buttons/8015vim.png" alt="Vim editor" /></a>
<img src="https://rubenerd.com/files/buttons/8015validcss.png" alt="Valid CSS" />
<img src="https://rubenerd.com/files/buttons/8015winamp.gif" alt="Winamp" />
<a href="https://en.wikipedia.org/wiki/Whole_Wheat_Radio"><img src="https://rubenerd.com/files/buttons/8015wwr.png" alt="Whole Wheat Radio, RIP" /></a>
<a href="https://rubenerd.com/feed/"><img src="https://rubenerd.com/files/buttons/8015rss.gif" alt="XML RSS Feed" /></a>
<a href="https://rubenerd.com/a-2024-yellow-pages-monitor-stand/"><img src="https://rubenerd.com/files/buttons/8015yp.gif" alt="Yellow Pages"></a>
<img src="https://rubenerd.com/files/buttons/8015yuki.png" alt="Yukiism!" />
</p>


