---
layout: page
title: "About"
---
<img src="https://rubenerd.com/files/2017/me.jpg" alt="Me!" style="float:right; margin:0 0 1em 2em; height:140px; width:140px; border-radius:128px;" />

* [Abstract](#abstract)
* [Me](#me)
* [The site](#site)
* [Rubi the mascot](#mascot)
* [Geek code](#geekcode)
* [Contact](#contact)


### Abstract

I've long been struck (ouch) by the futility of employing washroom hand dryers. Beyond those *Airblade* contraptions that sonically wash your eardrums in tandem, you press a button only to find a semblance tepid air hitting your responsibly-washed hands.


<h3 id="me">$(whoami)</h3>

My name is Ruben Schade, though I've been called Reuben Shade and "do I know you"? I adore vintage computer hardware, writing in coffee shops in the mid-morning, and non-fiction books. My beautiful girlfriend Clara and I spend far too much time drinking bubble tea and [travelling].

I make a living as a technical writer and solution architect for an [IaaS company](https://www.orionvm.com/) in Sydney and San Francisco, but I grew up in Singapore where I developed a taste for well-funded infrastructure, lah. I'm also working up the guts to do more speaking and attend more industry events, especially for the BSDs.

*Rubenerd* has been my unfortunate Internet handle since I was 13; at the time I thought it was a clever concatenation. Now I just think it's delightfully self-deprecating, just like my [last name](https://en.wiktionary.org/wiki/schade#Adjective).

[Clara]: http://kiri.sasara.moe/
[podcast]: https://rubenerd.com/show/
[travelling]: https://instagram.com/RubenSchade


<h3 id="site">The site</h3>

*Rubenerd* is my weblog and [podcast] of things that have interested me since 2004. I write it because it's fun; if people enjoy reading it, so much the better. The [archives] list all the topics.

The design is a bit of a protest of what the modern web has become right under our noses, and other body parts. There aren't thirty layers of obfuscated divs, tracking, banner ads, font downloads, embeds, hamburger icons, looping background animations, disingenuous cookie notices, scroll-jacking, popups, UTM link spam, grey text, newsletter signup prompts, autoplaying videos, lightboxes, fonts as big as your head, or sticky navbars. It's to respect you as a reader, and makes the site easier to maintain.

I publish an [RSS feed](/feed/) for blog posts, and [just for the podcast](http://showfeed.rubenerd.com/) you can subscribe to. Check out the [subscribe](/subscribe/) page if you need help.

The backend is proudly powered by the [Hugo] static site generator on a [FreeBSD] VM on [OrionVM]. [Omake] has more details.

I also used to maintain a [separate anime blog], which one day I'll piece back together here.

[archives]: /archives/
[Hugo]: https://gohugo.io/
[omake]: /omake.opml
[has become]: https://rubenerd.com/modernwebbloat-js/ "Blog post on ModernWebBloat.js"
[FreeBSD]: https://www.freebsd.org/
[OrionVM]: https://www.orionvm.com/
[separate anime blog]: https://rubenerd.com/anime-restore-my-blog/


<h3 id="mascot">Rubi the mascot</h3>

<img src="https://rubenerd.com/rubi@2x.jpg" alt="" style="width:128px; float:right; margin:0 0 1em 1em" />

Rubi is the site's mascot, conceived by Clara Tse. Her mismatching, Miku-esque boots allude to my predilection for not wearing matching socks.

*(Funny story, I was chided for wearing orange and red socks during a high school work experience day, with the implication that I wasn't taking the assignment seriously. The day I'm looked over on account of my socks is when I miss a bullet).*

We agreed that Rubi was born in Sapporo on her birthday. Her favourite foods are shiroi koibito biscuits and curry udon, washed down with green tea and copious amounts of black coffee. These are burned off by trips to indoor pools and nature hiking in the snowy Hokkaido wilderness. She enjoys reading, cosplaying, and slice-of-life anime that may or may not have robots. Her dream is to start a line of self-heating swimsuits and towels, and tactical cargo skirts with pockets that can actually fit modern smartphones.


<h3 id="geek-code">Geek code</h3>

    -----BEGIN GEEK CODE BLOCK-----
    Version: 3.1
    GIT/TW d+ s+:-- a C+++ UBL++++$ P+++>++++ L+ E--- 
    W+++ N+ !o? K-? w--$ O+ M++$ V PS+ PE- Y+ PGP+ t++ 
    5+ X+ R tv b++ DI-- D G+ e++ h r++>+++ y+
    ------END GEEK CODE BLOCK------ 


<h3 id="contact">Contact me</h3>

Thanks for taking an interest in my stuff! You can message me on [Mastodon], [Twitter], or email:

    printf "%s@%s.%s\n" me rubenschade com

If you're responding to a specific post, please be civil and read it first. I [have an FAQ](/faqs/#comments) if you're not sure. Cheers.

[comment policy]: /omake/terms/#comments
[Mastodon]: https://bsd.network/@rubenerd
[Twitter]: https://twitter.com/rubenerd
[PGP public key]: https://pgp.mit.edu/pks/lookup?op=vindex&search=0x9CFC8AEBBD528543
[Pinboard]: https://pinboard.in/u:Rubenerd
[GitLab]: https://gitlab.com/users/rubenerd/projects
[Flickr]: https://flickr.com/photos/rubenerd
[Instagram]: https://instagram.com/RubenSchade
[Wikipedia]: https://en.wikipedia.org/wiki/User:RubenSchade
[YouTube]: https://www.youtube.com/user/rubenerd
[Vimeo]: https://vimeo.com/rubenerd

