---
layout: page
title: "About"
---
<img src="https://rubenerd.com/files/2017/me.jpg" alt="Me!" style="float:right; margin:0 0 1em 2em; height:140px; width:140px; border-radius:128px;" />

* [Welcome to country](#welcome)
* [whoami(1)](#me)
* [The site](#site)
* [Usage and licencing](#usage)
* [Dedications](#dedications)
* [Rubi the mascot](#mascot)
* [Contact](#contact)


<h3 id="welcome">Welcome to country</h3>

The servers hosting our sites reside on the lands of the [Gadigal people](https://en.wikipedia.org/wiki/Gadigal) of the Gadi/Eora nation, to whom I offer my love and respect. 🖤💛❤️


<h3 id="me">whoami(1)</h3>

I'm [Ruben Schade](https://rubenschade.com/ "My glorified business card site"), and I'm an aspiring human. I'm into retrocomputing, writing in coffee shops, anime, and tinkering with server hardware. My partner Clara and I live for travelling, bubble tea, and drinking bubble tea while travelling.

I grew up in Singapore where I developed a taste for well-funded infrastructure, lah. Today I refill the coffee machines (cough) at an Australian IaaS company, and may also do technical writing and solution architecture. I'm also working up the guts to do more speaking and attend more industry events, especially for the fine [FreeBSD](https://www.freebsd.org/ "FreeBSD project home page") and [NetBSD](https://www.netbsd.org/ "NetBSD project home page") operating systems.


<h3 id="site">The site</h3>

This is my weblog and podcast of things that have interested me since school in 2004. It started as a hobby, and now it's the longest running project I've ever done.

I publish an [RSS feed for blog posts](https://rubenerd.com/feed/ "Rubenerd blog post feed"), and [just for the podcast](http://showfeed.rubenerd.com/ "Rubenerd Show podcast feed") you can subscribe to. My [Omake](https://rubenerd.com/omake.xml "My Omake file of everything") has way more details.

*Rubenerd* has been my unfortunate Internet handle since I was 13; at the time I thought it was a clever concatenation. Now I think it's delightfully self-deprecating, like my [last name in German](https://en.wiktionary.org/wiki/schade#Adjective "Wiktionary entry on my last name Schade").


<h3 id="usage">Usage and licences</h3>

This blog's text is licenced under [Creative Commons Attribution 3.0](https://creativecommons.org/licenses/by/3.0/deed.en) and [3-Clause BSD](https://opensource.org/license/BSD-3-clause). Broadly, these permit you to distribute and remix, provided you attribute the source. This includes creating derivative works with a large-language model.


<h3 id="dedications">Dedications</h3>

This site is dedicated to the memory of these lovely people:

* **Debra Schade**, my mum and best friend who encouraged me to write and keep a journal; the advice that has set me up for life.

* **Jim Kloss**, the proprietor of *Whole Wheat Radio* who proved you could be in tech with kindness and a conscience.


<h3 id="mascot">Rubi the mascot</h3>

<img src="https://rubenerd.com/rubi2024@1x.jpg" srcset="https://rubenerd.com/rubi2024@2x.jpg 2x" alt="Rubi the Mascot, drawn by Clara" style="width:128px; float:right; margin:0 0 1em 1em" />

Rubi is the site's mascot, conceived and drawn by my partner Clara.

We agreed that Rubi was born in Sapporo on her birthday. Her favourite foods are shiroi koibito biscuits and curry udon, washed down with matcha and copious amounts of black coffee. These are burned off by trips to indoor pools and nature hiking in the snowy Hokkaido wilderness. She enjoys reading, cosplaying, and slice-of-life anime that may or may not have robots. Her dream is to start a line of self-heating swimwear and towels, and tactical cargo skirts with pockets that can fit modern smartphones.

She's getting a fuller wardrobe of late. Right now she's sporting overalls and a classic Apple hairclip as she prepares to work on my Apple //e. More info on that machine is on my [retro site](http://retro.rubenerd.com/).


<h3 id="contact">Contact me</h3>

Thanks for taking an interest in my stuff. You can email me below:

    printf "%s@%s.%s\n" me rubenschade com

I'm terrible at responding, but I read everything I receive!

Since 2025 I've found it necessary to have some [rules for comments](https://rubenerd.com/feedback-rules/). They can be boiled down to *don't be a dick*, but please read these first if there's any ambiguity.

You can also catch me on the following sites. Cheers :)

* [Bluesky Social](https://bsky.app/profile/rubenerd.bsky.social)
* [Codeberg](https://codeberg.org/rubenerd)
* [FreeBSD Wiki](https://wiki.freebsd.org/RubenSchade)
* [Mastodon, on the BSD.Network](https://bsd.network/@rubenerd)
* [Twitter (RIP)](https://twitter.com/rubenerd)
* [Wikipedia](https://en.wikipedia.org/wiki/User:RubenSchade)
