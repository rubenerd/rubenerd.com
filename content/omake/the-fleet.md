---
layout: omake
title: "The fleet"
---
This page details the current machines I use, including vintage ones. I've got plenty sitting in storage I'll add one day. For those expecting a page about anime ship girls, please refer to [my 2018 post](https://rubenerd.com/azur-lane-neptunia/) on the surreal subject.


### *MacTheKnifeII* – The 16-inch 2019 MacBook Pro

My primary Mac and desktop replacement, in the spirit of my first 15-inch Intel MacBook Pro from 2006. Her discrete GPU is fast enough to play the few simulation and world-builder games I care about, and during the day she smashes through all my work and Windows VMs.

<p><img src="https://rubenerd.com/files/2020/fleet-macbookpro16@1x.png" srcset="https://rubenerd.com/files/2020/fleet-macbookpro16@1x.png 1x, https://rubenerd.com/files/2020/fleet-macbookpro16@2x.png 2x" alt="" style="width:128px; float:right; margin:0 0 1em 2em" /></p>

* **OS:**      macOS Catalina 10.15
* **Desktop:** Aqua
* **Shell:**   oksh (OpenBSD portable KornShell)
* **Display:** 16-inch 3072×1920 Retina
* **CPU:**     Intel Core i7 2.6 GHz, 6-core
* **Memory:**  16 GiB 2667 MHz DDR4
* **GPU:**     AMD Radeon Pro 5500M, 8 GiB
* **SDD:**     512 GiB


### *lum* – The Panasonic CF-RZ6 Let's Note

<p><img src="https://rubenerd.com/files/2019/fleet-letsnote-cf-rz6@1x.jpg" srcset="https://rubenerd.com/files/2019/fleet-letsnote-cf-rz6@1x.jpg 1x, https://rubenerd.com/files/2019/fleet-letsnote-cf-rz6@2x.jpg 2x" alt="" style="width:128px; float:right; margin:0 0 1em 2em;" /></p>

My daily carry and on-call machine, named for the cult 1980s anime character from *Urusei Yatsura*. Bought after AsiaBSDCon 2019 in Akihabara. She's implausibly light, small, quick, and has more ports than my Macs.

* **OS:**      FreeBSD 12.1-STABLE
* **Desktop:** Xfce
* **Shell:**   oksh (OpenBSD portable ksh)
* **Display:** 10.1-inch 1920×1200, HiDPI
* **Weight:**  0.775 kg
* **RAM:**     8 GiB LPDDR3
* **GPU:**     Intel HD Graphics 615


### *holo* – The FreeBSD bhyve box

<p><img src="https://rubenerd.com/files/2020/antec300.jpg" alt="Photo of the Antec 300 case" style="width:128px; float:right; margin:0 0 1em 2em;" /></p>

She replaced my old HP Microservers as my primary homelab machine. She's built from second-hand Gumtree and eBay parts around an old Antec 300 case. She runs guests and jails for everything I can offload from my Macs, including our SSH gateway, web proxy, sandbox, CD ripper, home router, netatalk file sharer, Plex host, Minecraft server, and Musikcube music streaming source, among others. Named for Hololive, because her guests and jails are named for their characters.

* **Logicboard:** Supermicro X11SAE-M, C236
* **OS:**         FreeBSD 12.2-RELEASE
* **Hypervisor:** bhyve
* **Shell:**      OpenBSD Portable oksh (/bin/sh for root)
* **CPU:**        Intel Xeon E3-1275 v6 (4C/8T)
* **RAM:**        16 GiB Kingston ECC PC4-19200U
* **SSD:**        SanDisk Ultra 240 GB
* **HDD:**        HGST and Seagate IronWolves in mirrored ZFS pools 
* **ODD:**        Lenovo DU-8A6SH 9.5mm DVD writer
* **SCSI:**       *TBD, for below*
* **Pointless:**  Internal Iomega Zip and Jaz Drives


### *ami* – The Pentium MMX tower

<p><img src="https://rubenerd.com/files/2019/mmx@1x.jpg" srcset="https://rubenerd.com/files/2019/mmx@1x.jpg 1x, https://rubenerd.com/files/2019/mmx@2x.jpg 2x" alt="" style="width:128px; height:103px; float:right; margin:0 0 1em 2em;" /></p>

She was the first computer I built as a kid in Singapore, with the help of the wonderful former Make Fine Computer in Funan Centre. She also inherited parts from the long-departed family 486. Named for my favourite *Sailor Moon* herione from back in the day.

* **OS:**    MS-DOS 6/WFW, NT 4.0, NetBSD 8
* **CPU:**   Intel Pentium 200 *with MMX Technology*
* **RAM:**   64 MiB PC-100 SD-RAM
* **HDD:**   32 GiB and 16 GiB SanDisk CompactFlash cards
* **ODD:**   Creative Infra48 IDE
* **FDD:**   3.5" 1.44 MiB Matsushita, 5.25" 1.2 MiB Panasonic
* **Expansion:**  Iomega 2 GiB SCSI Jaz, Iomega 100 MiB Zip
* **GPU:**   Matrox Mystique 200 VGA IDE card
* **NIC:**   Compex RL2000 FastEthernet, PCI
* **SCSI:**  Iomega Jaz Jet SCSI accelerator, PCI
* **Sound:** Creative SoundBlaster AWE32, ISA


### The Commodore 128

<img src="https://rubenerd.com/files/2021/commodore128@1x.jpg" srcset="https://rubenerd.com/files/2021/commodore128@1x.jpg 1x, https://rubenerd.com/files/2021/commodore128@2x.jpg 2x" alt="" style="width:128px; float:right; margin:0 0 1em 2em;" />

Hostname TBD. This is my ultimate 8-bit nostalgia machine, because of all her operating modes and CPUs! She was donated by Josh Nunn of [The Geekorium](https://the.geekorium.com.au/), and I've been slowing restoring her since.

* **OS:**    Commodore KERNAL 128, KERNAL 64, CP/M, GEOS
* **Shell:** BASIC 7, Desterm128
* **CPU:**   MOS 8502 (6510 compatible), Zilog Z80
* **RAM:**   128 KiB
* **FDD:**   Commodore 1541 (need a 1571 one day!)
* **GPU:**   MOS 8563 VDC (80 column), VIC-II E (40 column)
* **Sound:** MOS SID 6581


### *mio* – The Toshiba Libretto 70CT

<p><img src="https://rubenerd.com/files/2019/libretto@1x.jpg" srcset="https://rubenerd.com/files/2019/libretto@1x.jpg 1x, https://rubenerd.com/files/2019/libretto@2x.jpg 2x" alt="" style="width:128px; float:right; margin:0 0 1em 2em" /></p>

This was my dream machine as a kid, and armed with eBay and a salary I was finally able to get one! She's named for the shy bassist from *K-On!*, that light music anime that got me through hard times.

* **OS:**      Windows 95 with Plus!, NetBSD i386
* **CPU:**     Intel Pentium 120 MHz "with MMX Technology"
* **RAM:**     32 MiB
* **Display:** 6.1-inch 640×480
* **Memory:**  32 MiB PC-100 SD-RAM
* **Size:**    210×132×635 mm, 850 g
* **HDD:**     64 GiB SanDisk CompactFlash card
* **FDD:**     External Toshiba with PCMCIA interface
* **NIC:**     Xircom CreditCard Ethernet PCMCIA


### Others

These are in various states of repair or upgrading:

* **Commodore 128:** donated by [Screenbeard](https://the.geekorium.com.au)!
* **Commodore Plus/4:** bought for my 18th birthday in the 2000s
* **iBook G3:** needs replacement hinge
* **iMac DV:** blueberry, needs new logic board
* **MacBook Pro:** first generation, working but not in use
* **ThinkPad X61s:** needs replacement screen


### *In Memoriam*

* **HP Brio BAx:** Pentium III desktop, wrecked after a house move
* **Retina 27-inch iMac:** 2017 model, sold because I never used it
* **ThinkPad 600E:** leaking battery destroyed it
* **ThinkPad X40:** lost during a house move

