---
layout: page
title: Software
---
This page describes software I prefer and use on a daily basis. Visit the [engine room](https://rubenerd.com/omake/engine-room/) for what powers this site.


### Contents

<img src="https://rubenerd.com/files/uploads/bsd.daemon.png" alt="BSD Daemon" style="width:84px; float:right; margin:0 0 1em 2em;" />

* [Platforms](#platforms)
* [Applications](#applications)
* [Scripting](#scripting)


<h3 id="platforms">Platforms</h3>

I've been a desktop [macOS] user since the iconic blueberry iMac DV. It's what I'm used to, and lets me run Office for work files without Windows. Mac laptops have the best displays and battery life, and play the few games I care about.

[FreeBSD] is my preferred server operating system, and for running on an ultraportable for when I'm on call. It doesn't over-enginner new wheels or chase the shiny. It's stable, fast, has excellent documentation, the industry's best ZFS integration, and mature tooling with DTrace, Capsicum, and jails. You can rebuild the kernel and world so easily, and it has a reasonable init system. I still harbour a soft spot for [NetBSD](https://www.netbsd.org/) and [illumos] too; they're are fine OSs in the true \*nix tradition.

[Debian] is my go-to (GNU/)Linux distribution. It's predictable, easy enough to administer, and widely supported. I held off from learning and using systemd, but just like git, the industry has spoken.

I use the [Xen] hypervisor instead of using container orchestration if I can avoid it; all the latter does is push the burden from deployment to maintenance and troubleshooting. I'm also dabbling with bhyve, and QEMU still comes in handy for quick tests.

I still run MS-DOS 6, Windows for Workgroups, and Windows 95 (with Plus!) on my childhood PC because I'm a nostalgic fool.

[macOS]: https://apple.com/macos/
[FreeBSD]: https://www.freebsd.org/
[Debian]: https://www.debian.org/
[illumos]: https://www.illumos.org/
[Xen]: https://xenproject.org/


<h3 id="applications">Applications</h3>

I prefer using cross-platform software wherever I can. It's a platform insurance policy, and because I find it difficult to commit to things! 

The last macOS-specific holdouts are Microsoft Office for work&mdash;unfortunately&mdash;and Acorn which is significantly faster at opening and tweaking large files than either the Gimp or Krita. Ferdi keeps all the bloated Electron chat apps behind one window which is easy to close when the work day is over. *One day* I'll implement [BitlBee]!

As of 2020 I've consolidated a lot of my life into [GNU Emacs] with [Elfeed] for reading RSS, [ERC] for IRC, and [Org Mode] for capturing notes. It reminds me of the best parts of my favourite ever editor TextMate, though I can also run it on FreeBSD. That said, I still run [Vim] as my primary blog and text editor.

I've been a [Firefox] user since the Phoenix days, save for brief forays with SeaMonkey and Camino. It's just as important now as it was when we were dealing with the first browser monoculture. I run it with [Decentraleyes], [KeePassXC-Browser], [NoScript], [Tree Style Tab], and [uBlock Origin].

I write in [LaTeX] wherever I can, which I manage with [pkgsrc]. I still use Homebrew for graphical applications on the Mac, but pkgsrc works *so well* for tooling.

I do envelope budgeting, planning, and heaven forbid some home database stuff in long-running [LibreOffice] spreadsheets. I unashamedly love spreadsheets; I can often surface and process data with formulas and filters faster than any other way.

Our home FreeBSD media server runs [Plex] to stream content to our AppleTV, and I play media locally with [mpv] and [youtube-dl]. I'm still in the market for a good, cross-platform music player with local media libraries.

[Vim]: https://www.vim.org/
[GNU Emacs]: https://www.gnu.org/software/emacs/
[Elfeed]: https://nullprogram.com/blog/2013/09/04/
[ERC]: https://www.gnu.org/software/emacs/erc.html
[Org Mode]: https://orgmode.org/

[BitlBee]: https://www.bitlbee.org/
[LaTeX]: https://www.latex-project.org/
[Firefox]: https://getfirefox.com/
[LibreOffice]: https://www.libreoffice.org/discover/calc/
[mpv]: https://mpv.io/
[pkgsrc]: https://www.pkgsrc.org/
[Plex]: https://www.plex.tv/
[youtube-dl]: http://youtube-dl.org/

[Decentraleyes]: https://addons.mozilla.org/en-GB/firefox/addon/decentraleyes/
[KeePassXC-Browser]: https://addons.mozilla.org/en-GB/firefox/addon/keepassxc-browser/
[NoScript]: https://addons.mozilla.org/en-SG/firefox/addon/noscript/
[Tree Style Tab]: https://addons.mozilla.org/en-GB/firefox/addon/tree-style-tab/
[uBlock Origin]: https://addons.mozilla.org/en-GB/firefox/addon/ublock-origin/

<h3 id="scripting">Scripting</h3>

Most of my personal scripts and automation are still done in [Perl 5](https://perl.org/) which I learned in high school. It's so flexible, and I love its data structures. I've dabbled in Ruby and Python which I've liked, but not enough to switch. These are now version controlled in git after begrugingly making the switch from hg and Subversion.

I use portable [Bourne shell](https://www.freebsd.org/cgi/man.cgi?query=sh) for quick jobs, or for scripts that will end up on a server. You can't&mdash;or shouldn't&mdash;assume bash will be available on a target system, even on Linux.

As many of my machines as possible are defined in [Ansible](https://www.ansible.com/) playbooks. It's useful as a prescriptive form of documentation as much as it makes my life easier installing and maintaining systems.

I want to pick up Emacs lisp, and maybe Rust at some point. I'm not a developer anymore, but I like to learn new things.


<h3 id="games">Games</h3>

Writing is my main hobby, so I'm in Emacs most of the time!

I love open-world and puzzle games, so my favourites are the enduring SimCity franchise, Cities Skylines, and Minecraft.

I've bought every version of Need for Speed and Train Simulator going back to the start, though I shamelessly just use them to explore. Clara and I even went to specific places in Japan so I could revisit where I'd been in those games; interpret that how you will.

I've managed to avoid all other gatcha and mobile games, but I got hooked on Fate/Grand Order based on my love of the franchise.
