---
layout: omake
title: "Engine room"
abstract: "A list of what makes this site possible, and how posts are written"
---
This page lists the tech that makes this blog possible.

* [How I write posts](#writing)
* [What runs the site](#runs)
* [What used to run the site](#historical)


<h3 id="writing">How I write posts</h3>

**[Vim](https://vim.org)**

: I've tried everything, and keep coming back to this legendary editor. I'm still learning new things with it even after a decade. [NERDTree](https://github.com/scrooloose/nerdtree) is still my favourite plugin.

**[Emacs](https://www.gnu.org/software/emacs/)**

: Wait, Emacs too? Yes! I fell in love with org-mode as a cross-platform alternative to nvALT for managing notes, research, tasks, and snippets of text; and have starting finding more uses for it. I should probably choose a single editor one day, but for now I'm enjoying both.

**[Kensington Orbit Trackball](https://www.kensington.com/p/products/control/trackballs/orbit-trackball-with-scroll-ring/)**

: The only trackball mouse I've found with a scroll wheel. Once you use a trackball, you never want to go back to inferior pointing devices.

**[Git for version control](https://git-scm.org/)**

: Each blog post and site asset is version controlled with Git, which makes updates easier. I far prefer [Subversion](https://svnvsgit.com/), and even [Mercurial](https://www.mercurial-scm.org/) is better, but the industry has spoken.

**[Michael Franks](http://www.michaelfranks.com/)**

: I'm often writing while listening to my favourite singer/songwriter of all time. His over half a century career include albums for every mood and time of day. Incidently, I wrote all but two of his albums on Wikipedia.<p></p>

**[Coffee shops](/omake/coffee/)**

: If there's an environment more conducive to positive thought and writing, I don't want to know.


<h3 id="runs">What runs the site</h3>

**[OrionVM](https://www.orionvm.com/)**

: The world's easiest cloud. It's written and maintained by people who actually answer the phone, and care about the details. It's designed for wholesale infrastructure and white-labelling, but [ping the sales guys](https://www.orionvm.com/contact-us/) and I'm sure they could hook you up. Disclosure, I'm one of the engineers!

**[Hugo](http://gohugo.io)**

: This is the Porsche of static site generators; it's difficult to handle at times with Go's inscrutable templating, but it's the only one around that can handle 7,000+ posts without taking the heat death of several universes to render.<p></p>

**[FreeBSD](https://www.freebsd.org/)**

: It's still my preferred server operating system since trying it in high school. It's fast, Unixy, and has the best OpenZFS integration for snapshotting, updates, optimisation, and data integrity. I use cloud instances with jails to keep things secure and easy to update.<p></p>

**[nginx](https://nginx.org/)**

: The fast, simple to configure web server and reverse proxy. Thanks to the maintainers of the [nginx-devel](https://www.freshports.org/www/nginx-devel) FreeBSD port.<p></p>

**[Let's Encrypt](https://letsencrypt.org/)**

: I bought HTTPS certs in the past, but this makes the process so simple. It can also now handle subdomains with little fuss.<p></p>

**[Ansible](https://www.ansible.com/)**

: All the site configuration, package installs and updates are carried out with Ansible playbooks.<p></p>

**[Bourne shell scripts](https://en.wikipedia.org/wiki/Bourne_shell)**

: These are the glue for everything else, for podcast pages, encoding audio, scaling Retina&trade; images, uploading generated assets, and other tasks. No bashisms.<p></p>


<h3 id="historical">What used to run the site</h3>

**Hand-coded HTML, 1998–04**

: This was a general-interest site before a blog, and was written with Notepad back in my Windows days. Some may have also been prototyped in Frontpage back in the day too. I kinda miss it.

**[Perl CGI scripts](http://perl.org/), 2004–05**

: I wrote my first site engine while learning Perl at a job between high school and university. It used CGI, but fortunately it was so obscure and rarely visited it didn't spawn too many threads!

**[RapidWeaver](http://realmacsoftware.com/rapidweaver/), 2005**

: An intriguing and pleasant Mac application that generated static pages, but I soon ran into limitations with updating sites from different places. Thesedays you could probably use a Dropbox-like service.

**[Blosxom](http://blosxom.sourceforge.net), 2005**

: Another short-lived CMS before making the jump to WordPress. I loved how simple its file structure was, and being written in Perl was a plus.

**[WordPress](http://wordpress.org/), 2006–13**

: This was the Mack Daddy of blogging software for years, before it lost its focus becoming a general-purpose CMS. Radio UserLand was on the way out by 2006, and I couldn't afford Movable Type. It served me well for many years depite it's shortcomings.

**[Jekyll](https://jekyllrb.com), 2013–15**

: I still think Liquid is the nicest templating system I've used, but alas Jekyll simply couldn't handle thousands of posts well. I recommend it wholeheartedly for other use cases.

