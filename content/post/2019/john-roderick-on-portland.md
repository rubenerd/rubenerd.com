---
title: "John Roderick on Portland"
date: "2019-07-17T21:32:08+10:00"
abstract: "It was moss covered."
thumb: "https://rubenerd.com/files/2019/portland@2x.png"
year: "2019"
category: Media
tag:
- john-roderick
- quotes
- podcasts
location: Sydney
---
**Update 2021:** I’ve taken down fewer than twenty posts out of more than seven thousand in this blog’s history, and posts about this guy are some of them. [MBMBaM’s tweet put it best](https://twitter.com/MBMBaM/status/1345853685902036994) explaining why, though I also have personal reasons. Thanks for understanding.
