---
title: "Fate/Grand Order Valentine Rerun"
date: "2019-03-10T18:00:31+11:00"
abstract: "I got around to posting this, eventually."
thumb: "https://rubenerd.com/files/2019/fgo-valentine-martha-drink@1x.jpg"
year: "2019"
category: Anime
tag:
- fate
- fate-grand-order
- saber
- valentines
location: Sydney
---
Happy Sunday! It's been a month since Valentines, which means at least as many days since Clara and I were playing the *[Fate/Grand Order]* [Valentine Revival] event on the world's [favourite] mobile game. It seems the most [recent Music Monday] wasn't the only horribly delayed post.

I blogged about the event [first time], so I feel it's incumbent upon me to post for the revival/rerun/repeat. *Incumbent* never looks like its spelled correctly, like *cucumber* or *cactus*. Maybe that's just me. We start with the [Saberface] contingent!

<p><img src="https://rubenerd.com/files/2019/fgo-valentine-saberlily@1x.jpg" srcset="https://rubenerd.com/files/2019/fgo-valentine-saberlily@1x.jpg 1x, https://rubenerd.com/files/2019/fgo-valentine-saberlily@2x.jpg 2x" alt="Screeshot of Saber Lily saying: Master, today's Valentine's day! Please accept this!" style="width:500px; height:281px;" /><br /><img src="https://rubenerd.com/files/2019/fgo-valentine-squirtria@1x.jpg" srcset="https://rubenerd.com/files/2019/fgo-valentine-squirtria@1x.jpg 1x, https://rubenerd.com/files/2019/fgo-valentine-squirtria@2x.jpg 2x" alt="Screenshot of Altria saying: Blue Sky Vanilla Mountain!" style="width:500px; height:281px;" /><br /><img src="https://rubenerd.com/files/2019/fgo-valentine-saber@1x.jpg" srcset="https://rubenerd.com/files/2019/fgo-valentine-saber@1x.jpg 1x, https://rubenerd.com/files/2019/fgo-valentine-saber@2x.jpg 2x" alt="Screenshot of Altria saying: I have prepared the highest-quality items that I could, so I'd be happy if you accepted them." style="width:500px; height:281px;" /></p>

Musashi had the best reaction. I was surprised they translated *takoyaki* to *dumpling* in the game, but both are great!

<p><img src="https://rubenerd.com/files/2019/fgo-valentines-musashi@1x.jpg" srcset="https://rubenerd.com/files/2019/fgo-valentines-musashi@1x.jpg 1x, https://rubenerd.com/files/2019/fgo-valentines-musashi@2x.jpg 2x" alt="Screenshot of Musashi saying: Yep. Right. Dumplings. I don't know jack about chocolates." style="width:500px; height:281px;" /></p>

And reactions from those with the most ridiculous accessories, which presumaly would have made creating chocolate all but impossible:

<p><img src="https://rubenerd.com/files/2019/fgo-valentine-ishtar@1x.jpg" srcset="https://rubenerd.com/files/2019/fgo-valentine-ishtar@1x.jpg 1x, https://rubenerd.com/files/2019/fgo-valentine-ishtar@2x.jpg 2x" alt="Screenshot of Ishtar saying: Oh, well. Anyway, take this, Rubenerd." style="width:500px; height:281px;" /><br /><img src="https://rubenerd.com/files/2019/fgo-valentines-tamamo@1x.jpg" srcset="https://rubenerd.com/files/2019/fgo-valentines-tamamo@1x.jpg 1x, https://rubenerd.com/files/2019/fgo-valentines-tamamo@2x.jpg 2x" alt="Screenshot of Tamamo-no-Mae saying: The cold in the dead of winter doesn't bother me! I've got a hot delivery of super-sweet chocolates!" style="width:500px; height:281px;" /></p>

Scátach's comment about her alcoholic beverage offer reminded me just how much alcohol is displayed in the game. And yet, they're always careful to point out that only adults should be engaged in actually drinking it! Compare that to Cartoon Network euphemistically portraying Mordecai and Rigby drinking soft drink, or The Simpsons showing beer as a crutch.

<p><img src="https://rubenerd.com/files/2019/fgo-valentine-scathach@1x.jpg" srcset="https://rubenerd.com/files/2019/fgo-valentine-scathach@1x.jpg 1x, https://rubenerd.com/files/2019/fgo-valentine-scathach@2x.jpg 2x" alt="Screenshot of Scáthach saying: Well, even if you are over the age, I hope you have someone you can enjoy it with." style="width:500px; height:281px;" /></p>

But I think Martha wins this time around :).

<p><img src="https://rubenerd.com/files/2019/fgo-valentine-martha@1x.jpg" srcset="https://rubenerd.com/files/2019/fgo-valentine-martha@1x.jpg 1x, https://rubenerd.com/files/2019/fgo-valentine-martha@2x.jpg 2x" alt="Screenshot of Martha saying: So, here. Warm up. I know things have not turned out properly, but I think I poured it well." style="width:500px; height:281px;" /><br /><img src="https://rubenerd.com/files/2019/fgo-valentine-martha-drink@1x.jpg" srcset="https://rubenerd.com/files/2019/fgo-valentine-martha-drink@1x.jpg 1x, https://rubenerd.com/files/2019/fgo-valentine-martha-drink@2x.jpg 2x" alt="Screenshot showing a hot chocolate! :D" style="width:500px; height:281px;" /></p>

[Fate/Grand Order]: https://fate-go.us/
[first time]: https://rubenerd.com/fate-grand-valentines/
[Valentine Revival]: https://grandorder.wiki/Chocolate_Lady%27s_Commotion_(Rerun)
[favourite]: https://www.animenewsnetwork.com/interest/2019-01-28/fate-grand-order-tops-fortnite-as-twitter-most-talked-about-video-game/.142682
[Saberface]: https://grandorder.gamepress.gg/taxonomy/term/691
[recent Music Monday]: https://rubenerd.com/music-monday-uptown-funk/

