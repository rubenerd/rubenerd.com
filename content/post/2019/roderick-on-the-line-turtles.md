---
title: "Roderick on the Line: Turtles"
date: "2019-03-07T08:33:04+11:00"
abstract: "You never see a turtle frown."
year: "2019"
category: Media
tag:
- john-roderick
- podcasts
location: Sydney
---
**Update 2021:** I’ve taken down fewer than twenty posts out of more than seven thousand in this blog’s history, and posts about this guy are some of them. [MBMBaM’s tweet put it best](https://twitter.com/MBMBaM/status/1345853685902036994) explaining why, though I also have personal reasons. Thanks for understanding.
