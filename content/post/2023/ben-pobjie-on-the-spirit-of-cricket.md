---
title: "Ben Pobjie on the Spirit of Cricket"
date: "2023-07-05T14:56:32+10:00"
abstract: "“But to throw them down while he is out of his crease?”"
year: "2023"
category: Thoughts
tag:
- australia
- england
- news
- sport
location: Sydney
---
For those following the cricket controversy between England and Australia, a phrase I never thought I'd write here:

[The Roar: The spirit of cricket was murdered in cold blood by sunburnt thugs - and the game might never recover](https://www.theroar.com.au/2023/07/04/the-spirit-of-cricket-was-murdered-in-cold-blood-by-sunburnt-thugs-and-the-game-might-never-recover/)

*(In case it's not obvious, the article is satire)!*

> Now, it is one thing to throw the stumps down when the batsman is in his crease. That’s just good wholesome fun and a good way to get the umpires some much-needed exercise. But to throw them down while he is out of his crease? That, frankly, smacks of fascism.

I haven't ever been that into cricket... I mean, it's no badminton or gymnastics final, is it? But it's huge here, and the multi-day tests were always in the background on the TV when we'd visit family and friends.

It does seem odd that specific English fans would get so worked up about someone being bowled out when they're not in their crease, and to the point where they'd call it cheating. It'd be like a badminton player complaining he was standing *mere centimetres* out of the court when he hit the shuttlecock, or a gymnast claiming she came *so close* to sticking her landing.

I'm about to post about some coffee shop chatter in another post, but this is also raising some voices at the next table. Sounds like the English people sitting there think it's all a bit embarrassing on the part of their players. I just think it's *weird!*

