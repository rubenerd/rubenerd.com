---
title: "Peter Cook and Chris Morris in “Why Bother?”"
date: "2023-03-29T10:58:35+11:00"
abstract: "So if I was to say to you, has the answer always been no?"
year: "2023"
category: Media
tag:
- comedy
- radio
- uk
location: Sydney
---
This <a href="https://en.wikipedia.org/wiki/Why_Bother%3F_(radio_show)">mock 1993 BBC interview</a> with Peter Cook's Sir Arthur Streeb-Greebling character is so excellent, I still hear it echoing in my head. I'm sure the audio exists somewhere.

> **Chris**: &nbsp;Will you just grant us the answer to one question?
>
> **Sir Arthur**: &nbsp;Mmm?
>
> **Chris**: &nbsp;With an honest, straightforward answer.
>
> **Sir Arthur**: &nbsp;Well, it really depends on what the question is.
>
> **Chris**: &nbsp;Right. In many interviews…
> 
> **Sir Arthur**: &nbsp;Yes, yes indeed, thank you very much.
> 
> **Chris**: &nbsp;…
>
> **Sir Arthur**: &nbsp;…
>
> **Chris**: &nbsp;…in many interviews, the question that extracts the most valuable answer is the one which is never asked.
>
> **Sir Arthur**: &nbsp;Well you haven't asked it, so I'm really not in a position to answer it.
> 
> **Chris**: &nbsp;Right, well here we go. Obviously I can't ask the question that's never been asked, otherwise, by definition, it stops being…
> 
> **Sir Arthur**: &nbsp;You've asked.
> 
> **Chris**: &nbsp;…yes. But if you keep the question in your head and give us the answer then we can get some…
> 
> **Sir Arthur**: &nbsp;The answer is no.
> 
> **Chris**: &nbsp;…I beg your pardon?
> 
> **Sir Arthur**: &nbsp;The answer is no.
> 
> **Chris**: &nbsp;And what do you mean by that?
> 
> **Sir Arthur**: &nbsp;No is what I mean by that.
> 
> **Chris**: &nbsp;Has the answer for you, ever, been anything other than no?
> 
> **Sir Arthur**: &nbsp;No, the answer has always been no.
> 
> **Chris:** So if I was to say to you, has the answer always been no?
> 
> **Sir Arthur**: &nbsp;…
>
> **Chris:** …
>
> **Sir Arthur**: &nbsp;…I'd say hold on, a minute.

