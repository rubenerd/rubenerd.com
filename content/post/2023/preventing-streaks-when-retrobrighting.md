---
title: "Feedback: preventing streaks when retrobrighting"
date: "2023-02-12T12:45:19+11:00"
abstract: "Constant agitation!"
year: "2023"
category: Hardware
tag:
- commodore
- commodore-64c
- commodore-128
- commodore-vic-20
- commodore-vc-20
- retrobright
- retrocomputing
location: Sydney
---
I've retrobrighted my HP Brio desktop, [Commodore 128](https://rubenerd.com/final-commodore-128-retr0bright-post/), [Commodore 64C](https://rubenerd.com/retrobrighting-a-commodore-64c/), and mentioned I'm in the process of doing it for my newly-arrived [VC-20](https://rubenerd.com/buying-a-commodore-vc-20/). Retrobrighting is the process of removing superficial yellowing from old computer plastics using hair developer and sunlight to bleach the surface. It has worked *shockingly* well for me.

A few of you asked this in email and social media, including [Mark Paterson on Mastodon](https://mastodon.social/@markpaterson/109846891159903272)\:

> Nice! I did this to my A1200 but ended up with some subtle bleach patterning from the creases in the clingfilm. How did you avoid that?

In short, constant agitation!

I'm sure I'm not the first to do this, but my trick is to apply the cream with a brush, then periodically go back outside to lightly massage it under the clingwrap. Applying the cream in a few thick layers gives you more of a buffer for moving it around. I'll also take the opportunity to rotate the plastic to get more even UV coverage, and make sure the clingwrap is still stuck down properly.

The alternative, and probably more foolproof method, is to substitute the cream for clear liquid developer in a large container, in which you submerge your component. Unfortunately, you need bottles of the stuff to do anything of reasonable size, and it's not readily available where I live.

