---
title: "ATX bracket for the Cooler Master NR200P"
date: "2023-08-03T09:41:36+10:00"
abstract: "It comes with limitations, but it lets the larger power supply mount in this case."
thumb: "https://rubenerd.com/files/2023/nr200-atx@1x.jpg"
year: "2023"
category: Hardware
tag:
- cases
- shopping
location: Sydney
---
The Cooler Master NR200 and NR200P are fun mini-ITX cases that support SFX power supplies by default. I've since moved to a Fractal Ridge, but I'm repurposing mine for a friend. I remembered reading it supported ATX power supplies with the removal of a top fan too, but I couldn't figure out how. 

[Cooler Master: ATX Power Supply Bracket](https://www.coolermaster.com/catalog/cases/accessories/atx-psu-bracket-nr200/)

*Turns out*, Cooler Master makes a bracket that mounts in the front of the case that looks, a little something, like this:

<figure><p><img src="https://rubenerd.com/files/2023/nr200-atx@1x.jpg" style="width:500px; height:346px;" alt="Photo of an open NR200P, with the top front fan removed to accomodate an ATX power supply and supporting bracket." srcset="https://rubenerd.com/files/2023/nr200-atx@2x.jpg 2x" /></p></figure>

You can see the angled bracket in the right of the image. Like everything with the NR200P, I liked that they made it a bit more interesting than simply a solid metal cage. It comes in two variants depending on where you want to position it:

* The top orientation, as pictured, requires the removal of one of the 120mm fans, which might be a problem if you use the glass panel and have reduced airflow. Otherwise, this looks like the optimal position.

* The lower orientation retains the fan, which reportedly reduces your GPU space to 240mm. Part of the strength of this case was its support for larger cards, which this would negate.

Either orientation looks like it'll be a tight fit for cables, so you'll want to use a modular supply to keep the number of them down. You'll also be giving up on mounting your GPU vertically, if that was your thing.

This case was clearly designed around SFX, so you'll be compromising by having an ATX supply in here. But if you're on a budget as I am, and can live within these limits, it might prove useful.
