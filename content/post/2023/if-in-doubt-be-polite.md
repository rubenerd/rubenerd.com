---
title: "If in doubt, be polite"
date: "2023-09-26T08:51:18+10:00"
abstract: "If you’re not sure what’s expected, being polite gives you the best chance for success."
year: "2023"
category: Thoughts
tag:
- anxiety
- sociology
location: Sydney
---
It would surprise precisely none of you to discover I have social anxiety! I have all my life.

Being in a customer-facing engineering role for a decade has helped a *lot*, in much the same way any cognitive behavioral therapy does. If you're forced to confront small, regular doses of what you're scared of, you develop a tolerance you'd otherwise lack.

I know that for a lot of people though, especially in IT, social awkwardness rears its head for a *whole host* of other reasons, many of which are outside peoples' control. I wouldn't pretend to understand everyone's personal situation or mental health. Here comes the proverbial posterior prognostication: *but...*, there's one piece of social advice that used to help me a lot.

If you're not sure how to conduct yourself, or what’s expected in a specific social situation, be polite. At the risk of sounding cliché, it's a superpower.

Being polite, in the absence of other social information, gives you the best possible chance for success. It's nice, paints you in the best possible light, keeps you on the high road, and can save you from saying or doing something inappropriate. Politeness also gives you allies in a large social circle if someone is being confrontational or inappropriate.

I've witnessed so many interactions that would have gone better had the unwitting instigator of a bad situation had been polite, whether it's at a tech conference, in a client meeting, or waiting for a train.

