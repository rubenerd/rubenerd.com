---
title: "Tech I couldn’t live without: washing machines"
date: "2023-11-30T12:29:30+11:00"
abstract: "It’s been twelve years since my last one of these. Today we tackle that magical box of cleaning wonder."
year: "2023"
category: Hardware
tag:
- cleaning
- things-couldnt-live-without
location: Sydney
---
It's been twelve years since my last *[Tech I Couldn't Live Without](https://rubenerd.com/tag/tech-couldnt-live-without/ "Tech I Couldn't Live Without tag archive")* post, which frankly is reprehensible. And stop calling me Frank.

Not a day goes by where I'm not immensely thankful for our washing machine. This device lets me throw in whatever clothes, towels, or other fabrics I want into it, and they come out clean an hour later. No scrubbing, no wringing, no scalding. It's a magical box of cleaning wonder, and I love that we can have one.

Professionals with more skill than I could wash our clothes faster, and with more care, than this machine can. The difference is, we can do literally anything else while the machine is whirring away, and it will do a better job than I could (the same goes for [dish washers](https://www.columbiatribune.com/story/news/2021/09/29/dishwashers-hand-washing-science-settles-score/8418843002/ "Columbia Daily Tribune: Dishwashers or hand-washing? Science settles the score")).

I've read books crediting the washing machine as being a principle force in the women's liberation movement too. I can absolutely believe it; I scarcely have the energy to cook dinner at the end of a long work day without thinking about washing things too.
