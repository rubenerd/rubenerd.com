---
title: "Ordering my first nylon 3D printed part"
date: "2023-12-30T10:34:27+11:00"
thumb: "https://rubenerd.com/files/2023/nylon-print@1x.jpg"
year: "2023"
category: Hardware
tag:
- 3d-printing
- apple-ii
- apple-iie
- apple-iie-platinum
- disks
- floppy-emu
location: Sydney
---
*<strong>NOTE:</strong> Posting about 3D printing is an exercise fraught with peril; you'll always be told you used the wrong materials or hardware, that you bought it from the wrong place, that you should have done something else, and/or you'll be lectured by an armchair chemist about formulations they read on Wikipedia. This post shows something that worked amazingly well for me in this specific use case, but it's up to you to do research if you attempt one of these yourself.* 

Recently I ordered a [Floppy Emu](https://www.bigmessowires.com/floppy-emu/) for my [Apple //e Platinum](https://rubenerd.com/remembering-the-apple-iie-platinum-and-finally-finding-one/ "Remembering the Apple //e Platinum, and finally finding one!"); a sentence I'd never thought I'd write! I'll save the details for a separate post, but the Floppy Emu emulates various Apple floppy and hard drives, letting you load software and save data to SD cards. Think of it like an SD2IEC from Commodore land, or the ZuluSCSI on Mac and PC.

Unfortunately, while the frosted case for the Floppy Emu was available when I orderd, [Big Mess o’ Wires](https://www.bigmessowires.com/) didn't have stock of cases for the [Daisy Chainer](https://www.bigmessowires.com/shop/product/daisy-chainer/) that I needed to connect the Floppy Emu and my lone Apple 5.25 drive to my I/O controller. I did a bit of digging, and [found a 3D printable case](https://www.myminifactory.com/object/3d-print-bmow-daisy-chainer-enclosure-113302 "BMOW Daisy Chainer Enclosure") designed by Martin-Gilles Lavoie. It has space for the two DB-19 connectors, as well as a small opening for the optional ribbon cable. 

There was just one small, tiny, inconsequential problem: *I don't have a 3D printer.* While I've long been fascinated by them, I could never justify the cost or space for one. But I remembered that Jan Beta had mentioned PCBWay supported STL files for 3D printing, so I thought I'd give them a shot (not a sponsor here).

My impression of 3D printed parts thus far had been functional, but scrappy. Think visible seams, distinct layers, and rough edges that are ill-defined and need sanding.

<figure><p><img src="https://rubenerd.com/files/2023/nylon-print@1x.jpg" alt="Photo showing the 3d printed enclosure for the Daisy Chainer, alongside the frosted case for the Floppy Emu itself." srcset="https://rubenerd.com/files/2023/nylon-print@2x.jpg 2x" style="width:500px; height:333px;" /></p></figure>

PCBWay defaulted to a beige/white nylon formulation for these files, alongside the regular PLA and ABS. I was frankly shocked by the output. The parts are completely smooth, and even a myopic gentleman like me had to squint to make out the print lines! I've bought commercial devices that haven't looked this nice. It even *feels* premium, something photos can't convey.

I'm not sure whether commercial 3D printing houses have access to higher-resolution machines, or they have more time to print than hobbyists. All I know is this print is *leagues* ahead in fit and finish compared to anything I've received before. It has *completely* changed my perspective on the capabilities and ppolish of 3D printing.

Thanks again to Martin-Gilles Lavoie for this excellent model! A donation will be on its way in the new year.
