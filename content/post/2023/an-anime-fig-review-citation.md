---
title: "An comp-sci anime fig citation?"
date: "2023-08-26T10:21:11+10:00"
abstract: "An academic paper search engine shows an anime fig review of mine was cited as computer science."
thumb: "https://rubenerd.com/files/2016/ayanamirei-aizu.jpg"
year: "2023"
category: Anime
tag:
- anime-figs
- ayanami-rei
- evangelion
- research
location: Sydney
---
[According to the academic paper search engine Oa.mg](https://oa.mg/work/2913484807)

> MAG: 2913484807   
> Aizu Project’s Ayanami Rei   
> Ruben Schade   
> Computer science   
> 2016

Correct me if I'm wrong, but that means someone in 2016 cited the below post of mine, for... computer science? Classifying *Evangelion* in this way might be a bit meta even for me, but maybe there's some profound relationship between the two I'm missing.

Was it about the software required to interface a plugsuit with a human operator? Some other NERV research project?

[Post from 2016: Aizu Project’s Ayanami Rei](https://rubenerd.com/aizu-project-ayanami-rei/)

<p><img src="https://rubenerd.com/files/2016/ayanamirei-aizu.jpg" srcset="https://rubenerd.com/files/2016/ayanamirei-aizu.jpg 1x, https://rubenerd.com/files/2016/ayanamirei-aizu@2x.jpg 2x" alt="" style="width:500px" /></p>

If you wrote a research paper and cited this random post in 2016, do get in touch. Much as I was with the plot of *Evangelion*, I'm absolutely baffled.
