---
title: "The purpose of tech"
date: "2023-10-19T16:41:46+11:00"
abstract: "Andreessen is a product — and an engineer — of a tech bubble that doesn’t understand the people whom it purports to serve."
year: "2023"
category: Ethics
tag:
- internet
- quotes
location: Sydney
---
Amanda Silberling, Dominic-Madori Davis, and Kyle Wiggers [put it so beautifully](https://techcrunch.com/2023/10/17/when-was-the-last-time-marc-andreessen-talked-to-a-poor-person), I leave it here without comment:

> The missing link here is how we can use tech to actually take care of people; how to feed them, clothe them, how to make sure the planet doesn’t reach such high temperatures that we all just melt away. [...] What is missing here is that the technological revolution made it easier to hail an Uber or order food delivery, but did nothing about how those drivers and delivery people are being exploited, and how some live in their cars to sustain a decent wage.
>
> Andreessen is a product — and an engineer — of a tech bubble that doesn’t understand the people whom it purports to serve.

Also, [Dan Primack](https://www.axios.com/2023/10/17/marc-andreessens-ai-manifesto-hurts-his-own-cause "Marc Andreessen's AI manifesto hurts his own cause"):

> Right now, we're watching what happens when risk management and tech ethics are ignored for the sake of unlimited growth.
