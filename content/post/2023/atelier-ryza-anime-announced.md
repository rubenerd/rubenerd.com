---
title: "Atelier Ryza anime announced"
date: "2023-03-21T09:01:44+11:00"
abstract: "Fingers crossed the adaptation is good."
thumb: "https://rubenerd.com/files/2023/ryza-anime@1x.jpg"
year: "2023"
category: Anime
tag:
- atelier-ryza
- lidenfilms
location: Sydney
---
[@dressupgeekout](https://bsd.network/@dressupgeekout/110057836588889161) shared [this news](https://lemmy.ml/post/898195) about an Atelier Ryza anime, which has been officially announced for midway through the year. It has an [official site](https://ar-anime.com/), and a [cute new trailer](https://www.youtube.com/watch?v=Ly83UdDhv18) showcasing Ryza and her friends.

<figure><p><img src="https://rubenerd.com/files/2023/ryza-anime@1x.jpg" alt="Key visual from the anime PV announcement" style="width:500px;" srcset="https://rubenerd.com/files/2023/ryza-anime@2x.jpg" /></p></figure>

I'm stoked! Despite all the usual comments about *Thighza* in regular anime forums, the Atelier games are some of the most wholesome I've ever played, and Ryza is such a great character. I'll be interested to see how they turn a chill crafting game into a fully-fledged anime series.

The writer and music composer from the games will be behind this production as well, which gives me confidence that the original material will be respected. I also wasn't that familiar with the production company LIDENFILMS, but they have quite the [back catalogue](https://en.wikipedia.org/wiki/Liden_Films).

If they pull this off, I'd love to see a Sophie series too :). Also maybe [more figs](https://rubenerd.com/reisalin-stout-fig-from-atelier-ryza-3/) *cough*.
