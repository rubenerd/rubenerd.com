---
title: "An annual retrospective by numbers"
date: "2023-12-26T11:04:21+11:00"
year: "2023"
category: Thoughts
tag:
-
location: Sydney
---
2023 was my most prolific writing year here since 2011, and certainly a high over the mid-2010s when I hit a mental lull. This is opposed to a mental *loll* [sic], which is how a client used to respond to my messages.

*(I never thought it was my place to correct him, but I'd also want someone to let *me* know if I was writing that! Language is hard).*

As I used to say, *why settle on quality when you can have quantity?*

Wait, damn it.
