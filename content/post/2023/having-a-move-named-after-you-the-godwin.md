---
title: "Having a move named after you: The Godwin!"
date: "2023-11-27T11:01:06+11:00"
abstract: "Georgia Godwin has made history with a move named after her."
thumb: "https://rubenerd.com/files/2023/yt-dM5JOFqzwZk@1x.jpg"
year: "2023"
category: Thoughts
tag:
- australia
- gymnastics
- sport
location: Sydney
---
Via [Gymastics New South Wales](https://www.gymnsw.org.au/content/history-has-been-made-with-the-godwin "History has been made with 'The Godwin'!")\:

> Georgia Godwin has made history! A new skill has been named after the women's artistic gymnast and introduced into the Code of Points by the International Gymnastics Federation (FIG).
>
> The Godwin is performed on Uneven Bars and is defined by a clear hip circle forward to handstand with 1/1 turn (360°) in handstand phase (Weiler kip). The skill has been allocated an 'E' difficulty.
>
> [...] the process for an element to be named is very specific. The gymnast first submits a video of herself doing the element to the FIG Women’s Technical Committee for evaluation [...] the gymnast must perform the element without a major fault at an international competition [...] to be officially named.

That's *wildly* cool! But also, imagine the pressure being judged against your eponymous move. I'd almost feel like it'd work against you; or maybe that's just my anxious side talking.

<figure><p><a target="_BLANK" href="https://www.youtube.com/watch?v=dM5JOFqzwZk" title="Play THE GODWIN - 2023 FIG World Challenge Cup Tel Aviv (ISR) – Osijek (CRO) - WAG new UB element"><img src="https://rubenerd.com/files/2023/yt-dM5JOFqzwZk@1x.jpg" srcset="https://rubenerd.com/files/2023/yt-dM5JOFqzwZk@1x.jpg 1x, https://rubenerd.com/files/2023/yt-dM5JOFqzwZk@2x.jpg 2x" alt="Play THE GODWIN - 2023 FIG World Challenge Cup Tel Aviv (ISR) – Osijek (CRO) - WAG new UB element" style="width:500px;height:281px;" /></a></p></figure>

[Women's Agenda](https://womensagenda.com.au/life/sport/australian-gymnast-georgia-godwin-has-officially-got-an-original-move-named-in-her-honour/) linked to her [original Instagram](https://www.instagram.com/georgia_godwin/) account where she has more details, and the FIG has a video showing her perform the move.
