---
title: "Is it safe to read tech news again?"
date: "2023-11-23T17:32:00+11:00"
abstract: "I guess it was the tech world’s Royal Wedding, or Grand Final."
year: "2023"
category: Media
tag:
- ai
- news
location: Sydney
---
The tech world was all a dither over one specific company and its beleaguered CEO this week, with all the suspense and intrigue of a damp sponge. Even non-technical news outlets were keen to report on what amounted to a game of musical chairs, regurgitating the same opinions and speculation.

It's virtually impossible to avoid news like that. Even if you spam your mute filters with keywords and phrases to avoid, it still has a habit of seeping back into everything, like rising damp. That's two moist metaphors in as many paragraphs!

I guess it was the tech world's Royal Wedding, or Grand Final. It's not supposed to be meaningful or introspective, it's just a... *thing*. I just wish we could hold more than one of them concurrently.
