---
title: "Childhood car (in)dependence"
date: "2023-07-14T14:32:55+10:00"
abstract: "Car dependency and urban sprawl are awful for childhood development."
year: "2023"
category: Thoughts
tag:
- car-dependency
- safety
- urbanism
location: Sydney
---
[Not Just Bikes: Why we won't raise our kids in suburbia](https://www.youtube.com/watch?v=oHlpmxLTxpw)

> One thing you notice when you go to The Netherlands: there are a *lot* of children around ... the Dutch are famous for having the happiest kids in the world ... the big factor is [their] level of independence

I had a similar experience growing up in Singapore, though that was more for the ubiquitous, affordable, reliable, easy to understand, and safe public transport. I was allowed outside by myself when I was 9, and that was considered *late* by my friends.

When we moved to Kuala Lumpur years later, or back to Australia before I settled in a mixed-use neighbourhood, I had *less* independence. Go figure.

> I've talked to some parents from suburban Canada about why they don't let their children out on their own, and they usually say it's because it's "too dangerous" ... That's insane, because Canada is one of the safest countries in the world ... so why do people think this?

He cites some interesting theories, including Jane Jacobs’ idea of the *eyes on the street* effect, something you don't get in desolate, car-dependent suburbia. He also points out that North American cities are dangerous for pedestrians with their wide *stroads*, ever-increasing vehicle sizes, and driver distractions.

[Centre for the Living City profile on Jane Jacobs](https://centerforthelivingcity.org/janejacobs#info)

Australia doesn't suffer as much from these, though car companies and governments are doing their best to catch up. I've now had a day where I've seen *two* of those imported American Ram and F-150 trucks and, to the surprise of nobody, the obliviousness of their drivers bordered on criminal. Granted it's a small sample size, but I've learned to give these penile compensators a wide berth as a pedestrian.

> Whenever I have discussions with people from my suburban hellscape of a hometown, they usually agree the city is quiet and soul-crushingly boring, but they'll inevitably say "its a great place to raise children". I **completely disagree**. Suburbia may be a good place to shelter toddlers, but as soon as a child is more than about six years old, being trapped in a McMansion on the edge of town seriously inhibits their growth ...

In the words of Twitter before it imploded, I wish I could heart this twice.

> In the US and Canada you have the stereotype of the suburban "Soccer Mum": the mother who spends all her time shuttling her kids around to school, activities, play dates, and back. Because until a kid is 16 years old and has their own drivers’ licence, they [need to be].
>
> Is it any wonder that kids aren't getting enough exercise? I don't blame them for wanting to stay inside, when outside looks like... [photo of a massive suburban highway].

This is a fascinating point. My late mum made a big deal about having us walk to school, come rain or shine, because she wanted to instil good practices in us from a young age. It's probably among the most important things my parents ever did for me, though it's much harder if you have to live in a suburban dump.

Cars don't only impose negative externalities like pollution and dangerous crossings, they also make their drivers and passengers unhealthier by forcing them to sit in a chair. I do that enough at work and home.

I wonder how many suburban gyms with massive car parks would go out of business if people could cycle everywhere, or take public transport? Or how many more kids would be encouraged to take up more physical activity if it were safe and easy to do so?
