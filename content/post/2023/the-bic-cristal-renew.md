---
title: "Finding a Bic Cristal Re’New in Japan"
date: "2023-11-03T15:00:09+11:00"
abstract: "My new favourite pen!"
thumb: "https://rubenerd.com/files/2023/bic-japan@1x.jpg"
year: "2023"
category: Travel
tag:
- japan
- otsu
- pens
- writing
location: Otsu
---
*This draft post was originally written in Japan, but the images I uploaded broke. I've re-uploaded*.

I waxed lyrical [about the Bic Orange Finepoint](https://rubenerd.com/the-bic-orange-finepoint-most-certainly-is/) a decade ago, and I stand by them being my favourite ever pen. They're cheap as chips, light, feel nice to hold, are *wonderfully* smooth on any paper I've tried, dry instantly, and draw precise lines without being too thin. 

I'm sure there are plenty of other budget ballpoints out there that hit all the same notes, but why switch when you've found a winning formula? I've been whittling down the same budget box of Finepoints for years; they last forever, and are practically indistructable.

<figure><p><img src="https://rubenerd.com/files/2014/mai-pen@1x.jpg" alt="Photo of Mai with a box of Bic pens" style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2014/mai-pen@2x.jpg 2x" /></p></figure>

I do love the idea of a refillable pen, but I'll admit I prefer these cheap pens compared to anything else I've ever used. I've been gifted more expensive ballpoints and fountain pens before (my mum was a calligrapher), but none have supplanted these dependable French things for daily use. That perhaps says more about me than I care to admit.

Anyway, when Clara and I were in Japan earlier this year, we ended up in a suburban shopping centre on the outskirts of the beautiful little city of Ōtsu. Well, technically we were both in pants, because it was cold.

And what did the stationary store have? 

<figure><p><img src="https://rubenerd.com/files/2023/bic-japan@1x.jpg" alt="Photo of a Bic Cristal Renew box in a store in Japan." style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2023/bic-japan@2x.jpg 2x" /></p></figure>

Hanging between some efficient Japanese stationary was a box of *Bic Cristal Re'New pens!* The sleek metal housing is designed to resemble my beloved Finepoint and Cristals, but they have a small slider to eject its ink cartridge. The box comes with two refills, meaning this one box functionally has three pens.

The irony wasn't lost on me that we went to an amazing Japanese stationary store so I could buy a French designed pen&hellip; *omelette du fromage*. But was this the answer to my silly problem?

**はい! OUI! YES! Every language!**

The body of the pen feels more substantial and premium than a Finepoint, despite still being pretty cheap. Being a Bic though, it's still comfortable to hold, and easy and pocket. It doesn't leak, and it barely takes up space in my bag. It's a Bic, but metal! I feel like Milhouse with his Alf... in Pog form. And I have refills for when it runs out, which solves the only concern I had with the Finepoints.

Now I have to clarify what my favourite pen is going forward. Or upside down, or sideways, it works in any orientation. Alf! Wait, pog, damn it! 🖊️
