---
title: "My home IPv6 network plans"
date: "2023-07-13T17:11:28+10:00"
abstract: "Thinking through an alternative to NAT66, and a network diagram showing our future topology."
year: "2023"
category: Internet
tag:
- ipv6
- networking
- token-ring
location: Sydney
---
Thanks to everyone who replied to my previous IPv6 post, I have a much better idea of how I want to overhaul Clara's and my home network, and hopefully learn how to use IPv6 properly in the process.


### Sorting out addressing

Our ISP began offering IPv6 earlier this year. Rather than a single public IPv4 address, we've now been delegated a **/48**, from which our router can DHCPv6 addresses to every Internet-connected interface in our local domain.

My mistake was thinking this meant my entire IP address scheme would be subject to change whenever we moved house or changed ISP. While that's true if you only use a single global address on each device, IPv6 interfaces can be assigned multiple addresses.

For example:

* Each device would receive a **global address** from the router from that **/48**. This facilitates point-to-point connections without going through a NAT, though it still transits the firewall. Neat.

* Each device can also be configured with a **link-local address** for communicating with others on the local domain, such as to access our home file server. Devices will preference this where possible, so traffic doesn't need to hit a router.

These link-local addresses can be assigned by:

* **Using SLAAC**, in which the device broadcasts its intent to use a random, self-assigned address. The IPv6 space is sufficiently massive that it's unlikely to collide, but otherwise it'll generate another. This would be most useful for Wi-Fi devices like phones, laptops, and Commodore 64 StrikeLink modems (cough).

* Assigning a **unique local address** (ULA) on the IPv6 **fc00::/7** range, such as on home servers. This also means I can get nice, short, memorable addresses for common things, or ones with punny hexadecimal English words.


### Next steps

This is a quick and dirty diagram showing our plans. The router isn't set in stone yet, and I'm not sure what AP I'll end up replacing our crappy current one with:

<figure><p><img src="https://rubenerd.com/files/2023/tropology-ipv6.png" alt="Network diagram showing our idea for our new network topology." style="width:460px; height:500px;" /></p></figure>

Now I just need to figure out how best to connect my IPv4 retrocomputers to the network so they can still access the FTP jail on my FreeBSD box (though maybe that should be on its own box). I'm also long overdue to replace my home FreeBSD jail configuration with proper VNET interfaces instead of a simple loopback, which I'll have to also test with IPv6.
 
There's a part of me that thinks it'd be fun deprecating IPv4 entirely, and leaning on my Token Ring network for these old PCs. Does Ringdale still make those Token Ring to Ethernet routers? Do they talk IPv6? I could call it *Cursednet!*

