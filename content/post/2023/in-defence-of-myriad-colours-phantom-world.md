---
title: "In defence of Myriad Colours Phantom World"
date: "2023-01-14T09:55:24+11:00"
abstract: "Reviewers claim it’s the worst series Kyoani ever did. I don’t think that’s fair."
thumb: ""
year: "2023"
category: Anime
tag:
- art
- colour
- kyoani
- myriad-colours-phantom-world
- reviewers
location: Sydney
---
*(An earlier draft version of this post with broken Unicode placeholder text and no images was uploaded for some reason, and I don't know why. Let's try that again!)*

*Myriad Colours Phantom World* gets little love in anime retrospectives. The 2016 series is either ignored in posts talking about our beloved Kyoto Animation, or is held up alongside *Amagi Brilliant Park* as the worst the studio ever released. I mention this, because an amime blog I've followed for years only recently wrote a post exploring why they thought it was so dreadful; a bit of a surprise for a six-year old series that's largely been forgotten.

<figure><p><img src="https://rubenerd.com/files/2023/mcpw-huh@1x.jpg" alt="Screenshot" srcset="https://rubenerd.com/files/2023/mcpw-huh@2x.jpg 2x" style="width:500px; height:281px;" /></p></figure>

It may shock you from reading the title, but I think the characterisation is unfair. While I wouldn't put it in my list of favourites, or even rate it among the best the studio ever released, it was still a beautiful and fun series in its own right, with a level of self-awareness some of Kyoani's more serious shows sometimes lack.

The anime is based on the three-volume fantasy light novel by Sōichirō Hatano. It hasn't ever been (officially) translated into English, but I've read that the plot does deviate in typical Kyoani style from the original source material. For example, Ruru was introduced as a character to aid in narrating events and for comedy relief, but she wasn't in the light novel.

<figure><p><img src="https://rubenerd.com/files/2023/mcpw-hair@1x.jpg" alt="Screenshot" srcset="https://rubenerd.com/files/2023/mcpw-hair@2x.jpg 2x" style="width:500px; height:281px;" /></p></figure>

The plot revolves around a world beset by phantoms, and the release of a virus that permits humans to perceive them for the first time. Certain young people born after this event, including our illustrious cast, are bestowed with additional abilities to fight and seal phantoms; a useful trait given the danger some of them present. Much as we'd have anime clubs to watch the antics of fictional high school characters endure ever-escalating escapades, in this world clubs have been established for those who can deal with these phantoms.

This global situation has become normalised by the time this specific story starts, though battles involving these phantoms still draw crowds, as we see from the first episode. Kawakami Mai is an athletic, roundhouse-kicking badass who lowers the HP of these phantoms sufficiently for other protagonist Ichijo Haruhiko to seal them with his rapid drawing skills. His philosophical discussions are also our introduction to most episodes, usually in the service of delivering a bad joke or pun which I can appreciate.

<figure><p><img src="https://rubenerd.com/files/2023/mcpw-pfffft@1x.jpg" alt="Screenshot" style="width:500px; height:281px;" srcset="https://rubenerd.com/files/2023/mcpw-pfffft@2x.jpg 2x" /></p></figure>

Other lead characters include the ever-hungry Izumi Reina, who serves as the *moe moe* reserved character who Ruben from high school would have crushed over, and Kumamakura Kurumi who's cute bear takes on a surprising role later in the series. The aloof Minase Koito (above) was Clara's and my favourite, who can subdue a phantom with her vocals, cool attitude, and gap moe (cough).

Each episode does tend to follow the formula of *this week's monster*, but I thought there was enough variation in circumstances and characters to keep it interesting until the end. I'm in the minority when it comes to generally finding action-heavy *shounen* titles tedious, so this series got the balance right for me. Even reviewers hesitant to recommend the series commented on how Kyoani consistently animated the action scenes well, even if they thought they were too short.

<figure><p><img src="https://rubenerd.com/files/2023/mcpw-whatthe@1x.jpg" alt="Screenshot" style="width:500px; height:281px;" srcset="https://rubenerd.com/files/2023/mcpw-whatthe@2x.jpg 2x" /></p></figure>

The art has been a point of contention among certain Kyoani fans, though perhaps they were watching it on a 240p monochrome display. While it didn't have the gorgeous light and water reflections of *Tsurune* or the enduring *Free!* franchise, I thought they got the art direction from the light novels spot on. It was bold and colourful, with an interesting mix of textures and simplified patterns that I thought were well executed, especially during the character's special moves. I applaud them for taking it on and owning the aesthetic, even if it wasn't to everyone's tastes.

*(I felt echos of this years later when Mika Pikazo designed the lead characters for the latest *Fire Emblem* game, but that's for another post).*

<figure><p><img src="https://rubenerd.com/files/2023/mcpw-colour@1x.jpg" alt="Screenshot" srcset="https://rubenerd.com/files/2023/mcpw-colour@2x.jpg 2x" style="width:500px; height:281px;" /></p></figure>

If I had to somewhat agree on one aspect critics of the series raised, it was the underdeveloped plot. An invaded, dystopian world has become a common trope, but humans evolving a sense of what was already there (albeit via a contagion) is an intriguing idea. The series had to focus on the dangerous phantoms for those action scenes, but I'd be even more interested in the mundane or harmless ones that hide your house keys. Would someone born with only a mild ability be able to seal these phantoms, and call up Mai to roundhouse kick the hard ones? 

[Herald Journalism reported](https://heraldjournalism.com/myriad-colors-phantom-world-season-2-release-date-cast-plot/) that season two was in the works prior to Covid, which would have used the second and third volumes of the light novel to expand more on this world. But I suspect there aren't that many of us saddened by the news. Which is a shame, because I thought this series had real potential.

I forgot what my anime rating scale was, so for now consider it 3 stars. Out of what? Your guess is as good as mine.

<figure><p><img src="https://rubenerd.com/files/2023/mcpw-landscape@1x.jpg" alt="Screenshot" style="width:500px; height:281px;" srcset="https://rubenerd.com/files/2023/mcpw-landscape@2x.jpg 2x" /></p></figure>
