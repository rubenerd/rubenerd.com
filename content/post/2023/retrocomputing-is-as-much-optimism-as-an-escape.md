---
title: "Retrocomputing is as much optimism as an escape"
date: "2023-03-13T14:37:55+11:00"
abstract: "A quote from Studio 8501 on Mastodon about the reaction to the current state of IT."
year: "2023"
category: Hardware
tag:
- privacy
- retrocomputing
location: Sydney
---
More than a few of you have noted an uptick in the number of retrocomputing posts that have appeared here in the last few years, whether it be for 8-bit Commodore machines, or DOS that I grew up with. It wasn't a conscious decision on my part, but looking back at the archives it's *very* obvious!

I've long been interested in computer history and nostalgia, but having a bit more disposable income now to buy these machines from the past and my childhood has helped. I've also half-joked that it's a way to still enjoy computers while offering a break from my day job. I write thousands of words a day, so it's nice to talk about other things, while still scratching that electronic itch.

[Studio 8501 on Mastodon](https://oldbytes.space/@mos_8502/110011415193067579) expands on this with something I hadn't considered, but is obvious in retrospect:

> I believe that #retrocomputing is about more than nostalgia ... it's also about a reaction to the truly sorry state of modern computing. Old computers didn't spy on you, they didn't beg you for micropayments, they just did what they were told. They were tools for work, for learning, for entertainment ... we are, as a community, more than smart enough to design and build our own new retro-inspired designs, and build for ourselves a computing world worth living in.

Admittedly, rose-tinted glasses help here. The computer industry was as ruthless and cut throat back then, with dubious business practices and antitrust adventures. But the output from the customer side felt less invasive, gamed, and hostile. The modern web without active filtering plugins is *horrible*, and I say that as someone who lived through dialup and first-gen popups. And modern smartphones? Don't get me started.

I also appreciate their optimism. I didn't connect the dots before, but retrocomputing fans are natural allies to the *right to repair* and homebrew tech communities. Keeping these systems alive, and expanding upon them with modern enhancements, hints to an alternative future which is more inclusive, empowering, and fun.
