---
title: "Pavolia Reine discusses her 3D debut"
date: "2023-07-17T15:54:27+10:00"
abstract: "If we're not nervous, we're not ready!"
thumb: "https://rubenerd.com/files/2023/reine-3d-showcase-talk@1x.jpg"
year: "2023"
category: Anime
tag:
- hololive
- pavolia-reine
location: Sydney
---
Reine of Hololive Indonesia had a follow-up stream for her 3D debut last night, which I just got around to watching!

<figure><p><img src="https://rubenerd.com/files/2023/reine-3d-showcase-talk@1x.jpg" alt="Screenshot from her Supachat reading." style="width:500px; height:281px;" srcset="https://rubenerd.com/files/2023/reine-3d-showcase-talk@2x.jpg 2x" /></p></figure>

[Pavolia Reine's 3D debut!](https://rubenerd.com/pavoliareine3d-debut/)   
[3D Showcase Aftertalk!!! Q: Were you nervous?](https://www.youtube.com/watch?v=id8rQYCYPJs&t=11466s)

Aside from derailing her into talking about pancakes for a few minutes (cough), I asked if she'd been nervous, and if so she'd hid it well.

> I think during the preparation... of course there's going to be nervousness. But I felt, a mix of nervousness; not like the thing that makes you freeze, more like "you're so excited!"
>
> But the nervousness really kicked in when there was nothing else to do. Even on the day, there were so many, many things to prepare. I was so focused doing things right. But half an hour before when I didn't have anything to do, right before it began, THAT'S when it hit me. Whoa, this is starting!
>
> But it's such a big thing, how could I not be nervous? It's not really a negative feeling. You're so excited! If we're not nervous, we're not ready!

I do want to emphasise that streamers like Reine on YouTube and Twitch attract tens of thousands of live viewers, and hundreds of thousands after the fact. There are TV stations in the Before Times that wouldn't have attracted such viewership. And they're there with all the equipment, managing chat, and coordinating the stream in real time, with minimal or no support. It's ridiculous!

I get where she's coming from about nervousness too. I can prep for weeks for a talk, and the nervousness only hits when I'm waiting in the wings with the cue cards and the remote control for the slides, and I can hear the murmurs of hundreds of people in the audience.

Preparation is a distraction, but when you're waiting... that's when the excitement and nervousness creeps in. Have I done everything? What will people think? *What have I missed!?* If anyone knows how to silence that critic, do get in touch.

