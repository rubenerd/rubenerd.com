---
title: "December Time is when projects start"
date: "2023-11-24T08:37:49+11:00"
abstract: "Most of my projects start on the last day of the year."
thumb: "https://rubenerd.com/files/uploads/media.tree-glitter.gif"
year: "2023"
category: Software
tag:
- december-time
- holidays
location: Sydney
---
I was on a video call earlier this week where the client had decked out the room behind him in holiday decorations. I mentioned they [looked great](https://rubenerd.com/you-look-nice-today/ "Post: You look nice today!"), and he said he did it to remind him of his childhood.

I could relate! Our mum died in late December which muted my sister's and my love of the season, but recently it's come back to being a time where we remember positive things. We weren't religious, but our parents were both *really* into the holiday season. Decorated trees, lots of lights, all the German deserts, the Sunday Roast.

<figure><img src="https://rubenerd.com/files/uploads/media.tree-glitter.gif" alt="A delightful Xmas tree" style="width:100px" /></figure>

It's also the Lunar New Year equivilent in the West where everything shuts down. Work continues, but it's the time of year where I get extended periods of time to *do things*, without interruptions or distractions. Even this blog you're reading now was [started in late December 2004](https://rubenerd.com/the-first-post/ "The first post").

Taking a look at my repository folder where I stash my Subversion and git projects and writing, these were the months in which their first files were committed:
 
* December: 61
* Other months: 8

I... didn't realise it was even as stark as that.

This year I'm thinking of spending *December Time* on rearchitecting most of how the backend of this blog works, both at the server level and the software itself that runs it. Clara also wants to start writing again, and I've got a few ideas. [Antranig Vartanian](http://antranigv.am/ "Freedon Be With All") already knows what this is going to be!

