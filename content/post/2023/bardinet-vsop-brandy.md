---
title: "Bardinet VSOP brandy"
date: "2023-12-06T08:21:51+11:00"
abstract: "It’s lovely!"
thumb: "https://rubenerd.com/files/2023/48147-1@1x.jpg"
year: "2023"
category: Thoughts
tag:
- alcohol
- drinks
- france
- reviews
location: Sydney
---
A friend recommended this as an introductory brandy for someone who loves less-peaty Scotch and Japanese whisky. I'd never had brandy (or cognac) before, besides drizzling it into Xmas puddings.

It's lovely! It's fruity and light, but has that subtly matured taste of a younger whiskey.

<figure><p><img src="https://rubenerd.com/files/2023/48147-1@1x.jpg" alt="A bottle of Bardinet VSOP brandy" srcset="https://rubenerd.com/files/2023/48147-1@2x.jpg 2x" /></p></figure>

Abstaining from most sugar over the last couple of years has made me uncomfortably sensitive to most sweets. I miss having a tiny bit of Cointreau, Port, or even some red wine with the family, but they're *way* too sweet for me now. This brandy gives me that slight fruitiness I miss, but in something much lighter and dryer.

If you're new to brandy like me, VSOP refers to its maturation. VS is a minimum of three years, VSOP is five, and XO is seven. The ones that reach into four figures have been matured for decades.

This bottle will likely last me for many months, given how I prefer to sip small amounts at a time. But feel free to send me any recommendations or suggestions for what I should try next.
