---
title: "Review of Titanic II"
date: "2023-11-04T16:38:20+11:00"
abstract: "“A mixed bag” since “it’s better than one might expect, but not as good as one might hope”."
year: "2023"
category: Thoughts
tag:
- quotes
- pointless
location: Sydney
---
<a href="https://en.wikipedia.org/wiki/Titanic_II_(film)#Reception" alt="Wikipedia article on the Titalic II movie">According to Wikipedia</a>:

> On the website TheCriticalCritics.com the film was reviewed as being "a mixed bag" since "it’s better than one might expect, but not as good as one might hope."

Wait, was that a review of my blog, or that bad movie!?
