---
title: "“That’s not how that works!”"
date: "2023-11-18T09:58:28+11:00"
abstract: "People struggle to reconcile the idea of something sounding technically simple, but impossible."
year: "2023"
category: Software
tag:
- development
- psychology
- systems
location: Sydney
---
Riffing off my random post yesterday about [getting computers to do what you want](https://rubenerd.com/computers-doing-what-you-want-not-what-you-say/ "Software doing what you want, not what you say"), not what you say, I'm reminded of the phrase in the title. Anyone who's written and deployed a system of any kind is likely familiar with this retort. It's a way to insulate our systems&mdash;and by extension us&mdash;from stakeholders making technically-unreasonable demands.

*(I qualified "unreasonable", because there are plenty of non-technical demands one can make of a system too. The sound of all of you nodding your heads in weary agreement can be heard from here)!*

I've found myself reaching for *that's not how that works* line in response to requests in a bunch of scenarios:

* When something is technically impossible. I dub these Malcolm Turnbull requests, after our former prime minister who claimed the laws of Australia trump the laws of mathematics. No, really! Asking for a "secure backdoor", for example.

* When something would yield unintended consequences. Yes, dropping replication might improve performance under certain specific workloads, but the loss of redundancy <span style="text-decoration:line-through">might</span> will be catastrophic.

* When something sounds technically valid, but doesn't account for human behaviour. Think of YouTube blocking ad-blockers, and the subsequent rise in installation and technical sophistication of ad-blockers. Aka, the Streisand Effect.

* When something would violate the law, contracts, fiduciary responsibility, or ethics. People are surprisingly *blasé* about feeding personally-identifiable information into machine-learning models, for example.

* When someone confuses a feature's functionality. The classic example was emptying the Trash or Recycle Bin "deleting" their data. When someone has an idea like that, it can be difficult to discuss adding a similar feature.

I keep thinking of more! But I think you get the idea.

The challenge with such requests isn't that they're merely impossible (how inconvenient!), but that they're often coming from a place of misunderstanding. This means that to grok why something can't or shouldn't be done, preconceived ideas and prior learning need to be unravelled. But people making such demands are generally not as receptive to this; they just want the damned feature added, and the boffins are standing in the way.

It's not even limited to features. I was talking with my sister recently about her role in her company's complaints department. She mentioned one obstinant client who insisted he maintain two separate accounts in their system, despite it running afoul (🐔) of money laundering laws. His request was simple, and impossible to implement. People *really* struggle to reconcile these ideas.

I suppose these sorts of misunderstandings are inevitable. Our world is ridiculously complicated and complex, not least computer systems. Nerds like me don't do a great job of articulating our concerns in a way the public can (and should) understand. But I also wish sometimes that people could meet in the middle. We're not being arseholes for denying someone's request to leak their data, no matter how good of an idea they think it is!
