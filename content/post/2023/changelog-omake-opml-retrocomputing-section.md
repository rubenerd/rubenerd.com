---
title: "Changelog for omake.opml"
date: "2023-01-03T10:25:59+11:00"
abstract: "A few new sevice manual links, and rearranging things to make it all easier to find."
year: "2023"
category: Hardware
tag:
- retrocomputing
- weblog
location: Sydney
---
The Retrocomputers section in my [Omake outline](https://rubenerd.com/omake.opml) has a few new things and changes:

* My list of old computers are now under that root heading instead of gear, to make them easier to find.

* Links to CD-ROMs on the Internet Archive are under their own heading, because the list was becoming gigantic.

* There's a new Books and References section with links to service manuals and other material as PDFs on the Internet Archive. I'll be uploading any BIOS references etc that aren't there.

* Rearranged some of the XML namespaces. I have a metadata link to my blogroll hosted on FeedLand now. Probably redundant, but I'm of the opinion that more metadata is (nearly) always better.

On the `#TODO` list still:

* Find, upload, and/or link to older BSD releases that I used. I still have the original disk images I took of the drives before destroying them. Maybe I need to call [Call for Testing](https://callfortesting.org/) for hashes :).

* Needs more classic BeOS.

* There are still *tons* of missing ISOs.

* Add an expand a VC-20 [sic] subsection when I finish building my Das Franken-20 from parts!
