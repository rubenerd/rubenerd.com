---
title: "You look nice today!"
date: "2023-11-22T07:24:50+11:00"
abstract: "It’s a great ice breaker for the socially anxious."
year: "2023"
category: Thoughts
tag:
- anxiety
- psychology
location: Sydney
---
You know that line in the *Sunscreen Song* that encourages you to do one thing every day that scares you? I've been doing that with social anxiety, and I've been shocked at how well it's worked.

Over the last month I've been pointing out when someone I interact with changes something about their appearance, or if they stand out in a unique way. If a barista has a new haircut, or a delivery person has a bright purple shirt, or if a doctor has new glasses, I call it out for looking great. It's even worked for some clients.

This likely sounds like the ramblings of a weird person to someone who's otherwise adjusted and normal! They might even be right. But it's been amazing as an ice breaker, not only to start a conversation, but to get me over that initial cliff of social anxiety. Turns out, it's just another mental barrier you can push down. (It also feels good to be nice to people, shocking though it may seem).

You look pretty great yourself, reading this right now.
