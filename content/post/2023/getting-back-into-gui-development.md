---
title: "Getting back into GUI development"
date: "2023-08-20T22:37:05+10:00"
abstract: "Kinda sorta announcing a new project I’m working on."
year: "2023"
category: Software
tag:
- personal
location: Sydney
---
I haven't written a graphical desktop program for many years. Most of mine were done in Visual Basic 6 back in the day, but Borland Delphi and Java Swing were probably the last ones. But I've decided to give a new one a try.

I've got a specific idea of what I want in mind, and I think it'd be fun! It'll also be incredibly niche and silly, so likely only useful to a handful of people on the planet with similar obsessions and interests.

I've already written a basic data model and set of triggers in SQLite, so now comes the tricky thing of evaluating what graphical toolkit and language to use. The three canidates are Perl with Prima, PyQt, and Lazarus. I need to learn Python for work, so I figure that would be a *two birds with one stone* thing. Lazarus on the other hand offers a rapid prototyping environment for Object Pascal, which would be pretty great.

As I said on Mastodon earlier this month, that's all I'll be saying about it for now, but stay tuned :).

