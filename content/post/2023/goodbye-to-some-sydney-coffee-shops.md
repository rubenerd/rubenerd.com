---
title: "Goodbye to some Sydney coffee shops"
date: "2023-09-24T09:57:50+10:00"
abstract: "We’ve lost a few recently, probably due to soaring rent costs."
thumb: "https://rubenerd.com/files/2023/locomotion-cafe@1x.jpg"
year: "2023"
category: Travel
tag:
- business
- coffee
- coffee-shops
- goodbye
- sydney
location: Sydney
---
We've lost a few recently in my suburb, probably due to the soaring cost of rent and other pressures. It's a shame.

<figure><p><img src="https://rubenerd.com/files/2023/locomotion-cafe@1x.jpg" alt="View of the Locomotion Cafe boarded up" style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2023/locomotion-cafe@2x.jpg 2x" /></p></figure>

In memorium, we have:

* **Q5**. This was the best coffee shop in the Westfield, and did great salads too. During my once-a-week venture outside during COVID lockdowns for groceries, I'd swing by and buy beans from them and have a quick chat. They were lovely, but alas weren't in the best of locations for foot traffic.

* **Locomotion Cafe**. This one surprised me, because they were always so packed. Their location opposite station meant I'd often swing by for a takeaway on my way to catch the train, or sit there and prepare for my next client meeting.

* **Pattisons**. If you grew up as an Australian kid, it kinda felt like a school tuckshop with its faire. Some days when working remote I'd go there just for a spinach roll.

Chatswood is a mixed-use walkable neighbourhood in Sydney, so there are plenty of other coffee shops and similar places to choose from. But given how much of my life I spend in these places, it does feel weird suddenly not being able to go to these ones. When you spend more of your time there than at home sometimes, its like you've lost a friend and a room at the same time.

