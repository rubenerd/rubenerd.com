---
title: "Reconciling down to the cent"
date: "2023-08-02T08:20:21+10:00"
abstract: "I used to! But I’ve finally met my match."
year: "2023"
category: Thoughts
tag:
- budgeting
- finacnes
location: Sydney
---
Clara and I use a spreadsheet we hacked on to implement an envelope budgeting system. Since I started budgeting like this in 2015, I've reconciled everything to the cent. No exceptions.

Until today! One of my accounts was off by two cents, and I spent more than an hour trying to track it down. The weird thing is, the account reconciled perfectly last week, so I'm not quite sure what's going on. I drew 2c from the *barrier* envelope, and moved on. It feels weird.

*(Any Australian fans of the Finders Keepers book series here)?*

