---
title: "Dave Winer: an armada of Twitters"
date: "2023-05-07T10:43:26+10:00"
abstract: "“Like the podcastosphere with all the different players. Look at how much better that's working”"
year: "2023"
category: Internet
tag:
- podcasting
- rss
- twitter
location: Sydney
---
[Scripting News on Saturday](http://my.feedland.org/?item=3311177)

> What we need is not one twitter to replace the old twitter but a whole armada of twitters. Like the podcastosphere with all the different players. Look at how much better that's working. No Musk or Jack. Think about it.

[See also: the solution to centralisation](https://rubenerd.com/the-solution-to-centralisation/)
