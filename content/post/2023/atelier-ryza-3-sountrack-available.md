---
title: "Atelier Ryza 3 sountrack and theme available"
date: "2023-03-27T09:21:13+11:00"
abstract: "Bought for my birthday, and they’re beautiful."
thumb: "https://rubenerd.com/files/2023/atelier-ryza-soundtrack@1x.jpg"
year: "2023"
category: Anime
tag:
- atelier
- atelier-ryza
- music
- music-monday
location: Sydney
---
It's *[Music Monday](https://rubenerd.com/tag/music-monday)* time! And because it's my birthday, I thought I'd take the opportunity to share something I bought myself that has made me happy.

Hot on the heels of the [*Atelier Ryza* anime](https://rubenerd.com/atelier-ryza-anime-announced/) announcement, the game's developer Gust has released the [complete soundtrack](https://nex-tone.link/A00114279) for the third installment of the game. The theme song *[Travelers](https://lnk.to/Travelers)* is also now available for streaming and purchase. The covers are *so pretty!*

<figure><p><img src="https://rubenerd.com/files/2023/atelier-ryza-soundtrack@1x.jpg" alt="Covers from the soundtrack and theme song releases." srcset="https://rubenerd.com/files/2023/atelier-ryza-soundtrack@2x.jpg 2x" style="width:500px; height:350px;"/></p></figure>

Even if you don't play the games, the Atelier music is so comfy. Clara and I bought a three CD box set from *Atelier Sophie* at the Koei Tecmo store in Tokyo last year, and we often have it running in the background to relax.

