---
title: "An Intel i387SL coprocessor for my AMD Am386SX"
date: "2023-04-05T14:12:59+10:00"
abstract: "Researching a part I’d never had before, and installing in my 386 board."
thumb: "https://rubenerd.com/files/2023/i387-close@1x.jpg"
year: "2023"
category: Hardware
tag:
- am386
- retrocomputing
location: Sydney
---
So far in my unintentional [Am386 motherboard](https://rubenerd.com/tag/am386/) series we've [got it booting with graphics](https://rubenerd.com/my-386sx-16-20cn-rev-0-motherboard/), added a [multi-IO card](https://rubenerd.com/my-386sxs-working-acer-mio-400-io-card/), and even got [an IDE CD-ROM working](https://rubenerd.com/the-ess-audiodrive-es1868-for-sound-and-ide/) off a sound card which I'd never done before.

Today's post also breaks more new ground for me, because we only ever had a i486SX in our childhood PC. Behold, my "new" Intel N80387SL!

<figure><p><img src="https://rubenerd.com/files/2023/i387-close@1x.jpg" alt="Photo showing the i387SL chip" style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2023/i387-close@2x.jpg" /></figure>

Coprocessors were optional silicon that performed floating point arithmetic in hardware at much faster speeds than the host CPU could in software. They were especially popular with CAD and spreadsheet users, who could justify the additional cost. By the time 586-class CPUs like the Pentium came out, their functions were integrated into CPUs.

Finding a coprocessor for your specific CPU and motherboard could be tricky:

* Not all motherboards came with coprocessor sockets. This old Am386 board did, but for the longest time I didn't know what it was!

* The i386SX was a 32-bit CPU like the i386DX and the i486 family, but it only had a 16-bit external databus. This meant coprocessors, sockets, and motherboards weren't interchangeable.

* Later 386's also came in an SL variant for embedded and mobile use, which used less power. There were coprocessors targeting these CPUs as well.

The Am386SX CPU on this board is branded as a "SX/SXL", which I assumed meant it was compatible with standard and low power silicon. I thought an SL coprocessor was a bit more unusual and interesting, so I snapped one up for $25 on eBay, like a gentleman. It also tickled me having an Intel coprocessor alongside an AMD CPU on the same board.

<figure><p><img src="https://rubenerd.com/files/2023/i387-in-board@1x.jpg" alt="Photo showing the i387SL in its socket, with the am386SX above it" style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2023/i387-in-board@2x.jpg" /></figure>

I've been building and tinkering with computers since I was a kid in the 1990s, but the coprocessor came in a [PLCC chip carrier](https://en.wikipedia.org/wiki/Chip_carrier#Plastic-leaded_chip_carrier) package I've never used. The pins extend out from the sides of the chip and loop back underneath in a J shape, which make contact with the edges of the socket.

My main worry was making sure I socketed it in the right orientation; despite the chamfer the chip can technically fit in any orientation. Fortunately, the motherboard has a silk-screened print showing pin 1, and the chip has a divot indicating where the first pin is.

Once the chip was installed, I booted the machine and was greeted with the BIOS reporting a numeric processor present for the first time!

<figure><p><img src="https://rubenerd.com/files/2023/i387-bios@1x.jpg" alt="Photo showing the BIOS reporting a numeric processor being present." style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2023/i387-bios@2x.jpg" /></figure>

I haven't encountered any difference in operation yet, but I'm keen to run some benchmarks and tests for fun. As I said at the start, I've never had one of these chips before. It's cool that I could "upgrade" this old 1991 board with such an inexpensive (thesedays) chip.
