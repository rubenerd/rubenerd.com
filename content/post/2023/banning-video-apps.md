---
title: "Banning video apps"
date: "2023-01-02T10:21:37+11:00"
abstract: "There’s a reason people outside the Five Eyes think we’re hypocrites."
year: "2023"
category: Internet
tag:
- privacy
location: Sydney
---
There's furore among American talking heads, and more broadly within Five Eyes countries and allies like Australia, over whether TikTok should be banned, based on it being foreign spyware full of tracking. It won't be the last.

This reaction is generally met with bemusement elsewhere. It might be a novel concept in the States, but the rest of us are used to foreign IT companies tracking us. For example, my card transactions go through American payment processors, I use American social networks with published ties to American intelligence, and my primary phone chat app is Japanese.

I suppose the big difference, and why this isn't classic *whataboutism*, is whether you think said country:

* is your friend or ally;

* is acting in good faith;

* and has sufficient transparency, accountability, and due process.

I happen to (mostly) think so, but we've been trending backwards too with awful legislation like the Patriot Act and the #AABill.

Democracies need to approach this issue by taking an **uncompromising** stance on digital privacy and human rights. Accepting anything less, and we invite the comparison; whether you think it's fair or not.

