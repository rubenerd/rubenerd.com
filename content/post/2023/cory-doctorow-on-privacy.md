---
title: "Cory Doctorow on privacy"
date: "2023-08-04T08:34:32+10:00"
abstract: "They're disguising a demand as an observation."
year: "2023"
category: Internet
tag:
- privacy
- quotes
location: Sydney
---
[Posted this morning](https://twitter.com/doctorow/status/1686795269835485184)

> They're disguising a demand as an observation.
