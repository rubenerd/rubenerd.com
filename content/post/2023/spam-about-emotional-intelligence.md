---
title: "Spam about emotional intelligence"
date: "2023-06-06T08:22:33+10:00"
abstract: "You’d think wanting to be a good human would suffice!"
year: "2023"
category: Thoughts
tag:
- email
- emotional-intelligence
- spam
- work
location: Sydney
---
I got an email this morning describing a new management course. It was a bit eye opening:

> Emotional Intelligence (EI) is now widely recognised as the fundamental quality of effective leadership and management.
> 
> Delegates explore topics that fall under the two main areas: social awareness and social facility. Within these two areas a range of EI competencies are explored such as empathy, attention, rapport building, understanding others, influencing, acting and more.
> 
> The course provides a series of exercises that helps delegates to practice communication skills by focusing on specific EI competencies.

You'd *think* wanting to be a good human would be sufficient, but I'm also not surprised courses like this need to exist. The problem is, are the people who need the training receptive to it?

I thought the ethics courses I did at university were excellent, but most of us were already primed for thinking about the impact of what we do. A vocal minority dismissed them as being a waste of time, which for them was likely accurate. I know one of those people now has a senior management position.
