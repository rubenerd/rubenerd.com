---
title: "Obsolete tech: the Battery Daddy"
date: "2023-01-24T14:44:51+11:00"
abstract: "Finding a device that stores those cylindrical batteries we all used to use."
thumb: "https://rubenerd.com/files/2023/battery-daddy@1x.jpg"
year: "2023"
category: Hardware
tag:
- electronics
- pointless
location: Sydney
---
Australia Post stores are as much a trip down memory lane as they are places to dispatch parcels. The long queues meander through free-standing shelf units selling all manner of trinkets from another time, often proudly boasting *AS SEEN ON TV* with big red stickers.

As an aside, was that *ever* a badge of honour? Even as a kid growing up in the 1990s, my impression of stuff peddled on informertials and breakfast TV wasn't positive. It all seemed like junk sold with more enthusiasm than quality, and designed for those susceptible to making impulse purchases.

> But hurry! Order now and you'll receive this latex spatula, absolutely FREE! But wait, there's more! Buy three widgets, and that latex spatula becomes... THREE latex spatulas! Batteries not included, not for sale in Victoria or Antarctica.

Speaking of batteries though, that segues into this bizarre device I saw among the piles of stuff last year, and again just then as recommended by a large online retailer. It's a Battery Daddy, and it has a few batteries:

<figure><p><img src="https://rubenerd.com/files/2023/battery-daddy@1x.jpg" alt="A small, transparent briefcase with holders to stash dozens of batteries of different types and sizes" style="width:500px; height:342px;" srcset="https://rubenerd.com/files/2023/battery-daddy@2x.jpg 2x" /></p></figure>

I'm impressed they were able to tessellate that many different sized batteries into a space like that. If it were safe for infants to be left with batteries, it'd be quite the puzzle to reassemble them when the lid opens and they all fall out. I only just noticed while writing this paragraph that there are even a couple stashed within the handle. And check it out, it even has a battery tester!

I have so many thoughts about this. First, it screams Binford Tools from Tool Time... and if you got that reference, you must be my age or older! Secondly, when was the last time you replaced a cylindrical battery out of anything, save for the odd remote control? Thirdly, is a word that starts with T. And lastly, are people still buying disposable ones in lieu of rechargables?

I suppose I'm not a *Battery Daddy*, I'm a *Cell... Kid?* Wait, that doesn't sound good.
