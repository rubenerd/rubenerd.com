---
title: "Overheard coffee shop chatter, late August 2023"
date: "2023-08-31T08:40:42+10:00"
abstract: "I was fine when I got on, but not on the other end. A bit like Shakespeare."
year: "2023"
category: Thoughts
tag:
- coffee-shops
location: Sydney
---
These posts really write themselves.

* "I'll see you in the torture chamber."

* "If I'm starting with nothing, then I have nothing to start with, y'know what I mean?"

* "Everyone's leaving! Why is everyone leaving? [...] Is it my hair?"

* "I'll bring it up. No mate, I'll bring it up. No no, really, I'll bring it up. Oh, ohhhhhhhhh [silence]. Okay, you bring it up."

* "I was fine when I got on, but not on the other end. A bit like Shakespeare."

[Past coffee shop posts](https://rubenerd.com/tag/coffee-shops/)
