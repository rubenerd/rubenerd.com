---
title: "A blank text editor"
date: "2023-03-21T15:34:32+11:00"
abstract: "I wonder what it could have had?"
year: "2023"
category: Software
tag:
- text
- text-editors
location: Sydney
---
Before I started writing this, the buffer in this text editor was empty.

I wonder what it could have held?
