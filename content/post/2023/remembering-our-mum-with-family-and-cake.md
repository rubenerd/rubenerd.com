---
title: "Remembering our mum with family and cake"
date: "2023-12-24T09:43:45+11:00"
thumb: "https://rubenerd.com/files/2023/remembering@1x.jpg"
year: "2023"
category: Thoughts
tag:
- debra-schade
- family
- goodbye
location: Sydney
---
I had the joy of meeting with Clara, my sister Elke, and my brother-in-law Jesse yesterday. We used to get cheesecake to celebrate anniversaries, so it always seemed fitting to remember her passing with one as well.

This time of year is always rough. It was the lowest point in both my sister's and my lives, but now it's also a time to celebrate what we have. Clara and Jesse are wonderful people, and I'm sure Debra would have approved.

This year was rougher, likely due to us losing our dad on account of his partner not liking us. I wonder if he thought about her yesterday too? We didn't hear from either of them.

<figure><p><img src="https://rubenerd.com/files/2023/remembering@1x.jpg" alt="Photo of the Christmas tree inside Starbucks where we got a cheesecake." srcset="https://rubenerd.com/files/2023/remembering@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>
