---
title: "CityNerd on city values"
date: "2023-06-27T13:55:22+10:00"
abstract: "How a signalised intersection is designed and operates is really a statement of a city’s values."
year: "2023"
category: Thoughts
tag:
- cars
- citynerd
- health
- urban-design
- urbanism
- videos
- youtube
location: Sydney
---
At the risk of turning this blog into a stenography service for Not Just Bikes, CityNerd, and other urbanist channels, I thought this observation was poignant.

[Stroad vs. Stroad: Land Use, Traffic Engineering, and What Happens When Suburban Arterials Intersect](https://www.youtube.com/watch?v=3v537SEfDag)

> There's nothing about the design of these [massive] streets, or the intersections where they converge, that really convinces me that human health and safety are a high priority.
> 
> How a signalised intersection is designed and operates is really a statement of a city's values. Or maybe even the values of wider US suburban culture.
>
> If you have inhumanly large intersections with high-speeds and three minute cycle lengths [to wait as a pedestrian], it's a clear indication your cities values personal motorised vehicle throughput above everything else.

Could apply to much of Australia too.
