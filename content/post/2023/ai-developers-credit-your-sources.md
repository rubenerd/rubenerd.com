---
title: "AI developers: be nice, and credit your artists"
date: "2023-03-20T09:10:33+11:00"
abstract: "If you’re using their work, it’s literally the least you could do."
year: "2023"
category: Media
tag:
- ai
- art
- ethics
- ai-data-sources
location: Sydney
---
This summary from [an anime dataset](https://gist.github.com/harubaru/f727cedacae336d1f7877c4bbe2196e1) so beautifully captures the issues facing AI generated art:

> I also want to personally thank [the image board], as without their hardwork the generative quality from this model would not have been feasible without going to financially extreme lengths to acquiring the data to use for training.
>
> The Booru in question would also like to remain anonymous due to the current climate regarding AI generated imagery.

The developers thank the image board, but not the artists for their "hardwork". Might that be contributing to the "current climate regarding AI generated imagery"?

As long as developers treat art as merely butter to be churned, they can't act surprised at the resentment it breeds! Well, they *can*, but they might look as silly as someone sporting six fingers. From their feet.

