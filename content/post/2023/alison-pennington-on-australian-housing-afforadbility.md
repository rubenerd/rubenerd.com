---
title: "Alison Pennington on Australian housing afforadbility"
date: "2023-03-04T11:08:57+11:00"
abstract: "It’s a feeling shared in places around the world, though Australia’s local laws have only made this worse."
year: "2023"
category: Thoughts
tag:
- economics
- ethics
location: Sydney
---
[Alison's article in The Conversation last Friday](https://theconversation.com/friday-essay-how-policies-favouring-rich-older-people-make-young-australians-generation-f-d-199403) paints a bleak picture about the current state of housing in Australia's cities. It's an issue keenly felt in other cities around the world (Singaporeans and Vancouverites among you know of what I speak!), but Australian government policy has entrenched and expanded what was already a systemic problem.

This is the key takeaway:

> While Australia encourages the wealthiest households to build housing assets, it forces young and low-income households into an increasingly pressured private rental market. Over 60% of people aged under 30 are renting.
>
> Australian landlords demand a return on their investment as a matter of entitlement. As a class, they pocket billions in housing tax concessions, contribute to rising prices, and then demand their poorer tenants keep up with rents.

This is the rub. Other investment classes, like securities, aren't presumed to have guaranteed returns. And when interest rates rise, renters foot the bill. People I've talked with on social media assert neither are true; that they were told housing was a "sure thing", and renters are merely whinging for spending most of their disposable income on paying off someone else's unproductive asset. There's clearly a broader issue of financial literacy and empathy at play here... as is always the case with economics.

It's worth mentioning that Clara and I are looking to buy, and that we've been lucky to have either great or decent landlords. But we've also had the finances, time, and legal advice to negotiate and gently push back against things when we've needed to. Many people, especially ones struggling to make ends meet, don't have this luxury.

Both sides of the isle may lack the courage to save money and [give free housing to those in need](https://www.weforum.org/agenda/2018/02/how-finland-solved-homelessness), and have pushed for housing as an investment, not a necessity. In which case, it's critical this power imbalance is addressed by giving renters more security and rights. The alternative is to shove the vulnerable between Gyprock and a hard place.
