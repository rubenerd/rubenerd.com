---
title: "Greg Lehey on interoperability"
date: "2023-04-10T19:42:52+10:00"
abstract: "Why does the application layer get to break the rules?"
year: "2023"
category: Internet
tag:
- accessibility
- interoperability
- design
- networking
location: Sydney
---
[Greg mentioned interop in his diary yesterday](http://www.lemis.com/grog/diary-apr2023.php?subtitle=xterm%20for%20Android?&article=D-20230410-030259#D-20230410-030259)

> One of the most horrible things about “modern” computer infrastructure is the lack of interoperability. How do I interface SMS with email with Facebook? How do I share files between Android and FreeBSD? All these things that should be easy have been made difficult for no good reason. 

It does amaze me that we've developed all these amazing protocols, but reach software sitting atop the application layer of the OSI model, and all bets are off. Why is that layer special? Why does *it* get to break the rules?

Or put another way, can you imagine if a large site owner decided to implement Token Ring somehow, a mobile OS ran on IPX/SPX, and it was only plebeians like us on TCP/IP? It was great we coalesced on the one protocol suite, but it was by no means inevitable.

[IBM's fascinating docs on token-ring networks](https://www.ibm.com/docs/en/i/7.1?topic=standards-token-ring-networks)   
[Wikipedia article on the OSI application layer](https://en.wikipedia.org/wiki/Application_layer)   
[Wikipedia article on IPX](https://en.wikipedia.org/wiki/Internetwork_Packet_Exchange)

