---
title: "Hales on PCIe over USB (or lack thereof)"
date: "2023-06-05T08:24:11+10:00"
abstract: "It only uses the USB cable for transmitting signals."
year: "2023"
category: Hardware
tag:
- feedback
- pcie
location: Sydney
---
Last Friday I talked about the potential to repurpose Wi-Fi card slots on motherboards for other uses. This would be silly on a regular motherboard, but on a slot-constrained environment like Mini-ITX it offers a rare potential for internal expansion.

[Using M2 Wi-Fi card slots for other things](https://rubenerd.com/using-m2-wifi-cards-for-other-things/)

One of the connectors I found passed PCIe over USB, or I so I thought. Hales emailed with a correction:

> Surprisingly these actually don't use any USB signalling or do anything 
USB related.  They use USB 3 cables because they are a convenient and 
cheap set of wires for sending PCIE signals over a short distance (they 
have some matched pair wires in them that are good for high speed 
digital signals).  It's PCIE all the way down, USB is merely an illusion.

Huh! It reminds me of those HDMI-over-Ethernet devices.

Would I be showing my age to admit that I've used INTERLNK/INTERSRV and EGA on the same cables too? If so, please disregard.
