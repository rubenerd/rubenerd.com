---
title: "Transit to office campusus"
date: "2023-11-09T17:08:18+11:00"
abstract: "Asking whether we need them, and if they contribute to the health and well-being of staff and communities."
year: "2023"
category: Thoughts
tag:
- design
- public-transport
- trains
- urbanism
location: Sydney
---
Way back when, friends and I used to fantasise about where we'd like to work. The usual suspects kept coming up, but one thing that struck me was just how isolated their buildings were. We'd look them up, and they'd always be in some far flung "campus" somewhere; surrounded on all sides by parking, and a motorway off to the side. But look, *trees!*

It wasn't until I thought of the term more literally that I realised what the point was. "Campus" conjures images of universities, education, and research. And from those, legitimacy. It was the 20th-century equivalent of having a masonry building for your bank. You'd *made it* if your business had a campus, instead of an office.

Campuses? *Campii?* That sounds more like a drink. 乾杯!

But I thought that was a bit weird, and as time went on, undesirable. The idea of having to commute by car to a far flung campus somewhere, or take hours on public transport, didn't sound fun. You'd better pay me those SF salaries, and have great amenities at your campus, if I'm forced out there every day!

[This April 2017 report](https://www.spur.org/sites/default/files/2017-04/SPUR_Rethinking_the_Corporate-Campus_print.pdf "Rethinking the Corporate Campus") by SPUR had some great lines about the San Francisco Bay Area:

> The suburban corporate campus remains the predominant real estate solution for the region’s employers. With isolated single-use buildings set behind vast parking lots, far away from the public street, it is a model that reinforces dependence on cars and pushes sprawl development into open spaces and farmland.
>
> This environment emerged in an era of wide-open spaces, cheap land and easy mobility by car — an era that is long past. Today that same environment, built for near-term expedience, is expensive, congested and ubiquitous. 

Their recommendations included:

* Locating offices alongside transit and amenities, rather than behind walls and endless rows of parking spaces.

* Retrofitting low, large, single-use office buildings into mixed neighbourhoods.

* Up-zoning around transit, to permit more people to live close to where they need to shop and work, or to let them commute easier.

This is all now accepted wisdom among the next-generation of urban planners concerned with sustainability, health, fiscal responsibility, and generally making things better for people over things.

It's also become more important to discuss after Covid first hit. Cities worldwide are suffering from housing shortages, and these campuses stick out like sore thumbs. And are massive, single-use offices even viable in an age where you best and brightest won't stand for working in an office full-time? Or sit in them?

Skyscrapers may bring their own issues, but at least you can achieve density in a CBD with nearby services and transport, and even apartments for those of us who prefer them. Campii blanket areas with sprawl; land that could be lower-density residential for those who prefer it, or get this: farms and nature! 

I'd ask those building an office in 2023:

* Do you really need it?

* No, really?

* Will it contribute to the well-being of your staff and the surrounding area, or is it another gilded cage for commuters?

Silicon Valley might have among the most acute examples, but these wasteful, inaccessible, and therefore ugly buildings are everywhere.

I was going to end this by saying *everywhere, like TV Mobile!* Any Singaporeans who grew up in the late 1990s remember that? Talking of things we can all agree we're better off without!
