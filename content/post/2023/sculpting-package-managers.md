---
title: "Sculpting package managers"
date: "2023-10-10T07:24:26+11:00"
abstract: "We all have our own slabs of marble to chizel away, so to speak."
year: "2023"
category: Software
tag:
- bsd
- freebsd
- package-managers
location: Sydney
---
You know that [famous quip](https://quoteinvestigator.com/2014/06/22/chip-away/ "Quote Investigator: You Just Chip Away Everything That Doesn’t Look Like David") about sculptors removing everything from a marble slab that doesn't look like an elephant? The origins and meaning may be murky, but it's still a humerous way to summarise how effortlessly professionals perform their craft, while the rest of us stare in awe.

A few minutes later after joking about this with a client, we were onto what they'd need to run a WordPress site. I mentioned they'd ideally use FreeBSD, php-fpm, MariaDB, nginx, Certbot, and Varnish on the frontend for caching. They laughed and said I was chipping marble away from packages.

Sysadmin questions like that *pale* in comparison to sculpting, but I'll take the compliment! I guess we all have our own slabs of marble.
