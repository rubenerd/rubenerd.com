---
title: "Terramaster NAS’s eye-opening CVE"
date: "2023-02-11T19:56:37+11:00"
abstract: "“... and then reading the PWD field in the response”"
year: "2023"
category: Software
tag:
- security
location: Sydney
---
As published [last Tuesday](https://www.cve.org/CVERecord?id=CVE-2022-24990), via [@da_667 on Mastodon](https://infosec.exchange/@da_667/109842276646965255/favourites)\:

> TerraMaster NAS 4.2.29 and earlier allows remote attackers to discover the administrative password by sending "User-Agent: TNAS" to module/api.php?mobile/webNasIPS and then reading the PWD field in the response.

This is *hauntingly* similar to how I got into a photocopier/print server in my high school library in the mid-2000s. I couldn't believe the password was delivered to the endpoint for local comparison. It also delightfully dropped every non-alphanumeric character, and translated every letter into lowercase before evaluation, just to remove some more entropy.

*(I disclosed the issues, because I'm a square)!*

As an aside, expect to see more fundamental mistakes like this when artificial "intelligence" tools write more code. We're told it should only be used as a guide, or be reviewed by a human first. We know it won't be.
