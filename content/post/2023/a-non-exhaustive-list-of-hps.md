---
title: "A non-exhaustive list of HPs"
date: "2023-09-27T13:19:29+10:00"
abstract: "I didn’t know it was Houses of Parliament sauce."
year: "2023"
category: Thoughts
tag:
- food
- lists
- pointless
- hp
location: Sydney
---
Off the top of my head:

* Hit points
* Hewlett-Packard
* Houses of Parliament sauce

And some initialisms that aren't HP:

* ZFS
* BSD
* VLB
