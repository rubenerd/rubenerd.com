---
title: "Internet Archive downloads failing"
date: "2023-10-04T09:08:27+11:00"
abstract: "Try using a tool like aria2c, or resuming the download in Firefox to finish."
year: "2023"
category: Internet
tag:
- internet-archives
- troubleshooting
location: Sydney
---
Speaking of the Internet Archive, I've noticed many downloads from the site failing to finish. The item will *almost* finish, then Firefox will report the download failed. You have to be careful, because you might not realise it failed until much later when you try to use it.

I've got into the habit of copying the URLs from Internet Archive pages, and using the aria2c download tool to retrieve it. This is much more robust, and can handle retries:

	$ aria2c $URL

Otherwise, if you see the file shown as "Failed" in Firefox, click the retry arrow link next to it. It should finish the last few megs of the download.
