---
title: "Singular or plural follow-up"
date: "2023-12-06T08:08:20+11:00"
abstract: "British English uses plural verbs, mostly."
year: "2023"
category: Thoughts
tag:
- english
- language
- spelling
location: Sydney
---
Yesterday I posted about the difference between how British and other English speakers [use plurals for certain singular nouns](https://rubenerd.com/singular-or-plural-verbs-for-singular-nouns/ "Singular or plural verbs for singular nouns"). I'm used to reading a group **are** doing something, but most say the group **is** doing something.

I started doubting that what I remembered about British English was true. Not to get all Malcom Gladwell on you, but *turns out* it's even less clear cut than I thought!

Here's an [Ask the Editor article on Britannica](https://www.britannica.com/dictionary/eb/qa/Are-Collective-Nouns-Singular-or-Plural-verb-agreement "Are Collective Nouns Singular or Plural?")\:

> In American English collective nouns are more often singular, and so a singular verb is used with them. (In British English they are more often plural, and so a plural verb is used with them.)

"More often" is certainly less definitive than I thought the rule was.

The Modern Language Association of America [has a style guide](https://style.mla.org/verbs-with-collective-nouns/ "Should I use a singular or plural verb with a collective noun?") which partially agrees with my third point above:

> Collective nouns, like team, family, class, group, and host, take a singular verb when the entity acts together and a plural verb when the individuals composing the entity act individually.

Clear as mud! I guess use whichever one works better, and don't make the mistake I made of thinking too much about it.
