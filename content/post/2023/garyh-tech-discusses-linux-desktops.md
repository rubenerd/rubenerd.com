---
title: "GaryH Tech discusses Linux desktops"
date: "2023-07-19T11:55:58+10:00"
abstract: "The introduction of Wayland has coincided with less accessible customisation. How something looks is a feature"
year: "2023"
category: Software
tag:
- bsd
- design
- desktop-environments
- ergonomics
- freebsd
- linux
- netbsd
- ui
location: Sydney
---
I want to start this post by saying how sorry I am to hear about the struggles Gary and his family have been having of late. I send nothing but love and support. It sounds trite, but I genuinely mean when I say that Gary is *good civ*. I hope things pick up soon, because good people deserve good things. ♡

Catching up on some of his videos (which you should all watch and subscribed to!), I remembered he recently discussed the state of the Linux desktop. He mentioned an angle I hadn't considered before.

[GaryH Tech: What's going on with Linux](https://www.youtube.com/watch?v=aepW9o9p7eE)

> Does a nice looking GUI really matter? To me, not really. But it will to a lot of people. Because the real problem that happens here, if people aren't going to use it as much because its not as pretty looking...
>
> [Granted], people aren't that shallow, people will use it because it works for them. But if it doesn't work for them, and they're not attracted to using it, they're going to stop developing on it. And this is all open-source software. So we start losing some of the developers... we got a problem.
> 
> I don't care about the theming stuff. Yes its nice to have a desktop that looks pretty, but generally when I've used Linux or FreeBSD, I don't even have a GUI.
> 
> There are some distros out there using Wayland instead of X11. Great, if they've got it working for them. But it's not finished [and] the developers who are creating themes for these new systems... they're not there.
>
> [I haven't done a Linux review for a while because] they're all very samey.

I've been criticised for this before, but I do fundamentally think the appearance of a desktop is an important functional requirement. It was one of the contributing factors to me leaving Windows as my primary OS years ago, and what continues to frustrate me about modern macOS at work.

Form is just as important as function for a few reasons:

* Ergonomics.

* We use these machines day in and day out. It's not irrational to want that to be pleasant, whether it be a pretty desktop or simply consistent UTF-8 fonts in your terminal for CJK and Latin.

* If your aim is to convert people from other platforms, people will gravitate to systems that are nicer to use. Or put another way, they'll be put off something that isn't.

The challenge is that everyone has a different idea of what pleasant, fun, or useful is. But this gets to GaryH Tech's point about theming. There was a time when Linux was fun and interesting because of its visual customisations. If you didn't like something, you could change it.

That's still true, to an extent. I can open KDE on my openSUSE or FreeBSD box and choose a few new themes if I want. I've talked before about the "ricing" community who twist and shape Linux and BSD into all manner of interesting directions. But it's not as easy, ubiquitous, or even good as what we had before, and my impression is that it's more involved than what it used to be.

He's also right that there's is a degree of "sameyness" that has creeped into these desktops. The majority of distros have settled on Gnome as the default, or a Gnome derivative. Dare I say, it's part of a wider trend towards treating computers as more of a web-connected appliance instead of a "personal computer" where you're in control of everything.

I run FreeBSD and NetBSD on servers, desktops, and laptops for the same reason Gary does: they're almost always the right tool for the job, and they give me a blank canvas. But that doesn't make me miss what the broader ecosystem had before. I've been on record here and in talks that Linux doesn't have to fail for FreeBSD to succeed, and that in many circumstances FreeBSD *benefits* from the mindshare and development Linux has, especially on the desktop. But that also means we've subject to their same whims.

Semi-related, remember that brief period in the late 1990s when computers came in colours other than beige and black? Even now, modern computer cases for enthusiasts are generally Ford Model T specials (albeit with RGB to compensate). For want of a better word, I miss the whimsy.
