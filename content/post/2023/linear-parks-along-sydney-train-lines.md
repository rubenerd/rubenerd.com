---
title: "Linear parks along Sydney train lines"
date: "2023-11-25T12:27:22+11:00"
abstract: "We only just discovered one around Artarmon."
thumb: "https://rubenerd.com/files/2023/sydney-linear-park-trains@1x.jpg"
year: "2023"
category: Travel
tag:
- australia
- hiking
- nature
- trees
- sydney
location: Sydney
---
Sydney's sprawling suburban train system has a bunch of hidden gems, one of which Clara and I only just discovered last weekend:

<figure><p><img src="https://rubenerd.com/files/2023/sydney-linear-park-trains@1x.jpg" alt="View down the footpath of a linear park walk, with fences on either side." srcset="https://rubenerd.com/files/2023/sydney-linear-park-trains@2x.jpg 2x" /></p></figure>

This is one of the linear parks snaking its way up from Artarmon station. On the left you have the fences between you and the live tracks, and on the right are the backs of houses. The tree canopy is gorgeous, like you're walking through a quiet forest tunnel.

I sometimes walk home from work, which takes a few hours. I'm trying to find more parts of the route like this so I can spend time among trees, not walking alongside roads.
