---
title: "Urban sprawl guarenteed the success of online retail"
date: "2023-04-25T08:54:49+09:00"
abstract: "When you have a large, spread out, and isolated population of people, online retail makes sense."
year: "2023"
category: Internet
tag:
- health
- shopping
- urban-design
- walkability
location: Osaka
---
*(This was originally written last week, but I didn't get around to posting. I've set the publish date to today)*.

A recent TV ad in Japan for the world's largest online retailer was supposed to highlight its impressive automated warehouses and logistics, but instead it served to connect the dots for me on something I've been thinking about for years.

I've mentioned before that Sydney bills itself as a *city of villages*, and it shows when it comes to the differing approaches to urban design and planning. Development was unlocked around certain train stations, which is where you'll find most of the apartment buildings and younger people. Clara and I chose to live in one of these areas, because the lifestyle is great. We walk to parks, green grocers, supermarkets, coffee shops, libraries, outdoor restaurants, Asian night markets, retail stores, bubble tea, and trains that take us across the Harbour Bridge into town. We don't own a car, and we feel no loss of freedom whatsoever.

Unfortunately, much of Sydney is the same suburban "stroad" wasteland like you see in post-WWII New Zealand or North America. Weirdly enough, my first experience with this lifestyle was in Malaysia; my parents wanted a house again after living in high-rise Singapore, so we moved into a residential development on the periphery (to put it charitably!) of Kuala Lumpur. It was a beautiful, isolated prison accessible only by car, and I promised myself to never live in one again!

[Strong Towns article on *Stroads*](https://www.strongtowns.org/journal/2018/3/1/whats-a-stroad-and-why-does-it-matter)

Which leads me to this epiphany about online retail. Living in a remote house distorts your view of the world in so many ways, but this might be one of the more economically consequential.

Nobody likes driving to pick up groceries, finding a parking spot, wheeling all the bags out, loading up the car, and do it in reverse at the other side. It means you're incentivised to take as few trips as possible. You can't just walk to your nearest convenience store or grocer to get a missing ingredient, you have to plan for a week's worth of meals in advance, possibly for a whole family or more. This is also why people have been duped into thinking they need massive cars (though people living under suburban sprawl did fine without them before).

In this environment, online retail makes a whole lot of sense. Why go to the hassle of driving around when someone will literally drop off what you need? In other words, if leaving your house is a chore, don't!

This is where I start to see these massive warehouse videos not as a source of technical fascination, but as the inevitable and dystopian endgame of the great suburban experiment. When you have a large, spread out, and isolated population of people, giving them an Internet connection and optimising *everything* to shovel stuff is certainly one way to paper over the problem. The more I think about it, the more I worry that it can't be healthy for us, or the planet.

I buy lots of stuff online, as evidenced by my retrocomputing posts! That online retailer is also gaining ground in places like Japan, though still nowhere near to the extent of the Anglosphere. But while I'm thankful for access to niche goods, the experience only reinforces my interest to otherwise buy local if I can. I'm not going to go online to order paper towel if I can get it within five minutes down the street. The savings don't justify it, and even the fastest shipping can't compete with my nerdy legs.

I wish we designed our cities and communities so more people could experience this.
