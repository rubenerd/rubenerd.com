---
title: "Brahms on inspiration"
date: "2023-04-09T18:07:37+10:00"
abstract: "Without craftsmanship, inspiration is a mere reed shaken in the wind."
year: "2023"
category: Media
tag:
- music
- philosophy
- quotes
location: Sydney
---
As quoted in the most recent Economist:

> Without craftsmanship, inspiration is a mere reed shaken in the wind.

