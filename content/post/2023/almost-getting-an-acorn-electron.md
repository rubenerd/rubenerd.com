---
title: "Almost getting an Acorn Electron"
date: "2023-12-19T07:39:20+11:00"
abstract: "The British machine from the 1980s was going for a steal. Oh well!"
thumb: "https://rubenerd.com/files/2023/acorn-electron@1x.jpg"
year: "2023"
category: Hardware
tag:
- acorn-electron
- bbc-micro
- retrocomputing
location: Sydney
---
I missed bidding on a near-immaculate Acorn Electron yesterday, complete with a *Plus 1* expansion interface, floppy disk drive, and a copy of the educational Acornsoft *Workshop*.

My impression was the person selling them didn't quite know what they were, or was performing an estate sale, because the whole bundle went for AU $50. I've spent that much on *just* a cart for my Commodore 64, or an Apple IIe reproduction expansion card!

I have zero experience with the BBC Micro family, but I've read about them for a while now. I know the Electron was a lower-end variant, but it's such a beautiful piece of kit. [Thanks to Bilby](https://commons.wikimedia.org/wiki/File:Acorn_Electron_4x3.jpg) for the great photo on Wikimedia Commons:

<figure><p><img src="https://rubenerd.com/files/2023/acorn-electron@1x.jpg" alt="Photo of the Acorn Electron, by Bilby." srcset="https://rubenerd.com/files/2023/acorn-electron@2x.jpg 2x" style="width:500px;" /></p></figure>


