---
title: "It’s a relief seeing NFTs in the rear-view mirror"
date: "2023-10-18T15:44:10+11:00"
abstract: "This basketcase of tech hurt real people."
year: "2023"
category: Ethics
tag:
- art
- blockchain
- nfts
location: Sydney
---
Remember NFTs, or as those of us who knew how they worked called them: non-functional tokens? They were the AI of the late 2010s. Seemingly everywhere overnight, hyped to stratospheric proportions, and a cynical answer to an ill-defined problem.

Much of the world has been rubbing their hands in glee at the downfall of this basketcase of tech, with token valuations through the floor, and interest in the tech evaporating as the con artists and marks [move onto new grifts](https://rubenerd.com/the-grift-shift/ "The Grift Shift, with apologies to The Commodoers"). *Gonna be some big crash… scamming cash… on the Grift Shift.* Man, if [The Commodores](https://www.youtube.com/watch?v=FrkEDe6Ljqs) were any smoother, they'd have melted onto the floor.

I don't think it'd be a stretch now to say the people profiting the most from NFTs are debunkers, who've made a career selling ads on videos that destroy the premise of bad tech. I wish them nothing but success; we need to fight <span style="text-decoration:line-through">misinformation</span> lies with facts.

The problems with NFTs were threefold:

* The tech was deficient and redundant. NFTs relied on blockchain tech, which made them a non-starter when it came to scalability, privacy, and environmental impact. Their use of immutable smart contracts written by fallible humans guaranteed mistakes, exploits, and the introduction of third-parties existing off-chain to arbitrate disputes.

* Nobody knew what NFTs were, not even the grifters. Were you buying ownership, copyright, a link to a web endpoint that had no guarantees of longevity, or something else?

* It exploited vulnerable and uninformed people, by claiming blockchain tech could earn them a living selling their art online. In reality, most money flowed to procedurally-generated rubbish (sound familiar?) to prop up speculative tokens in a pump-and-dump.

While the tech deserves all the scorn and ridicule it's receiving now, it was that third point I found so ethically offensive. I'm sick and tired of creative people being used as fodder. More broadly, NFTs laid bare how hollow this industry can be, and how prone it still is to hype, lies, and scams.

The tech press, talking heads, and businesses will learn this lesson with AI eventually... though admittedly at least that tech has some use cases.
