---
title: "No more X11 on Fedora 40"
date: "2023-09-20T09:43:21+10:00"
abstract: "It all but kills it as a game platform."
year: "2023"
category: Software
tag:
- fedora
- linux
location: Sydney
---
[As reported by Michael Larabel in Phoronix](https://www.phoronix.com/news/Fedora-40-Eyes-No-X11-Session)

> In addition to Fedora 40 planning to ship KDE Plasma 6.0 and without any X11 session support, Fedora stakeholders are also looking at shipping GNOME for the Fedora Workstation 40 release without any X11 session support. 

If this happens, it all but kills Fedora as a game platform for those of us stuck with Nvidia silicon. I don't blame them, but it still sucks.

[My status update on playing games on Linux](https://rubenerd.com/my-status-update-on-playing-games-on-linux/)   
[Fedora 12 installed](https://rubenerd.com/fedora-12-installed/)

Fedora has been my primary Linux desktop since at least 2009, and it also became a great game platform that let me stop using Windows. I know most people have been fine with Wayland, but my experience remains mixed.

I'm not in a hurry to triple-boot this desktop with FreeBSD and two Linux distros, so I guess its time to look for an alternative.
