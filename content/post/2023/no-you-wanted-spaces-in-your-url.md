---
title: "No, you wanted spaces in your URL!"
date: "2023-10-24T11:31:30+11:00"
abstract: "We still have mobile software that sees a URL, and puts in spaces. Computers can’t take over the world, yet."
year: "2023"
category: Software
tag:
- accessibility
- design
- mobile-apps
location: Sydney
---
I wanted to type a URL into a popular mobile chat application. In the words of the late Elvis Presley, it went, a little something, like this:

1. Typed in **http://examplesiteaddress**

2. As I typed the full stop for the **.com** part, it autocorrected to **http://example site address.**

3. I backspaced over the words and retyped them without spaces, because having spaces in a URL is ridiculous.

4. It autocorrected the spaces back in. Again!

5. I backspaced over just the spaces. As soon as I typed the full stop for the .com again, it autocorrected.

It's 2023, and we have mobile software that sees a string of characters prefaced with **http**, and thinks *by golly, gee whiz, I'll bet they wanted spaces in that!*

This oddly gives me comfort that computers won't be taking over the world any time soon. They'd try to invote the `TakeOver()` function, but it'd put spaces in it and crash with a null pointer exception.
