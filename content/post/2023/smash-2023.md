---
title: "SMASH! 2023"
date: "2023-07-03T15:53:50+10:00"
abstract: "My first one in a few years. I didn’t spend as much time there as I would have liked, but was still amazing."
thumb: "https://rubenerd.com/files/2023/smash2023@1x.jpg"
year: "2023"
category: Anime
tag:
- anime
- hololive
- hololive-english
- hololive-indonesia
- pavolia-reine
- smash
location: Sydney
---
I mentioned my meet-and-greet with my HoloID oshi Pavolia Reine yesterday, but what about the rest of this year's Sydney Manga and Anime Show?

[Meeting Pavolia Reine at SMASH!](https://rubenerd.com/meeting-pavolia-reine-at-smash/)

This was the first SMASH! in a while where I was legitimately lost among the sea of art vendors. This is fantastic; I love seeing all the diversity, talent, and care that goes into all this stuff. Artists are facing stiff headwinds from IT people adjacent to me who'd like to generate them out of existence, but you wouldn't have been able to tell from the people queueing up to buy their prints, stickers, posters, wall scrolls, charms, and crafts.

<figure><p><img src="https://rubenerd.com/files/2023/smash2023@1x.jpg" alt="Some assorted stickers and cards from Clara and Amy from the event!" style="width:500px;" srcset="https://rubenerd.com/files/2023/smash2023@2x.jpg 2x" /></p></figure>

It's a running joke among my friends that we primarily see each other once a year during this event, but it's true! Even just stealing a few minutes to chat before we each had to head off to different events was more than I'd seen some of them in years. I didn't see everyone, but when the first response is *hey, you still exist!*, you know it was long overdue.

This 30-something weeb was also incredibly relieved to still count at least a few Sailor Moon, Suzumiya Haruhi, and even Gundam SEED cosplayers among the sea of new series I didn't recognise! Fate was definitely on the way out, though I suppose that was expected. I hung up my gatcha game hat a while ago, so I probably didn't recognise half of them... Genshin maybe? A few people called me out to see my Reine hat and *Tatang*, which I couldn't even take credit for.

Having worked behind the scenes on many past SMASH! and CeBIT events over the years, I know what a ridiculous amount of tireless work and planning goes into them. You all did great, thank you :).
