---
title: "Some new planter plants"
date: "2023-07-24T08:31:06+10:00"
abstract: "Let’s fill the world with more little plants."
thumb: "https://rubenerd.com/files/2023/planter-new-plants@1x.jpg"
year: "2023"
category: Thoughts
tag:
- commuting
- nature
- plants
location: Sydney
---
Some new plants in planters? Planter plants? Some planter new plants?

There's a building in Chatswood that has a series of planters along the footpath. They've been dustbowls for a while, but I've been happy to see some new friends appearing.

<figure><p><img src="https://rubenerd.com/files/2023/planter-new-plants@1x.jpg" alt="Photo showing a row of small plants." srcset="https://rubenerd.com/files/2023/planter-new-plants@2x.jpg 2x" /></p></figure>

Small things like this make my morning walk and commute that much nicer. I also saw someone watering them this morning, which they looked like they needed.

Let's fill our world with more little plants. Like these:

[Nice plant at St Leonards station in Sydney](https://rubenerd.com/nice-plant-at-st-leonards-station/)   
[A local planter now has a lovely plant](https://rubenerd.com/a-local-planter-now-has-a-lovely-plant/)
