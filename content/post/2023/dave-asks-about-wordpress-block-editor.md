---
title: "Dave asks about the WordPress block editor"
date: "2023-01-15T08:59:44+11:00"
abstract: "The feedback from writers on WordPress sites I maintain hasn’t been positive."
year: "2023"
category: Internet
tag:
- blogging
- wordpress
location: Sydney
---
[Dave Winer's blog yesterday](http://scripting.com/2023/01/14.html#a140751)\:

> I don't use WordPress very much, but recently I had a chance to try out the block editor and my reaction was that I can't imagine writing this way. If you're a writer who uses WordPress, do you use the block editor, if so what do you think of it?

I've maintained a dozen or so WordPress sites for various people and orgs since 2006. Not for lack of trying, but the feedback from people of all technical abilities, experience, and writing style is that the block editor is frustrating for anything more than page layout work.

Some of us have learned the hard way to write elsewhere and paste, but some people do use the editor as an... editor. And for that, it's primary conceit is treating paragraphs as discrete objects, rather than connected thoughts. Writers don't think, draft, or edit like this.

I've made them happier by installing the [Classic Editor](https://wordpress.org/plugins/classic-editor/), and am keeping my eye on projects like [ClassicPress](https://www.classicpress.net/). Hopefully their widespread use sends a message.

