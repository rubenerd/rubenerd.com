---
title: "Making mistakes with NICs"
date: "2023-01-13T12:39:55+11:00"
abstract: "Who needs an IP address on a NIC?"
year: "2023"
category: Internet
tag:
- making-mistakes
- networking
location: Sydney
---
I'm loving this idea of sharing ones personal foibles. We're all human here, and sharing our mistakes helps others learn. I also find it oddly comforting to see people I respect making the same mistakes I do.

I spent twenty minutes today trying to figure out why I hadn't been able to SSH into a VM. I verified I had the correct ports open on the firewall, that the OpenSSH service was running, and more embarrassing checks including making sure the VM was indeed running.

I hadn't attached an IP address.

