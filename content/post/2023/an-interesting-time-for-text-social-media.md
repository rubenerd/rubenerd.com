---
title: "An interesting time for (text?) social media again"
date: "2023-07-10T10:24:32+10:00"
abstract: "We haven’t seen a shakeup like this for a while."
year: "2023"
category: Internet
tag:
- bluesky
- social-media
- twitter
location: Sydney
---
Who'd have thought 2023 would be the year where text-forward social networks would get a shake up? It seemed that for years the Internet dust had settled on a handful of sites, and everyone had resigned themselves to this bland, dystopian reality.

I qualify my description with *text-forward*, because image and video sharing sites have been in flux for a few years now. It's interesting to think about, given you'd expect the barrier to entry for a social network based around text would be so much lower. Encoding text is what the web was designed for, but images and video introduce formats, scaling, encoding, and *vast* storage and bandwidth requirements.

*(I work at a small cloud infrastructure company that deals with petabytes of data. This already seems unfathomably large, but the space these latest video sites must use? It's beyond my ability to even comprehend).*

In some ways, the longevity of these text networks demonstrate just how powerful the *network effect* really is; and more broadly, how business incentives align to lock us into fewer and fewer places.

Those of us who've been around the block a few times remember when the web consisted of open or *defacto* protocols like mailing lists, IRC, and the like. Then it was Friendster, MySpace, and Orkut. Then it was Twitter, Facebook, Ping, and Google+. People attempted to replace Twitter with Jaiku, Pownce, and Soup.io. A decade later, and we were all talking about App.net and Tent.is. Most of these withered, and Twitter persisted.

Why does it feel different this time? It felt different last time too, so maybe there's a bit of wishful thinking. Here comes the proverbial posterior prognostication: *but*... I think there are different forces at play than before.

Whereas previous networks touted their features and benefits over Twitter, this time the site is being hollowed out from the inside. It wasn't the spam, trolls, and advertising that I thought would do it in, but the site being bought by a *baka*. Nobody could have scripted such a perfect storm, yet here was this lauded business expert torpedoing his net worth while overseeing an explosion in spam and network issues that make the *Fail Whale* era feel quaint. Also, the whole thing about realising you can't just fire people in places like Europe with a semblance of workers’ rights. *The whippings won't stop until morale improves!*

It's open season now.

This time, Mastodon has achieved what services like Tent.is didn't: it found a seizable niche of people who revel in not being a product themselves, and engaging with others in a decentralised, federated space in which anyone can host a server. I now spend more time there than any other social network, in spite of most of my friends not being there. Structurally, I think it has the greatest chance of being around when even the latest crop of would-be networks fizzle out, even if it doesn't become the place where *everyone* gathers. I suspect that for a lot of admins, that sounds great.

[@rubenerd@bsd.network](https://bsd.network/@rubenerd)

I mentioned (briefly) last week that I got Bluesky, after a generous person pinged me with an invite code. I've been reading the firehose there for a few days since, and it's been fascinating to read the reactions. People talk about how refreshing and friendly the site is compared to Twitter, and lament Threads stealing their brief spotlight. The promised "AT protocols" for open social networks nay warrant a mention. People see it as a drop-in Twitter replacement in a way they don't see Mastodon. Again, telling.

[rubenerd.bsky.social](https://bsky.app/profile/rubenerd.bsky.social)

I also got Threads, or perhaps more accurately, I activated my Threads account. It uses your Instagram account for auth, which admittedly I hadn't logged into for years. The service isn't available in Europe... gee, I wonder why that would be? But you're not missing out on much; I deleted it off my phone soon after. I will say that it's interesting that people love it for its reliability, familiarity, and the fact the sign-up process was simple. The invite-only Bluesky Social, and Mastodon clients, would do well to heed this feedback.

*(I was going to put my Threads handle here too, but... nah).*

Regardless of one’s thoughts on the personalities, companies, and motivations behind this latest crop of social networks, I'll admit it's interesting to watch. Maybe, just *maybe*, we'll trend in the right direction again after a couple of lost decades.
