---
title: "Coffee smells and lockdowns"
date: "2023-06-16T15:51:38+10:00"
abstract: "A random brew took me back to 2020."
year: "2023"
category: Thoughts
tag:
- covid-19
- coffee
- health
location: Sydney
---
I noticed something when I was making coffee yesterday in our kitchen. I ran the beans through the electric grinder, took the lid off, and the smell transported me back to 2020.

At the height of Covid, before we had vaccines and we were confined to home for weeks at a time, I substituted my morning coffee shop run with brewing it at home. In a world that was somehow uncertain yet tedious, I liked that I at least had control over this one part of my life. I'd brew the coffee, take it out to the balcony, set up the laptop, and repeat the same day I'd had for weeks.

[Working remote](https://rubenerd.com/working-remote/)   
[First coffee shop in four months](https://rubenerd.com/first-coffee-shop-in-four-months/)

I've made plenty of coffees at home since, but maybe it was the time of day, mixed with those specific beans, but I was right back there again. What is this virus going around? Is everyone going to be okay? What do I need to do? Why do I feel so uneasy?

I also realised recently there are already people alive now who didn't go through that, or were too young to remember. Let's hope we learned lessons for the next time.
