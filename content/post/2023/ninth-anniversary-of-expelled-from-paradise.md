---
title: "Ninth anniversary of Expelled From Paradise"
date: "2023-11-18T09:47:39+11:00"
abstract: "This 2014 anime movie somehow became my yardstick for how old to feel."
thumb: "https://rubenerd.com/files/2023/9-years-efp@1x.jpg"
year: "2023"
category: Anime
tag:
- anime
- saitom
location: Sydney
---
This 2014 anime movie somehow became my yardstick for how old to feel. Its use of computer graphics seemed so groundbreaking and modern at the time, even before Live 2D became a regular thing Clara and I would watch with VTubers. Its release also coincided with starting my current job.

Angela Balzac and I will be up for long-service leave next year. That feels like an accomplishment, but it's also ridiculous.

<figure><p><img src="https://rubenerd.com/files/2023/9years-efp@1x.jpg" alt="Ninth anniversary graphic from the original movie." style="width:500px;" srcset="https://rubenerd.com/files/2023/9years-efp@2x.jpg 2x" /></p></figure>

*Art from the [official EFP Twitter](https://twitter.com/efp_official/status/1724621374956949607), via [Saitom himself](https://twitter.com/_saitomasatsugu).*
