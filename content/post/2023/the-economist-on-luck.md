---
title: "The Economist on luck at work"
date: "2023-10-21T08:53:57+11:00"
abstract: "If it’s so important to outcomes, what are we doing to maximise it for everyone?"
thumb: ""
year: "2023"
category: Thoughts
tag:
-
location: Sydney
---
[From their issue last week](https://www.economist.com/business/2023/10/19/how-big-is-the-role-of-luck-in-career-success)\:

> But if luck does play a more important role in outcomes than is often acknowledged, what does that mean? For individuals, it suggests you should increase the chances that chance will work in your favour.

They do say luck favours those who are prepared.

> An awareness of the role that luck plays ought to affect the behaviour of managers, too. 

True.

I was expecting that to then flow onto what governments can do, but the article fell short. A pity, because if luck is so intrinsic to success, I'd consider it a moral imperative to improve it for everyone.

Luck is a touchy subject, because the "self made" bristle when they're told their hard work was aided by circumstances, genetics, upbringing, and the society in which they live. That attitude also needs to change for us to make a meaningful positive impact for everyone.
