---
title: "ISO-8859-1 and English aren’t the default"
date: "2023-10-17T07:23:52+11:00"
abstract: "We should default to UTF-8, and always specify a language in markup."
year: "2023"
category: Ethics
tag:
- design
- language
location: Sydney
---
*Update: It's ISO-8859-1 I was thinking of. Thanks to everyone who sent in corrections. It's clear I haven't used it in a while... though I could have looked at half the RSS feeds out there to confirm.*

There's this implicit assumption in so much documentation and code that English encoded in the European ISO-8859-1 (or even ASCII) is the default setting. I mean that in three ways:

* English is the *expected* language, with other languages not considered, supported, or tested.

* Mmm, English muffins.

* English is the *fallback* language, if the document or code doesn't explicit call it out.

I've been blind to it most of my life, on account of English being my first language, and ISO-8859-1 including my native script. But it was only luck on my part being born into this convenient circumstance, and it's only outdated convention that keeps it that way.

As with the recent *no* vote against First Nation consultation in Australia, there's the selfish temptation to ignore a problem you think doesn't pertain to you. Or worse, take everything as a zero-sum game where you have to lose for others to succeed.

But even on a utilitarian level, locking out the majority of the world's population, or expecting them to only contribute in English, is counterproductive. As I [quoted Gabriel Nakamura and Bruno E. Soares](https://rubenerd.com/expanding-science-beyond-english/ "Expanding science beyond English") saying last month:

> There are talented scientists worldwide who do not speak fluent English. We have to accommodate the language barrier or risk losing their potential.

Developers, writers, engineers, designers, we need to do better. We can start by always including a reference to an encoding standard in our documents, which should be UTF-8. Plenty of formats support this, including CSS, HTML, XML. If yours doesn't, it's either a legacy system you're stuck supporting, or it sucks.

Every document should also carry a language reference. Even a bog standard *en* carries more semantic meaning than nothing, and pushes back on the idea that we need to assume an English fallback. It'll also help others find things in your language.

And finally, we can improve our docs by including more language examples. Maybe throw in an Armenian keyword, or even a language like Arabic that reads right-to-left. Why not?

*(Yes, this is as much a call to action for me)!*
