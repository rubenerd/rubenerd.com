---
title: "It does everything, if it does something"
date: "2023-11-26T10:54:46+11:00"
abstract: "Dan Olson elaborates on how current AI hype compares to past tech bubbles."
year: "2023"
category: Ethics
tag:
- ai
- journalism
location: Sydney
---
Earlier this week [I quoted Dan Olson chatting with Adam Conover](https://rubenerd.com/dan-olson-and-adam-conover-discuss-the-open-web/ "Dan Olson and Adam Conover discuss the open web") about the open web. The whole interview is [worth a listen](https://www.youtube.com/watch?v=4aU-QkJfgGw "Debunking the Tech Hype Cycle with Dan Olson - Factually! - 213").

I was also intrigued to hear how the current hype surrounding AI compared to past bubbles like the metaverse and cryptocurrency:

> AI is tricker because it has emerged very slowly out of useful tools. It's in this irrational hype bubble that better resembles the original dot com boom. We have a tool that does **something**, so you have a whole bunch of people rushing in to claim it does **everything**.

And the market for what it produces?

> There is a very financially profitable race to the bottom [for quality] in being able to churn out [...] garbage at scale. You and I can argue audiences don't want mass produced, derivative slop that's cranked out by a robot that doesn't know what it's doing. And that's true, people don't want that. But people will still drink it.

The defence I've read is that these tools generate better slop than what modern journalism does. It's hardly a glowing endorsement of either if true, and not something for which I hold any excitement.

Hoping LLMs will *save* journalism, and generative AI will *save* art, is the tech equivalent of burning coal to power an air conditioner. We could address the causes for why things online have become so bad, but that would involve addressing more fundamental societal and economic questions. That sounds hard, so hey look, shiny tech!

In the (translated!) words of Jean-Baptiste Alphonse Karr, the more things change, the more they stay the same.

