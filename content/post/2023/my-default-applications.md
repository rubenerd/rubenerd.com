---
title: "My default applications, via proycon"
date: "2023-11-09T09:19:40+11:00"
abstract: "My latest list of default applications, even though I already have most of this on my Omake."
year: "2023"
category: Software
tag:
- apple
- bsd
- fedora
- freebsd
- linux
- macos
- lists
- tools
location: Sydney
---
Proycon did a post about [his default applications](https://proycon.anaproy.nl/posts/default-apps/), prompted by some others doing the same. I didn't listen to the podcast that started it, and I technically my [Omake](https://rubenerd.com/omake/ "Rubenerd Omake") enumarates these already, but I'll never pass up an opportunity to post a list!

Aside from a sneay "retro emulation" item below, I've resisted the temptation to include retrocomputer tools. Maybe that warrants its own separate list.

* Audio editing: [ffmpeg](https://www.freshports.org/multimedia/ffmpeg/), [sox](https://www.freshports.org/audio/sox/), [eyeD3](https://www.freshports.org/audio/py-eyed3/)
* Backups: [OpenZFS snapshots](https://docs.freebsd.org/en/books/handbook/zfs/), [rsync](https://www.freshports.org/net/rsync/)
* Budgeting, kitchen sinking: [LibreOffice Calc](https://www.freshports.org/editors/libreoffice-en_GB/)
* Credentials: [KeePassXC](https://www.freshports.org/security/keepassxc/)
* Database prototyping: [sqlite3](https://www.freshports.org/databases/sqlite3/)
* Desktop: [FreeBSD](https://www.freebsd.org/), [NetBSD](https://www.netbsd.org/), [macOS](https://www.apple.com/au/macos/), [Fedora KDE Spin](https://fedoraproject.org/spins/kde/)
* Disk images: [qemu-utils](https://www.freshports.org/emulators/qemu-utils/)
* Docs: [Handwritten LaTeX (!) via TeX Live](https://www.freshports.org/print/texlive-base/)
* Diagrams: [LibreOffice Draw](https://www.freshports.org/editors/libreoffice-en_GB/)
* Gemini browser: [Lagrange](https://www.freshports.org/www/lagrange/)
* Image editor: [GIMP](https://www.freshports.org/graphics/gimp-app/), [ImageMagick7](https://www.freshports.org/graphics/ImageMagick7/) scripts
* Music: [Elisa](https://www.freshports.org/audio/elisa/), [musikcube](https://www.freshports.org/multimedia/musikcube/)
* Orchestration: [Ansible](https://www.freshports.org/sysutils/ansible/)
* PIM, email, newsgroups, RSS: [Thunderbird](https://www.freshports.org/mail/thunderbird/)
* Proposals/tenders/etc: [Microsoft Word](https://formulae.brew.sh/cask/microsoft-word#default) (le sigh!)
* Retro emulation: [86box](https://86box.net), [ScummVM](https://www.freshports.org/games/scummvm/), [VICE](https://www.freshports.org/emulators/vice/)
* Scripting: [Perl](https://www.perl.org/)
* Shell: [Portable OpenBSD Korn shell](https://www.freshports.org/shells/oksh/)
* Text editor: [Kate](https://www.freshports.org/editors/kate/), [Vim](https://www.freshports.org/editors/vim/)
* Vector editor: [Inkscape](https://www.freshports.org/graphics/inkscape/)
* Video editing: [DaVinci Resolve](https://www.blackmagicdesign.com/products/davinciresolve)
* Video playing: [mpv](https://www.freshports.org/multimedia/mpv/)
* Web browser: [Firefox](https://www.freshports.org/www/firefox/)

I'm sure I'm forgetting things, but those are the essentials.
