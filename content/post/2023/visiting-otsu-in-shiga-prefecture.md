---
title: "Visiting Ōtsu in Shiga Prefecture"
date: "2023-04-29T22:23:42+10:00"
abstract: "What a beautiful little city!"
thumb: "https://rubenerd.com/files/2023/otsu-street@1x.jpg"
year: "2023"
category: Travel
tag:
- japan
- japan2023
location: Sydney
---
Much of my latest Japan trip was unfortunately spent with a series of headaches, but in a roundabout way they lead Clara and I to a new place we'd never been to before! And it was just what the doctor ordered.

We'd had such success at the *Hard Off* second-hand electronics store in Kyōto, that we decided to check out another branch that was accessible by rail. Unlike car or bus rides, the smooth and comfortable intercity trains in Japan are a great way to explore with a headache. Maybe that's another avenue for tourism promotion they should consider!

[Exploring an incredible Kyōto Hard Off store](https://rubenerd.com/exploring-an-incredible-kyoto-hard-off-store/)   
[Posts from our Japan 2023 trip](https://rubenerd.com/tag/japan2023/)

<figure><p><img src="https://rubenerd.com/files/2023/otsu-station@1x.jpg" alt="Photo of the Ōtsu station sign above the platform. An LED indicator board and analogue clock are in the background." srcset="https://rubenerd.com/files/2023/otsu-station@2x.jpg 2x" style="width:500px; height:333px;" /></p></figure>

Taking a look at their locations, we noticed there was a Hard Off near the coast of Lake Biwa in Ōtsu (大津市), the famed town just to the right of Kyōto. It was accessible via the Biwako JR West Line, and two Keihan lines. Like much of the Kansai region, it's well connected with Ōsaka, so we got comfortable in a train and watched the world go by... albeit with Tiger Balm and sunglasses!

I wasn't sure what to expect when we got there, but after we exited the tunnel underneath the mountains dividing Shiga Prefecture from Kyōto, we arrived at the JR station in the cute town square. You could immediately feel the cool breeze coming from the lake as you stepped out, which was welcome after a few days of much warmer temperatures.

<figure><p><img src="https://rubenerd.com/files/2023/otsu-street@1x.jpg" alt="Photo of a beautifully manicured street on the way to the station." srcset="https://rubenerd.com/files/2023/otsu-street@2x.jpg 2x" style="width:500px; height:333px;" /></p></figure>

We could have taken one stop further on the JR to Zeze to transfer easily to the Keihan line, but we elected to go for a stroll for some fresh air. It was a great decision; coming from the bustle of Ōsaka, and the throngs of tourists in Kyōto, the tranquil, tree-lined streets were an absolute joy. The well-manicured greenery and mix of classic and new architecture reminded me a bit of Odawara, that beautiful old town south of Tōkyō we visited in 2018 that has my favourite castle in the whole country.

Coming to the lakefront, we wandered along the pier and took in the sights of Lake Biwa. I forgot to take pictures of the lake itself (whoops!), but the one below shows some of the more modern hotels that sprung up around the shoreline. It was so calm, quiet, and the breeze smelled wonderful; I can imagine staying there would be quite the escape for a stressed salaryperson coming from a big city.

<figure><p><img src="https://rubenerd.com/files/2023/otsu-pier@1x.jpg" alt="Photo of a small building and much larger, modern hotel on the banks of Lake Biwa." srcset="https://rubenerd.com/files/2023/otsu-pier@2x.jpg 2x" style="width:500px; height:333px;" /></p></figure>

We probably only passed a dozen people on our way to the Keihan station, and not a single other *gaijin*. Both surprised me, given the town's close proximity to Kyōto. It proved to me again that while Japan has some amazing sights, it does pay to venture even just a little bit sometimes. It's probably true of any tourist hotspot.

We got to the Keihan station, which had a great view of the street-running section of track at the intersection below. I'm lucky that Clara is a *rail otaku* as well, and we camped out for a short time taking photos and some video of the cute, two-carriage trains, before heading down to take a ride.

<figure><p><img src="https://rubenerd.com/files/2023/otsu-train@1x.jpg" alt="Photo of a complex intersection with four tram lines and road traffic. Tree covered mountains are in the distance." srcset="https://rubenerd.com/files/2023/otsu-train@2x.jpg 2x" style="width:500px; height:333px;" /></p></figure>

We arrived at Ōtsukyo station, and went for a walk through suburban Ōtsu to get to the Hishiotsu *Hard Off* store. That'll be a topic for another post, but in short, it was also amazing!

With the sun setting, and some amazing food from the most unlikely of places (I'll reserve that for the *Hard Off* post too), we took the train back to Kyōto, then Ōsaka. We were only there for a few hours, but Shiga Prefecture has left an Ōtsu-sized mark on me. We're *definitely* going back!
