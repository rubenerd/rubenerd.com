---
title: "Reisalin Stout fig from Atelier Ryza 3"
date: "2023-01-15T08:30:32+11:00"
abstract: "She’s my favourite version, but I also have absolutely no space."
thumb: "https://rubenerd.com/files/2023/ryza-3.jpg"
year: "2023"
category: Anime
tag:
- atelier-ryza
- games
location: Sydney
---
Ryza has done so much for the visibility of the Atelier franchise, for reasons I suspect everyone can grok. Her personal popularity has also been cemented with more figures than I can count on two hands, which is *highly* unusual outside blockbuster anime series, or enduring characters like Hatsune Miku who could fill a shop by herself. And we'd let her.

Wonderful Works now has their rendition of Ryza from her upcoming third adventure *Alchemist of the End & the Secret Key* for preorder from the usual places. She's easily been my favourite out of her three versions since I saw the key visuals for the game, and especially after seeing her [life-sized statue](https://www.gonintendo.com/contents/9581-all-three-life-sized-ryza-models-are-on-display-at-tokyo-game-show-2022) at last year's Tokyo Game Show. 

<figure><p><img src="https://rubenerd.com/files/2023/ryza-3@1x.jpg" alt="Press image of Ryza in her third form, from Alchemist of the End & the Secret Key." style="width:300px; height:400px;" srcset="https://rubenerd.com/files/2023/ryza-3@2x.jpg 2x" /><br /><img src="https://rubenerd.com/files/2023/ryza-3-2@1x.jpg" alt="Press image of her back, showing her billowing cape." style="width:300px; height:400px;" srcset="https://rubenerd.com/files/2023/ryza-3-2@2x.jpg 2x" /></p></figure>

I love the extra detail in her hair, the gold trim and patterns on her cape, and that she's still wearing her cute white hat! The blue pennant is also a nice touch, though I'd worry about it sagging over time as some of my other figs have.

At 1:7 scale, she'd take up much less space than a life-sized statue from a game convention too. But I also only just cleared a shelf to make room for my Commodore 64C stuff, and I still need to get that kit for the PiPDP-11 built and accommodated for somewhere. The life of a retrocomputing weeb is a tough, though entirely self-imposed one.
