---
title: "Generative AI: Create, Don’t Scrape"
date: "2023-12-24T09:14:12+11:00"
year: "2023"
category: Ethics
tag:
- ai
- generative-ai
- spam
location: Sydney
---
Long-time reader Simon sent me a link to this great new site: *[Create, Don't Scrape](https://www.createdontscrape.com)*:

> Creators (Artists, Musicians, Actors, Writers, Models) have been scraped and exploited by tech companies without consent, credit or compensation. Below are resources to learn more about the Laion Database, Data Laundering, Lawsuits, and Generative AI. Stay strong.

It reminds me of *[Web3 Is Going Just Great](https://web3isgoinggreat.com/)* site, but for Generative AI.

This is a fantastic resource to link people to. Technical people who discuss their love of generative AI tools without any ethical or legal context [are negligent and irresponsible](https://rubenerd.com/journalists-failing-at-reporting-on-ai-chatbots/ "Journalists failing in their AI chatbot reporting"), but the person on the street genuinely might not know.
