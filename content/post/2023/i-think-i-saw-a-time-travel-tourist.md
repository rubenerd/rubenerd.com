---
title: "I think I saw myself as a time-travel tourist"
date: "2023-03-22T18:34:25+11:00"
abstract: "I liked your hat."
year: "2023"
category: Thoughts
tag:
- life
- philosophy
location: Sydney
---
There is no way to write this without sound ridiculous, *but when has that ever stopped me?*

I was walking out to get lunch today, like a gentleman, when out the corner of my eye I either saw my dad, or someone strongly resembling him. Maybe my uncle flew into Sydney from Germany, and decided that Chatswood was the place to be on a hot autumn day?

*(A point of clarification first: would my dad's half-brother be my half-uncle? Is there such a thing? Or would I simply refer to him as my uncle? "An uncle from another mother?" I really empathise with those for whom English isn't their second language; I'm a native speaker, and I barely grasp it at times).*

I was walking faster than this gentleman, so eventually I caught up to where he was walking. As I passed him at a pedestrian crossing, I turned to see who it was. Instead of seeing my dad or another long-lost relative, the person looking back was the spitting image of... me.

You know those aged mug shots that forensic artists draw to show what a fugitive might look like with the passing of time? Well, it was though someone had taken *my* face, aged it forty years, and slapped it onto someone's face in the real world. I can't give specifics, but everything about him looked like me. Or just as accurately, I was the spitting image of him.

We stared at each other for barely a moment before the crowd of other people crossing the intersection enveloped us. Then he was gone.

I can't explain it, but it shook me up! It was oddly... uncanny. How often do you walk around and see an older version of yourself?

Rationally, I know that it's bound to happen. We're all unique and special like everyone else, so the saying goes. There is tremendous potential for genetic diversity, but there's also a slim chance that two people will be born with sufficiently similar genes to bear a resemblance. Who knows, maybe he looked less like me when he was my age, but he evolved into my face shape and build with time. Clara suggests he might have been a doppelgänger.

Or, hear me out... I accidentally met myself, as a time-travelling tourist. By the time I reach my seventies, time-travel tourism may have become commercially viable, and I decided to see how lovely the world used to be before my run-in with a Japanese vending machine in 2026.

There would certainly be rules about avoiding contact with your past self, on account of the butterfly effect. Maybe the bean counters also consider it a potential source of insider trading, because they're buzzkills. Either way, he clearly fled the scene after I saw him, because I couldn't see him *anywhere*.

The thing is, he *must* have known this would happen, because forty years ago he saw an older version of himself. I wonder if this factored into his decision to make the trip at all? Maybe he was counting on my past self forgetting.

The good news is, it was comforting to see him living his best life. If you're reading this future Ruben, it was nice to see you. I liked your hat.
