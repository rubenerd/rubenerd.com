---
title: "Keeping computer stuff open or closed"
date: "2023-04-30T09:48:49+10:00"
abstract: "I keep everything closed if I can, but most people seem to live in a world of hundreds of tabs"
year: "2023"
category: Software
tag:
- anxiety
- memory
location: Sydney
---
Aside from a few reckless years in my early 20s (we all act out in our own ways at that stage), I've always guarded system resources like a hawk. Maybe it was due to my early childhood upbringing on DOS and Windows 3.x, or maybe I find it an affront to waste resources in general.

It means I keep applications I have open down to an absolute minimum. Granted, I'll run desktop environments like KDE on FreeBSD because it makes my machine more functional and fun. But terminal windows, browsers, text editors, and file managers will only have enough tabs, buffers, or windows open to do the task at hand, before being bookmarked, saved, and/or closed as soon as possible.

In recent years, I've also come to depend on this process to reduce anxiety and help with my ever-shrinking ability to focus. Clutter makes me nervous, so having a clean virtual workspace physically calms me, and aids concentration. Too many open things, and I feel an unsettling loss of control that's hard to explain (it's probably also why I hate feeling drunk).

Strangely, the only time I've caught myself with runaway tabs recently was with the Lagrange Gemini browser. When pages take up kilobytes and open instantly, they physically feel lighter too. I wonder if that plays into this?

[The Lagrange GUI Gemini browser](https://gmi.skyjake.fi/lagrange/)

I bring this up, because I get the impression this approach to using modern computers is uncommon. People see my Mac dock at work, or my KDE panels, and are surprised at how few icons are in them. Almost everyone else I know keeps hundreds of applications and tabs open, especially in web browsers. The fact their machines are functional *at all* under those conditions is a testament to modern operating system design.

We have the stereotype of the genius surrounded by millions of papers and open books, because their multitasking minds operate faster than the flow of information that can be feasibly sent to it. Not being a genius isn't news to me! But it's interesting to see how some people genuinely thrive in such an environment. Not to put too fine a point on it, but it would be *extremely* uncomfortable.
