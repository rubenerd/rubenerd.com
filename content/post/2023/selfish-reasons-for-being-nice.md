---
title: "Web form has a free text field? Say thanks!"
date: "2023-07-17T15:34:49+10:00"
abstract: "You’d be shocked how far such basic courtesy goes."
year: "2023"
category: Internet
tag:
- love
- shopping
- sociology
location: Sydney
---
Years ago I got into the habit of leaving positive messages in optional text forms on order pages, like those for delivery instructions or customisations. Often it's just a "thanks! :)" or my attempt at a translation into their local language. It takes almost no effort whatsoever.

You would be shocked how far such basic courtesy goes, which perhaps speaks to how rude most people tend to be online. One seller sent a wall of text thanking me for making their day better after dealing with someone awful. Other times, it's a handwritten note saying "right back at you!", a smile on the parcel, or some stickers... even from megacorporations. Clara gets a shocking amount of free stuff from indie artists for putting in what we both consider the bare minimum of pleasantries.

I know that when I get something positive from a client, it makes my day. So I try and do the same for others. Who knew, when you're nice to people, *they're nice back!*

So far the only time it backfired was when I ordered a few small PCBs from an on-demand fab. Because I put "谢谢您!" in the notes field, the order was put on hold while someone verified my "additional instructions". This delayed shipping by at least a few days, but maybe it gave someone on the factory floor a chuckle. Which really, I reckon was still worth it.

