---
title: "Timestamps are all over the place"
date: "2023-04-28T17:36:44+10:00"
abstract: "If you see this as the latest post on the 28th of April, it might not be as bad as I thought!"
year: "2023"
category: Internet
tag:
- weblog
location: Sydney
---
I've done goofed something with my blogging setup since coming back from Japan. Blog post timestamps are all out of order, and all over the shop. I feel like I'm scrolling through Instagram again, before I stopped using it.

I'll let the site catch up, maybe. If you see posts published after this one on Friday the 28th of April, know that it was supposed to be the latest one. Maybe. I can't even keep track. Where am I? Which one am I?

**WHICH ONE AM I!?**

...

... whew.

One day I'll implement my own unique blog engine, just like everyone else. It'll be in Perl, with Text::Template and XML::LibXML, just you `&wait()`.
