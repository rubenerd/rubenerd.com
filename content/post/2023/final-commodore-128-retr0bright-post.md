---
title: "My final Commodore 128 Retr0bright post"
date: "2023-01-02T08:57:39+11:00"
abstract: "It looks like a new machine."
thumb: "https://rubenerd.com/files/2022/commodore-128-numpad@1x.jpg"
year: "2023"
category: Hardware
tag:
- commodore
- commodore-128
- retr0bright
location: Sydney
---
Last year (wow, that felt weird to say) I [retr0brighted my Commodore 64C](https://rubenerd.com/retrobrighting-a-commodore-64c/) with some Australian hair developer cream. The results were spectacular; all the yellow streaks were completely gone, and the clean keyboard matched beautifully.

I've now done the same with my Commodore 128. My first attempt using Oxi Action powder reduced some of the yellowing on the keys, but it left room for improvement. I did the numpad first, and one day in the Australian summer sun with that cream showed a huge difference:

<figure><p><img src="https://rubenerd.com/files/2022/commodore-128-numpad@1x.jpg" srcset="https://rubenerd.com/files/2022/commodore-128-numpad@2x.jpg 2x" alt="A Commodore 128 keyboard showing slightly yellowed alphanumeric keys, and numpad keys that look practically the same as the case."></p></figure>

I've now done the rest of the keyboard, and couldn't be happier with the results.

The RAM replacement and diagnostic carts for this machine should be here soon, so hopefully I'll get other parts of this machine working again soon too :).

