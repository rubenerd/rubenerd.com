---
title: "Answering James Savage on blogging"
date: "2023-11-02T09:53:56+11:00"
abstract: "Writing versus publishing, reading what we right, and if something is too personal to write about."
year: "2023"
category: Internet
tag:
- blogging
- weblog
location: Sydney
---
*Apologies, the date on this got borked somehow.*

James Savage [has a new blog](https://axiixc.com/) you should all read. He also [posted some reservations](https://social.axiixc.com/@axiixc/111322543249599429 "Mastodon post") about it:

> I've been stewing on a bunch of thoughts like “do I actually want people to read things I've written" and “is this too personal to talk about on the internet”, but instead of coming to a resolution on those I stuck webmention link tags on my site. Curious if those will attract anything other than spam.

I ask myself this every time I press post. Some days are easier than others, but I have to psych myself up each time. Clearly I must get over it though, so what are the answers? Let's try and figure them out with some patented Rubenerd waffle!


### Writing versus publishing

James and I might have reservations for different reasons, but mine generally come from misplaced risk aversion, a bit of what cool people are now calling Imposter Syndrome, and this weird dichotomy between the activity of writing, and posting.

Writing online is a solitary, personal activity, but the output is broadcast over the most public system on the planet. It's surreal to think one can have reach now that eclipses what major newspapers had. That's a lot of eyeballs interrogating something I wrote while huddled in the corner of a coffee shop before work. Traditional outlets had editors, proofreaders, researchers, and printers between our words and the public. You'd have to be a stoic made of granite to not feel *any* intimidation from that.

I wish I could say it gets easier, but for me it comes in waves. There are days where I'm confident about what I've written, and others where I shuffle my feet for hours. The long-neglected contents of my drafts folder dwarfs the number of published posts by at least a factor of five, and I sometimes shamelessly plumb its depths for posts and topic ideas, wondering why the hell I was so scared about something.


### *Do I want people to read what I write?*

But back to James' point though, say we get over our fear of posting... do we *want* people to read it? Or inversely, what would make us *not* want people to read it?

I've noticed that empathetic people are especially vulnerable to worrying about judgement and feedback, because they know how bad it can feel to receive something bad. An attack on one's hobbies can feel alarmingly personal, because it's a part of our identity. It's even worse for those who's mere existence immoral people sook about, whether it's orientation, gender, ethnicity, or something else.

Mix this with a bit of undeserved self-doubt about one's writing style, authority, and abilities, and we have a powerful set of disincentives for posting.

It's natural to not want to broadcast your thoughts if you think they're weird, or will invite critique from people who don't have your best interests at heart. The web regularly attracts those who either don't know or care about how their opinions come across. Even today I get awful messages from people saying I'm an idiot who doesn't know what he's doing. *They're probably right!* But there's a time and a place.

The good news is, the web is equally excellent at delivering people who *are* into your hobbies and perspectives. I've learned there's no such thing as being *too* niche; some of the best feedback I've ever had are from engineers who dabble in horticulture as a hobby, and had feedback about house plants. These messages can turn me from feeling like a sook in the morning to clicking my heels as I run for the train. Knowing I helped someone, made them laugh, or made them *think*... there's no better drug.


### *Is this too personal to talk about?*

This gets to James' final point, which is also interesting to think about.

The short answer I've found is to trust your gut. This advice works well for activities involving your back too: should I lean over to pick this thing up in an awkward way? Or do any of the gymnastics moves I used to do with a decade or more away from training?

This does come down to personal preference. I used to keep a firewall up between my personal life and my blog, but the former has slowly crept into the latter over the years. Some of this was due to the fear of judgement I talked about above, which I feel far less of now. If anything, I've found people are *extremely* gracious and lovely when I've opened up in ways I would have been far to scared to a decade ago. Getting older comes with the realisation that *everyone* has problems and doubts, even the most well-adjusted people among us.

I'd say, experiment with opening up a little and seeing if you're comfortable. Most of us blog as a hobby, so it's pointless if you're nervous or not having fun.


### Conclusion

I feel like I'm in this weird position where I've blogged more than 8,000 times, but that I'm no more qualified to talk about it than someone who's posted a dozen times. The truth is, once you've got over that hurdle of posting once, you've already ahead. Or a foot. Or any other body part. And there's something to be proud of there.

We need your voice, *especially* in light of the procedurally-generated future in which we find ourselves. [Be human](https://rubenerd.com/daniel-jalkut-on-ai-authenticity/ "Daniel Jalkut on AI and authenticity")! 
