---
title: "Nico Cartron on French language misuse"
date: "2023-11-29T07:28:18+11:00"
abstract: "Omelette du fromage!"
year: "2023"
category: Thoughts
tag:
- dexters-labratory
- france
- language
location: Sydney
---
If you're an English-speaker in your thirties today, there's a chance you say *omelette du fromage* when you want to sound French. It comes from a [1996 episode](https://dexterslab.fandom.com/wiki/The_Big_Cheese "The Big Cheese") of *Dexter's Labratory* where a failed experiment to quickly learn French through audio osmosis ends up with him only being able to say that one phrase.

Nico Cartron emailed in a while ago to let me know that *it isn't even correct French!*

> You said "omelette du fromage" which means "omelette of the cheese", 
surely you meant "cheese omelette", whose French would be "omelette au 
fromage"?

He also has a [silly joke about omelettes](https://www.ncartron.org/fun-french-joke-about-omelettes.html "Fun French joke about omelettes") on his blog. *Magnifique!*

I'll admit, the sum total of my French comes from snippets reading and watching Tintin. And that's *Belgian* French. I'm sure there's a pun there somewhere about waffle.
