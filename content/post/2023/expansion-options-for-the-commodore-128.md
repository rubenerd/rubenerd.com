---
title: "RFC: expansion options for the Commodore 128"
date: "2023-06-29T10:04:30+10:00"
abstract: "I love my EasyFlash, but it’s not 128 compatible."
year: "2023"
category: Hardware
tag:
- commodore
- commodore-128
- commodore-16
- commodore-64
- commodore-64c
- commodore-plus4
- retrocomputing
location: Sydney
---
My beautiful shortboard Commodore 64C has been my main 8-bit computer of late, owing to the plethora of awesome hardware expansions and options available for it. Also because my 264-family machines are in various stages of repair right now; a topic for another time. But the Commodore 128 is my favourite 8-bit computer of all time, and I'd like to use it for more things.

I've made more progress fixing its 80-column mode, and correcting some memory issues with some of the upper bank of ICs, which I'll write about in a future post. It involved finally buying an IC programmer! But for now I want to talk about how one could use a C128 the same way one uses a C64. Turns out, it's not as easy I thought.

<figure><p><img src="https://rubenerd.com/files/2023/bearcat-64c-c128@1x.jpg" alt="My 64C atop my C128, featuring Clara and a Bear Cat." style="width:500px;" srcset="https://rubenerd.com/files/2023/bearcat-64c-c128@2x.jpg 2x" /></p></figure>

The device I probably use the most on the 64C, save for the RetroTINK 2 video upscaler and converter, is the EasyFlash 3 cartridge. I love the retrocomputing experience, but I'm happy to cheat and run modern storage. It's *so damned awesome* that I admit to reaching for the 64C to do some tinkering that could be done on my other 8-bit machines. I hear people have similar feelings for the Kung Fu Flash cart, which I'm also eager to try.

The EasyFlash isn't just zippier than an SD2IEC and a FastLoader cart, but you can even load your own kernels without mods. I'm still somewhat adverse to modding machines if I can get away with it&mdash;all my machines even still have their RF modulator cans for no good reason&mdash;so this is a great way to try various things like JiffyDOS.

[Thomas ‘skoe’ Giesel: EasyFlash 3 introduction](https://skoe.de/easyflash/ef3intro/)

I knew it'd be silly to expect this to work on the Plus/4, but for some reason I assumed the EasyFlash 3 would work on the Commodore 128. *Turns out* this isn't the case, not even in the machine's 64 compatibility mode. The architecture of the C128 is sufficiently different that the specific low-level functions that cart needs aren't available.

It's been bittersweet finding all these awesome devices that don't work on the C128, if still understandable. I know I personally love the C128 the most, but there's no question the C64 is the cult favourite from the time. It had significantly greater marketshare, which means more people have nostalgia and interest for it today.

Still! I'm interested to hear if anyone knows of any cool non-destructive C128 expansion options like this, especially for storage. The 1571 disk drive is already so much nicer than the 1541 I use on the other Commodore machines, but having a USB interface to dump ROMs and images onto would be incredible. The improved performance, BASIC, and capabilities of the Plus/4 and C128 would be great to use in conjunction with an external loader of some sort.

