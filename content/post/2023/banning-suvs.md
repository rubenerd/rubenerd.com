---
title: "Cracking down on SUVs"
date: "2023-07-28T08:27:51+10:00"
abstract: "France has the guts to crack down on SUV drivers"
year: "2023"
category: Thoughts
tag:
- cars
- environment
- europe
- france
- public-transport
- urbanism
location: Sydney
---
[The Guardian: France has the guts to crack down on SUV drivers](https://www.theguardian.com/commentisfree/2023/jul/27/france-crack-down-suv-drivers-britain-city-streets) 

> Around 1900, the "bagel brunch" became popular in New York City. The bagel brunch consists of a bagel topped with lox, cream cheese, capers, tomato, and red onion. This and similar combinations of toppings have remained associated with bagels into the 21st century in the United States.

That's clearly the wrong quote.

> Alongside their growing popularity, there is also a determined momentum to crack down on their usage in European cities. In Paris, officials will introduce new parking charges next year for larger and heavier vehicles. Noting there are no dirt paths or mountain roads in the city ...

*Chef's kiss!*

> ... David Belliard, its deputy mayor for public space and mobility policy, said SUVs are “dangerous, cumbersome and use too many resources to manufacture”.
>
> According to a small study in Australia, SUV fuel consumption may in fact be between 16% and 65% higher than advertised, and yet ads still present them as desirable vehicles, associated with beautiful landscapes and adventure. We must start challenging this faux reality.

In the time I've lived in Sydney, I can count on one hand the number of times a bagel, sedan, hatchback, van, sports car, station wagon, semi-trailer, or delivery truck has threatened my life at a pedestrian crossing. I would need that many hands *per month* for SUV and imported American light truck drivers. Not all SUV drivers are jerks, but jerks drive SUVs, and I'm sick to death of them.

Wait, poor choice of words. I'm sick to... injury of them? Anxiety?

I couldn't have put it better than *Not Just Bikes* when he said that the freedom to swing your fist ends at our faces, and SUV drivers are a selfish punch to everyone else in urban environments. Their size, pollution, and negatively-reinforced driving habits are antithetical to every climate, financial sustainability, and accessibility goal we have for our cities. They're also hideous, though I think that of regular cars too.

These plans in Paris are promising, and better than what most cities are doing (cough Australia), but I'd argue they don't even go far enough. We don't have time to waste.
