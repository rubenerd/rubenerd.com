---
title: "Disable GUI on modern Debian"
date: "2023-06-07T19:13:47+10:00"
abstract: "sudo systemctl set-default multi-user.target"
year: "2023"
category: Software
tag:
- debian
- linux
- systemd
location: Sydney
---
This is how you disable the GUI after logging in and launching a shell:

    sudo systemctl set-default multi-user.target
    sudo systemctl reboot

I inadvertently installed a graphical environment installing a new Debian Xen test server. This will let you reboot to a tty.

For fellow BSD people, remember that the **systemctl(8)** command has also kudzu'd **shutdown(8)** on most Linux distros now. I wonder how long it'll take for **sudoedit(8)** to be replaced with **systemctl text-editor-edit**.
