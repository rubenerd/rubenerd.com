---
title: "The freemium model to support web “content”"
date: "2023-12-22T19:49:59+11:00"
year: "2023"
category: Internet
tag:
- advertising
- content
- ethics
location: Sydney
---
Mike Masnick [wrote an interesting post for TechDirt](https://www.techdirt.com/2023/12/21/error-402-information-wants-to-be-freemium/ "Error 402: Information Wants To Be… Freemium? ") about the history of paywalls, online advertising, and the race to find a viable business model to support people. He [quotes Fred Wilson](https://avc.com/2006/03/my_favorite_bus/) who observed in 2006:

> Give your service away for free, possibly ad supported but maybe not, acquire a lot of customers very efficiently through word of mouth, referral networks, organic search marketing, etc, then offer premium priced value added services or an enhanced version of your service to your customer base.

Mike expands:

> It’s not a golden [sic] bullet, but it remains a useful tool for many web services. Of course, web service is not the same as pure web content, and experiments in freemium for content are a more recent phenomena.

One word Fred didn't use in his analysis: *content*. By contrast, Mike uses the term eight times. Granted, Fred is talking about web services, and Mike is extrapolating this onto creative works more broadly. But the framing matters. Authors, painters, designers, programmers, and musicians now [merely create *content*](https://rubenerd.com/stop-calling-people-content-creators/); gristle to be chewed through for profit and AI training. 

If we want a healthy, vibrant web to flourish, we [need to move away 📺](https://www.youtube.com/watch?v=hAtbFwzZp6Y "Patrick H Willems: Everything Is Content Now") from reductive industry terms like this.

As for the freemium model though, I think it's a great idea; certainly better than most other things we've come up with. People who'd never pay can continue not doing so, and those who can... can.

