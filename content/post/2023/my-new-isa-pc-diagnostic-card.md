---
title: "My new ISA/PCI PC diagnostic card"
date: "2023-03-23T15:21:08+11:00"
abstract: "I’m going to use this to troubleshoot a 386SX board."
thumb: "https://rubenerd.com/files/2023/isa-pc-diagnostic-card@1x.jpg"
year: "2023"
category: Hardware
tag:
- 386-pc
- retrocomputing
location: Sydney
---
I've been sitting on a surface-mounted AMD 386-SX motherboard since 2002, and it's never powered up properly. Maybe if I *stood* on it, I'd have better success. Recently I decided to try fixing it with a known-good power supply and peripherals, but beyond ascertaining that it indeed powers on, I haven't got any futher.

I'm not sure which YouTuber I first saw using one of these cards, but I searched eBay and bought one. Here it is next to [Shinri from Holostars](https://www.youtube.com/@JosuijiShinri/streams), because the three of us are of a similar vintage :').

<figure><p><img src="https://rubenerd.com/files/2023/isa-pc-diagnostic-card@1x.jpg" alt="Photo of the diagnostic card showing the edges with PCI and ISA connectors, and an acrylic stand of Shinri." style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2023/isa-pc-diagnostic-card@2x.jpg" /></p></figure>

The card can be inserted into an ISA or PCI slot. It uses a series of LEDs to report on voltages, and a PC speaker with two seven-segment displays for reporting BIOS POST codes. These can help isolate faults to specific hardware components, or at least send you in the right direction.

*(I remember having a similar ISA-only card for this years ago; I think from Emerson or Yokogawa if you can believe it. It originally came in a large, flat box with other component testing equipment, but I ended up with that one device after an office move and ewaste clean out. It vanished to the same place as my Gundam and DS9/VOY science officer uniforms when we moved back to Australia. Boo, and as well as that, hiss).*

Reviews for these specific cards are mixed. Some listings show similar cards constructed out of hot glue and tape, with plenty of negative reviews pointing it out. My card is soldered near perfectly, and the components all look brand new. I suspect there's a reference design that's been copied *ad infinitum*, and some sellers cut costs. If you plan on buying one, pay close attention to the seller, and only buy from ones that show specific photos of completed devices. I find that's a good rule of thumb with any retrocomputing device.

My hope is I can use this card to troubleshoot that ancient 386, but also my Pentium 1 which has developed some od dates anoddly picky about which cards and DIMMs it supports. I'm starting to suspect it might be a power delivery issue, which this card can help diagnose.
