---
title: "The post-conference lull"
date: "2023-07-03T17:27:31+10:00"
abstract: "Ever get that sinking feeling after a huge event you were hyped for is finally over?"
year: "2023"
category: Thoughts
tag:
- asiabsdcon
- events
- smash
location: Sydney
---
Ever get that sinking feeling after a huge event you were hyped for is finally over? The worst for me has been returning from AsiaBSDCon each year, but this last weekend sure feels similar.

Over time you hold onto the fond memories and are left with positive thoughts, but in the immediate aftermath? It's crushing.

I suppose it's a way for your body to recognise just how exuberant you must have felt, because the difference is so stark. Had it been merely middling or average, you'd probably feel little to no difference.

I'm sure there's a lesson in there somewhere.
