---
title: "Electronic bookends"
date: "2023-08-21T10:02:06+10:00"
abstract: "I see tech through the lens of where I was and what I was thinking."
year: "2023"
category: Hardware
tag:
- personal
location: Sydney
---
No, this isn't a post about Microsoft Bookshelf, or another multimedia CD-ROM containing volumes of books on one shiny disc. Though I do think those are overdue for a revisit too.

Tech is interesting because it effects social and global change, but also affects us personally in ways that are completely unique. One way I find myself thinking about tech's impact on my own life is seeing their introduction as a series of bookends.

There's pre and post-words for all sorts of things. I can remember a time in my early childhood before I'd used a computer, then a time where our PC didn't have a CD-ROM, then Internet access. Today's kids are as likely to consider the latter as integral a part of the experience of owning and using tech as I would have considered a colour display, despite operators in the past only owning orange phosphor terminals, or punch cards, or even banks of vacuum tubes.

What's been fun to rediscover recently was how much our physical location also dictated how I saw technical change. Our move overseas when I was a kid coincided with the widespread introduction of Internet connections in homes, for example. That timing really does a number on the nostalgia centre of my brain: for years I associated Asia with having always had Internet, and Australia being in the "before times". Based on the quality of some of our politicians of late, that might still be somewhat true.

It even applies to specific hardware generations. I still think of 486-era machines as "Melbourne PCs", because that's where I grew up when we first got them. Brisbane PCs were Pentium era, and Singapore is associated with blueberry iMacs. FreeBSD is a "Malaysian thing" in my head, because we were in KL when I finally started taking daemons in green shoes seriously.

Not sure what the point of this post was, I've just been thinking about this a lot lately.
