---
title: "Acoustic bikes and scooters"
date: "2023-12-18T08:29:18+11:00"
abstract: "How I started using the term, and the different types of scooters."
year: "2023"
category: Thoughts
tag:
- bikes
- language
- nomenclature
- scooters
- transport
location: Sydney
---
Jos Yule of *[The Organization](http://www.theorganization.net/)* (<a href="http://www.theorganization.net/feed.xml" title="Jos Yule's RSS feed">RSS feed</a>) asked:

> I was intrigued by the idea of “acoustic scooters”… are these special kinds that make specific noises? I’m reminded of that child’s toy that has the clear dome filled with balls and makes a satisfying click/pop sound as you drag it behind yourself.

It's kind of funny to think about, though maybe it should make an obnoxious rattling sound for safety reasons.

My use (abuse?) of the term *acoustic* is a tongue-in-cheek disambiguation, but it comes from family folklore. My dad was always into Ducati motorbikes, but we also mountain biked together. I got in the habit of asking whether he was taking the motorbike, or the *acoustic* bike on one of his trips. 12-year old Ruben thought his wordplay was rather clever, though it can't have been *that* original given the proliferation of the term thesedays.

We need better words for these things. The term *scooter* can refer to:

* **Motor scooters** or **mopeds**, like those from Piaggio.

* **Electric scooters**, which are [illegal to ride](https://www.transport.nsw.gov.au/roadsafety/road-users/e-scooters "NSW Government page on road safety") in public spaces in New South Wales. Don't get me started.

* **Skate scooters**, or **push scooters**. These are the ones I've had since I was a kid, and I'm trying to start riding on a regular basis again. They're *acoustic* on account of not having an engine, save for the organic food processor I inherited from my parents.

I've already had people ask me how fuel prices are affecting my use of a *scooter*, showing that maybe I need to be more specific. Unless they're referring to groceries.
