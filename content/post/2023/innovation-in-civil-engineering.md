---
title: "Innovation in civil engineering"
date: "2023-06-10T13:18:51+10:00"
abstract: "Gaining confidence in a design has as much to do with engineering theory, as it does to seeing how well similar designs performed."
year: "2023"
category: Hardware
tag:
- design
- engineering
location: Sydney
---
I quoted this from a video I watched in 2021, and promptly forgot where I saw it. I'm pulling it out of the drafts now because its still great. You could apply the same thinking to file systems.

> Innovation happens slowly in civil engineering, because the consequences of failure are so high. Gaining confidence in a design has as much to do with engineering theory, as it does to simply seeing how well similar designs have performed in the past. 

[It could be Practical Engineering?](https://www.youtube.com/@PracticalEngineeringChannel)
