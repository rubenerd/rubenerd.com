---
title: "Steam cleaning chewing gum"
date: "2023-08-24T08:40:10+10:00"
abstract: "Watching this contraption from inside this coffe shop was interesting."
thumb: "https://rubenerd.com/files/2023/steam-cleaner@1x.jpg"
year: "2023"
category: Hardware
tag:
- cleaning
- economics
- externalities
- singapore
location: Sydney
---
Shortly after sitting down at one of my local coffee shops this morning, I heard the loud sound of a compressor, followed by clouds of steam. I looked out the window and saw this contraption:

<figure><p><img src="https://rubenerd.com/files/2023/steam-cleaner@1x.jpg" alt="Photo of some industrial cleaning equipment outside a coffee shop, with a sign saying: Caution Steam Cleaning in Progress." style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2023/steam-cleaner@2x.jpg 2x" /></p></figure>

This is a high-pressure steam washer, designed to remove tough grime from surfaces like concrete and tiles. I remember we had these at a plant in Singapore for removing spills, albeit at a larger scale.

What this machine *wouldn't* have been used for over there is chewing gum! Here's the article on Wikipedia if you're not familiar with the backstory here:

[Chewing gum sales ban in Singapore](https://en.wikipedia.org/wiki/Chewing_gum_sales_ban_in_Singapore)

Watching this professional from inside is interesting. Rather than a nozzle you'd see on a water powerwasher, the end of the hose has a small transparent cup with a line of rubber along the edge, which lets him seal the hose around chewing gum to lift or dissolve it (I'm not sure which). It takes a solid few minutes of gentle swirling, but the end result is the gum is completely removed.

I remember chewing gum being used to explain economic externalities when I was a kid. The person chewing that gum and spitting it out is guilty of the act of being selfish and disgusting, but they don't pay the cost in gummed up shoes or cleanup. This is why we all pay council rates (directly or otherwise), so someone qualified can come in and spend minutes with expensive equipment doing what they could have done in five seconds if they disposed of their gum in a bin.
