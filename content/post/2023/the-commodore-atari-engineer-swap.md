---
title: "The great Commodore/Atari engineer swap"
date: "2023-01-11T08:53:01+11:00"
abstract: "Jack Tramiel really had his fingerprints all over both companies."
year: "2022"
category: Hardware
tag:
- atari
- commodore
location: Sydney
---
Spend any time reading about the history of 1980s home computers, and you'll learn about Jack Trameil's famous departure from Commodore to Atari, two of the biggest names in the industry at the time. What I didn't realise was just how much engineering talent and design ended up being swapped between the two companies.

Dave Farquhar has a [great article](https://dfarq.homeip.net/atari-st-vs-amiga/) exploring the history:

> In some ways, it felt like the legitimate successors of two of the most influential 8-bit computers of the 1980s ended up being traded for one another. The ST resembled the C-64 more than it resembled the 800, and the Amiga resembled the 800 more than it resembled the 64. The Amiga was the Atariest Commodore ever, and the ST was the Commodoriest Atari ever.

And a bit of history about how Amiga played into this:

> Amiga secured a short-term $500,000 loan from Atari in March 1984, just before the Tramiel purchase. Part of the agreement would allow Atari to use the Amiga chips in a new computer, which would be named the Atari 1850XLD. Amiga backed out of the agreement at the end of June when enough money to repay the loan, plus interest, and to terminate the contract turned up. The benefactor was Commodore, who provided the money to buy time to negotiate a win-win deal. Then in August 1984, Commodore purchased Amiga for $27 million.
> 
> The result was by August 1984, key engineers from both companies had changed sides and the two companies had traded lawsuits.

I've always loved Ira Velinsky's industrial design of the Atari ST. It seems weird to think it has ex-Commodore fingerprints on it; but then the whole history of these companies and product lines are bizarre.

I have a 1040ST and an Amiga 600 on the proverbial wishlist, but suffice to say space and finances won't be forthcoming for a while!
