---
title: "Three years on from Brexit"
date: "2023-05-03T08:37:49+10:00"
abstract: "Volultarily imposing sanctions on oneself, yay!"
year: "2023"
category: Thoughts
tag:
- politics
- united-kingdom
location: Sydney
---
*(This post was written back in January, but I forgot to post it).*

It's the three-year anniversary of Brexit, when Britain acted upon a referendum to impose sanctions upon itself. It wasn't unanimous; Northern Ireland, Greater London, Scotland, and Gibraltar conspicuously voted to remain, as did a disproportionate number of young people who had the most future to lose.

Funny how that always happens!

It was surreal seeing it play out from outside the UK. Here was a country voluntarily leaving one of the largest and most prosperous economic blocs in the world, and one in which it held significant sway and interest. British exporters would still be beholden to EU law, they just wouldn't have a say in shaping its direction. UK wasn't ever in the Shengen Area, but they still surrendered their preferential treatment... as Tory tourists discovered when visiting their Spanish holiday villas.

We only saw snippets of the debate here, but it seemed to come down to the well-worn formula of immigration, nationalism, and lies about economic benefits. Transparently-empty promises to fund the NHS printed on buses, the removal of red tape (somehow), and other populist rhetoric frothed people up into a fervour of independence, as though the UK needed to get it back from someone.

My pet theory is that a certain segment of British society were secretly jealous of America's 4th of July, or France's Bastille Day. They wanted to remain slavishly loyal to their unelected monarch who's head remains firmly attached, yet they want to bask in the glory of having won out over someone.

W-wait, Nicolas Cage, what are you doing here?

> I'm here to steal the UK's Declaration of Independence... *innit?*

Three years on, and I'm sure its cold comfort that the predictions remainers made are all coming to pass. The good news is more leavers are realising they were used and discarded... though what they do armed with that knowledge at the ballot box we'll have to see.

