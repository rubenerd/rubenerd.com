---
title: "Creativity under legacy constraints"
date: "2023-08-12T09:39:22+10:00"
abstract: "There's a unique kind of creativity that comes from working within a set of limitations."
year: "2023"
category: Media
tag:
- art
- creativity
- games
- music
location: Sydney
---
There's a unique kind of creativity that comes from working within a set of limitations or constraints. I know I have more fun when I don't use cheats, and try out old things with a new twist. What's the smallest financially viable SimCity 3000 town I can build? What are the fewest number of lines I can write a Sudoku game in with Pascal, or Perl?

The 8-bit demo scene is one of my favourite modern examples. I remember an artist saying they pitied software developers for not being able to serenade with a guitar (because nerds can't also play music?), but the fact these people are able to meld a complex and deep understanding of assembler and SID development *with* art and music is incredible. I have mad respect for people who can command both hemispheres of their brain with such precision and flare.

But you don't have to go niche to see this in action. Have you ever noticed how many albums (rememeber those!?) can be cleanly split in half, and that musicians tend to end albums on a quiet note? These have become how we expect albums to be structured, but they come from the limitations of vinyl. Louder and more complicated songs were placed at the start, where the additional surface area of a disk spinning at constant angular velocity would offer higher fidelity. Tracks also had to fit within the confides of a double-sided cassette tape or LP, so you couldn't have a long track in the middle.

I'm sure there are a *bunch* of examples like this.
