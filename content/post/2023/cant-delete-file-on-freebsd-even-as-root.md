---
title: "Can’t delete file on FreeBSD, even as root"
date: "2023-11-25T08:01:07+11:00"
abstract: "Check you don’t have an immutable flag set."
year: "2023"
category: Software
tag:
- bsd
- freebsd
- guides
- things-you-already-know-unless-you-dont
location: Sydney
---
Today's installment of *[Things You Already Know, Unless You Don't](https://rubenerd.com/tag/things-you-already-know-unless-you-dont/)* comes from a client issue I helped troubleshoot this morning. They tried to delete a file on a FreeBSD server, but even as root it didn't work:

	# rm file
	==> rm: file: Operation not permitted

This can throw people new to FreeBSD, because the file permissions may otherwise indicate that root can delete the file. The problem comes from it having an additional `schg` immutable flag set:

	# ls -lo file
	==> -rwxr-xr-x 1 root root file schg,uarch [...]

Files usually have immutable flags set for a reason. **Check, double check, and then triple check before deleting a file with this set.** To unset:

	# chflags noschg $FILE

And now the file can be deleted, though it probably shouldn't be. Consult the [file flags](https://docs.freebsd.org/en/books/handbook/security/#security-file-flags "FreeBSD Handbook: Security: File Flags") section of the FreeBSD Handbook for more information.

I use these flags on specific system files to prevent tampering, and on personal systems to prevent butterfingering. 

Linux implements an equivilent system with [chattr(1)](https://man.freebsd.org/cgi/man.cgi?query=chattr&manpath=Debian+8.1.0 "Manpage from Debian on the use of chattr"), not to be confused with a Web 2.0 social media site.
