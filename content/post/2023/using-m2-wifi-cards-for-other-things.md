---
title: "Using M2 Wi-Fi card slots for other things"
date: "2023-06-02T08:34:20+10:00"
abstract: "This small slot can be used for other PCIe devices, not just Wi-Fi."
thumb: "https://rubenerd.com/files/2023/m2-ethernet@2x.jpg"
year: "2023"
category: Hardware
tag:
- homelab
- networking
location: Sydney
---
The first thing I plug into any desktop, even before power, is wired Ethernet. It's always fun (cough) routing around furniture in a new apartment, and figuring out how you'll get *there* from *here*, where *there* is your router box, and *here* is yours and your partners' desk. I heard you liked large rugs that *definitely* don't have a suspicious, cable-shaped bulge running down one side.

Modern motherboards tend to include Wi-Fi modules that expose antenna sockets on their IO shields. I only learned recently you could unplug these modules, exposing a standard M.2 slot with A+E keys on their edge connectors!

[Delock: The M.2 interface](https://www.delock.de/infothek/M.2/M.2_e.html)

Why is this **awesome**? For rational people, it isn't. But for masochistic Mini-ITX builders such as myself, our boards are limited to a single PCI-express slot. Gaining an additional M.2 slot opens up a rare extra possibility for internal expansion that, for once, doesn't involve using dodgy internal USB connections.

So the next question is, if you don't need Wi-Fi, what can you use the slot for? According to the Delock website, A+E maxes out at two PCIe lanes, which limits its utility for NVMe SSDs. But this is ample for a bunch of peripherals.

Checking eBay, the most common expansion is an additional Ethernet port, which might be useful if you have a budget board. 2.5G Ethernet modules are even available, which is presumably the highest bandwidth version you can get on the limited lanes available. Though I wonder why so few of them come with PCI slot brackets.

<figure><p><img src="https://rubenerd.com/files/2023/m2-ethernet@2x.jpg" alt="Photo showing a 2.5G Ethernet jack connected to an m.2 A+E connector." style="width:184px;" /></p></figure>

There are adaptors for mSATA storage, SD cards, microSD cards, SIM cards, and Magic cards... if you stored images of them on SD cards. There are also USB adaptors, though Mini-ITX boards tend to come with plenty of these already because the PCIe lanes aren't allocated to many things.

Which lead me to wonder something awful: could I  expose the PCIe lanes for general purpose expansion cards? Not to get all Malcolm Gladwell on you, but *turns out*, you can! This was the first one I found: 

<figure><p><img src="https://rubenerd.com/files/2023/m2-slot-usb@2x.jpg" alt="Photo showing a 16x PCIe slot connected to a USB header on the m.2 slot." style="width:184px;" /></p></figure>

Upon closer inspection, this passes PCIe over USB. I could do this with regular USB ports. I suspect this was designed for miners, and not the good kind who build things with blocks.

Refining my search to "Wi-Fi to PCIe riser" lead to a few variations on this cable:

<figure><p><img src="https://rubenerd.com/files/2023/m2-slot-straight@2x.jpg" alt="Photo showing an M.2 adaptor connected with a ribbon cable to a PCIe 16x slot." style="width:300px;" /></p></figure>

It's comical to me that these have PCIe 16x connectors, given that A+E keyed M.2 slots are missing 14 lanes. But either way, we have a PCIe slot that we could use for what we wanted.

I'm thinking of using it for a proper sound card, because internal motherboard audio is always dreadful. My Fractal Ridge has a spare PCI slot, so I'm thinking this terrible contraption could work! Maybe.
