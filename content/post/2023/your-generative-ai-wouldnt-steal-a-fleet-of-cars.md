---
title: "Your generative AI wouldn’t steal a fleet of cars"
date: "2023-07-26T08:22:09+10:00"
abstract: "I hold our industry to a higher ethical standard."
thumb: "https://rubenerd.com/files/2023/you-wouldnt-steal-hubris@1x.jpg"
year: "2023"
category: Internet
tag:
- ai
- copyright
- ethics
location: Sydney
---
Remember that early-2000s ad campaign that equated downloading from peer-to-peer networks with car theft? The one that featured a stolen soundtrack? The shoe is on the other foot today, with large AI companies hoovering up yours and my data for their profit.

<figure><p><img src="https://rubenerd.com/files/2023/you-wouldnt-steal-hubris@1x.jpg" alt="You wouldn't steal a car!" srcset="https://rubenerd.com/files/2023/you-wouldnt-steal-hubris@2x.jpg 2x" /></p></figure>

Granted, generative AI's use of our data without permission, notice, attribution, licencing, or other compensation isn't the same as Aunt Mabel downloading an MP3. For one, she isn't selling unlicenced mix tapes. The sheer scale of AI also puts them in a class of their own.

I've never had an Aunt Mabel, but if she's anything like my German aunt Ilse, I'll bet she's lovely and makes amazing cakes.

Advocates will claim training generative AIs is more a form of *learning*, no different from a painter taking inspiration from trillions of images they farmed from the Internet, or an author reading a few million books in an afternoon. This is mathematically false of course; see how quickly this "learning" falls apart when a feedback loop is introduced. This is entropy 101 stuff: an artist's creations are additive, an AI can only be deriviative.

I'm glad to see more people talking about this, because *a rule for thee, but not for me* is ridiculous. I hold our industry to a higher standard.
