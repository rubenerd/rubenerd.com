---
title: "Shareholder-in-charge"
date: "2023-08-09T12:40:17+10:00"
abstract: "Who the heck is that!?"
year: "2023"
category: Thoughts
tag:
- spam
location: Sydney
---
I got some spam earlier this week from a vendor inviting me to their tech conference in the US. Fronting this event was the company's *shareholder-in-chief*, who'll be giving a keynote presentation about nuanced, hyperdisruptive paradigm synergies, or something.

Wait... what?

No, not the venture-capital Valley speak, the person's title. What's a *shareholder-in-chief?* Does every business have one? Could I become one, if I had nothing else going on? As an accounting hobbyist, and someone  (perhaps morbidly) curious about corporate structures and management, I couldn't help but be intruiged. 

What could it mean? Taking a stab in the dark, I might hit someone. In keeping with that hostile theme though, it sounds like caretakers appointing a company's largest shareholder after the dismissal of a managing director or board. *Who else do we have to run the business, Janet? I dunno, Bernard has massive amounts of stock for some reason, let's get him!*

But that can't be it, surely? Is it the most influential shareholder who's also on the board? But then wouldn't you still refer to them as a board member over a shareholder? I'm a shareholder in a bunch of companies, but that doesn't really confer much of anything.

I did a bunch of web searches for *shareholder-in-charge*, but all I got were company profiles from people who have that title. If anyone knows what someone in that role does, and how they go it, do get in touch.
