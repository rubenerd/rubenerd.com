---
title: "Exploring a separated cycle path in St Leonards"
date: "2023-12-17T18:48:40+11:00"
abstract: "It’s gorgeous!"
thumb: "https://rubenerd.com/files/2023/bike-path-artarmon@1x.jpg"
year: "2023"
category: Travel
tag:
- australia
- commuting
- cycling
- hiking
- scooters
- sydney
- urbanism
location: Sydney
---
Clara and I found a [cool linear park near Artarmon station](https://rubenerd.com/linear-parks-along-sydney-train-lines/ "Linear parks along Sydney train lines") in Sydney back in November. Our plan is to figure out a nice chain of trails between where we live and the city, to help us be more active and either cycle, scoot, or walk longer distances. The aim is to supplement our commute, and eventually replace it entirely.

This weekend we discovered another stretch, this time snaking from Artarmon to St Leonards. It's entirely grade separated, and while it does run alongside an ugly motorway for a few hundred metres, it's otherwise completely surrounded by bush and parkland. It's gorgeous!

<figure><p><img src="https://rubenerd.com/files/2023/bike-path-artarmon@1x.jpg" alt="Photo showing the entrance to the sheltered green cycleway near Artarmon." srcset="https://rubenerd.com/files/2023/bike-path-artarmon@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

We've walked this route a couple of times now, but we can't wait to try it on acoustic scooters.
