---
title: "Dutch milk bread and spreads"
date: "2023-06-01T14:34:12+10:00"
abstract: "Clara and I are going through a bit of a Dutch obsession, and this bread is amazing."
thumb: "https://rubenerd.com/files/2023/milk-bread@1x.jpg"
year: "2023"
category: Thoughts
tag:
- bread
- europe
- food
- netherlands
- wouter
location: Sydney
---
Clara and I are going through a bit of a Dutch obsession at the moment, from art, infrastructure, language, history, and food. This has only been reinforced from watching channels like Not Just Bikes, and from conversing with the few lovely Dutch readers among you who've found my ramblings and have reached out to say *hallo*. It's a shame that my last name is just as unfortunate in Dutch as it is in German, though arguably it's just as fitting.

*Schhhhhhhhhhhhhhhhhhhhiphol Airport!* 

...

So when we saw a local Sydney bakery started selling fresh Dutch milk *brood* loaves, we made like Mondrian and squared some away in our reusable shopping bag. I have no idea if Mondrian liked bread, or if he even liked milk bread, I just couldn't help but alliterate pointlessly. Also, get it, *squared* away? Because he painted... shaddup.

<figure><p><img src="https://rubenerd.com/files/2023/milk-bread@1x.jpg" alt="A photo of a fresh-baked loaf of Dutch milk bread, with its steep height and crusty... crust." style="width:500px; height:375px;" srcset="https://rubenerd.com/files/2023/milk-bread@2x.jpg 2x" /></p></figure>

I'm not sure what specifically makes this style of bread Dutch, though I do remember buying it back in the day from a Dutch bakery and cafe in Singapore growing up. *Proofing*, if you will. Wouter would probably consider Belgian bread better; they do make the world's best chocolate and beer after all.

[Wouter: Brain Baking!](https://brainbaking.com/)

In my experience, Dutch milk bread is crustier than equivalent loaves from French or Japanese bakeries, but the inside retains that impossibly-soft texture. That contrast surfaces even more with a very light toasting, and either Vegemite or De Ruijter Chocoladehagel. While their colour may appear superficially similar upon application, you'd best not get them confused.

Like alcohol, Clara and I limit our intake of processed carbohydrates where possible, so when we *do* eat carbs, we want it to be special. And wow, this bread sure is. I'd happy eat this whole loaf, right now, with nothing even on it. Would I regret it later? Almost certainly. Would I let it stop me? Almost certainly. But can a man dream? Maybe on a small urban canal he could, with a loaf of bread, a stroopwafel perched on his coffee cup, and a wry smile. A rye smile?

*Lekker!* 🍞

