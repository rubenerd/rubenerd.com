---
title: "When you write for yourself…"
date: "2023-11-05T17:31:25+11:00"
abstract: "… you only have yourself to blame!"
year: "2023"
category: Thoughts
tag:
- indieweb
- writing
- weblog
location: Sydney
---
... you only have yourself to blame!

I originally intended this to refer to technical stuff. We only have ourselves to blame if our self-hosted sites go down. We're not beholden to the whims and pressures of a social network, or site operator anywhere else.

*(This site has been around for almost two decades now, longer than most social networks. Some schmuck in APAC can maintain a site longer than a team of hundreds, or thousands. Yes I know my audience is orders of magnitude smaller, but still).*

But I guess this also applies to style, cadence, and topics too. I could put anything I want here, short of anything outright illegal or libelous.

Is all this obvious? Probably. Was just something rattling around in my head this afternoon. ⛅️
