---
title: "Site updates: Omake, and no more post abstracts"
date: "2023-12-22T18:27:23+11:00"
year: "2023"
category: Internet
tag:
- metadata
- omake
- xml
location: Sydney
---
A bit of housekeeping:

* I've decided to stop writing summaries/abstracts for posts. I don't think they were that useful, and I'd stopped exposing them via description metadata a while ago. Instead, I'm going to focus on writing better titles.

* My [Omake](https://rubenerd.com/omake.xml) page has been migrated from OPML to my [omake XML format](https://rubenerd.com/xmlns-omake/). The blogroll remains OPML. It's not anything special, but maybe I'll write about it in the future if people are interested.

* Lists look a bit barren with just two items. Consider this padding, or *stuffing*, as the season may warrant.
