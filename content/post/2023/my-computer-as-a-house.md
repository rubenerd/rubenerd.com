---
title: "My computer as a house"
date: "2023-10-01T10:06:44+11:00"
abstract: "“I visit this house every day and know most of the rooms on the ground floor, but there are nooks and crannies I’ve never explored”"
year: "2023"
category: Thoughts
tag:
- bsd
- freebsd
- home
- open-source
location: Sydney
---
[Sinclair Target](https://twobithistory.org/2019/08/22/readline.html)

> I sometimes think of my computer as a very large house. I visit this house every day and know most of the rooms on the ground floor, but there are bedrooms I’ve never been in, closets I haven’t opened, nooks and crannies that I’ve never explored. I feel compelled to learn more about my computer the same way anyone would feel compelled to see a room they had never visited in their own home.

*Beautiful!* This has oddly given me more motivation to go poking around my electronic attic than anything has in a while. Maybe there's a hidden door behind the wardrobe as well; and not just because I have Microsoft Bob running in Wine.

It also makes me think of rooms that companies have decided I'm not allowed to enter. FreeBSD is such an easy-going landlord.
