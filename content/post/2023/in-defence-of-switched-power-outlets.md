---
title: "In defence of power points with switches"
date: "2023-10-13T14:55:14+11:00"
abstract: "They’re better for accessibility and safety."
thumb: "https://rubenerd.com/files/2023/techconnectify-plugs@1x.jpg"
year: "2023"
category: Hardware
tag:
- accessibility
- electricity
- electronics
- safety
- technology-connections
location: Sydney
---
When I got my first external Iomega Zip drive as a kid, I was confounded by the instructions:

> If you'll be away for an extended period, unplug your Zip drive.

I remember thinking why I'd bother, when I could just flick a switch at the power point instead. Little did I know at the time, but this was another example of a weird cultural difference between where I grew up, and in this case the US where the Zip drive was designed.


### Technology Connections didn't with switches

Alec from *Technology Connections* isn't afraid to voice his opinions, no matter how contentious or controversial. It's easy when you so regularly have facts, good taste, and humour. But in a rare slip for the esteemed videographer, he expressed confusion over [power point switches](https://www.youtube.com/watch?v=2DGqVbTHX-k), emphasis added:

> ...plugs with switches *on them*, like you might find in the UK and Australia, or probably some other places [are] essentially *never* a thing [in North America]. And when it is, it's definitely a weird, special case.
>
> I *was* going to go on a rant here for the third time that this isn't an issue at all, and I don't get why those of you why an outlet **needs a switch**. Especially when **you think it adds a layer of safety**, because *the switch is right next to the plug* which you could also **just take out**.

There are a couple of misconceptions here, probably stemming from a lack of experience using them. I empathise; I was just as flummoxed when I first travelled to Japan and the US, and didn't see switches. We live so much of our lives on autopilot; I think it's fun being challenged in mundane ways like this.

But let's tackle his claims here.

<figure><p><img src="https://rubenerd.com/files/2023/techconnectify-plugs@1x.jpg" alt="" srcset="https://rubenerd.com/files/2023/techconnectify-plugs@2x.jpg" /></p></figure>

### Do power points *need* better accessibility?

Alec is right, power outlets don't *need* switches. All they need are live and neutral connectors to complete an electrical circuit. Except, humans are an integral and necessary part of the system too. We'll get back to safety in a moment.

I think Alec is passing judgement on the *function* of power point switches. Specifically, he claims that switches aren't necessary because you can just unplug the connector. This is akin to arguing electronics don't need off buttons because you can just pop out the batteries, or that a bike can be stopped with a wall instead of brakes. It's true, but misses the point.

There are a couple of accessibility concerns here. The first is that Alec's "just" can be turned on its head. Why should I have to yank out a cable (which even Alec admits can be frustrating and dangerous) when I can *just* flick a switch? The difference is perhaps less stark with flimsy North American connectors, but even then, I'd say a switch is easier.

Switches are especially helpful for people with arthritis, or those who shake and struggle to align connectors. They're also much safer for people to feel around if they have limited vision, or in situations where a bulb has blown. It's why new apartment buildings mandate accessible toilets, even if some of us don't use wheelchairs or walking sticks. A rising tide lifts all boats.

That reason alone is justification for their widespread use, but switched power points also act as a plug retainer. An unplugged cable might fall behind a couch, or disappear in a mess of wires. When you have a switch on the power point itself, you can keep the device plugged in but unpowered. It's the kind of feature you don't realise is useful until you move somewhere that doesn't have it. Sure, the device itself might have a power switch, but why not have one right next to the plug too?

As I eluded to above with terms like *yank* above (heavens), placing switches on power points can also help extend the life of the socket, plugs, and cords. Flicking a switch is less mechanically taxing for people *and* hardware. 


### Adding another layer of safety is a good thing

By now you might see why I think switches on power points are primarily useful for accessibility. But Alec raised safety, so it's worth mentioning.

Alec lamented the fact that we "think it adds a layer of safety". We do, because it's true. These aren't needed either, yet we decided they were good ideas for the same reason:

* safety shutters
* tamper resistance
* integrated fuses
* wiring into a circuit breaker panel or GFCI 
* non-conductive wall plates

Alec did a great video on his second channel [talking about safety shutters](https://www.youtube.com/watch?v=pmKL3pgPQhY "Tamper-resistant outlets aren't all the same - a follow-up"), which is worth checking out. Their implementation may not be perfect, but they're a step in the right direction. Just as adding switches would be.

As I said above, maybe some of Alec's confusion comes from not growing up where they're ubiquitous. We're taught as kids to turn off devices when they're not in use, which includes turning off power point switches. Not everyone does this; in which case, those plugs are no less safe than a standard US or Japanese plug without a switch. Otherwise, they're another layer of protection.

In an ironic twist, North American-style plugs are the ones that would benefit the most from switches! Modern European plugs have the benefit of being recessed, which hides the live pins. North American and Japanese plugs can be hanging out of the wall while still live, due to a lack of pin sheathing and bad mechanical design. A power point with an off switch would plug another hole in that Swiss Cheese.


### Counterpoint: confusion

There's only one major flaw with power point switches, and it's when electrical designers try and get creative, or attempt to reduce cost by merging disparate functions onto the one plate. Check out this absolute monstrosity:

<p><img src="https://rubenerd.com/files/2021/lightswitch@1x.jpg" srcset="https://rubenerd.com/files/2021/lightswitch@1x.jpg 1x, https://rubenerd.com/files/2021/lightswitch@2x.jpg 2x" alt="Power point on our bathroom wall, showing two appliances plugged in, and an additional switch mounted lower on the bracket." style="width:500px" /></p>

I wrote a [full post about this in 2021](https://rubenerd.com/bathroom-design-affordances/ "Bathroom design affordances"), but in short, we have two switches for the outlets, and a centre one for the bathroom light. The light switch is difficult to access when the plugs are populated, negating the captive benefit of such plugs I mentioned above. The switch also feels the same in the dark as the power switches, so I'm constantly turning off my toothbrush and/or electric razor chargers by mistake.

Switches are designed to make power points safer and more accessible, and **this plug managed to negate both!** If we owned this apartment, I'd be pulling out this socket faster than you can say "did I just blow our RCD?"


### Conclusion

Power point switches are a great idea, and I advocate for their use.

While I might disagree with Alec on this point, I'm actually *happy* that the broader conversation is being had. Safety and accessibility are important, and its great that opinions on what might have been considered niche topics are getting such a wide audience. This is how we make things better for everyone.
