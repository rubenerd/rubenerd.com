---
title: "A short, sharp storm"
date: "2023-12-27T14:43:45+11:00"
thumb: "https://rubenerd.com/files/2023/short-storm@1x.jpg"
year: "2023"
category: Thoughts
tag:
- australia
- weather
location: Sydney
---
I was sitting at a local coffee shop doing some work, like a gentleman, when the most sudden storm of my life hit outside! It went from tranquil to knocking over outdoor umbrellas in a matter of seconds.

<figure><p><img src="https://rubenerd.com/files/2023/short-storm@1x.jpg" alt="" srcset="https://rubenerd.com/files/2023/short-storm@2x.jpg 2x" style="width:500px; height:281px;" /></p></figure>

It's hard to believe with the cold wind and rain that it's the *middle of summer* in Sydney right now. It felt like a Hong Kong monsoon for a solid 20 minutes.
