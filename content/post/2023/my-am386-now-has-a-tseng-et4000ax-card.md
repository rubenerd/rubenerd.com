---
title: "My Am386 now has a Tseng ET4000AX card!"
date: "2023-06-09T15:13:13+10:00"
abstract: "Another card ticked off the list for this machine!"
thumb: "https://rubenerd.com/files/2023/tseng-et4000ax@1x.jpg"
year: "2023"
category: Hardware
tag:
- am386
- retrocomputing
location: Sydney
---
When I got my Am386 motherboard working earlier this year, I wed it to an Oak OTI-067, the only ISA graphics card I had. It worked surprisingly well, and its second connector even supported CGA, which assisted troubleshooting my Commodore 128's 80-column mode. But my dream was to find a Tseng Labs ET4000, the king of SVGA on the ISA bus.

[View all posts in this series](https://rubenerd.com/tag/am386/)

Well someone local I've bought from before listed theirs, and for less than half our grocery bill last week (yikes), so I pounced. It arrived yesterday, and here it is:

<figure><p><img src="https://rubenerd.com/files/2023/tseng-et4000ax@1x.jpg" alt="Top down view of the ISA Tseng card" style="width:500px;" srcset="https://rubenerd.com/files/2023/tseng-et4000ax@2x.jpg 2x" /></p></figure>

I know I said this on my recent Adaptec SCSI card post, *but isn't this beautiful?* Look at the whopping 1 MiB of user-replaceable memory spread across eight socket,the original 1993 manufacturer sticker on the IC in the top right, and the large TSENG IC with their epic logo. Aside from a little dust, the scuffed silver sticker on the VGA BIOS is the only part of the card that isn't immaculate; even the bracket looks brand new.

This is the **Tseng ET4000AX 9201 REV A1** (gesundheit), the best card the company made for the ISA bus. Tseng Labs were famous for their excellent SVGA graphics cards in the 1990s, though they weren't able to compete with later cards and were eventually absorbed into ATI, now AMD. It has full driver support for GEM, Windows 3.0 MME, and OS/2, which I want to run on this 386 alongside interesting things like Minix if I can.

But I had another, far less rational reason for wanting this card too. My dad found the purchase order for our original family PC a few years ago, which listed this exact card as an optional upgrade. It sounds silly, but having the money now to buy this card feels like I'm closing the loop on something from my childhood. I could have gone for any number of cards in this old machine, but *why not* the one we couldn't get at the time?

<figure><p><img src="https://rubenerd.com/files/2023/tseng-ic@1x.jpg" alt="" style="width:500px;" srcset="https://rubenerd.com/files/2023/tseng-ic@2x.jpg 2x" /></p></figure>

In a future post I'll run some benchmarks and compare how it renders various graphical environments.
