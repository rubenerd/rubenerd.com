---
title: "People who need cash being left behind"
date: "2023-12-09T10:41:42+11:00"
abstract: "Cash is rapidly disappearing, but some people still need it."
thumb: ""
year: "2023"
category: Ethics
tag:
- accessibility
location: Sydney
---
[Jonathan Barrett and Jordyn Beazley in The Guardian](https://www.theguardian.com/australia-news/2023/dec/09/my-whole-world-revolves-around-cash-why-some-australians-fear-being-left-behind-by-a-cashless-future)\:

> Future access to physical cash is now under a cloud, according to Australia’s primary cash transit company, amid a sharp decline in the use of notes and coins [...] which experts say risks isolating those who rely on [cash].
>
> [...] actual cash usage has dropped to just 13% of transactions, according to professional services firm Accenture, down from 27% before the pandemic.
>
> Australians are early adopters of payments technology and among the least likely in the world to transact in cash, alongside those in the Nordic countries.

Wait, 13% of transactions? I would not have guessed that little, not even close.

There are issues of digital fraud and tracking here worth considering. But the point raised in this article is one of accessibility. There's a real risk that those whom depend on cash may find themselves unable to live and work in our economy, or only with extreme difficulty.

I also see this across other digital services more broadly. You and I can submit a rental application, open a bank account, transfer funds, buy goods, and send our taxes online with very little effort (well, depending on your jurisdiction). What if you don't have access to a computer? Or didn't grow up using them?

Millennials like me were arguably part of the last generation to live in a world where computers were still seen as luxury, optional items. This isn't the case anymore; even smartphones are needed for an increasing number of tasks.

This shift might be inevitable, or required, or even preferred. But the point is we're leaving some people out, and that's a huge issue.
