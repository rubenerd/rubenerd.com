---
title: "Daylight savings in Sydney (AEDT)"
date: "2023-10-01T09:47:18+11:00"
abstract: "I wish it were this all year round."
year: "2023"
category: Thoughts
tag:
- adelaide
- south-australia
- australia
- melbourne
- new-south-wales
- sydney
- tasmania
- time
- victoria
location: Sydney
---
It just started for us in New South Wales, Victoria, and Tasmania this morning. It's 09:46 as I write this, but it feels like 08:46. South Australia also moved their clocks forward an hour for ACDT.

This is my favourite half of the year. I *hate* how early it gets dark in the middle of the year in temperate regions. More evening light makes it feel like you have more time to do stuff, even if it does fade the curtains faster (wink)!

I guess you can take the boy out of the tropics, but you can't take the tropics out of the boy.
