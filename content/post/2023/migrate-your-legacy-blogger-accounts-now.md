---
title: "Migrate your legacy Blogger accounts NOW!"
date: "2023-12-26T09:11:06+11:00"
year: "2023"
category: Internet
tag:
- blogging
- google
- weblog
location: Sydney
---
I only just noticed an email from Blogger earlier last month:

> Hi Ruben Schade,
> 
> This email address has a legacy Blogger account associated with it that hasn’t logged in since 2007. In 60 days it will lose access to the account and associated content; the data will be permanently deleted unless migrated to the Google Account system at Legacy migration page.

If you have an old Blogger account, migrate your posts off it NOW. Passing this on in case you missed it too.
