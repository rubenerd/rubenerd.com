---
title: "Stop calling people “content creators”"
date: "2023-10-12T08:26:32+11:00"
abstract: "Was Michaelangelo painting content?"
year: "2023"
category: Ethics
tag:
- content-creation
- language
location: Sydney
---
This is a phrase that's been getting under my skin of late. Summarising people's creativity, love, and energy as *content* sounds innocuous, but it's pretty cynical and demeaning if you think for more than a few seconds about it.

Was Michaelangelo making *content* as a *content creator?* Are the baristas at this coffee shop? What about the busker playing outside? Pardon, *generating* outside.

But I didn't make the connection to the [Grift Shift](https://rubenerd.com/the-grift-shift/) I talked about on Tuesday. As always, [Tante nails it](https://tante.cc/2023/09/21/the-age-of-the-grift-shift/ "The Age of the Grift Shift")\:

> And that is what is so sad about it. The grift shift and all the people and organizations who embody it are just a sign that we’ve all developed “morbus contentcreatoris” or “content creator brain”. And that is very depressing.
> 
> Because basically nobody doing anything interesting is “creating content”. The things we write, the paintings we draw, the music we play isn’t just about what the output is. Every creative activity is in itself a value, with the output sometimes being almost secondary. This article isn’t about the words I typed or even the process of typing them. It’s about me thinking through things I see in the world through the lens of my understanding of it, my values, my feelings and wishes. This isn’t content.

We've been down this road before. Developers pushed back on being labelled "programmers" for years, because conceptualising, designing, writing, iterating, and testing quality software is more than typing code. Yet artists are supposed to accept the equivalent term for themselves and their craft?

*Content* is a term designed to move the Overton Window, or to shift what's acceptable when discussing these topics. I now see it as a red flag.
