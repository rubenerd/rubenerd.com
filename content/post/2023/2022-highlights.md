---
title: "2022 blog h-highlights!"
date: "2023-01-01T10:31:04+11:00"
abstract: "Why have quality when you can have quantity?"
year: "2023"
category: Internet
tag:
- numbers
- weblog
location: Sydney
---
In 2022 my blog amassed:

* 159,791 words
* 548 new posts
* 6 cups of bought coffee; thank you!
* 5 fewer VMs; consolidated to FreeBSD jails
* 3 podcast episodes; not many, huh?
* 2 OPML outlines

Why have quality when you can have quantity? Wait.

As for highlights:

* In Australia, Dymocks is selling this handy [Twin Checker](https://www.dymocks.com.au/book/pentel-twin-checker-highlighter-yellow-light-green-4711577023069) in a fetching yellow and light green, double ended for convenience.

* In Singapore, Popular has the always attractive [Stabilo Swings](https://popularonline.com.sg/products/stabilo-swing-cool-highlighter-set-of-4-colours) in a handy four pack. I'm partial to the teal-ish blue.

If I can be selfish, I'd love for 2023 to be the year that *you* start a blog too, and for you to [tell me](https://rubenerd.com/about/#contact) about it. Your thoughts are more important than any social network, and I'd love having your RSS feed on my [FeedLand](http://my.feedland.org/Rubenerd). If you already blog, carry on ^^.
