---
title: "Typing on my new Gateron North Pole 2.0 keyboard"
date: "2023-06-23T16:49:00+10:00"
abstract: "These switches are ludicrously smooth, thocky, and quiet."
thumb: "https://rubenerd.com/files/2023/nk87-keyboard@1x.jpg"
year: "2023"
category: Hardware
tag:
- keyboards
location: Sydney
---
I've used mechanical keyboards for years, but this is the first one I've built from scratch. NovelKeys had a steep discount on their blue NK87 Entry Edition kit, so I figured why not! With the dual tone keycaps, I love how the colours harken back to my original blueberry iMac, and PCs of yore.

<figure><p><img src="https://rubenerd.com/files/2023/nk87-keyboard@1x.jpg" alt="Photo showing the teal blue keyboard and beige keycaps next to my blueberry iMac DV" style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2023/nk87-keyboard@2x.jpg 2x" /></p></figure>

After obsessively researching all the options, I went with Gateron North Pole 2.0 Yellow linear switches. I'd seen them start cropping up among the regular YouTube keyboard crowd, where they were ranked among the top switches for smoothness from the factory, and their deeper than average sound profile.

Gateron achieved this in a few interesting ways. They use the same housing and stem material as the company's beautiful mid-range Ink Blacks, and have an impressive lack of stem wobble, something I only learned to appreciate after using rattly MX Browns at work for years. They're factory lubed which a noob like me appreciates, and include a vented bottom which you really notice when bottoming out.

<figure><p><img src="https://rubenerd.com/files/2023/switch-north-pole-2@1x.jpg" alt="Photo showing a close-up of a single North Pole 2.0 switch." style="width:500px;" srcset="https://rubenerd.com/files/2023/switch-north-pole-2@2x.jpg 2x" /></p></figure>

Having used these in the NK87 for a week, I completely agree with assessment of those independent reviewers. These switches are *sublime*. They're the smoothest, thockiest switches I've ever used outside my beloved Topres, and their quiet, deep sound is an absolute joy, whether I'm writing documentation for a tender, or avoiding lava in Minecraft. I want keyboards with these switches *everywhere!*

But weirdly enough, these attributes might be the reasons why I wouldn't broadly recommend them to everyone. For example, a colleague tried a spare switch and said he was underwhelmed. I appreciate that, like my Topres, they have a specific feel and sound that others may characterise as mushy, muted, or even boring. I don't necessarily agree, but I can empathise.

There are far more qualified people to talk about keyboards than I am. But if you're starting out, I'd still recommend the Gateron Milky Yellow Pros that Clara uses. They're amazing for the price, responsive, and have that popcorn sound and feel that more people seem to love. If you'd rather a quieter switch, and want something even smoother than the buttery Ink Blacks, the North Pole 2.0 Yellows deliver. 

Well, technically not, you have to buy them and have them delivered. He said, while typing on his new keyboard.
