---
title: "I didn’t know Einstein was real"
date: "2023-03-24T15:46:51+11:00"
abstract: "… I thought he was a theoretical physicist"
year: "2023"
category: Thoughts
tag:
- pointless
- quotes
- science
location: Sydney
---
> ... I thought he was a theoretical physicist.

This makes the rounds occasionally, and I couldn't be happier. It reminds me of my dad, who was an industrial chemist, claiming he had solutions.

*(Weirdly, Australians call pharmacists "chemists". For my readers in this part of the world who are unaware, chemists deal with chemicals. Well, they also deal with other people who deal with chemicals too. A person made of chemicals formulating chemicals to tell other chemical beings about chemicals. As Sir Terry Pratchett didn't say, it's chemicals all the way down. Until, as my professor said, it isn't).*

*(This post may have set a new personal record for quotations outside an academic or technical writing capacity).*

*(If things come in threes, consider this "aside" the third one. We can pretend it had useful information if it helps, unlike the other ones which were completely pointless. Theoretically. With chemicals).*
