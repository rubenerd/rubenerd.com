---
title: "Dreams where you can’t get anywhere fast"
date: "2023-12-08T07:26:28+11:00"
abstract: "I keep having the same dream again and again. Anxiety, maybe?"
year: "2023"
category: Thoughts
tag:
- dreams
- personal
location: Sydney
---
I have two recurring dreams on a regular basis. *Genres*, you might say!

The first is this mad rush to pack before going somewhere, whether it be a holiday, business trip, house move, or even clearing a locker. There's always a time crunch involved, and insufficient boxes or bags to pack everything. 

The other type of dream riffs off that time crunch element, and applies it to new and wonderful situations. Last night I imagined I was a part of a band that was about to go on stage, but I realised my costume didn't match theirs. What was supposed to be a mad dash across the street to my apartment was stymied by road works, building construction, stairwells that was too narrow to use, tight crowds, ceilings moving up and down, missing my train, the train being too full, the train being on fire, the train being upside down, and the train doors really being white-water rafts that kept expanding out onto the platform, which made it hard to hold all my pepper grinders.

Wait, why do I have...?

The first I'm fairly sure is my subconscious playing out fears I have about clutter, perhaps seasoned with some rental insecurity. But this more recent second one is more of a mystery. General anxiety perhaps? Or could it be that the common element of feeling rushed is the primary concern, not the stuff itself?

Both these kinds of dreams are way scarier to me than the ones I had as a kid of monsters. *The monster is time!* I think The Cranberries sang a song about this.
