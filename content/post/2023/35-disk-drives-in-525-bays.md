---
title: "Losing 5.25-inch bays"
date: "2023-12-20T08:48:38+11:00"
abstract: "They’re still useful."
year: "2023"
category: Hardware
tag:
- cases
- cd-roms
- disks
- drives
- lto
location: Sydney
---
I've been on record lately lamenting the loss of the 5.25-inch drive bay in contemporary computer cases. It sucks for a couple of reasons:

* It erases another link modern machines have with the original IBM 5150, which is a bit sad if you're into computer history.

* The bays are also still *wildly* useful, whether you use them for LTO drives, optical burners, hard drive sleds, port expanders, fan controllers, status LCDs, cup holders, or junk drawers.

But we almost lost them much earlier. Look at pizza-box form factor computers from the early 1990s, and chances are you'll only see 3.5-inch drive bays. 3.5-inch floppy disks had usurped the venerable 5.25, and CD-ROMs were still expensive and unproven. Tulip's cute [Vision Line](https://www.homecomputermuseum.nl/en/collectie/tulip/tulip-vision-line-dc-386sx/ "Home Computer Museum page on the Tulip Vision Line") is a classic example of this type of machine. This form factor proved short lived, when CD-ROMs were shown to valuable for software distribution and, later, for burning your own discs.

Today, most consumer cases don't include 5.25-inch drive bays because "nobody uses" external storage anymore. I suspect cooling and *aesthetics* are likely the real reasons, things PC people simultaneously mock companies like Apple for caring about. The [Fractal Pop](https://www.fractal-design.com/products/cases/pop/ "Fractal's product page for the Pop") hides 5.25-inch bays behind a magnetic latch, but that's about it.

I'd love a case that is nothing but 5.25 bays in the front. They could be populated with a 360 mm AIO or fans if you wanted, or you could mix and match with drives. I miss when you didn't have to go to server hardware to get functionality.
