---
title: "The Galton Bridge"
date: "2023-01-24T20:36:53+11:00"
abstract: "Reading a Wikipedia article on the 19th-century cast iron bridge."
thumb: "https://rubenerd.com/files/2023/galton-bridge@1x.jpg"
year: "2023"
category: Thoughts
tag:
- bridges
- engineering
- uk
- wikipedia
location: Sydney
---
Sometimes I like to press the random button on Wikipedia and see where it takes me. Today I didn't even need to go that far; the [article of the day](https://en.wikipedia.org/wiki/Galton_Bridge "Galton Bridge") looked interesting:

> The Galton Bridge is a cast-iron bridge in Smethwick, near Birmingham, in central England. Opened in 1829 as a road bridge, the structure has been pedestrianised since the 1970s. It was built by Thomas Telford to carry a road across the new main line of the Birmingham Canal, which was built in a deep cutting. The bridge is 70 ft (21 m) above the canal, making it reputedly the highest single-span arch bridge in the world when it was built, 26 ft (7.9 m) wide, and 150 ft (46 m) long. The iron components were fabricated at the nearby Horseley Ironworks and assembled atop the masonry abutments. The design includes decorative lamp-posts and X-shaped bracing in the spandrels.

Anything sporting *spandrels* is a winner in my book. It sounds like a cross between a squirrel and a spaniel. *Abutments* also scream [Geoff Marshall](https://www.youtube.com/user/geofftech2/videos).

The [photo below by Harry Mitchell](https://commons.wikimedia.org/wiki/File:Galton_Bridge,_Smethwick_02.jpg) shows the aforementioned bridge over the aforementioned canal cutting, aforementioned. I'd love to tour all these 19th century engineering wonders around the UK one day.

<figure><p><img src="https://rubenerd.com/files/2023/galton-bridge@1x.jpg" alt="Photo of the Galton Bridge by Harry Mitchell" srcset="https://rubenerd.com/files/2023/galton-bridge@2x.jpg 2x" style="width:500px;" /></p></figure>
