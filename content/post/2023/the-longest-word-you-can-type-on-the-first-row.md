---
title: "The longest word you can type on the first row"
date: "2023-08-10T10:07:26+10:00"
thumb: ""
year: "2023"
category: Hardware
tag:
- keyboards
- language
location: Sydney
---
I grew up thinking *typewriter* was the longest English word one could type on the first row of letters on a QWERTY keyboard. I remember being told this was by design for typewriter demonstrations, then someone telling me it was an urban legend, then someone else saying it was true.

Here comes the proverbial posterior prognostication: *but...* what if the premise of this discussion wasn't true at all? What if you could type an even longer word on this first row?

Here's an example:

> Eriwtureiwotuqwriqwerewreqqwioop

Did a Welsh person get a hold of my keyboard? I kid, but what's the longest *real* English word? I went looking.

<a href="https://sheep.horse/2009/5/longest_word_you_can_type_(qwerty_edition).html">Andrew Stephens cited three back in 2009</a>

> Using Python I replicated Lloyd's results for the top row of the QWERTY keyboard: PERPETUITY, PROPRIETOR, REPERTOIRE, and TYPEWRITER

These are all ten characters long. This means the word *typewriter* remains the longest word, albeit among others. Or... is it?

[Jason Sitt performed another Python search in 2021](https://jasonstitt.com/longest-top-row-word-not-typewriter)

> This search finds three words longer than typewriter: **rupturewort**, **proprietory**, and **proterotype**.

According to Jason, *rupturewort* is a "decorative ground cover that was also used as a medicinal herb in the 1500s". There you go!

I imagine German has some epic words that can be written in just the first row. Let me know if your language does too.
