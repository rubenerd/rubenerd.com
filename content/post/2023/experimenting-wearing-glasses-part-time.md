---
title: "Experimenting wearing glasses part time"
date: "2023-05-30T08:50:15+10:00"
abstract: "I got into the habit of wearing them all the time, but I don’t need them."
year: "2023"
category: Thoughts
tag:
- health
- glasses
location: Sydney
---
I started wearing glasses when I was 14, because computer-induced myopia had made reading classroom whiteboards difficult. I could make out words with enough squinting, but sitting near my crush towards the back (who was even cuter wearing glasses, as Clara is now!) made reading all but impossible.

By my twenties, I got fed up with constantly losing my glasses and/or their cases, so I got into the habit of wearing them all the time. I'm not sure this was a great move with hind*sight*, especially considering I don't need them for all but distance vision. Whether they made my eyes worse or better I'm not sure.

Either way, I forgot to put them on when I left the apartment recently, and I didn't notice. I could make out traffic crossings, menu boards, and train station signs without much difficultly, and I'd already got into the habit of taking them off when using computers. The only time I struggled for a moment was waving at someone who, turns out, was waving at someone *next* to me (awkward)!

<p><img src="https://rubenerd.com/files/2016/screenie-ubw-glasses.jpg" srcset="https://rubenerd.com/files/2016/screenie-ubw-glasses.jpg 1x, https://rubenerd.com/files/2016/screenie-ubw-glasses@2x.jpg 2x" alt="Screenshot showing Tohsaka trying on glasses... wow #blush." style="width:500px; height:281px;" /></p>

Since then I've been seeing *(HAH!)* if I can avoid wearing them unless I need them. It's a scientific fact that everyone looks cooler with glasses, especially if you're a *Fate* character it seems, but I wanted to see what effect it had.

In short, is a phrase with two words. I haven't noticed a difference with my recurring headaches, but I feel like it's had a positive impact on my anxiety. I feel nervous with distractions, so not having glasses means I'm only focusing (literally) on a smaller part of the world at any one time, especially when commuting and walking around.

You often hear about people who've *started* wearing glasses, but I haven't seen that many who've gone the other way. Have any of you done this?
