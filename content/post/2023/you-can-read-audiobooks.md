---
title: "You can read audiobooks"
date: "2023-12-02T10:41:22+11:00"
abstract: "Gatekeeping reading to those with vision seems wrong, because it is."
year: "2023"
category: Media
tag:
- books
- reading
location: Sydney
---
A few months ago I witnessed someone get chewed out on a social network for mentioning they *read* an audiobook. Shocking, I know! I was glad to be sitting down at the time, because I'm not sure I could take such an assertion sitting down. Wait, standing. I can't stand for it. While sitting. Like a gentleman.

Look up any dictionary for *read*, and you'll see definitions describing "discovering" information, "interrogating" a record, "understanding" something, or "studying". Computers even *read* from databases, networks, and disks, and they don't use eyes for it.

People can read audiobooks the same way literary critics and high-school English teachers can refer to a film or music score as a *text*.

But recalling logic and definitions is playing their game. The core issue is that gate-keeping reading is so silly and rude in the first place. Do these people insist that our visually impaired friends merely *feel* a book instead of *reading* it? Or is that level of scorn reserved only for audio? Is reading only an activity done in a smoking jacket in a grand study overlooking a garden tended to by the hired help? If you think that's a straw-man argument, good! You're finally getting the point.

I read books, whether they be novels, manga, or fully-blown encyclopædias (okay, you can judge me for that). Most of the time this is via paper or a screen, but I do love audiobooks for non-fiction and history. Reading is wonderful, no matter the source!

But let's figure this out once and for all. Authors among you, do you consider your fans to have read your book only if they use their eyes to scan paper?
