---
title: "Jon Udell on critical mass, and social network size"
date: "2023-12-27T09:05:32+11:00"
year: "2023"
category: Internet
tag:
- jon-udell
- social-networks
- twitter
location: Sydney
---
[A post he wrote over Christmas](https://blog.jonudell.net/2023/12/25/critical-mass-in-the-goldilocks-zone/)\:

> Nuclear physicists know they are dancing with the devil when they bring fissile material to criticality. They also know that the reaction can be controlled, that it must be, and that the means of control obey well-understood principles.
> 
> Social sites typically push toward supercriticality with no such understanding.

That might be the best summation of social networks I've ever read.

> I feel the same way about Mastodon. Conventional wisdom says it’s dead in the water: “nobody” goes there, no Mastodon apps rank highly in the app stores. But if critical mass means operating at the scale of Twitter or Facebook, then who wants that? Who benefits from the inevitable enshittification? Not me, and probably not you.

He also mentions LibraryThing as an example of a site that has achieved *critical mass*, at least in his and my eyes. But the New York Times article he quotes earlier in the post would disagree. They want stratospheric growth for investors, and populations of users larger than that of most countries.

I'm onboard with Jon here. If journalists and investors classify sites as having achieved critical mass with such metrics, is that what we really want? And... is it even true?


### It's not just how you use it, it's the size (wait)

I've been thinking about the size of social networks a lot this year, not least due to my stepping back from Twitter as my primary way of talking with friends. This was hard, given it was the only social network I ever really grokked. But time and time again I see sites that investors adore, and people begin to despise. 

Twitter was fun in 2007, when the site had fewer than a million people. It's not elitism or exclusivity, it's just experience tells me that big sites are miserable. Moderation at that scale is functionally impossible, and features designed to serve those people don't appeal to those who signed up for a scrappy website where you could feasibly read the firehose to find interesting people.

This gets to how one defines critical mass. What Jon describes, and what I've been thinking for a while, is that a site achieves critical mass when its self sustaining; what Jon calls the start of the *The Goldilocks Zone*. Mastodon is self-sustaining. My silly blog here, and your blog if you have one, is self-sustaining. Neither require oodles of money or users to be successful; at least, if you define it as being able to support its own operation.


### Mastodon as a case study

Mastodon is an interesting case here, because while one instance could feasibly achieve a size that would push it into Twitter territory, its federated nature acts as a pressure release of sorts. The network effect makes it difficult to move, but it's possible on Mastodon (though it could still be made easier). Fediverse servers can be moderated more easily, and you can mute or decouple from instances acting in bad faith. This is literally how the Web used to work before silos.

I first knew Jon from his appearances on The Gillmor Gang back when it was on IT Conversations. This was my jam when I was in high school, and when I started commuting to my first job in Singapore. He's still making me think now.
