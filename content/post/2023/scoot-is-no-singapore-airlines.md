---
title: "Scoot was one of the worst airlines I’ve flown"
date: "2023-06-22T09:29:46+10:00"
abstract: "They’re not worthy to be called budget SIA."
year: "2023"
category: Travel
tag:
- airlines
- singapore
- singapore-airlines
location: Sydney
---
I've held off writing about this for a while, because it was one blemish on an otherwise wonderful trip. But I think it needs to be said.

Singapore Airlines is the best airline in the world. Thesedays I prefer to qualify such absolute statements with *in my opinion*, or *from my own experience*, or *based on my own tastes and preferences*. Singapore Airlines warrants no such language.

On our recent Japan trip, we flew via Changi Airport on Singapore Airlines so we could land in Kansai instead of Tōkyō, and so I could see home for a few hours (Singapore is the closest I've felt to having one). Two of the connecting flights were code-shared with Scoot, SIA's budget carrier. I'd flown with their former subsidiary SilkAir before, and even Scoot back to Sydney when they first launched, and both were fine.

I don't say this lightly, but they were the worst flights I've ever had!

### Expectations versus reality

Scoot's marketing team might disagree, but a big part of their appeal is their connection with Singapore Airlines. The airline launched with many ex-SIA aircraft, which have a reputation for their excellent maintenance records. The same crews maintain both fleets, and I've heard from a Scoot employee that the airline is a stepping stone to employment with SIA. And you'd assume a premium carrier wouldn't want to tarnish their image by code-sharing with something bad.

This discrepancy between expectations and reality goes part of the way to explaining why the experience was so grim. Flying AirAsia, Ryanair, or Jetstar, you don't expect anything (though AirAsia exceeded my expectations a few times back when I was a student).

But while the Scoot planes we flew were spotless, and some of the cabin crew were great, it wasn't "budget SIA". I could nitpick dozens of things, but four things stood out.


### The service and experience

I had a migraine on the flight from Sydney to Singapore, and I vomited several times. This wasn't Scoots fault! But twice I attempted to leave my seat for the nearest bathroom though, clearly under duress, only to be told angrily by staff that there was no using the bathroom while the seatbelt sign was on during turbulence. I suspect they were exhausted having to explain it repeatedly, but it doesn't excuse management for imposing such a rule that other carriers have the sense not to. The absolute despair I felt during those hours was not something I want to repeat. Ever.

Multiple times during the flight to Singapore I asked for water, which they refused without giving a reason. We got a paltry little bottle during the food service, then nothing. I resorted to drinking water from the bathroom to take headache tablets and to rinse my mouth out, which you should *never* do! On the flight back, this resulted in an absolutely comical line of people queueing for the lone water bubbler outside the gate to refill what bottles they could before the flight. It took so long, some people had to board having not reached the tap in time.

The legroom and seats were dreadful, but that's to be expected from a budget carrier. But somehow they felt even worse, like I was leaning against a shapeless ironing board for the duration. Even Jetstar seats feel better.

But the perfect representation of them *not getting it* would have to be their Pokémon-themed plane that greeted us on the return flight. Having loud game theme music piped through a cabin in the middle of the day might be a bit of cheeky fun... but on a redeye? I think the poor kids crying for it to stop summed it up.


### Summing up: I'll be avoiding Scoot 

This sounds like entitled rambling, and I suppose it is. But more broadly, it frustrates me that an industry has decided that treating plebeians like crap is acceptable. Airlines like Ryanair make jokes about it, and ones like Scoot have cutesy names for things, but it's easy to see through. We made the mistake of code-sharing with this airline, but there are plenty of people who wouldn't have a choice. Would a bottle of water or two really be too much to ask? Access to the bathroom when you need to wash vomit off your face? Little things!

I also want to stress that I pass no blame on the flight attendants. While several were more than a little terse, it would be a *hard* job for which I'm sure they're underpaid. Budget airline expectations work both ways: I expect they get treated like crap because passengers are uncomfortable, and it'd be hard not to internalise that. The whole situation is tragic, and I place the blame squarely on management. Some of them really tried their best under the circumstances. 

As part of our codeshare, we were offered free Scoot blankets we were told multiple times we could take home. We left them on the seat. To be clear, if the option was only affording to fly by taking Scoot, or staying home, I'll be watching a documentary about the destination instead.
