---
title: "Even chatbots are better than web search"
date: "2023-03-28T09:44:48+11:00"
abstract: "Not a ringing endoresement, but at least they deliver text."
year: "2023"
category: Internet
tag:
- ai
- chatbots
- search
location: Sydney
---
Until recently, I've struggled to understand the hype around AI chatbots and art generators. They've been trained on the ingenuity, creativity, and dreams of billions of people, and... that's the best they can do? When viewed in that context, it's hard not to see their lossy, error-filled output as anything more than bleak mediocrity; a procedurally-generated [sugar hit](https://twitter.com/Ninetail_foxQ/status/1636924016492699650) chosen over something nourishing.

Here comes the proverbial posterior prognostication: **But!** I'm starting to understand their appeal in a specific context, though it has less to do with the tech's strengths, and more the fact the modern web isn't even sugar, its outright muck!

British anthropologist Marilyn Strathern famously [generalised](https://en.wikipedia.org/wiki/Goodhart%27s_law#Generalization) economist Charles Goodhart's law as:

> When a measure becomes a target, it ceases to be a good measure.

This is how the [web responded](https://www.currentaffairs.org/2020/12/how-seo-is-gentrifying-the-internet) to page-ranking metrics. SEO'd flotsam washes over every search result page, and when you do get a page written by a person, it's so laden with junk to track engagement and milk every last cent of ad revenue that it's painful to use... to say nothing of bloat, clickbait, and other fun [dark patterns](https://rubenerd.com/tag/dark-patterns/).

Chatbots give you a text answer from a prompt... and that's it! People are literally willing to put up with the additional work of [fact-checking](https://www.nytimes.com/2023/02/08/technology/ai-chatbots-disinformation.html) its [fabrications](https://www.aiweirdness.com/search-or-fabrication/), rather than trawling through pages of search results (I say fabricate, because AIs lack the cognition to lie. I liken it more to [bullshit](https://en.wikipedia.org/wiki/Bullshit#Assertions_of_fact) or [bollocks](https://en.wikipedia.org/wiki/Bollocks#%22Talking_bollocks%22_and_%22bollockspeak%22), given accuracy and truth aren't concerns).

In that way, a chatbot could be thought of as an interface over something the industry has deemed an intractable problem, or one not worth solving. Building abstractions is old hat, but I reckon fixing the incentive structure around search would deliver a more positive impact. Yes, it would involve confronting the very fabric and business models of the modern web, but we must if we're interested in its long term viability.

But then, fixing things doesn't drive up stock prices or investor hype. Just ask any tired civil engineer looking at a crumbling overpass.
