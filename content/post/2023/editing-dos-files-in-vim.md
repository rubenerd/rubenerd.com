---
title: "Editing DOS files in Vim"
date: "2023-01-03T10:18:32+11:00"
abstract: "Setting Vim file encodings and formats for DOS files"
year: "2023"
category: Software
tag:
- dos
- guides
- retrocomputing
- vim
location: Sydney
---
If you're writing a file in Vim that will end up on a DOS machine, use these:

	set encoding="ISO8859-2"
	set fileencoding="ISO8859-2"
	set fileformat=dos

This is also the first addition to the [lunchbox](https://codeberg.org/rubenerd/lunchbox/src/branch/trunk/dos.vim) for this year :).
