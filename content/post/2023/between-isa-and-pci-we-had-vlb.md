---
title: "Between ISA and PCI, PCs had EISA and VLB"
date: "2023-07-20T21:05:23+10:00"
abstract: "The history of early 1990s graphics tech is fascinating."
thumb: "https://rubenerd.com/files/2023/eisa@1x.jpg"
year: "2023"
category: Hardware
tag:
- graphics-cards
- retrocomputing
location: Sydney
---
The late 1980s–early 1990s were a fascinating period of computer history. I feel like I say that about every time period, but you get the idea. I only wish sometimes I was either alive or old enough to understand what was evolving at that time.

In creating the PC from mostly off-the-shelf components, IBM had unintentionally set the wheels in motion for an open(ish) computer ecosystem. Other computer companies like Apple had their third-party cloners, but this was the first time no one business or entity controlled the destiny of a platform.

*(Though I bet Commodore could have used some divine control by that time period, but that's a separate post)!*

This wasn't from lack of trying. In an attempt to steer the direction of the PC market, IBM had attempted to supplant the AT/ISA bus with their own Micro Channel architecture with their PS/2 machine in 1987. ISA's bottlenecks and limitations were becoming a severe hindrance to further improvements to the PC, and MCA would let IBM collect royalties.

PC clone manufactures were having none of it. But this is where history glitches a couple of times. Take a look at most computer motherboards from this time period, and you'll almost certainly find a mix of ISA and PCI slots. But there were *transitional fossils* so to speak, and they look so unusual!

The first was the Extended Industry Standard Architecture, or EISA. Introduced a year after MCA, these slots were a functional superset of ISA, maintained full backwards compatibility, and crucially were royalty-free. The staggered pins appear similar of the Slot 1 used by the Pentium 2 in the following decade, with the same potential for alignment issues (cough). But with them came a 32-bit data bus, higher data rates, and software controls in lieu of jumpers.

[Konstantin Lanzet: ELSA Winner 1000 for ISA and EISA Bus](https://commons.wikimedia.org/wiki/File:KL_ELSA_Winner_1000_ISA_EISA.jpg)

<figure><p><img src="https://rubenerd.com/files/2023/eisa@1x.jpg" alt="Konstantin Lanzet: ELSA Winner 1000 for ISA and EISA Bus." style="width:500px;" srcset="https://rubenerd.com/files/2023/eisa@2x.jpg 2x" /></p></figure>

By 1993, graphics cards were reaching even EISA's improved bandwidth limits, so VESA went back to ISA and implemented the VESA local Bus, or VLB. As I mentioned on Mastodon, I only learned this week this *wasn't* a tonuge-in-cheek initialism for *Very Long Bus*, on account of the sheer size of these cards.

Unlike EISA which extended the ISA bus connector, VLB maintained compatibility by adding an additional connector inline with the ISA bus that would later be physically reused for PCI. The result is a bizarre Frankenstein card with PCI-looking pins on one end, and ISA on the other. It was quite normal for cards to have large blank sections simply to accomodate the width of these connectors.

[Konstantin Lanzet: Graphics card ATI MACH64 VLB](https://commons.wikimedia.org/wiki/File:KL_ATI_Mach_64_VLB.jpg)

<figure><p><img src="https://rubenerd.com/files/2023/vlb@1x.jpg" alt="Konstantin Lanzet: Graphics card ATI MACH64 VLB." style="width:500px;" srcset="https://rubenerd.com/files/2023/vlb@2x.jpg 2x" /></p></figure>

PCI was introduced scarcely a year later after VLB, and by 1997 the graphics industry began moving to AGP.

But now with all the hoops modern graphics card manufactures are going through to accommodate additional power delivery, we may yet see another weird mish-mash set of connectors again.
