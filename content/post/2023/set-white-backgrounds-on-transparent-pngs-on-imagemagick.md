---
title: "Set white backgrounds on transparent PNGs with ImageMagick"
date: "2023-12-06T09:04:36+11:00"
abstract: "convert $SOURCE -background white -alpha remove -alpha off $DESTINATION."
year: "2023"
category: Software
tag:
- guides
- imagemagick
- images
location: Sydney
---
Thanks to Rok Kralj [for this tip](https://stackoverflow.com/a/8437562 "Answer on StackOverflow")\:

    convert $SOURCE -background white \
        -alpha remove \
        -alpha off $TARGET

I've also added it to the [lunchbox](https://codeberg.org/rubenerd/lunchbox/src/branch/trunk/flatten.sh "flatten.sh in my lunchbox repo on Codeberg"). 🍱
