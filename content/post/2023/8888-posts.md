---
title: "Reaching 8,888 posts, and a bit of a retrospective"
date: "2023-11-13T08:23:59+11:00"
abstract: "Remembering back to a 2011 post about the future made me think about it again. Also, 8,888 posts!"
year: "2023"
category: Internet
tag:
- numerology
- pointless-milestone
- weblog
location: Sydney
---
Back in 2011, WordPress assigned an [internal ID of 8888](https://rubenerd.com/id8888/ "WordPress’s 8888 ID post of fortune!") to a random post, which I thought was a bit fun. I wrote at the time:

> So here we are at post ID number 8888. Granted this is far less impressive than 8888 actual posts, but nonetheless a reason to celebrate and to receive some luck. Right?
>
> I reach my actual 8888th post in... 5105 posts from now. That's practically nothing, better start planning what I'm going to write about, assuming I'm still writing absolutely riveting, educational and entertaining posts like this when that number rolls around.

Well, we're here! <strong><em>Shiok!</em></strong> I've reached the [luckiest post](https://en.wikipedia.org/wiki/Chinese_numerology#Eight "Wikipedia article on Chinese numerology, number 8") in my blog's history, and it only took eighteen years since I started writing. It also gives me a shameless opportunity to do some inward reflection on where I am now, and where this site is going.

<figure><p><img src="https://rubenerd.com/files/2023/post8888@1x.jpg" alt="Rubi hanging out with my coffee cup on the balcony" style="width:500px; height:375px;" srcset="https://rubenerd.com/files/2023/post8888@2x.jpg 2x" /></p></figure>


### Past, present, future

This blog started as a writing experiment on a site I self-hosted while I was in high school. I'd maintained a journal since I was a little kid, so this seemed like a natural progression. After a few fits and starts, I started posting regularly in late 2005. It's turned into one of the most rewarding and fun daily experiences in my life.

The web has changed *a lot* in the intervening years, as have I. There are pressures from all sides, from mass-generated AI spam, to data harvesting and dark patterns. Network engineers predicted the ad-hoc nature of the Internet would lead to its collapse, but now I see all the rot coming from the upper OSI levels. It's an... *interesting* time to be online.

I try to live my life as a *cautious optimist*, but a part of me is tired by what the web has become. I think my interest in retrocomputers is no accident here; it's not only nostalgia, it's a yearning for a time when computers were fun, grokkable,... and *ours.* I have a silly, related project in the works as we speak, which I hope to link to soon.

In the meantime though, there's still room for optimism. Syndication formats like RSS and Atom, for which endless obituaries were written, are thriving. Nascent federated technologies like Mastodon continue to grow. And I'm seeing a resurgent interest in the idea of maintaining a personal site or blog, unencumbered by pot-stirring algorithms and what Cory Doctorow refers to *enshittification*. You don't realise how hot the water has been until you leap out into a network like that.

Many of you continue to send me feedback, despite my bad response rate of late. The best have been those who've been encouraged to start writing after reading what I do here. I hope that might also include you one day, if it doesn't already. Clearly it can't be hard!

I'm not sure what the future has in store here. I do feel as though my current blogging system is limiting in same ways. I'd love to do more random link-style posts, and be able to post from places *other* than a laptop. Static-site generation simplifies so much, but I yearn for a database I can query and batch-process. Who knows, by the time 18,888 roles by I'll have this blogging gig all figured out.

Thanks again for reading :). Or given this is 8,888, and I lived in Singapore, it should be 感谢... or 谢谢!
