---
title: "Clara’s and my Minecraft third anniversary"
date: "2023-10-13T21:16:17+11:00"
abstract: "Our third year playing Minecraft on this server."
thumb: "https://rubenerd.com/files/2023/minecraft-third-anniversary@1x.jpg"
year: "2023"
category: Software
tag:
- clara
- games
- minecraft
location: Sydney
---
It's now our third year playing Minecraft, and we're still on the same *Kiriben* server and map. This year we went out to our jungle biome and planted a flower garden to celebrate :').

I forgot to post our second anniversary, but [this was our first](https://rubenerd.com/claras-and-my-minecraft-server-anniversary/ "Clara's and my Minecraft first anniversary").

<figure><p><img src="https://rubenerd.com/files/2023/minecraft-third-anniversary@1x.jpg" alt="Screenshot of our flower garden and three celebratory cakes." srcset="https://rubenerd.com/files/2023/minecraft-third-anniversary@2x.jpg 2x" /><br /><img src="https://rubenerd.com/files/2023/minecraft-third-anniversary-map@1x.jpg" alt="Expanded Dynmap of our server." srcset="https://rubenerd.com/files/2023/minecraft-third-anniversary-map@2x.jpg 2x" /></p></figure>


