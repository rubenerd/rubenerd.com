---
title: "The Foenix F256k retrocomputer"
date: "2023-07-04T09:10:04+10:00"
abstract: "This looks like an awesome machine! Jan Beta did a great video review outlining its construction and how it works."
thumb: "https://rubenerd.com/files/2023/yt-57FuA8YuXn0@1x.jpg"
year: "2023"
category: Hardware
tag:
- foenix-f256k
- jan-beta
- retrocomputing
- video
- youtube
location: Sydney
---
This looks like an *awesome* machine, but not only for its design and technical capabilities, but what it represents. This is my attempt to explain why, followed by a review of Jan Beta's... review!


### Retrocompting can be great, but...

Retrocomputing can be a fun hobby, and an interactive way to learn about computer history. But the barrier to entry can be steep if you're intimated by investing time and money in troubleshooting old hardware, and without any guarantees for success. It's finally taught me how to use oscilloscopes and soldering irons, but not without an incredible amount of frustration at times!

It can be even more difficult if you didn't grow up with the tech. I know my way around early 32-bit DOS machines because it's what I first used, but I still feel like a middling amateur around 8-bit era machines. This is doubly true for peripherals with exotic connections, formats, and capabilities that may work completely differently to what you're used to.

We also need to consider software. I used to be more interested in what the computers *ran* than the machines themselves, hence my interest in virtualisation and emulation tools (which ironically landed me my current job). These come with their own benefits like increased performance and flexible storage, which will never be as simple on physical hardware.

But... what if you *are* interested in the hardware, but are put off by the technical complexity and uncertainty that comes from running forty-year old tech? What if you want to do that eventually, but want a more accessible introduction? Or on the flipside, what if you love retro hardware, but want something a bit more modern and reasonable to do daily developing and testing? If you're like me, you long for the C128 or Plus/4 over the C64 simply because the BASIC is so much nicer, but even those machines have limitations in other ways.

*Turns out* there's a whole market segment now for thoughtfully constructed modern hardware designed to retro specifications. My favourite retrocomputer YouTuber Jan Beta has been reviewing a whole bunch of these, and his recent review of the Foenix F256k was a lot of fun.

<figure><p><a target=_BLANK href="https://www.youtube.com/watch?v=57FuA8YuXn0" title="Play Foenix F256K New Retro Computer"><img src="https://rubenerd.com/files/2023/yt-57FuA8YuXn0@1x.jpg" srcset="https://rubenerd.com/files/2023/yt-57FuA8YuXn0@1x.jpg 1x, https://rubenerd.com/files/2023/yt-57FuA8YuXn0@2x.jpg 2x" alt="Play Foenix F256K New Retro Computer" style="width:500px;height:281px;" /></a></p></figure>


### Jan's review of this machine

The F256K is a modern retrocomputer built around a WDC 65C02, the continuation of the original 6502 chip that powered everything from the Commodore PET to the Apple II. The unit Jan reviewed retains the wedge keyboard form-factor everyone is familiar with, though there's also a Mini-ITX version for those wanting to bring their own peripherals.

It comes with a mechanical keyboard which doesn't come with PETSCII, as Jan points out, but still has those familiar function keys and colour codes. We also get inverted T cursor keys, which is another modern convenience compared to using SHIFT in Commodore land.

Jan dives into a review of the motherboard, which looks thoughtfully put together and beautifully designed. I would have liked to see more socketable chips, but given the proliferation of complex FPGAs for some of the functions, they're probably not that user servicable, and would have added additional size and complexity to the machine. The CPU *can* be swapped however, and Jan mentioned you can substitute 65C02s with different clock speeds.

Another modern advantage Jan takes pains to point out is the machine's 640×480  graphics output at 60 Hz with analogue/digital DVI-I or VGA... *hallelujah!* A problem you soon face with plenty of retro gear is a hornet's nest of cables, upscalers, converters, and incompatible monitors. Jan has done videos in the past where he's replaced RF cans in various Commodore machines with video converters, but he won't have to with this!

The F256K is packed with Paul Robson's SuperBASIC, support for additional sprites, tiles, and even hardware accelerated scrolling which should make game development significantly easier and more fun. After years of pottering around in various modern languages, I'm thinking a system like this would be perfect to build the silly sci-fi puzzle game I've had in my mind for years.


### A (very predictable!) conclusion

I could go on and on, including mentioning that you can buy a matching disk drive for it that's compatible with Commodore kit, and its inclusion of a Yamaha OPL3 chip! But you should definitely watch Jan's video and check out the site for yourself.

[Jan Beta's video about the F256K](https://www.youtube.com/watch?v=57FuA8YuXn0)   
[Foenix Retro Systems](https://c256foenix.com/)

There are several machines attempting&mdash;with varying degrees of success&mdash;what the Foenix team has done here, but based on what I've read and watched, this easily looks to be the most polished modern retrocomputer available today. When the budget permits, I'll absolutely be buying one to check out!

