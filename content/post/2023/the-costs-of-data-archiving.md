---
title: "The costs of data archiving"
date: "2023-11-16T16:28:53+11:00"
abstract: "Making the leap from storage to cloud storage shows what a problem we have."
year: "2023"
category: Internet
tag:
- phones
- storage
location: Sydney
---
Shanti Mathias wrote an interesting article for the Kiwi-based *Spinoff*, in which she discussed the [true cost of digital memories](https://thespinoff.co.nz/internet/04-11-2023/the-true-cost-of-your-digital-memories)\:

> It’s easy to take hundreds of photos, or save hundreds of messages, without thinking about the cost. But the vision of unlimited digital space isn’t as breezy as the word ‘cloud’ implies.

I'm sympathetic to the problem, but the leap from photos to cloud storage highlights another issue.

The storage capacity of our mobile devices has remained stagnant for a decade, relative to the size of images and videos these devices produce. This despite huge breakthroughs in solid-state storage density in other devices; just look at desktops and servers. Why is this?

I can think of two reasons:

* Manufacturers want to sell you their proprietary cloud photo storage, in the hopes you'll treat it as extended capacity they can bill you for in perpetuity. Services are one of the few areas where traditional hardware companies are seeing growth, and you're captive once all your data is there.

* Remote storage keeps data safe if devices are lost.

The latter is true, but doesn't depend on the former. There are many, many alternatives to forking over money to your handset manufacturer for permission to keep your photos somewhere. It's a testament to marketing that the public now automatically conflate the two, and I don't blame anyone non-technical for thinking otherwise.

I'm glad to see more discussions being had about our digital footprints. Here comes the proverbial posterior prognostication: *but...* I think it's worth decoupling these issues so we can address them properly.
