---
title: "The story of a flawed office tap"
date: "2023-07-08T06:08:20+10:00"
abstract: "Designing a tap that fails with water being splashed on it. Brilliant!"
year: "2023"
category: Hardware
tag:
- design
- errors
- rambling
location: Sydney
---
Have you ever wondered how software, or hardware, or a system consisting of many of the aforementioned components, could contain *such* an obvious design flaw? Or ceiling? Or even walls? Thank you, I'm here all week.

The confusion around such glaring problems only mounts when one considers how many people must have conceived, designed, developed, tested, signed-off, implemented, distributed, sold, and installed it. *Anyone* in that chain could have seen it and asked... *wait, it does THAT?*


### Enter this office tap

Our office building has shared bathrooms on each floor, which include water taps for our hygienic convenience. Most people in the world are unfamiliar with their operation, but a tap works by providing a liquid interface between your hand and, ideally, a surfactant such as soap. This permits the removal of detritus, and the killing of bacteria that can cause all manner of health problems.

<figure><p><img src="https://rubenerd.com/files/2023/aqours@1x.png" alt="Love Live! Aqours" style="width:250px; height:190px; float:right; margin:0 0 1em 2em;" srcset="https://rubenerd.com/files/2023/aqours@2x.png 2x" /></p></figure>

The taps in this office building release water through their spouts, which is a desirable feature. They also eschew (gesundheit) a handle for a sensor, permitting the release of the aforementioned aqueous substance (not to be confused with *Love Live! Aqours*) without directly interfacing with the hardware. This is handy, as it reduces the spread of surface bacteria and other nasties.

Handy? Tap? Hand washing? *AAAAAAH!*

Unfortunately, the tap has a design flaw. A flaw so monumentally ridiculous that, again, I have to stress how unbelievable it is that nobody noticed.


### Operating the tap

I attempted to use the tap yesterday afternoon, and the water failed to flow. I waved my digits in front of the sensor with an embarrassing degree of enthusiasm, but nothing happened.

For all our technical progress in modern times, I felt like such a caveperson being stymied by a tap. A *tap*. Surely something we've been building for hundreds or thousands of years should be easily operated by an intelligent, charismatic, modest person such as... you. Not me, I'm more awkward. But at least I should be able to use the tap too.

After a distressing amount of time had passed questioning my cognitive functions and place in the world, I noted a tiny drop of water on the sensor. I flicked it off... and the tap sprang to life with all its watery goodness. Moving my hands away from the tap, then back against the sensor, and the tap behaved as one would expect.

Was... was that it? Had a drop of water really been responsible for the failure of this device?

<figure><p><img src="https://rubenerd.com/files/2023/damp-tap@1x.jpg" alt="A very damp tap." style="width:500px;" srcset="https://rubenerd.com/files/2023/damp-tap@2x.jpg 2x" /></p></figure>


### Diagnosing the fault

Being the scientifically minded person I am, I decided to perform an experiment. The bathroom was equipped with *two* of these taps for a degree of hygienic redundancy, so I decided to attempt the use of this other unit. It operated entirely as expected: waving my hand in front of it activated the flow of water, and it stopped upon moving my hand away.

Brilliant!

Next, I flicked the sensor with my wet hands until I got a few drops of water on the sensor, in an attempt to recreate the conditions that stopped the tap operating in the first example. I gestured as I had done before, and sure enough, my hails went unacknowledged.

This required me to take stock of the situation. Here we have a tap, that dispenses *water*, in a *wet* bathroom environment, that's liable to being *splashed with water*, that can be rendered inoperable by... get this... *water*.

Again, I ask, how did *entire teams of people* not spot this? Again, I point out, how utterly *ridiculous* it is that a water dispenser can be disabled with water. Again, how? HOW?! What the actual...!

The experience left me with more questions than answers. But if one thing is for sure, is a phrase with seven words. I will be paying very close attention to the next bathroom I attempt to use to see if they have the same fault. Because if there's one thing the world needs, its amateur hydrologists poking their heads into sinks, in public, for science.
