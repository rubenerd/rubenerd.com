---
title: "Yorkshire Tea Malty Biscuit Brew"
date: "2023-07-12T21:10:05+10:00"
abstract: "I can't wipe the grin off my face drinking this."
thumb: "https://rubenerd.com/files/2023/yorkshire-tea-malty-biscuit@1x.jpg"
year: "2023"
category: Thoughts
tag:
- drinks
- food
- tea
location: Sydney
---
<figure><p><img src="https://rubenerd.com/files/2023/yorkshire-tea-malty-biscuit@1x.jpg" alt="" style="width:164px; height:248px; float:right; margin:0 0 1em 2em;" srcset="https://rubenerd.com/files/2023/yorkshire-tea-malty-biscuit@2x.jpg 2x" /></p></figure>

Clara and I *had* to try this tea when our supermarket started selling it.

From the description:

> Here's a miraculous tea that tastes like biscuits - because when those two flavours combine, the resulting deliciousness creates a wave of happiness big enough to power an entire human being! It's a magical mug of biscuity FLAVOUR goodness that doesn't get crumbs on your jumper.

I don't know how they did it, but they did it! I'm drinking a cuppa right now as I type this, and I can't wipe the grin off my face.
