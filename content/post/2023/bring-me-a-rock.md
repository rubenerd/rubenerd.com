---
title: "The “Bring Me a Rock” phenomenon"
date: "2023-08-29T13:57:06+10:00"
abstract: "If you have a manager or client like this, ask as many questions as you can at the outset."
year: "2023"
category: Thoughts
tag:
- engineering
- work
location: Sydney
---
[Jonathan Becher](https://jonathanbecher.com/2020/08/30/the-bring-me-a-rock-phenomenon/)

> This phenomenon happens when a manager cannot or will not communicate their goals clearly and succinctly. Subordinates repeatedly try to fulfill their manager’s expectations through multiple attempts of bringing them a rock (i.e., proposal, product, process, etc.). Each time, the rock isn’t quite right – with the manager producing another requirement. Eventually, the manager becomes satisfied or the subordinates wearily give up.

Jonathan argues this arises from managers who are unsure about expectations, unclear about what they want out of a project, and may not be decisive.

A friend shared this article with me recently, and it's given me a new view on a range of things, including thinking about client work. The whole post is worth a read, but at the risk of spoiling the ending, here's how Jonathan proposes tackling someone who asks you to bring them a rock:

> ... ask as many questions as you can at the outset. Don’t be afraid it will make you look bad; it might actually help your manager better frame the request. It’s possible your questioning might frustrate this kind of manager but they are going to be frustrated by you not delivering what they asked for anyway.

