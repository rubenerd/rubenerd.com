---
title: "Fun Facts #101–110"
date: "2023-07-14T12:49:29+10:00"
abstract: "Allan Jude has never scrubbed a ZFS pool riding a unicycle."
year: "2023"
category: Thoughts
tag:
- fun-facts
- pointless
location: Sydney
---
In no meaningful order:

* I last published a *Fun Fact* in 2011, less than a century ago.

* It's impossible to eat a burger sideways.

* A German-born camel has never held car insurance.

* You can rotate a printer to have paper come out double-sided.

* Allan Jude has never scrubbed a ZFS pool riding a unicycle.

* The median average of everything is sometimes nothing.

* An office chair with four backs would be impossible to sit on. 

* This *Fun Fact* exists.

* You can fit an entire frypan into most train carriages.

* <span style="font-size:larger">32</span> is larger than <span style="font-size:smaller">48</span>.

[View all the great (cough) posts in this series](https://rubenerd.com/tag/fun-facts/)
