---
title: "Moving to ansible_facts['distribution']"
date: "2023-05-24T09:29:22+10:00"
abstract: "The previous ansible_distribution function is deprecated."
year: "2023"
category: Software
tag:
- ansible
- bsd
- debian
- fedora
- freebsd
- guides
- netbsd
location: Sydney
---
I always add conditions to my Ansible playbooks to check the target OS and distribution. Until recently I did this:

    - name: Install FreeBSD package (legacy)
      community.general.pkgng:
          name: "cowsay"
          state: present
      when: ansible_distribution == "FreeBSD"

This is also useful for differentiating between different distributions of the same OS, such as *Debian* or *Fedora*.

[Reddit: Preferable to use ansible_facts or ansible_distribution?](https://www.reddit.com/r/ansible/comments/13giahm/is_it_preferable_to_utilize_ansible/)

According to akasivel on this Ansible thread, this will soon be formally deprecated in favour of **ansible_facts**:

    - name: Install FreeBSD package
      community.general.pkgng:
          name: "cowsay"
          state: present
      when: ansible_facts['distribution'] == "FreeBSD"

