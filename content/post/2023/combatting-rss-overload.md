---
title: "Combatting RSS overload"
date: "2023-09-05T08:52:34+10:00"
abstract: "Reorgansing my posts by frequency and not topic."
year: "2023"
category: Internet
tag:
- rss
- web-feeds
location: Sydney
---
I was *sure* I talked about this before, but a quick look through the archives didn't turn up anything, at least not recently. Either way, what I'm about to describe is still happening, so it's worth thinking about.

I love RSS... perhaps a little too much. My feeds are full of great stuff that I've curated since high school in the 2000s. It outlasted any social network, and I suspect will continue to do so. But there's a lot there.

There are three problems:

1. **Is a number**. Mmm, that's some quality humour.

2. **Volume**. I'm not a completionist, but I still feel like I miss out on stuff I care about because I don't see everything that comes across my desk.

3. **Uneven-ness?** A dozen blogs constitute the bulk of the posts that appear on my feed, which drown out smaller voices.

These issues are compounded by RSS aggregators that treat feeds like email, which guilts you with an unread counter. I use Feedland which treats news like a *river*, similar to how social networks do it. That one change has made reading so much more enjoyable.

Thinking about the problems above though, I'm starting to think (which is always dangerous) I need to figure out a new organisational structure based on *frequency*, which is the metric that's having the most impact here.

I currently categorise RSS feeds by topic, but rarely am I only interested in one specific field. The entire point of RSS is to aggregate stuff that interests you, so seeing a post about a new railway signaling system next to a FreeBSD Foundation update and an anime figure isn't a concern.

Maybe the tabs in my Feedland need to be *Firehose*, *Daily*, *Weekly*, and *Rarer*, or something to that effect. This would help surface posts from people who don't post as often, while still having access to regular posters.

It's not an attempt to pull a signal from the noise, because in this case the "noise" is also good. Is there an information science term for this?

Let me know if you've struggled with this and have ideas too.
