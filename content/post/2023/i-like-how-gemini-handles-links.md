---
title: "I like how Gemini handles links"
date: "2023-04-06T17:52:16+10:00"
abstract: "Having links on their own line makes them easier to read."
year: "2023"
category: Internet
tag:
- design
- gemini
location: Sydney
---
I've been spending more time on Gemini, and enjoying the experience. It's not just a lightweight protocol for computers, it's quick to download and easy to read for us too. You don't realise just how much we've sacrificed on the modern web until you try something that's *unabashedly* usable. 

Weirdly, links are one of the things thing I've found myself liking the most. Gemini doesn't have the concept of an inline link; instead links are presented in their own paragraph:

[Gemtext documentation](https://gemini.circumlunar.space/docs/gemtext.gmi)

I'm still not as good with alt text for inline links as I should be, but Gemini sidesteps this issue by presenting the link and its description in a separate line. It also makes paragraph text easier to read. Dare I say, is a phrase with three words.

Even if my dream of publishing my blog in parallel on Gemini doesn't come to pass in the short term, I'm tempted to trial putting links like this so I've got something closer to polyglot markup. Might be cool to try.

