---
title: "Goodbye to Inasoul, and Banh and Boba"
date: "2023-12-03T10:40:38+11:00"
abstract: "Some of our favourite haunts shut their doors over the last month."
thumb: "https://rubenerd.com/files/2023/banh-and-boba@1x.jpg"
year: "2023"
category: Thoughts
tag:
- food
- goodbye
- restaurants
location: Sydney
---
Our favourite Bánh mì store *Banh and Boba* in Sydney shut its doors today. Clara was lucky enough to see their post on social media so we could have it one last time. This closure came just a few weeks after our favourite Filipino takeaway store *Inasoul* also abruptly shut down, and earlier this year we lost our local *Alley LuJiaoXiang*

<figure><p><img src="https://rubenerd.com/files/2023/banh-and-boba@1x.jpg" alt="Photo inside the now-closed Banh and Boba." style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2023/banh-and-boba@2x.jpg 2x" /></p></figure>

They didn't state the reason for their closures, but I wouldn't be surprised if it's upwards pressure on rents, and a steep reduction in disposable incomes over the last year. Now's not a good time to make food targeted at Gen X or younger, unless you're peddling canned beans.

Food has absolutely been one of the first things Clara and I have slashed spending on, and we're still lucky to have enough savings for travel. I'm lucky that my main hobby right now is also buying and restoring old computers most people don't care about. The squeeze is real.
