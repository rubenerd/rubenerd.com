---
title: "The pushback against productivity hacks"
date: "2023-08-27T11:08:12+10:00"
abstract: "Remember when productivity tools were seen as the solution to everything? People are realising it was misdirection."
year: "2023"
category: Thoughts
tag:
- productivity
- work
location: Sydney
---
Remember the early 2010s when it seemed everyone was getting into **#productivity**? The idea that all these emergent tools could be used to unlock a new, more productive, and happier you? Heck, maybe even more handsome or beautiful, because you'd be disruptively synergising all those nuanced paradigms with such orthogonal elegance.

More than a decade on, and most people are still living out of text editors and glorified **#TODO** lists. In the words of moral philosopher Curtis Stigers, *I wonder why?*

The least interesting reason is the hype-train running out of steam. An explosion of self-justifying tooling, and endless podcasts and videos simply weren't sustainable. I would know, I read and engaged with much of it! Despite building up the problem to such an extreme, there are only so many ways one can optimise a list of tasks, or allocate finite resources.

But I've been thinking about this at a macro level a lot more lately. I think people have started cottoning onto it being misdirection, whether well intentioned or not.

Productivity around the world has been increasing faster than real incomes for decades. The problem isn't personal productivity, it's that people are expected to do more for (and with) less. Productivity porn suggests these structural problems lie with the individual: it's *their* fault for not thriving in such an environment, and *their* fault for the ever-increasing demands on their attention and time. Therefore, the solution is this over-engineered software or expensive seminar. *No, don't look at the cause!*

The more recent idea of *hustle culture* is the other side of the same coin. Productivity gurus want to chide people, claiming the burned out are lazy, disorganised, or lacking discipline. But the reasons for their circumstances go far deeper.

The proof's in the pudding. The world is a complicated place, but adding another framework atop it clearly didn't resonate at scale. Proponents may chalk this up to laypeople not understanding their message, but that's as much on them not clearly stating their case, or not acknowledging the reasons for people being in this pinch in the first place.

People who offer help with *both* issues will be the ones who make a lasting impact in this space, rather than regurgitating another short-lived **#TODO** list.
