---
title: "Loading your website experience"
date: "2023-04-09T18:16:30+10:00"
abstract: "THAT’S why my blog has always been so niche. It doesn’t have an experiece."
year: "2023"
category: Internet
tag:
- design
- jargon
- pointless
location: Sydney
---
I just went to a corporate blog that showed nothing but a blank screen, followed by a single bouncing line reassuring me it was "loading your website experience".

A "website experience"!? And this whole time I was expecting to just read something! Maybe my blog has remained niche this whole time because it lacks a sufficient "website experience".

What would a "website experience" entail? Some flying toasters? A game of life clone? Something I'd immediately want to write to my friends and family about?

Alas, after a few years of waiting for this "website experience", I gave up and went to get coffee. Upon my return, I saw some corporate blog. This was not the "website experience" I'd been looking forward to as I waited in that coffee shop queue, expectant for what I was about to receive.

I checked the page source, and "loading your website experience" meant a stub page with a few million lines of JavaScript. No fallbacks, no graceful degradation, no text, nothing.

For shiggs and gittles, I opened the site in a text browser, and didn't even get the customary "you need JavaScript enabled to view the completely static text on this page". It was completely empty; devoid of all meaning and existence. I know what that feels like.

Maybe for accessibility, we need to demand nuanced, paradigm-disruptive synergies with our "loaded website experience".
