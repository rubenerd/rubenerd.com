---
title: "HTML5 doesn’t like self-closing void elements"
date: "2023-11-27T14:39:45+11:00"
abstract: "This sounds like a problem with unquoted attribute values, not polyglot elements."
year: "2023"
category: Internet
tag:
- html
- specs
- xml
location: Sydney
---
The HTML5 validator has started issuing information about elements with a forward-slash:

> **Info:** Trailing slash on void elements has no effect and interacts badly with unquoted attribute values.
>
> ==> &lt;hr /&gt;

HTML has accepted XML-style self-closing tags since the XHTML 1.0 days, but clearly there are rumblings that this may change. The validator references [this document on GitHub](https://github.com/validator/validator/wiki/Markup-%C2%BB-Void-elements#trailing-slashes-directly-preceded-by-unquoted-attribute-values "Trailing slashes directly preceded by unquoted attribute values"), and even the [HTML5 spec](https://html.spec.whatwg.org/multipage/syntax.html#start-tags "13.1.2.1 Start tags"), that argue that trailing slashes in void elements are bad, because they can confuse parsers interpreting an unquoted attribute value.

For example, this would return `logo.svg/`, which doesn't exist:

> &lt;img alt=SVG src=http://www.example.com/logo.svg/&gt;

This sounds like a problem with unquoted attribute values, not polyglot elements. 

If HTML is a living specification based in part on real-world usage, and if we want to instill "the right mental models" for writing robust markup, I'd advocate for quoted attribute values instead. Using bare attribute values has been considered bad practice for years, not least because of edge cases like this.

*(Though I suppose I should equally give up on the idea of XHTML too. It was such a convenient Trojan Horse to encorage better markup).*
