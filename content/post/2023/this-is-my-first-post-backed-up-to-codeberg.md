---
title: "This is my first post backed up to Codeberg"
date: "2023-03-15T19:06:49+11:00"
abstract: "If you’re reading this, my move to Codeberg was successful."
year: "2023"
category: Internet
tag:
- archiving
- git
location: Sydney
---
If you're reading this, it means my blog source (mmm, sauce) backup has successfully migrated to [Codeberg](https://codeberg.org/rubenerd). I'll update my source namespace references and other links in the coming days.

Thanks to [Screenbeard](https://the.geekorium.au/) for the idea. Let me know if you have an account and host some cool stuff that I can follow :)
