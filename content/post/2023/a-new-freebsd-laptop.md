---
title: "Researching a new FreeBSD or NetBSD laptop"
date: "2023-03-13T08:46:13+11:00"
abstract: "It’ll probably be a ThinkPad, but will need to review."
year: "2023"
category: Hardware
tag:
- bsd
- freebsd
- netbsd
location: Sydney
---
I love my tiny Japanese Panasonic Let's Note RZ6 subnotebook, but the keyboard is *just* small enough that I can't type with speed. It's a perfect "on call" machine, but I'd love something I can write longform stuff on.

The problem is, Panasonic and Apple have spoiled me with their 2× HiDPI displays. I love how images look, the higher fidelity of fonts in documents, and the ability to crank text down to smaller sizes when tailing logs. People saying these are silly luxuries were writing into PC Magazine saying 8-bit colour was sufficient in the 1990s, and that you don't need double-density floppy disks because in my day we used punch card libraries, *dagnabit!*

This would be my wish list:

* 144 DPI IPS display, 12 or 13-inch would be ideal
* User-serviceable storage for upgrades and removal
* Solid keyboard
* Lightweight(ish)
* Good FreeBSD and/or NetBSD support, such as Wi-Fi
* RS232? One fewer dongle would make my life glorious

My instinct is to buy a second-hand ThinkPad and be done with it, as is tradition in the BSD community. I'll have to see if anything fits.

