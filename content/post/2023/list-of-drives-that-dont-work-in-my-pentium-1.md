---
title: "A legacy PC drive update: little works!"
date: "2023-02-13T06:52:41+11:00"
abstract: "Some more fun getting any damned storage working on either my Pentium 1 or P3 tower."
thumb: "https://rubenerd.com/files/2023/storage-fail@1x.jpg"
year: "2023"
category: Hardware
tag:
- retrocomputing
- storage
location: Sydney
---
Sorting out storage in my Pentium and Pentium III towers are a bit of a comedy of errors at this stage. Today on the retrocomputer desk, we're checking out everything that has failed.

Every replacement drive I've tried hasn't been detected by the BIOS, or hangs the machine during POST. This is regardless of jumper settings, swapping ribbon cables from 40 to 80 and back, swapping power supply connectors, setting LBA or not, disconnecting everything else from the machine, and swinging a cat... 6 cable through the air.

Thus far I have this pile of silicon, none of which works:

1. A **consumer SanDisk SD card**, in SD to IDE adaptors
2. An **industrial SanDisk SD card**, in SD to IDE adaptors
3. A **Transcend Industrial CF card**, in passive CF to IDE adaptors
4. A **SanDisk SATA SSD**, with a SATA to IDE adaptor
5. A **Transcend 2.5-inch IDE SSD**, in two 2.5 to 3.5 adaptors
6. A **Post-it note** inscribed with the words *WA LAO EH*.

Each adaptor and device was verified in a modern system, and swapped out at least once with another model, with the same result. All in all, it's been over a year of testing a part, having it fail, ordering a different one, waiting another month, trying, rinse, repeat. I even bought a PCI SATA controller on a lark, which also worked in neither machine. Good luck trying to troubleshoot this by doing web searches as well, as SEO spam has made this all but impossible.

Curiously, old **SanDisk Ultra 16GB** and **SanDisk Extreme 32GB** cards *do* work, but are glacially slow, to the point where old spinning drive benchmarks out perform them. The Post-it note was also one of the few devices that didn't hang either machine on POST. Thanks, I'm here all week, under a pile of unusable electronics.

<figure><p><img src="https://rubenerd.com/files/2023/storage-fail@1x.jpg" alt="A pile of unusable parts, as per the list above." style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2023/storage-fail@2x.jpg 2x" /></p></figure>

Both machines use completely different chipsets, so I suspect there's something more fundamental going on. Maybe these machines depend on cylinders and sectors being defined manually for drives it can't detect, and I need to input pseudo-values for these. I have *very* vague memories of doing this on a friend's ancient 386 back in the day, but what *would* I set an IDE SSD to for those values?

To verify the machines otherwise worked, I connected my last mechanical hard drive to it using one of the failed SATA to IDE adaptors. It has a ton of bad sectors and I'm sure is close to shredding itself, so I wouldn't want to use it for actual data. But sure enough, **both machines detected this SATA hard drive** and attempted to boot from it. That lends credence to the theory that it's a cylinder/sector issue, or maybe something about what the solid-state drives are reporting to the host system.

The only type of drive I haven't tried is the **Disk on Module**, which is another type of solid-state industrial storage that connects directly to the motherboard. It's *extremely* expensive for the capacity though, and scarce as hens teeth unless I want to pay a few million extra dollars in shipping.

I'm burned out with these machines, so I'll be taking a break while I work on the Commodore machines again. *Se a vida é*.
