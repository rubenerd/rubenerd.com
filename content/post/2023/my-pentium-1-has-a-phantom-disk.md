---
title: "My Pentium 1’s phantom hard disk"
date: "2023-06-21T08:01:18+10:00"
abstract: "It takes the capacity of whatever the first IDE drive is. Weird!"
year: "2023"
category: Hardware
tag:
- pentium-mmx
- retrocomputing
- storage
location: Sydney
---
My childhood PC has started exhibiting strange behaviour with its internal hard drives. Any OS I boot reports seeing **two** internal drives, even when only one physical CompactFlash card is present. It didn't do this before, and I'm stumped as to why.


### What the BIOS initially detects

The BIOS correctly only autodetects the one CompactFlash card connected to the primary IDE interface of the board, along with my Creative Infra48 optical drive on the secondary IDE interface:

    Detecting HDD Primary Master   ... SDCFXS-032G
    Detecting HDD Secondary Master ... CREATIVECD3220E

The second BIOS screen also reports the correct drive:

    Pri. Master Disk : LBA,UDMA 7,32021MB

As an aside, this SanDisk Extreme 32 GiB card seems to be the sweet spot for this machine. It inks out all the performance I can get out of this legacy IDE controller, and the capacity has full BIOS support (for once)!

### Here's where thing get weird

When I launched **fdisk** on a Windows 95 boot disk to partition this CF card, I noticed this option appear:

    5. Change current fixed disk drive

This only appears when there's more than one internal fixed drive attached on the IDE interface, to permit you partitioning a different disk. But as the BIOS showed above, only one such device exists. What does this mean?

Pressing 5 presents this nonsensical list of fixed drives:

    Change Current Fixed Disk Drive
        
    Disk   Drv  Mbytes  Free  Usage
    1           30514         100%
           C:   30514
    2            1024         100%
           D:   30514          

I... wait, what?! It reports **Disk 1** correctly, but then it claims to see a non-existent **Disk 2** with the same logical partition size as my single CompactFlash card, despite only having 1 GiB of physical capacity. This isn't physically possible; it'd be akin to saying the 500 mL of drink in our fridge has the same volume of liquid as the 2 L bottle next to it.

The fact the capacity of real drive is always shown as the capacity of the phantom one, I suspected maybe the CF card itself was misbehaving. I swapped the card for three others of varying capacities, and even an old 8 GB Maxtor I use for testing. In every case, the capacity of that drive was presented as the size of drive D as well.


### Conclusion

There isn't one, yet. I'll have to keep digging the next time I get an opportunity to do some testing. I've been building computers since the late 1990s when I was in school, and I've never encountered something like this before.

My worry is something has silently failed on this board, which I suppose is to be expected for retro computer hardware. This Pentium 1 is already older than some young adults in university and the workforce, which makes me feel rather old.
