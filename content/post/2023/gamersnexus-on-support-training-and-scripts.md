---
title: "GamersNexus on support training and scripts"
date: "2023-02-25T10:53:52+11:00"
abstract: "“The fault falls on whomever built the QC process, and failed to train employees.”"
year: "2023"
category: Hardware
tag:
- gamersnexus
- reviews
- support
location: Sydney
---
Speaking of [hardware repair and support](https://rubenerd.com/the-social-effect-of-sealed-devices/), Steve recently uploaded a review of [another disappointing prebuilt game machine](https://www.youtube.com/watch?v=bflZYG5DWPg), which had eye-opening flaws for the price. 

But the real insight was his comments about support:

> The operators who use those [support] scripts, have to be trained on what any of it means. It doesn't matter how much data you collect, if the people looking at it don't know what it means.
> 
> With scripting, with software detection of failures of computers, it changes constantly. You have to keep up ... or you're not going to be checking the right things.
>
> So simply saying "we have [support] scripts now"... that's not enough.

And the broader issue of culture:

> ... you could say the [quality control person] screwed up, they signed off on it, and shipped it to a customer who happened to be GamersNexus ... realistically, the fault falls at a higher level. It falls on whomever built the QC processes, and failed to train the employees on what to look for.

In the lingo of social media, I wish I could upvote this twice. In all the incident reports, notices, and apologies I've ever received for technical issues, this is rarely (if ever) acknowledged.

If you'll forgive the indulgent self-quote from my post earlier this month about [ascribing blame in complicated systems](https://rubenerd.com/confirmation-bias-in-complicated-systems/):

> I’m baffled how often discussions about accidents, exploits, or other technical issues focus myopically on operators, and not the circumstances and system design that lead to those fateful decisions being taken.

It's worth mentioning too that this sentence has ten words, not including these ones. That's something you should probably leave off a support script.
