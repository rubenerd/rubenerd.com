---
title: "Music Monday and Dreams"
date: "2023-06-19T14:14:54+10:00"
abstract: "My favourite song from the Cranberries, and an email from Antranig."
thumb: "https://rubenerd.com/files/2023/yt-Yam5uK6e-bQ@1x.jpg"
year: "2023"
category: Media
tag:
- antranig-vartanian
- dreams
- music
- music-monday
- language
location: Sydney
---
This post comes from having that Cranberries song stuck in my head again, and from a fun email I received from Antranig Vartanian about a dream in which I played a small part!

First, to something completely pointless. This sentence doesn't have a single word containing the letter queue. See how I spelled the word without using the letter to... wait, damn it.

Moving on, *Dreams* was one of those songs that seemed to be everywhere in 1990s when I was growing up, and for good reason. Dolores' vocals, the composition, instrumentation, lyrics; there aren't that many songs where you can say you wouldn't change a single thing. I'm sure I've mentioned here a few times before, but it's one of my all-time favourites.

<figure><p><a target=_BLANK href="https://www.youtube.com/watch?v=Yam5uK6e-bQ" title="Play The Cranberries - Dreams (Dir: Peter Scammell) (Official Music Video)"><img src="https://rubenerd.com/files/2023/yt-Yam5uK6e-bQ@1x.jpg" srcset="https://rubenerd.com/files/2023/yt-Yam5uK6e-bQ@1x.jpg 1x, https://rubenerd.com/files/2023/yt-Yam5uK6e-bQ@2x.jpg 2x" alt="Play The Cranberries - Dreams (Dir: Peter Scammell) (Official Music Video)" style="width:500px;height:281px;" /></a></p></figure>

The *act* of dreaming hasn't been friendly to me of late though. I don't tend to have nightmares anymore, but regularly wake up in a pool of anxiety having dreamed about a complicated situation. It usually involves moving house, failing to pack in time to make a flight, missing an important work deadline, being told I can't do something because the opportunity closed. It's as though my subconscious is tying to tell me something.

Friend of the blog and Armenia's most handsome FreeBSD engineer Antranig emailed to say a more recent dream was significantly more pleasant:

> I was home where you gave me a call saying that it’s a very bad weather outside, asking if you can come into my place until the rain stops and I said sure. 
> 
> Next thing I know I’m running downstairs to greet you but I can’t see you so I use our phones’ GPS to find you in a rain storm. 
> 
> You do show up in the end and you say “nice t-shirt, haven’t seen it before” and I reply “oh yeah, Lilith’s been wearing it, I guess the only con in living with your SO is that all your tshirts and hoodies get shared, no questions asked”
>
> And then the elevator stops and I wake up.

I tell you what, I couldn't think of many more things I'd rather be doing that having lunch with Antranig in his home in Yerevan right now! Though I will say my significant other is tiny, so any hoodies or event outfits she wears of mine come down to her knees. You do see people in trendy parts of Asia wearing long shirts like this with a belt and boots, but it might raise eyebrows here.

I like to think that a version of me out there is giving someone a fun dream, and not a nightmare. Though I'm sure my Armenian dream version can write the այբուբեն a lot better than I could here. Heck, even Latin is a challenge.

[Antranig Vartanian's website](https://antranigv.am/)   
[Celebrating 5 years of English posts (շնորհավորում եմ)!](https://weblog.antranigv.am/posts/2023/05/5-years-of-blogging/)
