---
title: "Mashu’s Pure Hearted Shield"
date: "2023-08-09T09:15:16+10:00"
abstract: "The related game came out a while ago, but I’ll take it!"
thumb: "https://rubenerd.com/files/2023/mashu-melty-1@2x.jpg"
year: "2023"
category: Anime
tag:
- fate
- fate-grand-order
- games
- mashu
- mobile-games
location: Sydney
---
We got a new craft essence in *Fate/Grand Order*, to celebrate the launch of *Melty Blood: Type Lumina*. Granted the release date was actually more than a year ago Japan (we get things later in the English version), but I'll take it!

<figure><p><img src="https://rubenerd.com/files/2023/mashu-melty-1@1x.jpg" alt="" style="width:500px;" srcset="https://rubenerd.com/files/2023/mashu-melty-1@2x.jpg 2x" /><br /><img src="https://rubenerd.com/files/2023/mashu-melty-3@1x.jpg" alt="" style="width:500px;" srcset="https://rubenerd.com/files/2023/mashu-melty-3@2x.jpg 2x" /><br /><img src="https://rubenerd.com/files/2023/mashu-melty-2@1x.jpg" alt="" style="width:500px;" srcset="https://rubenerd.com/files/2023/mashu-melty-2@2x.jpg 2x" /></p></figure>
