---
title: "macOS’s experiment in rounded-square icons"
date: "2023-03-15T15:30:38+11:00"
abstract: "This experiment has run its course. It’s time to ditch them."
thumb: "https://rubenerd.com/files/2023/mac-square-icons@2x.png"
year: "2023"
category: Software
tag:
- apple
- design
- macos
location: Sydney
contributer:
- https://www.apple.com/macos/
---
I can't remember when Apple began encouraging rounded squares for application icons in Mac OS X, OS X, macOS, or whatever they're calling it now. Not-iOS OS? 
But it's safe to say this experiment in reduced visual accessibility has been a failure in all but the strictest of senses.

I present this screenshot of my Dock as evidence:

<figure><p><img src="https://rubenerd.com/files/2023/mac-square-icons@2x.png" alt="Screenshot of a row of icons, almost all of which consist of a white rectangle with an icon in the centre." style="width:500px; height:79px;" /></p></figure>

Now granted, my preference is generally for open-source software, most of which tend not to have macOS-specific design affordances. But even among the commercial tools I have to run, and even Apple's own software, the trend is clear. Of those who changed their icons to conform, their designers set their icons on a white background and called it a day. Or a night, or whatever time of day they work.

These icons are the graphical design equivalent of conforming to the letter of the law, but not the spirit. Is the icon a rounded square, your honour? *Absolutely!* But, I mean, is it *really...?* Not so much.

And can you blame them? Of course you can, you can blame anyone for anything, regardless of merit. Last week a motorist blamed me (with a horn) for inconveniencing them, given I was crossing the street on a green pedestrian light. The *[hide](https://www.macmillandictionary.com/dictionary/british/have-the-hide-to-do-something)* of some people!

But a reasonable person probably couldn't blame them. Some icons are liquid enough to conform to a rectangular container, like MacVim or Zoom's uninspired word mark. Others required minimal modification, like the Adobe suite which became square years ago during their "elements" rebranding. But it's clear from viewing any current Applications folder that most aren't.

There's likely a simple reason. macOS isn't sufficiently large or important a platform for most companies, developers, or projects to change their visual identity to conform to Apple's whims, or to maintain separate icon styles across platforms. Software like the Lagrange Gemini browser have gorgeous icons that use the shape as a picture frame, but they're the exception. Apple doesn't even do it consistently, so where's the motivation for others to?

There's no shame in admitting mistakes: I'd be in *big* trouble otherwise. Bring back shapes Apple, you know you want to.

