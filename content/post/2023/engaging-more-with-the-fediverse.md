---
title: "Engaging more with the Fediverse and IndieWeb"
date: "2023-11-26T10:27:48+11:00"
abstract: "I’m happy it exists, but I’m also a turtle."
year: "2023"
category: Ethics
tag:
- fediverse
- indieweb
- privacy
- weblog
location: Sydney
---
It's been so much fun seeing independent movements such as the [Fediverse](https://www.fediverse.to/) and the broader [IndieWeb](https://indieweb.org/) gain traction in recent years. They're not just reactions or counterpoints to dystopian commercial sites, they're proof that we can build a vibrant, sustainable web without insidious inventions like tracking, "engagement", and malware disguised as advertising.

Thinking back, it reminds me of those early days of the *blogosphere*. Anyone could write, link to, and share ideas thanks to tools like pingback and RSS. All this technology still exists, but is being expanded upon by a new generation. It makes me smile just writing this.

This leads me to doing a bit of soul searching as well. I've always had my own site, even before it was turned into a blog in late 2004. But I never engaged in things like webrings, even as a kid when everyone was connecting their Tripod and GeoCities sites to them. I was also quick to disable certain features that might have fostered collaboration or sharing out of fear of trolls and spamming. My brief experience with blog comments back in the day was so profoundly negative, it soured me on it for years.

The site you're reading today is "static". I mean that more than just the CMS; there are no external mechanisms to electronically ping me whatsoever. The only hole I've punched in the firewall, so to speak, is an email address. That has turned out to be a *tremendous* bottleneck, but it still weirdly feels like one I can control and manage.

Maybe this is the extent to which I can interact with the modern web as an introvert who writes what's basically a journal, and who happens to publish it as a side effect. Maybe it's also why I'm so bullish about tools like RSS. I can subscribe to people, read their responses on their blogs, and respond in kind. That way, the turtle can retreat into his shell when he needs to. 🐢

That's probably the biggest benefit of these independent movements. You interact with it on *your* terms, not what some psychopathic billionaire wants you to. For that, I owe these movements a great deal of respect and gratitude.
