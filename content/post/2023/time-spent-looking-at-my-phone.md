---
title: "Time spent looking at my phone"
date: "2023-01-08T08:35:59+11:00"
abstract: "Running a personal experiment to justify their intrusions."
year: "2023"
category: Hardware
tag:
- health
- personal
- phones
location: Sydney
---
I gave myself a few goals to meet this year, one of which was to be more aware of how I use my phones. Whenever I reach for them, I take deliberate note and have them justify their intrusion, or their flip cases remain closed. I've found this reframing useful; I'm not denying myself permission, I'm the gatekeeper. Phones aren't the boss, *I am!*

My motivation was to see if my suspicion was true that I was filling a lot of time with mindless use of these devices, and for bonus points if they're contributing to stress, shorter attention spans, fatigue, and a lack of mindfulness. I've already delegated certain things to a separate music player, ebook reader, and camera, so I figured it was the next logical step.

I was pleasantly surprised that I *do* reach for a phone for productive uses more than I expected. I respond to messages, look up vintage computer specs, run currency conversions, check foreign timezones, do some basic calculations, look up the opening hours for a coffee shop, etc. A phone in this capacity is like a scientific calculator mixed with an encyclopaedia… the ultimate device I could have only dreamed about before smartphones.

The other observations were a tad unsettling. The most alarming is what I'm dubbing the lucid dream effect. I've become more attuned to when I'm reaching for my phone, but I've still caught myself mindlessly scrolling well after the fact. It's become so rote, in some cases I had no recollection of picking it up in the first place. It's like realising you've been in a dream this whole time.

Mostly though, I've realised just how often I reach for it to fill a spare moment. This is unsurprising; it's conventional wisdom that we're hooked on these devices and crave the sugar hit and dopamine that comes from a brief, superficial distraction. But I think we kid ourselves by thinking *yeah, but I'm smart, and that doesn't happen to me!* It does.

Walking to the coffee shop this morning to write this post, I caught myself reaching for the phone in the stairwell, walking down the street, waiting for the traffic light, waiting for the other traffic light, walking down another street, and while I was in the queue to order. None of these activities took more than a few minutes, but the pattern of behaviour has become so established that I didn't give it a second thought.

And I've already noticed a change, even after just a week. There isn't a way to talk about these without sounding cliché, but I've become more aware of my surroundings. I've smiled at more people! I look out the window more. But the biggest shift has been a higher tolerance for boredom. I can tolerate brushing my teeth for a few minutes without needing to check who's mentioning me on a thread about something.

We can't use these devices on our own terms. We're expected to be on call and available 24/7, lest we be considered negligent in our jobs, or rude to our family and friends. But we can claw back a bit of control.
