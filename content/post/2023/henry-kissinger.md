---
title: "Henry Kissinger"
date: "2023-12-01T08:57:37+11:00"
abstract: "I’m just sad now he won’t have to answer to an Earthly court for his crimes."
year: "2023"
category: Thoughts
tag:
- politics
location: Sydney
---
[Anthony Bourdain, via Wikiquote](https://en.wikiquote.org/wiki/Anthony_Bourdain)\:

> Once you’ve been to Cambodia, you’ll never stop wanting to beat Henry Kissinger to death with your bare hands. You will never again be able to open a newspaper and read about that treacherous, prevaricating, murderous scumbag sitting down for a nice chat with Charlie Rose or attending some black-tie affair for a new glossy magazine without choking. Witness what Henry did in Cambodia&mdash;the fruits of his genius for statesmanship&mdash;and you will never understand why he’s not sitting in the dock at The Hague next to Milošević. While Henry continues to nibble nori rolls & temaki at A-list parties, Cambodia, the neutral nation he secretly and illegally bombed, invaded, undermined, and then threw to the dogs, is still trying to raise itself up on its one remaining leg.

