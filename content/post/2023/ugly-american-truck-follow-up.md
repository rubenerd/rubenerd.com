---
title: "Ugly American truck follow up"
date: "2023-08-21T15:34:28+10:00"
abstract: "The sentiment among my American readers is that these trucks are ubiquitous, but unwanted."
year: "2023"
category: Thoughts
tag:
- cars
- environment
- trucks
- urbanism
location: Sydney
---
I haven't had that much follow up for a post in a while:

[Ugly American trucks](https://rubenerd.com/ugly-american-trucks/)

The sentiment among my American readers is that these trucks are ubiquitous, but unwanted. They make driving in anything other than a large truck scary, because they wreck your visibility even in optimal conditions. They also tend to be driven by oblivious people or jerks, which I can appreciate from personal experience.

Aaron in Milwaukee told a harrowing story of his hatchback being forced off a road in sleety conditions by a Dodge RAM driver who was either asleep or malicious. Chuck in Austin discussed a move back to the Pacific Northwest, in part due to being fed up being threatened by the sheer number of these trucks in Texas. A person with an anonymous address expressed sadness that her husband and youngest son had been sucked into these things, and she worried about the safety of her other children.

A minority view was that these trucks might be ugly, fuel inefficient, take up a lot of space, and are bad for hauling freight, but people have a right to drive them. Fortunately, Daniel (not Mr O'Conner from a recent post) emailed to say he hadn't even considered his impact on other urban road users and pedestrians until he'd started binging urbanist videos on YouTube, and was considering getting something more practical.

This makes me optimistic. It's proof that discussing and sharing issues like this is a way we can change minds.
