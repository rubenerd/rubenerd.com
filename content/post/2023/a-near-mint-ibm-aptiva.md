---
title: "A near-mint 1999 IBM Aptiva desktop"
date: "2023-06-12T10:58:09+10:00"
abstract: "A desktop from eBay that ticks off another childhood dream."
thumb: "https://rubenerd.com/files/2023/aptiva-touching-grass@1x.jpg"
year: "2023"
category: Hardware
tag:
- 1990s
- aptiva
- nostalgia
- pcs
- retrocomputing
location: Sydney
---
I appreciate from the outside this is starting to look a bit silly, but *I could stop buying these at any time!* Am I trying to convince you, or me?

I've made no secret of the fact that I harbour tremendous affection for 1980s home computers, and 1990s PCs. The former are fascinating snapshots of an era I wish I could have lived through, and the latter are... what I lived through. Having a salary in my 30s has given me the opportunity to buy some of the things I so badly aspired to have at the time, which has let me live out some of my sillier childhood dreams. If we're going to spend most of our lives working, why not?

Well, other than for the reason that those of us living in tiny apartments don't have space! But I'll get back to that.

Late 1990s beige boxes haven't quite reached the cult-like following that earlier retrocomputers have, but prices *are* ticking up. Generic-looking Pentium III towers or Apple boxes that would have been recycled even five years ago are now fetching low three figures on eBay. As the supply starts to dwindle, and more people gut them for sleeper PC cases, I expect these prices to shoot up further.

Which leads us to the star of today's post, this beautiful old IBM desktop. I found it on eBay quite by accident while looking for something else. It was ridiculously cheap, fully functional, and cosmetically perfect. Even more dangerously, the seller lived within a 45-minute walk from our apartment.

<figure><p><img src="https://rubenerd.com/files/2023/aptiva-touching-grass@1x.jpg" alt="Photo of the Aptiva desktop touching grass in Sydney suburbia." style="width:500px; height:375px;" srcset="https://rubenerd.com/files/2023/aptiva-touching-grass@2x.jpg 2x" /></p></figure>

What proceeded was a lovely mid-morning stroll, followed by some sore arms as I carried it back. Clara and I even found a cute new coffee shop we didn't know existed on the periphery of Chatswood, which raised a few eyebrows as I sauntered in with a desktop that was older than the barista who made me coffee.

Ah jeez, why did I just think about that. 1999 was ten years ago, right?

I never had IBM hardware at the time, but I *loved* the design language of their desktops. They incorporated those fake-grills Apple pioneered in the 1980s, along with sleek lines and a minimal aesthetic. Whereas Compaq went for rounded forms in their moulded plastic for their *Presario* desktops, IBM embraced the boxiness of their cases while still creating something a bit more unique than an OEM like Dell. They even still used that serif font!

Here's the Aptiva (right) alongside my HP Brio (centre) and Compaq Presario from the time period. It's fun seeing the differences.

<figure><p><img src="https://rubenerd.com/files/2023/aptiva-friends@1x.jpg" alt="Photo of the Aptiva alongside the other desktops." style="width:500px; height:375px;" srcset="https://rubenerd.com/files/2023/aptiva-friends@2x.jpg 2x" /></p></figure>

The machine has an AMD K6-III, which is comparable to what the Presario has. But as I'll detail in a future post, the IBM uses entirely standard components and positioning, which should make upgrades easier. Unless you had specific nostalgia for that Compaq hardware like I did, I'd stay well clear of them today if only because they're such a *monstrous* pain in the behind to work on and replace parts for. I'd put that business-oriented HP Brio somewhere in the middle, *as I literally did* here.

I also need to find space for it; I did do another massive cleanout of old stuff recently, but ideally I'd need a shelf under the desk to put it. Australian readers: do Bunnings cut wood to specific sizes before or after you buy a sausage?

I do think this will be my last beige box. Absolutely.
