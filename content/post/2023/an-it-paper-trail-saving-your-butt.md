---
title: "An IT paper trail saving your butt? Well, maybe"
date: "2023-12-18T09:29:17+11:00"
abstract: "Having evidence doesn’t help if you don’t set expectations."
year: "2023"
category: Thoughts
tag:
- soft-skills
- work
location: Sydney
---
The Register [reported an all-too-familiar story](https://www.theregister.com/2023/12/15/on_call/ "You don't get what you don't pay for, but nobody is paid enough to be abused") of an engineer having to deflect anger and indignation with a legal document:

> Doug's tour of the office to make sure service was restored was going well, until a director shouted "Hey, you, this is crap, this is not the service we pay for!" Then proceeded to offer similar abuse throughout the office at unmissable volume.
> 
> Doug waited for a gap in the rant, before stating "Actually, this is precisely the service you paid for" and producing a printed copy of one of the mails he had sent about proper service level guarantees.

We've all been there, and as The Reg reminds us, "nobody is paid enough to be abused". While I relish these sorts of stories in a *comeuppance* sense, they don't mirror my experience.

The challenge I've found isn't that I have evidence, but that people's **expectations** aren't congruent with it. People have this impression of documents like this being a paper mic drop, but this wouldn't end the conversation at all. In some cases, it may only serve to inflame.

When expectations aren't met, no amount of correctness will dissuade someone from being angry, and drop you as an employee, client, or supplier. "It's not my fault, but it's now my responsibility", as Marco Arment once put it on an old episode of *Build and Analyse*. Legal documents, strictly speaking, only protect you from liability.

Armchair legal experts, like those frequenting social media, struggle with this concept. You'll be told to quit or lose a client, and sue them into oblivion for any indiscretion you can prove with a piece of paper. Those of you who live in the real world know where the issue is here: it's important to set boundaries, but sometimes the opportunity cost isn't worth it. Unfortunately, abusive people know this too.

Those same experts intuit the flaw in their logic it in other ways. If your partner is baking, and they message you to pick up *flower* instead of *flour*, see how far you get when you deliberately deliver a bouquet. *It's what you said, see, I'm in the clear!* It's no different in business, whether we'd like it to be or otherwise. Business is made of squishy people, not compute units (another reason why the whole concept of "smart contracts" on blockchains was nonsense).

Perhaps it's a weeding exercise. Those who would exploit you aren't worth working for or with. In an ideal world, where we're all dancing around with flowers and permissible downtime. The real world is greyer.
