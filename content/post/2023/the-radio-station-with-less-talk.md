---
title: "The radio station with less talk"
date: "2023-06-25T09:13:29+10:00"
abstract: "Before launching into a long news break and ads."
year: "2023"
category: Media
tag:
- ads
- bsd
- grilled-cheese-sandwiches
- music
- netbsd
- radio
location: Sydney
---
A local coffee shop I love pipes FM radio through their speakers. It's an interesting system; rather than decoding a digital stream from a packet-switched network, it encodes a signal using frequency modulation broadcast from a tower in your area. You don't need Internet access, nor do you have to pay access fees.

One of their advertisement bumpers claims they have "less talk", usually before launching into a news break and a long series of ads. It's delivered with earnest excitement, so I don't *think* they're being sarcastic.

Maybe I need to claim that Rubenerd.com has "less waffle" before I launch into a digression about eating grilled cheese sandwiches while updating a package manager. The secret is to use more than one kind of cheese, because you want them melting at slightly different rates. The Swiss have this figured out, and to a lesser extent the French. Maybe I need to seek someone from Geneva, or the wider Romandy region, to make me a croque monsieur. While updating pkgsrc.

[Wikipedia: Croque monsieur](https://en.wikipedia.org/wiki/Croque_monsieur)  
[pkgsrc.se](https://pkgsrc.se/)   
[Grilled cheese sandwich archive](https://rubenerd.com/tag/grilled-cheese-sandwiches/)
