---
title: "Memories of Lotus Organizer"
date: "2023-08-13T10:26:55+10:00"
abstract: "My favourite tool from my childhood still hasn’t been surpassed."
thumb: "https://rubenerd.com/files/2023/lotus-organizer-61.png"
year: "2023"
category: Software
tag:
- filofax
- lotus-organizer
- retrocomputing
location: Sydney
---
The transition of this blog into a FreeBSD and retrocomputing site was mostly an accident! But in keeping with this theme, I've decided to start a new series dedicated to showcasing some of my favourite software from the past. I still think there's a lot we can learn from these tools.

And what better way to start than with the king of them all for me, *Lotus Organizer.* This personal information management (PIM) tool was originally released by British dev *Threadz* in the early 1990s. Taking design cues from the Filofax I always wanted as a kid, it organised your calendar, todo lists, contacts, notes, yearly planner, and anniversaries into tabs. Later versions also incorporated website links.

Here's a screenshot of version 6.1 from 2003, the final version before IBM retired SmartSuite and Organizer:

<figure><p><img src="https://rubenerd.com/files/2023/lotus-organizer-61.png" alt="Screenshot showing Lotus Organizer's tabbed interface, currently showing To Do list items" /></p></figure>

The experience of using Organizer was exactly what you'd expect. Clicking the tabs along the side would bring you to that section, just as you'd navigate a paper organiser. Each section had its own set of local tabs, such as years for the Calendar, letters for Contacts, and priority for To Do lists. You could also sort elements from the grey bar at the bottom of each page, and customise the appearance of each tab.

I *loved* this skeuomorphic design as a kid, and learned to appreciate it as I got older. I maintained meticulous Contacts, planned much of my life in the Calendar and Planner, and the Notepad tab served as my *Zettelkasten* for years. Granted it was less of a business tool and more a childhood novelty, but it was *fun* in a way no other PIM has been since. Heck, I was even still using it in Wine long after I left Windows and school behind.

<figure><p><img src="https://rubenerd.com/files/2023/lotus-organizer-custom.png" alt="Screenshot showing the Customise tab with textures and colours." /></p></figure>

Reviews at the time were mixed. Anyone familiar with Lotus Notes by the late 1990s probably wanted out of that entire ecosystem. Fans of Lotus Agenda on DOS hoping for a GUI version were disappointed. But most of all, detractors panned its UI as a waste of time and screen space, in arguments that foreshadowed the hipster backlash against Steve's *rich Corinthian leather* in Mac OS X two decades later.

I appreciate it comes down to personal preference, but I miss when software had personality, or at least the option for it. Contrasted with our crushingly dull world of modern flat UIs where visual cues are aberrations, *Organizer* had depth and a bit of whimsy in just 256 colours. And in *enterprise* software that one has to look at every day!?

That's its lasting legacy for me. Some people might have yearned for a more orthodox desktop PIM package, but I loved flipping through the pages in my virtual binder. They didn't *need* to add fake leather texture or customisable colours to pull off the metaphor, *but they did anyway.* I wish more modern software dared to do this.

In future posts, I might do a deeper dive into some of its functions, and how the software evolved over the years. Mark it in your Filofax :).
