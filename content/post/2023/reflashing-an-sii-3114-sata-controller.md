---
title: "Reflashing an SiI 3114 RAID into a SATA controller"
date: "2023-12-28T13:41:38+11:00"
year: "2023"
category: Hardware
tag:
- dell-dimension-4100
- hard-drives
- raid-controllers
- retrocomputing
- ssds
- storage
- windows-2000
location: Sydney
---
Last year I picked up a random Silicon Image (SiI) PCI SATA controller, in the hopes I could use it to add SATA storage to older PCI machines with just IDE. While Windows 2000 could install on it with the requisite drivers, I could never get it to boot.

*Turns out*, reflashing the card from a SATARAID to a SATALink controller lets it boot, and all it took was some digging to find the requisite files and utilities!


### Initial configuration

The card first appeared after POST as a RAID controller, which did show the correct drive attached to SATA port 1:

    Sil 3114 SATARaid  BIOS Version 5.4.03
    Copyright (C) 1997-2006 Silicon Image, Inc.
       
    Press <Ctrl+S> or F4 to enter RAID utility
    1   SanDisk SDSSDA120G                    111 GB

Pressing **F4** resulted in a menu where various RAID configuration options could be set. I couldn't see any option to make the drive bootable, and it wouldn't let me create a logical volume from this single disk. *Phooey.*

Interestingly, the Windows 2000 installer was able to detect the disk by booting and pressing **F6** to [load the SiI 3114 driver](https://download.cnet.com/silicon-image-sii-3114-sataraid-controller/3000-18492_4-150480.html "Driver from Download.com") from floppy drive A. I could then partition the disk, and install as though I was installing onto a SCSI drive. Note the device at the end of the last line:

    Windows 2000 Professional Setup
    ===============================
       
    Please wait while Setup formats the partition
       
    C: New (unformatted) 114473 MB
       
    on 114474 MB Disk 0 at Id 0 on bus 1 on si3114r

Unfortunately, the resulting system wasn't bootable from that drive. I got around this by attaching a small CompactFlash card, which the Windows 2000 installer placed the NT boot loader. But I thought there had to be a better way.


### Sourcing files for the SiI card

A [thread on VOGONS](https://www.vogons.org/viewtopic.php?t=46721 "VOGONS: Booting from a Sil3114" ) suggested updating the system BIOS to enable booting from add-in cards. Placing **Option ROM**, **IDE-HDD**, and **ARMD-HDD** on my Coppermine Dimension 4100 didn't make any difference, but it might be worth trying on your rig too if you're going through this process.

Later on that thread, [kaputnik suggested](https://www.vogons.org/viewtopic.php?p=479007#p479007 "kaputnik: Have you tried with the IDE BIOS for those card") the BIOS on the SiI controller itself could be replaced with an IDE BIOS found on the Silicon Image website, and a DOS imaging tool. Their link redirected to a site with a broken SSL certificate, but the [firmware downloads](https://www.latticesemi.com/en/Support/ASSPSoftwareArchive "ASSP Software Archive") for the old cards were still available.

Unfortunately, the link to the DOS imaging tool was behind a login, but [JurgisSA on BIOS-MODS](https://www.bios-mods.com/forum/showthread.php?tid=40129 "JurgisSA: Solved! I found it in the package of utilities here") thankfully found it on the <a href="https://web.archive.org/web/20070101155701if_/http://www.siliconimage.com:80/docs/SiI3x12A-PC%20Serial%20ATA%20(SATA)%20BIOS%20Flash%20Utilities.zip">Internet Archive</a>.

Extracting both archives resulted in these files:

    .
    ├── BIOS5500
    │   ├── 5500.bin
    │   ├── SiI3114_BIOS_ReleaseNotes_Ver_5500.DOC
    │   └── r5500.bin
    └─── SiI3x12A-PC Serial ATA (SATA) BIOS Flash Utilities
        └─── 3x12A_x86_dos_updflash-319
            ├── 5500.bin
            ├── ReadMe.doc
            └── UPDFLASH.EXE

According to the DOC files, **5500.bin** was the BIOS version, and **r5500.bin** was the RAID version. I copied **5500.bin** and **UPDFLASH.EXE** to a bootable PC DOS floppy disk, like a gentleman.


### Flashing the SiI card

I booted the Dell tower with the boot disk inserted, and used the utility to detect and install the new BIOS:

    A:\>UPDFLASH.EXE 5500.BIN

It took a few minutes, during which I worried that it might have crashed or bricked the card. But it *looked* like it worked:

    UpdFlash V3.19 (06292004)
       
    This utility is for flash chip used on PC add-on card only!
    This utility is not for flash chip in MacOS add-on card or
        onboard chipset!
    Continue? (Y/N): y
    Use PCI BIOS to scan
    Found 1 SI controller(s)
    Found 1 3114 controller(s)
       
    SiI3114 version 2
       
    Loading BIOS...
    Verifying...
    BIOS is loaded
    Return Code: 0


### Booting and testing

Sure enough, I rebooted, and got a different BIOS prompt that didn't mention a RAID. It also has a more recent copyright date and version number, which were a nice bump:

    SiI 3114 SATALink  BIOS Version 5.5.00
    Copyright (C) 1997-2008 Silicon Image, Inc.
       
    1   SanDisk SDSSDA120G                    111 GB

I booted the Windows 2000 installer, hit **F6** to load the SiI controller driver, and it installed as it would from any SCSI drive. Yay!

<figure><p><img src="https://rubenerd.com/files/2023/sii-win2k.png" alt="Screenshot showing Windows 2000 booted with the SiI controller and SSD." /></p></figure>


### Conclusion

I've been using this system for a few hours now, and it's been stable and fast; certainly more responsive than the SanDisk CF card I was using on the motherboard's IDE controller. It also feels faster than the 32 GiB Transcend IDE SSD I moved to my Compaq.

With this card now set as a bootable SATA controller, I now have a viable avenue for adding modern(ish) SSDs to older machines with PCI. Next step will be some proper benchmarks.
