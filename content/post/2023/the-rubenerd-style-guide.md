---
title: "The Rubenerd Style Guide 1.0"
date: "2023-10-23T17:41:46+11:00"
abstract: "Codifying things I do here, for better or worse!"
year: "2023"
category: Internet
tag:
- language
- weblog
location: Sydney
---
I haven't ever given much thought to whether I should have a style guide over these past seventeen years. I now know why, having compiled this list.


### Punctuation and syntax

* The Oxford comma is encouraged. It's not strictly necessary, but then again, neither is this blog.

* Commands should be rendered with monospace characters, and reference their manpage section, such as `zfs-list(8)`. I almost never remember to do this.

* Hyphens are important to disambiguate triplicates. Is it a top-heavy statue, or a top heavy-statue?

* M-dashes (&mdash;) can be an alternative to brackets in certain contexts, though it probably means the sentence was written poorly. They mustn't have spaces on either side.

* *Emphasis* should be represented with italics, not inverted commas.

* Put commas where you'd pause if you were reading a line, even if other guides say doing so is redundant or bad form. I'm more concerned with readability.

* Full stops, exclamation marks, and other terminating punctuation belong outside brackets, irrespective of context. It looks awkward otherwise.

* Curly “quotes” look nice, but aren't mandatory.

* Organisations don't get to dictate punctuation. They're *Yahoo*, especially in the middle of a sentence. The *SMASH* anime convention organisers in Sydney might have words.

* It's … for an ellipsis, not three full stops. And they should [never be used to end a post](https://rubenerd.com/people-who-end-comments-with-an-ellipsis/ "People who end comments with an ellipsis…").

* Trademark symbols are only to be employed for Comedic Effect®.

* Overuse of semicolons is encouraged, to compensate for their lack of use elsewhere.

* Use *italics* for localised phrases your audience might not understand, as an alternative for [sic]. *Alamak*.

* Slavishly use bullet points to break up lists within text, to make them easier to read. Ordered lists must always be rendered with numbers.


### Systems

* SI units are mandatory, though supplying additional legacy ones are acceptable within brackets. Exceptions are made for litres and hours.

* Dates must follow Singaporean conventions, because they have it figured out. Short-form dates must conform with ISO 8601, long-form dates and times must have leading zeroes, and 24-hour time is encouraged.

* Mebibytes/Megabytes is a worthwhile and important distinction to make when expressing file sizes and storage capacity.


### Spelling and vocabulary

* British spelling and vocabulary are both preferred. It's their language, sort of. That said, I also reserve the right to use American words like *truck*, *soccer*, and *program* interchangeably where they suit.

* Spelling misatkes are inexcusable, and inevitable.

* I agree that it's illumos, otherwise it looks like LLLumos.

* Accents should be preserved on Romanised names, such as Kyōto.

* Adverbs are perfectly fine, and those who prescribe their complete eradication should be judged with rank suspicion.

* The only people I dub *[content creators](https://rubenerd.com/stop-calling-people-content-creators/ "Stop calling people “content creators”")* are those who use the term.

* It's an *article* or a *post*, not a *piece*. We don't need our pinkies up while typing.

* It is never appropriate to say "pun intended".

* The plural of LEGO is LEGO. Don't be a sheeps!

* Only utilise nuanced, impactful synergistic-paradigms in your lede for disruptive irony. I call this the [Merlin Mann Principle](https://rubenerd.com/merlin-mann-on-jargon/ "Merlin Mann on jargon").


### Style

* Jokes are funnier the more times you tell them. Tell them what? Exactly.

* Every post should reference a count, is a phrase with six words. If not, at least a comment that I eschewed (gesundheit) something, or I did something like a gentleman, or an admission of a digression. I'd hate people to think I don't take myself seriously. *But that's for another post.*

* Quotes longer than a sentence belong in an HTML `blockquote` element, and **[&hellip;]** should be used where words are omitted. Added emphasis must be called out.

* Blog Post Titles Shouldn't be Capitalised, unless it's a formal name.

* Dangling participles, and ending sentences with prepositions, aren't the end of the world. But ignoring them completely is something up with which I will not put.

* Posts must reference places I want to go in geographic examples. I'm looking at you, Riga, Yerevan, Kyiv, and Sapporo.

* Active voice is preferable, unless it makes the sentence needlessly stilted arranging it as such.

* Occasional bouts of waffle are acceptable, especially on a jovial site like this. I'd go look at a picture if I didn't want to read words.


### Parting thoughts

* Always never not be excellent.

* In the words of [The Economist](http://cdn.static-economist.com/sites/default/files/store/Style_Guide_2015.pdf), break any of these rules sooner than say anything outright barbarous.

* Pilfer whatever you want from this guide for your own if you'd like. Who knows, yours might actually be coherent!

