---
title: "Distracted drivers at a meta level"
date: "2023-08-11T08:14:58+10:00"
abstract: "We need to give people a mental break from their phones, and more mobility options"
year: "2023"
category: Thoughts
tag:
- cars
- public-transport
- safety
- transport
- urbanism
location: Sydney
---
Futurity published an article yesterday on a recent University of Missouri study, which I found quite interesting:

[Focus on choice may get drivers to quit texting](https://www.futurity.org/texting-driving-2957652/)

> The findings also indicate that reactance&mdash;or the resistance to being persuaded or controlled&mdash;played a paradoxical role in *texting-and-driving* prevention. When individuals were primed to experience reactance and exhibited initial resistance to prevention messages, researchers found death awareness effectively reduced those individuals’ resistance and positively influenced their behavior.

In other words, people behave more responsibly when they feel they have agency over their decisions. I'd be interested to see the accident rates in my Australian state of New South Wales after they introduced mandatory phone lockouts for all young drivers.

But this idea of *choice* hits on something bigger. Studies like this ask what we can do to reduce distractions, but they don't ask the question **why** people choose to use their phones while driving. Understanding this is just as important if we're going to meaningfully impact safety.

<figure><p><img src="https://rubenerd.com/files/2023/BU005304@1x.png" alt="Stock photo of a time card clock." srcset="https://rubenerd.com/files/2023/BU005304@2x.png 2x" style="display:inline; float:right; margin:0 0 1em 2em" /></p></figure>

I've discussed the pressures we feel to be permanently online, both from social circles and work. Missed calls and messages are construed as a snub, or dereliction of responsibility. This is why younger people bristle at being told they're "addicted to their phones", while being chided for not answering a call. I have a few friends in IT overseas who've been summoned for not getting headsets to wear while driving, which is terrifying.

[From 2021: Mobile phones, and always being available](https://rubenerd.com/mobile-phones-and-always-being-available/)

Another, more insidious illusion of choice comes from car infrastructure itself. If you're stranded living in a suburban wasteland, or have a job only accessible by car, sitting in one for hours at a time is unavoidable (I call it a mobility tax imposed by bad urban design). It's no wonder bored drivers feel the need to reach for their phones, or their in-car entertainment system. I can read a book or message friends on the train; a long daily commute offers no such respite, at least legally speaking.

This isn't to say these are *valid* excuses to engage in irresponsible behavior, but they go part of the way to explaining why motorists find themselves doing them. If the above study is about an evidence-based approach to legislation, we should apply the same logic here.

We can help here by pushing back on this societal need for constant connectivity. It's entirely appropriate to tell someone you're unavailable, or that you'll get back to them, or not even offer a response at all. Our lives are complicated with ever-escalating demands on our attention. Respecting someone includes their boundaries, time, and mental health.

We also *badly* need more mobility options in urban areas that don't involve safety being dependent on the undivided attention of a bored driver. Public transport within walking distance of medium-density housing would be ideal, but even a mature approach to street design that incorporates pedestrian and bicycle-friendly infrastructure (aka, not painted gutters) would be a start. New developments on the peripheries of our cities that are connected only with bitumen and pipes will only further entrench these dangerous patterns.

This will take far more effort than an awareness campaign targeted at motorists. But if we invoke what I call my *Posterior Principle*, it's worth not half-arsing this.
