---
title: "The Law of Mobility"
date: "2023-07-09T10:22:02+10:00"
abstract: "If you find yourself in a situation where you are neither learning nor contributing, go someplace else"
year: "2023"
category: Internet
tag:
- quotes
- social-media
- sociology
location: Sydney
---
[Mark Cornick, December 2021](https://mcornick.com/posts/2021/12/13/law-of-mobility/)

> "If you find yourself in a situation where you are neither learning nor contributing, go someplace else."
> 
> The intent here is to convey the agency that each participant has over their own participation. If you are enjoying the conversation, contributing to it, and/or learning from it, that's a good place for you to be, and you should stay. If you are not, then it's time to go and find something else that is more fulfilling.
>
> ... you need not feel remorse, anger, sadness, or obligation towards a community that is not right for you. Consider it an experiment that didn't yield the desired results. Remember, there is no such thing as a failed experiment, as long as it yields data; even if that data is "Don't do that again."

I can't stop thinking about this.
