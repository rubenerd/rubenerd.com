---
title: "Energy retailer of last resort?"
date: "2023-06-23T15:24:21+10:00"
abstract: "When you privatised grid loses a retailer."
year: "2023"
category: Hardware
tag:
- politics
- power
- privatisation
- utilities
location: Sydney
---
What happens when you privatise power distribution? You raise prices for one, because you've introduced for-profit companies between you and a natural monopoly. But what happens when those middle men go out of business, or decide they no longer want to supply you?

Enter this email we got from our power company, pardon, *retail energy provider*:

> Retailer of Last Resort: Your Mojo Power Account Notification

What a terse, meaningless subject line. I haven't the foggiest idea what that means, but it sure sounds ominous.

> Mojo Power is no longer retailing electricity. On Friday 16 June 2023, the Australian Energy Regulator (AER) issued a Notice of a Retailer of Last Resort (RoLR) Event to Mojo Power. This protects customers where their current retailer is no longer operating and ensures customers do not lose or have their power supply interrupted.

Great. That's... that's great!

Appreciate for a second how needlessly brittle this all is. Before we would have bought from a national energy grid, but we now buy from companies engaged in price speculation that go bust, which requires an entire extra system to ensure people are pushed across to someone else. A *Thatcher Shuffle*, you could say.

[Clarke and Dawe really did nail it five years ago](https://www.youtube.com/watch?v=ELaBzj7cn14)
