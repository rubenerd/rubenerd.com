---
title: "Rikka has her phone… on my phone"
date: "2023-04-07T21:32:06+10:00"
abstract: "A cool new wallpaper gleaned from a fig announcement."
thumb: "https://rubenerd.com/files/2023/rikka-phone-phone@1x.jpg"
year: "2023"
category: Anime
tag:
- backgrounds
- phones
- ssss-gridman
location: Sydney
---
Thanks to STUDIO TRIGGER and @annulus_toy, who showcased this official art to announce their new plastic model kit that I missed buying. Which given the state of our apartment, was probably for the best.

[Link to the Annulus TOY kit announcement](https://twitter.com/annulus_toy/status/1628332213229203456)

I never did review *SSSS.Gridman* here, and all the associated shows in the franchise. I'll add it to the ever-increasing pile of follow-up, topics, and hyphenated-phrases. ...

<figure><p><img src="https://rubenerd.com/files/2023/rikka-phone-phone@1x.jpg" alt="Photo of Rikka from Gridman holding a phone on my phone" style="width:375px; height:667px;" srcset="https://rubenerd.com/files/2023/rikka-phone-phone@2x.jpg 2x" /></p></figure>
