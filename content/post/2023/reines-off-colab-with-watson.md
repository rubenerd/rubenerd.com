---
title: "Reine’s off colab with Watson!"
date: "2023-06-30T22:21:23+10:00"
abstract: "My HoloID oshi streaming with Clara’s and my first vtuber? Yes!"
thumb: "https://rubenerd.com/files/2023/reine-watson-off-colab@1x.jpg"
year: "2023"
category: Anime
tag:
- hololive
- pavolia-reine
- watson-amelia
location: Sydney
---
This was such a fun treat for the end of the week! Pavolia Reine is my Hololive ID oshi, and Amelia Watson was Clara's and my first vTuber who remains one of our all-time favourites (and who's mannerisms have made it into our family folklore). The last time they did a collab was building Reine on the HoloEN Minecraft server... it seems like an age ago.

[#SaltedTurkey Back on the menu with THE Amelia Watson!](https://youtube.com/watch?v=62MxM2d_LT0)   
[Hololive EN tour from 2021](https://www.youtube.com/watch?v=t2ynZue1ELk)   
[Pavolia Reine's channel](https://www.youtube.com/@PavoliaReine)   
[Watson Amelia's channel](https://www.youtube.com/@WatsonAmelia)

They talked about travel, Japan, Indonesian snacks, games, and took questions from fans on Twitter. They have such great chemistry, I hope they do another one again soon.

<figure><p><img src="https://rubenerd.com/files/2023/reine-watson-off-colab@1x.jpg" alt="Screenshot from the stream on Reine's channel" style="width:500px;height:281px;" srcset="https://rubenerd.com/files/2023/reine-watson-off-colab@2x.jpg 2x" /></p></figure>

When people ask how they get into Hololive, I usually recommend these sorts of chill streams. "Off colabs" refer to them being in the same place too, which adds a whole other fun dynamic.
