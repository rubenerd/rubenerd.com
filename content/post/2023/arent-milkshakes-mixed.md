---
title: "Aren’t milkshakes mixed?"
date: "2023-10-22T08:55:53+11:00"
abstract: "I suppose milkmixed doesn’t sound as appetising."
year: "2023"
category: Thoughts
tag:
- language
- pointless
location: Sydney
---
I suppose *milkmixed* doesn't sound as appetising. But then, that etymology leads to the *thickshake*, which makes even less sense.

It reminds me of how a food from *Hamburg* was updated to be called a *cheeseburger*, despite the original one not being made of ham. James Bond also asked for his *Martinis* to be shaken nor stirred, though that would make it a *Bradford*.

I had insomnia again last night, and both these things were stuck in my head for more hours than I care to admit. Maybe I should have had gin in a thickshake. Wait... ew.
