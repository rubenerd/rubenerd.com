---
title: "Mobile SEGA “Forever” games delisted"
date: "2023-09-28T07:38:39+10:00"
abstract: "Yet a retrocomputer console can't be revoked."
year: "2023"
category: Software
tag:
- games
- retrocomputing
location: Sydney
---
[Mikhail Madnani wrote for Toucharcade](https://toucharcade.com/2023/09/26/select-sega-forever-service-games-delisted-iphone-android-mobile/)

> Back in June 2017, SEGA announced its SEGA Forever initiative to bring its classic games to iOS and Android as free to start games with a no ads IAP. Each game was to be adapted for mobile while remaining faithful to the originals. We saw the service launch with five games with more coming every few weeks. As recently as this week, select SEGA Forever games have been delisted on iOS and Android. 

I wonder if this is yet another reason other people are getting into retrocomputing? You might struggle with damaged disks or cartridges, but at least the company can't revoke support or their activation servers. It's a bit silly that a machine 40 years older can still run something.

There's really a sense of permanence we're losing with modern computing. Everything is subscription based, temporary, and ephemeral. I'm lucky that I have nostalgia for a time in computing when this wasn't the case; can you imagine ten years from now having nostalgia for games and software you can't even download and run anymore?
