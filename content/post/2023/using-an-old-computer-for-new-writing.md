---
title: "Using an old computer for new writing"
date: "2023-12-14T11:10:42+11:00"
abstract: "There might be some benefits when it comes to distractions."
year: "2023"
category: Hardware
tag:
- retrocomputing
- writing
location: Sydney
---
This is my first post written in a while with the [Toast Vim theme](https://github.com/jsit/toast.vim "Toast! by jsit") by Jsit. It's rather delightful, even if my lower-carb diet right now precludes me from eating it. The're toasting some right now in the kitchen, and it smells incredible.

This pointless distraction is an example of something I want to avoid with a dedicated writing device. How's *that* for a segue!? While such a theoretical device couldn't be used to block the smell of freshly-baked sourdough (or... could it?), the lack of on-screen or other-hyphenated distractions might help me focus and write better.

Focus... that reminds me I wanted to look up that Canon lens that... congratulations Ruben, you didn't even make it to the end of a sentence.

*End of the Sentence. <strong>Harrison Ford</strong>. GET ME OFF THIS SENTENCE.*

...

I've seen more people on Mastodon take up old computers for this purpose, which as a self-respecting retrocomputing nerd has inspired me tremendously. These are some of the reasons I've seen:

* Single-tasking operating systems force the text editor to be front and centre, and nullify any temptation to ALT+TAB to a browser, or chat window, or a quick game of *Chips Challenge* running in Wine.

* Spartan GUIs or text interfaces have fewer distracting *Das Blinkin Lights*, relatively speaking.

* Old machines and word processors have their charms. Why *wouldn't* you want to write on something old and cool, instead of something new and meh?

* Some old keyboards were garbage. Some were wonderful. The latter, turns out, are still great to type on.

* Writers have muscle memory going back to the days of WordPerfect or Paperback Writer. I think The Beatles sang a song about that.

* Fast character recognition. Modern computers have higher latency between key presses and typing, which I *absolutely* notice now.

There are some obvious downsides to this approach, including the chance for data loss or corruption with flaky hardware. But with the appropriate backup systems and modern interfaces for storage and/or networks, I could see this being an SI *mégagramme* of fun. This would be 2,205 pounds of fun [according to Wolfram Alpha](https://www.wolframalpha.com/input?i=megagram+in+pounds "Wolfram Alpha: Megagram in pounds"), which currently buys SG $3,711 [according to XE](https://www.xe.com/currencyconverter/ "XE Currency Converter") at the time of writing. I really need a distraction-free setup.

If you use a retrocomputer for writing, what's your preferred platform and software? I grew up on DOS so am probably leaning in that direction, but my Apple IIe or Commodore 128 might be fun as well.

***Harrison Ford.***
