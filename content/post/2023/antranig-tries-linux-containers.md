---
title: "Music Monday, and Antranig tries LXC"
date: "2023-07-10T10:15:01+10:00"
abstract: "It went as well as I think any BSD operator would expect."
thumb: "https://rubenerd.com/files/2023/yt-cSGpLto1yxU@1x.jpg"
year: "2023"
category: Software
tag:
- antranig-vartanian
- bsd
- containers
- freebsd
- linux
- music
- music-monday
- virtualisation
location: Sydney
---
In a situation in which I'm well familiar... am familiar with... am with a familiar because I... man, English, isn't it great? Fellow FreeBSD aficionado and friend of the Internet Antranig Vartanian required the provisioning of a Linux host for a client.

Given his deep understanding of many topics (life, the universe, process isolation), he thought it would be worth configuring it using Linux Containers, or LXC for short. The X in this case stands for extreme.

[Antranig's blog post](https://weblog.antranigv.am/posts/2023/07/710/)  

If you're spoiled by the maturity and documentation of BSD and illumos tooling, this experiment went as well as you'd expect. But I think this was the key takeaway:

> The more I use Linux Containers, the more I understand why FreeBSD Jails / illumos Zones didn’t win. People don’t want automation or control, they want “please do this for me as I don’t wanna do it myself” tools.

The "it runs on my machine!" attitude towards containerisation did always strike me as an odd admission of failure, though I stopped trying to understand a lot of the direction certain parties within the Linux ecosystem have taken of late. Antranig's observation here is interesting in this context.

As my old boss used to say, people don't want to be sysadmins, until their leaky abstractions fail and they don't have a choice. Split Enz did a song about this... hey, it turned into a Music Monday after all!

<figure><p><a target=_BLANK href="https://www.youtube.com/watch?v=cSGpLto1yxU" title="Play Split Enz - Six Months In A Leaky Boat (Official Video)"><img src="https://rubenerd.com/files/2023/yt-cSGpLto1yxU@1x.jpg" srcset="https://rubenerd.com/files/2023/yt-cSGpLto1yxU@1x.jpg 1x, https://rubenerd.com/files/2023/yt-cSGpLto1yxU@2x.jpg 2x" alt="Play Split Enz - Six Months In A Leaky Boat (Official Video)" style="width:500px;height:281px;" /></a></p></figure>

Who knew one of New Zealand's greatest exports had an opinion on orchestration? According to Wolfram Alpha, the archipelago is only 15,760 kilometres from Armenia, which would take 74 milliseconds for light to traverse in a fibre. That's only marginally further away than Australia.

