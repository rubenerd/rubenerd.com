---
title: "Daisy-chaining Apple IIe disk drives"
date: "2023-10-07T08:53:34+11:00"
abstract: "Reading an Apple bulletin from the Internet Archive discussing the limits of the Apple IIe compared to the IIGS or IIc."
year: "2023"
category: Hardware
tag:
- apple
- apple-ii
- apple-iie
- disk-drives
- floppy-disks
- retrocomputing
location: Sydney
---
I've seen a few videos and images of Apple II fans daisy-chaining drives, like those of us with Commodore kit would have with our 1541s. I was especially interested in people who'd chained 5.25 and 3.5 drives.

Thanks to the Internet Archive, we can read [this Apple Tech Info Library](https://archive.org/details/Apple_IIe-No_Daisy-chain_3.5-5.25 "Apple Tech Info Library - Apple II Hardware: Apple IIe-No Daisy-chain 3.5-5.25") bulletin:

> With an Apple IIGS or IIc, you can daisy-chain disk drives. This is not true with the Apple IIe. Two 5.25” drives can be run from one 5.25” controller card, but you need a separate 3.5” drive controller card if you add a 3.5” drive.
>
> Tech Info Library Article Number:395

I guess that means you can only daisy-chain like-for-like devices on the Apple IIe. That means a [Floppy Emu](https://shop.bigmessowires.com/products/daisy-chainer "BMOW: Daisy Chainer Disk Coupler") could run as a second 5.25-inch drive on [my Apple IIe](https://rubenerd.com/remembering-the-apple-iie-platinum-and-finally-finding-one/ "Remembering the Apple //e Platinum, and finally finding one!") with its Apple I/O controller, but I'd also need a 3.5 controller.

