---
title: "The art of Blue Archive"
date: "2023-09-18T19:08:24+10:00"
abstract: "I might need to get an official artbook."
thumb: "https://rubenerd.com/files/2023/ba-screenshot-01@2x.jpg"
year: "2023"
category: Anime
tag:
- art
- games
- mobile-games
location: Sydney
---
I don't need another mobile game in my life, thanks to *Fate/Grand Order*. This isn't from a lack of desire however; friends have tried repeatedly to get me into *Arknights*, *Honkai Star Rail*, *Genshin*, and *Time-Strapped Ruben*.

But I'll admit, *Blue Archive's* art won me over two years later, no thanks to  reader Simon who told me I had to try it. It's his fault, in other words.

<figure><p><img src="https://rubenerd.com/files/2023/ba-screenshot-01@2x.jpg" alt="Screenshot from the opening screen with one of the characters who might have been my favourite cough." style="" srcset="https://rubenerd.com/files/2023/ba-screenshot-01@2x.jpg 2x" /><br /><img src="https://rubenerd.com/files/2023/ba-screenshot-02@2x.jpg" alt="" style="Screenshot from the opening screen showing several of the characters." srcset="https://rubenerd.com/files/2023/ba-screenshot-02@2x.jpg 2x" /><br /><img src="https://rubenerd.com/files/2023/ba-screenshot-03@2x.jpg" alt="Screenshot showing the text gameplay." style="" srcset="https://rubenerd.com/files/2023/ba-screenshot-03@2x.jpg 2x" /><img src="https://rubenerd.com/files/2023/ba-screenshot-04@2x.jpg" alt="Scenery image from the game opening." style="" srcset="https://rubenerd.com/files/2023/ba-screenshot-04@2x.jpg 2x" /></p></figure>

The game dynamic wasn't quite my cup of tea, but as I suspected, the art and immersive world were stunning, and the chibi versions of the characters were more than a little cute.

If they have an official artbook, maybe I need to get that. Cough.
