---
title: "Long-term effects of concussions"
date: "2023-10-05T16:36:49+11:00"
abstract: "Research shows that suffering concussions is linked to increased cognitive decline."
year: "2023"
category: Thoughts
tag:
- health
- sport
location: Sydney
---
[Futurism: If You Had a Concussion When You Were Younger, Scientists Have Bad News](https://futurism.com/neoscope/younger-concussion-bad-news)

> New research shows that suffering concussions — or even just one — is linked to increased cognitive decline later in life, including in patients who appeared to have fully recovered from the injury.

Yet another reason for contact sports like rugby, Australia's various <abbr title="three-letter abbreviations">TLAs</abbr>, and American football to be put out to pasture.

Not that I'd dare admit that scientifically-backed thought in public, because that would also likely result in a concussion. Need to *head* it off at the pass. *Balls*.
