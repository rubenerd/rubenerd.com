---
title: "Molly White on AI and effective altruism"
date: "2023-12-04T12:55:11+11:00"
abstract: "The various philosophical movements that people used to justify accumulating wealth to the detriment of others."
year: "2023"
category: Ethics
tag:
- ai
- medicine
location: Sydney
---
Molly White, the software engineer and writer famous for running the sublime *[Web 3.0 Is Going Just Great](https://web3isgoinggreat.com/)* site, [has a new post](https://newsletter.mollywhite.net/p/effective-obfuscation "Effective obfuscation") about AI, shortsighted views of technology, and the various philosophical movements that people use to justify accumulating wealth to the detriment of others.

The whole post is fire, but this was my favourite passaige:

> Both ideologies embrace as a given the idea of a super-powerful artificial general intelligence being just around the corner, an assumption that leaves little room for discussion of the many ways that AI is harming real people today. This is no coincidence: when you can convince everyone that AI might turn everyone into paperclips tomorrow, or on the flip side might cure every disease on earth, it’s easy to distract people from today’s issues of ghost labor, algorithmic bias, and erosion of the rights of artists and others.

I do agree with tech utopians on one point: I miss being broadly optimistic about the future promise of tech. The idea that all the world's problems can be solved by applying science and technology to it. That every new tech is basically good, even if it can be abused.

There's still so much to be excited for. The Covid pandemic showed how quickly we can develop once-in-a-generation treatments and vaccines like mRNA if we put our collective attention towards it. Our silicon continues to push the envelope of Moore's Law, which has the potential to reduce energy use per unit of work, and put it into the hands of more people. The IndieWeb is showing that there's a viable community of people outside the perverse incentives of tracking and malware.

But that's I think the thing I most resent: we keep pumping money into guff as well, and guff that's [pushing us backwards](https://www.ri.se/en/news/blog/generative-ai-does-not-run-on-thin-air "Generative AI does not run on thin air"). Human ingenuity and creativity aren't zero sum, but our resources are certainly finite.
