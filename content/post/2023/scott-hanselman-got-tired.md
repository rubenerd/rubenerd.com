---
title: "Scott Hanselman and I got tired"
date: "2023-12-01T08:59:00+11:00"
abstract: "You don’t owe anyone an explanation."
year: "2023"
category: Internet
tag:
- blogging
- weblog
location: Sydney
---
[From his blog last December](https://www.hanselman.com/blog/i-got-tired "Scott Hanselman: I got tired")\:

> I have been blogging here for the last 20 years. Every Tuesday and Thursday, quite consistently, for two decades. But last year, without planning it, I got tired and stopped. Not sure why. It didn't correspond with any life events. Nothing interesting or notable happened. I just stopped.

He's quick to clarify that his podcast is going strong, and that you can now reach him on Mastodon. 👍

I also get it. [My own silly show](https://rubenerd.com/show/ "The Rubenerd Show!") has been more off than on lately, and I'll take time off social media when I'm not feeling it. I maintained a dozen or so blogs under various pseudonyms over time (some of which had far more readers!), but it's this one I always ended up retreating to.

There was a time where I tried to blog every single day, because there's value in cultivating a habit. I even had draft posts in the wings for those days where I didn't or couldn't write for whatever reason. But while I love writing, it's also something you have to feel.

It's why I love writers, podcasters, artists, and videographers who are more spontaneous. The best thing about independent media is that we get to dictate our schedules. If posting daily, weekly, monthly, or annually feels natural, that's great! Conversely, you can tell the ones that have attracted advertising contracts which place demands on regularity, and the writers or hosts subsequently phone it in sometimes (I'd definitely place most modern podcasts in this camp). It's human nature.

Fear of a schedule is one of the top reasons smart people I know say they "can't write". But you own your space. You can post whatever, whenever. You don't owe anyone an explanation, unless you want to provide one.
