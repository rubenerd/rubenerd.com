---
title: "The trend of no friends"
date: "2023-11-05T08:09:03+11:00"
abstract: "I saw this on YouTube, and I’m not sure what to make of it."
year: "2023"
category: Thoughts
tag:
- friends
- psychology
location: Sydney
---
I tend not to see recommended videos on YouTube. I still follow channels [with RSS feeds](https://rubenerd.com/blogroll.opml), and in the evening we watch our favourites via subscriptions on the Apple TV. But occasionally I do see a topic on the web UI that piques my interest.

Last weekend's rabbithole journey was *not* what I expected. The theme was hosts not having friends, and exploring how it affeced their lives. They could be broken down into these three groups:

* Lonely people who don't know how to make friends, and are stuck in a spiral of self-hatred and doubt. My heart goes out to them, because I know *exactly* what that's like. Chances are if you're the kind of person who reads a blog like this, you do too.

* Hustle-culture scam artists who claim friends should be discarded unless they deliver value, or some other superficial benefit. These people are in their thirties or older, but their zero-sum heads are stuck in high school.

* Those professing introversion, or a general apathy to having interactions with other people. They claim friends are a burden, overrated, or even unnecessary.

It's that last group I'd never heard from before. These people claim they *could* make friends, but have made a concious decision not to. They want to live a life of solitude, and they want to tell *you* why you're not weird for wanting the same thing.

This message struck a chord, based on the volume of replies under each video. While the presenters and people replying skewed towards Zoomers, there were Millennials and older among them as well.

<figure><p><a target=_BLANK href="https://www.youtube.com/watch?v=HWnWQgcPLo4" title="Play Lonely"><img src="https://rubenerd.com/files/2023/yt-HWnWQgcPLo4@1x.jpg" srcset="https://rubenerd.com/files/2023/yt-HWnWQgcPLo4@1x.jpg 1x, https://rubenerd.com/files/2023/yt-HWnWQgcPLo4@2x.jpg 2x" alt="Play Lonely" style="width:500px;height:281px;" /></a></p></figure>

> They're tellin' me the dos and don'ts;   
> But nobody's askin' me what I desire.
> (I wanna be lonely).

These videos impacted me more than I would have expected, in part because I can see where they're coming from. I had precious few friends in school and uni, though that's changed slightly in recent years. While I personally feel anxiety if I'm away from anyone for too long, I need introversion time alone or I feel irritable and fatigued. It's not a stretch for me to imagine some people want to spend their lives in that latter state.

It's also a peek into a broader sociological shift that's taking place. I've noticed Zoomers in particular eschew (gesundheit) social norms at a scale even Millennials like me didn't, *especially* since they went through Covid in school or uni. If people aren't bringing them love, happiness, or acceptance, they're cutting them out. I've seen parents, mentors, politicians, and employers wring their hands over this perceived lack of *commitment*, but they ignore this at their peril.

We also have to acknowledge the current global environmental, economic, and security situation is putting pressure on young people. If you're expected to work your arse off for peanuts, of course you'll latch onto things that give you comfort. For some, that's solitude, and not suffering fools under a pretence of having a healthy social life.

I'm not about to psychoanalyse anyone based on a snippet of themselves they edited into a video. Heck, I consider myself a high-functioning socially-anxious introvert myself. What I will say is that I'm a *bit* concerned that some people are giving up on a mental support network entirely. Everyone should live their best life, but I hope they can keep the door open for human connection as well. It's the sort of thing you don't know you need until you do.
