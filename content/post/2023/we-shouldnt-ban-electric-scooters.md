---
title: "Banning electric scooters"
date: "2023-09-05T17:55:05+10:00"
abstract: "“It comes down to road design at that point, and what is a priority”."
year: "2023"
category: Thoughts
tag:
- environment
- sustainable
- transport
- urbanism
location: Sydney
---
Jason from NotJustBikes, and Alan Fisher from the Armchair Urbanist did a great podcast episode last March about micro-mobility devices like e-bikes and scooters.

[The Big Problem with Small Vechiles](https://podcasts.apple.com/us/podcast/the-big-problem-with-small-vehicles-with-alan-fisher/id1678391788?i=1000605767478)

> **Jason:** I get when people are like, "these things shouldn't be riding at high speed on the sidewalk" [... but] cars are so much worse than this. Provided there's enough space and safe-enough infrastructure, I really think these are a good idea.
>
> **Alan:** Usually the only time people ride on the sidewalk is when they feel like the street isn't safe, or there isn't an option for them to go a certain direction because it's made for cars.
>
> **Jason:** [Cars] really have the majority of space between buildings, whether for moving or just sitting there all day. I generally don't have patience for that. There's enough space for everyone if the sidewalks and cycle lanes are wide enough [...] we can get along.
>
> **Alan:** It comes down to road design at that point, and what is a priority.
>
> **Jason:** Like that study you found, when they banned [micro mobility devices] there was an increase in car traffic.

