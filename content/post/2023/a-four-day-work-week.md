---
title: "A four-day work week"
date: "2023-03-08T09:43:08+11:00"
abstract: "There’s a preponderence of evidence to suggest it improves wellbeing and productivity."
year: "2023"
category: Thoughts
tag:
- psychology
- work
location: Sydney
---
This was some interesting, if not surprising, research summarised by Sophie Bushwick in a recent [Scientific American article](https://www.scientificamerican.com/article/a-four-day-workweek-reduces-stress-without-hurting-productivity/)\:

> The results of a test involving dozens of employers and thousands of employees suggests that working only four days instead of five is good for workers’ well-being—without hurting companies
>
> ... independent research organization Autonomy, in conjunction with the advocacy groups 4 Day Week Global and 4 Day Week Campaign and researchers at the University of Cambridge, Boston College and other institutions, [published] a report on what happens when companies reduce the number of days in a workweek. According to surveys of participants, 71 percent of respondents reported lower levels of burnout, and 39 percent reported being less stressed than when they began the test. Companies experienced 65 percent fewer sick and personal days. And the number of resignations dropped by more than half, compared with an earlier six-month period. Despite employees logging fewer work hours, companies’ revenues barely changed during the test period. In fact, they actually increased slightly, by 1.4 percent on average.

The first two contributers aren't exactly impartial, but all the evidence I've seen thus far supports their cause. Four-day work weeks have been shown to be better for health, creativity, personal growth, employee retention, and productivity than five, or heaven forbid the 996 system that people in parts of East Asia have to endure.

But I think even this is still too rigid under some circumstances. Customer-facing roles like mine will necessarily be constrained to regular business hours, and people working in teams will need dedicated times to collaborate and pool ideas. But otherwise it should be sufficient to deliver projects not track time. Creative people especially don't get bursts of inspiration on a predictable schedule.
