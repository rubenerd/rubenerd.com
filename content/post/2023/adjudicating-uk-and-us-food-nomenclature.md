---
title: "Adjudicating UK and US food nomenclature"
date: "2023-10-26T13:51:27+11:00"
abstract: "It was more mixed than I expected."
thumb: "https://rubenerd.com/files/2023/ukus-jelly@1x.jpg"
year: "2023"
category: Thoughts
tag:
- food
- mythical-kitchen
- sorted
- tasting-history
location: Sydney
---
Ben from *Sorted Food* channel [recently did a collaboration](https://www.youtube.com/watch?v=cMnMzbzrIP0 "UK vs USA Food Fight: 5 Dishes, SAME name VERY Different!") with Josh from the *Mythical Kitchen*, and Max from *Tasting History*. It was an absolute blast seeing my favourite people in this space come together to settle once and for all the differences between British and American foods.

While Australia does tend towards the British side culturally speaking, we do certain things differently, and others we do like the Yanks. As an Australian who grew up overseas with a German father, I also feel uniquely qualified to pass judgement on the outcome of this.


### Round one: Biscuits

<figure><p><img src="https://rubenerd.com/files/2023/ukus-biscuit@1x.jpg" alt="Photo of Ben with british biscuits on a plate, and Josh with American biscuits that look like scones." style="width:500px; height:280px" srcset="https://rubenerd.com/files/2023/ukus-biscuit@2x.jpg 2x" /></p></figure>

Ben brought out sweet baked goods with various fillings you'd have with tea, and Josh brought out those American scone-looking things.

This is a clear winner: **the British biscuit**. You put the kettle on, open the wrapper on a pack of Arnotts, and have a *biscuit* with a hearty brew. You'll regularly get a free *biscotti* with your takeaway coffee in Australia too, both of which Italian immigrants brought down. Even the *ANZAC biscuit*, a cultural icon of Australia and New Zealand, comes from that tradition. I'd even class German deserts like pfeffernusse and gingerbread as *biscuits*. I suppose Americans would call those *cookies*.

I had an American *biscuit* in New York back in 2016. It was yummy, but not a *biscuit*. I don't even know how you'd describe it.


### Round two: Jelly

<figure><p><img src="https://rubenerd.com/files/2023/ukus-jelly@1x.jpg" alt="Ben and Josh with British and American takes on jelly, with Max deciding which constitutes jelly." style="width:500px; height:280px" srcset="https://rubenerd.com/files/2023/ukus-jelly@2x.jpg 2x" /></p></figure>

Ben brought out a wiggly desert, and Josh brought out preserved juice solidified with pectin and gelatin.

This was another easy one: **British jelly**. *Jelly* is something you set in the fridge and have as a summer desert treat. You might suspend some fruit in it, or in Singapore I used to add coconut or even include it in a weird British-Malay *cendol* hybrid.

> *I love Aeroplane Jelly! Aeroplane Jelly for me! ♬*

That said, Josh finally cleared up something for me! I had long assumed *jelly* was synonymous with fruit *jam*, but it's a distinct product in America. *Jelly* of that sort isn't something we eat in Australia or the UK; it's either jam or marmalade. We wouldn't mix it with *peanut butter* on a sandwich either.


### Round three: Chips

<figure><p><img src="https://rubenerd.com/files/2023/ukus-chips@1x.jpg" alt="Ben and Josh with plates of chips, and Max looking on." style="width:500px; height:280px" srcset="https://rubenerd.com/files/2023/ukus-chips@2x.jpg 2x" /></p></figure>

Ben brought out slices of fried potato one would have with beer-battered fish and vinegar at the beach, and Josh brought out salted crisps you'd have from a bag and couldn't stop eating.

This one was a wash: **they're both correct!** Australians don't use a different term; they're either *hot chips* or *chips*. Though I am seeing more young Australians refer to the former as *fries*, as they do in the US.

Either way, I feel like we're building bridges here.


### Round four: Muffins

<figure><p><img src="https://rubenerd.com/files/2023/ukus-muffin@1x.jpg" alt="Ben and Josh with plates of stacked muffins" style="width:500px; height:280px" srcset="https://rubenerd.com/files/2023/ukus-muffin@2x.jpg 2x" /></p></figure>

Ben brought out an *English muffin* (foreshadowing), and Josh brought out a baked good, almost like a cake.

This one I have to side with **American muffins**. *English muffins* are definitely a thing, but I'd think of *American muffins* first if you asked me for a *muffin* without qualifiers. *English muffins* are only *muffins* by virtue of having both terms.

I don't really like *muffins* or cake at all; I far prefer a savoury English muffin (or a New York cheesecake, which isn't really cake). But that's not what this test is about.


### Final round: Pudding

<figure><p><img src="https://rubenerd.com/files/2023/ukus-pudding@1x.jpg" alt="Close-up of British and American pudding." style="width:500px; height:280px" srcset="https://rubenerd.com/files/2023/ukus-pudding@2x.jpg 2x" /></p></figure>

This one surprised me. Ben brought out small pucks of black pudding, and Josh brought out chocolate custard in a cup.

**Neither of these are pudding!** At least, to me. Thing is, the Brits have wonderful boiled Christmas puddings Ben could have brought out, and *that* I would have said was pudding. My parents always had an alcohol-soaked Christmas pudding hanging in the kitchen for most of the year, to be cut down and eaten during the holiday season.

I had black pudding in the UK and Ireland a decade ago, and it was okay. I do love oats, and appreciated that both the ones I had were very oat-forward. I'd far prefer to eat an American-style *pudding* cup though, even though I don't see that as *pudding* either.


### Conclusion

By my count, that's 2 for the UK, 1 tie, 1 for the US, and 1 that wasn't claimed. That's more even than I was expecting.

I hope they make another video like this for me to critique; we're doing good work here.
