---
title: "Singular or plural verbs for singular nouns"
date: "2023-12-05T13:36:47+11:00"
abstract: "I use the British plural verb for singular nouns, but this is broadly unusual."
year: "2023"
category: Thoughts
tag:
- english
- language
- spelling
location: Sydney
---
I'm Australian, but grew up in Singapore where British English is mostly standard. Australian English is a bit of weird hybrid of British spelling with some American vocab and grammar, which has caught me out before.

This reads normally to me, for example:

> The anime club **are** watching Suzumiya Haruhi.

Though most Australians, Americans, and Canadians would write it as this:

> The anime club **is** watching Suzumiya Haruhi.

The **anime club** is the singular noun here. Like *Highlander*, there can be Only One. Unless there are two, in which case it's a plural group of **anime clubs**.

Most English speakers match the singular verb with a singular noun, and a plural verb for a plural noun. Makes sense! Thus, a single anime club **is** watching something. If there are anime clubs together, they **are** watching something.

British English is more subtle. A club, or team, or country, **are** said to be doing something, even if there's only one group. There doesn't seem to be any consensus as to why, though my theory is there are three related reasons:

* Convention! Toddle pip!

* A group constitutes more than one person, so they **are** doing something.

* A group is a concept, not a squishy human with eyes and ears. Therefore, the group **is** incapable of watching something. The *members* of the group **are**, though.

English, *amirite!?*

**Update:** [I wrote a follow-up](https://rubenerd.com/singular-or-plural-follow-up/ "Singular or plural follow-up"). It's even messier than I thought!

