---
title: "Ugly American trucks"
date: "2023-08-18T16:20:47+10:00"
abstract: "America, I love you, but what the fsck?"
thumb: "https://rubenerd.com/files/2023/ugly-car@1x.jpg"
year: "2023"
category: Thoughts
tag:
- cars
- environment
- transport
- united-states
- urbanism
location: Sydney
---
I just saw another one of these appear in Sydney. Or should I say *heard*, because its engine and tyre noise could be perceived from a block away before I saw it park.

<figure><p><img src="https://rubenerd.com/files/2023/ugly-car@1x.jpg" alt="A Penile Compensator 5000 taking up one and a half parking spaces on the side of a Sydney road" srcset="https://rubenerd.com/files/2023/ugly-car@2x.jpg" style="width:500px; height:333px;" /></p></figure>

America, I love you, but what the fsck?
