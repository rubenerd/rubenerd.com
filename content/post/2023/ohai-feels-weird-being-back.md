---
title: "Ohai, feels weird being back"
date: "2023-05-24T09:02:45+10:00"
abstract: "Thanking everyone for thoughts, and what’s in the pipeline."
year: "2023"
category: Internet
tag:
- jim-kloss
- weblog
location: Sydney
---
Thanks to all of you who reached out; I'll admit I took Jim's passing, and some others in the extended family, pretty hard. I've lost important people in my life many times, but never a close friend like that. I've also had the flu which hasn't helped!

My sister put it best when she said that by continuing to use his moral compass, and writing posts as though they were directed at him, he's still influencing the world. I really like that.

As I said in my farewell post, I'm still thinking of ways to remember him in a more permanent and fitting way. For now, I've added him to the dedications section of the About page.

[About rubenerd.com](https://rubenerd.com/about/)   
[Goodbye Jim Kloss ♡ 1956–2023](https://rubenerd.com/goodbye-jim/)

While I didn't feel up to writing (or more specifically, I didn't feel like I could), I did a bit more plumbing work to hopefully make the site a bit more robust, and fixed a ton of legacy posts that were missing metadata. I've gone 18 years without a CDN, but I'm starting to look at what I can do to host cached versions in different places to speed up image-heavy posts. I'm also reviewing making a Gemini version of the site; hence the switch to links on their own lines over the last few months.

There's also a ton of cool stuff in the pipeline! I made a bunch of progress on some personal projects (albeit very slowly), there's some FreeBSD and NetBSD stuff I've learned on cloud servers at work and on laptops, plenty of Commodore and DOS retrocomputing updates with hardware and software (and potentially some Atari!?), some anime and coffee reviews, and way more Japanese holiday stuff from our trips in March and November last year. I also owe many of you replies to email feedback.
