---
title: "Lawsuit against OpenAI, and generative AI theft"
date: "2023-09-21T17:31:06+10:00"
abstract: "I’m glad this is getting a public hearing, even if the case ends up being settled."
year: "2023"
category: Ethics
tag:
- art
- generative-ai
- theft
- writing
location: Sydney
---
[The Guardian](https://www.theguardian.com/books/2023/sep/20/authors-lawsuit-openai-george-rr-martin-john-grisham)

> John Grisham, Jodi Picoult and George RR Martin are among 17 authors suing OpenAI for “systematic theft on a mass scale”, the latest in a wave of legal action by writers concerned that artificial intelligence programs are using their copyrighted works without permission.
> 
> In papers filed on Tuesday in federal court in New York, the authors alleged “flagrant and harmful infringements of plaintiffs’ registered copyrights” and called the ChatGPT program a “massive commercial enterprise” that is reliant upon “systematic theft on a mass scale”.

I'm glad this is getting a public hearing, even if the case ends up being settled. Tech pundits and the wider press have utterly failed in their duty to discuss the ethics of using people's digital works without their consent, attribution, or compensation. Because *hey, shiny tech!*

I'm less interested in these big names though, and more about small writers, artists, musicians, videographers, developers, and designers who's love, energy, and inspiration are being so cynically pilfered to enrich a select few. It's deeply unsettling that it's been normalised so quickly.

To anyone out there feeling upset that their passion has been treated as gristle to be chewed through and spat out by one of these tools, I hear you. Some of us do care, and we'll continue to call this out. Please don't forget: *you're* the ones that make our lives beautiful.
