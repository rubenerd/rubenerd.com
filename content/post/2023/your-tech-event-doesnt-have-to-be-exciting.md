---
title: "Your tech event doesn’t have to be exciting"
date: "2023-07-29T09:07:28+10:00"
abstract: "A bored photo from a tech conference spoke volumes."
year: "2023"
category: Software
tag:
- events
location: Sydney
---
I can’t stop thinking about a photo I saw on social media yesterday. It was posted by an evangelist for a tech company to celebrate the opening of their industry event. They were *EXCITED! PUMPED! JAZZED!* I'm sure I'm missing some other adjectives.

You know the type of account. Their avatars sport cheesy grins, and their post history exudes nothing but giddy, insincere optimism for their employer and life in general. *I'm so EXCITED to be at this launch! We're taking on the WORLD! Check out this HAM SANDWICH!*

The audience photo they posted from their recent event showed anything but. Most were on their phones. Those actually looking at the brightly coloured stage, smoke machines, and thumping lights looked bored witless. Two in the front row were literally caught mid-yawn, and one sported such an ashen expression I wanted to reach out and give him a hug.

This is a dramatic re-enactment with Hololive stands:

<figure><p><img src="https://rubenerd.com/files/2023/stands-sleeping@1x.jpg" alt="A set of Hololive English acrylic stands... lying down on a table." style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2023/stands-sleeping@2x.jpg 2x" /></p></figure>
 
Now you might say it was early in the morning, and maybe they'd been partying hard with this vendor the night before. That would explain the tired people... but it doesn't account for those vacant stares and look of quiet desperation at being roped into a product pitch. Sure it's time off work, but was it worth it? Would you have gone to it otherwise?

This is the current tech industry writ large. Everyone wants to be Apple, at least when that company was doing cool stuff. People study those Steve Jobs announcements as though they're PR gospel, but always forget that he was *announcing something cool*. You can coach your CRM or automation platform in the same words, but you won't get the same response.

*But that's fine!* Not every tech is destined to make a public splash, but it doesn't diminish its significance. Behind every iPhone is a complicated and essential set of infrastructure that make its software and networks tick. Look to the BSD OS conferences for examples of industry events done right for technical people to discuss important work.

Maybe these PR agencies think they need to do this to appease their employer's investors or drum up interest, but what they don't understand is that they're doing the exact opposite. Even the most breathless, saccharine excitement couldn't change the fact that people at that event looked like they didn't want to be there. I've been at events like that, and their veneer of enthusiasm is exhausting.

I talk often here about how our industry is immature. I mean that literally, in the sense that it's been around for less than a century in its current form. But the way it presents itself and treats its workforce is equally juvenile. It's *hello fellow kids* territory, only we're hearing how *HEART-STOPPINGLY ORGASMIC* a package manager is.

Fun is great! But it only works with self-awareness.
