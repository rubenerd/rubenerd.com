---
title: "Pigeonholing your unrivalled tech buyers"
date: "2023-12-08T08:03:14+11:00"
abstract: "You need UNRIVALLED performance, as a HARDCORE gamer!"
year: "2023"
category: Hardware
tag:
- hard-drives
- marketing
location: Sydney
---
*Pigeonholing* is an English idiom that suggests one is classifying people based on something arbitrary or limiting. It comes from a classic postal *pigeonhole*, where letters are sorted into individual slots for people to collect.

Tech companies seem particularly vulnerable to pigeonholing their customers, because their devices or services can be made so specific with walls of technical specifications. Here's how a company branded their SSD:

> $DEVICE is specially designed for tech enthusiasts, hardcore gamers, and professionals who need unrivalled performance.

I'd consider myself a tech enthusiast and a professional, but am I a *hardcore* gamer, or a professional who needs *unrivalled* performance? Unrivalled in comparison to what? Presumably their other devices they deliberately compromise. I guess it's not for me!

Steve from GamersNexus did another of his [bombastic videos 📺](https://www.youtube.com/watch?v=xUT4d5IVY0A "GamersNexus: Intel Snake Oil") exploring one well-know silicon vendors' attempt to pigeonhole people in education by age class. Did you know only small kids need Celerons or i3s, and that when you hit 14 you'll need to upgrade to an i5? No wonder I had to spend so much time studying in high school, the i5 didn't even exist yet! Well that, and the fact I had an Athlon XP and a PowerPC G3, but leaving that aside.

Like other forms of clichés, most bluster does contain a small kernel of truth. The problem is they're blown *way* out of proportion, and into territory where they're questionably useful or actionable. Someone like me who's running OpenZFS on a drive array probably *does* need good performance. But *unrivalled?* And does my love of open-world games and puzzles count as being *hardcore?*

I was at a coffee shop earlier this week, and the local high school had just closed for the day. I could tell immediately that those sitting at the table next to me wouldn't succeed without at least a premium *Collateral Damage* class SSD paired with an i7, based entirely on the fact they're in year 12. Though they were all on MacBooks, so there goes that idea.

What's your favourite example of this sort of tech bluster? Is there one in your region of the world that left you chuckling? I'm kind of curious!
