---
title: "Daniel Jalkut on AI and authenticity"
date: "2023-03-31T11:48:39+11:00"
abstract: "With every advance in AI, the value of authentic creativity increases. Stay human."
year: "2023"
category: Media
tag:
- ai
- art
- writing
location: Sydney
---
[Via his Microblog](https://danielpunkass.micro.blog/2023/03/30/with-every-advance.html)\:

> With every advance in AI, the value of authentic creativity increases. Stay human.
