---
title: "New years resolution for 2024"
date: "2023-12-18T15:21:34+11:00"
abstract: "Stop greasing the squeaky wheel."
year: "2023"
category: Thoughts
tag:
- kindness
- philosophy
location: Sydney
---
I [posted this on Mastodon](https://bsd.network/@rubenerd/111599467379341276 "Link to the post on BSD.Network"), but I think it's warrants a mention here:

> If I have one new year's resolution for next year, it'll be to stop greasing the squeaky wheel. I want to spend more time helping people I love, respect, and who are kind to me, not the jerks who shout the loudest. That goes for business as well as personal.
