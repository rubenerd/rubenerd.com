---
title: "Citibank Australia were pleased to close our account"
date: "2023-04-02T09:23:49+11:00"
abstract: "Also, I like pie."
year: "2023"
category: Thoughts
tag:
- business
- finance
- uh-oh
location: Sydney
---
It's time for another installment of *[Uh-Oh](https://rubenerd.com/tag/uh-oh/)!*, the series where I post a press release from a company being excited about something, only to have it negatively impact their customers.

Citibank Australia emailed this last year, emphasis added:

> I like pie.

That's clearly the wrong quote. Let's try again:

> We're **pleased to advise** that on 1 June 2022, National Australia Bank Limited (NAB) acquired the consumer banking business from Citigroup Pty Ltd (Citi) and appointed Citi to provide transitional services. 

*Pleased!* That's at least a bit more synergistic for disruptive, paradigm-transformative nuance than *excited*.

Fast-forward to last month, emphasis added:

> I like pie.

Has Chet Baker taken over this post?! Geddit, because bakers and... shaddup. Let's try again:

> Following the sale of the Citigroup Pty Limited (Citi) consumer banking business in Australia to the National Australia Bank (NAB), we conducted a review of the NAB product suite. Unfortunately, we were unable to identify a NAB product that [synergistic nuanced paradigms, so] will soon need to **close your impacted account(s)**.

They were pleased to announce they're closing our accounts! *Wah lah, by golly, gee whiz, bloody oath, shiok, ain't that neat?*

I stopped using Citibank for travel as soon as the sale was announced, because we all knew this would happen. It sucks, because it had decent support for ATMs in the US and Japan, and their phone support was among the better ones I interacted with.

I did buy a slice of apple pie from a bakery in Singapore with my Australian Citibank card back in the day. Maybe the memory of it was so good, the regret of not being able to again informed the rest of this post. Pie.
