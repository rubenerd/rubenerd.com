---
title: "Main lesson from uni: know thy spec!"
date: "2023-03-11T08:16:17+11:00"
abstract: "Asking at the start can and will save you a bunch of work and frustration!"
year: "2023"
category: Software
tag:
- diving
- free
- kyoani
- technical-writing
- specifications
- sport
location: Sydney
contributer:
- "Kyoto Animation"
---
I was trading stories about university with someone recently, and it reminded me of a few occasions where reading and clarifying the spec would have solved everything! You owe it to yourself and your group to make sure everything is clear from the start, lest you go down unproductive and pointless rabbitholes that chew time and sleep.

For a security subject, we had to demonstrate we understood public and symmetric key cryptography. Public key cryptography uses a pair of mathematically-related keys to encrypt and decrypt, which permits only those with the corresponding key to access the plaintext. Symmetric cryptography use one key, and is usually significantly faster.

The spec for this assignment asked us to implement a public key cipher that encrypts a symmetric key to encrypt bulk data, a common use case. I read that to mean implement *known* ciphers. I spent *days* building and testing something that could do 2048-bit RSA and 128-bit Twofish in Perl (yes!), the language I'd elected to do the subject in. The tutor was impressed and gave me full marks, but said that a basic proof of concept would have sufficed... something I could have scripted an hour before!

The second was for a multimedia elective I took with Clara and a few friends. It required the development of a "gamification" system with a web portal and rewards for performing certain activities. Being weebs who all met in the anime club, we did something related to that. Clara and a few of the others in the group drew all the cute art, and the rest of us worked on the Django portal. Unfortunately I didn't get the portal done in time, but the lecturer didn't care because the assignment was for a prototype and plan for a system, not a completed one. Welp!

We weren't always so lucky though. One assignment I worked on with my friend [Vadim](https://jamiejakov.me) involved us "researching the IT systems of a sports venue", something the tutor probably thought was a funny thing to give a room full of nerds. We both went to the Ian Thorpe Aquatic Centre in Sydney; Vadim knew some of the people who worked there, and diving was among the few sports I knew anything about after some volunteer IT admin work for a community centre and team in Singapore. We submitted our *thicc* report to the tutor, including several references to the cute young gentleman from the *[Free!](https://myanimelist.net/anime/18507/free)* swimming anime, only to have him deduct marks for it not being a "sports venue". I think Haruka's expression (left) from this key visual by Kyoani says it all.

<p><figure><img src="https://rubenerd.com/files/2023/free-key-visual@1x.jpg" alt="Key visual by Kyoto Animation showing Haruka’s trademark blank expression." style="width:500px; height:340px;" srcset="https://rubenerd.com/files/2023/free-key-visual@2x.jpg" /></figure></p>

Clearly we made the silly mistake of assuming sport to be... sport. He didn't consider aquatics sport, something he didn't budge from even after being asked why it was in the Olympics. I'm sure Rebecca who reads this blog would have words! It was a small enough project that it wasn't worth appealing, but asking from the start what he meant by "sport" would have nipped this in the bud. Only sports with a ball are sports, *dontchaknow?*

I don't regret any work I did at uni. I did learn a great deal, especially from my assignments that I always over-engineered for what was required. But would I have sacrificed that much sleep if the marks would have been the same under some of these circumstances? Maybe not!

**Read the spec**. If there's *anything* ambiguous, **ask**. It's as true in business as school.
