---
title: "A sneaky Singapore trip"
date: "2023-04-13T08:53:42+09:00"
abstract: "We had a few hours, so went to town to show Seb the sights."
thumb: "https://rubenerd.com/files/2023/singapore2023-merlion@1x.jpg"
year: "2023"
category: Travel
tag:
- japan2023
- singapore
location: Singapore
---
We're off to Japan, after a few late starts, and despite the best efforts of family, health, and a multitude of other things that always seem to present themselves. But then, when is anything worth doing ever easy?

[List of all posts tagged with Japan2023](https://rubenerd.com/tag/japan2023/)

This time we're travelling with a dear friend of Clara's and mine we met back in the anime club at UTS. And rather than going direct to Tokyo, we went via Singapore so we could fly into Kansai instead, our favourite part of the country.

<figure><p><img src="https://rubenerd.com/files/2023/singapore2023-merlion@1x.jpg" alt="View of the Merlion and part of the Singapore CBD" srcset="https://rubenerd.com/files/2023/singapore2023-merlion@2x.jpg 2x" /></p></figure>

We had 7 hours in Singapore, so we did a sneaky trip into town on the MRT for the obligatory Merlion shot for Seb, and to have some food. I must have been on that viewing platform with friends and family a million times, but it never gets old. I still hold out hope that one day I'll sling myself back there into a small apartment, not sure how. But I digress.

Much to our chagrin, the Aniplus Café temporarily hosting all our beloved Hololive characters wasn't open for when we were there, but that's a topic for another post, on account of Clara getting some highly embarrassing photos standing next to some of the cutouts!
