---
title: "Osaka stores remember decade-old Kyoani ♡"
date: "2023-04-14T18:44:55+09:00"
abstract: "Finding shitajiki of Suzumiya Haruhi and K-On!"
thumb: "https://rubenerd.com/files/2023/japan2023-kyoani@1x.jpg"
year: "2023"
category: Travel
tag:
- japan2023
- sos-dan
- suzumiya-haruhi
location: Osaka
---
Actually it's even worse than that, given the *Suzumiya Haruhi* anime came out in 2006, almost seventeen years ago. I remember thinking, as I'm sure we all did, that it was so novel and new. A science-fiction series disguised as a slice-of-life with cute characters, and episodes aired *out of order!?* Many of my blog posts from that time were laced with SOS-Dan, Hare Hare Yukai, and other references.

[View posts filed under Japan2023](https://rubenerd.com/tag/japan2023/)   
[My 2006 post about Haruhi dancing on FreeBSD](https://rubenerd.com/haruhi-suzumiya-on-freebsd/)   
[Wikipedia article on Japanese pencil boards](https://en.wikipedia.org/wiki/Pencil_board)

Now when I see *Haruhi*, and stuff from my beloved *K-On!*, I lunge at the opportunity to add them to the shitajiki folders. They're becoming harder to find, to the point where I doubt as many people even know what they are now, even in places like Nipponbashi/Den-Den Town in Osaka, or Akihabara in Tokyo.

<figure><p><img src="https://rubenerd.com/files/2023/japan2023-kyoani@1x.jpg" alt="Pencil boards of Suzumiya Harhuhi from her eponymous series, and Akiyama Mio from K-On!" style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2023/japan2023-kyoani@2x.jpg 2x" /></p></figure>

Forgive the awful lighting in this serviced apartment, it's not doing either of our heroines any favours here. Poor Mio looks practically wooden.  
