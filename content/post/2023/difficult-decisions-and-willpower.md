---
title: "Difficult decisions and willpower"
date: "2023-10-03T08:05:35+11:00"
abstract: "We have a finite capacity to make decisions."
year: "2023"
category: Thoughts
tag:
- life
- work
location: Sydney
---
I'm *obsessed* with the idea that we have a finite capacity for making decisions in the course of a day. It's why people are more reckless in the evenings when it comes to driving, gambling, and alcohol... we've been worn down by the day, but especially when it comes to our capacity for making good decisions.

If you imagine a Sims-style set of bars for hunger, energy, and comfort, I reckon there's another called willpower, or maybe "decision capacity".

I've noticed two ways this plays out for me. The first is that I can only make a fixed number of decisions a day before I feel tired or irritable when I encounter another one. Even tiny decisions accumulate, which is why I've been on such a kick lately to automate the small fries.  If the importance, urgency, and impact are all low, I really don't want to waste time and finite willpower thinking about them.

It sounds silly, but there are two equally great coffee shops within walking distance of our house. My path to reach each one diverges at one intersection, so I let the pedestrian traffic lights decide where I go! I'm happy with either one, and this way I'm never waiting long at the lights before at least one of them goes green.

I also need to acknowledge that the majority of my decision-making capacity will be used up on work days. Much of my job is spent troubleshooting problems for people, and dealing with open-ended tasks that can change depending on what I decide to do. I ran an experiment last month to see how many times I noticed myself having to make a decision, but I lost count after 36 times before lunch. Is this a lot? I'm not sure. It felt like it was!

But there's no question that big, life-changing decisions absorb more daily willpower for decisions. I had to make a couple of these yesterday, and I felt mentally spent. I could even find myself craving junk food as Clara and I walked down to get groceries, when I'm usually able to walk past such shops without a second thought.

I'm lucky that I live with someone who can keep me in check and accountable, and *vice-versa* when she's had a tough day. And when all else fails, we sleep on it. Through some mechanism I don't understand, I wake up with a recharged decision making centre almost every time. Iaving to decide something when you're mentally exhausted, irritable, or sad is one of the worst feelings in the world... and you're not giving yourself the capacity to make a good decision either.

I wrote this open-ended post yesterday, but couldn't *decide* whether to post it. In the light of this lovely new day, I figured why not.
