---
title: "Sites redirecting you to localised nothing"
date: "2023-06-18T07:30:30+10:00"
abstract: "There’s no point redirecting people to something that doesn’t exit."
year: "2023"
category: Internet
tag:
- accessibility
- localisation
location: Sydney
---
This is happening more and more. I'll visit a global website for a computer or camera site, and the server will detect I'm in a different country. They'll claim there's an Australian or Singapore-specific page I can go to for more relevant information, which I'll click through to. Invariably, I get a 404 on that localised site.

Not *out of stock*, or *this isn't available*, or *here's an equivalent local device*. Nothing.

I get it; subsidiaries and regional differences mean that devices might have different levels of stock, be subject to different laws, or even have localised versions that don't map 1:1 with those offered in other places. But if there's no equivalent local version, why offer to redirect it in the first place? Seems weird, and only *slightly* frustrating!

