---
title: "Security and feature updates should be discrete"
date: "2023-08-17T08:10:17+10:00"
abstract: "If we want to reduce alert fatigue, we need a more mature approach."
year: "2023"
category: Software
tag:
- security
location: Sydney
---
Updates are critical to maintaining the security of our computer systems, but they can be disruptive to work or leisure time, can result in changes to interfaces and functions, and only ever seem to increase in number. These have trained people to ignore and dismiss them, regardless of their severity or importance.

We can't get rid of the need for updates (at least, not in the way we currently write and deliver consumer software), but we can improve our odds of them being taken seriously.

If we want to reduce alert fatigue, we need to be treating security and feature updates as separate, or at least as much as possible. Even if the underlying mechanisms are the same, they need to be surfaced differently in interfaces. People need to understand that a new feature is cool or even useful, but this one security change is *critical*.

I see too many software houses attempt to Trojan Horse entirely new features or unrelated changes alongside their critical security updates, which only makes people reluctant to apply them. Like vaccines, everyone benefits when people around them have patched machines.

