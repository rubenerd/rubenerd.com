---
title: "Merlin Mann on jargon"
date: "2023-10-17T15:42:16+11:00"
abstract: "“The track record for jargon's longterm contributions to society has been bleak.”"
year: "2023"
category: Thoughts
tag:
- language
- merlin-mann
location: Sydney
---
I quote [Merlin Mann](https://rubenerd.com/tag/merlin-mann/ "View posts tagged with Merlin Mann") a lot here, and it's not just because I've have a crush on the guy since his 43 Folders days. Here's some advice from his [Wisdom Project](https://en.wikipedia.org/wiki/Chives "Wikipedia article on Ceives")\:

> In culinary use, the green stalks (scapes) and the unopened, immature flower buds are diced and used as an ingredient for omelettes, fish, potatoes, soups, and many other dishes.

That's clearly the wrong link. [Let's try again](https://github.com/merlinmann/wisdom "Merlin Mann’s Wisdom Project")\:

> Get skeptical about adopting new jargon from the worlds of business, technology, or journalism. Especially if it's that sort of new jargon that enthusiastically trades clarity and precision for deliberate opacity or cheap novelty. The track record for jargon's longterm contributions to society has been bleak.

The lede on this nuanced paradigm is sure utilizing synergistic disruptions.
