---
title: "Still NAT’ing with IPv6"
date: "2023-07-06T11:11:56+10:00"
abstract: "Want static IPs on your home network? Good luck with that."
year: "2023"
category: Internet
tag:
- ipv6
- networking
location: Sydney
---
A commonly-touted benefit of IPv6 is that it does away with kludge of network address translation. NATs were only necessary because of IPv4 address space exhaustion, and so on. But then, private IPv6 addresses exist, so what gives?

I am *not* a network engineer, as will soon become apparent reading this post! But having had some fun with my ISP recently, I now have a bit more personal experience for why "IPv6 gets rid of NATs" is more a goal than a practical reality.

In IPv4 land, your ISP DHCP's a public IP address to your router, which then NATs to your local area network. I've always assigned static IPs to devices that don't move, like desktops, printers, home servers, and jails/VMs on those servers, and DHCP the rest. Already some of you might know where this is going.

Things are different with IPv6. I learned that ISPs typically delegate you an entire **/48** range of addresses, from which your router DHCP's to your local machines. This negates the need for NAT and port forwarding, because every end point is globally routable.

After you get over the confusion of your ISP cutting you over without warning while you sleep, and everything on your network having to be rebuilt while your partner frantically attempts to join her client video call, happy days!

But this presents a few problems.

I'll admit, it scares me a *little* having a public IP on each device. Granted, it's still going through my router and firewall, and there are plenty of ways to fingerprint an endpoint that doesn't rely on matching its IP. But it still obfuscates my LAN that a flat IPv6 network doesn't. Given the original IPv6 proposal literally baked MAC addresses into IPv6 addresses, privacy was clearly never a primary concern.

But my much larger problem comes to allocating internal network addresses. As a renter bouncing between apartments in this brave new world of housing (un)affordability, I churn between ISPs and locations with depressing regularly. This results in a new **/48** delegation each time, which means my static internal addresses... aren't anymore. I've also had my **/48** change even on the same ISP, because #AustralianInternet.

This means, functionally, I have surrendered control of my IP space. I've coupled my home network to the whims of my ISP, or my physical location. This doesn't feel like progress.

Now, you might say I should just SLAAC everything and be done with it, but what about my static hosts? Whereas before I assigned IPv4 addresses from an unchanging subnet, now I have to reconfigure addresses from a new **/48** each time. This is incredibly tedious for anything more than a trivial home network, and additional work I simply didn't have to do before.

I've asked a few network engineers, and the only real solution is to use a private IPv6 network with NAT66, which maps 1:1 global and local addresses. Sounds a bit like a NAT. With this, and NAT64 to talk with the bulk of the web, and I've doubled my number of NATs rather than eliminating them.

I'm not sure what the solution is here. I can't permanently reserve a **/48** with the APNIC unless I'm a business, and how would I even get an ISP to advertise it? Thanks also to the *Second-System Effect* baked into the design of IPv6, it's been two decades and IPv4 is still ubiquitous.

Technology always has to be weighed on its utility versus cost. My current IPv6 network is fine, but it's not as cut and dry as I feel I was lead to believe.
