---
title: "Things I’d love to build this year"
date: "2023-01-20T07:54:40+11:00"
abstract: "They only constrains are... time and money!"
year: "2023"
category: Hardware
tag:
- commodore
- commodore-64
- commodore-vic20
- figures
- pidp11
- pidp8
location: Sydney
---
Some pie-in-the-sky ideas, in no particular order. Which I suppose is still an *order*, just not an orderly one. Wait, what?

* **A new from parts Commodore VIC-20/VC-20**, if possible. The [Vicky Twenty](https://www.tindie.com/products/bobsbits/vicky-twenty-commodore-vic-20-pcb-replica/) board looks gorgeous, and I have a spare C64 keyboard that would be electrically compatible.

* **A Commodore cart kit**, whether it be something fancy like a Kungfu Flash or EasyFlash 3, or just a diagnostic cart. Maybe I should start on the latter and work my way up, considering the potential damage they can inflict if not built correctly.

* **A new game machine for Clara**. She's content using my hand-me-down GTX970 machine in her cute new pink NR200P, but I'd love to upgrade her to something better. Powerwashing Simulator on a 1440p display is really stretching its limits.

* **A PiDP-11 kit**. By the end of the year I hope to have the confidence in my soldering skills to [assemble one of these beautiful kits](https://obsolescence.wixsite.com/obsolescence/pidp-11). I think it'd be great to learn more about the history of UNIX and DEC, and it would look *so cool*. Except then I might also be tempted by a **PiDP-8** kit. Damn it...

* **A PiDP-8 kit**. As per above, because I'm the highly-suggestible type.

And if I'm really reaching:

* **A new Flash-based NAS and FreeBSD bhyve server**. I'd *love* to replace all the heavy, large, slow(ish) spinning rust with a NAS with SSDs. It'd barely take up any space, would use less power, and presumably would have faster IO. Prices per gigabyte still haven't dropped below Ferrari territory, but I'm keeping an eye on it.

* **An anime figure garage kit**. My painting skills are somewhere between limited and non existent, but that hasn't stopped me before!

All of these require two precious commodities: free time, and money. Let's see what supply I have of each this year.
