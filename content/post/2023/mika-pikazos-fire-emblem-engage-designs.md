---
title: "Mika Pikazo’s Fire Emblem Engage designs"
date: "2023-03-30T09:15:23+11:00"
abstract: "They’re so cool!"
thumb: "https://rubenerd.com/files/2023/fire-emblem-1@1x.jpg"
year: "2023"
category: Anime
tag:
- art
- games
- mika-pikazo
location: Sydney
---
I know I'm late to this, but [Mika Pikazo's](https://twitter.com/mikapikazo) character designs for *Fire Emblem Engage* are so cool! In an increasingly bland and monochromatic world, nobody does colour quite like her.

You can buy the game in physical stores, [and online](https://www.nintendo.com.au/games/nintendo-switch/fire-emblem-engage).

<figure><p><img src="https://rubenerd.com/files/2023/fire-emblem-1@1x.jpg" alt="" style="width:500px;" srcset="https://rubenerd.com/files/2023/fire-emblem-1@2x.jpg 2x" /><br /><img src="https://rubenerd.com/files/2023/fire-emblem-0@1x.jpg" alt="" style="width:500px;" srcset="https://rubenerd.com/files/2023/fire-emblem-0@2x.jpg 2x" /></p></figure>
