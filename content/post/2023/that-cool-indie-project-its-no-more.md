---
title: "That cool indie project? It’s no more!"
date: "2023-11-28T07:21:05+11:00"
abstract: "Indie hardware projects always wrap up and disappear as soon as I find them."
year: "2023"
category: Hardware
tag:
- lists
- retrocomputing
- shopping
location: Sydney
---
*I'm no Superman ♫*, in the words of Lazlo Bane. But I do have three unique, superhuman abilities I like to think are quite special:

* I can write lists of nonsense that some seem to take far too seriously.

* People *always* [stack it](https://en.wiktionary.org/wiki/stack_it "Wiktionary definition of Stack It") whenever I watch them on a scooter or skateboard. I bet I could even make Tony Hawk fall off just by watching him.

* Indie hardware projects always wrap up and disappear as soon as I find them.

It happens every single time.

1. I'll have a pressing requirement, or discover something that would solve my problem.

2. The designer has a crowdfunding page where you can back their project, or a Tindie store where you can buy their devices.

3. The crowdfunding page will show as fulfilled and closed, or the Tindie store will show *Out of Stock since 1951*.

I feel as though I need to stop looking for cool indie hardware projects, because I seem to kill them or push them into the past!
