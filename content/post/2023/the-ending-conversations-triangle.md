---
title: "The Ending Conversations Triangle"
date: "2023-10-28T09:30:22+10:00"
abstract: "It’s difficult to simultaneously do three things: end an interaction, be honest, and be considered polite."
year: "2023"
category: Thoughts
tag:
- communication
- language
- marketing-triangles
- psychology
location: Sydney
---
You know the marketing triangle that says you can have two of fast, cheap, or good? Joe Pisker [made this observation in the Atlantic](https://www.theatlantic.com/family/archive/2021/03/conversations-never-end/618309/ "How to End a Conversation Without Making Up an Excuse") 

> It’s difficult to simultaneously do three things: end an interaction, be honest, and be considered polite.

I've found that telling friends I'm an introvert has let me do all three! But this isn't broadly applicable.

My favourite tip from the article was to strategically deploy the "I'll let you go" line.
