---
title: "A hot spring weekend ☀️"
date: "2023-09-17T21:24:03+10:00"
abstract: "It already feels like summer, and it’s only been two weeks."
year: "2023"
category: Thoughts
tag:
- environment
- weather
location: Sydney
---
It was more than 30 degrees in Sydney this weekend. The cool sea breezes stopped it feeling hotter, but it was still a bit disconcerting.

It's been spring down here for two weeks, and it already feels like summer. It's not supposed to be like this. What are we doing?
