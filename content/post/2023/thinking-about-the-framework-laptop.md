---
title: "Thinking about the Framework laptop"
date: "2023-12-17T19:10:42+11:00"
abstract: "I love what it represents, and would love one to tinker with."
year: "2023"
category: Hardware
tag:
- bsd
- freebsd
- framework
- laptops
- linux
- right-to-repair
- pc-screen-syndrome
location: Sydney
---
I was window shopping at the [Framework laptop site](https://frame.work/ "Framework's official website"), like a gentleman, when I realised the cost being tallied was in *Australian dollars.* I'm so used to mentally converting any price I see online by at least 1.5, then dealing with a proxy service, that it was a pleasent surprise.

The Framework laptop is wonderful. It's modular hardware is easy to upgrade, repair, and build for your specific requirements. It also shows up huge companies  that would have you believe sealed boxes are the only viable method of delivering mobile compute. It's a political statement as much as it practical hardware.

<figure><p><img src="https://rubenerd.com/files/2023/framework13@1x.jpg" alt="Press photo showing two Framework 13 laptops" srcset="https://rubenerd.com/files/2023/framework13@2x.jpg 2x" style="width:500px;" /></p></figure>

Another benefit is the quality of the hardware they use. The 2:3 displays shipped with the 13-inch model are in the same league as Retina Macs, not the usual [low-resolution rubbish](https://rubenerd.com/why-do-most-pc-laptops-have-awful-screens/ "Why do most PC laptops have awful screens?") modern PCs ship with. I really wanted to love the KDE Slimbook, or even the Pinebook, but their displays are worse than what my MacBook Pro had *eleven years ago.* Once you get used to having 2× HiDPI, going back to HD is like stepping from SVGA to Hercules; you can tail more logs, photos pop, and everything in between.

I do want to drive this point home. Framework *could* have chosen a crappier display, and most PC people would have been fine. The fact they didn't shows their committment to making something *good*, not just passable. You also get your pick of AMD or Intel, and it'll even take the same M.2 SSDs I'm putting in my home server. I've been told Intel is a safer bet for the BSDs&mdash;particuarly around Wi-Fi&mdash;but I'd be keen to hear more feedback.


### Where to go from here

This comes at an interesting time. I'm at the point now where I *prefer* my FreeBSD/Fedora tower at home. KDE with Dolphin is a delightful environment to use, to the point where a Mac user of two and a half decades is ready to throw in the towel.

But these systems haven't traditionally done as well in mobile. My M1 MacBook Air gets more than a full day of battery life, has great performance, a Retina display, and a UNIX environment out of the box. I can attach a second monitor to it, have it suspend/resume without a second thought, and it runs all my work software. Granted the UI is going backwards, Apple's priorities haven't been in sync with mine since the iPod days, but it still puts PC laptops to absolute shame. 

Admittedly, a new laptop is far down on the budget list right now, behind some other fun&mdash;and other less fun&mdash;things coming up next year. But I'm still *very* interested. If this can do 90% of what my Mac can, and in a user-servicable package, they may have a convert.
