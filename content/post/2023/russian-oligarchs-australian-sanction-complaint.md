---
title: "Russian oligarch’s Australian sanction complaint"
date: "2023-10-02T07:49:09+11:00"
abstract: "What’s Russian for “the world’s smallest violin?”"
year: "2023"
category: Thoughts
tag:
- news
- war
location: Sydney
---
[The Guardian](https://www.theguardian.com/australia-news/2023/oct/02/russian-oligarch-oleg-deripaska-asks-court-to-render-australias-sanctions-regime-invalid)

> A Russian oligarch sanctioned over his alleged links to Vladimir Putin has asked a court to render Australia’s sanctions regime invalid, documents show.

He claims his massive aluminium company hasn't assisted Russia's effort to kill innocent Ukrainians, and therefore the sanctions against him are unjustified. In Australian vernacular, we would say "yeah, nah".

> Australia imposed sanctions on billionaire Oleg Deripaska in March last year, a move that prevented him from travelling to Australia or profiting from his company’s stake in an alumina refinery in Gladstone, Queensland.

What's Russian for *the world's smallest violin?* 🎻

