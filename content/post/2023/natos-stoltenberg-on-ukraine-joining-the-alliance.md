---
title: "NATO’s Stoltenberg on Ukraine joining the alliance"
date: "2023-04-08T07:05:34+10:00"
abstract: "They should have had support when Crimea was invaded."
year: "2023"
category: Thoughts
tag:
- peace
- politics
- ukraine
location: Sydney
---
[The Kiev Independent reported](https://kyivindependent.com/stoltenberg-says-ukraine-will-join-nato-vows-continued-support-despite-russias-dangerous-and-reckless-nuclear-rhetoric/)

> Responding to the Kyiv Independent at a news conference, Stoltenberg said that "NATO's position remains unchanged and that Ukraine will become a member of the alliance."
>
> NATO's long-time leader emphasized that the "main focus" now is to ensure that Ukraine remains a sovereign, independent nation in Europe.

Good. If it's the democratic will of Ukrainians, no other nation should be able to dictate their voluntary associations.

> Ukraine has long aspired to join NATO, while the alliance had kept the country at arm's length even before Russia's invasion.

This was shameful. It's good the country is finally getting support, but Putin's intent was plainly obvious when he invaded Crimea. Ukrainian families are broken with the loss of loved ones who should be with us today, not to mention the uncertain future a generation of conscientious Russian objectors face separated from their relatives. I've had emails from a few of you on both sides, and it's heartbreaking.

*(It's also interesting to see this in the context of Finland's NATO ascension, which hasn't generated near the same vitriol as Ukrainians wanting to be closer to Europe. It unmasks the lie that Russia invaded Ukraine to preserve sovereignty. Like any abusive partner, Putin shouldn't ask what he can do to stop them, he should ask what he's done to push them away. His guns mean he doesn't have to confront it now, but eventually he'll have no choice).*

[Donate to United24 for defence, demining, or for medical aid](https://u24.gov.ua/)  
[Donate to Unicef](https://www.unicef.org.au/donate/ukraine-emergency-appeal)

If you dismissed legitimate concerns of further invasion as warmongering, you can donate to their cause as a constructive penance. Ukrainians need all the love and support they can get right now. 🌻



