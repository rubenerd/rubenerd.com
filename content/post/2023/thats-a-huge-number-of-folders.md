---
title: "That’s a huge number of folders"
date: "2023-06-02T09:22:01+10:00"
abstract: "A quarter of a million folders detected on my tiny MacBook Air feels ridiculous!"
thumb: "https://rubenerd.com/files/2023/grandperspective-folders.png"
year: "2023"
category: Software
tag:
- apple
- disks
- file-systems
- freebsd
- macos
- netbsd
location: Sydney
---
I use a Mac for the technical sales side of my work, for the uninteresting reason that I need Microsoft Office. The internal SSD was starting to get full, so I ran GrandPerspective to surface where the space all went.

<figure><p><img src="https://rubenerd.com/files/2023/grandperspective-folders.png" alt="Screenshot showing the detection of 229,759 folders" style="width:436px;" /></p></figure>

Wait... that's almost a quarter of a million folders! On a MacBook Air with a stingy SSD? That's more virtual folders than I'd suspect all but the largest physical archives in the world would have.

It's at this stage I would lean back in my chair, fall over, pick myself up, then regale you all with tales of my childhood DOS days. I'd mention that I tried maintaining a clean directory structure, but would also limit the number of nested directories to a few dozen, given it'd be tedious to traverse anything complicated on a command line. I'd contrast this to my modern FreeBSD desktop and laptops having *orders of magnitude* fewer folders than this Mac despite having full KDE Plasma desktops, or my classic NetBSD systems sporting Fluxbox. Then finally, I'd attempt to make a point about the state of modern computing, and the never-ending spiral of complexity, and whether even the metaphor of a folder is even meaningful anymore.

There are a bunch of reasons why, but it only reinforces my feeling of merely being a guest on macOS. In 2023, I could still tell you where almost everything is on a FreeBSD or NetBSD system (and even Linux, despite their best efforts of late). On a Mac? I wouldn't have the foggiest idea of anything outside my home directory, `/usr/local`, and `/opt`.
