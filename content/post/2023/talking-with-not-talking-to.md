---
title: "Talking with, not talking to"
date: "2023-01-05T10:40:16+11:00"
abstract: "The former is duplex, the latter is simplex."
year: "2023"
category: Thoughts
tag:
- language
location: Sydney
---
My late mum Debra, whom I miss daily, demanded excellence with language. She pointed out that *gotten* isn't a word outside the US, that saying *like for example* is redundant, that it's a *moot* point not *mute*, and that we're *wont* to get these wrong, not *want*. I cheekily messaged her that her lose tap was hers to loose once, and was eviscerated by her humour and wit upon returning home.

My example of this is *taking to*, versus *talking with*. They're phrases we use all the time, but they have subtly different meanings and implications. 

Being *talked to* is simplex communication. You're being talked to at a lecture, or a disciplinary hearing, or the jury box. More information is being imparted on you than you're communicating back, if anything. There may also be a power structure at play, such as being *talked to* by your boss, or a police officer, or your parents as a child. The reverse is true if you're the one talking to someone.

*Talking with* someone is duplex, even if it's asynchronous or delayed. It's a conversation among peers, in which each party is communicating with the other. You talk with your friends, or your colleagues, or with the person over email. If there's a power structure, talking with them is an equaliser.

I'm careful to use one over the other, even though I'm sure most people don't notice... or more importantly, don't care! The latter are likely the wisest of us all.
