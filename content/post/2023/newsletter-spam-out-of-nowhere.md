---
title: "Newsletter spam… out of nowhere!"
date: "2023-03-01T08:32:00+11:00"
abstract: "Going from one email a month to two a day? That’s a paddlin’."
year: "2023"
category: Internet
tag:
- email
- marketing
- spam
location: Sydney
---
Until this morning, I'd been subscribed to an email newsletter for an event organiser. It was more of an announcement feed than anything else, which probably could have been better serviced by RSS.

Is there a gmane-like service, but that converts bulk email received in a special inbox to an RSS feed? Now there's an idea! But I digress.


### Opening the floodgates

Having emailed me one a month, or even once a quarter on occasion, this event organiser decided that escalating this to one to three emails **a day** was a good idea. This represents a thirty-fold increase at minimum, which is quite the feat given their emails printed on paper could [barely achieve a tenth of that](https://www.sciencefocus.com/science/whats-the-maximum-number-of-times-that-you-can-fold-a-piece-of-paper/ "What's the maxinum number of times you can fold a piece of paper?"). They've now cumulatively sent me more email in the last week than the last three years, combined.

Combined. Combine harvester. Is that what my inbox is to these people, a piece of farming equipment? Whatever intentions this service had, it lead to an unsubscription. Lead to me unsubscribing. UNSUBBING. HARRISON FORD. **GET ME OFF THIS LIST.**

*(A few of you asked if that silly line I keep co-opting is a reference to the seminal 1997 action film *Air Force One*, starring Harrison Ford. You are correct! Rebecca also asked me why. I wish I knew).*

Email is like any other tool, and should be evaluated by balancing its utility with its cost. This newsletter tipped the scales for me, so it was summarily removed from my inbox permanently. Whatever they hoped to tell me about will no longer be received at all.


### Why would they do this?

Checking the last few messages hints at the reason, and I don't think it's in the service of the reader.

Whereas previously their newsletters contained multiple stories broken down with headings (like a *newsletter!*), these new emails are atomic, with a single news story or event each. Sometimes single events are cleaved even further, with a separate email announcement for each new speaker to the same event. I checked just now, and they've mentioned the same person in three separate messages in as many days.

They could claim this reduces the chances of a reader missing out on a specific story that would otherwise be buried in a longer newsletter, but I don't buy that. It's more overhead for me to open nine individual emails than it is to open one. The added noise means I'm also *less* likely to open their messages, either because an individual story gets buried, or I start to subconsciously tune out anything from a specific sender. I see it as a subtle form of [alert](https://en.wikipedia.org/wiki/Alarm_fatigue) or [notification fatigue](https://www.pcmag.com/encyclopedia/term/notification-fatigue), a known phenomena that everyone from app developers to managers seem to ignore.

Metrics and tracking could be another reason. In the parlance of marketers, these individual messages might be easier to tie to specific campaigns, which means the event, its messaging, and the design of the email can be evaluated with a fine-tooth comb by tracking bounce rates and clickthroughs. An aggregate newsletter doesn't surface these additional details, which could be tied to someone's KPIs or other internal reporting. Again, none of this infrastructure helps the *reader*, and may even make their experience worse, but that's not the point.


### The backfire effect

But all of this is moot when you consider the *primary* metric they should be concerned about: the number of subscribers! It's an own goal if they assumed spamming people would result in more of their stuff being read. I can't imagine I'm the only one who unsubscribed as a result, thereby negating the entire exercise.

It's a bit of a metaphor for the modern Internet really. In the rush to track, optimise, and monetise every piece of "content", the web is made a little bit more crap. Or so an email told me.
