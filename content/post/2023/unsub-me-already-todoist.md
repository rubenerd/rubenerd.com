---
title: "Unsub Me Already: Todoist gets a fail"
date: "2023-12-16T09:02:56+11:00"
abstract: "Even their unsubscribe page 404’s!"
year: "2023"
category: Media
tag:
- from-unsub-me-already
- spam
location: Sydney
---
It's been a while since we've had an *[Unsub Me Already](https://rubenerd.com/tag/from-unsub-me-already/)* post. The only acceptable outcome from clicking unsubscribe in an email footer is immediately being unsubscribed! Let's see how Todoist goes.

Here's the email:

> The Todoist Monthly

I didn't subscribe to this. Footer:

> Inspiration Hub | Download Apps | Help Center | Unsubscribe

An unambiguous unsubscribe link, good. Let's click:

> todoist: Log in

That's an automatic **fail**. You should *not* need to log in to unsubscribe to anything.

But let's log in and see if we...

<figure><p><img src="https://rubenerd.com/files/2023/todoist-fail.png" alt="A 404 page for Todoist's unsubscribe page." style="width:500px;" /></p></figure>

Oh well! I guess Todoist is being flagged as spam.
