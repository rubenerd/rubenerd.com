---
title: "Exclamation marks"
date: "2023-09-06T08:49:56+10:00"
abstract: "An exclamation point is like laughing at your own joke!"
year: "2023"
category: Thoughts
tag:
- language
- punctuation
location: Sydney
---
[Quote Investigator, February 2015](https://quoteinvestigator.com/2015/02/05/laugh-own/)

> "Cut out all these exclamation points" [F. Scott Fitzgerald] said. "An exclamation point is like laughing at your own joke."

According to The Silver Searcher:

	$ cd ~/repos/rubenerd.com/content/post/
	$ ag --stats-only '!'
	==> 16482 matches

... whoops!
