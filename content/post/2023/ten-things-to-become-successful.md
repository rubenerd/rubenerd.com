---
title: "Ten things to become successful?"
date: "2023-02-02T10:04:09+11:00"
abstract: "Listicles with vague terms aren’t useful, but are everywhere."
year: "2023"
category: Media
tag:
- lists
- spam
location: Sydney
---
I saw one of these listicles speed past on the *Engagement Train* yesterday, along with the regular dubious life hacks and advertisements that seem to constitute the bulk of social media and search engine results thesedays. Not that I have a strong opinion either way about this, because that'd be eleven words.

These were their ten items quoted verbatim, which is not to be confused with the brand of floppy disks:

* Vision
* Consistency
* Adaptive environment
* Independent thinking
* Learning from mistakes

Wait, sorry I need a breather. That's a lovely tree outside my window; I never noticed that the leaves change colour during the day. That's beautiful. 

Okay, let's get through this last batch. Can someone hold my hand please?

* Goal-setting
* Hard work and persistent [sic]
* Mentorship seeking
* Collaboration
* Risk-taking

I confess, I changed one of them. Can you tell which one, without doing a search for the text? I might reveal it in a follow-up.

The ambiguous nature of these lists hint to their true purpose: to be quickly and easily disseminated for exposure. Any help or advice someone takes from them is entirely incidental, much like a sugar hit. I wanted to make a pun about film exposures being short lived, but I couldn't make it work.

Even ignoring the lack of context and survivorship bias that comes from blanket advice like this, a prescriptive list of well-defined, actionable items wouldn't fit in a screenshot or a tweet, nor would they solicit comments from people seeking clarification on what the flip *vision* means. Even reacting negatively is a trap when you measure success based on metrics such as views, shares, and comments.

And unfortunately, we're only going to see more of it. As I've come to say here over the years, it comes down to incentives. Unless you care about people, have ethics, or are passionate about writing, why put the extra effort into making something useful if there's no financial upside?

These lists are noise. And because they're full of the same clichés and internal inconsistencies, they're not even [useful noise](https://en.wikipedia.org/wiki/Entropy_(computing)). I think even an AI would take a break from drawing thirteen fingers and writing mediocre, error-filled docs to object to being trained on a dataset like this.

Maybe they lack *consistency* with their *adaptive environment*. I know I sure do, and I suspect you might as well! It's hard to be the same when you're changing.

