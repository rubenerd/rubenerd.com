---
title: "YAXI Ear Pads for the Porta Pro"
date: "2023-08-26T12:45:46+10:00"
abstract: "I’d recommend these even for new purchasers of the Kos Porta Pros."
thumb: "https://rubenerd.com/files/2023/yaxi-pads-porta-pros@1x.jpg"
year: "2023"
category: Hardware
tag:
- headphones
- music
- reviews
location: Sydney
---
These Kos Porta Pro X headphones continue to deliver my favourite home music experience. I'm no audiophile, but they have a richness of sound beaten only by my original bulky AKG monitors that were more than triple the price. I'd even use them for commuting if they weren't open-backed (others don't care if they subject others in a train to their music, but they deserve a trip to The Hague).

[2020: The Kos Porta Pro X headphones](https://rubenerd.com/the-kos-porta-pro-x-headphones/)

After three years of use though, the provided pads finally started coming apart. They were always on the thin side, so I thought I'd source some aftermarket ones that might offer a bit more comfort. I spent a bit of time researching, and found these Japanese *YAXI Ear Pads* for less than $30 shipped in Australia:

[Groove Central: YAXI Porta Pro Headphone Earpads](https://www.groovecentral.com.au/collections/yaxi-replacement-ear-pads/products/yaxi-porta-pro-koss-sennheiser-earpads)

Here they are installed on my Porta Pro Xs, with the tired old pads to the side for size reference:

<figure><p><img src="https://rubenerd.com/files/2023/yaxi-pads-porta-pros@1x.jpg" alt="Photo showing the large blue pads of the YAXI Ear Pads, with the much smaller and well-worn original pads alongside." style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2023/yaxi-pads-porta-pros@2x.jpg 2x" /></p></figure>

The material is much finer than the original pads, which feel sublime in combination with their larger size. They also have a bunch of fun colours, which is wonderful in our current world of blandness.

Unless you especially like the original pads, I'd recommend these even for new purchasers of the Kos Porta Pros. They bring the comfort of the headphones in line with their audio quality.
