---
title: "An amazing feature from my telco"
date: "2023-06-21T15:37:21+10:00"
abstract: "Summaries aren’t useful if they don’t tell you anything."
year: "2023"
category: Media
tag:
- spam
- telcos
location: Sydney
---
Clara’s and my telco just spammed us with this:

> "Living Network features: A range of innovative features that give you more from your connection, in the moments that matter to you."

It's not really a summary if it doesn't tell you anything about it. Didn't they also learn you're not supposed to use the term again in the definition?

I'm thinking I should rebrand my blog here to this:

> "Rubenerd: A range of innovative and paradigm disruptive nuances with synergies that captivate you... *to the next level!*"

Why are you looking at me like that?
