---
title: "Why is web search getting worse?"
date: "2023-09-27T08:06:59+10:00"
abstract: "SEO, AI-generated spam, and the web being filled with garbage."
year: "2023"
category: Internet
tag:
- ai
- spam
location: Sydney
---
[Charlie Warzel notes three reasons, in The Atlantic](https://www.removepaywall.com/article/current)

> There is a creeping sense—among frustrated programmers, searchers, and even journalists—that the site is no longer as useful or intuitive as it once was. One reason for this feeling may be that Google’s algorithms have been successfully gamed by low-quality websites and search-engine-optimization companies that help their clickbaity clients show up in the first page of Google Search results. 

I can still find things thesedays, but it's certainly a slog. And if you're doing general research for buying something? As my New York friends would say: *eh, forget about it.*

> There are indicators that the product might continue to struggle to deliver high-quality search results on an internet soon to be flooded with AI-generated photos, videos, and text.

Ironic given these companies are betting the farm on AI saving them from the onslaught. It's turtles all the way down, only they're generative tools classified by slave labour in Africa!

> The former Googler and ex–Yahoo CEO Marissa Mayer offered a charitable explanation as to why Google Search feels worse, arguing that the search engine is merely a reflection of the internet as a whole, which has become more complex and laden with fraudsters and garbage.

Grim, but also something I hadn't considered.

It's sad to think the web's best days were behind it, but it's future isn't set in stone either. Clearly it's current commercial custodians can't continue to... alliterate? Or dictate its future.

It makes me cautiously optimistic, weirdly enough. The bad web is beginning to collapse upon itself. And good riddance, I say.
