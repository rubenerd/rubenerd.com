---
title: "Styling OPML and RSS with XSLT to XHTML"
date: "2023-06-22T09:54:34+10:00"
abstract: "It works great, as long as browsers support it."
year: "2023"
category: Software
tag:
- opml
- rss
- xml
location: Sydney
---
*That's a few initialisms!*

I've been seeing a few people blog again recently about styling RSS feeds and OPML outlines. Paul Traylor whom I met on Mastodon emailed to share a post describing how this works with XSLT:

[PaulTraylor.net](https://paultraylor.net)  
[Darek Kay: Style your RSS feed](https://darekkay.com/blog/rss-styling/)

I do the same thing with my feeds. I normally see JavaScript libraries recommended, but it seems heavy and a bit redundant when our browsers can literally render it however we want for us, for free:

[My Blogroll](https://rubenerd.com/blogroll.opml)   
[My Omake outline](https://rubenerd.com/omake.opml)   
[My RSS feed](https://rubenerd.com/blogroll.opml)

As an aside, this flexibility of rendering XML was the primary reason those of us advocated so much for XHTML back in the day. It's not hard to see how you could build a data structure around an open data format, then render it for whatever viewport or requirement you need. I literally had an early version of another blog that didn't just use OPML as a blogroll, but as the data store itself!

XHTML was pretty forward-thinking for the time. We had WAP and XVGA monitors back then, but now we have phones, tablets, laptops, workstations, e-readers, and watches with a dizzying number of resolutions, aspect ratios, and technical capabilities. We twist ourselves into knots supporting all of them, or we wait for commercial browsers to implement what we want, or we optimise for one at the expense of others. With tools like XSLT, this would be so much easier.

But back to the nominal topic at hand! Using XSLT in 2023 comes with a large caveat: I'm not sure for how much longer our browsers will support XSLT, and XML more broadly. Version 2.0 has been around for years, yet no major browser has implemented it. I'm sure there will be an inexperienced developer somewhere who hasn't heard of the tech, and will advocate for its removal from a popular browser engine on the basis it's legacy or outdated.

Unfortunately, there's precedent for this behaviour. The same thing happened with RSS, for the same short-sighted misunderstanding that a lack of experience with something equates to a lack of utility or purpose. We're still paying the price for the removal of RSS support to this day, right at a time when closed social networks are eating their own heads.

Feel free to take a look at the source of my RSS and OPML if there's anything useful to glean from them. I think a bit of styling does make them nicer to use, and may even encourage others to give the tech a try. The best way we can push back against people saying "nobody uses RSS or XML lmao" is to use them!

Thanks again to Paul for sharing that link, and to Darek for providing concise, easy to follow instructions for how to use it.
