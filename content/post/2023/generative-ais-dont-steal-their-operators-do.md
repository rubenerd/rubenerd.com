---
title: "Generative AIs don’t steal; their operators do"
date: "2023-12-06T17:28:02+11:00"
abstract: "hbomberman's post about plaigarism made me rethink my position a bit."
year: "2023"
category: Ethics
tag:
- internet
- generative-ai
- llms
- plaigarism
- spam
location: Sydney
---
A recurring theme here this year has been my conscientious objection to how large-language models and generative AI tools are trained on peoples' work without permission, attribution, or compensation. [The legality](https://rubenerd.com/lawsuit-against-openai-for-theft/ "Lawsuit against OpenAI, and generative AI theft") and [business viability](https://rubenerd.com/the-grift-shift/ "The Grift Shift") concern me less than the grim prospect of someone's inspiration, love, and creativity being [churned like butter](https://rubenerd.com/ai-developers-credit-your-sources/ "AI developers: be nice, and credit your artists") for commercial gain and [spam](https://amycastor.com/2023/09/12/pivot-to-ai-pay-no-attention-to-the-man-behind-the-curtain/ "Amy Castor and David Gerard: Pivot to AI: Pay no attention to the man behind the curtain"). Ross Anderson from Cambridge [likens their entropic decay to pollution](https://www.lightbluetouchpaper.org/2023/06/06/will-gpt-models-choke-on-their-own-exhaust/ "Will GPT models choke on their own exhaust?"), which also demonstrates why these tools aren't "inspired" by works as creative people are. Humans create, AIs consume.

Here comes the proverbial posterior prognostication: *but...* there's an angle to this I hadn't considered, and that I now concede I got wrong. [hbomberguy's video on plagiarism 📺](https://www.youtube.com/watch?v=yDp3cB5fHXQ&t=12587s "Plagiarism and You(Tube)") concludes with a brief section on generative AI:

> As technology progresses, the methods for plagiarism are getting stranger and more complex. The hot new tool in the Stealing Tools box is Generative AI ... which can produce new art or words on command, using a process known as "stealing". Sorry, that's an oversimplification. The process is called "complicated stealing".
>
> Theoretically, generative AI works by training on a large dataset of existing material to figure out how to make new things. [...] How did they get all that material? Well, by taking a lot of stuff without the creators' consent.
>
> $VENDOR has intricate knowledge of copyrighted works it shouldn't legally have access to [...] but you bet they're charging you a monthly subscription for using Theft Machine 4.0!
>
> It's heartening to think [...] these aren't really even AI. A person had to make [this material]. It's still just people doing the stealing here.

He's more right than he thinks. The data is created by people, but it's also *classified* by people for basically [slave wages](https://www.wired.co.uk/article/low-paid-workers-are-training-ai-models-for-tech-giants "Wired: Low paid workers are training AI models for tech giants"). AI&mdash;at least in popular usage&mdash;[isn't artificial or intelligent](https://www.scientificamerican.com/article/chatgpt-and-other-language-ais-are-nothing-without-humans/ "ChatGPT and Other Language AIs Are Nothing without Humans"); though there's certainly a lot of marketing trying to convince us otherwise.

He then drops a truth bomb:

> Pointing out AI was involved is almost a misdirection. A human being stole [material] and made his own. All the AI did was speed up the process. People have been ripping each other off long before AI, and for now, it's still people.

This surprisingly-optimistic distinction between tool and human operator is significant, and exposes a blind spot I've had when discussing the topic. LLMs exert their [negative externalities](https://www.britannica.com/topic/negative-externality "Britannica article on Negative Externalities") on the world once a human invokes them to act irresponsibly.

It's a human who decides to generate millions of [junk pages](https://www.technologyreview.com/2023/06/26/1075504/junk-websites-filled-with-ai-generated-text-are-pulling-in-money-from-programmatic-ads/ "Junk websites filled with AI-generated text are pulling in money from programmatic ads") to sell ads. Humans ask AIs to generate a simulacrum of art to [avoid paying an artist](https://tenforward.social/@noracodes/111536635074220985 "Nora Tindall: Can you imagine if a tenth of the money and manpower spent on just Midjourney and DALL-E had gone to arts grants?") what they paid for a tool subscription or dataset. Humans pass off [substandard code](https://matthewbutterick.com/chron/this-copilot-is-stupid-and-wants-to-kill-me.html "This copilot is stupid and wants to kill me") and [documentation](https://legalvision.com.au/risks-using-generative-ai-drafting-business-documents/ "https://legalvision.com.au/risks-using-generative-ai-drafting-business-documents/") out of prior work, with [glaring security issues](https://infosecwriteups.com/the-2023-ai-generated-code-security-report-by-snyk-9fb79fa04903 "The 2023 AI-Generated Code Security Report — By Snyk"), privacy violations, and <a href="https://en.wikipedia.org/wiki/Hallucination_(artificial_intelligence)" title="Hallucination (artificial intelligence)">inaccuracies</a> they justify as being better than what's out there now. Humans use them to [entrench negative stereotypes](https://www.bloomberg.com/graphics/2023-generative-ai-bias/ "Humans Are Biased. Generative AI Is Even Worse"), or to plagiarise, or to lie on a report, or to cheat on an assignment, or to spam a creative site [into shutting down](https://futurism.com/the-byte/sci-fi-magazine-shuts-down-submissions-ai-spam "Sci-Fi Magazine Shuts Down Submissions After Onslaught of AI-Generated Spam"), or to [waste our time](https://rubenerd.com/ai-security-companies-spamming-abuse-lines/ "AI companies spamming abuse email addresses"), or pass off creativity as their own, or to... okay, we all get it.

Sure, people will use generative AI to act without moral scruples, all in the name of making a buck and avoiding paying creative people. But it's sadly only the latest chapter in a sordid history of screwing the little guy. We're seeing similar rumblings in streaming music, where piracy was "solved" by giving large platforms money and the artists a pittance. Dan Olson has exposed the exploitive [ghost-writing industry 📺](https://www.youtube.com/watch?v=biYciU1uiUw "Contrepreneurs: The Mikkelson Twins"), and Tom Nicolas has covered [online scams 📺](https://www.youtube.com/watch?v=2bq3SdfzcA4 "Griftonomics") more broadly.

But this is where I see signs of hope.

First, we're starting to see [the bottom fall out](https://archive.is/LCnK0 "Archived post from @flashman showing a drop in traffic after an alleged SEO heist") on some of this generated guff, which can only be a good thing for the health and viability of this wonderful web we inhabit. Even legislators are finally taking notice, because if an industry has demonstrated it's unwilling to act in the best interests of a society, they can't whinge when the book is thrown at them.

More broadly though, I'm overjoyed to see these actions putting attention back onto the very creative people who's work makes our world beautiful and fun. They, and their work, deserve to be respected as much as it is enjoyed.

