---
title: "Changing sites for new stakeholders (aka: not us)"
date: "2023-04-26T16:03:40+10:00"
abstract: "You can just tell when the creators of a tool use their own stuff and engage with the community."
year: "2023"
category: Software
tag:
- cory-doctorow
- enshittification
- manton-reece
- tracking
location: Sydney
---
Manton Reece microblogged about a phenomena to which every tired engineer and corporate officer worker can attest:

> This is not exactly a commentary on any specific platform, but you can just tell when the creators of a tool use their own stuff and engage with the community. The farther you get away from that, the less confidence users will have.

It really is that obvious! Yet few companies larger than a dozen or so people ever get it, either because they're blissfully unaware, or they're structurally incapable of doing so. I'm starting to think it's more of the latter.

[Manton's microblog post on Tuesday](https://www.manton.org/2023/04/25/this-is-not.html)

This phenomena manifests in a few ways. Traditionally it's been why middleware and enterprise software has been so gosh darn awful to use, because of the truism that the people making the purchasing decisions for these tools aren't the ones using them. There's a reason why for the longest time people's *bring-your-own-devices* differed so starkly from what their companies provided (phones, laptops, toasters).

But I think the more tragic examples are those that *previously* served people and had community engagement as Manton describes, but have since reached their *enshittification* stage. Think of social networks, password managers, or search engines people enjoyed using, before being retooled to serve a different customer, such as an advertiser. You're now the product, or thanks to the network effect, ancillary as an individual to the platform's goals and agenda. Fun!

[Cory's Wikipedia article mentioning enshittification](https://en.wikipedia.org/wiki/Cory_Doctorow#Degradation_of_the_online_environment)   
[John Naughton's article on enshittification](https://www.theguardian.com/commentisfree/2023/mar/11/users-advertisers-we-are-all-trapped-in-the-enshittification-of-the-internet)

This first appears in the form of features that are, at best, tolerated by the people using the systems before becoming outright hostile to them. Nobody that uses software asks for native applications to be re-written in Electron, or for timelines to be received out of order, or to be tracked for advertising, or to receive a swift punch to the face by an auto-extending boxing glove. These are the *enshittification* features Cory describes designed to extract value out of something, not to contribute to it.

When these bad new features face resistance, the developers will shore up their offerings by hampering interoperability. RSS is a popular one for the chopping block with a bizarre line of people gleeful for its demise, along with APIs and access tokens that may be deprecated, paywalled, restricted, broken, or cynically left static as the rest of the system adds features.

[The surprising backlash against RSS](https://rubenerd.com/the-surprising-backlash-against-rss/)

Which raises the question: why would these features ever be included in the first place, if we all know they'll be removed in the future? Or asked another way: if businesses work in their own interests, why waste resources?

*Turns out*, they expose the conceit. Such features are necessary for growing a base of people using the platform, but they run counter to the interests of new stakeholders like advertisers and investors. It's difficult to force unwanted features on people if they can outsource access to a third party that respects them, thanks to interoperable standards and formats.

*(I'm trying hard not to reduce people to "users" or "customers" here, but it's... harder than I expected)!*

A drop-off in community engagement follows, once it's established the people using their tools are no longer the  priority. Avenues for feedback are quietly closed, the original developers or owners appear to stop using the service and fade away behind PR, and the remaining people still attempting to reach out are met with canned, vague assurances that *we take your concerns seriously*. The fact they even need to assure us of that is troubling.

The silver lining here is that it opens the market for new developers to serve people again, until they reach the same scale and and fall for the same siren song. In the immortal words of Jim Kloss, *it's a big ol' circle.*

Which gets us back to Manton's final point about confidence. I'd take it a step further, and say that using your own tools and engaging with the community builds *trust*. This explains why software and website developers are so routinely surprised or bemused at the visceral reactions to product changes that aren't in peoples' interests. Defenders of these practices will laugh it off as *caveat emptor*, or some other Latin phrase they read on a forum somewhere that makes them feel so... phisticated.

Making things a bit crappier in the service of another stakeholder is one thing, but violating trust feels hostile, because it is. Companies make this mistake *every... single... time,* and it leads to their downfall. Usually.

This is deeply fascinating to me, because just like soylent green, companies are made of people. People who, when they get home, are just as frustrated by things getting worse as anyone else. This suggests to me the issue is endemic to tech companies. If so, this needs to change.

