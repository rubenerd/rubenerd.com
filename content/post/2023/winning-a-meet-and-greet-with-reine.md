---
title: "Winning a meet-and-greet with Pavolia Reine"
date: "2023-06-10T08:33:36+10:00"
abstract: "SMASH held a lottery for slots to meet her at their next event!"
thumb: "https://rubenerd.com/files/2023/reine@1x.jpg"
year: "2023"
category: Anime
tag:
- hololive
- hololive-indonesia
- pavolia-reine
- smash
location: Sydney
---
I needed some good news last week, and the Sydney Manga and Anime Show delivered! The event held a contest to let you book a meet and greet with a couple of talents from Hololive Indonesia at their event in early July and I got a slot with my oshi Pavolia Reine.

[Hololive: Our Bright Parade](https://rubenerd.com/hololive-our-bright-parade/)   
[Pavolia Reine on YouTube](https://www.youtube.com/channel/UChgTyjG-pdNvxxhdsXfHQ5Q)

I've talked about Reine a few times here, most recently in her HoloFes concert performance linked to above. HoloID hits different, and her streams especially. She was my gateway to everything from Persona 5 to writing fanfic in Minecraft! Most Holo streamers do catchup streams for superchats and readings, but I think she does them the best with her brunch chats. I also love her interactions with other members, especially with Ollie, Moona, and Fauna from EN Council. 

<figure><p><img src="https://rubenerd.com/files/2023/reine@1x.jpg" alt="Press image of Reine from her debut." style="width:400px;" srcset="https://rubenerd.com/files/2023/reine@2x.jpg 2x" /></p></figure>

It took a while for me to pinpoint why her accent, mannerisms, and jokes felt so nostalgic, but she sounds like so many of my friends from high school in Singapore back in the day. There's one especially who I'd swear she's either her sister, a clone, or she quit her job to pursue a career as an idol.

I'm shy in person to the point of it being painful, but if the way she handles her live chat is any indication, I'm sure it's nothing she can't handle. I'll need to track down some teal and blue clothes. *Apa k-kabar?*



