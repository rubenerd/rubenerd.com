---
title: "Slap anime on it"
date: "2023-10-11T08:55:31+11:00"
abstract: "I’d never be caught doing this."
year: "2023"
category: Anime
tag:
- hololive
- holostars
- josuiji-shinri
- ssss-gridman
location: Sydney
---
[Via my favourite HOLOSTARS-EN streamer, Josuiji Shinri](https://archive.is/HhSll)\:

> honestly though, if you just slap anime on something, i'm more likely to enjoy it. coffee? slap anime on it. vegetables? slap anime on it. paying bills and rent? oof, but slap anime on it.

Good thing nobody would ever accuse me of doing that here. Isn't that right, best characters from *Gridman?*

<figure><p><img src="https://rubenerd.com/files/2023/gridman-cute@1x.jpg" alt="Key visual from the series SSSS.GRIDMAN." srcset="https://rubenerd.com/files/2023/gridman-cute@2x.jpg 2x" ></p></figure>
