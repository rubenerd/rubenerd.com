---
title: "Expanding science beyond English"
date: "2023-09-03T07:44:24+10:00"
abstract: "“We have to accommodate the language barrier or risk losing their potential.”"
year: "2023"
category: Thoughts
tag:
- language
- science
location: Sydney
---
Gabriel Nakamura and Bruno E. Soares wrote an important article for the Scientific American:

[English May Be Science’s Native Language, but It’s Not Native to All Scientists](https://www.scientificamerican.com/article/english-may-be-sciences-native-language-but-its-not-native-to-all-scientists/)

> Recently, a team of researchers led by Tatsuya Amano of the University of Queensland tried to quantify the time and career costs of lower English proficiency. Whether needing nearly twice as many minutes to read in English and up to 51 percent more time to write in English than native English speakers or being about 2.5 times more likely than a native English speaker to have journal editors reject their work on a basis of language, not having the advantage of English language in education unfairly punishes good scientists doing good research.
>
> There are talented scientists worldwide who do not speak fluent English. We have to accommodate the language barrier or risk losing their potential.

A couple of Ukranians among you have messaged me over the last year saying how learning English unlocked the world for you. I'm happy, but at the same time one language evolved on a swampy island in the Atlantic shouldn't be necessary for sharing your talents and knowledge with the world. *Especially* in a prescriptive field like science, where we need everyone's help confronting the massive global challenges we face.

This is what certain other native English speakers fail to grasp, overtly or otherwise. We worked hard to get where we are, just like everyone else, but we started with a unfair and unearned advantage. We should use our position to advocate for a broader base of ideas. Locking people out of the scientific community on the basis of language is absurd.

I suppose I big challenge would be having translators with sufficient scientific knowledge to accurately bridge that gap. Fortunately, we all use the same number system, constants, units, and formulas, and we're all bound by the same scientific laws. It's not like palladium is a gas at STP in Japan, or gravity doesn't affect bodies in Armenia!
