---
title: "CityNerd on working from coffee shops"
date: "2023-10-07T09:06:41+11:00"
abstract: "Keeping focus and enjoying the buzz."
year: "2023"
category: Thoughts
tag:
- citynerd
- coffee
- coffee-shops
- quotes
- work
location: Sydney
---
[From one of his latest videos](https://www.youtube.com/watch?v=tMmPWAC_eWQ)\:

> If you follow me on The Apps, you probably know I really like working in coffee shops. I like the buzz and human activity. And something about being under the watchful eye of complete strangers, and imagining they're judging me rather than completely ignoring me (which is much more likely), well it really helps keep me on task.

Coffee shops are my vice. There's something so comforting about a warm drink, and being surrounded by people without any social obligations. I love that I can have a quick chat with the barita, tip my hat at the other regulars, then get stuck into work and projects.

They're basically tables in an introvert paradise you can rent for a fixed fee, and you even get a free coffee.
