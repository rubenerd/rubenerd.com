---
title: "Retrocomputer FPGAs, featuring Wouter of Brain Baking"
date: "2023-12-02T11:49:20+11:00"
abstract: "FPGAs have ushered in a wonderful new generation of retro-inspired hardware."
year: "2023"
category: Hardware
tag:
- retrocomputing
location: Sydney
---
Back in October I [attempted to classify](https://rubenerd.com/what-counts-as-authentic-retrocomputing/ "What counts as authentic retrocomputing") what different people consider *authentic* retrocomputing, based on my own experiences blogging and discussing the hobby. It spanned the gamut between original hardware and emulators, with faithful reproductions and modern conveniences in between. My conclusion was that *any* retrocomputing activity counts, though I understood the appeal of each level.

Level four involved FPGAs:

> Those willing to accept drop-in replacements that are functionally equivalent, but operate differently from original components. These include modern ICs or even full FPGA re-implementations. The latter especially causes consternation, with detractors arguing you’re introducing chips more powerful than the original, but for a secondary role. They’re probably against maths coprocessors, too.

You can tell from my tone that I was frustrated with people saying the tech doesn't result in an *authentic* experience. I get the philosophical position that you're introducing a far more powerful CPU to render HDMI graphics for your 6502-based machine, but who cares?

Gate-keeping aside, I'd also argue such an attitude ignores so much wonderful stuff being developed now, all thanks to FPGAs. I'd rank the accessibility of these chips alongside the 3D printer as enabling a new generation of hardware to replace, supplement, or expand upon retro tech that may be broken, difficult, or expensive to use. That's wonderful!

[Wouter of Brain Baking credits affordable FPGAs](https://brainbaking.com/post/2023/11/fpgas-and-the-renaissance-of-retro-hardware/ "FPGAs And The Renaissance Of Retro Hardware ") are a primary reason for the growth of retrocomputing hardware in recent years:

> Almost every few weeks, a new and exciting retro hardware project is announced. These are truly great times for retro computing enthusiasts. How come these hobby projects exploded in popularity lately, besides the obvious growth in demand and the availability of crowdfunding platforms such as Kickstarter that at least partially remove the stress of funding? One answer could be the rapid evolution and availability of FPGA chips. [...]
>
> Why? Because you don’t need a powerful FPGA chip to emulate an old embedded 8-bit CPU. In other words: slap an FPGA on a board, flash it to perfectly emulate your favorite retro hardware, and you’ve got yourself a beefed up Game Boy/Commodore/Whatever!

Want a Gravis Ultrasound for less than the cost of a kidney? How about VGA-quality 80-column colour from your Apple //e? Or seamless storage for a Commodore 16? What about accessing a BBS via Wi-Fi? As Wouter says, every month I learn of some ingenious new kit that makes these wonderful machines sing, and we have FPGAs to thank for much of it.

As I said in that original post, I'm sure we'd all love to have authentic hardware so we could recreate nostalgic setups, or use the machines as originally intended. But this new generation of hardware has its place, and it's making the hobby more accessible, fun, and affordable for everyone. That's a cause for celebration, and why I agree that we're living in the golden age of retrocomputing now.
