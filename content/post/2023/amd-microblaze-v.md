---
title: "AMD Microblaze V, and CPU diversity"
date: "2023-11-08T11:24:58+11:00"
abstract: "I didn’t know this existed!"
year: "2023"
category: Hardware
tag:
- amd
- alpha
- arm
- cpus
- intel
- mips
- powerpc
- risc-v
- solaris
- sun-microsystems
location: Sydney
---
Speaking of AMD! My colleague shared [this product page](https://www.xilinx.com/products/design-tools/microblaze-v.html "AMD MicroBlaze™ V Processor: A Flexible and Efficient RISC-V Processor")\:

> The AMD MicroBlaze™ V processor is a soft-core RISC-V processor IP for AMD adaptive SoCs and FPGAs. The MicroBlaze V processor is based on a 32-bit RISC-V instruction set architecture (ISA).

These have likely been made for a while, but this is my first time hearing of them. Friggen cool, is my initial impression.

This is clearly a different market segment to desktop and mobile CPUs, but it's still intersting to learn in the context of AMD delivering ARM chips as well. AMD have a perpetual licence to develop x86, but you'd think they'd have less to lose pivoting to other architectures as Intel would. It's cool to imagine AMD being a trailblazing company making ARM and RISC-V hardware.

AMD could be a recursive initialism for *AMD Makes Diversity!* And they could even bring Sailor V out of her 1990s retirement to help promote it!

<figure><p><img src="https://rubenerd.com/files/2023/sailor-v.jpg" alt="Sailor V" style="width:282px; height:503px" /></p></figure>

Wait, she's Sailor *Vee*, not Sailor *Five*. Mambo No... RISC?

If you'll stop interrupting me, being fabless comes with its own risks and limitations: not least, being beholden to the same manufacturing processes and queues as your competition. But it does mean AMD would be able to migrate their flagship hardware more easily than Intel with their x86 fabs. Or so I'd think; I could be wrong.

Which leads me to don my nostalgia flatcap. I kid, I never take it off.

A decade ago I lamented the loss or niche relegation of SPARC, Alpha, POWER/PowerPC, MIPS, and plenty of others. IT used to be *so cool* with such diversity... and secure. And thanks to a common set of network protocols, it was entirely feasible, and perhaps even preferable, to have a mix. Think of a cool Sun machine running illumos on SPARC for your OpenZFS, and Alpha hosting some mythical open-source VMS for engineering tasks. Those days largely disappeared as x86/amd64 swallowed the world, and with it, our lives became that much less interesting.

It might be a bit much to hope for more than *just* x86 and ARM; whether a third player ends up being RISC-V or otherwise. Another colleague who wrote his own CPU architecture at uni might have thoughts.

That reminds me, I still have that Kickstarter RISC-V board that I've never got around to writing about. Sailor V wouldn't shirk her responsibilities like that. She'd be too busy *processing* them.
