---
title: "Paul Simon parodying Bob Dylan"
date: "2023-08-15T07:43:04+10:00"
abstract: "Somebody’s tapped my phone!"
thumb: "https://rubenerd.com/files/2023/yt-utZ-r2ESRls@1x.jpg"
year: "2023"
category: Media
tag:
- bob-dylan
- music
- music-monday
- paul-simon
- simon-and-garfunkel
location: Sydney
---
It's Music Monday time! Wait, today is Tuesday. It's a *belated* Music Monday, the series where I post about much each Monday. Unless it's a Tuesday. It's still Monday on the other side of the date line, right?

[View all past Music Mondays](https://rubenerd.com/tag/music-monday/)

I think a lot of younger people are more familiar with individual songs from artists like The Beatles, and Simon and Garfunkel. It's part of a broader trend away from albums towards individual songs and custom playlists. There's nothing wrong with this approach; music is a personal thing, and everyone has their own tastes. But Clara and I have been going the other way, and listening to albums as the artists originally intended. It's been fun.

We got a copy of *Parsley, Sage, Rosemary, and Thyme* by the iconic duo, and were listening through when we heard a song that sounded *suspciously* Bob Dylan. It was really well executed, right down to his vocal delivery and obscure one liners (and I just discovered, somebody's tapped my phone)!

I had to do some digging about this song.

<figure><p><a target="_BLANK" href="https://www.youtube.com/watch?v=utZ-r2ESRls" title="Play A Simple Desultory Philippic"><img src="https://rubenerd.com/files/2023/yt-utZ-r2ESRls@1x.jpg" srcset="https://rubenerd.com/files/2023/yt-utZ-r2ESRls@1x.jpg 1x, https://rubenerd.com/files/2023/yt-utZ-r2ESRls@2x.jpg 2x" alt="Play A Simple Desultory Philippic" style="width:500px;height:281px;" /></a></p></figure>

[According to Arun Starkey from Far Out Magazine](https://faroutmagazine.co.uk/paul-simon-garfunkel-song-parody-bob-dylan/)

> In a Dylan-esque vocal delivery, he drones: “Not the same as you and me, he doesn’t dig poetry / He’s so unhip, when you say Dylan / He thinks you’re talking about Dylan Thomas, whoever he was.
>
> Although at first inspection, this might seem to be quite a thinly veiled dig at Bob Dylan, Simon has always maintained that it was actually written as a satirical exploration of his artistry rather than a full-on dig at a man whom he has frequently hailed as an inspiration. He told Rolling Stone: “One of my deficiencies is my voice sounds sincere. I’ve tried to sound ironic. I don’t. I can’t. With Dylan, everything he sings has two meanings. He’s telling you the truth and making fun at the same time.”

That's actually the best description of Bob Dylan's style I've ever read, and why I think some people never "got" his music. I wouldn't say I'm a huge fan, but some of it is pretty great... even some of his more modern albums like *Modern Times*.

But back to Simon and Garfunkel. I really enjoyed this album, and can see now why critics dubbed it their first masterpiece. This song was such an unexpected treat among a bunch of very familiar tunes.
