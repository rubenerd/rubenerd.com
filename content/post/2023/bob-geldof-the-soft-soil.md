---
title: "Bob Geldof, The Soft Soil"
date: "2023-08-28T16:25:23+10:00"
abstract: "Today’s Music Monday is an old favourite."
thumb: "https://rubenerd.com/files/2023/yt-cqhc0SS0uzA@1x.jpg"
year: "2023"
category: Media
tag:
- bob-geldof
- music
- music-monday
location: Sydney
---
Today's Music Monday is an old favourite. "The Happy Club" was one of those albums my parents always had on, either in the car or on the home Hi-Fi. I still grab the Walkman and put this on when I'm feeling a bit stressed, to remind me of happy times. Works a treat.

[Past Music Monday posts](https://rubenerd.com/tag/music-monday/)   
[The Soft Soil on YouTube](https://www.youtube.com/watch?v=cqhc0SS0uzA)   
[Related: The House at the Top of the World](https://rubenerd.com/the-house-at-the-top-of-the-world/)

<figure><p><a target=_BLANK href="https://www.youtube.com/watch?v=cqhc0SS0uzA" title="Play Soft Soil"><img src="https://rubenerd.com/files/2023/yt-cqhc0SS0uzA@1x.jpg" srcset="https://rubenerd.com/files/2023/yt-cqhc0SS0uzA@1x.jpg 1x, https://rubenerd.com/files/2023/yt-cqhc0SS0uzA@2x.jpg 2x" alt="Play Soft Soil" style="width:500px;height:281px;" /></a></p></figure>

*This may not mean a lot to you. It means a lot, to me ♫*
