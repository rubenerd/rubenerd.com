---
title: "A rattling truck of the mind"
date: "2023-02-27T08:31:09+11:00"
abstract: "My adventure with a truck that didn’t exist."
year: "2023"
category: Thoughts
tag:
- psychology
- sounds
location: Sydney
---
Last week I'd rushed back to my desk for a client meeting, when I heard the sound of a large truck idling outside. A driver had decided the street under our balcony was a fabulous place to stop and idle their behemoth while they did who knows what.

In a twist of pure evil, the manufacturer or maintainer of this vehicle's engine had tuned it perfectly to hit the resonate frequency of our apartment. The rumbling wasn't that audible, but it carried through the room's surroundings and drilled slowly into my skull as I sat there trying to concentrate on a call.

Our previous apartment was situated by a bus bay, where drivers would routinely park their boxy conveyances to make loud rattling sounds. Steam locomotives take many hours to "fire up" after ignition, so I assumed Sydney buses used the same engines. Why else would they idle for hours at a time wasting fuel and silence?

Back upstairs, the video call lasted almost an hour, much of which was spent being distracted by the rumbling of this truck. I could picture its large chrome exhaust pipes belching its acrid fumes, the semi-trailer parked across an access road, the cab hanging over the pedestrian crossing, and the driver sitting there not giving a shit. Maybe their company had messed up a schedule and had forced them to wait. Maybe they were being lazy, and were sitting there using the truck's air conditioning while they frustrated everyone within a hundred metre radius.

Upon completion of the call, I flung the headset off in pent-up frustration, and bolted towards the window to see who thought it was a good idea to idle their truck for *an hour*. I bumped one of our dehumidifiers as I stomped past, and I was almost glad that I had something to vent (hah!) my frustrations against.

The truck disappeared.

It didn't drive away, or even make up for its intrusion with some cool Doppler sounds. It vanished instantly, in a puff of diesel particulates.

It took a few seconds longer than it should have, but eventually I realised there hadn't been a truck at all. The dehumidifier had been up against one of our bookshelves, and I'd bumped it away. Positioning it back against the shelves, and the rumbling sound resumed.

What we had was a classic case of Acoustic Amplification, or as I abbreviate in this case: *AA... AAAAAAA!* The compressor in the dehumidifier does rattle quietly to itself sometimes, but against the large, hollow volume of the bookcase, it amplified the sound enough to fill the whole room. It had been this sound that preoccupied me for the better part of an hour, to the point where I was ready and willing to walk out onto my balcony and start shouting at a figment of my imagination.

I couldn't help but laugh. All the tension and frustration was still there, but now it was directed at myself for letting something so trivial and obvious preoccupy me.

What can we learn from this silly experience? I had three key takeaways, none of which could be used to turn the ignition on a truck:

1. I'm a *baka.*

2. My mind had filled in all the details about this alleged truck, right down to its design and the motives of its occupant, based *entirely* on a single rumbling sound. Which means:

3. I had more mental control over the perceptions of my environment than I thought.

I wonder how many other idling trucks I've had, or continue to have, in my mind? How many other times have I sat there fuming about something, or someone, based on details filled in by my subconsciousness? That's a lot of letters, as I'm sure there have been many occurrences.

