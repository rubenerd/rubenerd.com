---
title: "Coffee chatter about urbanism"
date: "2023-07-05T08:23:53+10:00"
abstract: "Gives me some perspective about what people like me are pushing against."
year: "2023"
category: Thoughts
tag:
- coffee-shops
- public-transport
location: Sydney
---
You hear some... things sitting at a coffee shop. Today's *very* loud conversation concerned real estate:

> The houses down there are insane *LAUGHS*. If I can't drive there, what's the point? *LAUGHS.* Build another damn freeway and more houses, stop faffing about.

It's important I hear stuff like this; it gives me valuable perspective I otherwise lack in my comfy bubble of sustainable urban planning and public transport. I have this caricature of a suburbanite in my head that earnestly believes "one more lane, bro"... but they do exist.

The conversation shifted soon after, but the bulk of what they discussed concerned traffic, a hatred of apartments, and the sky-high cost of living in Australia right now. A few times I felt like they were on the verge of an epiphany, but then they went to hating on buses and trains for the "people [who travel] on them".

*(Not wanting to take public transport because of "gross poor people" is one of the reasons I gave up giving someone adjacent to the family any of my attention. Leaving aside the fact investment bankers with expensive watches share my train to Wynyard each morning, what a horrible thing to think about your fellow human beings. Public transport is wonderful precisely because it helps everyone. And frankly, my experiences with them have been near universally nicer than entitled arses who equate morals with money... and then show themselves as having none of the former).*

Cost of living pressures, sustainable environmental impact, responsible use of public finances, these all seem like things we could improve with better urban spaces and public transport. But while I'm relived that these aren't as minority a set of views as I originally thought, the Anglosphere still has people who can't see beyond suburban brick and bitumen.

Sydney bills itself as a city of villages, which I didn't understand until I started living here. The mixed-use neighbourhood I live in here is regularly cited as among the most liveable and pedestrian friendly in the country, to the point where international YouTubers like RM Transit even call it out specifically. But then you have expanding suburban wastelands and car sewers that would make even an American urban planner blush with excitement.
