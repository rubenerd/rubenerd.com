---
title: "Reconciled down to the cent again"
date: "2023-08-23T08:11:18+10:00"
abstract: "I stored a currency as a number with three decimal points. Whoops."
year: "2023"
category: Software
tag:
- budgeting
- numbers
- spreadsheets
location: Sydney
---
At the start of the month I talked about being off by a couple of cents in Clara's and my envelope budget spreadsheet:

[Reconciling down to the cent](https://rubenerd.com/reconciling-down-to-the-cent/)

I couldn't for the life of me figure out where these 2 errant cents went, so for the first time ever I had to pull from the *barrier* envelope to make the numbers match. As our accountant at work said, I was valuing my time more than 2c to track it down.

*But I couldn't leave it at that*. I poured over my transactions with a fine tooth comb over coffee this morning, and unearthed the **ridiculous** problem.

What do *you* think it was? Do we have any actual accountants in the audience today?

If you guessed I was storing currency as a floating point, much to the chagrin of computer science professors everywhere, you're getting warmer. It's actually something I would have learned in late primary school.

In the **Debits** column, I had inexplicably had two $4.25 transactions stored as 4.245. LibreOffice only showed two significant figures after the decimal point for these accounts, because they're all that would have made sense. These were rounded up to the nearest cent when displayed, but *not* when calculated. Thus, the total was off by two cents, despite showing as being the correct numbers.

I've added a new data validation rule to these columns to only accept two decimal places to prevent me doing something silly like this again.

I'll admit, is a phrase with two words. It baffles me why I would have typed those numbers out like that. But I'm just happy to be fully reconciled again! I know my time is worth more than 2c, *butttt...* the satisfaction I got from this was worth far more.
