---
title: "I miss 4:3 and 16:10"
date: "2023-10-30T07:49:05+10:00"
abstract: "1920×1080 wasn’t an upgrade."
year: "2023"
category: Hardware
tag:
- displays
- ergonomics
- monitors
location: Sydney
---
I thought this article on Wikipedia about [graphic display resolution](https://en.wikipedia.org/wiki/Graphics_display_resolution) was interesting. Not for the text itself, but for how it was presented. Emphasis added:

> The 16:10 ratio allowed some compromise between showing **older 4:3** aspect ratio broadcast TV shows, but also allowing better viewing of widescreen movies. However, around the year 2005, home entertainment displays (i.e., TV sets) gradually **moved from 16:10 to the 16:9** aspect ratio, **for further improvement** of viewing widescreen movies. By about 2007, virtually all mass-market entertainment displays were 16:9 ... 1920×1080 was the favored resolution in the most heavily marketed entertainment market displays.

Notice how the text goes from televisions at 4:3 aspect ratio, to monitors at 16:10, and then to 16:9. This progression, along with the words *further improvement* suggest that each step built upon the previous one.

But these numbers are hiding something: the *resolution* of the displays at those ratios. A common 16:9 resolution was 1920×1200. 16:10 was a truncation of this to 1920×1080, not an improvement. Microsoft pundits pulled this swifty [with the Zune](https://rubenerd.com/thurrott-zunehd/ "Taking Paul Thurrott to task on the Zune HD") back in the day, but we knew better.

As the article states, 1920×1080 was popular for entertainment, but it's lousy for documents. This harmonisation was done as a business measure to cut costs, not to improve ergonomics. We've been stuck with this decision ever since; even Retina-grade or 2x HiDPI displays fitted to computers that aren't stuck in 2009 mostly have this ratio.

16:10 was amazing. You could have two full sized A4 documents in view at a comfortable size, and the added vertical space meant your eyes spent less time darting from side-to-side. 4:3 was great for writing code and prose without distractions. 16:9, sadly, is neither.
