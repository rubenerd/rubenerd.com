---
title: "Finding myself in a video rendering studio"
date: "2023-08-25T17:39:23+10:00"
abstract: "They’re using GPUs to put smiles on faces, not generate bullshit!"
year: "2023"
category: Hardware
tag:
- ai
- blockchain
- design
- graphics
- work
location: Sydney
---
Today was fun. Having set up shop in the office, my manager informed me that we were to have a meeting at a rendering studio. They were in need of burst capacity, and we'd delivered compute in this space before for clients in the US.

Their office was *spectacular*. You know that cliché image of a creative space with unpainted concrete walls, polished wooden floors, plants, and art hanging everywhere? It was even more than that.

It was funny sitting at a desk full of creative professionals as IT people. It was a completely different energy to what I'm used to, though I also felt like a square. At least I had my brightly coloured and mismatched socks :).

It's also so much fun talking with people who are using GPUs not for bullshit, but to... *render things!* As in, making movies! The rest of the industry is slurping up entropy and electricity to generate rubbish and chain together tulip blocks, but these people actually put smiles on faces.

I hope we can do more stuff in this space. That one chat this morning did more to recharge my batteries than anything else has for a while.
