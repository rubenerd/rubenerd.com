---
title: "Using config files for SSH"
date: "2023-06-01T11:04:24+10:00"
abstract: "They reduce the chance for error, help you to remember details, and can be portable across machines."
year: "2023"
category: Software
tag:
- security
- ssh
- things-you-already-know-unless-you-dont
location: Sydney
---
In today's installment of *things you already know, unless you don't*, I'm talking about config files for SSH. I did a video call with a client recently who didn't know these existed, and it made his day.

[View all posts in this series](https://rubenerd.com/tag/things-you-already-know-unless-you-dont/)

Rather than remembering which usernames, key files, ports, and hostnames you need for each machine, you can stash them in an SSH config file. Remote orchestration tools like Ansible can also use hosts defined in this, making managing fleets of servers infinitely easier.

For example, let's pretend I'm connecting to a server with this line. I'm connecting on port 3222, and I'm using a specific SSH key:

    $ ssh -p 3222 -i ~/.ssh/key chitanda@server.tld

We can create an `~/.ssh/config` file to retain this information, with appropriate permissions to hide it from other users:

    $ touch ~/.ssh/config
    $ chmod 600 ~/.ssh/config

Then add a server config to this file:

    Host server.tld
        User chitanda
        Port 3222
        IdentityFile ~/.ssh/key
        [other settings]

Now you can SSH in with just the hostname. I tend to enable verbosity on first connection to make sure I've configured everything correctly:

    $ ssh -vvv server.tld

This takes fewer keystrokes, reduces the chance for error, helps you to remember details, leaks less information in things like command histories, and is even portable across machines.

I originally wrote that as *potable*, but even as a byte stream you wouldn't want to drink it. It'd probably be tedious unwrapping each packet to drink anyway.

