---
title: "Equinix retiring copper"
date: "2023-04-01T08:44:57+11:00"
abstract: "I do love the idea of ordering coax in 2023 though."
year: "2023"
category: Hardware
tag:
- data-centres
- networking
location: Sydney
---
[Via their newsletter](https://equinixcc.us.newsweaver.com/1rpgd458xq/1c6ml3hm5rrt0ukz7qdmj1):

> Effective 31 March 2024, all new Cross Connects globally will be Single-Mode Fiber (SMF). Copper (including POTS, UTP, COAX, Cat5, Cat6 media types) and Multi-Mode Fiber (MMF) will no longer be available as media types for new Cross Connects in any IBX data center.

I love the idea of ordering coax in 2023! My first home and school Ethernet networks were 10BASE2. If I had my own pair of racks in disparate halls, and more money than common sense, I'd order a few coax connections just for the heck of it.

As an aside, Dave Tweed provided [this interesting answer](https://electronics.stackexchange.com/a/80223) about why coax was used for these networks in the first place:

> Coax was used for its controlled impedance, its bandwidth and its self-shielding properties.
>
> Sure, twisted-pair wiring has existed for a very long time, mostly used to carry audio frequencies in telephone wiring. That isn't where the technical advancement was required. In order to compensate for twisted-pair's lossiness and impedance issues, major technological improvements in the electronics used to interface to it (such as high-speed adaptive equalizers) were required in order to make it more cost-effective than coax.
