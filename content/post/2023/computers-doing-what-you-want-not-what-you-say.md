---
title: "Software doing what you want, not what you say"
date: "2023-11-17T09:42:29+11:00"
abstract: "Contrasting the KDE Plasma launcher, and Python3."
year: "2023"
category: Software
tag:
- interfaces
- ui
location: Sydney
---
Ever hear the axiom that computers will do what you say, not what you want? Anyone living in the physical world of silicon, storage, and networks knows even that's questionable, but we get the sentiment.

My favourite software features are those that subvert this idea. Take a look at this KDE Plasma launcher, and note what I attempted to do:

<figure><p><img src="https://rubenerd.com/files/2023/plasma-launcher.png" alt="KDE Plasma launcher menu, showing Konsole and QTerminal in a set of search results. I've typed 'ter' in the search box." style="width:500px; height:250px;" /></p></figure>

I pressed the **Meta/Windows** key, typed **ter**, pressed **Return**, and got the exact application I was after. KDE *knew* what I meant despite technically offering the incorrect name for Konsole. Neat!

Contrast this approach with Python's interactive shell:

	$ Python 3.8.18 (default, Oct  5 2023, 06:38:14)
	[...]
	>>> exit
	Use exit() or Ctrl-D (i.e. EOF) to exit
	>>> quit
	Use quit() or Ctrl-D (i.e. EOF) to exit
	>>> 

Python *knew* what I meant, as evidenced by its syntax suggestions. But it didn't act upon it, unlike the KDE launcher.

I'll admit this isn't apples to apples. Scripting languages have to be prescriptive. The KDE launcher also isn't doing anything fancy; it's matching on metadata associated with the launcher. But little affordances like this add up, and make computers nicer to use.
