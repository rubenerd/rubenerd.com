---
title: "Larger parking spaces proposed in Australia"
date: "2023-10-16T09:00:36+11:00"
abstract: "Massive cars should be ridiculed, not accommodated!"
year: "2023"
category: Thoughts
tag:
- australia
- cars
- transport
location: Sydney
---
[Elias Visontay and Josh Nicholas reported](https://www.theguardian.com/australia-news/2023/oct/15/australia-may-increase-standard-car-parking-spaces-as-huge-vehicles-dominate-the-streets "Australia may increase standard car parking spaces as huge vehicles dominate the streets")\:

> For the past few decades, the standard size for car spaces on streets and in parking lots has been 5.4 metres long and 2.4 to 2.6 metres wide [...] Standards Australia has proposed increasing the required length in off-street lots by 20cm.
>
> Today, Australia’s most popular car is the Toyota HiLux dual cab ute, which is about 5.27 metres long – giving it less than 15 cm of breathing space in average parking spots. Some vans and SUVs are even longer – approaching 6 metres.

What a waste of metal, fuel, and space. These massive cars should be ridiculed, not accommodated!

Well, except vans. There'd be more space for them if these compensation devices weren't around. I routinely see hatchbacks carry more people and cargo than these contraptions; probably because they're not cosplaying as tradies. You know what my electrician friend drives? *A Yaris!*

The saving grace, if there is one, is that the HiLux would still be considered small compared to [imported American light trucks](https://rubenerd.com/ugly-american-trucks/ "Ugly American trucks"). But it's a *very* slippery slope for normalising these pedestrian killers.
