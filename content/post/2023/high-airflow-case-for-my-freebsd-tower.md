---
title: "High-airflow case for my FreeBSD workstation"
date: "2023-10-26T08:22:16+11:00"
abstract: "The Fractal Ridge case is beautiful, but doesn’t save space for all the poor venillation it offers."
thumb: "https://rubenerd.com/files/2023/torrent-case@1x.jpg"
year: "2023"
category: Hardware
tag:
- bsd
- computer-cases
- freebsd
- homelab
location: Sydney
---
I love my FreeBSD workstation. It's a lowly ASRock business board and [Ryzen 7 5700X](https://www.amd.com/en/products/cpu/amd-ryzen-7-5700x) from the previous generation with DDR4, but it's my favourite computer to use. It even dual boots into the Fedora KDE Spin to play Minecraft and Steam games.

There's just one problem, and unfortunately it comes down to the case.

<figure><p><img src="https://rubenerd.com/files/2023/fractal-ridge-desk@1x.jpg" alt="Photo on my desk of the Fractal Ridge." srcset="https://rubenerd.com/files/2023/fractal-ridge-desk@2x.jpg 2x" style="width:500px; height:333px;" /></p></figure>


### The Fractal Ridge

The above photo shows the *Fractal Ridge*, the latest generation of what I've seen dubbed the *console* form factor. I built this machine to save desk space, but I soon realised its slender size is deceiving. The machine draws in air from the left and expels heat from the right, so it needs sufficient clearance. This means it functionally takes up the same desk space as my previous case, while offering less physical cooling mass and capacity.

The motherboard/CPU chamber for this case gets *hot.*

Despite getting the best Noctua cooler and thermal paste I could, the CPU cooler can only soak so much heat in such a tiny space. I recently set the CPU on permanent eco mode which dropped temperatures slightly, but even this lowly 65 W TDP component generates enough heat under load that the case is warm to the touch. I couldn't imagine squeezing a game-focused X3D AMD part into here, or some Intel silicon.

The massive upper cooling chamber and fans for the GPU demonstrates where the design priorities lay for this case. To that end, my hungry RTX 3070 remains cool even when playing Minecraft with shaders, or rendering a video in Resolve, or plagiarising some artists using Stable Diffusion (I kid). Even if the CPU chamber got the same treatment though, it would still direct hot air to the side, which isn't ideal considering my Apple IIe sits there.


### Saucing (mmm) a replacement

All this is to say I'm in the market for a new case. I've been a MiniITX user for years, but I'm also considering expanding slightly to MicroATX for my next eventual upgrade. As I get older, I'm realising things like heat and noise are more important to me than raw performance; though the latter would be helped by a system that can handle the former. And while MiniITX is impressive, it's a bit of a pain to work with.

It's that classic marketing triangle of *small*, *quiet*, and *cool*. I'd like to move from a case optimised for the first point, to one optimised for the last two.

So far I'm most interested in the Fractal Torrent, which reviewers have praised for having the best airflow of any modern computer case. The large air intake means the fans spin at a lower speed for equivilent air displacement, and helpfully directs air away from the desk, rather than into it. It also looks like you can get them with plain panels instead of glass.

<figure><p><img src="https://rubenerd.com/files/2023/torrent-case@1x.jpg" alt="Press photo of the Fractal Torrent" srcset="https://rubenerd.com/files/2023/torrent-case@2x.jpg 2x" style="width:500px;" /></p></figure>

Thermodynamics are fun! Though I wonder if the case would run even cooler if I spray-painted the panels beige to match the retrocomputers around it? It's such a ghastly idea that I'm half-tempted to do it.

