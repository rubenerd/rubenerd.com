---
title: "BNC connectors cut in half"
date: "2023-07-12T21:04:12+10:00"
abstract: "Who knew they were so intricate"
thumb: "https://rubenerd.com/files/2023/bnc-inside@1x.jpg"
year: "2023"
category: Hardware
tag:
- cables
- networking
location: Sydney
---
I was reading the Wikipedia article on BNC connectors this afternoon, like a gentleman, when I was struck by this picture. Well, not literally, unless I printed the photo and hit myself on the head.

Who knew they were so intricate?

<figure><p><img src="https://rubenerd.com/files/2023/bnc-inside@1x.jpg" alt=" Cross sections of BNC and HDBNC connectors. Both are 50 ohm." style="width:500px;" srcset="https://rubenerd.com/files/2023/bnc-inside@2x.jpg 2x" /></p></figure>

[Wikipedia: BNC connectors](https://en.wikipedia.org/wiki/BNC_connector)   
[Photo by TubeTimeUS on Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Cross_sections_of_BNC_and_HDBNC_connectors.png)
