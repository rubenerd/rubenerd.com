---
title: "AI, music, and valuing art"
date: "2023-01-13T09:19:43+11:00"
abstract: "Even the most cynical of free market, survival-of-the-fittest stans go home and watch shows."
year: "2023"
category: Media
tag:
- art
- economics
location: Sydney
---
I haven't so much spilled electronic ink on this topic, as much as I've come to routinely soaking everyone within a five hundred kilometre range with my ink hose. I apologise if I've stained your clothes, but this is the kind of thing that preoccupies my mind and keeps me up at night.

Music, and specifically online distribution of music, is such a useful lens through which we can understand online economics and attitudes. Where musicians and artists are lead, or should I say pushed, is often indicative of where the industry as a whole is trending, for better or worse.

Streaming services are a perfect example. They've been credited for "solving" music piracy, though only if you have sufficient volume. My brother in law talked about the pittance his band received in funds from sites like Spotify despite tens of thousands of plays. It's not surprising; when you turn a creative endeavour into a commodity and slap it behind a flat fee, something has to give. It can't be taken from the profits of media labels, or the tech company that needs to remain afloat to keep the system operating... so artists it is.

This is a recurring theme in tech, and we're now seeing it with writing and visual arts. We build these systems to disseminate and price people's material, without giving much (if any) thought to the people who create it in the first place.

Having weathered the procedurally-generated muck of NFTs, we're now doing this with a new generation of AI that's not so much dystopian as it is painfully mediocre. The fact there's so much hype, and there are so many people enamoured with it, demonstrates how hollow and immature our industry can be.

People like myself keep the proverbial electronic wheels turning on the engineering side of the fence, but artists make life worth living. Even the most cynical of free market, survival-of-the-fittest stans who claim tech doesn't owe anyone a livelihood go home and watch shows, play games, read books, and listen to music. These things don't magic themselves into existence, and we make our culture all the poorer by pretending so.

I don't want to lose art, but right now tech is treating it as a disposable, cheap, procedurally-generated token; shared on a pointless blockchain and used to train an AI. What message does that send to artists? Are we okay with that? I'm not.
