---
title: "Driverless cars"
date: "2023-06-29T08:58:52+10:00"
abstract: "A report in the Los Angeles Times proves they’re not fit for purpose."
year: "2023"
category: Thoughts
tag:
- ai
- ethics
- driverless-cars
- safety
location: Sydney
---
Los Angeles Times staff writer Russ Mitchell filed a worrying report about the state of driverless cars in his state, and how badly they've been handing specific safety situations. I knew driverless cars had issues, but strap in for this.

[San Francisco’s fire chief is fed up with robotaxis that mess with her firetrucks. And L.A. is next](https://www.latimes.com/business/story/2023-06-22/san-francisco-robotaxis-interfere-with-firetrucks-los-angeles-is-next)

The article highlights five incidents:

> Running through yellow emergency tape and ignoring warning signs to enter a street strewn with storm-damaged electrical wires, then driving past emergency vehicles with some of those wires snarled around rooftop lidar sensors.
>
> Twice blocking firehouse driveways, requiring another firehouse to dispatch an ambulance to a medical emergency.
> 
> Sitting motionless on a one-way street and forcing a firetruck to back up and take another route to a blazing building.
> 
> Pulling up behind a firetruck that was flashing its emergency lights and parking there, interfering with firefighters unloading ladders.
> 
> Entering an active fire scene, then parking with one of its tires on top of a fire hose.

Granted, human drivers can also be reckless, irresponsible, and life-threatening. But that's not an argument for driverless cars; fast metal boxes in dense urban areas will always be dangerous, regardless of whether the operator is a human or a computer program not fit for purpose.

The only real solution is a reduction in car dependency. This can only happen with a concerted and mature approach to public transport, and changing roads to properly accommodate pedestrians and cyclists. Anything else is window dressing... assuming the car ploughed into a house with curtains.

