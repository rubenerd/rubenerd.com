---
title: "Derek Sivers on OpenBSD"
date: "2023-02-10T09:14:42+11:00"
abstract: "Discussing consistency, knowing what is running, and the beauty of doing the minimum necessary."
year: "2023"
category: Software
tag:
- bsd
- derek-sivers
- openbsd
location: Sydney
---
Last Tuesday I wrote that it was worth considering a [FreeBSD or NetBSD desktop](https://rubenerd.com/its-worth-running-a-freebsd-or-netbsd-desktop/) if you're at all technical. I glossed over many of the engineering benefits for once, in favour of something more touchy feely:

> It was fun to tinker and try something new, especially an OS with that history and lineage going back to the original AT&T UNIX. And now I have something that works great for me. There’s this pervasive attitude in IT and hustle culture nonsense that you should have to justify and monetise everything you do. Sometimes you need to give yourself permission to tinker.

Based on the email I've had, this was received poorly. Which sucks; even if you disagree with the merits of BSD, I was hoping the takeaway would be about fostering curiosity and fun with something you haven't tried before.

Anyway, I thought it was worth expanding on. [Derek Sivers has a concise post](https://sive.rs/openbsd) about why *he* uses OpenBSD, much of which also applies to my preferred BSD platforms. These two points are worth underscoring:

> But if you’re experienced, like to “look under the hood”, and prefer software that does the minimum necessary, OpenBSD is for you. 
> 
> The whole system is carefully planned and consistent, instead of a hodge-podge of bits and pieces. 

This attitude is reflected everywhere, from documentation to hardware support. This is also what I've meant when saying NetBSD is beautiful.

Every now and then I open **top(1)** and **ps(1) aux** on my Mac and Fedora machines, and balk at the sheer volume of background processes. Resource use is desirable if it can be accounted for, is transparent, serves a functional purpose, and if I require it. Otherwise it's bloat, like that feeling when I eat too much Korean cabbage salad.

Running a BSD desktop takes more manual work at the outset, but I feel in complete control. My work MacBook Air is light, fast, and runs the specific proprietary software I need, but I'm but a guest within its plush but [increasingly broken](https://rubenerd.com/upgrading-to-macos-ventura-was-a-mistake/) confines.

*(I also wonder if that's why I'm so drawn to retrocomputing as well. There's no hiding what these legacy machines are doing, even at a fundamental electronics level).*
