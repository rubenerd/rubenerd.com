---
title: "Exploring an incredible Kyoto Hard Off store"
date: "2023-04-26T09:20:43+09:00"
abstract: "This chain of second-hand stores has everything."
thumb: "https://rubenerd.com/files/2023/hardoff-inside@1x.jpg"
year: "2023"
category: Travel
tag:
- hankyu
- hi-fi
- music
- japan
- japan2023
- retrocomputing
location: Kyoto
---
*Hard Off* is a chain of second-hand stores in Japan that sell computers, retro consoles, Hi-Fi gear, camera lenses, scientific equipment, electric guitars, toasters, and everything in between. Their logo suggests they're related to the *Book Off* chain of second-hand book stores.

I'd never been to one in Japan before, but Clara and I tracked down a branch on the outskirts of Kyoto one evening. It was a bit of a trek; the sheer size of the warehouse presumably makes it difficult to afford rent or floor space in the centre of town. Fortunately, there was a Hankyu line station within a fifteen minute walk.

<figure><p><img src="https://rubenerd.com/files/2023/hardoff-inside@1x.jpg" alt="Photo looking to the side of the store, with rows of shelves and the high ceiling of the warehouse." style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2023/hardoff-inside@2x.jpg 2x" /></p></figure>

When you enter the store, you're greeted with row after row of shelves holding almost every electronic device you can imagine. Save for a bit of dust, all of them are clean and meticulously labelled with their condition, price, and status of testing. This even goes for individual disks, which we'll get to in a moment!

A "junk" section is located to the back of the store which, despite its connotations in English, merely indicates the devices haven't been fully tested. The term should be familiar to those who've used Japanese auction proxy mailing services. It was helpful having Clara there to be able to interpret some of the labels, though they're also clear enough to take photos with your phone and do a translation for "powers on".

My interests were mostly with Hi-Fi and computers, so this is where I spent the bulk of my time. There were so many drawers of old components, but my eyes were drawn to the piles of Magneto Optical (MO) disks! My Japanese friends in Singapore had MO drives, but I only ever used Iomega devices. There was a handsome Sony cart in teal for ¥200, which was only a bit more than I spent on the coffee vending machine outside. I couldn't resist getting it, alongside a Betamax cassette, because of course I was going to buy a Betamax video cassette for no good reason whatsoever.

<figure><p><img src="https://rubenerd.com/files/2023/hardoff-hifi@1x.jpg" alt="Photo showing a shelf of Hi-Fi components and labels" style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2023/hardoff-hifi@2x.jpg 2x" /></p></figure>

There were so many amplifiers, tuners, receivers, Laserdisc players, CD changers, and tape decks that some were even stacked *vertically* on the shelves! It felt like a historical exhibit, though much of this stuff would have seemed new and modern when I was a kid in the 1990s. I was *sorely* tempted by a gorgeous 1980s Technics tape deck that boasted Type III (yes, type III!) cassette support and Dolby C.

The challenge for most tourists buying Japanese goods are the incompatible voltages. Japan's split electrical system means most of their goods can operate at both 50 and 60 Hz, the former of which works for those of us in most of the world. What doens't work is the North American-style voltages, which would require a step-down transformer for use on 230 V. I suppose one could replace the power supply if you were sufficiently adept, but that goes beyond my limited electronics abilities.

I did end up getting a piece of Hi-Fi gear that was shockingly cheap, and bypassed any voltage issues, but that's for another post! I also picked up an IDE CD-ROM, some USB headers, a Sailor Moon music CD, a rounded IDE ribbon cable, and a 60 minute type IV metal cassette in its original Sony shrink wrap, again for shockingly little.

<figure><p><img src="https://rubenerd.com/files/2023/hardoff-shelves@1x.jpg" alt="Photo showing one of the many glass cabinets inside the store, this one stocked with camera gear. More Hi-Fi equipment is to the side." style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2023/hardoff-shelves@2x.jpg 2x" /></p></figure>

Best of all though were the staff. Service staff in Japan always wrap things with such precision and care, but the gentleman serving us here took it a step further with his attention to detail with bubble wrap and tape. This small Hi-Fi component was packed better than any new laptop I've bought in the last decade.

If you're in Japan and are at all interested in retro tech stuff, you need to find a *Hard Off* branch. They can be a bit of a trek to get to, but they're well worth it. Just make sure your luggage has enough space.
