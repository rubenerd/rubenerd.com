---
title: "We noticed a new login to your …!"
date: "2023-08-05T09:46:09+10:00"
abstract: "These new messages are worse than useless for security."
year: "2023"
category: Internet
tag:
- authentication
- email
- security
location: Sydney
---
It's amazing (and a tiny bit scary!) how much authentication and identity we still bootstrap off email. As one of the last open, standardised, distributed protocols still in widespread use, it probably should come as no surprise.

It now means much of our email is ephemeral flotsam related to this secondary purpose. Think time-limited two-factor authentication tokens, or sites that email you a one-time login link. It's all a bit of a mess, and further proof that we desperately need a widely-supported alternative for authentication. *But I digress!*

My new favourite is the *we noticed a new login to X* email. These were originally designed to alert people to anonymous account activity, such as someone logging in from a different physical location from where you reside, or at unusual times of day.

Except, some services have now taken it upon themselves to send these emails as a matter of routine. Every time I log into Google, or Todoist, or a handful of other sides, I'm emailed something along the lines of *we noticed you logged in from... where you always do!*

These are worse than useless, because they bury actionable, important information in spam. If you receive these emails all the time, they're less likely to surface something important, and you're less likely to notice. Or worse, you'll ignore or delete the messages on purpose out of irritation. 

Alert fatigue is a well-understood psychological phenomena where we discount, tune out, or ignore information when we're overwhelmed by it, thereby defeating the entire point of the alerts. I'd say IT security professionals forget the human element at their peril, though it's the rest of us that pay the price.

Granted, only sending important login alerts for a specific person is trickier than it sounds. But authentication is too important to half-arse. One's entire arse should be invoked for such efforts. I dub it the *Posterior Principle*.
