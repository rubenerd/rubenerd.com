---
title: "Atelier Games on special on Steam"
date: "2023-02-23T09:55:36+11:00"
abstract: "It’s not an ad, it's a recommendation! "
thumb: "https://rubenerd.com/files/2023/ryza-screenshot@1x.jpg"
year: "2023"
category: Anime
tag:
- atelier
- atelier-ryza
- atelier-sophie
location: Sydney
---
You can [buy games in the Atelier franchise](https://store.steampowered.com/developer/KOEITECMO/sale/atelier2023) for ~35% off on Steam until the end of the month. It's not an ad, it's a recommendation! Right?

I only got into the Atelier games in the last few years, but they're so comfy and fun. The alchemy/crafting mechanics are approachable and interesting, the art is beautiful, and the voice acting is top notch. Unlike previous Atelier titles that had time limits, this slower gamer appreciates that you can take the modern ones at your own pace. They also reward you for exploring, which admittedly is something I tend to do more in these sorts of games.

<figure><p><img src="https://rubenerd.com/files/2023/ryza-screenshot@1x.jpg" srcset="https://rubenerd.com/files/2023/ryza-screenshot@2x.jpg 2x" style="width:500px;" alt="Key visual from Atelier Ryza: Ever Darkness and the Secret Hideout" /><br /><img src="https://rubenerd.com/files/2023/sophie-screenshot@1x.jpg" srcset="https://rubenerd.com/files/2023/sophie-screenshot@2x.jpg 2x" style="width:500px;" alt="Key visual from Atelier Sophie 2: The Alchemist of the Mysterious Dream" /></p></figure>

[Ryza](https://store.steampowered.com/app/1121560/Atelier_Ryza_Ever_Darkness__the_Secret_Hideout/) was my gateway (above), but I think [Sophie](https://store.steampowered.com/app/1621310/Atelier_Sophie_2_The_Alchemist_of_the_Mysterious_Dream/) might be my favourite. I didn't realise that her [first title](https://store.steampowered.com/app/1502970/Atelier_Sophie_The_Alchemist_of_the_Mysterious_Book_DX/) got a re-release a couple of years ago. Clara insists think she's "not cute enough" to cosplay from it, but that's complete bunk.

I was so cut that the official Koei Tecmo store in Tōkyō were sold out of most of their Atelier stuff when we went there last year. Clearly a revisit is in order. Can someone brew me a potion that can grant me temporary teleportation, or at least share the crafting recipe?
