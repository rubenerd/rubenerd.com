---
title: "In Sydney? Want a free Compaq retrocomputer?"
date: "2023-12-03T10:57:38+11:00"
abstract: "I’ve decided to give away one of my spare towers. If you’re willing to pick up, it's yours!"
thumb: "https://rubenerd.com/files/2023/presario-5070-sale-front@1x.jpg"
year: "2023"
category: Hardware
tag:
- compaq
- retrocomputing
- sale
location: Sydney
---
I'm doing something a bit different today. I was going to list my spare Compaq Presario 5070 tower from 1999 on eBay or Gumtree, but I'd rather just give it to one of you who'd appreciate it.

Anyone in Sydney want it?

<figure><p><img src="https://rubenerd.com/files/2023/presario-5070-sale-front@1x.jpg" alt="Photo showing the front of the machine." style="width:500px; height:375px;" srcset="https://rubenerd.com/files/2023/presario-5070-sale-front@2x.jpg 2x" /></p></figure>


### Specs from the factory

* AMD K6-II 350MHz
* SiS 530 chipset
* Onboard SiS AGP 2x VGA
* Onboard ESS AudioDrive Solo
* 3.5-inch floppy drive
* Game port, Centronics/parallel, RS232/serial, USB 1.1


### Parts I added

* 64 MiB SDRAM; original DIMMs had severe MemTest errors
* 512 MiB disk-on-module; small but very fast
* New-old stock LG DVD-RW; works in DOS
* An SMC 10BaseT NIC; needs drivers for W98
* 56k fax modem; because I have oodles of them

<figure><p><img src="https://rubenerd.com/files/2023/presario-5070-sale-bios@1x.jpg" alt="Photo showing the BIOS screen." style="width:500px; height:375px;" srcset="https://rubenerd.com/files/2023/presario-5070-sale-bios@2x.jpg 2x" /></p></figure>


### Condition

* Boots into Windows 98 SE, and previously ran NetBSD. The DOM is small, but so much faster than the failing drive it had before. You might want to use XT-IDE and add a larger drive.

* There's very little dust in the PSU, suggesting it wasn't used much.

* The plastic bezel is in great cosmetic condition, though the drive door panel doesn't latch. This could be an easy fix for someone who knows what they're doing. It says "5060" because the original "5070" bezel was in bad shape.

* The chassis has a few gashes in the back, which I've put tape over.

* The case itself has a dent in the top-front corner, which you may be able to hammer out. I live in an apartment with thin walls, so I haven't attempted.

* All the parts came from smoke-free homes, something I've come to appreciate after a few recent negative experiences.

<figure><p><img src="https://rubenerd.com/files/2023/presario-5070-sale-back@1x.jpg" alt="Photo showing the back of the machine." style="width:420px; height:560px;" srcset="https://rubenerd.com/files/2023/presario-5070-sale-back@2x.jpg 2x" /></p></figure>


### What's the deal?

Last month I got a second Presario tower I was going to use for parts, but then by fluke another that got lost in transit suddenly appeared. From these *three* towers (two 5060s and a 5070), I was able to put together two that work flawlessly.

I only have space for one of them (yay, Sydney housing market), so I'd rather give this to someone who'd appreciate it. Comes with just the tower; the peripherals were only there to demonstrate it was functional.


### How do I get it?

If you're interested, [send me an email](https://rubenerd.com/about/#contact) or [message on Mastodon](https://bsd.network/@Rubenerd). If you're willing to meet at Chatswood Station in Sydney, it's yours! You'd be doing me a huge favour.

If I don't get a reply by the end of December 2023, I'll list on eBay.
