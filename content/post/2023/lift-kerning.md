---
title: "Floor indicator kerning, and other things"
date: "2023-10-25T12:08:16+11:00"
abstract: "These two indicators are completely different, and it bothers me."
thumb: "https://rubenerd.com/files/2023/lift-1@1x.jpg"
year: "2023"
category: Travel
tag:
- accessibility
- lifts
- oleds
location: Sydney
---
I was gravely concerned by something I witnessed in an Australian lift lobby I visted recently, and felt compelled to share it here.

This was the floor indicator for the lift on the left:

<figure><p><img src="https://rubenerd.com/files/2023/lift-1@1x.jpg" srcset="https://rubenerd.com/files/2023/lift-1@2x.jpg" alt="Photo of a low-resolution OLED screen displaying P1" /></p></figure>

And here's the lift on the right:

<figure><p><img src="https://rubenerd.com/files/2023/lift-2@1x.jpg" srcset="https://rubenerd.com/files/2023/lift-2@2x.jpg" alt="Photo of another low-resolution OLED screen displaing P 1 with a noticable gap, and dimmer light." /></p></figure>

P1 is such a ridiculous number for a floor, but I digress. Notice anything different? Besides the fact I clearly didn't crop them the same, despite my best efforts? Feel free to scroll up and consult the first image again.

I could count no fewer than four problems:

* Indicator one has a noticably brighter and more consistent display matrix; the difference was even more stark in person than the photos suggest. Indicator two has a weird bright band across eighth row

* It's not apparent in the photos, but indicator two flickered violently like a modern OLED phone. This was most noticable when the display was updated with a new number.

* Indicator one has tight kerning between the P and 1 characters. Indicator two spaces the characters far apart for some reason.

* Indicator one has a sharp corner on the P. Indicator two is either missing an OLED bulb, or the bulb is blown, or the chararacter encoding is wrong.

I noted indicator two's bezel didn't have any paint residue, indicating (HAH!) to me that it had either been pried off or replaced after indicator one. This suggests an inspection or maintenance was performed on it to diagnose or fix a fault. This maintenance was either unsuccessful, incomplete, or introduced additional bugs.

I'm not sure to whom I should refer my concerns; perhaps the building strata? I'm sure the people in this building would appreciate it, and might even notice.
