---
title: "Adrian Black explains his TRS-80 diagnostic"
date: "2023-02-01T08:23:10+11:00"
abstract: "He even walks through some Z80 assembler! So much fun."
thumb: "https://rubenerd.com/files/2023/yt-Hh8dRgtu1Jk@1x.jpg"
year: "2023"
category: Hardware
tag:
- adrians-digital-basement
- retrocomputing
- trs-80
- videos
- youtube
location: Sydney
---
I've namedropped *[Adrian's Digital Basement]()* here a few times in the context of vintage Commodore computers, but Adrian also dabbles in other vintage machines too. And this third attempt at fixing a TRS-80 might be my new favourite video of his.

Adrian had been frustrated with the opaque errors he was getting trying to boot a TRS-80 model III, so he reached out to Frank from [IZ8DWF](https://www.youtube.com/user/iz8dwf) and David KI3V to help co-author a diagnostic ROM from scratch.

<figure><p><a target=_BLANK href="https://www.youtube.com/watch?v=Hh8dRgtu1Jk" title="Play I helped make a diagnostic ROM to help me fix the broken TRS-80 Model III!"><img src="https://rubenerd.com/files/2023/yt-Hh8dRgtu1Jk@1x.jpg" srcset="https://rubenerd.com/files/2023/yt-Hh8dRgtu1Jk@1x.jpg 1x, https://rubenerd.com/files/2023/yt-Hh8dRgtu1Jk@2x.jpg 2x" alt="Play I helped make a diagnostic ROM to help me fix the broken TRS-80 Model III!" style="width:500px;height:281px;" /></a></p></figure>

It was such a treat seeing him dive into the Z80 assembler to see how it worked, and seeing it running in an emulator so he could pause and explain what it was doing. It's a bit of a crash course in 8-bit assembly, right down basics like stack traces and memory pages. If his day job isn't in education, he should consider it.

I've had it on the backburner for years to learn some 6502 and Z80 assembler. This video was *ultra* cool!
