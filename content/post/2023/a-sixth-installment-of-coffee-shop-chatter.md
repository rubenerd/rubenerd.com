---
title: "A sixth installment of coffee shop chatter"
date: "2023-08-08T07:45:07+10:00"
abstract: "You’re playing Pokemon Go while walking across the street!"
year: "2023"
category: Thoughts
tag:
- coffee-shop
- quotes
location: Sydney
---
[View all past coffee shop posts](https://rubenerd.com/tag/coffee-shops/)

* "You're playing Pokemon Go while walking across the street!"

* "I used to, but I can't afford it anymore. It's bungee-jumping without a tightrope [sic]".

* "I need a large one this morning. Wow, that's pretty big."

* "Where does hot chocolate come from? South America? Wait, how does everything good come from there?"
