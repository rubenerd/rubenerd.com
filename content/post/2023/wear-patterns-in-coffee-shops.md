---
title: "Wear patterns in coffee shops"
date: "2023-07-21T08:22:02+10:00"
abstract: "They expose where the popular tables are!"
year: "2023"
category: Travel
tag:
- coffee
- coffee-shops
- data
- design
- retail
location: Sydney
---
It may shock some of you to know that I spend altogether too much time and money in coffee shops. They're my favourite places in the world to write, code, and think. The vast majority of the words you've read here over the years come from one of these lovely places.

One of the older establishments I frequent in my local suburb has squishy bench seats along the walls, and faux wooden tables. The leather and laminate surfaces have started to degrade slightly, but on some surfaces more than others.

If we assume this uneven wear is the result of differences in use, we can draw some interesting data from it.

The most degraded chair and table combo by far is at the back. It's next to one of the large windows, is relatively secluded, and only has one other table next to it instead of two or three. It happens to be my favourite spot, and based on this pattern, its everyone else's as well.

Another very popular seat is near the counter, perhaps out of convenience. Here, the wear is limited to one side of the table, suggesting people are mostly sitting there facing *into* the shop than the wall. I wrote recently about how I far prefer sitting like this, but maybe this is more common than I thought.

[Fight or flight mode in your computer chair](https://rubenerd.com/fight-or-flight-mode-in-your-computer-chair/)

Among the "mid-tier" of tables, we have a cluster near the other window, which I'd think are less desirable on account of being near the front door with lots of foot traffic. There are the row of bench seats across from my favourite spot that, like the one near the counter, are unevenly worn on the surfaces that face *into* the room. There's also a single table near a support column that might otherwise be more popular, but it's *just* a bit awkward to sit at with more than one person.

The centre of the coffee shop is where you'll see the least wear, suggesting these aren't the popular seats. There's also a window-adjacent table you'd *think* would be popular, until you realise the reverse-cycle air conditioner is directly above. Clara and I sit here when there isn't an another option, but it's why we bring a jumper just in case. Like any good *third place*, this shop attracts a lot of regular locals, who have probably remembered this from a past experience and steer clear.

If I ever did quit my infocomm career and started my Australian branch of a Singaporean-style *kopitiam* cafe, I think some of my research would involve digging into random data like this. There's probably a lot you could learn, even with superficially simple things like table layout.
