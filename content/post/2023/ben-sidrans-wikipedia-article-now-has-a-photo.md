---
title: "Ben Sidran’s Wikipedia article now has a photo"
date: "2023-01-28T19:43:29+11:00"
abstract: "Plus a description on how you can add images and text from other Wikipedia language wikis."
year: "2023"
category: Media
tag:
- ben-sidran
- jazz
- wikipedia
location: Sydney
---
I noticed one of my favourite jazz gentleman had a photo on Wikimedia Commons that was linked to on a bunch of other wikis, but not his English one. I've [now has a photo in his infobox](https://en.wikipedia.org/wiki/Ben_Sidran)! 

If you're not as familiar with editing Wikipedia as you are reading it, you can often find lots of useful information on other language wikis which you can transcribe, provided you have citations. This is especially true for subjects, people, and places that aren't originally from the English-speaking world (though Sidran is).

For images on other wikis, if you click them and you're taken through to a Wikimedia Commons page, you've almost certainly got a permissively-licenced image you can use. Like a jazz guy.
