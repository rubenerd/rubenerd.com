---
title: "Parts reviewers, clickbait, and absolute price"
date: "2023-03-16T17:37:27+11:00"
abstract: "It doesn’t matter if $100 gets you much more, if you don’t have an extra $100."
year: "2023"
category: Hardware
tag:
- clickbait
- engagement
- journalism
- reviews
- youtube
location: Sydney
---
PC Magazine (or was it PC World?) used to summarise their reviews with *Bang for the Buck* charts in the 1990s. Components would be plotted against price and performance, and chosen based on where they fell. Save for any glaring or obvious issues, the one that offered the *most bang* for *the most buck* was recommended.

I remember my mum telling me I shouldn't use those terms too loudly in polite company, for reasons I'd know "when I got older". Whoops!

Two decades on, and everyone from bloggers to YouTubers still follow this for consumer electronics recommendations. It's rational to want as much performance or efficiency per dollar you can get. On the flip side, you'd be reticent to recommend something with 10% better performance for twice the price. The *value*, as they'd say, isn't there.

But this is where clickbait starts to skew the picture. While those magazines of yore did have to lure buyers with catchy covers, videos and bloggers have to fight among thousands of others for engagement and clicks.

And what draws attention? Well, I also brought Shaggy back alongside those magazines, and he tells me it *needs to be bombastic*, to be truly *fantastic*. The Shaggy Marketing Principle, they call it. Mr Lover Lover.

This leads to, what could be charitably described, as *rage reactions*. It's not sufficient to brand a CPU, graphics card, or cooler as not cost effective, or not suited to a specific use case or budget. They have to be **absolutely shocking**, or **a literal waste of silicon**, or **shouldn't exist**. These parts aren't just bad value, their existence is **offensive** to civilisation, up there with the worst mistakes made in this history of humanity. I think I've seen enough thumbnails with literal flames and exaggerated facepalms to last a lifetime. 🔥

Journalistic integrity aside, these attitudes seep into forums and social media, where people who don't pick the blessed parts are piled onto when sharing builds or ideas. This is most pronounced early on in product launches, when the majority of builders can only base their judgement on what a few YouTube reviewers said. It always sucks seeing a young enthusiast sharing their build, only to be pelted by reply guys.

And it doesn't even make sense half the time. As mentioned in the title, missing from most of this analysis is the absolute cost of a device, which depending on the market and your budget can be the difference between affording something and not. It doesn't matter if $100 more gets you twice the performance, <em>if you don’t have $100 more.</em> It also discounts global price variations and supply. To paraphrase Henri Cartier-Bresson about cameras, the best one is the one that's available.

This drive for engagement warps everything, and it has consequences far beyond trolls saying you're a sucker for buying an A770 or a 7900 XT. 
On the plus side, you don't need permission from other people to feel good about yourself, and you're not required to rationalise any of your decisions to random Internet strangers. That's something I wish I'd learned years ago.
