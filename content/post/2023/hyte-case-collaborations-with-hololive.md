---
title: "HYTE case collaborations with Hololive"
date: "2023-11-30T11:20:24+11:00"
abstract: "These cases POP! If only I had space, money, and space."
thumb: "https://rubenerd.com/files/2023/hyte-y40-watson@1x.jpg"
year: "2023"
category: Anime
tag:
- hardware
- hololive
- hololive-en
- hakos-baelz
- ouro-kronii
- watson-amelia
location: Sydney
---
American computer case manufacturer HYTE has been collaborating with various English Hololive vtubers to create themed cases showcasing the talents. They're not simple wraps or stickers either; the cases have custom frame colours and power buttons, and come with some incredible desk mat art and other goodies.

[Kronii](https://www.youtube.com/channel/UCmbs8T6MWqUHP1tIQvSgKrg "Kronii's YouTube channel") and [Bae](https://www.youtube.com/@HakosBaelz "Bae's YouTube channel") were the first to get the treatment in the last year or so, with the HYTE Y60:

<figure><p><img src="https://rubenerd.com/files/2023/hyte-y60-baelz@1x.jpg" alt="The HYTE Y60 with Baelz" style="width:430px;" srcset="https://rubenerd.com/files/2023/hyte-y60-baelz@2x.jpg 2x" /><br /><img src="https://rubenerd.com/files/2023/hyte-y60-kronininini@1x.jpg" alt="The HYTE Y60 with Kronii" style="width:430px;" srcset="https://rubenerd.com/files/2023/hyte-y60-kronininini@2x.jpg 2x" /></p></figure>

I *love* how much these cases pop, despite my usual preference for minimalism. I saw them in Japan earlier this year, and if anything the colours are even more vibrant and detailed in person. I was especially impressed with the quality of the glass prints; they could have gone either way. Kronii is so cool, and Bae is absolutely adorable (bruh)!

The Y60 case itself has won praise from reviewers for its unconventional design and cooling performance (the intakes are in the bottom and side). In a sea of Ford Model Ts and Boy Racers, it's refreshing to see designs in something *other* than black with RGB, and HYTE have used the unusual chamfer to mount displays and other accessories in a way no other case has.

But its unique design is also its Achilles' Heel in a sense. It's what I refer to as an *Inverted Tardis*: a box that manages to be voluminous but low capacity. This comes out in a couple of ways:

* The vertical GPU mount requires the adjacent PCI slots be low-profile. There are NICs, sound cards, port expanders, and RAID controllers that could fit (more on that shortly), so the slots aren't *completely* pointless. Not to get all Tom Nicholas on you, but *nevertheless*, this compromise seems silly for such a wide case.

* The massive volume doesn't afford it many drive mount options. Clara's 12 L NCASE M1 Mini-ITX case has more storage capacity, which is a bit funny.

As cool as they look, these are for showpiece game machines with large cooling systems and graphics cards, not for something like a NAS that would otherwise command such a large chassis. A RAID controller, or even a SATA expander for a JBOD would fit, but its storage devices sure wouldn't. That's fine; they know their market.

Recently HYTE announced their Y40, a scaled-down version of the Y60 that trades the characteristic glass chamfer for a smaller desk footprint and slightly rearranged internals. Reviewers broadly lamented the loss of what made the case unique, and the fact it's no cheaper when you add additional fans to achieve similar cooling performance.

While it retains the low-profile PCI slots, the form factor appeals to me far more. And it's with this case that HYTE announced their latest collaboration, this time with [Amelia Watson](https://www.youtube.com/@WatsonAmelia "Amelia Watson's YouTube channel")!

<figure><p><img src="https://rubenerd.com/files/2023/hyte-y40-watson@1x.jpg" alt="The HYTE Y40 with Amelia WATSON!" style="width:500px; height:500px;" srcset="https://rubenerd.com/files/2023/hyte-y40-watson@2x.jpg 2x" /></p></figure>

Watson was Clara's and my first vtuber, and she still holds a special place with us. I could absolutely see myself with one of these, which would go well with some of those legendary brown and beige Noctua fans. It also wouldn't look out of place next to all my retrocomputers, the majority of which are a similar beige!

Now all I need is space, money, shipping from the US which currently isn't guaranteed, and space. Did I mention money, and space?
