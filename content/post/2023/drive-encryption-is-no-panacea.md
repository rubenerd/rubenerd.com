---
title: "Drive encryption is no panacea"
date: "2023-08-19T17:09:55+10:00"
abstract: "Explaining why unqualified security statements are dangerous."
year: "2023"
category: Software
tag:
- encryption
- privacy
- security
location: Sydney
---
Tell me if you've read something like this before:

> Run File Vault, the Apple disk encryption on your drive. And you never need to worry again.

A friend of mine forwarded that comment from a popular (though rapidly degrading) social network, and asked if the advice was true.

It's another classic example of a **no with an if**, or **yes with a but**, followed by a discussion about their threat domain. Or in English, what is it you're trying to protect, and from whom.

Saying drive encryption will protect your data is akin to claiming one's VPN service is *military grade*, and will protect you from online threats. At best it's marketing fluff, at worst it lulls people into a false sense of security.

Drive encryption is fantastic for protecting data at rest. You can lose an encrypted USB key, ship back a defective laptop drive under warranty, or blow away a cloud VM disk, and you've made it practically infeasible to retrieve the plaintext. As long as that's how it's sold, it's completely fine.

Even assuming trustworthy and perfectly-implemented cryptography, drive encryption only protects your data while its encrypted. Drives must be mounted to be of any use, which renders it as vulnerable as a regular one! If your laptop isn't patched, or you visit malicious sites, or you leave your machine unattended while running, or you think a file is gone upon emptying the Trash, or you're the victim of a social engineering attack, the drive encryption is doing *sweet FA*, to use the technical term Bruce Schneier employed in his seminal work *Practical Cryptography.* Or was it some notes I took?

This is why unqualified statements like the above are so dangerous, especially when discussing this with non-technical people. Drive encryption is a valuable and necessary layer of security, but you still need to be responsible.
