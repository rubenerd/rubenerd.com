---
title: "E-readers, and feedback from Daniel O’Connell"
date: "2023-08-18T16:38:28+10:00"
abstract: "Going back to e-readers after using an iPad."
thumb: "https://rubenerd.com/files/2022/paperwhite-bye@1x.jpg"
year: "2023"
category: Hardware
tag:
- books
- comments
- ebooks
- feedback
location: Sydney
---
On Tuesday I talked about what I'd love to have in my bag, in addition to what I currently cart around to coffee shops and work:

[My daily carry in 2023](https://rubenerd.com/my-daily-carry-in-2023/)

I mentioned I was interesed in e-readers:

> Speaking of books, I’m reconsidering my stance on e-readers. I tried also using my iPad Mini as a daily reader, but its middling battery and weight lead me to reconsider. I’m thinking probably a mid-sized Kobo with enough resolution and storage for manga as well as books. I tend to have a few books going at the same time, so having one reader is brilliant.

This was written in haste, so was missing some context. I've had e-readers before, specifically Kindles. I absolutely loved them, and read hundreds of novels, non-fiction works, light novels, and manga on them. Their months-long battery life, crisp screens, storage capacity, and near weightlessness made them perfect for commuting and travel.

Weirdly enough, my (diagnosed, thank you!) OCD took great satisfaction in seeing those dark flashes that come from the screen refreshing, even though I know others find it jarring. *But I digress*.

<p><img src="https://rubenerd.com/files/2022/paperwhite-bye@1x.jpg" srcset="https://rubenerd.com/files/2022/@1x.jpg 1x, https://rubenerd.com/files/2022/paperwhite-bye@2x.jpg 2x" alt="Screenshot showing the last book I read on the Kindle: the first volume of the Penguindrum manga" style="width:500px; height:375px;" /></p>

After I retired and gave away my last Kindle, my plan with the iPad Mini was to recreate this experience, but with a colour screen. This would let me read news magazines, colour manga, and periodicals in addition to text. But as I mentioned above, it's far heavier for its given size, and its battery life means carrying an additional charger everywhere. As I've come to expect with many things, technical specifications don't mean squat if the experience you're after isn't met.

Daniel O'Connell chimed in with his experience:

> I have my second (or is it third?) kobo reader. Previously I had Sony T2s. They’re an amazing invention. Whenever one breaks, I order a new one as soon as I’m back to having internet access. I used to read a lot (I had a ~2-3h buses trip each way to university, and then work) and carrying around multiple books was a pain, especially when they were large. My kobo aura is waterproof, so I can read in the bath, or on a boat. Not that I often do, just it’s nice when the option comes up.

As an aside, that bus trip sounds brutal! Accoustic books are fantastic in their own way, but I also love the fact I can keep several of them on me in the same light package, and I'm not bound (HAH!) to the same typographic constraints of the original book.

> That being said, refresh times are really annoying when you want to read a pdf or anything which needs to be zoomed in. Manga is ok, the big offender are technical references, articles and textbooks. If you’re sitting in a well lit room they’re passable, but reading on a train or something is hard. It’s mainly an issue of the zooming - you don’t want to zoom too much, as then you have to scroll around, which takes ages. But you have to do a slight zoom, or otherwise it’s unreadable. If you have the room, and will read a lot of pdfs, go as large as possible.

I'm relieved to hear about manga. Even though manga pages tend to be much smaller and more ergonomic than *bandes dessinées* volumes (sorry, my beloved Tintin!), my old Paperwhite's screen was still a shade too small to read comfortably on a moving train.

As for larger pages, I remember others warning me about this a few years ago too. There was that extremely expensive Kindle back in the day that offered near-A4 sized page reading, and there are a few bespoke e-readers at that size now. But I never tried reading textbooks or manuals on those smaller screens, despite my aching back being sorely tempted.

> One very nice thing about ebook readers are tickets on the kobo - when you turn it off, it will show the current page. Which can be anything, so you can have tickets etc. continuously displayed on your screen without having to worry about unlocking your screen etc.

This is *brilliant!* Again, especially for travelling.

I'm not sure if it's still the case, but the Kindle had a frustrating "screen saver" image that would appear after a few minutes of use. I never understood why the last page couldn't be left there; it's not like the girls in the *Quintessential Quintuplets* need a rest break after being on a page for too long. Or Captain Haddock, if the stories of his escapades fit.

Money is a bit tight again right now for external reasons, but I'm thinking a larger-sized Kobo might be in the works.
