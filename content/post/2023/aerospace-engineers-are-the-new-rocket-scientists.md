---
title: "Aerospace engineers are the new rocket scientists"
date: "2023-01-11T07:55:40+11:00"
abstract: "I’m not sure what uniqely qualifies them to endorse a mattress or breed of tomatoes."
year: "2023"
category: Thoughts
tag:
- advertising
- language
location: Sydney
---
Do you remember when everyone held *rocket scientists* with such high regard, that their intelligence was strategically launched as joke punchline? I feel like aerospace engineers are that for advertisers.

In the space of a week I've heard *aerospace engineers* (allegedly) support a mattress payment plan, a razor, and a breed of tomatoes.

You'd need to have a brain between your ears to juggle the physics, mathematics, and industrial design to be an aerospace engineer, but I'm not sure what uniquely qualifies them to recommend such things! ✈️
