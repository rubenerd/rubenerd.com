---
title: "FreeBSD brightness controls on Panasonic laptops"
date: "2023-06-08T13:23:50+10:00"
abstract: "Use acpi_video(4), not acpi_panasonic(4) for modern machines"
year: "2023"
category: Hardware
tag:
- bsd
- freebsd
- laptops
- panasonic-lets-note
location: Sydney
---
I have a longer post about my "new" Japanese Panasonic CF-SZ6 laptop pending. But in the meantime, you can enable Yamamoto Taku's **acpi_video(4)** driver to get backlight controls on your function keys:

    sysrc kld_list="i915kms acpi_video"

Older Japanese Panasonic laptops used Ogawa Takaya's and Takahashi Yoshihiro's **acpi_panasonic(4)**, which I assumed was required but broken on these newer machines. I'd written my own devd wrapper scripts around intel_backlight, before realising they implement generic ACPI drivers. *Whoops!*

[FreeBSD manpage(4) for acpi_panasonic](https://man.freebsd.org/cgi/man.cgi?acpi_panasonic)   
[FreeBSD manpage(4) for acpi_video](https://man.freebsd.org/cgi/man.cgi?acpi_video)
