---
title: "Happy New Year, 2024!"
date: "2023-12-31T17:23:32+11:00"
year: "2023"
category: Thoughts
tag:
- milestones
- new-year
location: Sydney
---
However you celebrate it, and wherever you are, I hope you have a great new year. As I used to end posts with, wishing you *peace, health, and happiness.* Thanks for sharing some of your time with my blog. 🎉

Time to create a new `/2024/` folder on the server.
