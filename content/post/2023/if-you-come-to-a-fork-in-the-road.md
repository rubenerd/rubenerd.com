---
title: "If you come to a fork in the road"
date: "2023-07-19T18:56:49+10:00"
abstract: "… wash it, and use it for dinner."
year: "2023"
category: Thoughts
tag:
- pointless
location: Sydney
---
... wash it, and use it for dinner.

Then when life gives you lemons, ask where it got them.

Bagels.
