---
title: "Concluding my link experiment"
date: "2023-10-06T09:37:46+11:00"
abstract: "I thought it made posts easier to read, but it made writing harder."
year: "2023"
category: Thoughts
tag:
- design
- gemini
- gopher
- links
location: Sydney
---
This is not to be confused with a [Link](https://zelda.fandom.com/wiki/Link "Link's page on the Zelda Wiki") experiment from *Zelda*, which it might shock you to hear that I've never played... though not from lack of motivation and encouragement from friends and people I respect. But as is extremly unusual for me, *I digress*.

Back in April I started an experiment where I [stopped using inline links](https://rubenerd.com/i-like-how-gemini-handles-links/ "I like how Gemini handles links"). I thought putting links in their own lines would aid readability, and make polyglot markup for Gemini possible. *Polyglot* still sounds like something you'd unclog a drain with. Maybe there's a metaphor there for what Gemini represents compared to the modern web. But, damn it, I digress, again.

A few months in though, and I don't think I like it. It made my writing more stilted, and I struggled at times to figure out where to put links. I've been on a tear lately to remove things that make writing harder, and this turned out to be another one.

I also couldn't help but feel I was missing out on one of the core design choices of the original world wide web; this is a much bigger topic I'm saving for another post. Even those classic multimedia CD-ROM encyclopædias I love have inline links, and I find them extremely useful.

Inline links also allow me to [include link gags](https://en.wikipedia.org/wiki/Mashed_potato "Wikipedia article on mashed potato, for no good reason"), though the absense of those lately was probably more a feature than a bug. *Polyglot*.

I'm going back to inline links, but I'm making sure each link has alt text. I should have been doing that already for accessibility, but this way I can always render them out into their own section if I end up doing a Gemini (or Gopher) site as well.
