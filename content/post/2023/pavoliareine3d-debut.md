---
title: "Pavolia Reine’s 3D debut!"
date: "2023-07-15T00:22:33+10:00"
abstract: "AAAAAAAAAAAH!"
thumb: "https://rubenerd.com/files/2023/reine-end-card@1x.jpg"
year: "2023"
category: Anime
tag:
- hololive
- hololive-indonesia
location: Sydney
---
My vtuber *oshi* Pavolia Reine from Hololive Indonesia had her 3D debut this evening, and it was *spectacular!* The setup, songs, her interactions with her guests, the dancing *Tatang*, AAAAAAAH!

During the brief meet-and-greet earlier this month, she talked about how much work it was preparing for it, and having now seen it, *I can believe it.*

[Meeting Pavolia Reine at SMASH!](https://rubenerd.com/meeting-pavolia-reine-at-smash/)

I've had "oshis" among each Hololive branch and generation, but as if I needed further confirmation that she's the best of them all :'). Terima kasih!

<figure><p><img src="https://rubenerd.com/files/2023/reine-end-card@1x.jpg" alt="Ending card after the stream with her smiling next to her mascot Tatang! Text reads: After waiting for so long, the time to celebrate and cheer as finally come! There are still so many things to see and experience in the story of Pavolia Reine, and I am forever grateful to have you all be a part of it. I will always treasure the memories we have, and I'm excited to make new ones!" style="width:500px; height:282px;" srcset="https://rubenerd.com/files/2023/reine-end-card@2x.jpg 2x" /></p></figure>

[【3D SHOWCASE】Attention Please!! Special Tea Party with Pavolia Reine!!! #PavoliaReine3D ](https://www.youtube.com/watch?v=aBgFX_2oIU4)
