---
title: "Australian floor numbering"
date: "2023-10-31T09:51:41+11:00"
abstract: "P1, P2, and B1 makes no sense, to say nothing of floors being off by one above ground!"
year: "2023"
category: Thoughts
tag:
- australia
- design
- lifts
- numerology
- numbers
location: Sydney
---
Last week I talked about some issues I had with a [pair of lift indicators](https://rubenerd.com/lift-kerning/ "Floor indicator kerning, and other things"), like a gentleman. I mentioned that:

> P1 is such a ridiculous number for a floor, but I digress.

Me, digress? *Never!* But it's time to address this.

I grew up in Singapore and Malaysia, countries with English-speaking populations that understand how to number floors in buildings. Floors are numbered, and basements are prefaced with a **B**. That's it:

* Floor 3: **3**
* Floor 2: **2**
* Floor 1: **1**
* Basement 1: **B1**
* Basement 2: **B2**
* Basement 3: **B3**

Want to know where you are relative to another floor? Simple, minus your destination from your current position. This is useful if you're like me and take the stairs for exercise, but more importantly it helps in an emergency. Ask me how I know *that!*

Australian buildings don't do this, perhaps from all the jostling we get being carried around in kangaroo pouches to work. Here's what a typical building here looks like:

* Floor 3: **2**
* Floor 2: **1**
* Floor 1: **G**
* Basement 1: **P1**
* Basement 2: **P2**
* Basement 3: **B1**

This might seem normal if you grew up here, but it makes absolutely no sense.

The first floor of Australian buildings are listed as **G** for Ground. This sounds logical, until you realise buildings on anything other than a flat surface have exits on other floors. I was in a shopping centre in Eastwood recently where the main entrance to ground outside was level **2**. What's the point of trying to unambiguously refer to a ground floor, if it's not at ground?

As an aside, I've been told labelling **G** is important so people know where to evacuate from, and where emergency service vehicles should be posted. Leaving aside other countries say the same thing about level **1**, the above example proves that's not helpful. I'm going to head to a lobby and exit out whichever door I can. Emergency evacuation points exist for offices and industrial facilities, but do you think people in a sprawling shopping centre will know where this is in any given building? If emergency exit signs exist to direct people to safety, I've seen them clearly indicate exit doors in plenty of buildings on floors above (and in one case, below) **G**, and I've worked in an office building where the emergency evacuation point was out of a door on level **1**. There's a whole post probably buried here, but I don't buy this justification for a magical **G** floor.

This also has other implications. You'd think calling your first floor **G** would mean the second floor is **2** right? Of course not! Instead, the second floor starts counting from **1**. So you get into this situation where every floor is off by one, for the entirety of the building.

Basements are somehow even worse. Australian buildings reference basement floors by function, not location. **P1** in my original post refers to Parking Floor 1, which is the first basement. You might then have a **P2** and a **P3**. As we saw with **G**, the fourth basement without cars isn't labelled **B4** as you'd expect, but instead **B1**. I believe the colloquial term for this is *bong rip!*

This is a horrible idea. If I'm stuck somewhere, I want to know how many floors I am from safety. Floors being off by one above-ground feels like an engineer [took the piss](https://en.wiktionary.org/wiki/take_the_piss "Wiktionary definition of the idiom taking the piss"), but **B1** gives me no indication whatsoever where I am in the building.

I may only be *slightly* embellishing my frustration here, but it still strikes me as odd. The arguments I hear for maintaining this system remind me of Americans who claim archaic measurements makes more sense than SI. [Australia transitioned to Metric](https://en.wikipedia.org/wiki/Metrication_in_Australia "Wikipedia article on Metrication in Australia") in the 1970s, we can transition to logical floors too.
