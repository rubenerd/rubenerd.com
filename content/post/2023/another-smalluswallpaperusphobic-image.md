---
title: "Another Smalluswallpaperusphobic image"
date: "2023-04-02T08:58:51+11:00"
abstract: "Bradenham Village Green by Gordon Eagling"
thumb: "https://rubenerd.com/files/2023/Village_Green,_Bradenham_-_geograph.org.uk_-_600750.jpg"
year: "2023"
category: Media
tag:
- chill
- desktop-backgrounds
- photos
- uk
location: Sydney
---
In 2010 I defined *[smalluswallpaperusphobia](https://rubenerd.com/anime-smalluswallpaperusphobia/)* as the fear of not being able to use an epic image as a background, on account of it being too small.

<figure><p><img src="https://rubenerd.com/files/2023/Village_Green,_Bradenham_-_geograph.org.uk_-_600750.jpg" alt="Photo of a lovely green English landscape" style="height:136px; width:235px;" /></p></figure>

Today's image isn't of Akiyama Mio from *K-On!*, but a photo of Bradenham Village Green in the UK, [by Gordon Eagling](https://commons.wikimedia.org/wiki/File:Village_Green,_Bradenham_-_geograph.org.uk_-_600750.jpg). The colours would make a great, calming background. Alas, its diminutive size even limits its upscale potential, especially on the HiDPI screens I optimise this site for.

As an aside, *green* spelled backwards would present its letters in reverse order.
