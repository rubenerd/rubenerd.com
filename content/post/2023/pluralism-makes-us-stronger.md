---
title: "Pluralism makes us stronger"
date: "2023-08-30T08:15:18+11:00"
abstract: "Populists always want to make it about us versus them. The opposite is true."
year: "2023"
category: Thoughts
tag:
- culture
- politics
location: Sydney
---
Timothy Snyder [makes an interesting observation](https://snyder.substack.com/p/gratitude-to-ukraine) in a post thanking Ukraine last year:

> Who were these people? Zelens'kyi, who represents a national minority in Ukraine (he's Jewish), was elected by 73% of the population. This suggests the pluralism that is essential to Ukraine, and to Ukrainian resistance.  Apologists for dictators (and there are many such people in the United States), tend to claim that only uniformity will bring effectiveness, especially is times of war. This is certainly the approach that Russia brought to the war: uniformity of command, uniformity of ideology, and the bloody and criminal attempt to homogenize Ukrainian territories under occupation which amounts to genocide.

I've noticed this latter streak in so many populist ideologies: there's a core class of people who have to be defended from a wave of "undesirables" who are different from us. It ignores that many of those people in places like the United States, Australia, and Canada are themselves a product of immigration, and that political borders are largely arbitrary and have been fluid throughout history.

I raise this in the context of Russia too, given their country's diverse population and superficial federalist structure. Do we think Putin and his cadre of Muscovites represent people in Tuva (a place I've always wanted to visit), or even give them a second thought? Likewise, does a Tuvan want to be drafted into a war on the other side of the planet to attack people for whom they harbour no ill will?

This isn't to say one shouldn't celebrate their own ethnic culture and history, but that it's one part of an even more spectacular patchwork. Some people want to cut their cultures out of it, which is a shame for everyone.
