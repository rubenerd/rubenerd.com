---
title: "FreeBSD 14-RELEASE’d!"
date: "2023-11-24T09:00:15+11:00"
abstract: "Lots of changes in ZFS and bhyve look amazing."
year: "2023"
category: Software
tag:
- bsd
- freebsd
- news
location: Sydney
---
[From the announcement](https://www.freebsd.org/releases/14.0R/announce/)\:

> The FreeBSD Release Engineering Team is pleased to announce the availability of FreeBSD 14.0-RELEASE. This is the first release from the stable/14 branch.
> 
> Some of the highlights:
> 
> * OpenSSH has been updated to version 9.5p1.
>
> * OpenSSL has been updated to version 3.0.12, a major upgrade from OpenSSL 1.1.1t in FreeBSD 13.2-RELEASE.
>
> * The bhyve hypervisor now supports TPM and GPU passthrough. **[YAY! –ed]**
>
> * FreeBSD supports up to 1024 cores on the amd64 and arm64 platforms.
> 
> * ZFS has been upgraded to OpenZFS release 2.2, providing significant performance improvements.
> 
> * It is now possible to perform background filesystem checks on UFS file systems running with journaled soft updates.
>
> * Experimental ZFS images are now available for AWS and Azure.
> 
> * The default congestion control mechanism for TCP is now CUBIC.
> 
> * And much more…

OrionVM's FreeBSD 14 template is in testing now; I hope to have it done in the next week or so.

I haven't had the time to mess with bhyve much lately, but GPU passthrough is incredibly exciting. Maybe my longstanding project to use and write about this will be another [December Time project](https://rubenerd.com/december-time/)!

Thank you to the release and engineering teams who made this possible. Don't forget you can support FreeBSD directly by [contributing to the Foundation](https://freebsdfoundation.org/donate/ "FreeBSD Foundation donate"). 
