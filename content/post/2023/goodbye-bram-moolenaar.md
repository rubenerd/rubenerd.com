---
title: "Goodbye, Bram Moolenaar ♡"
date: "2023-08-06T11:15:53+10:00"
abstract: "The lead developer of Vim just wrote :wq. Rest in peace. ♡"
thumb: "https://rubenerd.com/files/2023/goodbye-bram@1x.png"
year: "2023"
category: Software
tag:
- editors
- goodbye
- vim
location: Sydney
---
<figure><p><img src="https://rubenerd.com/files/2023/goodbye-bram@2x.png" alt="The lead developer of Vim just wrote :wq. Rest in peace. ♡" style="width:456px; height:364px;" /></p></figure>
