---
title: "Seeing Paul McCartney live in Sydney! ♡"
date: "2023-10-29T11:49:04+11:00"
abstract: "It was the best performance of my life!"
thumb: "https://rubenerd.com/files/2023/paul-mccartney-syd-accoustic@1x.jpg"
year: "2023"
category: Media
tag:
- music
- paul-mccartney
- the-beatles
location: Sydney
---
Clara and I booked tickets to see Paul McCartney's sold-out Friday show in Sydney, and deliberately avoided reading about his US tour so as much of it would be a surprise as possible. 

It was... I can't summon the words. Even Clara's [bear cat came](https://rubenerd.com/happy-80th-paul-mccartney/ "Happy 80th, Paul McCartney!")!

Paul and his incredible band played Beatles, modern songs from *Memory Almost Full* and *NEW*, and delved into plenty of his classics. He was witty, self-effacing, and he *still had it!*

**BAND ON THE RUN!** *Maybe I'm a lonely man who's in the middle of something, that he doesn't really understand...*.

<figure><p><img src="https://rubenerd.com/files/2023/paul-mccartney-syd-accoustic@1x.jpg" alt="Photo of the large screens during one of Paul’s accoustic numbers" style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2023/paul-mccartney-syd-accoustic@2x.jpg 2x" /></p></figure>

I was bitten by *Beatlemania* around the time I learned how to walk, but I only started down the rabbithole of Paul's Wings and solo work in my twenties. With my (remaining) family as witnesses, Starbucks were running a promotion in Malaysia at the time for *Memory Almost Full*, which I became more than a little obsessed with.

Over the years I slowly started working backwards through his modern catalogue, and since meeting Clara we've been going the other way from *McCartney* upwards. He spans so many genres and styles, we joke that Paul has an album for any mood or time of day. He joked that people only wanted to hear his old stuff, but as I was overjoyed to hear him grin with his mandolin playing *Dance Tonight*.

*That said*, there was also the obvious giddy, unreal thrill of **seeing a fucking Beatle** perform *Can't Buy Me Love*. I choked up when he played *Something* on an instrument given to him by George; my mum and I loved his tunes, and I wish she could have been there to see it too.

<figure><p><img src="https://rubenerd.com/files/2023/paul-mccartney-programme@1x.jpg" alt="Photo showing the Got Back programme and some of our LPs" style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2023/paul-mccartney-programme@2x.jpg 2x" /></p></figure>

I didn't take many photos, because I was too busy watching and generally bouncing around in what could best be described as... embarrassing! It was the best performance of our lives; we can scarcely believe it happened. Thank you Paul, Wix, Rusty, Brian, Abe, and all the crew! We'll treasure these memories for a very long time. 🎸
