---
title: "Spelling misatkes (cough)"
date: "2023-08-19T09:07:16+10:00"
abstract: "I’m trying to train myself to stop misspelling these words."
year: "2023"
category: Thoughts
tag:
- english
- language
- pointless
location: Sydney
---
I'm trying to train myself to stop misspelling these words:

* acknowlege &rarr; acknowledge
* discernable &rarr; discernible
* existance &rarr; existence
* eshew &rarr; eschew
* maintanance &rarr; maintenance
* vaccum &rarr; vacuum
* vechile &rarr; vehicle

My success rate thus far is hovering around... 50%? I'm at the point now where I remember both spellings, but forget which one I'm supposed to be using.

Have I mentioned recently that I feel for people learning English?
