---
title: "Data privacy in Musk’s data centre move"
date: "2023-09-18T12:39:55+10:00"
abstract: "Or lack thereof."
year: "2023"
category: Ethics
tag:
- news
- privacy
location: Sydney
---
This story of Space Karen's physical data centre move has been drawing the ire and glee of many a sysadmin and project manager today. They did everything predictably wrong, and faced the consequences.

[CNBC: Elon Musk moving servers himself shows his ‘maniacal sense of urgency’ at X, formerly Twitter](https://www.cnbc.com/2023/09/11/elon-musk-moved-twitter-servers-himself-in-the-night-new-biography-details-his-maniacal-sense-of-urgency.html)

But this was the section that raised my eyebrows, emphasis added:

> The **servers had user data on them**, and James did not initially realize that, **for privacy reasons, they were supposed to be wiped clean** before being moved. “By the time we learned this, the servers had already been unplugged and rolled out, so there was no way we would roll them back, plug them in, and then wipe them,” he says. Plus, the wiping software wasn’t working. “F---, what do we do?” he asked. Elon recommended that they lock the trucks and track them.

This cavalier attitude pervades IT, whether it's the latest generative AI trained on data without permission or recourse as required by law, or a company thinking it's sufficient to use a physical padlock on a truck to secure personally-identifiable information (PII).

It's not funny, it's horrifying. Okay, it's a bit funny, at least when viewed as richly-deserved schadenfreude. But it's mostly horrifying. Your size, connections, or ignorance shouldn't absolve you from responsibility.
