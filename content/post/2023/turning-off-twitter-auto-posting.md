---
title: "No more @Rubenerd_Blog Twitter posts"
date: "2023-05-27T12:15:17+10:00"
abstract: "Twitter’s API changes make it infeasible."
year: "2023"
category: Internet
tag:
- goodbye
- twitter
location: Sydney
---
With changes to Twitter's API, it's no longer feasible for me to auto-post new entries to the **@Rubenerd_blog** account. I apologise if that's how you tracked stuff here.

As an alternative, I'd recommend opening an account with one of the RSS reader services below, and adding my feed so you don't miss a post:

    https://rubenerd.com/feed/

It's the end of an era, but not unexpected. Up yours, Elon Musk. Maybe one day you'll be able to buy respect.

### Links

[My blogroll of sites I subscribe to](https://rubenerd.com/blogroll.opml)   
[My 2021 RSS primer](https://rubenerd.com/replacing-facebook-news-with-rss/)   
[TheOldReader](https://theoldreader.com/)   
[FeedLand](http://feedland.org/)   
[NetNewsWire](https://netnewswire.com/)
