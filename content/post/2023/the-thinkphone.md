---
title: "The ThinkPhone"
date: "2023-01-12T14:23:53+11:00"
abstract: "Looked cool! But of course it has an OLED, so that’s a fail."
year: "2023"
category: Hardware
tag:
- accessibility
- oleds
- phones
- thinkpad
location: Sydney
---
A ThinkPad... phone? [Sounds cool](https://www.motorola.com/us/smartphones-thinkphone/p):

> From complex dashboards to dense presentations, the smallest details stand out on a 6.6” pOLED FHD+ display.

Motorola has a bit more of an excuse than Apple for [not caring about accessibility](https://rubenerd.com/the-iphone-14/), given the lower volumes they'll be selling. But an LCD option would have been nice.

*(These posts always solicit at least one comment from someone claiming they can use OLEDs just fine, and feigning surprise at knowing your physiology better than you do. I wonder if they voice their cheese preferences to the lactose intolerant, or boast the repeated blows to the head they can take upon passing a helmet-clad cyclist).*

