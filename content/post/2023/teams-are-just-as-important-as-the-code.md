---
title: "Teams are just as important as their code"
date: "2023-03-30T09:00:35+10:00"
abstract: "“Additionally, your teams must be given the responsibility, authority, ownership, and support needed”"
year: "2023"
category: Software
tag:
- development
- quotes
- work
location: Sydney
---
I'm not as onboard with the idea of microservices as Lee Atchison is, but I loved this comment in his [December 2021 InfoWorld article](https://www.infoworld.com/article/3642835/a-cure-for-complexity-in-software-development.html) about it:

> Team size, structure, ownership responsibilities, and lines of influence are just as critical in building your application as the code itself. In order to handle a service architecture efficiently, you must organize your development teams around your application architect appropriately. Additionally, your teams must be given the responsibility, authority, ownership, and support needed to provide complete management of their owned services.

This takes on more urgency when you consider [Conway's Law](https://en.wikipedia.org/wiki/Conway%27s_law)\:

> Any organization that designs a system (defined broadly) will produce a design whose structure is a copy of the organization's communication structure.
