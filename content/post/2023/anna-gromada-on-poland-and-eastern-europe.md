---
title: "Anna Gromada on Poland and Eastern Europe"
date: "2023-04-28T11:15:11+10:00"
abstract: "The region has learned the hard way that if you are not at the negotiating table, you are on the menu."
year: "2023"
category: Thoughts
tag:
- eastern-europe
- europe
- germany
- poland
- news
- ukraine
location: Sydney
---
[Anna's opinion article in The Guardian](https://www.theguardian.com/commentisfree/2023/apr/25/poland-change-europe-high-achievers-country)

> When the iron curtain was swept away on that miraculous night of 9 November 1989, it exposed some of the deepest differences between geographical neighbours the world has ever recorded. The 13:1 GDP per capita gap between Poland and soon-to-be united Germany was twice that between the US and Mexico.

The whole article is interesting, but the ending articulated so well what so many of my friends have said, especially over the last year:

> As eastern Europe grows in power, it is questioning its role in the pecking order. The region has learned the hard way that if you are not at the negotiating table, you are on the menu.

