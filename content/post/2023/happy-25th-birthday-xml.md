---
title: "Happy 25th birthday, XML!"
date: "2023-02-11T08:38:20+11:00"
abstract: "XML is the language of the open web."
year: "2023"
category: Internet
tag:
- anniversaries
- atom
- foaf
- libreoffice
- opml
- rdf
- rss
- standards
- web-feeds
- xml
location: Sydney
---
The [Extensible Markup Language specification](https://www.w3.org/XML/) was first published on the 10th of February, 1998:

> Extensible Markup Language (XML) is a simple, very flexible text format derived from SGML (ISO 8879). Originally designed to meet the challenges of large-scale electronic publishing, XML is also playing an increasingly important role in the exchange of a wide variety of data on the Web and elsewhere.

XML continues to be an important format for documents and data exchange to this day. I appreciate its extensibility with namespaces, inline comment support, tooling, and that it can be easily transformed and validated.

Every day I interact with ODF, SVG, RSS, OPML, Atom, RDF, FOAF, XSLT, and/or (sigh) OOXML. Even if that all sounds like alphabet soup, you probably use or benefit from it without even realising.

Many formats and standards exist, but I'd argue XML remains the *lingua franca* of the open web. *Bagus!* 🎂
