---
title: "Buying parts online and iRL"
date: "2023-05-06T22:39:44+10:00"
abstract: "I do miss living in an Asian city with access to computer parts."
year: "2023"
category: Hardware
tag:
- funan-centre
- japan
- osaka
- sim-lim-square
- singapore
- shopping
- tokyo
location: Sydney
---
Some of my earliest and most fun childhood memories revolve around buying computer parts with my dad. We'd pour over price lists together, spec out what we wanted to do, then head down to explore. I've mentioned many times my first childhood computer that was financed by a primary school writing competition; this was how we built it.

As he travelled overseas more and I got older, I'd hop on the MRT and check out the parts shops myself. It was a weekend introvert activity before I even knew the term, though I also had many a fun chat with the shop owners in Funan Centre and Sim Lim Square. Turns out, if you're nice to people and slip in some local colloquialisms, you're seen as a harmless angmoh instead of an arrogant tourist. What a concept!

Singapore was a wonderland for computer fans in the 1990s and 2000s. It's faded a bit now in an age of disposable, sealed smartphones and tablets, with more shops selling boring cases as genuine computer parts. But even today one can get lost in their local *Challenger* store.

Kuala Lumpur in Malaysia was the same. Imbi Plaza had you covered for all manner of components, parts, and accessories.

<figure><p><img src="https://rubenerd.com/files/2023/me-hardoff-store-japan@1x.jpg" alt="Photo of me going through boxes of second-hand computer parts." style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2023/me-hardoff-store-japan@2x.jpg 2x" /></p></figure>

Ditto Japan. Clara and I know to reserve a solid day for Akihabara and Den-Den Town whenever we're lucky enough to venture there, because we know we'll be pouring over the computer shops for hours (especially second-hand stores like Hard Off, where Clara snapped that incredibly handsome photo of me above. Did I say handsome? I meant awkward)! I continue to be shocked at the niche, specific items that are available in Japanese computer stores, stuff that even Singaporean vendors had the business sense to fire sale years ago.

[Exploring an incredible Kyoto Hard Off store](https://rubenerd.com/exploring-an-incredible-kyoto-hard-off-store/)

Some of it is window shopping. Okay, most of it is. But I do miss living in an Asian city where I could just walk down the road to get a part for a project. A 6502 CPU? Some DIMMs? An ancient SCSI cable? A modern SATA adaptor? It doesn't matter, it's all a few train stops or a walk away. Heck, I'd even take an American Microcentre!

And it's not even the instant gratification; at least, not primarily. It's hard to describe, but having a place dedicated to your hobby feels *awesome*. It's a way to connect with people in real life who share your interests. They can help you with problems, appreciate your enthusiasm, and you can return in kind. It wouldn't be a stretch to admit that I got most of my social interactions during school holidays and weekends by doing this.

Practically speaking, it also means you can iterate on ideas and troubleshoot quickly and easily. More on that in a moment.

Sydney is not like this. Granted, there are the computer stores in Capitol Square, but it's not the same. Being an isolated continent in the middle of nowhere comes with many advantages (clean air, the world's most amazing skies), but with a population smaller than Ukraine or Texas in a land area almost as big as the contiguous US, it seems there simply isn't a big enough market to sustain those sorts of stores.

At any one time, I have dozens of random things in the post. I'm lucky that there's a world of parts I can tap into, even with the *de facto* Australia Tax we pay for shipping things out here. I'm not sure I'd be half as engaged in any of this stuff, or my professional career at all, if it weren't for the Internet or a precursor.

*(Wait, I wouldn't have a job at all if it weren't for the Internet. Wow, it sounds ridiculous, but I've never considered this).*

But in what's probably another extreme case of *first-world* problems, it does make things tedious and less fun. I'd have built out this refurbished 386 motherboard ages ago. Troubleshooting my Commodore 128 and C16 would have taken days, not months. You'll realise you need a part, order it, wait a few weeks, realise it's wrong or not where the problem was, then sit on your hands as you do it all again.

If I had more money and time than common sense, I'd open a retro computer store parts shop right in the centre of Chatswood in Sydney, selling all manner of things that those Japanese, Singaporean, and Malaysian outlets would. Even if it were a glorified storage facility for my own interests!
