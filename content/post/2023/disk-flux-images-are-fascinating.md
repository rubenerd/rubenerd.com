---
title: "Disk flux images are fascinating"
date: "2023-10-03T08:32:49+11:00"
abstract: "Revealing where data is stored, and how tracks/sectors are laid out."
thumb: "https://rubenerd.com/files/2023/apple-ii-flux@1x.png"
year: "2023"
category: Hardware
tag:
- data
- disks
- retrocomputing
location: Sydney
---
In addition to disk images on the Internet Archive, people have been uploading magnetic flux information for various disks. This permits performing low-level forensic or other analysis on the signals stored on the disk, which can help unearth, decode, an correct unearthed data.

The images reveal what parts of the disk contain data, and how the disk was low-level formatted. The sectors on this image make the disk look like the saucer section of a Star Trek ship.

<figure><p><img src="https://rubenerd.com/files/2023/apple-ii-flux@1x.png" alt="Monochromatic image of the surface of a disk, showing lighter and darker areas where data are located." srcset="https://rubenerd.com/files/2023/apple-ii-flux@2x.png 2x" style="width:500px; height:500px;" /></p></figure>

Other images look more like rotating cutting disks, or the centre of a black hole, depending on their track layouts and sector boundaries. Check out this archive to see more:

[Internet Archive: Flux Compilations](https://archive.org/details/flux_compilations)
