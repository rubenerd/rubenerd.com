---
title: "Behind every rock, there’s another one"
date: "2023-04-03T15:31:59+10:00"
abstract: "Being mature is realising it."
year: "2023"
category: Thoughts
tag:
- personal
location: Sydney
---
**Update:** I've removed this.

The subtext was that something had again been impacted by a health issue, and that everything important has been since I was a kid. But I'm far from the only person who's lived a life like this, and I have other things to be thankful for.

Being mature isn't (only) coping with the rock, it's handling the fact there's always another one behind it. All the more reason to have a sense of humour about it too.

