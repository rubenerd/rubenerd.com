---
title: "Sources in my saving video post"
date: "2023-07-26T17:01:00+10:00"
abstract: "Linking to channels I showed on that post."
thumb: "https://rubenerd.com/files/2023/indie-youtube@1x.jpg"
year: "2023"
category: Internet
tag:
- adrian-black
- ceres-fauna
- hololive
- melifssa-anya
- retrocomputing
- garyh-tech
- pavolia-reine
- video
- urbanism
- youtube
location: Sydney
---
Last Tuesday I wrote a post about the ephemeral nature of online video, and my attempts to preserve stuff Clara and I care about.

[If you care about it, archive it: take two](https://rubenerd.com/if-you-care-about-it-archive-it/)

I included a sample of some random videos I'd recently watched to break up the wall of text:

<figure><p><img src="https://rubenerd.com/files/2023/indie-youtube@1x.jpg" alt="A small sample of what Clara and I watch thesedays." style="width:500px;" srcset="https://rubenerd.com/files/2023/indie-youtube@2x.jpg 2x" /></p></figure>

A few of you asked what those channels are, so here we go. From top-left to bottom-right:

[Pavolia Reine](https://www.youtube.com/@PavoliaReine/streams)

She's my favourie vtuber of all time, as mentioned here a few times this month. She's a member of Hololive Indonesia Generation 2, and is one of those maddeningly talented people who can switch between English, Japanese, and Bahasa Indonesia on the fly, while playing something, and keeping track of chat. She's smart, funny, and entertaining!

[Not Just Bikes](https://www.youtube.com/@NotJustBikes)

My favourite urbanist channel. I knew I preferred mixed-use neighbourhoods with walkability and public transport over owning a car, but his channel based out of Amsterdam gave me the tools to understand *why*. I've suffered neck injuries from nodding profusely at his meticulous environmental, financial, and health arguments against car-centric policy.

[Adrian's Digital Basement](https://www.youtube.com/channel/UCE5dIscvDxrb7CD5uiJJOiw)

A retrocomputing channel run by Adrian Black out of Oregon in the US. His enthusiasm for computer history is infectious, and his deep and broad knowledge of retro hardware have been instrumental in helping me fix my own gear. I look to his channel for advice as much as entertainment.

[Ceres Fauna](https://www.youtube.com/@CeresFauna)

My favourite vtuber from Hololive English. Her silky calm voice and weekly Minecraft streams are so soothing, I've melted in my chair and had to be re-solidified on at least a few occasions. She also plays games I'm interested in, and I loved her *Our Bright Parade* bossa nova music performance. Incidently, her collaborations with Pavolia Reine have been among my favourites from both of them.

[GaryH Tech](https://www.youtube.com/@GaryHTech)

Along with RoboNuggie, they're my favourite FreeBSD video creators. His production values are top notch, and I love how he walks you through his thought process for building, testing, and fixing things, rather than reading off a script or giving a dry talk.

[Anya Melifssa](https://www.youtube.com/@AnyaMelfissa/streams)

One of Pavolia Reine's genmates from Hololive Indonesia. Like Ninomae Ina'Nis who she collaborated with on that video (and who I also love!), she's a chill, fun, and talented streamer who plays a lot of stuff I'm interested in. Her *Unpacking* stream may be one of my go-tos when I'm feeling stressed.

