---
title: "Heatwave and Unicode in Sydney"
date: "2023-12-09T09:49:21+11:00"
abstract: "Stay safe and hydrated, and also check out these temperature symbols."
year: "2023"
category: Thoughts
tag:
- climate
- unicode
- weather
location: Sydney
---
It's already 38 degrees (≅ 100 ℉) where Clara and I are in Sydney, and the forecast expects it to go much higher by midday. I've broken out the collared shirt and chinos, because while looking overdressed, they're the only clothes I can wear without my angmoh skin burning off.

You don't need me to tell you, but stay hydrated and cool. I have a friend who's a paramedic, and you *don't* want to not take that seriously. That was a double negative way of saying *take it seriously*. No, really, have you had a glass of water recently?

I suppose one good thing has come out of this: I learned about some new Unicode symbols! We have ℃ and ℉ for temperature, and ≅ for approximate values. In the words of Technology Connections: *neat*.

In the old days I used to wrap unusual Unicode characters with `abbr` HTML tags, with an alternative representation if the client didn't support rendering them. I don't think I need to do that anymore, but let me know if your browser shows you those dreaded Unicode boxes. I'd love to sprinkle in more of these finds into posts.
