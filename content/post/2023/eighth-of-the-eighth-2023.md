---
title: "Eighth of the eighth, 2023"
date: "2023-08-08T08:08:08+10:00"
abstract: "Here are eight disparate thoughts for the day."
year: "2023"
category: Thoughts
tag:
- dates
- numerology
- pointless
location: Sydney
---
Here are eight disparate thoughts for the day:

* Symmetrical dates like this bring everyone together, whether you use the superior `YYYY-MM-DD` ISO and East Asian date format, the global `DD-MM-YYYY` format, or North American `MM-DD-YYYY`. I love my American friends, but the order of your dates make absolutely no sense.

* *08-08* looks like a pair of glasses a manga character would wear to express confusion.

* Eight is the luckiest number in Chinese numerology. It's still so ingrained from living overseas that I wouldn't pretend I don't believe in a *tiny* bit of it. Maybe it's a self-fulfilling prophecy: if I *think* the day will be lucky and good, the chance is higher it will be.

* I just updated pkgsrc on an older box, and got eight outdated packages. If I had one fewer, it'd only be seven.

* This is not the eighth point in this list. If it was, it'd be the eighth point.

* I'd say I owe my lovely readers eight email replies, but that would be underplaying it by at least eight others.

* I'm posting this at 08:08, but I'd be tempted to post at 18:00 so I could also talk about what I ate on the eighth of the eight. Mmm, that's a quality pun.

* This list has eight items, including this one. Is that a cheat? Almost certainly.
