---
title: "Defaulting pedestrian traffic lights to green"
date: "2023-03-17T09:37:13+10:00"
abstract: "Can we have this everywhere?"
year: "2023"
category: Travel
tag:
- cars
- pedestrians
location: Sydney
---
The traffic lights at a local T-intersection were recently tweaked, then changed back. It's a larger thoroughfare with a smaller branch road, and pedestrian crossings on each side.

The pedestrian lights used to default to red, even if the cars also had red. This meant it was one of the few intersections where you'd be standing there indefinitely unless you pressed the crossing button.

The *ackchyually, crossing buttons don't do anything* factoid seems to circulate every now and then, but it's absolutely not true in places where I've lived. I had a friend in Singapore who's father worked at the Land Transport Authority, and he confirmed some buttons work, even at intersections. But I digress.

A few weeks ago I walked out and the pedestrian light was green, and the traffic light for the branch road was red. It stayed like this until a car came down the street and, presumably having driven over the pressure sensor, flipped the pedestrian crossing light to red.

Not sure whether this was a one-off, I decided to actively observe the state of each light whenever I used the intersection, as if to make a little mental truth table. Almost every day, the pedestrian light across this branch road remained green, and only changed when a car approached.

This was great! This crossing was otherwise one of the slowest I'd ever come across, and now I could saunter across whenever I wanted to, like a gentleman.

Alas, whatever experiment they were running on pedestrian accessibility came to an end, and now it takes me two more minutes to cross this intersection every day. It was a nice idea having motorists wait by default for once; they're the ones sitting in air-conditioned bubbles that make the rest of us wait in the beating sun or rain.
