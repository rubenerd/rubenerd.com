---
title: "A fear of the future is a fear to learn"
date: "2023-10-10T08:15:29+11:00"
abstract: "Not sure I entirely agree, but it’s interesting."
year: "2023"
category: Thoughts
tag:
- psychology
location: Sydney
---
I can't remember who posted this on Mastodon, but it's an interesting line.

I'm not sure I entire agree. I often fear the future *because* I learned something. Though maybe it’s more a comment about positive change. Our instinct is to fear it, until we learn how it would make things better. Some people never get to part two, and vote accordingly.

Maybe I should have had coffee before writing this. Will I ever learn?
