---
title: "Foam glass aggregate is a thing!"
date: "2023-03-21T08:30:23+11:00"
abstract: "I love discovering things I didn’t know existed."
year: "2023"
category: Thoughts
tag:
- engineering
- practical-engineering
- video
- youtube
location: Sydney
---
I love learning of things I never knew existed, especially if I've likely interacted with it before without even realising. It scratches that same itch I had as a kid when I'd pepper my poor parents with a trillion *why?* questions, or when they'd let me crack open a broken VCR to see how it worked.

Practical Engineering's [recent video on lightweight backfill](https://www.youtube.com/watch?v=t65tbfU9sCI) introduced me to the idea of foam glass aggregate, which has impressive properties for its volume and weight:

> It's created in a similar way to the expanded shale, where heating the raw material plus a foaming agent cretes tiny bubles. When the foamed glass exists the kiln, it's quickly cooled, causing it to naturally break up into aggregate-sized pieces. 
>
> You can see in my graduated cylinders here that I have one pound, or about half a kilogram of soil, sand, and gravel. It takes about twice as much expanded shale aggregate to make up that weight, since its bulk density is about half that of traditional embankment materials. And the foamed glass aggregate (right) is even lighter.

Here's a close-up screenshot from the video showing pieces of the aggregate. It looks like forbidden bread, or even bone. I suppose the principle is the same.

<figure><p><img src="https://rubenerd.com/files/2023/fga@1x.jpg" alt="Screenshot showing the foamy, bread-like texture of the beige aggregate pieces." style="width:500px; height:280px;" srcset="https://rubenerd.com/files/2023/fga@2x.jpg 2x" /></p></figure>

And here's how it compares volumetrically with other materials with the same total mass:

<figure><p><img src="https://rubenerd.com/files/2023/fga-vw@1x.jpg" alt="A row of glass cylinders showing various aggregate materials. The far left shows sand barely filling the bottom, the middle are half full with shale aggregate, and the foam glass aggregate is practically overflowing." style="width:500px; height:280px;" srcset="https://rubenerd.com/files/2023/fga-vw@2x.jpg 2x" /></p></figure>

I'd never considered that you'd want to make a backfilled onramp light to prevent sinking, but failure to do so can lead to the road deck on a bridge or overpass to end up higher. If you've driven up over an embankment and felt a bump, that's likely what happened.

One thing the video didn't address was the recycling potential, which also looks interesting. While it can be made with virgin material, foam glass aggregate can even be made with [recycled glass](https://foamit.fi/en/foamit-products/), helping to send less stuff to land fill. Although, now that I think about it, that's literally what it's being used for here! Sort of.

I often wonder how my life would have turned out if I studied to become a civil engineer (or an accountant) instead of going into IT. 
