---
title: "Archiving from The Internet Archive"
date: "2023-08-25T17:46:27+10:00"
abstract: "I worry the site isn’t long for this world."
year: "2023"
category: Internet
tag:
- archiving
- internet-archive
location: Sydney
---
This has been a recurring theme here of late. If you care about something, archive it. You may not end up being any more reliable than the service you love, but it's worth giving it a shot.

I've had regular monthly donations set up for the Internet Archive for years, and have been an advocate for the invaluable service they've offered the web over the past few decades. There isn't anywhere else online like it, whether it be their forward-thinking *Wayback Machine*, or their online library of materials.

[Donate to the Internet Archive](https://archive.org/donate)

Having said that, is a phrase with three words. I've been worrying about the future of the site for a while, not least due to their recent legal troubles. I'm starting to recommend that if people find something valuable there, download it. I'd hate for my fears to be proven right.
