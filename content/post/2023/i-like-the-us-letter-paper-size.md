---
title: "I… like the US Letter paper size"
date: "2023-03-03T10:10:26+11:00"
abstract: "It makes no mathematical sense, but the ratio is appealing in a visual way."
year: "2023"
category: Media
tag:
- calligraphy
- measurements
- paper
- standards
- united-states
location: Sydney
---
My current blog theme&mdash;itself a rehash of a much earlier one&mdash;hints to my interest in paper and paper sizes. But I don't think I've ever talked about it explicitly, or otherwise. Talking explicitly about paper sounds like doing things with it that would have you arrested if done in public.

I live outside the US, so I grew up using A4 paper for everything. It's one of those things you assume is the same everywhere until you come across a counter example.

A4 paper, like metric and SI units, has some neat features. Starting at A0, you can fold it lengthwise and horizontally to get the next size. A3 is 4× the area of A4, which is 4× the area of A5, and so on. It makes measuring, scaling, and tessellating paper trivial, something I also learned to appreciate living with a calligrapher who was charged for materials by weight.

Japan uses a variation of this called the *B Series* which follows the same internal logic. There's also a *C Series* which closer matches the *A Series* in size, though I don't think I've encountered it. They're all a part of the same [ISO 216 standard](https://en.wikipedia.org/wiki/ISO_216).

My introduction to ANSI/US Letter was via our first inkjet printer, albeit indirectly. I assumed our printer was wonky or misaligned, on account of the bottom being truncated, and text not being centred on the page. I got into the habit of adding additional right and footer margins to "fix" it. It's amusing to me now that I had the technical skills as a kid to mess with margins, but not to select A4 from a dialog box!

*(Our second printer coincided with an upgrade to Windows 98 SE, which must have been configured with the correct locale and printed to A4 by default. That was an exciting birthday for an office supplies fan).*

Years later I bought something from eBay in the US, and it came with an invoice printed on a sheet of paper that didn't look quite right. Putting it against a sheet of "normal" paper, this one looked shorter and wider. A quick search lead me to this whole other world of American paper sizes.

**I'll admit, I liked it.** The dimensions and ratio made no mathematical sense to someone not also used to inches, furlongs, and measuring things by counting chickens or something (it makes more sense than Celsius because poultry doesn't need a decimal point!), but it was visually pleasing. I can't explain it, but US Letter *looks* right.

My favourite paper size, weirdly enough, is Japanese B5. I got some strange looks for it at university and the last couple of years of high school, but most of my notes were taken in Campus B5 notebooks instead. To this day I prefer it; it's just wide enough for a good mind map, and because I can carry books of that size in a backpack without the corners getting snagged on the zipper (heavens). It makes me wonder if a US Letter notebook wouldn't have that issue, and if these backpacks were designed with them in mind. Being a bit more squat, and a bit wider, would probably fit perfectly.

The world has moved on from archaic paper sizes and measurements, and the US should definitely follow the lead of their scientific community by adopting what the rest of us have used for decades. *But...* I do admit US Letter is shockingly appealing in a way I can't explain.

One day the so-called *Paperless Office* will come around, which will negate the discussion. Or thanks to PDFs and word processors... will it?
