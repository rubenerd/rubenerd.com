---
title: "Using scp(1) to transfer multiple files"
date: "2023-01-05T10:21:21+11:00"
abstract: "Today’s instalment of things you already know, unless you don’t."
year: "2023"
category: Software
tag:
- security
- things-you-already-know-unless-you-dont
location: Sydney
---
In today's instalment of [things you already know, unless you don't](https://rubenerd.com/tag/things-you-already-know-unless-you-dont/), you can use wildcards in scp(1) when transferring files:

	$ scp user@host:file-* .

This came up during a customer call last year, where the engineer was running it a few times. On the backend it's [sftp(1) now anyway](https://www.openssh.com/txt/release-9.0), as it probably should be.
