---
title: "Michał on the joys of closing tabs"
date: "2023-05-07T10:12:16+10:00"
abstract: "Mobile OS tab management is a massive pain, as he describes."
year: "2023"
category: Software
tag:
- accessibility
- android
- design
- feedback
- ios
location: Sydney
---
Michał of the [d-s] blog commented on my post earlier this week about keeping tabs and applications closed.

[d-s: Keeping Browser Tabs to a Minimum](https://d-s.sh/2023/keeping-browser-tabs-to-a-minimum/)   
[Rubenerd: Keeping computer stuff open or closed](https://rubenerd.com/keeping-computer-stuff-open-or-closed/)

He agreed with me about the anxiety-inducing state of having hundreds of web tabs and applications open, though raised a good point about how the calculus is entirely different in mobile environments:

> On iOS, you are penalized for closing an app, as a cold start is much slower than waking from hibernation. And even if you close it, the app can still track you. So disabling all background activity for all apps seems to be a must.

And mobile web browsers? *Forget about it!*

> There is no tab management. I constantly have over 100 tabs open because there is no easy way to close them. There is no tab bar, so I can’t see them. There is no quick way to close multiple, so every few weeks I declare bankruptcy and use the one button keeping all this madness - close all tabs. I waste resources because the tooling is not there.

iOS on the iPad (iPadOS? I lose track of what they call these things) is marginally better in this regard, with a tab bar and other visual cues. But you still end up with a bunch of stuff open. It unloads pages to save memory, but you're still stuck with a wall of pages which is a cognitive load. There's a reason I use the iPad as a glorified colour eBook and magazine reader, and my phone as little as possible.

For a while I ran Firefox Focus which explicitly doesn't support tabs, but I ran into too many limitations when trying to juggle things, especially when travelling. I haven't tried bookmark syncing with the other Firefox app, but using the Pinner iOS app to send pages to my Pinboard has been useful way to keep them at a manageable level. Sometimes on a long commute or while waiting for things, I'll swipe through and pin a few dozen tabs to clear them out.

At some point I realised that having a clean slate, bookmarks, and as few applications and tabs open as possible is not how most of the world works. Which means application vendors don't prioritise it as a use case. It'd be overstating the case that I think using computers that way is *healthier*, but it certainly makes using these things better for me.

