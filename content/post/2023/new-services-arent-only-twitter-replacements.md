---
title: "New services aren’t only Twitter replacements"
date: "2023-09-15T08:16:33+10:00"
abstract: "A comment from Tedium about Twitter not really pulling in new people got me thinking."
year: "2023"
category: Internet
tag:
- mastodon
- social-media
location: Sydney
---
[Tedium discussed a new social network in May](https://tedium.co/2023/05/31/picnic-social-network-profile/)

> Today in Tedium: One thing I’ve noticed with the recent social media diasporas that have emerged in the wake of Twitter’s decline is that the people who make up those diasporas don’t really seem to be pulling in new groups of people so much as giving the types of people that are already addicted to Twitter a new place to go.
>
> We are not bringing in new members of the club, for the most part—we are instead doing the social media equivalent of a ’90s revival night at the local hipster dance club.

I don't think that's true. I've seen people attempt to "revive" their circle elsewhere, but there are social and technical reasons why it usually doesn't happen, and why new communities invariably have a different mix of people.

The Network Effect is the one we all know about. It's unusual to lift and shift an entire community or group wholesale onto another platform, even if the tech is there to make it happen. This underscores the hidden truth to so many of our problems: they're not technical. As long as entrenched platforms exist, it's rare to move an entire community intact.

But even if *everyone* in your extended group lifted and shifted elsewhere,the far more interesting reason why you don't end up with a revival is that every social network has its own dynamic. Again, this is as much to do with social trends as it is technical features or limitations.

Mastodon is a useful case study here. The system bears a superficial similarity to Twitter, but its architecture, lack of advertising, longer character limits, content warnings, optional search and RSS, and the ability to self-host yield a fundamentally different experience. I have many former Twitter friends in my Mastodon circles, but there are plenty of new people too that have sprung up without my active involvement. 

Different sites attract and retain different groups of people. I consider that a positive, especially if you're coming from a sinking ship like Twitter.

