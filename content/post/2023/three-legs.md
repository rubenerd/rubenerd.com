---
title: "Three legs"
date: "2023-08-12T09:17:31+10:00"
abstract: "They passively balance, damn it!"
year: "2023"
category: Thoughts
tag:
- pointless
- pontifications
location: Sydney
---
Have you ever wondered what it'd be like to have three legs?

Tripods, and stools with three legs, are amazing. They passively balance, something that two and four legs can't do. I'm typing this on a coffee shop table right now that's rocking back and forward because one of the legs is too short.

How much of our subconscious mental energy is wasted balancing our awkward bodies on two legs? As any toddler or Data from TNG could appreciate, it's likely a lot. We decided millennia ago that it was more useful for us to walk around like this than on all fours, but it sure came with some compromises.

I suppose the space we'd take up would closer resemble a triangle instead of a rectangle with three legs, which would make queues and public transport tricky. And how would one sit with an extra leg? Would it hang out in front, or would we need an extra slot in the back?

The universe is unfathomably large and old, so somewhere out there either has, or had, three legs. They'd likely see us and think *wow, how do they even balance? That's a superpower!* 
