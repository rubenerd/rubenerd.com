---
title: "Alter releases Love Live! figs"
date: "2023-04-27T11:14:42+10:00"
abstract: "They’re my favourite fig manufacturer, and this one of Watanabe You is excellent!"
thumb: "https://rubenerd.com/files/2023/alter-watanabe-you@1x.jpg"
year: "2023"
category: Anime
tag:
- anime-figures
- idols
- love-live
- watanabe-you
location: Sydney
---
Clara and I saw posters for these latest *Love Live!* figs everywhere in Den-Den Town in Osaka, and they look amazing. Alter has been my favourite anime fig manufacturer ever since the Suzumiya Haruhi days, so it was no surprise they were behind these as well.

This is the press image of Watanabe You, one of my two favourite *Love Live!* idols. The posters there showed a lot more detail, but you get the idea. And *o-only* ¥28,380! You have two kidneys, right?

<figure><p><img src="https://rubenerd.com/files/2023/alter-watanabe-you@1x.jpg" alt="" srcset="https://rubenerd.com/files/2023/alter-watanabe-you@2x.jpg 2x" /></p></figure>

The base seems to have been the biggest criticism of this fig series, but I prefer something simple. When you have a row of these things (which you should definitely not do, for financial and space reasons), I think complicated and larger bases only make things more difficult.

Naturally I'm most excited for her hat. The ribbon alongside it is a nice touch!
