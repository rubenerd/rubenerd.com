---
title: "FPS and resolutions we’re used to"
date: "2023-07-21T08:54:17+10:00"
abstract: "A streamer complaining about not being at 60 FPS was a boomer moment for me."
year: "2023"
category: Media
tag:
- hakos-baelz
- hidpi
- resolution
- video
location: Sydney
---
Along with giving me more hope for the future than my Millennial generation managed, Zoomers have also been helpful for questioning some assumptions I didn't even realise I was making.

I was watching a Hakos Baelz stream recently, and she realised midway through her introduction that her framerate had been set at 24. She voiced genuine surprise, made a change to OBS, and began streaming at 60 FPS. Based on the feedback from chat, the relief was immediate and overwhelming for everyone involved.

[Hakos Baelz Ch. hololive-EN](https://www.youtube.com/@HakosBaelz/streams)

But here's the thing: *I hadn't noticed a problem!* While 60 FPS did look buttery smooth, 24 FPS was absolutely watchable to my eyes. It might have even looked more *normal*... well, as normal as watching a cute motion-capture anime character illustrated by Mika Pikazo voiced with an Australian accent can be. *Bruh.* 🧀

This is a well-understood phenomena. Those of us who grew up at the tail end of PAL/SECAM and NTSC broadcasts are used to sub-30 FPS video. I read a line in someone's blog recently (apologies, I couldn't remember where) that they'd shot a sequence at 23.976 FPS to mimic the appearance of classic film they loved. That's 2.6× slower than what people were crying out about on that stream.

I remember the first time I saw a 60 FPS TV broadcast: I was sitting at the Boat Deck Cafe in Adelaide, and the owner had turned on his new wall-mounted panel. I remember thinking how *uncanny* all the footage looked, as though I was watching hyper-realistic 3D renderings of people and scenery rather than the real thing. Because, to me, the "real thing" was what people looked like moving around at 25 FPS PAL. I don't remember specifics from that time, but I remember the unease vividly.

Fast-forward to 2023, and I love 60 FPS (or 59.94, or whatever it is). You get so much more information and clarity, especially from streamers who are typing on a console or playing a game. It makes intuitive sense in that context, given we're used to 60+ FPS on our monitors. But I'll even watch live action at that framerate and appreciate the extra level of detail.

The interesting thing for me is how this differs from my perception of resolution. I started using Retina 2× HiDPI monitors more than a decade ago, and I can't overstate just how **dreadful** 1080p panels and 1.5× HiDPI laptops look to me now. Their grainy image texture and low-resolution text look like I've rubbed sand in my eyes, or forgot to clean my glasses. The fact PC manufacturers outside Apple still think these crappy resolutions are appropriate floors me, as I've talked about here in the past.

Point is, our eyes are weird. But also unique! Uniquely weird.
