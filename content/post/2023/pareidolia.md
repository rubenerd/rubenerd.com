---
title: "Pareidolia and pattern matching"
date: "2023-03-28T18:10:42+11:00"
abstract: "Pareidolia is the tendency for perception to impose a meaningful interpretation on a nebulous stimulus"
year: "2023"
category: Thoughts
tag:
- language
location: Sydney
---
You know that feeling when you see a book online or in a store, and you buy it because it looks interesting, only for it to sit on a shelf or your ebook list forever? I've been *plowing* through them lately, and turns out that Ruben guy was right, these were interesting!

Two entirely unrelated books, one about German philosophy, and another about what a glorious con Bitcoin is, mentioned the phenomena of *Pareidolia*. It sounded more like a small European flower than a psychological phenomena, but I wanted to check it out.

Wordnet didn't list anything, curiously:

	$ wn pareidolia
	==> No information available for noun pareidolia

[But Wikipedia to the rescue](https://en.wikipedia.org/wiki/Pareidolia)\:

> Pareidolia is the tendency for perception to impose a meaningful interpretation on a nebulous stimulus, usually visual, so that one sees an object, pattern, or meaning where there is none.

The article cites the following examples:

> [P]erceived images of animals, faces, or objects in cloud formations, seeing faces in inanimate objects, or lunar pareidolia like the Man in the Moon or the Moon rabbit. The concept of pareidolia may extend to include hidden messages in recorded music played in reverse or at higher- or lower-than-normal speeds, and hearing voices (mainly indistinct) or music in random noise, such as that produced by air conditioners or fans.

It sounds like it's related to our compulsion to match patterns.

I suppose the opposite of pareidolia would be stenography, where patterns and messages are intentionally hidden so as to not be perceptible. Or where there's a legitimate pattern that people can't see because they haven't been exposed to it before, or refuse to see because it doesn't conform to their internal biases or worldviews.

I *love* patterns. I think seeing a pattern that isn't there, and exploring how and why people perceive them, is just as interesting as the patterns themselves.
