---
title: "The OSs I use most often"
date: "2023-01-26T15:20:36+11:00"
abstract: "macOS, Linux, FreeBSD, Windows Server, Windows desktop"
year: "2023"
category: Software
tag:
- bsd
- freebsd
- lists
- netbsd
- macos
- windows
- windows-11
location: Sydney
---
I've waxed lyrical about my operating systems I *prefer* to use, including the BSDs and plenty of ancient systems for nostalgic pointlessness. But what about what I actually run most often on a daily basis for work and personal activities?

It turned out to be an easy exercise to quantify and sort them, though the order did surprise me a little:

1. macOS
2. Linux
3. FreeBSD
4. Windows Server
5. Windows 11

To clarify, this only accounts for machines to which I have direct access. I suspect Linux would shoot to the top if I included systems I use indirectly, including most web services. I have a 70/30 love/hate relationship with Tux, but there's no question he's taken over the world.

FreeBSD's lower placement is its own fault. My FreeBSD virtual servers, homelab gear, and work machines truck along with very little active effort most of the time, thanks in no small part to OpenZFS and periodic scrubs. "Solid" is a qualitative metric that's hard to define, but it's how it feels.

Still, I'm disappointed in myself that I didn't get NetBSD, DOS, and some 8-bit systems higher.

