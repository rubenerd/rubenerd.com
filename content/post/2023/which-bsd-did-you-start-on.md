---
title: "Which BSD did you start on?"
date: "2023-09-12T08:36:14+10:00"
abstract: "Mine was Red Hat Linux. Wait, hear me out!"
year: "2023"
category: Software
tag:
- bsd
- freebsd
- illumos
- linux
- netbsd
- red-hat-linux
location: Sydney
---
This post is dedicated to Benedict Reuschling, who encouraged me to write about this again! Danke :).

I saw this question floating around the socials recently, and it got me thinking where I started. It was tricker to answer than I thought, though I was able to trawl through the archives to find some info.

My first BSD was **Red Hat Linux**. You read that right: practically every Linux distribution has BSD-licenced code, and they depend on tooling like OpenSSH from the OpenBSD project for secured administration. I like to point out examples like this whenever a GPL advocate claims their licences are necessary to ensure freedom. Pity that importing into a GPL'd project is a one-way transaction.

[2009: Installing Linux for the first time](https://rubenerd.com/first-linux-install/)

Taking a *step* closer to what the question really meant, my first BSD-like OS was **Darwin** in its Aqua clothing. Like FreeBSD upon which its userland was based, early versions of Mac OS X defaulted to **tcsh(1)** for user shells. I got pretty good at scripting in it, despite the indignant suggestions such actions were considered harmful! I've been on **oksh(1)** for the last few years, but I still harbour tremendous affection for the C Shell family.

[2010: tcsh telling me DING!](https://rubenerd.com/tcsh-ding/)

**ksh(1)** is a nice seque into **NetBSD**; I believe 2.0? It was the first BSD OS I installed myself from scratch, because their PowerPC port was so easy to get running on my iBook G3. It's where I also learned about pkgsrc, which I still run to this day on systems I administer. NetBSD remains a staple on my vintage computers, including a SPARCStation and my beloved Pentium 1, the first PC I built myself as a kid. It also runs a cloud server I maintain for personal projects.

[2009: Trying pkgsrc on my MacBook Pro](https://rubenerd.com/testing-pkgsrc-mbp/)   
[2010: My HiME NetBSD desktop background whatnot](https://rubenerd.com/p2764/)


The first **FreeBSD** version I ever tinkered with was 6.1, when I got it running in a series of Parallels and VMware Fusion VMs on my then-new Intel MacBook Pro. Much of the early history of my blog detailed my adventures learning about open source desktop environments, window managers, Xorg, and building a FreeBSD desktop. It was a lot of fun.

[2006: Parallels Desktop FreeBSD issues](https://rubenerd.com/parallels-desktop-freebsd-issues/)   
[2006: Haruhi dancing on FreeBSD?](https://rubenerd.com/haruhi-suzumiya-on-freebsd/)   
[2007: FreeBSD boleh!](https://rubenerd.com/freebsd-boleh/)

Funnily enough, it wasn't until the FreeBSD 7.x branch when I first installed it on a server, at the time an old Athlon XP I used as a Netatalk box. Am I the first person to come to FreeBSD in serverland via desktops instead of the other way round? Not sure!

I've tinkered and build Dragonfly BSD and OpenBSD systems out of curiosity since, though I'll admit I've spent more time on the OpenSolaris-derived illumos since. It's long time I give them a proper try too.
