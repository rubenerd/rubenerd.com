---
title: "The Micro 8088, and my dream pedestel machine"
date: "2023-10-22T09:19:21+11:00"
abstract: ""
thumb: ""
year: "2023"
category: Hardware
tag:
- ibm
- ibm-8088
- retrocomputing
- single-board-computers
location: Sydney
---
I just [found this project](https://github.com/skiselev/micro_8088 "The Micro 8088 repo on GitHub") via an [article in Hackaday](https://hackaday.com/2023/10/20/finally-an-open-source-8088-bios/ "Finally, an open source 8088 BIOS"), and it looks amazing. Skiselev has even built an [8088 BIOS](https://github.com/skiselev/8088_bios "skiselev’s 8088 BIOS repo on GitHub").

> Micro 8088 is an easy to build IBM PC/XT compatible processor board. It uses a fairly common Faraday FE2010/FE2010A chipset, that implements most of IBM PC/XT LSIs (Intel 8xxx ICs) and glue logic. Micro 8088 uses SRAM ICs to implement the system RAM, and a Flash ROM IC to store the BIOS, further reducing the number of components, and simplifying the build process.

Here's a photo of a completed board, showing its 8-bit ISA connector:

<figure><p><img src="https://rubenerd.com/files/2023/8088-board@1x.jpg" alt="Top-down photo of a populated Micro 8088" style="width:500px; height:375px;" srcset="https://rubenerd.com/files/2023/8088-board@2x.jpg 2x" /></p></figure>

I continue to be floored by the creativity, ingenuity, and dedication of engineers in this field. People like skiselev aren't just building a homage to a retro computers, they're keeping computer history alive, and making it more accessible to everyone. I'm wearing one of my flatcaps right now, and I'm tipping it in his general direction :).

Projects like this make me want to realise my dream of getting one of those monster AT pedestel cases, and filling it with single board computers, either those industrial ones, or modern boards like this. Imagine all the history you could have in *one* case?

The trick would be to have a switching mechanism on the backplane to select which card you want active, otherwise you'd have them all power up at once and cause all sorts of resource conflicts. I'm no electrical engineer, but I *feel* like this should be possible. Maybe you'd need to build your own backplane with isolated ISA slots, instead of a shared bus?
