---
title: "Don’t wash your Commodore 64C too hot"
date: "2023-08-20T09:38:21+10:00"
abstract: ""
thumb: ""
year: "2023"
category: Thoughts
tag:
-
location: Sydney
---
This is a cautionary tale as much as a shaggy dog story. But overall is an item of clothing, and I'm happy with how things turned out.

<figure><p><img src="https://rubenerd.com/files/2023/bearcat-64c-c128@1x.jpg" alt="My 64C atop my C128, featuring Clara and a Bear Cat." style="width:500px;" srcset="https://rubenerd.com/files/2023/bearcat-64c-c128@2x.jpg 2x" /></p></figure>

### Cory Wong is awesome funk, but this was not

I've been lucky that most of my second-hand vintage computers didn't arrive smelling awful. Oftentimes these machines meet ignominious ends for many years, as many of you can attest if you've rummaged through your attics. If it's not mildew or mould, it's something worse.

This streak of luck ended with the 1987 Commodore 64C I bought last December. I downplayed it in my post, but the smell hit me as soon as I opened the shopping box. It was a delightful (cough) concoction of wet cardboard, burnt paper, and mildew.

[Cleaning and retr0brighting a Commodore 64C](https://rubenerd.com/retrobrighting-a-commodore-64c/)

I pulled the machine apart, scrubbed the case with isopropyl alcohol, retr0brighted away some of the yellowing with some developer creme, and finished with some 303 Automotive plastic restorer. This made the machine look practically brand new, and got rid of most of the stink.

*(I remember feeling decidedly out of place buying hair developer at a pharmacy with my clearly limited capacity to use it, and automotive plastic spray from an automotive store. I doubt either shop keeper would have cared that I was buying these products to restore an old computer, but maybe they'd think it was funny).*

Unfortunately, use of this machine since proved decidedly aromatic. As the machine warmed up from use, the unpleasant elixir of chemicals that permeated the plastic seeped out like a cheap motel. It made me not want to use it.


### "Cleaning windows" ~ Van Morrison

This weekend I finally decided to do something about it. This was to be a last-ditch attempt to rid this machine of its scented ghosts, given I'd already thoroughly scrubbed and cleaned it at least twice before. If I couldn't get it clean, I decided to give myself permission to replace the case entirely.

I pulled the machine apart again, gently removed the power LED from the top panel, and placed both case pieces in a large tub of warm water with some detergent for some steamy surfactant action. *Chica bow wow*.

<figure><p><img src="https://rubenerd.com/files/2023/c64-bath@1x.jpg" alt="Photo of the C64 midway through a rotation in its bubble bath" style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2023/c64-bath@2x.jpg 2x" /></p></figure>

I was not prepared for what I would see an hour later. *Turbidity* is a great word, not just for its succinct description of cloudy water resulting from suspended matter, but what disgusting material the *sound* of the word evokes. I'll spare you the details beyond *brown*, but my jaw dropped at how repulsive it looked... and *smelled!*

But this was a good sign, right? I ran cold water over the panels, then let it soak again in warm water for the rest of the afternoon. Once again, the water looked disgusting, but it got progressively clearer with each soaking and scrub.


### "You're too hot to touch" ~ Ben Sidran

I left the panels to dry overnight, and was thrilled to discover they didn't have any smell whatsoever. The plastic also looked a little less yellow than before, suggesting that that retr0bright wasn't completely effective before on account of staining of another kind.

Unfortunately, this was where I realised my mistake. One of the washings must have been too warm for the plastic, which resulted in dozens of bubbles and streaks forming on the surface. A couple of large ones near the function keys were especially obvious:

<figure><p><img src="https://rubenerd.com/files/2023/c64-streaks@1x.jpg" alt="Close-up photo showing the streaks and bubble marks on the plastic. Damn." style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2023/c64-streaks@2x.jpg 2x" /></p></figure>

Clara inspected some of the photos taken before the washing, and suggested that a few of the marks existed before, as though a more thorough washing dislodged some embedded grime or dirt. There might be something to this, though they looked more noticeable in concert with the other marks I introduced.

Remembering a trick I learned from Techmoan, I applied a rice-grain sized blob of Brasso to a microfibre cloth, and was able to rub off all but the deepest accumulated bubbles and streaks. This had the effect of removing some of the texture from the plastic, though I preferred that to having a case that looked like it'd gone through a kiln.


### "Alive again!" ~ Chicago

This 64C has been up and running again for a while, and it passes the literal sniff test with flying colours. It has that oddly comforting smell of old electronics, not smoke or mould! Whatever crap was in that plastic, it's been pulled out and consigned to our shower. Whoops, I'd better clean that too.

I'll admit, I was upset at ruining the surface of the case in places. Like my Commodore 16 keycap misadventure, I feel as though I'm the custodian of computer history when restoring and cleaning these devices, and I feel pangs of guilt when I fuck things up. But the reality is, the case was already compromised with this smell before I got my hands on it. It might have needed to be that hot to get the smell out, which is why my previous case cleanings failed.

But this is a lesson learned. If you're going to clean forty-year old computer plastic, test it with lukewarm or cold water first, and maybe test it on the bottom piece first. If it's worth doing, it's worth doing right.

<figure><p><img src="https://rubenerd.com/files/2023/c64-keys@1x.jpg" alt="Photo showing me reassmbling the keyboard after putting the machine back together." style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2023/c64-keys@2x.jpg 2x" /></p></figure>

Now if you'll excuse me, I'm off to reattach all these keys, then fire up some 8-bit pistons in *Flight Simulator II*.
