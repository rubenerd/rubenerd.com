---
title: "Connecting a CD-ROM to a Sound Blaster 32 IDE header in DOS"
date: "2023-06-13T18:30:59+10:00"
abstract: "It’s possible if one downloads the SBIDE.SYS driver."
year: "2023"
category: Hardware
tag:
- am386
- guides
- retrocomputing
- hard-drives
- sound-cards
location: Sydney
---
Back in April I talked about connecting my Am386's CD-ROM to the IDE header on an ESS AudioDrive ISA sound card, and using it in DOS. I wanted to see if I could repeat this with a Creative Sound Blaster 32 that also has an IDE controller. It worked!

[View all Am386 posts](https://rubenerd.com/tag/am386/)   
[The ESS AudioDrive ES1868F for sound and IDE](https://rubenerd.com/the-ess-audiodrive-es1868-for-sound-and-ide/)


### Warnings before we start

Before attempting this yourself, **check your Sound Blaster card really has IDE**. Earlier Creative cards had proprietary CD-ROM headers that *look* like IDE, but are electrically incompatible. You could damage your drive or precious card connecting an IDE drive to a non-standard header.

Look for a silk-screened notes around the connector that says "IDE", **not** "Panasonic" or "Sony" for a proprietary drive header. Better yet, search the model number of your card and confirm.


### Preparing the card and drive

The Sound Blaster 32, AWE32, and AWE64 are Plug-and-Play cards. On DOS, you load them in this order:

1. Creative's PnP detection tool **CTCM.EXE**
2. The **SBIDE.SYS** driver for the IDE controller
3. **MSCDEX.EXE**, or equivalent CD-ROM driver

The Creative website miraculously still has the first two as of 2023. In case this goes down, do web searches for the following self-extracting archives:

[Creative Downloads: SBIDE20.EXE](https://support.creative.com/downloads/searchdownloads.aspx?filename=SBIDE)   
[Creative Downloads: CTCMBBS.EXE](https://support.creative.com/downloads/searchdownloads.aspx?filename=CTCMBBS)

You can run these installers directly in DOS, but I've only had mixed success with them working. I use **UNZIP** to extract them on my FreeBSD machine or Mac first to get the files I need, and put them in **C:\DRIVERS**.

And finally we need **MSCDEX**. This comes in **C:\DOS** in PC DOS 7/2000, or you can get it on a Windows 9x boot disk. Alternatively, you can use SHSUCDX as a drop-in replacement that uses less memory:

[SHSUCDX, by Jason Hood](http://adoxa.altervista.org/shsucdx/)


### Find your card's IDE settings

The **CTCMBBS.EXE** archives includes **CTCU.EXE**, which you can use to interrogate the config of your Sound Blaster card. You can use arrow keys, but its easier if you load a mouse driver first.

1. Run **CTCU.EXE**
2. Click the **Menu**, and click **PnP Cards**
3. Click **IDE** under **List of Devices**
4. Click the **Resources** button.

Under **Settings**, you should see three lines such as this:

    I/O Range 1e8-1ef
    I/O Range 3ee-3ef
    Interrupt 11

Make a note of these settings. Press **ESC** twice, then choose **Exit** from the **Menu**.


### Configuring the drivers

In **CONFIG.SYS** we define **LASTDRIVE** so we have a spare letter to assign to the CD-ROM. We run the **CTCM** driver, which doesn't need to be loaded high because it quits once its finished. Then we load the **SBIDE.SYS** IDE driver. Note the **/P** flag uses the start of the first I/O range above, then the Interrupt, then the second I/O range.

    ;; CONFIG.SYS
    LASTDRIVE=E
    DEVICE=C:\DRIVERS\CTCM.EXE
    DEVICEHIGH=C:\DRIVERS\SBIDE.SYS /D:JIMKLOSS /P:1E8,11,3EE /V

And then **AUTOEXEC.BAT**. Use the same drive label you defined using **/D** above.

    :: AUTOEXEC.BAT
    SET SOUND=C:\DRIVERS
    SET CTCM=C:\DRIVERS
    :: Uncomment if using Windows 3.x
    :: C:\DRIVERS\CTCU.EXE /S /W=C:\WINDOWS
    C:\DRIVERS\MSCDEX.EXE /D:JIMKLOSS /L:E /M:15 /V


### Reboot and test

If all goes well, you should see output like this:

    Creative CD-ROM Device Driver Version 1.22
    Copyright (C) Creative Technology Ltd,1994-97. All rights reserved.
      Transfer Mode      : Programmed I/O
      Number of drives   :
      Drive    0:  Port=  1E8  IRQ= 11 CD-ROM CRN-8241B 2.23
       
    MSCDEX Version 2.24
    Copyright (C) IBM Corp. 1986-1994. All rights reserved.
         Drive D: = Driver JIMKLOSS unit 0

Or this if you use **SHSUCDX**:

    SHSUCDX installed.
      Drives Assigned
    Drive  Driver   Unit
      D:   JIMKLOSS   0

Now you can access!


### Troubleshooting

It sounds obvious, but if the drive isn't detected, try re-seating the IDE ribbon cable on the card, or swap it out. I wasted hours troubleshooting before realising I was using a crappy cable, and those old sound card IDE connectors can be flaky AF.

**CTCU.EXE** is your friend for troubleshooting problems. If it doesn't list your card at all, it might not be connecting properly to the ISA bus. Grab a can of DeoxIT or similar deoxidising solution, spray the slot, and clean the pins on the card. I know I know, you don't *expect* it to be the reason, but this stuff is getting old.

If the card is detected, try using **CTCU.EXE** to set a different Interrupt and I/O addresses, then update **CONFIG.SYS** to reflect this. I've read that certain boards don't like the default settings.

If nothing works, it might be worth throwing in the towel and using a dedicated IDE controller with two interfaces. I get a kick out of retrocomputing, but I'm not above solving it with a new card so I can do other things.
