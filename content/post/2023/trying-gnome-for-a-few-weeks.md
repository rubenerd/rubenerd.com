---
title: "Trying the GNOME desktop for a few weeks"
date: "2023-09-27T09:35:09+10:00"
abstract: "It mostly works, though I miss menus."
thumb: "https://rubenerd.com/files/2023/gnome-again@1x.jpg"
year: "2023"
category: Software
tag:
- bsd
- fedora
- freebsd
- linux
- netbsd
- gnome
- reviews
location: Sydney
---
Keen-eyed readers among you might have noticed my tribute to Bram Moolenaar was wrapped in GNOME window dressing. I haven't made the switch, but it was fun experimenting again with what people on the other side of the fence use.

[Goodbye, Bram Moolenaar ♡](https://rubenerd.com/goodbye-bram-moolenaar/)

By way of context, is a phrase with four words. Much of my blog from the early-2000s was spent chronicling my adventures with various desktop environments and window managers on FreeBSD, NetBSD, and a smattering of Linux distros. I ended up sticking with the delightful Xfce for the longest time, before recently switching back to KDE Plasma.

My current Ryzen desktop is dual-booted with FreeBSD and Linux, so I thought I'd try installing Fedora Workstation with GNOME during a system rebuild. This is what you see below, with an open Nautilus file manager, Firefox, GNOME Terminal, and Minecraft.

<figure><p><img src="https://rubenerd.com/files/2023/gnome-again@1x.jpg" alt="Screenshot of GNOME showing four open window previews over a desktop picture of Hatsune Miku, a system menubar along the top, and a horizontal dock of icons along the bottom." srcset="https://rubenerd.com/files/2023/gnome-again@2x.jpg 2x" /></p></figure>

GNOME and KDE are the two biggest desktop environments for \*nix, and have very different philosophies. GNOME is arguably the more Mac like, with its minimal interface, dock, task switcher, and system bar along the top where it belongs. While I appreciate the customisation KDE offers, I will admit GNOME mostly looked great for me from the start.

[2011: Ruben's review of GNOME 3](https://rubenerd.com/review-of-gnome-3/)

I'm actually surprised how little the interface seems to have changed since GNOME 3 controversially entered the scene last decade. The desktops and dock are aligned horizontally, and elements of the UI have been flattened, but otherwise it was very familiar. I suppose KDE 4+ has remained similar as well, albeit with its new Breeze theme.

[GNOME Tweaks on GitLab](https://gitlab.gnome.org/GNOME/gnome-tweaks)   

I found myself changing little about the desktop, though GNOME Tweaks should still be considered mandatory so you can replace that ugly Cantarell font that's still everywhere in the UI. Swapping to Nimbus Sans (my personal favourite) or Liberation Sans makes a huge difference to legibility, especially in smaller sizes.

Unfortunately, I'm also disappointed to see Nautilus and other GNOME bundled software use the hamburger icon, with no option to enable a menubar like you can on KDE or Xfce. This is where the drive for minimalism crosses the line into being an accessibility issue.

Other than that, there wasn't much more to report. It mostly got out of the way and worked, which I could appreciate. The UI is simple, and I got into the habit of throwing the cursor into the corner to see all my open windows. The main things I missed were my memorised KDE shortcuts, Dolphin file manager layout, and those aforementioned menus. My daily Qt applications like Kate also integrated well, despite coming from KDE land.

I should do a post about why I love Plasma so much, and why I still lean towards Xfce for a GTK desktop. But in the meantime I'd rate the current version of GNOME (as ships in Fedora 38) three feet out of a possible five, which last time I checked is just shy of a metre.
