---
title: "Ride sharing and autonomous vehicles cause traffic"
date: "2023-08-03T08:41:49+10:00"
abstract: "These services were billed as solutions to traffic, but they were anything but."
year: "2023"
category: Thoughts
tag:
- public-transport
- urbanism
location: Sydney
---
Having largely lost the debate over whether cars are a good thing in urban areas, car-centric planners have been trying to figure out how to continue incorporating them in new and innovative ways. *Hyperloops!*

Massive road tunnels were the *in vogue* approach from the 1990s. City and state governments have poured billions into digging and covering up their traffic problems, rather than solving them with investment in public transport. Alan does a great video about this in the context of the Boston Big Dig, though it could as easily apply to all the recent road works in Sydney as well.

[Alan Fisher: The Tunnel of Failure](https://www.youtube.com/watch?v=d5pPKfzzL54)

Futurists have since been looking at less drastic tweaks to our existing road systems, given the increasingly negative public attention these massive projects have garnered. It's hard to see the value when you're in gridlock underground.

One solution the Valley offered was ride sharing. MIT modelled in 2016 that ride sharing services would reduce traffic from a pool of existing drivers; we'll touch on that shortly. Bruce Schaller also published a paper using evidence from Uber and Lyft (wait, what?) to make similar claims in 2017.

[MIT: Ride sharing can improve traffic](https://news.mit.edu/2016/how-ride-sharing-can-improve-traffic-save-money-and-help-environment-0104)   
[Science Direct: Can sharing a ride make for less traffic?](https://www.sciencedirect.com/science/article/abs/pii/S0967070X20309525)

Around the same time, autonomous vehicles were offered as another solution. Veritasium made a video about Waymo's self driving car project in Phoenix which, he argued, could reduce traffic. Tom Nicholas produced a great rebuttal that I've linked to here before, which is worth checking out.

[Veritasium: Why you should want driverless cars](https://www.youtube.com/watch?v=yjztvddhZmI)    
[Tom Nicholas: When Science Meets Misinformation](https://www.youtube.com/watch?v=CM0aohBfUTc)

Implicit in all these studies and video advertisements is that ride sharing and autonomous vehicles will reduce traffic, because one car is potentially taking a few more off the road. But this car-centric perspective strikes me as a myopic view of the overall problem with urban transport, and is` entirely self-defeating in the real world.

Just as those mega tunnels siphon money from public transport projects, Smart Cities Dive reported on a study that concluded ride sharing services predominately competed with public transport, not other cars. The Wall Street Journal has since sounded the alarm as well, with those same ride sharing services from the late 2010s now admitting they contribute to traffic, not solve it.

[Katie Pyzyk: Ride-sharing increases, not reduces, traffic](https://www.smartcitiesdive.com/news/report-ride-sharing-increases-not-reduces-traffic/528659/)   
[WSJ: The ride sharing utopia that got stuck in traffic](https://www.wsj.com/articles/the-ride-hail-utopia-that-got-stuck-in-traffic-11581742802)

This is what city planners are referring to when discussing *induced demand*, and it's proof that building car infrastructure in a growing city never solves traffic, it only contributes to it. What's that line about the angry motorist who doesn't realise they **are** traffic?

Cars will always have a place in urban areas, but fancier car allocation, regardless of fuel source or operator, won't drive us out of our current problems.
