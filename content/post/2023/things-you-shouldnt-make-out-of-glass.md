---
title: "Things you shouldn’t make out of glass"
date: "2023-04-27T14:17:22+10:00"
abstract: "Noodles."
year: "2023"
category: Thoughts
tag:
- lists
- pieces-of-eight
location: Sydney
---
* Jaws and/or jaw breakers
* Space-lift tethers
* Carpet
* Printed copies of the *FreeBSD Handbook*
* Bathroom walls in hotel rooms
* InfiniBand connectors
* Pool and/or buckwheat noodles
* Mobile phones

And a bonus item:

* Is a sentence with four words

[Archive of other pieces-of-8 posts](https://rubenerd.com/tag/pieces-of-8/)
