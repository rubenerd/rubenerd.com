---
title: "Researching if Commodore’s PCs were profitable"
date: "2023-05-02T13:57:19+10:00"
abstract: "It’s a firm maybe."
thumb: "https://rubenerd.com/files/2023/commodore-pc30@1x.jpg"
year: "2023"
category: Hardware
tag:
- commodore
- commodore-pc
- retrocomputing
location: Sydney
---
Commodore, better known for their legendary PET, C64, and Amiga computers, also made PC clones during the 1980s. While largely unremarkable in a crowded market dominated by the likes of Compaq and Tandy, their mere existence on the retrocomputing timeline fascinates me. Learning of them a few years ago was like discovering DEC also made Intel-based workstations. It just... it doesn't *compute*.

[Jan Beta's PC 10-II video provides a great summary, edited for brevity](https://youtu.be/q7Fe1I6mwkE)

> The German branch of Commodore decided, quite in secret really (they didn't inform the US headquarters, though they were free to develop their own products at that time) to develop a line of PCs. They thought the PC market would be a good fit, given they previously made business machines: hence the name **Commodore Business Machines**. Commodore decided to make a more affordable clone of the IBM PC.

Most Commodore retrospectives ignore their PC clones entirely, or consign them to footnotes. There just doesn't seem to be that much interest in them, perhaps because they're not as unique or special as their other machines. Which as a guy who grew up on old PCs, I think it's a shame.

<figure><p><img src="https://rubenerd.com/files/2023/commodore-pc30@1x.jpg" alt="Photo of a fairly typical looking PC compatible from the late 1980s, with dual 5.25-inch disk drives." srcset="https://rubenerd.com/files/2023/commodore-pc30@2x.jpg 2x" style="width:500px;" /></p></figure>

[PC30-III photo by Raimond Spekking](https://commons.wikimedia.org/wiki/File:Commodore_PC30-III-7614.jpg)

Among the tomes, blog posts, and videos that even acknowledge Commodore's PC efforts, they either make no comment about their impact on Commodore's business, or only offer speculation. Here are some examples that I think are representative of what I could find.

[Darth Azrael's retrocomputing page about Commodore](https://steemit.com/retrocomputing/@darth-azrael/commodore-pc-10-iii)

> Ironically, Commodore's line of PC compatible computers accelerated their demise. At this time the Commodore 64 and Amiga were still quite successful. However, their DOS based computers lost them money. Maybe if Commodore had put that money into further developing the Amiga line maybe Commodore would have had some of Apple's later success.

This is the first I've read of their PCs losing Commodore money. Perhaps the author theorised the PCs undercut or cannibalised sales of their own platforms, but I doubt it. The Amiga line was already a superior machine in many ways, but it was doomed by Commodore's management. I doubt reallocating engineering time spent cloning PCs would have made much of a difference.

[The 8-Bit Guy's PC video had a theory based on integration](https://www.youtube.com/watch?v=e63XWCW2ADY)

> Commodore and Tandy had a high level of vertical integration ... it's likely Commodore had significantly cheaper production costs than IBM, and this was reflected in the price ... My understanding was that PC clones were profitable for Commodore, but they just didn't have a big enough presence in the market to keep the company afloat.

While he doesn't mention how Commodore's PC clones started, his comments here echoes most of what I've read elsewhere. The PC unit couldn't save the company which, by that point, was basically dysfunctional.

[Encyclopedia.com offers insight into the company's profitability](https://www.encyclopedia.com/books/politics-and-business-magazines/commodore-international-ltd)

> Thomas Rattigan, a former PepsiCo vice-president who had become Commodore’s president in 1985, succeeded Smith as chief executive officer. In the summer of 1986 Rattigan returned Commodore to profitability. He introduced a line of IBM-compatible PCs and presided over continued successes in Europe

We know that Commodore PC clones were sold in Europe well before 1986, so they're probably referring to the parent company in the US returning to profitability. It reads to me that Commodore PCs played a part in this.

[Remi Jakobsen talked about their success in Norway](https://rclassiccomputers.com/2020/02/26/commodore-pc20-ii/)

> Commodore became the third largest distributor of PC compatibles here in Norway, in addition to their other very successful lines of computers like the Commodore 64/128 and Amiga-family.

[Dave McMurtrie puts some figures against this](https://commodore.international/2021/12/05/commodore-colt-pc-xt-clone/)

> Commodore began development of their PC clones in 1984 ... by 1986 Commodore was the second most popular PC brand in Germany and by 1988, PC sales accounted for 20% of Commodore’s overall revenue and covered the full line from 8088 to 80386.

He quotes an excerpt from Commodore International's 1985 strategic plan that states that their PC clones were "doing very well" in Europe. I'm starting to suspect they were profitable on both sides of the Atlantic, albeit modestly, and not enough to save the business.

[MCbx suggests the Commodore PC's fate was sealed by Asian clones](https://oldcomputer.info/pc/commodore10/)

> The problem was that PCs manufactured by Commodore (mostly in Germany) were not much different than much cheaper Chinese and Taiwanese clones. So after releasing next versions, previous were made even more "cost-reduced" giving a mess of revisions, versions and editions.
>
> In Europe, Commodore PCs got some small popularity in Germany where they were manufactured, but in other countries they were much more expensive than Taiwanese PC clones. Because PC was, in user's perspective, just a PC with all PC software, most customers bought cheaper Asian PCs with the same specifications.

That sounds about right. PC clones made in Asia by this point were of equivalent or better quality, and could compete on price. If you don't have much to differentiate your device, you can't be surprised.

Anyway, this was fun to research! It's interesting to learn about the history of these machines, even if most people haven't heard of them, or think they're unremarkable. 

