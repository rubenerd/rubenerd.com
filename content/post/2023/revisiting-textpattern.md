---
title: "Revisiting Textpattern"
date: "2023-10-27T08:25:27+11:00"
abstract: "Explaining the pros and cons of this blog engine compared with WordPress."
thumb: "https://rubenerd.com/files/2023/textpattern-writing.png"
year: "2023"
category: Thoughts
tag:
- blogging
- reviews
- textpattern
- weblog
location: Sydney
---
I mentioned last week that I [still host WordPress and Textpattern sites](https://rubenerd.com/missing-server-side-blogging-again/ "Missing server-side blogging again") for various people and orgs. Everyone knows WordPress, but it generated a few comments asking about what [Textpattern](https://textpattern.com/) is, my experience hosting and writing with it, and why someone might want to run it instead of WordPress.

Textpattern is my favourite server-side blog software, if you can work within a couple of its limitations. You know their priorities are right when you log in, and it immediately directs you to the Writing page. No dashboard, no news feed, no notification bubbles, *just write!*

<figure><p><img src="https://rubenerd.com/files/2023/textpattern-writing.png" alt="Screenshot showing the Textpattern writing screen." style="width:500px;" /></p></figure>

The default editor uses Textile, a lightweight markup language that even non-technical people are able to pick up. A charity I used to host had a small team of writers in their seventies that loved Textile, and would tell me they missed it when they went back to Microsoft Word and email. Think of Markdown, only more logical.

You can also write HTML, though there's no built-in replacement for WordPress's Classic Editor if you're after something WYSIWYG. There's also no Gutenberg to get in the way of writing though, so that's a pro.

Textpattern is organised around the concept of *sections*, which you can use to divvy up a site beyond simple categories and keywords (we'll get back to that in a moment). Think of a technical site with documentation, blogs, service announcements, and static pages, all rendered differently. This additional taxonomy makes writing themes much easier for complicated structures. I've achieved something similar in WordPress by abusing categories, but that introduces other problems.

On the server side, I love Textpattern because its not a moving target. It doesn't chase the shiny, as evidenced by the relative stability of its installer size, and its infrequent updates. It mostly just works, and has proved over the years to be easier to maintain than WordPress for broadly equivilent sites. FreeBSD, nginx, php-fpm, MariaDB, done.

There are a few limitations to keep in mind however.

Textile has the concept of post keywords, but it lacks a comprehensive tag system with archives and links. I've used [tru_tags](https://forum.textpattern.com/viewtopic.php?id=15084) for years to achieve this, but it's something to keep in mind. The smaller install base also means fewer extensions, themes, and professional support are available, which may be a deal breaker. WordPress is also at the forefront of emerging tech like ActivityPub that Textpattern isn't.

Textpattern's database schema is also a bit less flexible/normalised than WordPress. While keywords are comma separated in one attribute, categories are defined in two, limiting your posts to that number. I only assign posts to a single category anyway, but you'd need to see if that fits with how you structure your metadata.

I'm not a PHP developer, but I'd be tempted to learn just so I can contribute back in a meaningful way. Textpattern is really quite nice.
