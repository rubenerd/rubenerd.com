---
title: "Cellophane coffee shop chatter"
date: "2023-01-30T13:36:29+11:00"
abstract: "I put cellophane on my phone! WHICH DISK!?"
year: "2023"
category: Thoughts
tag:
- coffee-shop-chatter
- hypervisors
location: Sydney
---
I forget which installment we're up to.

* "They want a fully-customised stack with their own components. You know, like KVM or some shit."

* "I put cellophane on my phone!"

* "Ooh, avocado! But neither of us like avocado *(indecipherable)* it's fully smashed."

* "Yeah but the disk mate, which disk!?"

* "What's *after* French dressing?"
