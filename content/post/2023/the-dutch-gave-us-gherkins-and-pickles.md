---
title: "The Dutch gave us “gherkins” and “pickles”"
date: "2023-09-30T10:38:11+10:00"
abstract: "Wikipedia’s article on pickles describes the etymology of these words."
year: "2023"
category: Thoughts
tag:
- food
- language
- netherlands
location: Sydney
---
[From Wikipedia's delicious pickled cucumber article](https://en.wikipedia.org/wiki/Pickled_cucumber#Etymology)

In North America:

> The term **pickle** is derived from the Dutch word pekel, meaning brine. In the United States and Canada, the word pickle alone used as a noun refers to a pickled cucumber (other types of pickled vegetables will be described using the adjective "pickled", such as "pickled onion", "pickled beets", etc.). 

And in Australia too:

> The term traditionally used in British English to refer to a pickled cucumber, **gherkin**, is also of Dutch origin, derived from the word gurken or augurken, meaning cucumber.

How good are gherkins? They're one of those rare foods I'm always in the mood for. I'd even been known to eat one for breakfast, back when I used to have that meal.
