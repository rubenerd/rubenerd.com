---
title: "Finding those few bloggers all those years later"
date: "2023-10-09T19:35:08+10:00"
abstract: "It's interesting to think about how we existed in the same spaces, and ended up completely different places."
year: "2023"
category: Internet
tag:
- blogging
- politics
location: Sydney
---
On the rare chance I come across an old blog that either wasn't abandoned or deleted, it's interesting to see where the writer ended up.

A Singaporean anime blogger I used to follow in the 2000s (and I'm pretty sure I bumped into iRL a few times) became a cryptocurrency bro in the intervening years. It's weird, he seemed so grounded and funny at the time.

Another whom I followed for their cute sewing and software development advice came back from a blogging sabbatical, claiming that the German Workers' Party failed because they weren't right-wing enough. A pity, they conclude.

It's interesting to think about how we existed in the same spaces, and ended up completely different places. I may be insufferable in my own ways, but at least I'm not peddling scams and extermination. I know, a high bar!
