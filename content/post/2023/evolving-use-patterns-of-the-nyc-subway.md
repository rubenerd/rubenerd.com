---
title: "Evolving use patterns of the NYC subway"
date: "2023-05-26T08:55:00+10:00"
abstract: "Not everyone is a 9-5 commuter going to a desk job, and our public transport should reflect this."
thumb: "https://rubenerd.com/files/2023/nyc-subway@1x.jpg"
year: "2023"
category: Travel
tag:
- new-york
- public-transport
- urbanism
- united-states
location: Sydney
---
[Alissa Walker's report in Curbed](https://www.curbed.com/2023/05/public-transit-fiscal-cliff-off-peak-mta.html)

> The pandemic subway is a different system than what cities have known in the past. The same numbers of people aren’t flooding into central business districts every weekday, but ridership for many agencies has surged on afternoons and weekends. “Because of reduced daily commuting, many cities are working to expand midday and evening service in order to capture more ridership and provide service that is useful for a different mix of trips,” says Matthew Dickens, director of policy development and research for the American Public Transportation Association.

It's fascinating to read how these traffic patterns have changed, and I suspect its the same on other global metros. Not everyone is a 9-5 commuter going to a desk job, and our public transport should reflect this. I see it as an opportunity. For cash-strapped services like the NYC Subway, they may not have a choice but to roll with the times.

And for no reason, here's the first photo I found of the subway from Clara's and my trip to New York back in 2016. Even though the framing sucks, I find these sorts of shots way more interesting than those taken around Grand Central. Maybe because everyone takes photos of Grand Central.

<figure><p><img src="https://rubenerd.com/files/2023/nyc-subway@1x.jpg" alt="Photo of one of the platforms at the 42nd Street–Bryant Park station, with the name of the station on one of the support columns." style="width:500px;" srcset="https://rubenerd.com/files/2023/nyc-subway@2x.jpg 2x" /></p></figure>
