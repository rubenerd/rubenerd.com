---
title: "My daily carry in 2023"
date: "2023-08-15T08:12:23+10:00"
abstract: "It all started with a wonderful new Uniqlo bag."
thumb: "https://rubenerd.com/files/2023/bag@1x.jpg"
year: "2023"
category: Hardware
tag:
- apple
- bsd
- cameras
- freebsd
- macbook-air
- netbsd
- olympus
- om-system
location: Sydney
---
Wouter's latest post concerns his daily carry, which he posted like a gentleman:

[Brain Baking: What I Carry to Work](https://brainbaking.com/post/2023/08/what-i-carry-to-work/)

*Turns out*, he and I share a few things. This got me thinking what else I carry, and how so much has changed in the last year. Each of these will likely get their own post at some point.

<figure><p><img src="https://rubenerd.com/files/2023/bag@1x.jpg" alt="Photo of my tiny backpack with the components listed below." style="width:500px; height:375px;" srcset="https://rubenerd.com/files/2023/bag@2x.jpg" /></p></figure>


### Current essentials

I really tried to pare down what I carry this year. The perfect bag for this eluded me until Clara and I saw this impossibly-slim **Uniqlo backpack** in Ōsaka. It passed the crammed Japanese train test, has a mesh back panel for breathability, and a couple of small pockets. I may have even bought a spare; it's weird they don't offer it in stores overseas. The Hankyū luggage tag was from a train shop in Tōkyō.

[Wikipedia: Hankyu 9300 series](https://en.wikipedia.org/wiki/Hankyu_9300_series)

I downsized from an Intel MacBook Pro to an **M1 MacBook Air** for my daily work last year, which I swap for an even smaller Japanese **Panasonic Let's Note** running FreeBSD on the weekends. Both have such great battery life, I can eschew (gesundheit) a power brick.

The bag's smaller pouch has a few **USB keys**, and a **USB-C cable** for transferring photos off the **Ricoh GR III** camera I also carry. I'm not really a fan of it after all, so I've decided to sell it for something else. A lone refillable **Bic Crystal Re'New** pen is there for filling out forms. I also carry a spare coffee bag (not pictured) and tea bag like Wouter, because why not? I *still* carry a **$1 coin** in case my mobiles die and I need to use a pay phone, though I was 17 when that last happened.

[Camera specs don't tell the whole story](https://rubenerd.com/camera-specs-dont-tell-the-whole-story/)

The **small medicine bag** from the Singapore MRT gift shop includes a cute jar of Tiger Balm, a spare N95 mask in case I get the sniffles, a few Band-Aids, and my migraine medicine. It's been *wonderful* having multiple tablets be replaced with just the one with my latest prescription!


### Optional extras

These weren't pictured, but I'll strategically add these pouches when needed.

The first is a **Hatsune Miku travel pouch** that has my passport, ticket printouts, a map to our hotel and the nearest Australian embassy or high commission, and where I put my public transit cards, SIM cards, and keys from home. You don't want to be fumbling for those when you need them.

I also have a **data centre trip pouch** from Ansett, that defunct Australian airline. This has my ear plugs, access fobs, a coiled Ethernet cable, null modem cable, a few spare cage nuts, a small screwdriver set with swappable heads, USB to RS-232 adaptor, a muesli bar, and a bunch of installers on USB keys I try to keep current. I've been challenging myself to make this as small as possible.


### Future expansion

Daily carries are evolving beasts, and I'm always trying to find way to pare down. Ironically, these will only add bulk.

I'm giving serious consideration to a **Filofax personal binder** again. It's not just wanting to fulfill a childhood fantasy; I've started to think electronic PIM tools aren't working for me. Having a small binder with all my organised notes, and planning my week and #TODO list items with a pen might be useful. You probably already grokked this after reading my previous post about Lotus Organizer.

Speaking of books, I'm reconsidering my stance on **e-readers**. I tried also using my iPad Mini as a daily reader, but its middling battery and weight lead me to reconsider. I'm thinking probably a mid-sized Kobo with enough resolution and storage for manga as well as books. I tend to have a few books going at the same time, so having one reader is brilliant.

I'm also hoping to eventually replace that Ricoh with a slightly bigger **Micro Four Thirds** camera and a pancake lens again. My OM-D E-M10 was impossibly small and fun to use, and I'm so relieved Olympus is getting a new lease on life under *OM Systems*. Like that old laptop, my days of lugging an SLR boat anchor are long gone.

My dream is also to have a single, **multi-boot USB key** to replace the half dozen I carry around, I just haven't got around to doing it. Something like Rufus but with multiple images or partitions. Anyone know of a straightforward tool that could be used to do this, say for FreeBSD, Fedora, ESXi, Windows Server, macOS, and the like?

And finally, my laptops need more **NetBSD stickers** to complement the FreeBSD ones. This wouldn't add any discernible weight, though it would add discernible awesomeness. 🧡
