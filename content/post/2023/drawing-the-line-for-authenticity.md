---
title: "Drawing the line for food authenticity"
date: "2023-10-08T08:46:26+11:00"
abstract: "Someone saying Japanese curry isn’t authentic got me thinking about what the damned term even means."
year: "2023"
category: Thoughts
tag:
- food
- india
- japan
location: Sydney
---
I was ordering some Japanese with Clara yesterday, and overheard a white couple talking about their views on Asian food:

> I love it Japanese food! I mean, gyoza is authentic, but I draw the line at thinking curry is. That's obviously Indian.

It's an easy mistake to make, but [curry is huge in Japan](https://en.wikipedia.org/wiki/Japanese_curry "Wikipedia article on Japanese curry"). Despite its relatively recent introduction by British merchants in the 18th century, it's evolved into something unique. Think of Berlin's iconic [currywurst](https://en.wikipedia.org/wiki/Currywurst "Wikipedia article on the curry sausage"), only at a larger scale. Even curry powder, upon which these foods are based, was a colonial invention to conveniently export something approximating curry flavours; a traditional Indian cook wouldn't have touched the stuff. So in a way, these impromptu gastronomic epidemiologists were even a step further removed than they thought. Try saying that fast with a mouth full of curry.

But this hints at what might be behind such statements. I assumed people who got frustrated over a perceived lack of authenticity were snobs, contrarians, or otherwise closed-minded. But it's merely a function of time; or to use their words, at what *point* you draw the line. I consider Japanese curry authentic because it has its own distinct preparation, dishes, and culture. But if you're looking much further back through history, the currywurst and katsu curry look more like aberrations on the culinary timeline.

It's why I find the idea of *fusion* food equally interesting and confusing. Go back far enough, and everything is. Ice cream was fusion food, between Western confectionery and Chinese sorbet; if we're to believe the legends surrounding Marco Polo. Would you call traditional French ice cream fusion now? Equally, British high tea was fusion of this random imported Asian drink, and scones from a marshy island that ended up colonising the place where I'm writing this. Yet the English penchant for tea is a well known cultural identifier, maybe even a stereotype.

And while we're on the subject of geographic time: summarising something as being authentically "Chinese" or "Indian" is nigh meaningless. A chef trained in Andhra cuisine might have a better idea of how to cook sarson ka saag than someone outside the subcontinent, but it'd be just as disengenuous as a New Yorker claiming their deep dish pizza is authentic Chicago style, merely because they're also from the US.

I suppose it's why I bristle when I hear complaints about authenticity. What they're really saying is "not authentic to me, based on an arbitrary border and timeline". Sure, there's bad food out there that's tarted up to look like something it isn't, but it's likely suspect for reasons well beyond authenticity.

Damn it, now I want some Cantonese satay.
