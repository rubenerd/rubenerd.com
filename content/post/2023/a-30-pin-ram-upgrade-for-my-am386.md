---
title: "A 30-pin RAM upgrade for my Am386"
date: "2023-04-08T08:05:14+10:00"
abstract: "I bought some Intel/Samsung kit, and it just worked. Yay!"
thumb: "https://rubenerd.com/files/2023/simms-installed@1x.jpg"
year: "2023"
category: Hardware
tag:
- am386
- retrocomputing
location: Sydney
---
In case you haven't noticed, I'm somewhat obsessed with my Am386 motherboard of late. It was dead for years, but thanks to a modern ISA POST card I was able to diagnose and fix it. Since then, I've been on a mission to kit it out, both with parts I find in other boxes, and from eBay on a budget.

[List of all the am386 posts so far](https://rubenerd.com/tag/am386/)  
[Wikipedia article on single in-line memory modules](https://en.wikipedia.org/wiki/SIMM)  
[Liquid DeoxIT from Jaycar in Australia](https://www.jaycar.com.au/deoxit-contact-cleaner-rejuvenator-solution-kit/p/NS1436)

The board originally didn't boot thanks to some faulty RAM, or so I thought. As I wrote in that first post, I liberally applied some de-oxidising spray in the sockets, and brushed some liquid DeoxIT to the individual pins on the SIMM modules. Brushing it off removed decades of crud, and the machine booted for the first time in decades... with all four SIMMs detected!

Could I pull this off again, I asked myself while stroking my non-existent beard and nodding furiously, much to Clara's chagrin.

<figure><p><img src="https://rubenerd.com/files/2023/simms-installed@1x.jpg" alt="Photo of the Am386 motherboard showing the existing SIMMs installed in the background, and the four new modules in the front." style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2023/simms-installed@2x.jpg 2x" /></p></figure>

The motherboard has eight SIMM slots, four of which were unpopulated prior to installing this kit. This surprised me; most 16-bit 386 SX boards I'm familiar with only have four. When I bought this board in 2002, it came with a mix of Oki and NEC modules, suggesting the previous owner attempted an upgrade before recycling the machine.

I went on eBay, and found someone selling four 1 MiB modules for less than $30 shipped. Each one has a giant Intel logo silkscreened on them, with **T21044-07** silicon that a web search showed were Intel CMOS DRAM from September 1991. The PCBs have Samsung printed on them, and are listed as **E55991 9142 Rev-1**.

<figure><p><img src="https://rubenerd.com/files/2023/samsumg-ics@1x.jpg" alt="Photo showing two of the four modules, showing the aforementioned details." style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2023/samsumg-ics@2x.jpg 2x" /></p></figure>

I sprayed de-oxidiser into the sockets, donned the anti-static strap, installed the SIMMs, and booted the machine. I remember the conventional wisdom as a kid that you should never mix memory manufacturers, because it can calls all sorts of odd or unexpected behavior, especially around timings. If these SIMMs didn't work, my next plan was to remove the other SIMMs and try with just these, then perhaps attempt to source four more of the same.

To my surprise and relief, the machine booted, with all 8 MiB of memory detected! I've run some more tests (posts incoming) and they've all passed. Between this and the coprocessor I bought for peanuts, I've added more capabilities to this board than almost anyone would have had at the time, which is both ridiculous and delightful.

