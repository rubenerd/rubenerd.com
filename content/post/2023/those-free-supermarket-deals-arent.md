---
title: "Those free supermarket deals aren’t"
date: "2023-08-30T07:15:00+10:00"
abstract: "Subsidising drivers is my favourite part."
year: "2023"
category: Thoughts
tag:
- advertising
- aldi
- food
- shopping
location: Sydney
---
I'm not sure if supermarkets elsewhere do this, but the local duopoly in Australia are always doing these cheesy voucher promotions. Spend a certain amount of money, you accrue points, and you can use them to get a cheap saucepan, *for free.* It might even have a handle!

Of course, none of it is free... and deep down I think we all know this. The cost of the items are factored into the prices we pay for other goods. It's the same reason supermarkets will boast how cheap their staples are, then raise prices on everything else.

You don't notice how pervasive this is until you start shopping at local greengrocers, and supermarkets like Aldi. The former can often have higher prices on individual items, but in aggregate it's surprising how often you come out ahead. And Aldi, well, I unabashedly love it.

But back to these "free" deals and prices. It even extends to fuel in Australia. Certain local supermarkets will offer you a few cents a litre off petrol if you spend over a certain amount, which locks you into buying from their preferred vendor. It also means people like me subsidise drivers by paying extra for groceries.

You'd *think* there'd be greater pushback for such price manipulation. But this mental gymnastics clearly works, or they wouldn't continue doing it.

I'm also old enough to remember Apple's school promotions in the 1990s, in which parents were encouraged to "donate" their grocery receipts in exchange for their local primary school receiving a shiny new computer. I only have the vaguest of memories using those IIc's and Macintoshes... but hey, at least it was a better outcome than a washed up celebrity chef from a reality TV show spruiking a rusted knife!

Damn it, now I want an Apple IIc.
