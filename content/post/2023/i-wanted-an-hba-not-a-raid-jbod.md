---
title: "I wanted an HBA for FreeBSD, not a RAID JBOD"
date: "2023-12-08T11:46:36+11:00"
abstract: "JBOD on RAID doesn't make sense. I really wanted a general HBA."
year: "2023"
category: Hardware
tag:
- bsd
- drives
- freebsd
location: Sydney
---
Anyone outside IT would likely read that title and wonder if the heatwave in Sydney today has got to me. I need to *hiven glaven* the *bleep bloops*.

OpenZFS operates optimally without a RAID controller between physical storage devices and the host. Actually, I'm starting to think that *all* RAID controllers today are more trouble than they're worth (if only I could tell stories...), but that's a separate thing.

Earlier today I [asked on Mastodon](https://bsd.network/@rubenerd/111541734237167974 "Post on BSD.network asking about controllers") if there was a budget RAID controller I could use in a JBOD configuration. This term isn't as well known in the industry, but it literally referes to *just a bunch of drives*. In other words, give me some extra ports on a card that that the host can detect directly, don't give me a logical RAID volume of some kind.

Michael Dexter of [Call for Testing](https://callfortesting.org/ "Call for Testing"), OpenZFS, and bhyve fame reminded me that what I was really after was an [HBA](https://en.wikipedia.org/wiki/Hobart_Airport "Wikipedia article on Hobart Airport")\:

> Hobart Airport (IATA: HBA, ICAO: YMHB) is an international airport located in Cambridge, 17 km (11 mi) north-east of the Hobart CBD. It is the major and fastest growing passenger airport in Tasmania.
>
> Due to the airport's southern location, Skytraders operates regular flights to Antarctica on behalf of the Australian Antarctic Division using an Airbus A319

That's cool! But also clearly the wrong HBA. [Let's try again](https://en.wikipedia.org/wiki/Host_adapter "Wikipedia article on host adaptors")\:

> In computer hardware, a host controller, host adapter, or **host bus adapter (HBA)**, connects a computer system bus, which acts as the host system, to other network and storage devices

I'm so used to dealing with RAID controllers and HCA cards that I'd forgotten an overarching term existed.
