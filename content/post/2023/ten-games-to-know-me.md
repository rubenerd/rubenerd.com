---
title: "Ten games to know me"
date: "2023-10-11T10:14:05+10:00"
abstract: ""
thumb: ""
year: "2023"
category: Software
tag:
- bsd
- cities-skylines
- commander-keen
- dos
- freebsd
- games
- lego-island
- minecraft
- stanley-parable
- superliminal
- simcity
- the-sims
location: Sydney
---
<figure><p><img src="https://rubenerd.com/files/2023/ryza-screenshot@1x.jpg" srcset="https://rubenerd.com/files/2023/ryza-screenshot@2x.jpg 2x" style="width:500px;" alt="Key visual from Atelier Ryza: Ever Darkness and the Secret Hideout" /></p></figure>

I saw this meme floating around again, so I thought it'd be fun to try out.

* **BSD Games**. They taught me that we haven't *entirely* engineered fun out of our systems, despite the best efforts of certain people. I consider them essential, because even server sysadmins need a console game break sometimes.

* **Commander Keen**. A kid who built all this cool stuff, used it all to go to far-flung places, and had the courage to fight monsters!? My friends wanted to be comic book heroes; I wanted to be Billy Blaze.

* **Stanley Parable**. This surrealist, self-aware sense of humour is *exactly* what I aspire to have. I still laugh out loud and quote it regularly. Are... are you *really* still in the broom closet?

* **Superliminal**. Critics liked the game, but said it was too short and didn't explore the "perspective" concept enough. I read probably far too much into the message, and got a *lot* out of it. The settings, music, and writing... it was a masterpiece.

* **SimCity**, **The Sims**, **Cities Skylines**. They maybe speak to my desire for control over my surroundings, given how little of it I feel. Or I just love laying out grids and building nice places.

* **Microsoft Bob**. Not really a game, but I spent hours of my childhood arranging my virtual home and adding software launcher icons to all the right rooms. File Manager belongs in the garage, Media Player is in the loungeroom, Notepad and Word go in the study. Perhaps says more about my personality than I care to admit.

* **Atelier Ryza**. I love open worlds, and the fact you can really prioritise crafting over battle. I've played other Atelier games since, but this was my introduction. They're comfy, beautiful, and fun, qualities that have since come to define what I want out of most things! Life is short, why not?

* **LEGO Island**. This one is hard to explain. I thought it'd be about building things, but it was an off-beat puzzle and adventure game that I still think about twenty years later. I'm humming one of the songs right now, and even built it in Minecraft. Which reminds me...

* **Minecraft**. It's the building I expected from LEGO Island, and with far more freedom than the Sim games I was so hooked on. It's a sandbox in the most satisfying way possible, and is the only multiplayer game I explore regularly. We can build things without taking up space!

I suppose that's technically eleven, because I put three Sim games into one. Perhaps I should have spent more time in **Brandon's Lunchbox** learning how to count, or **Fate/Grand Order** having Nero telling me what to do. Wait, that's thirteen.
