---
title: "Troubleshooting my Am386SX’s RAM issues"
date: "2023-07-15T10:27:57+10:00"
abstract: "It all came down to a busted SIMM."
thumb: "https://rubenerd.com/files/2023/parity-error@1x.jpg"
year: "2023"
category: Hardware
tag:
- am386
- retrocomputing
location: Sydney
---
Last month I talked about getting a gorgeous Tseng ET4000AX ISA graphics card for my recently-fixed Am386SX motherboard. I was looking forward to seeing Windows 3.0 [sic] in 256-colour goodness for the first time since our childhood PC, and maybe even GEM.

[My Am386 now has a Tseng ET4000AX card!](https://rubenerd.com/my-am386-now-has-a-tseng-et4000ax-card/)   
[View all posts in this series](https://rubenerd.com/tag/am386/)

But there was just one hitch: *none* of the drivers I found online for the card worked. Regardless of resolution or colour depth, the GUI would scarecely progress beyond the splash screen before showing a blinking cursor and returning me to the DOS prompt.

Windows 3.0 isn't exactly the most stable graphical environment (neither is Windows 11, three decades later!), but when I tried it with Windows 3.11 for Workgroups, the drivers were enough to crash the box entirely.

I tried reseating the card (which works shockingly often on ISA slots), removing other cards in case there was a conflict, and even having one of my Pavolia Reine resin stands stare at the machine while I do it. But given the card worked flawlessly within DOS, and even in GUI modes for the PC DOS Shell, I suspected something else was going on.

[My 386SX’s working Acer MIO-400 IO card](https://rubenerd.com/my-386sxs-working-acer-mio-400-io-card/)   
[Using an IDE CD-ROM with a Sound Blaster 32](https://rubenerd.com/using-an-ide-cdrom-with-a-sound-blaster-32/)

By mid-June, the machine started exhibiting other erratic behaviour. The Multi-IO card I'm using only has a single IDE controller, so I have the CD-ROM connected to the Sound Blaster 32 card, like a gentleman. But occasionally the **SBIDE.SYS** driver would report no card present, or would hang on detecting the CD-ROM.

Around the time I swapped MS-DOS 6.22 for PC DOS 2000, I noticed **HIMEM.SYS** reporting issues I'd never seen in all my years using (abusing?) DOS:

    Unable to control A20 line. XMS driver not installed

This should have raised massive red flags about the RAM, but instead I figured out I could "solve the issue" by manually specifying the machine type. You can get details of this by using **HELP** in MS-DOS 6 and later; it's a flag I've never used before.

    :: CONFIG.SYS
    C:\DOS\HIMEM.SYS /TESTMEM:ON /MACHINE:1 /VERBOSE

This actually worked for a solid week. Well, "worked" in the sense that the machine booted, albeit sometimes without an optical drive or working VGA in Windows. But as though the machine was finally fed up with dropping hints, last night it dropped to 40-column mode and halted on this ominous message:

<figure><p><img src="https://rubenerd.com/files/2023/parity-error@1x.jpg" alt="PARITY ERROR ???? SYSTEM HALTED" style="width:500px;" srcset="https://rubenerd.com/files/2023/parity-error@2x.jpg 2x" /></p></figure>

Welp!

While waiting for Pavolia Reine's 3D debut, I spent an hour removing pairs of SIMMs from the fully-populated board until the machine booted. This included removing the new old stock ones I'd bought earlier this year, and those that had been rattling around in the box with this board for two decades before I got the machine working again.

[I fixed my 1991 386SX-16/20CN motherboard!](https://rubenerd.com/my-386sx-16-20cn-rev-0-motherboard/)   
[A 30-pin RAM upgrade for my Am386](https://rubenerd.com/a-30-pin-ram-upgrade-for-my-am386/)

I quickly zero'd in on one module with pins that looked *significantly* more worn than any of the others (pictured below with the white label), and that I remembered I'd needed to apply several coats of DeoxIT to for it to even be recognised. Sure enough, removing this and its matching twin resulted in the machine booting flawlessly.

Just in case, I added each pair of SIMMs back, ran CheckIT Pro's built-in quick and comprehensive memory tests, then added the next pair back. It all passed with flying colours. I was also able to remove that **MACHINE** line from **HIMEM.SYS**, Windows 3.0 worked with the Tseng VGA drivers, and the CD-ROM comes up every time.

<figure><p><img src="https://rubenerd.com/files/2023/check-it-example@1x.jpg" alt="Pavolia Reine standing to CheckIT reporting no issues." style="width:500px;" srcset="https://rubenerd.com/files/2023/check-it-example@2x.jpg 2x" /></p></figure>

Despite only having 6 MiB of 70 ns memory instead of 8 now, removing this module gave this machine more of a perceptible performance uplift than the i387SL maths coprocessor I added! Clearly this one SIMM was dragging this machine down for the entire time I was using it. This machine feels like a 386 now, not an AT with more RAM.

I raise this not just to state the obvious, but to encourage anyone stymied by retrocomputer problems to try the basic stuff first. Reseat things. Remove everything you can, and add things back upon verifying them. You would be shocked how often all these seemingly-disconnected issues can be traced to something so small.

