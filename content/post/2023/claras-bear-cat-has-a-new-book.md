---
title: "Clara’s Blue Bear Cat has a new book"
date: "2023-11-17T12:24:01+11:00"
abstract: "Cute!"
thumb: "https://rubenerd.com/files/2023/bear-cat-book@1x.jpg"
year: "2023"
category: Thoughts
tag:
- bear-cats
- clara
location: Sydney
---
<figure><p><img src="https://rubenerd.com/files/2023/bear-cat-book@1x.jpg" alt="Two photos of Clara's Blue Bear cat holding a scale copy of a book." style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2023/bear-cat-book@2x.jpg 2x" /></p></figure>
