---
title: "Missing server-side blogging again"
date: "2023-10-19T11:59:11+11:00"
abstract: "I miss being able to write everywhere, and having a database."
year: "2023"
category: Internet
tag:
- classic-editor
- textpattern
- weblog
- wordpress
location: Sydney
---
I've mentioned here that I maintain a bunch of Textpattern and WordPress blogs for various people and non-profits. It sort of happened by accident, but I keep picking them up.

Every time I log into one of them, the sysadmin in me is relieved that my personal blog here is static. Those other blogs have databases, caching, PHP, web servers, and FreeBSD jails to maintain. On my site? It's an install of [nginx-lite](https://www.freshports.org/www/nginx-lite/ "FreshPorts page on nginx-lite") and an [mdmfs(8)](https://man.freebsd.org/cgi/man.cgi?query=mdmfs) RAM disk. That's it.

But the *writer* in me sighs. Writing on those systems is so *easy*. You can do it from anywhere, on anything. [Textpattern](https://www.textpattern.com/ "Textpattern’s home page") is especially pleasant to use, though even WordPress with the [Classic Editor](https://wordpress.org/plugins/classic-editor/ "WordPress’s plugin page for the Classic Editor") plugin is also fine.

I also miss having a proper database. Bulk-updating metadata or fixing things is an SQL command on those sites, not a bunch of awk to rewrite touchy YAML frontmatter that side-eyes you if you dare not write it exactly as it wants. Maybe this would be fine with a few hundred static pages, but the nearly nine thousand I have now? *Alamak.*

The question is: what will I do with this newfound re-appreciation for server-side blogging? I wonder.
