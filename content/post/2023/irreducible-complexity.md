---
title: "Irreducible complexity"
date: "2023-09-25T08:02:02+10:00"
abstract: "The idea that something complicated can’t be reduced. I’m finding this in my work too."
year: "2023"
category: Thoughts
tag:
- psychology
- work
location: Sydney
---
Those confused or uneducated about evolution often dismiss it for not explaining *irreducible complexity*. This is the idea that something complicated can't possibly be simplified while maintaining function. I feel as though my brain is operating under a similar misapprehension, only instead of evolution, it's about... everything!

I tell myself I should break tasks up into smaller steps. Doing so makes the overall endeavour easier, because the objectives and requirements for smaller things are easier to conceptualise and track. It reduces the barrier to entry, by making a massive task seem less intimidating. You also get the satisfaction of ticking off a few things off a list, rather than having one glaring item judging you for not finishing it off after weeks of work.

*I'm not shifting a boulder, I'm carrying a dozen rocks. I got this.*

But what if you can't see the rocks in the boulder? What if there aren't any cracks you can cleave with your trusty #TODO chizel? What if this task is some massive, seamless block?

I don't think I need help with the *concept* of breaking down tasks, but rather the practical way one actually does it. And I fear the more I overthink it, the harder it's becoming!
