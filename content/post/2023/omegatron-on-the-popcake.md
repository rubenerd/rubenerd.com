---
title: "@Omegatron on the POPCAKE"
date: "2023-09-04T08:24:31+10:00"
abstract: "He says they’re expensive and use a specifc mix, damn."
year: "2023"
category: Hardware
tag:
- comments
- food
- travel
location: Sydney
---
[The POPCAKE airport lounge pancake maker](https://rubenerd.com/airport-lounge-pancake-makers/)   
[@Omegatron Via Twitter](https://twitter.com/omegatron/status/1698452997141860595)

> I love these things so much that I looked into getting one, but it turns out: a) they are rather expensive machines and b) they use an expensive premix packet that goes in the top, gets squeezed out like a tube of toothpaste and only lasts a few days once it’s opened :(

Darn! I assumed it was a resovoir of sorts, that anyone could pour batter into. I wonder how hackable it is? Could I put NetBSD on it, and attach a funnel?

I guess I'll put this alongside the Mitsubishi JetTowel, Tennant ride-on floor polishers, and rackmount 10G switches as commercial devices I'd *love* to have for home, but it's probably for the best that I can't. Or... could I?
