---
title: "An apartment tower of lithium"
date: "2023-04-09T17:34:00+10:00"
abstract: "Thinking back to what a fire inspector told me as a kid."
year: "2023"
category: Hardware
tag:
- batteries
- design
- electronics
- laptops
- phones
- safety
- tablets
location: Sydney
---
In one of our last apartments in Singapore, my sister, dad and I were around when the building's fire inspector came through to look at our smoke detectors, front door seals, sprinkler systems, and the like. Our unit was the last on his clipboard for the day, so after he finished we sat around the kitchen and offered him some tea.

One remark that stuck with me since as we talked: he said he was far more worried about apartment fire safety than any of the factories and offices he also inspected. He pointed at the Palm Tungsten W smartphone in my hand (it was a different time) and said we were on the cusp of something serious; something that "none of us are prepared for".

A decade or so later today, and Clara and I were catching the lift up to our Sydney apartment when we noticed a memo on its small notice board, emphasis theirs:

> Dear Residents
>
> This memo is to highlight that there has been a signifiant increase of fires Australia wide caused when charging electric vehicles like bikes, scooters, and skateboards.
>
> Fires are caused when the integrity of lithium batteries used by the above equipment are compromised, the energy they store is released as heat, known as **thermal runaway**, and this can cause fires which are extremely difficult to extinguish while releasing an extrodinary array of deadly toxic gases.
>
> This situation would not be good in a building such [as ours].
>
> Please be careful.

Like so much legislation and rules, I can't help but read things like this and wonder what inspired it. Earlier in the week we'd been awoken by three consecutive fire alarms in as many hours. Did we avoid something serious?

It does make me think about all the smartphones, laptops, and other devices with batteries we all have in our homes, and that we've now stacked sky-high in residential towers. Looking around Clara's and my workspace here, I can count sixteen lithium batteries. Even in less nerdy dwellings, it's reasonable to think there are at least a few phones, maybe a laptop or two. Throw in some personal mobility devices that people are charging in these confined spaces, and it's pretty scary.

People talk about the knife-wielding maniacs in their nightmares, or plummeting down a hole only to wake up before they hit the ground. My nightmares are full of clutter and lithium. I'm relieved we don't hear about fires more often, but that fire inspector was probably onto something.
