---
title: "A generative AI provider almost gets it"
date: "2023-09-08T08:45:24+10:00"
abstract: "“Unless explicitly permitted, you may not use web scraping...”"
year: "2023"
category: Internet
tag:
- ai
- scams
location: Sydney
---
[From their revised Terms of Service](https://www.microsoft.com/en-au/servicesagreement/upcoming.aspx)

> p. ii. Unless explicitly permitted, you may not use web scraping, web harvesting or web data extraction methods to extract data from the AI services.

They're *so close* to being self aware! Next they'll admit they want to prevent the very behaviour that made their datasets possible.

[Emil Oppeln-Bronikowski](https://bsd.network/@rubenerd/111026283837849378)

> *Rule for thee...*

