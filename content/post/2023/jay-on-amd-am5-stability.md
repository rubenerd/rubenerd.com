---
title: "Jay on AMD AM5 system stability"
date: "2023-11-06T08:25:41+11:00"
abstract: "Memory timing, stability issues, and 3D V-Cache not enabling/disabling for everything."
year: "2023"
category: Hardware
tag:
- amd
- bsd
- freebsd
- gamimg
- linux
- jayz2cents
- videos
location: Sydney
---
Jay's latest video discusses [the two key challenges](https://www.youtube.com/watch?v=JZGiBOZkI5w "Why I switched back to Intel") he's faced over the last year with his AMD 7950X3D system, specifically: 

* Memory timing and resulting stability

* Cores containing 3D-Cache not being deactivated properly

The former I've heard *a lot* about from friends over a similar time period, but the latter was new to me.

For those not versed in AMD nomenclature, X3D CPUs are variants of AMD's desktop silicon that include more cache. Games benefit significantly from this, but they offer negligible improvement for production tasks. Because the cache is more heat sensitive, X3D CPUs are clocked lower than their non-cache enhanced versions, meaning they're really optimised for gaming.

For people switching between production and games on these CPUs though, there's a feature that can temporarily disable the chiplets containing the 3D V-Cache. This means the other chiplets can clock themselves higher for more performance outside games.

It sounds great in theory, but Jay's experience was that this wasn't always applied correctly. Production software would have the 3D V-Cache enabled, and indie games wouldn't. He also hinted that it uses an XBox feature on the desktop, so such a feature wouldn't work for those of us playing on Linux or FreeBSD.

This news doesn't really impact me; I've never bought the high-end stuff, I don't game that much, and I've always favoured stability and longevity over things like overclocking. If you *do* appreciate those things though, this might be something to consider.

The real answer seems to be having separate game and production systems. Sounds nice, if stuff weren't so expensive!
