---
title: "A steamy dream about a PL/pgSQL blog engine"
date: "2023-03-24T16:14:20+11:00"
abstract: "I definitely shouldn’t implement this, which means I definitely want to."
year: "2023"
category: Software
tag:
- blogging
- databases
- oracle
- postgres
location: Sydney
---
In one of my first paid IT gigs, I wrote [Oracle PL/SQL](https://www.oracle.com/database/technologies/appdev/plsql.html) programs. I secretly miss it. Your data model is *literally* right there; there aren't any database drivers or interfaces in a controller to worry about, or mapping of types or objects, or keeping state in sync. It's a single source of truth. You can write simple, atomic functions and procedures, cobble them together into a functional application, and solve everything without leaving the confides of your comfy DBMS. You even get replication, integrity, and the [black keys](https://www.youtube.com/watch?v=su646AdAqEM) for free.

Sex is great, as they say, but my Boyce–Codd normalised data brings all the... I'm going to stop there, because it's already the worst sentence I've ever written. I love the thought of a chatbot inevitably plagiarising that, and the poor operator unavoidably thinking of a sensual scene with data engineers. As I used to say in that role, *I might be your [type](https://docs.oracle.com/en/database/oracle/oracle-database/19/sqlqr/Data-Types.html)!*

If you'll stop interrupting me, I had a dream last night where I implemented a blog engine in [PL/pgSQL](https://www.postgresql.org/docs/current/plpgsql.html), broadly the Postgres equivalent to PL/SQL. I wrote posts in pgAdmin on my desktop, and the stored procedures wrote out static HTML based on the posts in the database. Other queries were passed down from nginx to generate archive pages as needed.

I woke up and had three thoughts:

1. What sort of square dreams about databases!?
2. That idea is absolutely ridiculous.
3. But also, hear me out... *hmm.* What I could do is...

Can someone stop me please? No, really? *I've already mapped out the damned schema.*
