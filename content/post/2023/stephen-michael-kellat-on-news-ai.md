---
title: "Stephen Michael Kellat on news, and my last AI post"
date: "2023-12-31T17:22:19+11:00"
year: "2023"
category: Ethics
tag:
- ai
- ethics
- news
- politics
- society
location: Sydney
---
[In reply to @evan@cosocial.ca about authoritarianism](https://kellat.me/2023/in-reply-to-evan.html)\:

> When your news media dies off and people become numb to the world around them because they don’t understand what’s going on, authoritarianism has quite an opening to take root. Authoritarians provide answers about a complicated and chaotic world when nobody else is around. They provide that feeling of safety when people feel alone and disconnected as we’ve seen with QAnon’s rise and with the resurrection of the Sovereign Citizen Movement.

Peddling superficial answers to complicated problems has always been popular, but it certainly feels like more people are falling for it (sorry, Steven Pinker)!

> Keeping a civil society running requires work. It doesn’t operate on its own. Right now nobody wants to invest in it as there is a big push to try to let computers handle things and to delegate to AI as much as possible. Society still needs a human touch and that lack is a big part of what is driving this decline.

AI is a solution to society's ills the way an air conditioner is to climate change. Sure you get a cool respite and some synthesised information pulled from the slurry of modern web spam, but all you have to do is inspect the inputs to understand its limits.

🌷

This will be my last post about AI for the foreseeable future. Whatever practical application it has (at least, in the way popular culture has come to understand "AI"), its stratospheric hype and irresponsible application have completely numbed me to it. It's difficult for me to be optimistic or impartial knowing how the sausage is made.

If people aren't defending the pollution generative AI produces, or the training data used commercially without permission, attribution, or compensation, they're drawing tepid comparisons to other untrustworthy media as though it absolves them of burning more power than a crypto miner to automate away the only thing that makes life worth living: human love, inspiration, and creativity expressed though art.

Some days I find it hilarious! Other times, less so.

It's worth remembering that the pundits and investor class will lose interest eventually, and focus on the next tulip they can shovel onto unsuspecting people as a quick fix to social and economic malaise. I dare us all to be wiser next time.
