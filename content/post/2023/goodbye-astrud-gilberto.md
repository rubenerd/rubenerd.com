---
title: "Goodbye Astrud Gilberto ♡"
date: "2023-06-08T08:36:19+10:00"
abstract: "She was my introduction to bossa nova and latin jazz."
thumb: "https://rubenerd.com/files/2023/yt-sVdaFQhS86E@1x.jpg"
year: "2023"
category: Media
tag:
- bossa-nova
- goodbye
- jazz
- music
location: Sydney
---
She was my introduction to bossa nova and latin jazz, which have since become my favourite genres of music. I had a poster of her in my bedroom as a teenager. I bonded with my parents over her music, and it got me through some rough years.

I was trying to think of a fitting song to share, but I think it had to be *The Girl from Ipanema*. This was her performing it live with Stan Getz in 1964.

<figure><p><a target=_BLANK href="https://www.youtube.com/watch?v=sVdaFQhS86E" title="Play Astrud Gilberto and Stan Getz - The Girl From Ipanema (1964) LIVE"><img src="https://rubenerd.com/files/2023/yt-sVdaFQhS86E@1x.jpg" srcset="https://rubenerd.com/files/2023/yt-sVdaFQhS86E@1x.jpg 1x, https://rubenerd.com/files/2023/yt-sVdaFQhS86E@2x.jpg 2x" alt="Play Astrud Gilberto and Stan Getz - The Girl From Ipanema (1964) LIVE" style="width:480px;height:360px;" /></a></p></figure>

