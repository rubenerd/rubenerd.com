---
title: "When does retrocomputing begin?"
date: "2023-06-25T09:41:41+10:00"
abstract: "When it can’t be used for work, and when it’s more expensive used than new!"
year: "2023"
category: Hardware
tag:
- michael-dexter
- retrocomputing
location: Sydney
---
My dear friend and fellow Baltic state fan Michael Dexter ran a fun poll that ended yesterday, with the options of end-of-life date, five years, ten years, twenty years, and *never, stay current!* I put myself down for twenty years, which turned out to be the most popular answer.

[@dexter: When does #RetroComputing begin?](https://bsd.network/@dexter/110595795926769119)

It may *shock* some of you that I have a couple of theories of my own!

I'd say retrocompting visibly starts when devices **fetch more on eBay than at retail**. This follows what I dub the *bathtub curve of computing*, where a new device loses value as it becomes outdated, before becoming collectable. This is why a Commodore Plus/4 or a Sun SPARCstation is retro, but my smashed T550 is e-waste.

<figure><p><img src="https://rubenerd.com/files/2023/commodoreplus4-table@1x.jpg" alt="The Plus/4 showing a welcome greeting!" style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2023/commodoreplus4-table@2x.jpg 2x" /></p></figure>

The second yardstick is a bit more nebulous, but I think it's interesting.

When I was a kid in the late 1990s, I was *really* into learning about DOS, old-school UNIX, and 8-bit computers. I grew up on the former, and the latter two because I wanted to know what came before. These systems felt *retro*, because they didn't have contemporary features like CD-ROMs, sound cards, XVGA, Ethernet, Zip drives, or FireWire. They had SIDs, datasettes, 5.25-inch floppy drives, and ISA cards.

That was a difference of a decade, so it's tempting to set that as the yardstick. But... is that true today?

For all the progress modern computing has made, the pace of these new people-facing features seem to have slowed. We still manage to push more bits faster and in less space, but has the way we computed really changed that much?

Ten years ago now, Apple had Retina-grade screens, ThinkPads had Wi-Fi by default, 64-bit silicon was the default, and capacitive-touch smartphones were already entrenched. My old hardware from that time runs the latest FreeBSD and NetBSD just fine, and thanks to their removable storage, memory, and batteries, will almost certainly outlive my current machines. Hey, we're Apple, and we care about e-waste!

<figure><p><img src="https://rubenerd.com/files/2023/aptiva-friends@1x.jpg" alt="Photo of the Aptiva alongside the other desktops." style="width:500px; height:375px;" srcset="https://rubenerd.com/files/2023/aptiva-friends@2x.jpg 2x" /></p></figure>

This leads me to conclude that retrocomputing also starts **when you can't use something for anything practical**, or at least not without jumping through hoops. Retrocomputing is done for the love of computers and their history, not to do your work. This might also explain why its so captivating and fun, and a bit of a cheeky escape.

*(I say that, but DesTerm128 on my Commodore 128 could technically dial back to my FreeBSD bhyve box, which could then SSH to a production server somewhere. I'd better not tell my boss that).*

The only counterpoint to that theory is the existence of NetBSD, Minix, and FreeDOS. My three beige boxes above can still run these contemporary OSs, and with a shockingly little loss of functionality. In that case, they become retrocomputers when they're rebooted back into FreeBSD 4, the Halloween release of Red Hat Linux, or Windows 2000.
