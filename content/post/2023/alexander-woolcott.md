---
title: "Alexander Woolcott"
date: "2023-08-16T15:30:27+10:00"
abstract: "There's no such thing in anyone's life as an unimportant day"
year: "2023"
category: Thoughts
tag:
- life
- quotes
location: Sydney
---
[Wikiquote: March 30](https://en.wikiquote.org/wiki/March_30)

> There's no such thing in anyone's life as an unimportant day.

I needed that.
