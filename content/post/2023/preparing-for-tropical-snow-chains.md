---
title: "Preparing for tropical snow chains"
date: "2023-11-13T17:08:45+11:00"
abstract: "Bad things are inevitable, but how you respond is up to you"
year: "2023"
category: Thoughts
tag:
- backups
- life
- preparation
location: Sydney
---
I briefly had a job in Singapore where an English client had a wall of those motivational posters. One of them showed a snow drift, and a line underneath asking "Where are your snow chains?" I remember thinking this was pretty funny to have up in a tropical country, and assumed he put it on the wall as a joke regarding the ubiquity of air conditioning.

His answer wasn't what I expected. The point wasn't to be ironic, but to remind people that uncommon, unusual, or even unheard of things can happen, and should be prepared for. His attitude was that you should have contingencies for every possible scenario. Bad things are inevitable, but how you respond is up to you.

I've become a *bit* more circumspect of this idea as time goes on. There's always the risk that you'll paralyse yourself or your team trying to cover every conceivable circumstance, and in doing so cause more damage than what you're attempting to avoid. There's a line somewhere though, and people are generally more *blasé* about where it is than perhaps they should be.

I wonder if the Singapore government has an emergency plan under lock and key for a freak snowstorm? How *would* a tropical, humid country full of flat roofs, low elevation, and unfamiliarity with frostbite handle being snowed under?

