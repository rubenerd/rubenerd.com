---
title: "Using what you have is better than not!"
date: "2023-08-27T10:22:09+10:00"
abstract: "Discussing a revelation with my retro computers and life in general"
year: "2023"
category: Hardware
tag:
- am386
- commodore
- commodore-128
- commodore-vc-20
- life
- philosophy
- retrocomputing
location: Sydney
---
I've caught myself making another logical mistake over, and over, and over again, and thought I'd share it.

I was trying to figure out why I'm able to work through and resolve technical issues for clients at work so quickly, yet things I have at home languish for weeks, months, or even years. We've all heard the phrase that the *cobbler's children walk barefoot*, but I wanted to know why.

Financial pressure is an obvious reason. If someone paying your bills asks you to do something, you'll do it. Time is also a factor; people in professional settings tend to work around deadlines, deliverables, and... damn, I wish I could think of something else starting with D. Despite spending just as much time at home, the reverse is often the case for these.

But for me personally (as opposed to be me impersonally?), I've realised that projects sit half finished for ages because I'm always waiting for something better. And it's even more ridiculous than I realised.

I've noticed this before. I'll be missing a component for a modern or retrocomputing build, so I'll order it and wait the month for it to be delivered to our remote country out in the Pacific. It'll arrive, and it won't work for what I needed! I'll order something else, and wait another month. Before I know it, it's been a year and my Commodore 128 still doesn't have a functional cassette deck, or my VC-20 doesn't have the right video cable.

But there's something more going on here. I'll get halfway through a project, and get stuck because I think I'm waiting for something to fix it. Only, I have stuff to fix it already, it's just not exactly what I want.

Here's the galaxy brain moment: **a suboptimal solution is better than none!**

While waiting for those parts, these various machines will sit broken and unusable. Alternatively, I can cobble together components I already have while I wait, and enjoy this kit immediately.

Take my Am386 computer I've been fleshing out this year. I've been struggling to get a reliable CD-ROM working with it, and figured I could kill two birds with one stone by getting a SCSI optical drive alongside something like a ZuluSCSI to work around BIOS drive size limitations. The end result was a machine that had everything disconnected for months. Yesterday, I connected my Disk-on-Module and a bargain basement IDE CD-ROM to the same lone IDE bus on my Multi-IO card, and both now work. Is it optimal? No. Does it work while I wait for those other components! Yes! And **it's awesome!**

As another example, I've been wanting to do an S-Video mod to my Commodore VC-20 for months, because its composite video output isn't the best. But you know what's better than an absent S-Video mod? Composite video! It literally works, **right now!**

[Posts tagged with Am386](https://rubenerd.com/tag/am386/)   
[Restoring my Commodore VC-20](https://rubenerd.com/restoring-my-1983-commodore-vc-20/)

I'm sure highly-functional readers among you are laughing at how ridiculous this all sounds, but this has been nothing short of a revelation for me. While I wait for a future that will make things better, I'm missing out on the present.
