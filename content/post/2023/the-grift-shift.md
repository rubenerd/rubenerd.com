---
title: "The Grift Shift, with apologies to The Commodores"
date: "2023-10-10T10:12:24+11:00"
abstract: "Crypto collapse? Get in loser, we’re pivoting to AI!"
year: "2023"
category: Ethics
tag:
- ai
- blockchain
- nfts
- scams
location: Sydney
---
[Amy Castor and David Gerard back in June](https://davidgerard.co.uk/blockchain/2023/06/03/crypto-collapse-get-in-loser-were-pivoting-to-ai/ "Crypto collapse? Get in loser, we’re pivoting to AI")\:

> Half of crypto has been pivoting to AI. Crypto’s pretty quiet — so let’s give it a try ourselves! Turns out it’s the same grift. And frequently the same grifters. [...]  Much like crypto, AI has gone through booms and busts, with periods of great enthusiasm followed by AI winters whenever a particular tech hype fails to work out.

[Michelle Celarier's report in August](https://www.institutionalinvestor.com/article/2c4fad0w6irk838pca3gg/portfolio/money-is-pouring-into-ai-skeptics-say-its-a-grift-shift)\:

> Orso Partners co-founder Nate Koppikar [...] calls the phenomenon “the grift shift” — arguing that companies and venture capital funds have pivoted from their losing crypto and tech bets to cash in on the AI moment.
>
> “This looks like a great market for fraudsters to chase because of the ability to ‘Theranos’ your product” with semifunctional prototypes and claims of AI being used in products that are really just powered by humans.

[The Commodores](https://www.youtube.com/watch?v=FrkEDe6Ljqs)\:

> Gonna be some big crash... scamming cash... on the *Grift Shift.*
