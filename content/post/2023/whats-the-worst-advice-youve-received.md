---
title: "Whats the worst advice you’ve received?"
date: "2023-07-30T09:52:18+10:00"
abstract: "Don’t prepare for meetings! You should always tolerate family! And so on."
year: "2023"
category: Thoughts
tag:
- advice
- bsd
- freebsd
- gymnastics
- live
- personal
- sport
location: Sydney
---
This started as a cautionary tale for the first item here, but I kept thinking of other ones. Rather than seeing this as a negative post, maybe there's something here that's useful.

### Don't do meeting prep

Someone with a degree of authority advised me that I spend too much time preparing for client meetings, which probably stemmed from my anxiety and a deep-seated need to feel in control. They suggested that any information one needs can be gleaned from talking with people, so I didn't need the half hour or hour beforehand to take notes.

The first meeting I tried this on, and the second, were unproductive. I like to spend time and find out who someone is and where they work, so I can reference it and offer specific information from the start. I felt disrespectful without doing this; these people are taking time out of their busy day to have a chat, the least I could do would be check out their website, read a few of their company blog posts, skim their product documentation, and make a list of who they work with.

But awkwardness aside, these meetings were even failures by their own metrics. Much of these two calls were spent answering with *I'll get back to you*, which necessitated taking more notes for email, and a follow-up call. If the point of eschewing (gesundheit) preparation was to save time, it did the exact opposite! I've seen sales people wing an entire meeting with only the person's business card to go on. It's impressive, but it's not my style.


### It's easier to seek forgiveness than permission

As the person on the receiving end of this more often than not: nah.


### Tolerating family

Here's one you might have heard: you should tolerate bad people if they're family, because they're family. You don't want to regret not reaching out when you had the chance, even if it does mean forming a sentence with a double negative.

I lived by this for most of my life, and all it did was make me stressed and miserable. I suspect many of you reading this could relate!

Life is too short, and hearts too precious to spend them on bad people, family or otherwise. This year I decided I'll extend a bit more courtesy to someone if they're family, but I set boundaries. I can't tell you how much better I feel.


### Top-loading washing machines

I was always told these machines were preferable over front-loaders because they're mechanically simpler and therefore more robust.

These might be true, but they fail to mention how much more water they use, and how you can't stack things on top of them! If you live in a tiny apartment with a glorified cupboard as your laundry, this can be the difference between having your detergent box and hampers sitting in a hallway, or where they belong.

If there's a meta point here, it'd be that you should be careful about optimising for specific metrics if other ones are important.


### Yeah, well it is what it is

If "it" is bad, I reject the premise. There's pragmatic realism in accepting how things are, but I'm not going to stop questioning it. If there's space for improvement, let's do it! 


### You shouldn't use BSD, because nobody else does

This is false, but let's assume *nobody* has an invisible qualifier.

I can see where this advice comes from. There are benefits to using the dominant platform, programming language, framework, or design methodology. There's more documentation. More people can offer advice. There's more industry support, either in software or hardware. And so on.

But if you're able to spend the time building a system optimised for it, and you understand it well, you reap the rewards. Unlike me, who had to spend his Saturday fixing iptables rules for someone because I gave them Debian a few years ago for something for which FreeBSD would have been perfect.


### There are girly sports

This is for my younger readers in school. When our high school sports classes had to break out in multiple streams, I was one of the few gents who took up gymnastics over football. I copped it badly from people afterwards, but it was genuinely one of the few times during those classes where I had fun. Turns out, our bodies are all designed for different things.

Eventually I let the bullies win, which resulted in me losing what little interest I had in sport and physical activity for many years. There's another meta point there somewhere, but leaving aside the fact male gymnastics is *huge*, and that the women's World Cup has been drawing massive crowds, you should do what you want when given the choice!


### Ditch XML, JSON is so much nicer

At the risk of being a guy in his thirties launching into a *back in my day* rant about schemas, namespaces, and validation, I'll move on. Or I would, if my damned YAML would marshal.


### Limit your blog to one topic

This is one I never took seriously, but I see other people struggle with. Self-anointed blogging experts used to say your blog should focus on a primary topic, because something something metrics. They also said people blogging about multiple things was boring or a waste of time, especially if you subscribe to their RSS feeds.

I couldn't disagree more. The blogs I enjoy reading the most are the ones where people have disparate interests. They pull me in with their discussion of open-source software or the Indie Web, then *BAM*, Star Trek, coffee, and bread! Some examples:

[Brain Baking by Wouter Groeneveld](https://brainbaking.com/)   
[James’ Coffee Blog](https://jamesg.blog/)   
[Michał Sapka](https://michal.sapka.me)


### Always have a conclusion

Ending your writing with a definitive point, or even a full stop, sounds great in theory, but instead I prefer to do this thing where I
