---
title: "Getting frustrated at inanimate IT objects"
date: "2023-06-05T10:52:46+10:00"
abstract: "Frustration by proxy?"
year: "2023"
category: Hardware
tag:
- design
- ergonomics
location: Sydney
---
I had a bit of an epiphany today. I'm not frustrated at my computers for not doing what I want, or for behaving unexpectedly, or for having increasingly-hostile interfaces and design. I'm frustrated at the *people* who designed them, made them, and/or signed off on them.

Frustration by proxy?
