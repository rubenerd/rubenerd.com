---
title: "Hololive: Our Bright Parade!"
date: "2023-03-20T15:15:21+11:00"
abstract: "Umarvelous... umarvelos!"
thumb: "https://rubenerd.com/files/2023/holofes4-fauna@1x.jpg"
year: "2023"
category: Anime
tag:
- ceres-fauna
- concerts
- hololive
- music
- music-monday
- pavolia-reine
location: Sydney
---
Today's *[Music Monday](https://rubenerd.com/tag/music-monday/)* is a *thoroughly* inadequate and incomplete concert review, but I absolutely have to post about it.

The Hololive virtual YouTuber idol agency put on their annual live concerts over Saturday and Sunday night, this year dubbed *Our Bright Parade*. I've watched them on Blu-ray in the past, but this was first time I watched them with Clara live.

Past concerts have been great, but this was really something special. Along with my HoloJP oshis Suisei and Subaru, we also got to see Hololive EN's Council perform, as well as some of HoloID for the first time.

<figure><p><img src="https://rubenerd.com/files/2023/holofes4@1x.jpg" alt="Key visual from Holfes4 showing all the talents performing over the two days" srcset="https://rubenerd.com/files/2023/holofes4@2x.jpg 2x" style="width:500px;" /></p></figure>

I'll admit, I'm at a loss for words about where to even begin talking about it; they were all spectacular! Some of my highlights were seeing Cali and Gura's banter about finally being idols, Watson's incredible shriek at the start of her song, Bae's amazing chorography, Ina being just as comfy and we all knew she'd be, seeing Reine and Anya debut with HoloID's undisputed diva Moona, and hearing Kronii debut singing *Daydream* with that same cool voice that gave us "*... flower".*

I loved both days, but I think I have to give it to the second day for having music that was more my vibe! As one example, I leaped out of the chair in shock and awe when Mio, Okayu, and Korone performed the cheeky modern disco throwback *[Umarvelous](https://www.youtube.com/watch?v=XWo627F7CeU)*... they pulled it off **so damn well!** If you've never seen the original music video, consider it homework, it's so, so good!

<figure><p><img src="https://rubenerd.com/files/2023/holofes4-holoid-oshis@1x.jpg" alt="Anya, Moona, and Reine performing." srcset="https://rubenerd.com/files/2023/holofes4-holoid-oshis@2x.jpg 2x" style="width:500px;" /></p></figure>

But the stars of the show for me were Reine (top right) from Hololive Indonesia, and Ceres Fauna (below) from Hololive English Council. This was the debut concert for both of them, and Reine delivered her MV *[Illusion Night](https://www.youtube.com/watch?v=gREPKQUoD4o)* with such confidence and style you'd think she'd been doing it with her 3D model for years. Like Bae, her choreography was *on point*, and I couldn't believe her range!

<figure><p><img src="https://rubenerd.com/files/2023/holofes4-fauna@1x.jpg" alt="Fauna at the end of her song." srcset="https://rubenerd.com/files/2023/holofes4-fauna@2x.jpg 2x" style="width:500px;" /></p></figure>

Which leads us to Ceres Fauna, and the performance that may have cemented her as my Hololive EN oshi. Bossa nova has been my favourite music since first hearing Jobim, so hearing her singing her *[Let Me Stay Here](https://www.youtube.com/watch?v=0RMVJTLZOzQ)* original among the falling leaves in lieu of flashing lights felt like worlds colliding in the comfiest way possible. It was fun looking out at the sea of green glowsticks swaying as if in a gentle breeze. I could have listened all day. 🌿

I'm not sure if we'd be able to roll it up with a trip to AsiaBSDCon next year, but I think Clara and I have decided: we'll need to go to the concert for real next year in Tokyo. Better start saving for that, and for the Blu-ray!
