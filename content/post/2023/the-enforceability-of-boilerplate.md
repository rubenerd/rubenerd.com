---
title: "The enforceability of boilerplate"
date: "2023-05-25T09:28:07+10:00"
abstract: "Being boilerplate doesn’t make it optional."
year: "2023"
category: Hardware
tag:
- law
- licencing
location: Sydney
---
If you haven't been watching the controversy surrounding Asus recently, the company's high end AMD motherboards have been implicated in a series of catastrophic CPU failures. The technical details are fascinating, but for the purposes of this post, the company's provided solution was installing a beta UEFI update that would, in the words of the official download site, "void your warranty".

Reactions in the tech community ranged from bemusement to confusion, though I noticed a vocal minority defending the language for being "boilerplate". The fears were overblown, you *obviously* don't need to worry about it, people claiming otherwise were being irresponsible and peddling clickbait, *achyuuyally*, and so on!

I chalked this up to contrarianism, but it was still surprising seeing technical people&mdash;those who pride themselves on the accuracy of their statements&mdash;saying a statement could be ignored. Imagine applying that logic to a licence term in the GPL, or a passage in a code of conduct, or a line in a rental contract, on the basis these documents contain "boilerplate".

[GamersNexus: The Asus Problem and Resolution](https://www.youtube.com/watch?v=xJKzKbqxa0A)

Steve from GamersNexus did a great follow-up video with statements made by Asus, and even asked a local lawyer to define boilerplate and its enforcement, emphasis added:

> Boilerplate language is contract terms that are designed to be applied to a variety of situations without having to adjust them for specifics. Almost always they're going to be very generalised, very broad ... but it **doesn't affect its enforceability**. Boilerplate language can be enforced, and frequently is.
> 
> ... if [the company chooses] to enforce this, versus them having a legal entitlement to do so, you end up with this "fair-weathered friend" problem. As long as everything is going well, the company isn't enforcing its terms or legal entitlements. But for example, when claims become too expensive or numerous, the company always has the option of using this technical language to provide a defence against those claims.

He also made an interesting point on the chilling effect of such language, to which I can speak from personal experience:

> [Consumers] might think they've already voided their warranty, and its not worth bringing these claims.
>
> It also affects attorneys ... who may look at this language and say "I don't think I can bring this claim, it might be too costly, the likelihood of success is less than what we'd want."
>
> ... language does matter.

Steve asked how he'd handle such a case, to which he pointed to the language in the documentation, website, and warranties being vague, ambiguous, contradictory, and self-referential. He argued that as the writers of the contract, the burden would be on Asus to defend it all.

