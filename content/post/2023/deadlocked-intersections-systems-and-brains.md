---
title: "Deadlocked intersections, systems, and brains"
date: "2023-10-09T17:54:29+11:00"
abstract: "Watching an intractable traffic situation reminded me of a computer science class that shook my mind."
year: "2023"
category: Thoughts
tag:
- cars
- computer-science
- traffic
- psychology
- urbanism
location: Sydney
---
Our apartment window overlooks a distant intersection with a decent amount of daily traffic. It's also a thoroughfare for buses, and those articulated ones that deliver fewer people than a double-decker in twice the space. They're also the ones who think pedestrian crossings are overflow, not that I'm bitter or anything!

Due to a combination of terrible street design, and obstinate drivers that break the law by queuing across the intersection, there are situations where it gets jammed for multiple light cycles without any movement. You know it hasn't budged for a while when the horns start.

The bus has the green, but can't move because a car is obstructing their path. That car then gets the green, but can't move because a truck is obstructing their path. The truck can't move when it gets the green, because of... the bus. If you've ever played one of those sliding block puzzles, you get the idea.

It's a physical version of a *deadlock* we know all too well in distributed computer systems. A process might be hung waiting for a transaction, that's locked because the process hasn't finished. We do a ton of work to make sure such situations don't occur, lest we require invoking some percussive maintenance. Only a reboot in the real world is rarely possible.

In physical situations like that intersection, traffic only moves again when someone takes the initiative to solve it. They might start reversing to clear space for another car, which signals to the car behind them to move, and a chain effect starts. Soon, that truck has moved, another car can, and finally the articulated bus that decided blocking three lanes and a pedestrian crossing was a good idea can get on its way. Congratulations, the stack is popping again! Well, until the next imported American light truck arrives.

I feel like I'm carrying dozens of these deadlocks in my head right now. I tell myself I can't do task X, because it's dependent on Y, but that can't be done without Z, which at least partly depends on X. None have right of preemption, and I also can't think of any meaningful metrics with which to weigh them.

I never thought watching a seemingly intractable traffic situation would remind me of computer science 101 classes, or the psychological impact it had learning about this class of problem at the time. What's the mental equivalent of the Bankers Algorithm?
