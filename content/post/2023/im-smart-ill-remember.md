---
title: "I’m smart, I’ll remember!"
date: "2023-05-04T10:12:17+10:00"
abstract: "No I won’t. Write it down."
year: "2023"
category: Thoughts
tag:
- personal
- tasks
location: Sydney
---
How often do you find yourself thinking: *ah, I got this! I'm a smart, modern human being who can remember things! It's only XYZ!*

Each time I have that thought, I've decided to write it down. Nothing more reliably signals I'll forget something than thinking I won't.
