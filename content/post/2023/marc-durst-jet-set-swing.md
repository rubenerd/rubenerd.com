---
title: "Marc Durst, Jet Set Swing"
date: "2023-09-25T16:48:42+10:00"
abstract: "Delightful!"
thumb: "https://rubenerd.com/files/2023/yt-GFz_NF4q_UA@1x.jpg"
year: "2023"
category: Media
tag:
- jazz
- music
- music-monday
location: Sydney
---
Delightful!

<figure><p><a target=_BLANK href="https://www.youtube.com/watch?v=GFz_NF4q_UA" title="Play Jet Set Swing"><img src="https://rubenerd.com/files/2023/yt-GFz_NF4q_UA@1x.jpg" srcset="https://rubenerd.com/files/2023/yt-GFz_NF4q_UA@1x.jpg 1x, https://rubenerd.com/files/2023/yt-GFz_NF4q_UA@2x.jpg 2x" alt="Play Jet Set Swing" style="width:500px;height:281px;" /></a></p></figure>

[View past Music Mondays](https://rubenerd.com/tag/music-monday/)
