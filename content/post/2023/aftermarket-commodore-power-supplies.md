---
title: "Aftermarket Commodore power supplies"
date: "2023-04-07T21:57:34+10:00"
abstract: "Choosing Keelog supplies, and a lesson in supply compatibility."
thumb: "https://rubenerd.com/files/2023/c128-power-supplies@1x.jpg"
year: "2023"
category: Hardware
tag:
- commodore
- commodore-128
- commodore-64
- commodore-plus-4
- reviews
location: Sydney
---
In the retrocomputing space, Jan Beta and Adrian Black make much of the fact you can't trust most legacy Commodore power supplies, as they're not failsafe.They built their own power supply replacements, but I'm not confident enough to do that yet.

[Jan Beta's channel](https://www.youtube.com/channel/UCftUpOO4h9EgH0eDOZtjzcA)  
[Adrian Black's Digital Basement channel](https://www.youtube.com/@adriansdigitalbasement)  
[Adrian Black's second Digital Basement channel](https://www.youtube.com/@adriansdigitalbasement2)

There are a bunch of aftermarket manufacturers, but the two most commonly seen by retrocomputer bloggers and video creators are Electroware (centre) and Keelog (right). To the left is an original "brick of death" from my Commodore 64C that got so hot and smelly after plugging it in, I haven't dared touch it since:

<figure><p><img src="https://rubenerd.com/files/2023/c128-power-supplies@1x.jpg" alt="Photo showing (from left) my Commodore 64C power supply, the Electroware, and the Keelog." style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2023/c128-power-supplies@2x.jpg" /></p></figure>

Both these after-market units promise overvoltage and overcurrent protection, which should reduce the chance of damage. The Electroware comes in an OEM enclosure, with a handy power switch. The Keelog is earthed, which gives me more confidence, though the LED is enough to blind a pilot at 25,000 feet (their newer PSUs don't have this).

I bought the Electroware for my C128, and the Keelog for the C64 and VIC-20, before realising these machines, and my Plus/4, can all operate off the same supplies with adaptors. Whoops! I prefer keeping my machines stock, but I do like the idea of one day swapping them all over to the C64 round connector.

In operation, both units power these machines just fine, and my own rudimentary multimeter tests show they deliver near perfect voltages within the same margin of error as the original Commodore resin bricks I have. I've read a few opinionated people who claim you shouldn't use these home computers with switching power supplies, but I haven't had any issues.

[Lemon64 post claiming that linear supplies should be used](https://www.lemon64.com/forum/viewtopic.php?p=929392#p929392)  
[Keelog power supplies page](https://www.keelog.com/power-supply/)

One minor quibble I had with Keelog ended up being a point in their favour. After initially shipping me the wrong adaptor for another machine, they express shipped a replacement free of charge. This, coupled with the earth pin and the general fit and finish of their supply, leans me towards using and recommending Keelog for future builds.

