---
title: "Inkscape turns 20"
date: "2023-11-20T09:24:15+11:00"
abstract: "The world’s greatest open source vector editor is in its third decade, wow!"
thumb: "https://rubenerd.com/files/2023/inkscape-20@1x.jpg"
year: "2023"
category: Software
tag:
- bsd
- design
- freebsd
- illumos
- inkscape
- graphics
- linux
- netbsd
- open-source
- svg
location: Sydney
---
Speaking of [feeling old](https://rubenerd.com/ninth-anniversary-of-expelled-from-paradise/ "Ninth anniversary of Expelled From Paradise"), I just saw the news over at the Inkscape blog that the world's best open source vector graphics editor [is now in its third decade](). Wow!

I've been using Inkscape since at least 2007. Every SVG in the history of this site was either drawn, modified, optimised, or exported from Inkscape. I owe them a great deal. Thank you to all the contributers, a [donation](https://inkscape.org/support-us/donate/ "Donate to Inkscape") will be on its way shortly :).

<figure><p><img src="https://rubenerd.com/files/2023/inkscape-20@1x.jpg" alt="Creative Growth: 20 Years of Inkscape!" style="width:500px;" srcset="https://rubenerd.com/files/2023/inkscape-20@2x.jpg 2x" /></p></figure>
