---
title: "An even tinier NAS"
date: "2023-07-11T08:38:33+10:00"
abstract: "The Silverelab NAS is a tiny, dense wonder! Thanks to Jeff Geerling for sharing."
thumb: "https://rubenerd.com/files/2023/6-bay-rock5-nas@1x.jpg"
year: "2023"
category: Hardware
tag:
- nas
- reviews
- ssds
- storage
location: Sydney
---
Behind Clara's and my TV sits a monstrously large Antec 300 fitted to the gills with hard drives. It weighs as much as a school bus, and makes even more noise than one when performing ZFS scrubs, but *wow* it works well. I was able to consolidate five machines into the one, thanks in part to FreeBSD bhyve and jails.

Back in January last year I talked about the next step in my dream (literally) NAS: one that uses all-flash storage.

[A home NAS of the future](https://rubenerd.com/a-home-nas-of-the-future/)

This would be a boon for a few reasons. An SSD NAS would be a tiny fraction of the space and weight of my Antec beast; would use less power; would generate less heat; and might even be faster if Gigabit Ethernet isn't the bottleneck.

Jeff Geerling has now done my dream one better, and sourced a NAS that's barely larger than the tiny motherboard hosting it:

[Building a tiny 6-drive M.2 NAS with the Rock 5 model B](https://www.jeffgeerling.com/blog/2023/building-tiny-6-drive-m2-nas-rock-5-model-b)

The photos from Silverlab provide a sense of scale. The board is barely wider than six USB ports, with two stacks of 3 M.2 SSDs. This device could have more storage capacity than my entire tower of hard drives, and in a space barely larger than the floppy drive I have in that case because I'm a nostalgic fool. That's *dense!*

<figure><p><img src="https://rubenerd.com/files/2023/6-bay-rock5-nas@1x.jpg" alt="Prototype photo of the Silverlab NAS, showing its six M.2 bays" style="width:500px;height:333px;" srcset="https://rubenerd.com/files/2023/6-bay-rock5-nas@2x.jpg 2x" /></p></figure>

My first worry would be heat dissipation, but if the use case of this is similar to our NAS, it would only get light to moderate WORM use. It also looks like there's sufficient space between the units to provide space for active air cooling. Even with a beautiful big Noctua fan, this would still be quieter and smaller than any other NAS on the market with that capacity.

You can express interest on the Silverlab site. I'm going to answer this after work today!

[Welcome to the home of Silvertip Labs: Questionaire](https://rpgtavern.live/index.php?page=mininas)

