---
title: "Thoughts on an entirely new Commodore 64"
date: "2023-01-18T08:14:05+11:00"
abstract: "Retro Rewind builds a new machine from new parts. This is HUGE."
thumb: "https://rubenerd.com/files/2023/yt-Qhdc-jgwqVQ@1x.jpg"
year: "2023"
category: Hardware
tag:
- commodore
- commodore-64
- video
location: Sydney
---
The transition of this blog into a [Commodore nostalgia site](https://rubenerd.com/tag/commodore) was as gradual as it was unexpected, especially for a guy who grew up firmly in the 16-bit world of DOS and Windows 3.x.

Nico Cartron and Andrew Wheeler both shared this video by Retro Rewind of Commander X16 fame, demonstrating his Commodore 64 build made entirely from modern parts. His enthusiasm and unbridled joy were infectious. He got a part of his childhood in a modern form, and I couldn't be happier for him.

<figure><p><a target=_BLANK href="https://www.youtube.com/watch?v=Qhdc-jgwqVQ" title="Play THIS CENTURY’S FIRST 100% NEW COMMODORE 64!"><img src="https://rubenerd.com/files/2023/yt-Qhdc-jgwqVQ@1x.jpg" srcset="https://rubenerd.com/files/2023/yt-Qhdc-jgwqVQ@1x.jpg 1x, https://rubenerd.com/files/2023/yt-Qhdc-jgwqVQ@2x.jpg 2x" alt="Play THIS CENTURY’S FIRST 100% NEW COMMODORE 64!" style="width:500px;height:281px;" /></a></p></figure>

It also sends chills down my spine for all the right reasons. Having reverse engineered and sourced replacements for every single part, it means the venerable C64 "will never die". If you want a new C64, you can build one. If you have an old C64 with a broken part, you can replace it. Retro Rewind's bill of materials in the video description is a sight to behold, even if he didn't get a couple of them in time to make the video. Even though it's not essential to the operation of the machine, I'm so happy that there's even a great set of keycaps now!

Taking a step back though, you'd fall off a cliff. We've reached the point where serious technical enthusiasts, armed with off the shelf components, FPGAs, programming knowledge, and an understanding of electronics, are able to create socket-compatible components that a 1980s chip foundry could. Couple that with injection moulds, 3D printing, lithographic advancements, video creators with large audiences, and a community of interested fans offering feedback, views, and money, and it's feasible for a community in 2023 to recreate an entire 1980s computer. 

It's easy to feel dejected at the current trends and state of contemporary IT, but it was this confluence of technical capabilities, circumstances, and interest that lead to this project being feasible. Not to get too melodramatic, but *this* is the future I was looking forward to!

There's also a practical consideration to what Retro Rewind and all the contributors have achieved here. Mat from Techmoan cautions people interested in getting into retro Hi-Fi not to buy second-hand systems to start, even if they're technically superior and more interesting, because it will entail troubleshooting that can be frustrating and put people off. A modern Commodore 64 is a gentler introduction to the world of 8-bit machines as well, especially considering the original kit is approaching forty years old.

There are modern versions of the C64 that use emulators, but a version built from parts that replicates the *architecture* and feel of the original machine is a whole other kettle of fish sprites.

Congratulations to Retro Rewind, and thanks to everyone's tireless efforts creating all these components. You're not just fixing a computer, you're preserving our history and bringing joy to a new generation of 8-bit enthusiasts :).
