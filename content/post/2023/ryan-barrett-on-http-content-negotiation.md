---
title: "Ryan Barrett on HTTP content negotiation"
date: "2023-03-25T08:39:47+11:00"
abstract: "A nice idea in theory, but modality should be best avoided."
year: "2023"
category: Internet
tag:
- development
- quotes
location: Sydney
contributer:
- https://snarfed.org/2023-03-24_49619-2
- https://www.manton.org/2023/03/24/i-like-this.html
---
I've been bitten by this before, and [agree with Ryan's thoughts](https://snarfed.org/2023-03-24_49619-2)\:

> Content negotiation is a feature of HTTP that lets clients ask for, and servers return, different content types based on the request’s Accept header.
> 
> Sounds great, right? Well, no. Content negotiation is the classic example of an idea that sounds good in theory, but for the vast majority of web developers, turns out to be net harmful in practice. 

I also think his conclusion could be applied to *so many* things; emphasis added:

> I think most of this boils down to: modality generally considered harmful. When something always behaves the same way, it’s reliable and easy to use. When it **behaves differently based on something far away that you may not know exists, it’s unreliable and surprising**. Add in a very large ecosystem of independent tools that all need to interoperate, often in fine-grained ways, and you have a recipe for failure.

Thanks to [Manton Reece](https://www.manton.org/2023/03/24/i-like-this.html) for sharing this.
