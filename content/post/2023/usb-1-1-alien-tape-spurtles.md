---
title: "USB 1.1 Alien Tape Megasaur Spurtles"
date: "2023-04-11T13:41:27+10:00"
abstract: "What about a Jungle Megasaur?"
year: "2023"
category: Hardware
tag:
- advertising
- australia
- pointless
location: Sydney
---
Back in January I mentioned a device at our local Australia Post, sold under the obviously reputable *As Seen on TV* brand. It was like stepping through a time warp.

[Blog post about the Battery Daddy](https://rubenerd.com/obsolete-tech-the-battery-daddy/)

Today I found myself back at that post office, and decided to note the first five non-philatelic items I saw. These are quoted verbatim, double checked for typos, and without further comment:

* Multifunctional Alien Tape
* A greeting card with a lizard saying “I’m so frilled”
* A Jungle Megasaur
* Egg Chalk
* The Original Spurtle

I was also tempted by their memory keys which promised USB 1.1 compatibility. It's still too new for most of my computers.
