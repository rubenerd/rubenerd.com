---
title: "Using the “right” social networks"
date: "2023-07-24T07:27:21+10:00"
abstract: "Abstaining from Facebook let me stick it to the man, and also lead to social isolation."
year: "2023"
category: Internet
tag:
- facebook
- social-media
location: Sydney
---
For all the frustration I have with social networks, I chat and post to them regularly. For an introvert, short snippets of text give me an avenue to stay connected with people I care about, at my own pace, and on my own terms.

Much of my blog from fifteen years ago was spent lamenting the rise of Facebook, a social network I loathed for what it represented, and for the bellend who founded it. My perceptions have changed little about either one, though the latter's bumbling waste of money on the ill-conceived metaverse has been entertaining.

Abstaining from that platform resulted in two things:

* I felt good sticking it to the man. I wasn't contributing my eyeballs to their ad revenue, and I wasn't encouraging others to be there.

* I drifted apart from people who used it as their primary avenue of communication, and missed many important milestones in their lives.

One thing it didn't achieve? The downfall of Facebook. All it achieved was social isolation. This has been a bitter pill for me to swallow.

The *Network Effect* ensures the utility of a social network comes not from its architectural or ethical attributes, but whether one's friends are there. We can be advocates for alternatives, but at some point there's also the question of cutting off one's nose to spite one's face.

This is why I don't judge people for using a network I might otherwise find distasteful, and why I think it's counterproductive to shame people into changing. We move the needle by demonstrating how much better something else is.

