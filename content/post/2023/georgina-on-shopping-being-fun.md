---
title: "Shopping, featuring Georgina"
date: "2023-12-20T12:48:36+11:00"
abstract: "THere are entire industries designed to exploit weaknesses in our lizard brains."
year: "2023"
category: Thoughts
tag:
- psychology
- self-esteem
- shopping
location: Sydney
---
Few activities come with such *baggage*&mdash;thank you&mdash;as shopping. We all have to do it, whether it be for cooking, clothes, or coffee (cough). We might *shop around* for essentials like rent, utilities, and business expenses. We feel proud when we're able to *save* on a shop, because we're clearly being smart and fiscally responsible.

Shopping is, in every sense, serious business. It's the primary driver of economic growth (for better or worse). It can be a bonding exercise, such as shopping for a LEGO set to build together, a wedding dress, or for movie tickets. It can be motivating, like saving up to spend on a holiday or hobby. The outcome of shopping can also be a source of great joy, like a Christmas present, a celebration of a life milestone, or a surprise for a job well done.

But we're all well aware of its dark side. There are schools of thought that decry the accumulation of *stuff* as materialistic, with any accrued happiness being superficial, hollow, and fleeting. George Carlin pointed out those conspicuous consumers who buy things they don't want, with money they don't have, to impress people they don't like.

Worse, those who enjoy the act of shopping itself can find themselves drowning in easy, cynically advertised credit, just as a gambler can find themselves underwater at the pool table. Thank you, I'm here all week.

Georgina recently wrote a [brutally honest and candid post](https://hey.georgie.nu/no-shopping/ "No shopping") about her own journey with clothes shopping. She discussed it being a coping mechanism, and how online shopping was always available as a distraction. Eventually the volume of packages brought shame, and ran counter to her ethical perspective on sustainability. She concluded:

> As I fight to be patient and wait for interested people to buy my second-hand clothes online, and as I educate myself more about sustainable fashion, and as I dive into trying a rental wardrobe of curated pre-loved pieces—I know change doesn’t happen overnight, and I strive to make small steps of progress. But my shift in attitude, from adding everything I only remotely like to a virtual cart on a screen, to the buildup of resisted urges to open an app just to look–is a sign that I’ve advanced further than I thought. And I’ll take that.

I see this in used electronics too, which is another reason why the right to repair is so critical. The world is full of **stuff**, and we can help the planet and ourselves make *use* of it, rather than throwing away or even recycling to make **new** stuff. There's a reason the order is *reduce, reuse, recycle*.

I can even see echos of how I've lived my own life recently. I never considered myself a *shopper*, and my fashion sense can best be described as *UMY: Uniqlo,  Muji, YouTuber Merch*. But I spent my formative years looking in Singaporean computer stores, and some of my most treasured childhood memories were literally building things I bought with my parents. In recent years, I've found myself with a regular stream of retrocomputer stuff, homelab server parts, obscure Hi-Fi gear, coffee making paraphernalia, and anime figs. I've been in the same situation Georgina was, only my virtual cart on eBay at 23:00 was full of Token Ring network cards from the 1980s.

Clara and I spend well below what we earn, in part because my parents were so bad with money and it scared the shit out of me. I refuse to carry forward credit card debt (cards are only for points and insurance), and every major purchase or holiday has to be saved for in advance. I'd also promised myself that any new thing is a replacement for something else, meaning I have to get rid of a thing before buying another one. When you live in a tiny apartment in stratospherically-expensive Sydney, you have no choice. 

*BUT...* this year has contained the first few times in my life where I've come close to buying something ridiculous on a whim, with money that either isn't mine or in my budget. I *feel* that dopamine rush and empathise with getting hooked on it; especially for something as intertwined with self-esteem as clothes.

There are *entire industries* crafted to exploit weakeners in our lizard brains, and in this case, buy stuff. Confronting this head on, and making a deliberate effort to fight it, is tough.
