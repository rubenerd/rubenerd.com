---
title: "Moving off iTunes after two decades"
date: "2023-01-23T09:03:47+11:00"
abstract: "Apple no longer prioritises jukebox software anymore."
thumb: "https://rubenerd.com/files/uploads/media.iTunes6.png"
year: "2023"
category: Software
tag:
- apple
- history
- itunes
- music
location: Sydney
---
I initialised my first iTunes music library back in 2001, on my blueberry iMac DV. I created a new one a couple of years later on my then-new iBook G3 to sync with my FireWire iPod, and I've carried it with me ever since.

This library has been (re)imported and moved onto every Mac I've ever owned, and on a couple of Windows machines. I've added songs, created playlists, bought my first digital music, and synced it with every one of my iPods and iPhones, along with a few Palms using iSync. It received audio magazines, New Time Radio programmes, and podcasts from iPodder and Juice Receiver before it even got [podcasting](https://rubenerd.com/p1164/) support. It's what I listened to live Whole Wheat Radio house concerts on, and was my first attempt at archiving before offloading video to Plex. It wasn't the first software I burned or ripped CDs with, but it was the one I used the most.

<figure><p><img src="https://rubenerd.com/files/uploads/media.iTunes6.png" alt="A screenshot of iTunes in 2007, during better times." style="width:500px;" /></p></figure>

It was with me when I left home for university, when I got my own place, and when I moved in with Clara. It's been fun seeing my musical tastes evolve and change over time, but I can re-live those heady days of the early 2000s by sorting by *add* or *publish* date. It's been a welcome distraction and a source of strength during tough times, especially when I didn't really have a permanent base.

This all may sound like fawning embellishment, but I want to emphasise just how important this software, its music, and its rich history of metadata have been to me over the years. iTunes ran on Windows, but it was the *killer app* for the Mac for me. I used Winamp prior to it, and dabbled with other third party software, but as the [history of this blog attests](https://rubenerd.com/tag/itunes/), iTunes has been the centre of my digital world for a long, long time. And in the words of Alanis Morisette *I know I'm not the only one*.

<figure><p><img src="https://rubenerd.com/files/uploads/icon.itunes1.jpg" alt="The Classic iTunes icon" style="height:128px; width:128px;" class="transparent" /></p></figure>

In 2019 Apple replaced iTunes with Apple Music/Music.app, and delegated many of its functions to separate applications. iTunes was burdened with an ever-increasing list of features to support functions, mobile devices, and stores for which it was never designed, and it was beginning to show.

But it was clear to anyone who'd invested years into curating iTunes playlists and music collections that Music.app was no replacement, as we all feared. The superficially similar interface belied an embarrassing number of bugs, missing features, and poorly-implemented UI. It's yet another example of Apple's well-publicised, conspicuous, and baffling slide in software quality over the last decade.

Among the litany of problems, the way Music.app handles local albums is the barometer with which I gauge how much Apple cares. For whatever reason, the software routinely splits local albums in two in the Albums view, despite having made no changes to the metadata. I tend to play whole albums, so I'm constantly missing tracks when playing them.

*(Before anyone chimes in saying it doesn't happen to them, or that there are workarounds, appreciate for a moment that iTunes never had this problem for anyone, even back in the Mac OS 9 days. We've slid a long way when people reflexively defend modern software for being less functional and robust than what our G3s used to run).*

We've come to expect beta-quality software for new Apple releases, but the fact it's been almost half a decade and the software still sucks is a worrying sign of the company's priorities, at least for those of us who grew up in an era when the iPhone was billed first as a *widescreen iPod with touch controls*.

I like to think that companies with endless cash and talent could fix these problems if they had the inclination, but the reality is they'll prioritise what makes them money. Apple has moved on from jukeboxes, music stores, and iPods, whether old school customers like me care or not. They don't see their expensive devices as targets to rip/mix/burn music to, or to sell you a song or album to sync to, but as a recurring source of streaming revenue, listened through wireless earbuds that require regular replacement when their non-serviceable batteries wear out.

I'd say its an ignominious end, but the only ignoramus is me for holding on as long as I have!

So where to from here? This is where I need your help. [Let me know](https://rubenerd.com/about/#contact) what jukebox software you run and like your machines. I've delegated portable music playing [to my Walkman](https://rubenerd.com/my-new-sony-nw-a55-walkman/), so all I need is something reliable with a decent interface. It's been so long since I've had to look, I wouldn't even know where to start.
