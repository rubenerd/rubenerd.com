---
title: "A “read more” link in GoHugo"
date: "2023-06-04T16:40:41+10:00"
abstract: "Using .Truncated, .Summary, and defining summaryLength."
year: "2023"
category: Internet
tag:
- blogging
- hugo
- guides
location: Sydney
---
Hugo supports the WordPress-style <code>&lt;!\-\-more\-\-&gt;</code> tag in blog posts, which split your text into a summary and body. What if you wanted your template to always show the full text of a post, *unless* you provided a manual summary?

Define this in your `index.html` template:

<pre><code>{{ if .Truncated }}
    {{ .Summary }}
    &lt;a href="{{ .Permalink }}"&gt;Read more&hellip;&lt;/a&gt;
{{ else }}
    {{ .Content }}
{{ end }}
</code></pre>

This almost works. Hugo automatically generates summaries for posts that don't include them, so `.Truncated` always evaluates true by default.

The trick (hack?) is to define a massive default summary length in your site config, so it will (practically) never autogenerate them:

    summaryLength = 999999

Now the template will show a summary for posts that have them, otherwise they'll default to showing the whole post.
