---
title: "A basic ASRock board for my FreeBSD tower"
date: "2023-08-31T08:06:55+10:00"
abstract: "You don’t always need the best or latest, surprising nobody!"
thumb: "https://rubenerd.com/files/2023/asrock-for-ridge@1x.jpg"
year: "2023"
category: Hardware
tag:
- amd
- bsd
- freebsd
- hardware
- shopping
location: Sydney
---
Clara bought me a slim Fractal Ridge case in white for my birthday earlier this year, which has let me downsize my FreeBSD tower to save space for my burgeoning retrocomputer stack.

The case has plenty of space for all but the most *unobtainium* graphics cards, but the lower motherboard chamber only has space for a slim CPU cooler. You *really* want to maximise every available millimetre of space for cooling you can, which means going for something like a Noctua NR-L12S.

[Noctua NR-L12S](https://noctua.at/en/nh-l12s)   
[Fractal Design Ridge](https://www.fractal-design.com/products/cases/ridge/ridge/white/)   
[Asus ROG Strix Z570-I Gaming motherboard](https://rog.asus.com/au/motherboards/rog-strix/rog-strix-x570-i-gaming-model/)

But just as Manhattan and Singapore maximised space by building up, so too have Mini-ITX board manufacturers. My overspec'd Asus ROG Strix Z570-I (gesundheit) came with an IO shield, a massive VRM heatsink, and an M.2 stack that doesn't encumber tall coolers or AIO pumps, but they encroach on the space needed for a slim cooler. Clara already needed an upgrade for her 8th gen Intel game machine, so I gave her this souped-up board and went looking for something smaller.

I originally bought the Strix motherboard having convinced myself I needed the extra features... and proceeded to use none of them. It's the same pattern of behaviour that lead me to buying more expensive laptops, cameras, and phones in the past on the *off chance* that someday I might need their additional capability. In reality, I was suckered in by price bracketing, and spent more than I needed.

Back in May, I landed on a budget ASRock B550M-ITX/ac on special:

<figure><p><img src="https://rubenerd.com/files/2023/asrock-for-ridge@1x.jpg" alt="Photo of the board populated with a Ryzen 5700X" style="width:500px;" srcset="https://rubenerd.com/files/2023/asrock-for-ridge@2x.jpg 2x" /></p></figure>

It's perfect! It's low enough to fit a larger slim cooler, was a third the price, and only included things I actually use. That board, combined with a budget Ryzen 5700X from the previous generation, still came out cheaper than just the Strix board from a couple years ago, and certainly cheaper than the current 7000 series. And because the 5700X is a 65 W part, it doesn't get too toasty with this slim Noctua cooler. 

[ASRock B550M-ITX/ac](https://www.asrock.com/mb/AMD/B550M-ITXac/index.asp)   
[AMD Ryzen 7 5700X](https://www.amd.com/en/products/cpu/amd-ryzen-7-5700x)

Here it is mounted in the case with the Noctua NR-L12S, just before installation of the power supply. Yes, those are Bluey pajamas.

<figure><p><img src="https://rubenerd.com/files/2023/asrock-installed@1x.jpg" alt="Photo of the board populated with a Ryzen 5700X and the Noctua cooler inside the Fractal Ridge" style="width:500px;" srcset="https://rubenerd.com/files/2023/asrock-installed@2x.jpg 2x" /></p></figure>

If this was your only computer, you might want to spend extra for a higher-end board with more M.2 slots and USB ports. But for my use case running FreeBSD as my daily driver, and Fedora for Steam, I've noticed zero difference between this and the expensive Strix board I used before. Actually that's not completely true; ASRock's BIOS configuration is much easier.

I'm sure I'll continue to make the mistake that *no, I have to buy the better thing, damn it!* But it's always a pleasant surprise when you come out ahead with something that's perfect for your use case.

Now if you'll excuse me, I'm off to fetch some KDE.
