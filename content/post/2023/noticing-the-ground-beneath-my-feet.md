---
title: "Noticing the ground beneath my feet"
date: "2023-12-04T18:14:39+11:00"
abstract: "It seems ridiculous, but noticing what I was walking on changed something, at least briefly."
year: "2023"
category: Thoughts
tag:
- anxiety
- books
location: Sydney
---
I've been reading *[Unwinding Anxiety](https://www.kobo.com/au/en/ebook/unwinding-anxiety-3)* by Dr Juston Brewer which has been equal parts rewarding and overwhelming.

One thing he points out is that mindfulness isn't achieving a zen-like state on a mountain top, but instead being aware of the things you do on autopilot. He cites a study that we spent about half our lives performing rote actions we're not even conciously aware of. Peversely, these habitual things can be a source of anxiety, and recognising what we're doing in real time can be a way to break out of it.

I haven't reached that level of understanding yet! There's also no way of writing this without sounding a bit daft. But having told myself I'd try and be more aware of what I was doing, I walked out of the house, and I *felt the ground*.

The carpet in our apartment hallway gave slightly as I walked on it. The concrete didn't. The steps in the stairwell felt weird: I didn't notice that the arch of my feet wrap along the edge of the step. Wow, I have big feet. The tiles were *really* weird: sometimes I'd feel the gaps between them, other times I wouldn't, it all depended on where my big feet landed.

I've been a bipedalled organism my whole life, save for a couple of broken ankles, and crawling before I could walk. I've also noticed nice carpets in a new place, or felt the exhaustion in my feet walking in concrete all day. Nothing here should have been new, interesting, or surprising.

But you know what? I didn't realise until I reached the supermarket that my heart rate was lower. *LOWER!* And despite walking at a fairly brisk pace! And all it took was me being aware of just a *tiny sliver* of my surroundings as I walked. There is *so much more* out there, and I was ignoring it all. Well, not *ignoring*, more that I was unaware.

It's weird to think that boredom and stress can come from the same place.
