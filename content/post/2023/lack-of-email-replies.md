---
title: "Lack of email replies"
date: "2023-10-20T08:23:27+11:00"
abstract: "As always, thank you for your patience and kind words."
year: "2023"
category: Internet
tag:
- feedback
- weblog
location: Sydney
---
I've let a bunch of replies from people accumulate in my Inbox again. My apologies for not getting back to you yet. I'm in a bit of a weird mental state at the moment where writing is a cinch, but email or calls are... terrifying? For want of a better word?

As always, thank you for your patience and kind words. It means a lot.

