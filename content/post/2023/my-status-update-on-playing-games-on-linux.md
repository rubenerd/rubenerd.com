---
title: "My status update on playing games on Linux"
date: "2023-01-28T08:37:33+10:00"
abstract: "Patience is a virtue."
year: "2023"
category: Software
tag:
- games
- linux
location: Sydney
---
I've talked about how I dual-boot my FreeBSD tower with Linux to play games. I moved from Kubuntu back to Fedora that I used to run for years, and stuck with it since.

This has lead to a few questions asking:

* Why would you run Linux to play games?
* Is it worth it?
* Should I switch from Windows?

The short answer to all of these is: it's worth it if you already use Linux, or dislike Windows. Stuff mostly works, but you have to be willing to put in the time to troubleshoot things.

> The game launcher doesn't work... oh, it's because you installed the Flatpak version from the official distro app store, just install the jar file directly. The status bar tool no longer shows the temperature of my GPU... oh, it's because the tool you used has been replaced with another one with a different name, check this Reddit post. Sound stopped working... oh, you just need to change the output a few times. HiDPI scaling got reset... oh, just change it back. My screen started tearing like crazy... oh, you need to enable this option, and use this cron hack to make it persistent. Icons on menus look tiny now... oh, just set this environment variable. The machine no longer suspends/resumes properly... oh, that means you haven't set this flag with the most recent GPU driver to do memory stuff. This new bug appeared... oh, that's because you're using a more bleeding-edge distro, you should use an LTS version. This problem is still happening... oh, it's because your distro is old/stale, try a recent one. The password for this game or app store isn't saving... oh, that's because you're using keychain X not keychain Y. Why does Gnome report this game has crashed when it hasn't... oh, just ignore that, but remember to dismiss it before the game goes full screen or only a portion of it will show up. The joystick no longer appears... oh, just reboot. The desktop shows garbled text for some reason... oh, just do a `killall -3` or log out and in again. This program no longer launches... oh, that's a known issue with the most recent Wayland which you must be using, switch back to xorg. I don't have any of these problems, it's clearly PEBKAC... oh except for that one, that's a known bug in kernel blah, you just just need to...

Most of these issues can be attributed to using binary Nvidia drivers, games and software not having the same level of testing and polish as they do on Windows, the general moving target Linux represents, and only having two decades of Linux experience not three. I'm half tempted to replace my 3070 with an AMD to solve half these problems, but then I'm not sure what FreeBSD support is up to with that for when I boot back into that (Nvidia drivers work great on FreeBSD).

Linux gaming has come a *long* way, thanks in no small part to the efforts of Valve, Proton, and Wine. It's also a familiar enough environment for someone who spends most of his time on FreeBSD, and people on forums, Discord servers, and IRC are helpful for solving problems.

Nothing I wrote above was a show-stopper, but it's also not even a complete list. I don't want to give people the illusion it's simple; my mum used Ubuntu and she wasn't technical... but she also didn't game on it.

The Year of the Linux Desktop is asymptotic; we get ever closer to it being true, even if it feels like we never get there. I'm sure games will be turnkey soon enough; the Steam Deck shows it's possible. 
