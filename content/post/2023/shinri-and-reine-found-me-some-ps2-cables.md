---
title: "Shinri and Reine found me some PS/2 cables!"
date: "2023-11-29T21:00:58+11:00"
abstract: "4 down, 12 to go."
thumb: "https://rubenerd.com/files/2023/ps2-holo@1x.jpg"
year: "2023"
category: Hardware
tag:
- kvms
- retrocomputing
location: Sydney
---
Last Saturday I talked about [buying a KVM](https://rubenerd.com/always-buy-kvms-with-cables/ "Always buy old KVMs with cables") for my stack of old computers, on account of having no space. The unit didn't come with cables unfortunately, and tracking down ones that didn't end up costing as much as the unit itself was difficult.

Fortunately, Holostars EN's Shinri and Hololive ID's Reine found me four PS/2 cables by keeping an eye out on saved searches! My friend Robin also says they may have some too. Eventually I'll amass enough of these to plug everything in.

<figure><p><img src="https://rubenerd.com/files/2023/ps2-holo@1x.jpg" alt="Photo showing the edge of that Master View KVM with four cables, and acrylic stands of everyone's favourite characters from HoloID and Holostars EN" style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2023/ps2-holo@2x.jpg 2x" /></p></figure>

This also leads me to a follow-up question: how should I be tracking each of these cables? Should I get some of that heat shrink tubing to bundle them together with a VGA cable? I could do that for Ethernet as well, then put the switch above the KVM.

Sound was also another thing I didn't consider. They all have PC speakers for the bleeps and bloops, but I'll need to route some 3.5 mm audio jacks somewhere.
