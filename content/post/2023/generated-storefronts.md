---
title: "The brave new world of procedurally-generated stores"
date: "2023-04-06T07:21:38+10:00"
abstract: "These are the inevitable end game of advertising on social media."
thumb: "https://rubenerd.com/files/2023/junk-stores.png"
year: "2023"
category: Internet
tag:
- advertising
- design
- spam
- twitter
location: Sydney
---
Have you ever had a sense of *déjà vu* browsing the web, as though you've seen something before? Wait, as opposed to a sense of déjà vu having *not* seen something before? How does that make sense? Where am I? What am I looking at? Have I made this mistake before?

Well today I'm thrilled, excited, and amazed to introduce you to the *Lese* store! Not your thing? What about *Razo?* Too expensive? I'm sure *Buto* has you covered. Or *Zono*. Or *Dazo*. Or *Bupo*, surely you can trust a name like that. Here are but a few more:

<figure><p><img src="https://rubenerd.com/files/2023/junk-stores.png" alt="A selection of stores ranging from Hese, Razo, Daco, and the legendary Buto." style="width:500px;" /></p></figure>

There are *hundreds* of these stores that have made it into my blocklists, all peddling the same junk under the same procedurally-generated logos and names. They have the same grammar mistakes in their descriptions, their advertisements all have the same "five minute craft" style videos, and the comments are all suspiciously excited about ill conceived and poorly constructed novelties. I haven't ever clicked through to their "stores", but I suspect they're also the same.

As with all auto-generated content released into the wild without human oversight (*cough* chatbots) there are hilarious, if unintended, accidents. While most of the stores sport forgettable shapes and colours, a few have... shall we say, *phallic* connotations. The generated names have lead to the likes of *Bozo* and *Dumm*, which is perhaps how they're able to defend themselves under those pesky *truth in advertising* laws.

*(Yes that's a joke, I very much doubt they're concerned with the legality of what they're doing! Though it does make me want to test how far I'd get trying to return a defective product to a reputable store like Buto, Zono, or Dadi; because we all know this crap will break if its not immediately thrown away).*

While those are "fun", other logos and names aren't. I've seen a couple of accidental swastikas, and names charged with racial slurs. It's the *Wild West* when it comes to this stuff, and anytime I see someone defending them on the quaint basis that *tech is inherently neutral*, I feel like I need a shower under a holographic, coconut-shaped shower nozzle to cleanse myself of the *Razo*. Or Dilo, or something.

If the electronic spam these multiheaded spammers regurgitate onto the web weren't bad enough, the offensively low-quality rubbish they waste fuel shipping around the world is even worse. It's all destined for landfill and incineration, if it doesn't end up in the ocean. It's repugnant how much of the world's precious and limited resources are being spent on this *objectively* useless trash, and the fact it's thrown in my face on an hourly basis by these auto-generated spammers just adds insult to injury.

**Whoa**, that got a bit blue there. Maybe I need some plastic, tinted glasses with pandas on them to salve my frustrations.

Ethics and the environment aside (a phrase I'm becoming tired of saying, but that's a whole other can of novelty pepper shakers), the existence of these shell accounts are interesting for other reasons.

The advertisements these fake companies buy are very much real. I don't know what the asking rates are for Musk's dumpster fire, but any price greater than free has traditionally presented a sufficient barrier to entry for most spammers. The fact they're buying *thousands* of impressions says the ads are working, or they're fly-by-night operations with outstanding invoices that the system hasn't cottoned onto yet. The latter honestly wouldn't surprise me, given the shambles the rest of the site is now.

The proliferation of these accounts demonstrate one of the fundamental, structural limitations of commercial social media. Human moderation, whether it be the social media owners or account holders, can't match the bandwidth of an automated farm of fake accounts. It's hard not to see it as the inevitable endgame of these platforms. And no, AI won't be the answer, for the simple reason that both sides have access to it.

The reason for these spam accounts is clear: they're designed to work around people blocking them. A single store can be blocked and forgotten, but you can wear down someone's defences if you continuously pummel them with garbage from thousands of profiles. At some point, you start to give up trying to hide them. Which an ad-dependent platform would consider *good*.

This is part of what Cory Doctorow refers to as *[enshittification](https://pluralistic.net/2023/01/21/potemkin-ai/#hey-guys)*.

And that's the rub. Twitter is surely aware of the issue of spam advertising, but are willing to reduce the quality of their network to make a quick buck. Again, as always, it comes down to incentives, regardless of how short sighted or self destructive they turn out to be. The idea that *we* are the product on a free site is long antiquated; we don't even factor into their decisions at all.

Maybe someone needs to send their remaining management a few thousand boxes of dancing avocado peelers.
