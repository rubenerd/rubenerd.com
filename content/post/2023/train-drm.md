---
title: "Train DRM"
date: "2023-12-13T13:01:19+11:00"
abstract: "“We found that the PLC code actually contained logic that would lock up the train with bogus error codes”"
year: "2023"
category: Hardware
tag:
- drm
- security
- trains
location: Sydney
---
*[Speaking of trains](https://rubenerd.com/bangkok-mrt-monorail-open-to-passengers/ "Bangkok MRT monorail open to passengers")!* We all know how digital-restrictions management (DRM) schemes prevent people accessing what they paid for, encourages the very piracy they're nominally designed to avoid, are questionably legal, and represent a potential privacy and security risk.

But [this Mastodon thread by @q3k](https://social.hackerspace.pl/@q3k/111528162462505087 "@q3k Mastodon with photos of the locomotive with the uncovered DRM") is next level, emphasis added:

> We ([@redford](https://infosec.exchange/@redford "Redford's Mastodon account"), [@mrtick](https://infosec.exchange/@mrtick "mrtick's Mastodon account") and I) have reverse engineered the PLC code of NEWAG Impuls EMUs. These trains were locking up for arbitrary reasons after being serviced at third-party workshops. The manufacturer argued that this was because of malpractice by these workshops, and that they should be serviced by them instead of third parties.
> 
> We found that the PLC code actually contained logic that would lock up the train with bogus error codes after some date, or if the train wasn't running for a given time. One version of the controller actually contained GPS coordinates to contain the behaviour to third party workshops.
>
> It was also possible to unlock the trains by pressing a key combination in the cabin controls. None of this was documented.

@q3k goes into detail on his post, with photos and detail of their discovery process. It's jaw dropping.

I suppose trains are just giant computers on wheels now. If John Deere can do it with farm equipment, why not a locomotive?
