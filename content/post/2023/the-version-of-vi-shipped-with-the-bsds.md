---
title: "The version of vi shipped with the BSDs"
date: "2023-09-02T16:24:15+10:00"
abstract: "It’s not Vim, despite articles suggesting otherwise."
year: "2023"
category: Software
tag:
- bsd
- dragonfly-bsd
- freebsd
- netbsd
- openbsd
location: Melbourne
---
Hackaday ran a story about Vim with some interesting history. I knew the late Bram Moolenaar had started the project on the Amiga, though I didn't know the history of the original code going back to Tim Thompson's Stevie on the Atari ST, and the ports made by Tony Andrews. I swear the world is conspiring to get me into buying a 520 or 1040ST.

[On Vim, Modal Interfaces And The Way We Interact With Computers](https://hackaday.com/2023/09/01/on-vim-modal-interfaces-and-the-way-we-interact-with-computers/)

It did have this one tidbit though:

> Vim is still the editor you’ll usually find installed by default on Unix, BSDs, Linux distributions and others, whether in its vi-compatibility configuration or full Vim glory.

I get what the author is saying here, but it's not strictly true.

They're right that most Linux distributions include Vim in vi-compatibility mode, but the BSDs don't have Vim by default. Instead they use their own variants of nvi, itself based on elvis. While not strictly compatible with the original vi, it's arguably closer in functionally than Vim.

[Trying out the nvi editor](https://rubenerd.com/trying-nvi/)

I taught myself how to use it back in 2010. I do like Vim's niceties, but I've come to just use nvi on FreeBSD and NetBSD servers I maintain. Why not, if BASE ships with it by default?

And for the sake of completeness, illumos ships with the original BSD vi, as I learned from browsing their hosted manpages:

[illumos: vi(1HAS)](https://illumos.org/man/1/vedit)
