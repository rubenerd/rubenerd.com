---
title: "Computers don’t do what you “want”"
date: "2023-01-04T09:28:31+11:00"
abstract: "They do what you tell them; except when they don’t."
year: "2023"
category: Software
tag:
- computer-science
- maxims
- programming
location: Sydney
---
This is among the first maxims we're taught during computer science or IT classes at university:

> The computer doesn't do what you want;   
> it does what you tell it.

It's useful to think about. There's no [spirit of the law](https://en.wikipedia.org/wiki/Letter_and_spirit_of_the_law) for computers; they live in absolutes. A program you write will be executed faithfully to perform the actions you prescribed, regardless of our aims. We may sometimes anthropomorphise or project ill intentions on these damned machines, even go as far as to pretend they have artificial intelligence. But that's on us.

It's also not universal. Developers, like physicists, live in the abstract; only to be rained on by the physical world. Faulty silicon, degraded physical connections, and unreliable networks can result in computations being done that aren't what you want, *or even what you told it*. The [Pentium FDIV bug](https://en.wikipedia.org/wiki/Pentium_FDIV_bug) is a classic example, but it could even be something as mundane as coming short in the [silicon lottery](https://www.makeuseof.com/silicon-lottery-why-no-two-processors-are-the-same/), or the heatsink falling off your RAID controller at 03:00.

Sure there's exception handling, hashing, and defensive programming, but ultimately most consumer software and languages are written with the implicit assumption that their operating environment can be trusted to do *what it's told*.

The more time you spend in the industry, the more that infallibility façade falls apart, along with the hubris that one can check every single operating parameter before each computation. In a real Sir Terry Pratchett sense, it's assumptions all the way down.

Perhaps a more accurate version should be that computers don't do what you want, they do what you tell them, except when they don't... and in which case, you'd better hope your stuff is robust enough to handle it.

*Fscking machines!*
