---
title: "A phantom text buffer"
date: "2023-02-09T15:24:13+11:00"
abstract: "What was there? Who knows!?"
year: "2023"
category: Software
tag:
- pointless
- text
location: Sydney
---
I had an empty, unsaved text file open in Vim. I went to close the window, like a gentleman, but it asked if I wanted to save changes. Undo didn't show anything, and I didn't have anything in the clipboard.

Now I'm left wondering what ephemeral thoughts were once there, that will now never see the light of day. Was it a glorified scratch space? Something I copied out but forgot to paste before clearing the clipboard? Who knows. Maybe it only ever had a non-printable character, or a butterfingered space.

These are the sorts of low-impact, pointless things that occupy my thoughts far more than they should. Somewhere in the multiverse there's a version of me with that same buffer open, and it still retains its original text and ideas. What does it say?

At least I know for sure it doesn't have this specific post, which only came about because that empty buffer happened. Unless entropy strikes and by sheer fluke I did a keysmash and these exact characters appeared. 
