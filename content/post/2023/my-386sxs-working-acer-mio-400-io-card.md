---
title: "My 386SX’s working Acer MIO-400 IO card"
date: "2023-03-27T15:08:10+11:00"
abstract: "This card lets me connect almost everything I’d need onto my 386SX board."
thumb: "https://rubenerd.com/files/2023/asus-mio-400@1x.jpg"
year: "2023"
category: Hardware
tag:
- am386
- nostalgia
- retrocomputing
location: Sydney
---
Last Saturday I fixed an [old 1991 DFI 386SX motherboard](https://rubenerd.com/my-386sx-16-20cn-rev-0-motherboard/) that I'd been carting around with me for years, and had assumed was long-since unsalvageable. Today, I wanted to see if I could get it to recognise a floppy drive and, as a bonus, boot something from it.

Barring outstanding examples like the Commodore PC line (yes, they made them too!), most motherboards well into the early 1990s didn't include onboard peripheral connections or drive controllers. It was up to the OEM, or the customer, to provide the requisite expansion cards.

This board was no different, but I knew I had a few options. For example, every ISA sound card I have sports an IDE controller for a CD-ROM, even though I don't think I ever used them.

I ruffled through my legacy parts box, and found this amazing (and dusty!) **ISA Acer MIO-400 KF Multi-IO card**. Rather than needing separate cards for printer ports, serial, game ports, floppy drives, and IDE controllers, this integrated them all into one card, with jumpers on the board to tune their settings and enable/disable specific functions:

<figure><p><a href="https://rubenerd.com/files/2023/asus-mio-400-full.jpg"><img src="https://rubenerd.com/files/2023/asus-mio-400@1x.jpg" alt="ISA Acer MIO-400 KF Multi-IO card" style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2023/asus-mio-400@2x.jpg 2x" /></a></p></figure>

The AMI BIOS on the board supported defining two floppies, including high density 5.25 and 3.5-inch drives which was a relief. I connected the IO card to the board, a spare 3.5-inch drive with a floppy ribbon cable to the card (after the cable twist), and powered up the machine.

The first signs were not good. The BIOS reported an **FDD controller failure** error. I swapped to another floppy drive cable, re-seated the ISA card, and changed the mode set for the drive in the BIOS. No change.

*DeOxit that socket!* I sprayed some into several adjacent ISA slots, cleaned the contacts on the ISA card, and reinserted it one slot down from where it was before. As if this is about to become a recurring theme here, the machine no longer reported a controller error.

The next challenge was to see why the floppy drive had no signs of life. It didn't seek on start with that familiar buzzing sound, and the board reported there was no bootable drive present. I confirmed I had the ribbon cable connected in the right orientation, with the red wire connected to pin 1 on the drive and the IO card.

At this point, I felt like I was trying my luck. The fact this 386SX board was fixed with most of its original memory, and this amazing old Oak VG-7000 VGA/EGA/CGA/Hercules ISA graphics card *also* worked, was unusual enough. Was it too much for me to expect this thirty-year old card would too?

<figure><p><img src="https://rubenerd.com/files/2023/acer-mio-400-teac@1x.jpg" alt="Photo showing the TEAC drive connected to the MIO-400, and a DISKETTE BOOT FAILURE message." style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2023/acer-mio-400-teac@2x.jpg 2x" /></p></figure>

I was sure the floppy drive worked, but I decided to swap it out for a TEAC 5.25-inch drive I use to test various encodings and densities. This time, the drive seek'd immediately on boot, and it attempted to read a boot disk with all those glorious whirring and clunking sounds. *Huzzah!* Being given a **DISKETTE BOOT FAILURE** here was expected, because it wasn't a boot disk.

While the motherboard BIOS supported high-density drives, I assumed maybe the IO card itself didn't. But just in case, I harvested another high-density 3.5-inch drive from my Pentium 1 desktop, and *this* time it worked. It booted off my MS-DOS 6.0 rescue disk without problems... and surprisingly quickly. `COMTEST` also confirmed it saw the two COM ports on the IO card too.

Forgive the *Dutch Angle*, but I suppose that happens when I get excited.

<figure><p><img src="https://rubenerd.com/files/2023/acer-mio-400-serial@1x.jpg" alt="Photo showing COMTEST on the screen, showing the two available COM ports on the IO card." style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2023/acer-mio-400-serial@2x.jpg 2x" /></p></figure>

I still have no idea what case I'll put this thing in, what OSs it will eventually run, and what permanent storage it'll end up with! But at least I have a way to boot and import data now :).
