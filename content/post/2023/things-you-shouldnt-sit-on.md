---
title: "Things you shouldn’t sit on"
date: "2023-10-06T12:25:24+11:00"
abstract: "Another Pieces of Eight post."
year: "2023"
category: Thoughts
tag:
- lists
- pieces-of-eight
- pointless
location: Sydney
---
We haven't had a *[Pieces of Eight](https://rubenerd.com/tag/pieces-of-eight/ "View all posts tagged in the Pieces of Eight series")* post in a while.

A non-exhaustive list, because I still have plenty of energy having written it:

* Bagels, with or without cream cheese
* Ancient Roman spears, particularly those oriented vertically
* Benches sporting *wet paint* signs
* Rack-mount servers with dodgy rails
* Something you've been cautioned is "rank"
* This list, printed on food-safe dye atop a cake
* Tempered-glass porcupine or hedgehog statues
* Durians

And a bonus item for something you *should* sit on:

* Something safe to sit on
