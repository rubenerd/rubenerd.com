---
title: "I’m rubenerd.bsky.social"
date: "2023-07-07T15:12:39+10:00"
abstract: "I’d rather you ping me on Mastodon, but this is my profile on Bluesky."
year: "2023"
category: Internet
tag:
- bluesky
- mastodon
- social-media
- twitter
location: Sydney
---
I'd rather you ping me on Mastodon:

[bsd.network/@rubenerd](https://bsd.network/@rubenerd)

But here's my extremely new Bluesky profile if you're there:

[rubenerd.bsky.social](https://bsky.app/profile/rubenerd.bsky.social)

And I'm still on Twitter, for the Hololives:

[twitter.com/rubenerd](https://twitter.com/rubenerd/)

I wonder how all of this will shake out? Will it be another Jaiku, Pownce, Soup.io, App.net, Tent.is situation? Or is this time different?

