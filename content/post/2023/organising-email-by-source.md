---
title: "Organising email by source into six folders"
date: "2023-12-24T10:09:26+11:00"
year: "2023"
category: Internet
tag:
- email
- history
- organising
location: Sydney
---
2023 was the year I changed up how I organise email, so I thought it might be fun to do a bit of a nerdy discussion on how it went.

For some context, I've been carrying the same Netscape, then Mozilla, then Thunderbird profile since I was in primary school in the late 1990s. It went from a Pacific Internet Singapore address, to a Hotmail, to a Gmail, then my current domain. I'm sure many of you have email far older than this, but it's still oddly fun going back to what I sent as a kid.

I've always been a bit obsessive when it comes to organisation; I'm sure surprising none of you. I settled on sorting email based on a set of fairly arbitrary categories that morphed and evolved into about 180 folders and sub-folders. A dozen or so of these were auto-sorted, but to this day I still tend to hand sort and archive as it comes in. Like manual account reconciliation, it's my way of making sure I don't miss things (though getting the mental space to *reply* to what I'm supposed to really became a crapshoot this year, to which many of you can attest).

This year, I made a clean break and had a "2023" root archive folder, and sorted email based on source, rather than categories. Namely:

* Announce
* People
* Mailings
* Newsgroups
* Receipts
* Work

... and that was it!

The People folder turned out to be the best decision here. The vast, vast majority of email I get now is mass-delivered or auto-generated. When I get an email from *someone*, as opposed to *something*, it's special.

Towards the end of the year, I started cheating and using Thunderbird *Saved Searches* as "subfolders" under some of these. This way, I could quickly differentiate between various mailing lists, such as ausnog, freebsd-doc, and netbsd-users. A part of me thinks I should have made an exception for lists, but this has worked surprisingly well.

The other change is that I'm spawning a new folder for each year going forward. I've done this for my documents folders since I ferried stuff to school on Zip Disks, and it's worked really well.

I'd better get started in finishing off my sorting for this year so I can start a 2024 folder.
