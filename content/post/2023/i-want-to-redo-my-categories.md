---
title: "I want to redo or stop using categories"
date: "2023-03-23T09:36:38+11:00"
abstract: "Now how do I categorise this!?"
year: "2023"
category: Internet
tag:
- categories
- metadata
- tags
- weblog
location: Sydney
---
There's an Evangelion joke in there somewhere, but I digress. Back in 2008, I had [too many categories](https://rubenerd.com/p2564/). I condensed them into fewer than a dozen, which I've mostly retained since. I know them off by heart:

* anime
* annexe
* hardware
* internet
* media
* show
* software
* thoughts
* travel

I don't like them now. Before they were too numerous and prescriptive, and now they're too vague. **Hardware**... you mean, like a hammer? Ones like **annexe** are only ever used for housekeeping; **show** is only used for podcasts; and **thoughts** is a catch-all that doesn't mean much.

If I think about the kinds of things I blog about now, it'd probably be closer to this:

* anime
* bsd
* cafes
* ethics
* macos
* music
* retrocomputing
* reviews
* travel

But that raises the question, does that phrase have five words? And if I change my category scheme here, does it need to be retroactive? Do I try and convert tags?

I suppose the bigger question is whether people even use the categories in the first place to find stuff. I don't think I have for a long time. Maybe I was on the right track in 2006 with [using categories as tags](https://rubenerd.com/using-wordpress-categories-as-tags/). One fewer taxonomy would make some things easier.
