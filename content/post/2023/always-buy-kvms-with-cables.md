---
title: "Always buy old KVMs with cables"
date: "2023-11-25T12:13:24+11:00"
abstract: "Whatever you save on the unit, you lose on the cables."
year: "2023"
category: Hardware
tag:
- retrocomputing
- shopping
location: Sydney
---
I bought a KVM recently for my retrocomputer setup, on account of having lots of machines and [absolutely no space](https://mastodon.nl/@jan/111431123921339740 "A Mastodon thread about how I have space for these machines.") for them. The unit didn't come with cables, but the price seemed shockingly reasonable, so I thought I could get them separately.

**This was a mistake!**

There are two problems with buying old KVMs without cables:

* Some units have proprietary breakout connectors to limit cable clutter. These cables are impossible to find, and are ridiculously expensive.

* Other units, like the one I bought, have standard VGA and PS/2 connectors. I thought this would make finding cables easier... except an 8-port KVM needs 16 PS/2 cables, and 8 VGA cables! The latter are easy enough to find, but PS/2 cables are also rare and expensive.

Now you might be able to source a spool of cable, some PS/2 male connectors, and spend a weekend crimping them thirty-two times. But I value my time more than this.

Lesson learned: there's a reason old KVMs are so cheap! Make sure you source and factor in the cost of cables into whatever unit you buy. The more expensive one *with* cables is probably the better deal, or at least much less hassle.
