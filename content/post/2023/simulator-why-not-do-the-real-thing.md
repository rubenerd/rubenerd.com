---
title: "Simulator game? Why not do the real thing!?"
date: "2023-05-29T13:04:23+10:00"
abstract: "Being obtuse about why people enjoy these games is a weird hangup."
year: "2023"
category: Software
tag:
- games
- hololive
- humongous-entertainment
- ouro-kronii
- putt-putt
location: Sydney
---
One of my favourite games as a kid was a minigame in *Putt Putt Joins the Parade*. To make enough money for a new paint job and a car wash, you would mow people's lawns, with ever-escalating obstacles to route around. It was immensely satisfying, relaxing, and a great way to blow off steam.

> Errrrr, ummmm, ackchyually, Ruben, why not mow a real lawn?

I love other games for similar reasons. *Train Simulator*, the enduring *Flight Simulator* franchise, even games like *Cities: Skylines*. They're fun because...

> Ruben, are you there? I said don't see the point! Why not mow a real lawn, fly a real plane, drive a real train, build a real city, launch a real rocket to Mars?

...they're analogues of the real world, but in a chill environment at home without physical limitations and no pressure if you make mistakes.
This is why...

> RUBEN!? Simulation games make no sense! Why not do the real thing!?

...it warms my heart reading news that *Powerwash Simulator* is doing so well. If you pitched this game to plenty of people, they'd ask what's the point. It's an excellent game, to play and to watch.

[PowerWash Simulator on Steam](https://store.steampowered.com/app/1290000/PowerWash_Simulator/)   
[Ouro Kronii streaming the game](https://www.youtube.com/watch?v=kay32mA4eUc&list=RDCMUCmbs8T6MWqUHP1tIQvSgKrg)   
[Noisy Pixel: PowerWash Simulator Achieves Over 7 Million Players](https://noisypixel.net/powerwash-simulator-7-million-players-worldwide/)

Remember the golden rule of the Internet: you don't need to justify yourself, *especially* to those asking why you're having fun.
