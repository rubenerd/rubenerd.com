---
title: "Camera specs don’t tell the whole story"
date: "2023-07-27T08:12:30+10:00"
abstract: "A swing and a miss with a Ricoh GR III made me miss my Olympus."
thumb: "https://rubenerd.com/files/2023/kyoto-at-night@1x.jpg"
year: "2023"
category: Travel
tag:
- cameras
- canon
- kyoto
- japan
- new-york
- nikon
- olympus
- photos
- united-states
location: Sydney
---
Cameras are weird devices. They're first and foremost a tool for artistic creativity, yet nerds like me are also drawn to their endless potential for technobabble and obsession about specifications. But as I've now learned, spending your time on the latter may impact the former in ways you don't expect.


### I heard you like skipping background!

My first camera was an Olympus Trip 35 film camera I inherited from my mum. I was so excited to come home from school to see the photos she took to be processed. I loved the hobby so much that my parents later bought me a FujiFilm S9600 bridge camera, which had all the controls of an SLR, but at a much lower price and with a permanent f/2.8 telephoto. Yes, f/2.8! It was a fantastic introduction to wider photography.

My first SLR was a Nikon D60, which I carried in a separate bag for almost a decade. I had a couple of budget kit lenses, but I spent most of my time using a 35mm f/1.8 prime. My only regret not going Canon, or spending the extra for a D90, was being limited to AF-S lenses for autofocusing. But again, it felt like a massive upgrade, and I learned a lot.

<figure><p><img src="https://rubenerd.com/files/2023/kyoto-at-night@1x.jpg" alt="Photo taken with my Olympus on a warm Kyoto night of lanterns" srcset="https://rubenerd.com/files/2023/kyoto-at-night@2x.jpg 2x" /></p></figure>

Midway through the 2010s I got a promotion, and Clara and I went on our first international holiday together. I spent months researching what I should get next, and decided on a FujiFilm X100S. I walked into the store, tried a few different cameras for fun, and instantly fell in love with the OM-D E-M1 Mark II mirrorless Micro Four Thirds camera. Surprising even myself, I bought it and a M.ZUIKO 17mm f/2.8 pancake instead, and walked out an Olympus guy. That's foreshadowing for later.

Not to put too fine a point on it, but it *completely* transformed how I used cameras. Its mechanical dials and digital viewfinder were a pleasure to use, and its tiny size meant it was always with me and available for those unplanned moments, like wandering around a narrow street after leaving a cute Kyōto isakaya. It's probably the single most enjoyable piece of consumer tech I've ever owned.

Last year the camera was involved in an accident, so I did some more obsessive research into this category of pocket cameras, and bought a Ricoh GR III. It packs an APS-C sensor in something even smaller than the Olympus, and its prime lens is fast, tight, and sharp. There's a reason this camera is a cult classic, especially among street photographers.


### Taking fewer photos

But then I noticed something weird. Despite always having the best possible compact camera on my person, or in my bag, I started taking fewer photos. My 2022 OpenZFS photo pool is half the size of 2012, despite the photos themselves being significantly larger. I didn't lose interest in the hobby, so what was going on?

Part of this was due to Covid lockdowns, and what I'm calling my 2020-23 blues period in which I'm still struggling. But even when we could travel and explore again, I found myself reaching for my iPhone SE. Why?

The reason has nothing to do with technical features or capabilities, and everything to do with that squishy part of our brains responsible for gratification. The budget Nikon SLR was fun. The Olympus was *extremely* fun. The Ricoh GR III is the best camera I've ever used, but I don't enjoy using it anywhere near as much.

<figure><p><img src="https://rubenerd.com/files/2023/new-york-haze@1x.jpg" alt="Photo taken with the Olympus looking out over Manhattan on a hazy morning." srcset="https://rubenerd.com/files/2023/new-york-haze@2x.jpg 2x" /></p></figure>


### Spec sheets don't tell the whole story

I could explain all the reasons why the GR III's physical and touch-screen interfaces don't gel with how my mind works, but if they resolved all of them, it wouldn't be a GR III. I'm thankful the GR line exists for the people who love them, and is so well suited to their shooting style.

This is an incredibly important point to stress, whether you're talking about cameras, phones, movies, restaurants, or anything. You can spend your whole life pouring over specification sheets trying to pick the perfect device for your budget, but you can't account for interest or joy. Sometimes things just don't click, or not in the way you expect or are used to. Emotions aren't logical; *that's what makes them emotions!*

I'm probably going to get it serviced and cleaned, then take the opportunity to sell it and upgrade to something else.

I made the mistake of trying a Nikon Z fc and a Canon mirrorless EOS R7 in a store recently, and they were *right* up my alley. They was light like my Olympus, not as big as I thought they'd be, and the menus and dials were *so* much nicer than any Sony Alphas I've tried. The R7's grip took me back to my beloved Nikon D60, and the Nikon Z fc also felt even closer to the Olympus with all its beautiful manual dials.

By the time I've saved up enough money for a new camera, I'm sure there will be an entire new crop of them available, though I'm sure that won't make the decision any easier!
