---
title: "Goodbye to Flash Coffee"
date: "2023-11-11T09:58:50+11:00"
abstract: "The Singapore coffee chain shut down last month."
thumb: "https://rubenerd.com/files/2023/flashcoffee@1x.jpg"
year: "2023"
category: Thoughts
tag:
- coffee
- food
- goodbye
- singapore
location: Sydney
---
Those bright yellow coffee stalls were seemingly everywhere in Singapore overnight from 2021, but they [shut last month](https://www.channelnewsasia.com/singapore/flash-coffee-outlets-closed-employees-strike-3843451) with unpaid wages and debts.

I never got to try it; friends spoke highly of it compared to other chain coffee on the island. I'm more sad for the people who's livelihoods have been left in a lurch though. It sucks.

<figure><p><a href="https://rubenerd.com/files/2023/flashcoffee@orig.png"><img src="https://rubenerd.com/files/2023/flashcoffee@1x.jpg" alt="Flash Coffee's website" style="width:500px; height:310px;" srcset="https://rubenerd.com/files/2023/flashcoffee@2x.jpg 2x" /></a></p></figure>

Their [website still resolves](https://flash-coffee.com/sg/), like a snapshot in time. The [Wayback Machine](https://web.archive.org/web/20230000000000*/https://flash-coffee.com/sg/) also has it for when it disappears like their drinks.
