---
title: "Electric cars aren’t a silver bullet"
date: "2023-04-26T09:04:00+10:00"
abstract: "Electric cars are still cars."
year: "2023"
category: Thoughts
tag:
- cities
- health
- transport
- urban-planning
location: Sydney
---
[Willem Klumpenhouwer's article in the Globe and Mail](https://www.theglobeandmail.com/business/commentary/article-electric-cars-emissions-climate-change/)

> But there’s just one nagging issue that no amount of technical innovation can solve: Electric cars are still cars. Outside of tailpipe emissions, they come with all the problems of cars, whether those problems relate to the environment, social equity or public health. The focus on electric cars stands in the way of truly transformative change: better public transit and better laid-out cities that encourage active modes of getting around, such as cycling.

