---
title: "Meeting Pavolia Reine at SMASH!"
date: "2023-07-02T19:23:56+10:00"
abstract: "A highlight of the year! She was gracious, funny, easy to talk with, and put this clearly nervous gentleman at ease!"
thumb: "https://rubenerd.com/files/2023/ruben-with-reine@1x.jpg"
year: "2023"
category: Anime
tag:
- hololive
- hololive-indonesia
- pavolia-reine
location: Sydney
---
Remember when I posted this in early June about my oshi from Hololive Indonesia?

[Winning a meet-and-greet with Pavolia Reine](https://rubenerd.com/winning-a-meet-and-greet-with-reine/)

It happened! And no shot, it was one of the highlights of the year! Clara made me a cute *Tatang* mascot and put some of her trademark peacock feathers in my hat, and I did my best to colour coordinate an outfit with her debut outfit.

<figure><p><img src="https://rubenerd.com/files/2023/ruben-with-reine@1x.jpg" alt="An awkward but well-meaning gentleman standing besides Reine on the screen at SMASH in Sydney." style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2023/ruben-with-reine@2x.jpg 2x" /></p></figure>

This was my first meet-and-greet with a vtuber, and to have it with my favourite one?! *Shiok!* You enter a room with a large TV screen and a camera, as though you're about to watch a stream. Only instead of talking with thousands of people spread across the world, she's commenting directly about your hat and asking what your name is! Words can't describe.

She was gracious, funny, and just as easy to talk with as she is with her chat on live streams. The highlight for me was talking about travelling around Indonesia; I don't think many in the West appreciate just how *massive* and spread out the country is. One day I'll see more!

Clara couldn't be there in the room too, but I let her know what a joy her streams had been for both of us, especially during Covid and lockdowns where we need something fun and familiar. I grew up with so many Indonesian friends in Singapore, it's also felt like I have them around again when I hear her voice.

Time was short, so I didn't get time to talk about our favourite Minecraft fishing streams of hers, her recent song releases, her Persona playthroughs, her keeb adventures, or even how I as an Australian/Singaporean approved of her *Nasi Tim Tam* concoction! But then I suspect we would have been there forever, and she had dozens of people to talk with in a short amount of time.

[Pavolia Reine's channel](https://www.youtube.com/@PavoliaReine)   
[Post about Hololive: Our Bright Parade!](https://rubenerd.com/hololive-our-bright-parade/)

Pavolia Reine is a member of Hololive Indonesia Gen 2 who debuted in November 2020, and has become the main streamer from all of Hololive I watch. She's come such a long way since, and I couldn't be prouder. Terima kasih Reine! I know you will continue going far. 🦚

