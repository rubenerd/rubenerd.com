---
title: "Ruben’s Laws of Tabbed Interfaces"
date: "2023-11-16T09:30:50+11:00"
abstract: "You notice what you needed on a tab as soon as you close it."
year: "2023"
category: Software
tag:
- browsers
- laws
- tabs
location: Sydney
---
1. You don't need a tab when it's open.

2. You notice why you needed a tab as soon as you close it.

3. Your chance of a tab reopening in the same state as before is inversely proportional to how important it was.
