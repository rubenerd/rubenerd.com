---
title: "Nico Cartron’s BSD journey, and a coffee"
date: "2023-10-06T11:03:13+11:00"
abstract: "A post from a fellow BSD user who also started on NetBSD!"
year: "2023"
category: Software
tag:
- bsd
- feedback
- freebsd
- linux
- netbsd
- nico-cartron
location: Sydney
---
I'm writing this post at a coffee shop&mdash;no wait, really!?&mdash;having one of the many brews Nico has graciously bought me on [Ko-fi](https://www.ko-fi.com/A625HL "Buy me a coffee on Ko-Fi"). This is a graphical representation! ☕️

In response to [my BSD history post](https://rubenerd.com/which-bsd-did-you-start-on/ "Rubenerd: Which BSD did you start on?") , Nico has [written his own](https://www.ncartron.org/which-bsd-did-you-start-on.html) again. He included a cheeky reference to Linux as well, given distros also depend on BSD-licenced code to operate. He also started on NetBSD like I did!

Nico is *good civ*. You should definitely [read his blog](https://www.ncartron.org/ "Nico's Blog"), subscribe [to his feed](https://www.ncartron.org/feed.rss "Nico's Blog RSS feed"), and shout him a [coffee](https://ko-fi.com/nicoblu42 "Nico's Ko-Fi page") too. *Merci!*
