---
title: "Max Miller makes butter"
date: "2023-09-29T09:08:23+10:00"
abstract: "til all the buttermilk... fell out."
thumb: "https://rubenerd.com/files/2023/max-miller-butter-2@1x.jpg"
year: "2023"
category: Media
tag:
- food
- quotes
- history
- vidoes
- youtube
location: Sydney
---
[Tasting History: How to Make Old Fashioned Butter](https://www.youtube.com/watch?v=Fqkx4itmNEQ)

<figure><p><img src="https://rubenerd.com/files/2023/max-miller-butter-1@1x.jpg" alt="Whipped cream that got whipped too much" style="width:500px; height:281px;" srcset="https://rubenerd.com/files/2023/max-miller-butter-1@2x.jpg 2x" /><br /><img src="https://rubenerd.com/files/2023/max-miller-butter-2@1x.jpg" alt="til all the buttermilk... fell out." style="width:500px; height:281px;" srcset="https://rubenerd.com/files/2023/max-miller-butter-2@2x.jpg 2x" /></p></figure>
