---
title: "“Hate it? Sell it to me”"
date: "2023-09-19T10:15:44+10:00"
abstract: "A tactic my brother-in-law taught me about viewing the perspective of other people."
year: "2023"
category: Thoughts
tag:
- family
- psychology
- work
location: Sydney
---
I had a great conversation with my brother-in-law over the weekend about old job experiences. He recalled a tactic he used as a manager to interview people looking to work in sales in his store:

> **Him:** What's a game you really hate?
> 
> **Prospect:** Oh, that FIFA thing.
>
> **Him:** Okay then, sell it to me!

He said it was a great icebreaker, and immediately demonstrated the qualities he was after in a sales person. Thinking on one's feet was important, but also the ability to empathise with a potential customer. You might not like sports management games, but someone entering the store wearing a Liverpool FC shirt might.

I'm generally not a fan of *gotcha*-type questions, but I thought this was one example that was pretty clever. He didn't dismiss people based on this one answer, but it was another variable in their favour.

I remember doing a similar exercise years ago for English class in school. We had to deliver a speech on a topic with which we disagreed, but have it sound convincing. I'd moved to \*nix by this point, so I chose to extol the virtues of Windows. She told me I should work in Microsoft PR, and I haven't forgiven her since.

It's probably a useful exercise to do from time to time to see someone else's perspective. Understanding is the first step to affecting change.
