---
title: "Merlin Mann and my Commodore 16 taught me a small step is a catalyst"
date: "2023-07-09T09:47:18+10:00"
abstract: "Every time you realise a dependency, you get closer to what you need to do."
thumb: "https://rubenerd.com/files/2023/c16-keycap-broken-step-1@1x.jpg"
year: "2023"
category: Hardware
tag:
- merlin-mann
- productivity
- retrocomputing
location: Sydney
---
This is a variation on a theme I've brought up here many times, but the fact I'm still surprised when it happens shows I clearly haven't internalised it!

Here's the truth bomb: you have to take a step to start something. Or as I literally fucking quoted Merlin Mann a decade ago saying:

> Every time you realise a dependency, you get closer to what you need to do.

[October 2013: Dependencies and @hotdogsladies](https://rubenerd.com/dependencies-and-merlin-mann/)


### Hyping oneself for failure

Among the big traps I fall into in my thirties is that I don't want to start something... or more truthfully, I don't feel like I can. It's usually for one of these reasons:

* The end is indeterminate

* I don't have every step planned

* I don't have everything I need to finish it

* Something something self doubt anxiety

These mean I don't start, and I feel guilty. I iterate on these feelings a few billion times and: congratulations! I've built that necessary or fun thing up into an insurmountable challenge I don't want to touch.

This happens *constantly*, but I'm trying to be better at recognising it.


### Okay, what's an example?

Where do I even start? That loose cabinet door in my bathroom? That unfinished Perl RSS reader? The expired vegetables in my crisper?

It's a silly example, but a couple of months ago I broke off a key from my second-hand 1984 Commodore 16. I knew that replacing it would entail removing the broken stem from the kepcap, desoldering the board to get access to the other broken part, sourcing a replacement stem, adding the stem, testing it, potentially sourcing replacement carbon pads for the PCB if they're also damaged, soldering it back together, adding the cap back, and testing again.

<figure><p><img src="https://rubenerd.com/files/2023/c16-keycap-broken@1x.jpg" alt="A photo of my Commodore 16 with a broken kepcap, and part of the stem inside the cap. To the side is the keycap spring." style="width:500px;" srcset="https://rubenerd.com/files/2023/c16-keycap-broken@2x.jpg 2x" /></p></figure>

There wasn't anything especially challenging there, but I worked myself up into thinking it'd be too difficult, and that the hardware was too precious. The outcome being I have a broken Commodore 16. If I want to respect this hardware and treat it properly, this is hardly a glowing example of doing that.

Yesterday something twigged, and I decided to try the first step. I took the tiniest drill bit I could find and applied the lightest, slowest amount of pressure I could. It took a long time, but eventually I felt something give, and I pulled the drill out along with the broken stem. Done!

[@rubenerd@bsd.network: First step in my #Commodore16 keyboard repair done!](https://bsd.network/@rubenerd/110677095857025007)

<figure><p><img src="https://rubenerd.com/files/2023/c16-keycap-broken-step-1@1x.jpg" alt="The broken stem has been removed!" style="width:500px;" srcset="https://rubenerd.com/files/2023/c16-keycap-broken-step-1@2x.jpg 2x" /></p></figure>


### Your point?

This is only the first in *many* required steps to get this machine working again, but taking this first step unlocked two things:

* It made me feel *awesome!* There's hope for this machine yet, and I feel a sense of validation.

* I overcame that first step, and now I have a clearer view and more motivation to continue ticking off those dependencies.

I have a replacement keycap stem coming from Germany, which thanks to being on the Australian continent floating in the middle of nowhere, will probably arrive in November. But at least in the meantime I've taken a step.

Now if I could get it into my head that I can apply this to so many other things in my life, I might have learned something from this experience.

Maybe I need a David Allen context in my GTD system called: *spark this!*
