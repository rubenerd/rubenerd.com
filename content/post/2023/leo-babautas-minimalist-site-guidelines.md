---
title: "Leo Babauta’s minimalist site guidelines"
date: "2023-12-08T16:18:29+11:00"
abstract: "Respect your readers and visitors!"
year: "2023"
category: Internet
tag:
- design
- minimalism
location: Sydney
---
[Leo Babauta has a great post](https://mnmlist.com/w/ "mnmlist: minimal web") talking about minimalist websites. He defines such a site as: 

> A website with the main purpose of having people read content would best serve its readers with almost nothing else but what’s needed for the reading experience.
>
> Strip a site of all its distraction, cruft, gimmicks, promotions, advertising, social sharing and more … and all you have is the pure reading experience.
> 
> A minimalist website. Perfect for the readers, which is perfect for the writer. Not so perfect for advertisers and marketers, perhaps, but we’re not creating sites for them. We’re creating sites for us.

I love the pitch. Most of the suggestions on his page are great, from eschewing (gesundheit) ads, cookies, tracking, popus, social media sharing buttons, and so on. Yes, yes, and *yes!*

I only have a few tiny reservations:

* He says *content* in the first sentence. This is ad speak. We're [not content creators](https://rubenerd.com/stop-calling-people-content-creators/).

* He advocates for "minimal images" with "small page weight". I can see he's broadly comparing this to heavy pages with multiple megabytes of junk, but I think a page can still be minimal with relevant illustrations, photos, and a fun banner image.

* He discourages tags. I haven't exposed mine for years, but I do leave category links in headers. A bit of metadata does improve the accessibility of your site, and makes it easier for new readers to grok the kind of topics in which you have an interest.

I'd also add a few of my own:

* Use clean HTML. Remember when people used to take pride in their source code? You don't need hundreds of class IDs and divs so heavily-nested they could host birds. Lean on the rich vocabulary HTML gives us out of the box, and leave CSS to style them.

* Make your site as simple as possible, but not simpler. A minimal site that's obtuse or difficult to navigate is as useless as one full of JavaScript and ads.

* You don't need to shed personality, uniqueness, or fun in the pursuit of making something minimal. It's more to do with respecting your readers and visitors while you do it. Which I think is Leo's point.
