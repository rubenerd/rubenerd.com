---
title: "Firefox can now match on diacritics"
date: "2023-05-01T09:28:25+10:00"
abstract: "You can search for them on pages now."
year: "2023"
category: Software
tag:
- internationalisation
location: Sydney
---
I'm not sure when the Firefox devs added this feature, but it's already made my life so much better!

If you hit **CTRL+F** or **CMD+F** to find text on a page, you now have these options:

* Highlight All
* Match Case
* **Match Diacritics**

This means you can do a search for Ōtsu, not Otsu. *Awesome!*

As an aside, it sucks that English only makes use of diacritics and accents on loanwords. They have the potential to remove so much ambiguity.

