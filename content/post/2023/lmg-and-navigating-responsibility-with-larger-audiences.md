---
title: "LMG, and navigating responsibility with larger audiences"
date: "2023-08-15T17:12:25+10:00"
abstract: "Tackling the issue of independent video creators shirking their responsibility to their audiences."
year: "2023"
category: Ethics
tag:
- ethics
- gamersnexus
- video
- youtube
location: Sydney
---
Welcome to the inaugural post in my Ethics category! I'm not sure if I'll retroactively re-tag older posts, but this is topic I care deeply about, and one in which I want to contribute more thoughts. Thanks again for taking the time to read my posts.


### Trends in independent media

I've noticed a worrying movement among independent authors, podcasters, and videographers. There comes a point where their audience size eclipses even those of traditional media, which fills me with more optimism and hope than I expected. Yet despite this, they continue to operate as though they're a scrappy upstart.

It's easy to empathise. Most things are fun and easier when you're small. You have full creative and editorial control; part of the reason why I love blogging to a few hundred of you every day! You also want to keep hold of the thing that made you popular in the first place, and are likely reticent to recreate the stuffy and inflexible corporate structures you may have escaped from.

But there's an inescapable corollary: your responsibilities grow inline with your audience and reach. I'm convinced more videographers play the role of a small creator to absolve themselves of behavior that would otherwise be out of step or inappropriate. They want the reach, success, and money that comes from being a large outfit, but only want to be held as accountable as they were before.

This becomes a huge issue when the outfit leans technical. Audiences place a level of trust in these independent videographers in a way they wouldn't have with a traditional news or media outlet, in part due to their perceived approachability and authenticity. This means they wield tremendous power, both to influence the purchasing decisions of millions of people, and to sway public opinion and debate. They may even hold the futures of companies and other independent video creators in their hands.

They might argue these are unintended consequences of their success, or that such responsibility is overplayed. Take a certain large podcaster who was happy to spread lies about vaccines during a global pandemic, for example. Regardless of the creative person's opinion, it doesn't change reality of those caught in their wake, willing or otherwise.


### Enter Linus Media Group

LMG, creators of the wildly successful *Linus Tech Tips* channel among others, are the latest to learn this lesson the hard way. Linus's success has earned him millions, a staff of hundreds, and lucrative sponsorship deals and tie-ins. But when he tried to sell a backpack without a warranty because *you can trust him, he's small*, the ethical and legal backlash lead to him to (fortunately) reverse his decision.

More recently, LMG has invested into building a testing lab. This pivot from an entertainment network to one offering quantitative reviews has been rocky, with regular inaccuracies and mistakes in their testing approaches, data, and conclusions. Critics who'd forgive a small channel have pointed again to LMG's reach, influence, and budget as reasons for why they don't pass muster.

This came to a head this week, with Steve from GamersNexus presenting evidence of lapses in LMG's conduct. Rather than owning the mistakes, Linus deflected responsibility, and appealed to his team made of "real people". Aka, they're small. Like Steve, I don't buy it.

[The Problem with Linus Tech Tips: Accuracy, Ethics, & Responsibility](https://www.youtube.com/watch?v=FGW3TPytTjc)   
[HW News - Linus Tech Tips' Terrible Response](https://www.youtube.com/watch?v=X3byz3txpso)

I've had whiffs that something's been up with LMG for a while now, which is why I stopped watching their videos. Their approach to tacking reviews and issues has felt increasingly shallow, as though they were pumping out more videos across multiple channels for engagement over sincerity. One standout was Emily Young, who I would pay good Patreon money to if she decided to go independent.

It does suck when something you're a fan of falls apart, though I will admit I wasn't hugely surprised.


### Parting thoughts

Everyone from independent writers to video creators need to realise the calculus changes once you reach a scale like this... and not just because more people see when you trip. You either make this transition with grace and maturity, or you crash and burn. These channels have legions of fans, and it'd be disappointing and a tremendous waste to squander their good will. In the words of moral philosopher Billy Joel, it's a matter of trust.

