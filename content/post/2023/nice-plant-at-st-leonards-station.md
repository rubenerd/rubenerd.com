---
title: "Nice plant at St Leonards station in Sydney"
date: "2023-05-21T22:28:50+10:00"
abstract: "A cute plant kept me company for a bit."
thumb: "https://rubenerd.com/files/2023/st-leonards-station-plant@1x.jpg"
year: "2023"
category: Travel
tag:
- photos
- plants
- trains
location: Sydney
---
<figure><p><img src="https://rubenerd.com/files/2023/st-leonards-station-plant@1x.jpg" alt="Photo showing a small tree in a square planter above the railway tracks at St Leonards station in Sydney" style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2023/st-leonards-station-plant@2x.jpg 2x" /></p></figure>
