---
title: "Filter jugs should be rectangular"
date: "2023-03-07T16:39:49+11:00"
abstract: "Oblong-shaped vessels waste a huge amount of space in fridges."
year: "2023"
category: Hardware
tag:
- beverages
- design
location: Sydney
---
I did an [episode of my silly show back in 2015](https://rubenerd.com/show276/) that dealt with the nonsensical application of slanted bottlecaps on shampoo, thus preventing the orientation of the bottle upside down and letting gravity assist in removing the last of its contents. Like an ocean liner of yore, it was riveting. Riveted. River. Water. *Foreshadowing.*

I've got around this problem now by physically having less hair to shampoo in the first place, though I admit that wasn't so much voluntary as it was a genetic inevitability. But I digress.

I feel like we have a similar situation with water filter jugs. If you look at large containers sold for bulk water storage in fridges, most are a rectangular shape with rounded edges and corners for strength and ergonomics. Not so for filter jugs, which have all taken a sweeping, oblong shape to look modern, or something.

The upside to this is the bottles take up more space in the fridge for less total volume. On a shelf, there are entire sections of wasted space around the  oblong shape that can't be used to store anything more than a Tabasco bottle, which one wouldn't be refrigerating in the first place. I've put bananas and cheese sticks there in a pinch, but they all fall back onto the shelf upon removal of the jug, which requires the whole assembly to be reorganised when one wants a glass of water. 

Water filter jugs should be rectangular prisms with rounded edges to assist with cleaning, and to prevent silly people like me poking my eyes out on a corner when using it at 3am to pour myself a glass. We'd get more volume of water for a given amount of shelf space, and may even reduce plastic waste.

I know every water filtration mover and shaker must read this blog (*activated carbons*, they call themselves), so I implore you to have your design teams consider this. We would all thank you.
