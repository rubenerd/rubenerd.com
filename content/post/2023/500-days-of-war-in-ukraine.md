---
title: "500 days of war in Ukraine"
date: "2023-07-13T14:13:42+10:00"
abstract: "The war didn’t start 500 days ago. Please report responsibly."
year: "2023"
category: Thoughts
tag:
- news
- ukraine
location: Sydney
---
It's infuriates me that this war has passed this milestone, but it's also a testament to the Ukranian spirit and will under unrelenting aggression. Once again, I remind war hawks that this would be over *today* if Russian troops withdrew.

I bring this up not just to mark this sombre occasion, but to remind journalists and commentators that **this didn't start 500 days ago**. Russia annexed Crimea in 2014, and has been waging war in the Donbas region ever since.

There are Ukrainian children in school now who were born when this war started almost a decade ago. Please keep that in mind when writing your news reports and social media reactions. 🌻

[Donate to UNITED24: The initiative of the President of Ukraine](https://u24.gov.ua/) 
