---
title: "Daniel Jalkut projecting good"
date: "2023-07-16T20:31:35+10:00"
abstract: "Optimism has its own power."
year: "2023"
category: Thoughts
tag:
- good
- quotes
location: Sydney
---
[From his Microblog yesterday](https://danielpunkass.micro.blog/2023/07/13/many-people-have.html)

> Many people have responded to my assertion that Apple should do something good with the argument that they simply never would. My style of activism includes proclaiming unlikely outcomes. Like Phil Ochs, who sang in the midst of the Vietnam war: “I Declare the War is Over”. Optimism has its own power.

I feel that in my bones. I'll talk about the potential for something, and I'll get a wall of responses explaining why something can't be... right now. Just because something is a certain way now, doesn't consign it to that fate forever.

It took years of cynicism to become a cautious optimist. Even now, I still fall into the trap of projecting our current circumstances into the future, as if its already been written. It's not even just fatalist, it's perversely a self-fulfilling prophecy.
