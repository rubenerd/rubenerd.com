---
title: "Twitter Circles feature leaked data, as expected"
date: "2023-05-06T23:10:32+10:00"
abstract: "Remember when I said you shouldn’t trust it?"
year: "2023"
category: Internet
tag:
- privacy
- security
- twitter
location: Sydney
---
Twitter admitted to a privacy breach involving Circles, the feature that lets you limit the visibility of specific tweets to a few trusted accounts. I wasn't sure if it was an *homage* to the same Google+ feature from years prior.

[The Guardian: Twitter admits to security incident involving Circles tweets](https://www.theguardian.com/technology/2023/may/06/twitter-admits-to-security-incident-involving-circles-tweets)

When it was first announced, I posted that you shouldn't trust it with anything you wouldn't want to be public. This confused some people, who claimed that using it that way defeated the purpose of the feature. I agreed! But that's not how the tech world works.

I'm still bracing myself for when the likes of Slack and Discord are broken into, and their messages are leaked or made public. I'll bet there are chats buried in those services that could break careers, companies, or worse.

I really wish there was a word for "worse" that started with C. That alliteration would have been, as Gen Z says, *lit*. Or *pog*, even. It's amazing how words I grew up with are back in vogue again, though for different reasons.

I could have made those observations in a Twitter Circle for the whole world to see. Never mind, my blog here will have to suffice.
