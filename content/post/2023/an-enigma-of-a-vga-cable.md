---
title: "An enigma of a VGA cable"
date: "2023-05-04T08:59:44+10:00"
abstract: "It's physically built, but none of the pins are internally connected."
thumb: "https://rubenerd.com/files/2023/enigma-vga@1x.jpg"
year: "2023"
category: Hardware
tag:
- cables
- retrocomputing
- shopping
- vga
location: Sydney
---
I've started mounting my various upscalers and video converters to the back of my old monitor to save space, but it didn't do much to reduce cable clutter. So I ordered the shortest VGA cable I could find.

I was surprised at how *thin* it was when it arrived. The RS232 cable I inherited from dad's old modem that I use for CGA on my Am386 and Commodore 128 is thicker! I suppose being an analogue cable, it didn't need as much shielding for such a short distance.

<figure><p><img src="https://rubenerd.com/files/2023/enigma-vga@1x.jpg" alt="Photo of a short VGA connector." style="width:500px;" srcset="https://rubenerd.com/files/2023/enigma-vga@2x.jpg 2x" /></p></figure>

Alas, it has the dubious distinction of being the first VGA cable I've owned that doesn't work (or at least, that I remember). I swapped the cable across, and the LCD immediately showed the dreaded *waiting for signal* message. Swapping the cable back, and *The Games: Winter Challenge* reappeared on screen.

I thought maybe there might have been a dodgy pin or two, so I did a closer visual inspection. As far as I could see with my flawlessly myopic eyes, all the pins appeared to be there. This might *sound* like a silly thing to verify, but as anyone who's had to go to a data centre and use one of their KVM trolleys can attest, you can't assume anything when it comes to these connectors.

The next step was to break out continuity mode on the multimeter. One by one I realised *none* of the pins were connected between the two plugs! Just in case there was a mixup, I tested each pin against all the pins on the opposing side. Nothing.

I'm not half as frustrated as I am fascinated. I... *huh!?*

How does someone build, QC, and ship a cable like this? If the aim was to scam someone, would you really have saved much time building the entire rest of the cable, but not doing the final connection? If it was a mistake, how do you not connect *any* of them?

Even though it was only a few dollars, I'm going to send it back or arrange a refund. If this is a scam, they need to pay for it. If not, they need to know that they're sending faulty cables.
