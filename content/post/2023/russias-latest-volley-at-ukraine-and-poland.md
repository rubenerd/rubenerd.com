---
title: "Russia’s latest volley at Ukraine, and Poland"
date: "2023-12-30T08:21:40+11:00"
year: "2023"
category: Thoughts
tag:
- europe
- news
- poland
- ukraine
location: Sydney
---
[The Guardian](https://www.theguardian.com/world/2023/dec/29/russia-launches-huge-wave-missile-strikes-ukraine "Poland reports airspace incursion as Russia launches huge strike on Ukraine")\:

> Russia has launched a huge wave of missile strikes on Ukrainian cities, including the capital, in one of the biggest attacks on the country since the start of the war.
> 
> Poland’s armed forces said an unknown airborne object, which they identified as a Russian missile, entered the country’s airspace from the direction of Ukraine for three minutes.

Иди нахуй. 🌻
