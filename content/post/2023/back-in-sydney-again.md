---
title: "Back in Sydney again, hi!"
date: "2023-04-24T21:31:52+10:00"
abstract: "Back from our Japan 2023 trip."
year: "2023"
category: Travel
tag:
- japan
- japan2023
- singapore
location: Sydney
---
We arrived back home yesterday. It was a great trip, and I've got a lot of follow up posts, photos, and ideas to write about in the coming few weeks!

We're lucky in Australia that we're in the same rough timezone as Japan and Singapore which reduces jetlag, but long flights still leave me feeling exhausted. Time for some bed, then to tackle the unpacking.

**Update:** I wrote this post, but only uploaded it the following morning. I must really have been out of it.
