---
title: "Pronouncing “composite” video"
date: "2023-04-30T13:27:14+10:00"
abstract: "I feel like I must have been saying this wrong the whole time."
year: "2023"
category: Hardware
tag:
- retrocomputing
- video
location: Sydney
---
Composite video isn't as clear as S-Video, or other connectors that separate the luma and chroma signals. Because it's purely a video format, it also can't output any audible instructions on how to pronounce it, which only exacerbates the issue.

I've been saying **com**-posit my whole life, with emphasis on the first syllable. But I hear more people saying com-**pos**-it, with emphasis on the second syllable. Which one is it? I don't know.

It's the word *project* all over again. I've always said **prough**-ject, but coming back to Australia, everyone says **proh**-ject.

These are the thoughts that keep me up at night. Well, that, and the glow from a composite video connector. That I've unplugged and replaced with S-Video where possible.

[Restoring my 1983 Commodore VC-20](https://rubenerd.com/restoring-my-1983-commodore-vc-20/)

That reminds me, I got the parts to do the required mod to my Commodore VC-20 to get S-Video-like output. I'm still not confident enough in my soldering skills yet, especially on an old board I want to respect. I might need to do a bit more practice first. Some **com**-posite experience? Wait, that doesn't make any sense.
