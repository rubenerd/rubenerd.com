---
title: "rsync(1) on FreeBSD with a modern Walkman"
date: "2023-02-24T09:10:50+11:00"
abstract: "It makes synching music so easy."
thumb: "https://rubenerd.com/files/2022/sony-nw-a55m-hand@1x.jpg"
year: "2023"
category: Software
tag:
- music
- rsync
- walkman
location: Sydney
---
One of the primary draws to a [modern Walkman](https://rubenerd.com/my-new-sony-nw-a55-walkman/) is that it presents as a generic storage device, meaning my FreeBSD tower can now act as a media hub, rather than relying on dedicated software like iTunes or Apple Music.

<figure><p><img src="https://rubenerd.com/files/2022/sony-nw-a55m-hand@1x.jpg" alt="Photo showing the tiny A55 in my hand, playing Esther Golton's Stay Warm album" style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2022/sony-nw-a55m-hand@1x.jpg 1x, https://rubenerd.com/files/2022/sony-nw-a55m-hand@2x.jpg 2x" /></p></figure>

You can drag and drop music to the Walkman and its built-in microSD card reader, but I'm using <a style="font-weight:bold;" href="https://rsync.samba.org/">rsync(1)</a> for a few reasons:

* I already use it for everything.

* It makes it trivial to diff my [local music folder](https://rubenerd.com/the-quest-for-high-resolution-album-art/) for new albums and songs I've added, or ones I've fixed ID3 metadata on. I tend to only add an album or two a month, but if I can get a computer to do this work, why not?

* I plan on using a FreeBSD <a style="font-weight:bold;" href="https://www.freebsd.org/cgi/man.cgi?query=devd.conf">devd.conf(8)</a> rule to detect and mount when the Walkman is connected, then transfer new music, then unmount without interaction.

One thing I noticed though is that **rsync(1)** was transferring the entirety of my music library each time, regardless of whether the music already existed on the Walkman. I'm not sure if this was a FAT limitation or quirk of the device, but using **size-only** meant it only transferred new tracks:

	$ rsync --recursive --delete --size-only \
		"$MUSIC_FOLDER/" "$WALKMAN/"

Note that you **don't want to do this when running rsync** outside edge cases like this, because file size isn't a sufficient metric for meaningfully comparing files. But for static music files on an SD card, it's fine.

I copped a lot of flack from Hacker News folks for daring to write about a dedicated music player with offline files, when I could *just* use a streaming service or my phone. I hope this shows another fun use case.
