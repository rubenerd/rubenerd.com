---
title: "The Independent Singapore’s “friend zone” article"
date: "2023-02-05T10:24:20+11:00"
abstract: "Another person surprised that love isn’t a transaction. Do better for yourself."
year: "2023"
category: Thoughts
tag:
- psychology
- relationships
location: Sydney
---
You know when you see someone holding a corrosive belief, and you want to pull them aside and explain, as someone who has their best interests at heart, that they're hurting themselves and those they purport to care about?

The Independent Singapore [posted a guest article](https://theindependent.sg/opinion-there-are-worse-places-than-the-friend-zone-its-called-the-parasite-zone/) by someone offering advice for those caught in the *friend zone*, a mental space where someone's unrequited feelings are a source of resentment. Instead, it offers a cautionary tale.

> We hung out and things seemed to flow so naturally that when she mentioned that she needed a place to stay – I invited her in. I was, as they say, a helpless romantic. But once she moved into the flat, I got “friend-zoned.” 

If he wasn't upfront with his feelings, she saw a friend offering help.

> However, she was troubled and while she did honour her two promises to me (This was back in the Asian Financial crisis and I lent her 2,000 GBP for her school fees and got her to promise that she would do well in her career. In fairness, she paid back what she owed and if the profile I saw was really her – she seems to have done well), she had some funny ideas of what friendship meant.

This exposes the fundamental mistake friend zoners make. **Love isn't a transaction.**

Being an honourable, caring person can surface feelings, but they can't magic them into existence. The truth comes out with empathy: would you develop feelings for someone you don't feel that way about if they took you to dinner or bought you a nice phone? Would you... want it to work like that?

I certainly made this mistake in high school. You could argue we were taken advantage of (the author labels his one-sided crushes "parasites" for added class), but we were willing participants. I want to tell my teenage self that you don't trade favours for feelings, but that's not how growing up works.

The author *approaches* an epiphany; albeit coached in the wrong way:

> ... there are worse places to be than in the friend zone. While it may not be the relationship that you want to be in, a good friendship can add value to your life.

But then the proverbial bottom falls out:

> Having a friendship with a girl, especially a good-looking one, can help you. She can introduce you to her other friends and you may meet the girl of your dreams. Your guy friends will value you more for being friends with a hot, likeable chick.

This might be the most insecure thing I've ever read.

Don't value yourself and your relationships based on superficial bullshit like this. Reducing women to some transactional value is the entire conceit of the *friend zone*. Would you want to know your best friend is only around because they want to get in the pants of someone you know?

Take it from a socially awkward, shy nerd myself: *friend zoning* is a self-fulfilling prophecy. People you have feelings for see through it, and treating potential friends like that will leave you with nobody. Then you're back to resenting people.

Do better for yourself.
