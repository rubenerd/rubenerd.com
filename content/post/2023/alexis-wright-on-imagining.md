---
title: "Alexis Wright on imagining the future"
date: "2023-11-14T18:51:15+11:00"
abstract: "We have to think big."
year: "2023"
category: Ethics
tag:
- internet
- quotes
location: Sydney
---
Alexis is a Waanyi writer, most famous for her books *Plains of Promise* (1997) and most recently *Praiseworthy* (2023) which is probably my next book to read.

[Via Wikiquote](https://en.wikiquote.org/wiki/Alexis_Wright "Wikiquote page on Alexis Wright")\:

> We have to think big...'We have to imagine big, and that's part of the problem. We're letting other people imagine and lead us down what paths they want to take us. Sometimes they're very limited in the way their ideas are constructed. We need to imagine much more broadly.

She spoke within the context of writers, but it could easily apply to IT. We're being sold a reductive future on so many fronts. It's up to us to think bigger.
