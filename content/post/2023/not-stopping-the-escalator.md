---
title: "Not stopping the escalator"
date: "2023-09-24T08:52:16+10:00"
abstract: "It’s easier to gain momentum if you don’t stop"
year: "2023"
category: Thoughts
tag:
- psychology
- work
location: Sydney
---
I was watching an escalator recently, like a gentleman. Weirdly, escalators always scared me far more than lifts as a kid; maybe it was all those spikes at the top, or the potential to trip and fall down. There was one escalator in Shaw House in Singapore that bridged across multiple floors which felt *extremely* precarious as a little guy. *But I digress!*

This particular escalator was fitted with one of those variable-speed sensors. Approaching passengers were detected by the aforementioned stair-based elevation device, and the speed of the steps rapidly increased from its leisurely roll back up to regular operation. Upon its passengers exiting the device at the other end, the escalator would continue at this speed for a few more seconds before slowing again.

These sorts of escalators (and travelators) seem to be more popular at airports, where there might be long gaps of time between arriving flights and hoards of passengers. I was surprised to see this particular unit in a shopping centre where you'd expect a more regular flow of passengers, but perhaps its location in a lesser-frequented area still gave it plenty of idle time to slow down.

Idle time to slow down... sounds nice.

Anyway, I always assumed this speed reduction was to reduce power and noise, though I've since been told it's as much to do with saving wear and tear on the escalator's mechanical components, even accounting for the additional complexity from speeding up and slowing down. It makes sense when you think about it. Everything from light bulbs to hard drives tend to fail the most when they're first powered on, owing to electrical and/or mechanical stress.

But here's where that aforementioned lightbulb was lit. He also reminded that a slow escalator is easier to spool up than one starting from idle.

*Like us!*

I can't tell you how many times I start something, then stop out of a seeming lack of progress. As soon as I stop, the mental barrier to start again is so much higher than had I just plodded on. It's like there's friction in my head I need to overcome to resume what I need to do.

Progress, no matter how small, is letting the escalator keep moving.
