---
title: "Coach Gowron on growth"
date: "2023-08-05T08:52:08+10:00"
abstract: "Don’t be too harsh on your past self. That person is trying to be better now."
year: "2023"
category: Thoughts
tag:
- growth
- life
- personal
- quotes
location: Sydney
---
[On Mastodon today](https://tenforward.social/@coachgowron/110833782521212168)

> Growing as a person involves reexamining your past decisions and actions, maybe even renouncing who you once were. But as much as you learn from your mistakes, don't be too harsh on your past self. That person is trying to be better now.

Related, in the words of Mary Schmich and the *Sunscreen Song*:

> Don't congratulate yourself too much.   
> Or berate yourself either.
> 
> Your choices are half chance.   
> So are everybody else's.
