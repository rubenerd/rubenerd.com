---
title: "Price versus affordability and quality"
date: "2023-11-30T09:35:20+11:00"
abstract: "Someone buying a luxury good is just as influenced by price as someone buying a cheap pair of slippers at a big-box store."
year: "2023"
category: Thoughts
tag:
- economics
- quality
location: Sydney
---
These three things are true:

* Buying something higher quality can save you money in the long term, because you're not having to replace it. It's one of the many perverse ways in which being wealthy is more affordable.

* Something more expensive isn't automatically higher quality.

* This... *is a word!*

My broad impression is that there's a bathtub curve of problems, regardless of whether you're buying bathroom fixtures or not:

1. You start with something crappy, because it's been designed primarily to a *price*. It needs to sell for 100 §, so everything has to be whittled down, compromised, or removed to achieve it. 

2. As you move along, you get to things that are broadly built to a *standard* first, then price. It needs to have feature foo, or behave like bar.

3. Further along still, the item is more expensive because it has to support complementary services, like free repairs, extended warranties, training, and upgrades. The higher margins also let the manufacture spend more on R&D.

4. Eventually the problem curve trends upwards again, because the item has been designed primarily as a status symbol for conspicuous consumption. Once again, it's competing on price.

It's funny how cyclical this is. A person walking into a luxury fashion store is just as affected by price as someone buying another cheap pair of slippers at a big box store (and the item will last as long too).

