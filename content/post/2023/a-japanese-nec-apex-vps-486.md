---
title: "A bit of 486 CPU nostalgia and history"
date: "2023-11-14T08:16:24+11:00"
abstract: ""
thumb: ""
year: "2023"
category: Hardware
tag:
- cpus
- i486
- intel
- retrocomputing
location: Sydney
---
I've blogged about my 386 and Pentium-era hardware before, but not the 80486. This post is a bit about the history of the CPU, and my plans for my own 486 build in 2023. Because that's what rational people do!


### I know I say this about every CPU architecture...

...but the 486 represents a fascinating moment in PC history. Introduced by Intel in 1989, it was the first x86 CPU to exceed a million transistors (the 386 started with 275,000). It included L1 cache and a floating-point unit on the die, a clock multiplier, and even spawned its own expansion bus in the form of VLB (albeit from outside Intel).

That's three brackets in the post already (not that I'm counting).

The 486 coincided with a boom in PC clone sales, which attracted established players and fly-by-night operators making all manner of chipsets and boards, the most notorious of which included <a title="Fake 486 cache chips" href="https://rubenerd.com/fake-486-cache-chips/">fake L2 cache chips</a>. 486 boards were the first to adopt PCI, finally a clean break from ISA. They could be found in the Space Shuttle, Boeing aircraft, a year 7 art project I made, and in embedded systems for decades.

AMD, Cyrix, UMC, TI, and even IBM made 486 CPUs, either by cloning or clean-room reverse engineering. Intel would brand their final variant the DX4 after losing their famous trademark battle with AMD, though the others continued referring to them as 486s.

From a logistics perspective, there were also more than 486 486s manufactured, and the number written backwards was 68408, a number that sounds more like a Motorola chip. Neither of these facts are useful in any way, but I thought they were still worth mentioning.


### My ideal 486 build wish list 

This gets me to my plan to build a 486, at least intially. Ouur first family machine had a 486, and while my memory of its exact specifications is hazy, I figured a build like this would be a great start:

* **Motherboard with a ZIF CPU socket**. Earlier sockets were prone to bending pins if you weren't careful. I shake (though without rattle and roll), and worry about damaging stuff.

* **Dual-voltage CPU support**. Later 486s used 3.3 V instead of 5 V. Intel made Overdrive CPUs with integrated VRMs, but they're rare and expensive. Whatever you save getting an older/cheaper board you pay with less flexibility.

* **DX or DX2-class CPU**. I'm 99% sure our first family PC had a 486 SX, but I already have an Am386-SXL, and I'd love to see what a higher-end 486 is capable of. 

* **Decent chipset.** The quality of 486 motherboards vary wildly. I've been told to look out for SiS or UMC if I can.

* **VESA Local Bus slots**. I’m fairly sure our family 486 had them, but I’ve never used or seen them since. Vanishingly rare 386 boards aside, it was a 486 feature I’m keen to mess with again. There’s a reason they were jokingly called Very Long Bus cards!

* **Real cache memory**. As mentioned, dodgy boards shipped with fake cache chips and manipulated BIOS settings. I've seen committed people solder sockets and replace the BIOS on these rubbish boards to add cache, but my skills aren’t there yet.

* **A cool old case**. I see the case our PC had every now and then on eBay, but I'm not wed to it. Anything interesting that can fit a Baby AT board would be great.

I figure if I can build a system with most of these, I'll be set. Stay tuned!
