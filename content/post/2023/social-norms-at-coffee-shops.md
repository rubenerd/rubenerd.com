---
title: "Social norms at coffee shops"
date: "2023-01-04T08:50:30+11:00"
abstract: "Where you sit seems to be a function of density."
year: "2023"
category: Travel
tag:
- coffee-shops
- people
location: Sydney
---
It may shock some of you to learn that I often work from coffee shops. *No, really!?* They're strange, magical places where I can focus/write/code easier than any home or work office. I've come to realise you either get this bizarre phenomena, or you don't. Yes Ruben, that's how binary works. 

They're also interesting places in their own right, from their wildly different decor, to the specific smells and sounds that only come from someone grinding, brewing, and serving this peculiar drink. No matter which one you sit at around the world, they're always a buzz of conversation and thought which I find infectious and fun.

My favourite high school history teacher Mr Wagner (rest in peace, sir) had a theory that coffee caused the French revolution, given the drink got the bourgeois off their steady diet of wine. I'm not about to rush out and behead a despot after spending my time in these places, though maybe I should.

They're also a lesson in social norms. We accept that a person writing in a journal, or on their laptop, likely wishes to be undisturbed as they concentrate in a place seemingly hostile to their intentions (whether it be the rattling of crockery, loud conversations, distracting music, or the hiss of the espresso machine).

The other is personal space. It was normal in Singapore and Malaysia for me to offer the spare chair at my table, especially during peak periods where they may otherwise have to wait for an age to get a seat. In the days of hour-long battery life, I even brought power boards which random strangers with anime stickers plastered on their laptops would plug into. I wasn't sharing illicit drugs, I was dolling out shots of electricity! I wonder where those people are now.

But a quirk of our human nature means we treat people differently depending on density. Whereas sharing tables within close quarters are expected and reasonable, it's unsettling in a near-empty coffee shop to be asked to share your table, or have someone sit right next to you. We interpret it as being intimate in a way that's uninvited or unnecessary, given there are so many other places to sit. And unfortunately, I suspect I'd have an extra level of anxiety if I weren't a man in this situation.

I've been sitting with an older Cantonese gentleman for the better part of half an hour here. He hasn't said much, but every now and then I look up and he smiles, then goes back to his newspaper. Maybe he just [needs the company](https://mstdn.social/@stux/109628030839487584). Happy to oblige.

