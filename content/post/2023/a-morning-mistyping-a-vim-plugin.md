---
title: "Mistyping a Vim plugin on my FreeBSD laptop"
date: "2023-05-01T10:01:12+10:00"
abstract: "Typing UltiSnip as UtilSnip"
year: "2023"
category: Software
tag:
- errors
- mistakes
- vim
location: Sydney
---
I think it's important to share one's mistakes. It might help someone with a specific issue. More broadly, I hope it reassures new people in the industry that we're all human.

> **Spock:** "I find that remark... insulting."

Today's mistake was extremely silly. I was configuring a fresh FreeBSD install on my laptop, like a gentleman, when I got stuck trying to define some text snippets. It just... wouldn't work. GAH.

If you have Vim experience, see if you can spot the problem:

    :UtilSnipsEdit
    ==> E492: Not an editor command: UtilSnipsEdit

This confounded me for *at least* twenty minutes. I removed my Vim dotfiles, reinstalled the plugins with **vim-plug**, nothing. I have **wildmenu** enabled so I can tab-complete commands, and even that didn't show anything. It was as though the plugin wasn't loading at all.

[vim-plug: the minimalist Vim plugin manager](https://github.com/junegunn/vim-plug)   
[UltiSnips: the ultimate snippet solution for Vim](https://github.com/SirVer/ultisnips)

As you can tell from the links above, I was spelling it as **UtilSnips**. Once I realised, I laughed louder than what is perhaps socially acceptable in this coffee shop. The guy in the corner is still looking at me.

To this day I still think UltiSnips is a truncation of **Utility** Snips, not **Ultimate** Snips. I guess my fingers were subconsciously typing it like that today too. If only I had a UtilSnip... DAMN IT, UltiSnip snippet defined for its name.
