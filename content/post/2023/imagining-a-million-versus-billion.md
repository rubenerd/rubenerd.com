---
title: "Imagining a million versus billion"
date: "2023-12-11T11:48:16+11:00"
abstract: "A million seconds is about 12 days. A billion seconds is 31 years!"
year: "2023"
category: Thoughts
tag:
- numbers
- psychology
- time
location: Sydney
---
Did you know a **million** seconds is about 12 days, and a (short scale) **billion** seconds is 31 years!?

I remember one of my lecturers telling us this, but [Heliograph reminded me](https://mastodon.au/@Heliograph/109923222025740867) recently. I love comparions that put things into context like this.

Note too this is a short-scale billion. More rational languages say a billion is a million million, which would be more than 31.7 *millennia*, or the time it takes for an average telco to send a tech out.
