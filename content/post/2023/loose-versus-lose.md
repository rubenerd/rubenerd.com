---
title: "Loose versus lose"
date: "2023-04-05T08:18:09+10:00"
abstract: "Some mnemomics to help remember the difference."
year: "2023"
category: Thoughts
tag:
- language
location: Sydney
---
Here's a [mnemonic](https://en.wikipedia.org/wiki/Mnemonic) to remember:

* You **lose** a letter O spelling it that way.

* **Tight** and **loose** are opposites, but have the same length.

English... it makes sense until it doesn't!
