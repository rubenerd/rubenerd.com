---
title: "Dan Olson and Adam Conover discuss the open web"
date: "2023-11-20T14:32:33+11:00"
abstract: "“We need a new version of the web, so we can get a do-over”."
year: "2023"
category: Internet
tag:
- business
- walled-gardens
location: Sydney
---
They were [talking about](https://www.youtube.com/watch?v=4aU-QkJfgGw "Debunking the Tech Hype Cycle with Dan Olson - Factually! - 213") the growth ambitions of a large social network, though it applies to so many web businesses. This was about 36 minutes in, edited for brevity:

> **Dan:** This applies to a lot of tech guys. This sort of regret that they don't have a time machine to hire Tim Berners-Lee and turn the web into a walled garden. If AOL...
>
> **Adam:** ... had won!
>
> **Dan:** ... or even been just five years earlier, then you would be a trillionaire. I think a lot of this is just "we need a new version of the web, so we can get a do-over."

Adam's introduction about tech-based hype is also well worth a watch.
