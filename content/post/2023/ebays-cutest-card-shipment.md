---
title: "eBay’s cutest anime card shipment"
date: "2023-10-08T14:45:45+11:00"
abstract: "From waifumon, complete with cute card holders!"
thumb: "https://rubenerd.com/files/2023/waifumon@1x.jpg"
year: "2023"
category: Anime
tag:
- cards
- fate
- fate-grand-order
- shopping
location: Sydney
---
This might the most fun one Clara and I have ever received. Not only were the cards shipped in adorable sleeves, but she even bundled a free [Fran](https://fategrandorder.fandom.com/wiki/Frankenstein "Fran's page on the Fate/Grand Order Wiki") with our [Jeanne Alter](https://rubenerd.com/grailed-jeanne-alter-quotes-daft-punk/ "Grailed Jeanne Alter in FGO quotes Daft Punk") Berserker!

Head over to [waifumon's eBay Germany store](https://www.ebay.com.au/usr/waifumon "User profile of waifumon on eBay") if you're after some. Use the shipping instructions box to say her new favourite customers in Australia sent you :).

<figure><p><img src="https://rubenerd.com/files/2023/waifumon@1x.jpg" alt="Photo of two holo Goddess Story cards, one of Jeanne Alter, and the other of Fran. There are two packing sleeves. Messages say: Fate. Jeanne d'arc Berserker. Thank you for buying! waifumon. Waifu Card. They're surrounded by hearts" srcset="https://rubenerd.com/files/2023/waifumon@2x.jpg 2x" /></p></figure>

