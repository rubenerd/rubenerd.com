---
title: "When have you checked your ears?"
date: "2023-06-27T09:54:23+10:00"
abstract: "No point having a higher bitrate if you don’t hear it."
year: "2023"
category: Thoughts
tag:
- audio
- music
- personal
- health
location: Sydney
---
IT people like me, and perhaps you, are a bit strange. We'll spend hours slaving over technical specifications for keyboards, cameras, and headphones to get the best possible device for a given budget and requirement... then interface them with our body without a second thought.

I was at my local GP for an unrelated issue, when as a matter of procedure he checked my ears. In short, he claimed I probably had compromised hearing for quite some time. It would muffle sound, make vertigo from migraines worse, and would make flight landings more painful.

It's the kind of observation you risk attributing every ill in your life to, but wow this would explain a *lot!*

I can't get them irrigated right now, but now I'm *very* interested to hear any difference when I do. It'd be a bit hilarious to think I've been gradually upgrading my local audio library this whole time, but my ears have been stuck on Type I cassette mode, with perhaps *too much* Dolby noise reduction. Thank you.

If you can, get your ears checked. At worst, you'll be told everything is fine. At best, it might do more to upgrade your audio experience than any bitrate... and might take up less space.
