---
title: "Giving up on a forever crossing"
date: "2023-11-03T07:47:43+11:00"
abstract: "Waiting at a light for more than 10 minutes made me think of other systems in my life that lack a feedback mechanism."
year: "2023"
category: Hardware
tag:
- analogies
- cards
- roads
- system-design
location: Sydney
---
I've been talking a lot about crossings and roads again recently. I wouldn't say they'd been on my mind lately, so much as they've forced themselves upon me. They're also a fascinating physical system anyone can watch evolve and change in real time: *a car-switched network* you could say.


### This traffic light

Clara and I were waiting to cross a T junction last week. We arrived, pressed the beg button, and stood there waiting. We don't "jaywalk", but even if we did, there was no gap in this peak-hour traffic to mosey through. As Jason from Not Just Bikes would say, the smelly car sewer was flowing at full capacity that evening.

We continued our previous conversation, interrupted only by some messages from my sister to which I replied.

A small crowd of people gradually gathered around us, as the number of cars continued to stream past unabated. We could hear a gen X couple quote that *Shawshank Redemption* line about the world getting itself into a big hurry, which I appreciated.

We kept waiting.

More people arrived. The footpath started to get a bit crowded, with others having to shimmy their way around us to continue walking down the street. We reorganised ourselves to let someone through with a walking frame, then huddled back around the pedestrian crossing, lest a limb or bag inadvertently overhang onto the street where it could be clipped by a motorist who drove too close to the kerb.

We waited some more.

I looked back down at the conversation I'd been having with my sister, and realised I'd sent that first message more than 5 minutes ago. I joked with Clara about that research that found people begin to cross on red when they've had to wait for more than a minute and a half. Maybe it was a peak-hour traffic control or something, because naturally cars are more important than pedestrians.

It wasn't until I heard a chorus of horns that it dawned on me that something was off. I glanced over at the road perpendicular to us that also had the red this whole time, and the queue of farting metal boxes stretched as far as the eye could see. People weren't beeping their horns to deal with an errant motorist, they were doing it out of frustration. A bus crammed full of standing peak-hour commuters sat in the far lane, which looked like an even less fun situation.

We kept waiting!

You know when you're waiting for a lift, and it's been stuck on one floor for so long that you give up and take the stairs? Initially it's a normal wait, then it's a bit irritating, then it's frustrating. Yet, you stay there waiting, thinking Murphy's Law all but confirms the lift will come for you as soon as you turn your back. We were now doing the horizontal equivalent of this at an intersection, only we were waiting for legal passage through a stream of metal.

I'm not sure how long we were waiting at that light, but eventually a critical mass of people had enough and began walking away. The next crossing wasn't an insignificant distance away, but it had to be a damned site better than waiting for the heat-death of the universe. At least we had a choice, unlike those motorists stuck behind a red for ten minutes.

Clara and I walked with everyone down to the next block, where the lights were behaving as normal. I'll admit, the camaraderie and joy when *this* light turned green felt pretty great, like sharing a joke with strangers in a packed movie theatre. By the time we'd doubled back to where we had tried to cross before, the lights were still firmly stuck in place. The queue of cars waiting could best be described as... irate. I didn't blame them.


### Agency and feedback mechanisms

It's here in posts like this where I like to draw an analogy to something else, usually contemporary computer systems or the web. This one is obvious! How long are we all stuck somewhere because a computer somewhere has a bulb set wrong? Or a timer? Or some other state? 

These sorts of situations are frustrating not merely for the time wasted, but the complete lack of agency we have to affect the outcome. Systems like (bad) traffic lights don't have a feedback mechanism, or if they do, they're apparently liable to enter a condition from which they're incapable of ack'ing or acting upon external inputs. Their failures have a measurable impact on the safety of people's lives, regardless of cause.

Traffic light systems must be pretty reliable in the grand scheme of things; I rarely see them getting stuck or [going on the blitz](https://www.youtube.com/watch?v=z_jdiU47bFA "YouTube: Sweet, The Ballroom Blitz!"). The last few times I've had to cross a road dangerously were the result of a power fault at street level, not a failure of the control systems of the lights themselves (though maybe they should have backup power).

I did see a crossing in KL once show green *and* red on the same bank of lights, something I would have thought logically impossible for a computer to execute (motorists predictably interpreted that as green, the most convenient signal they could receive). But these systems are still governing physical devices, which any storage or network engineer will tell you can go rouge. A cable can be knocked out, a drive can fail, a lightbulb can short. Very easy thing to happen when you've just had another monsoon.

But back to feedback mechanisms. I felt something similar recently when a site refused to email me a login code. There really isn't much you can do beyond trying again, waiting, trying again, and coming back later. The size of the company effectively insulates them from individual feedback about the fault, though these recurring issues did ultimately lead me to choose another site to solve a problem. The workaround for the traffic light situation was to use other ones as well, albeit ones much further away.

Systems need feedback mechanisms if they're expected to exist in, and more importantly, *direct* behavior in real world. I suppose how they implement them is the tricky bit.
