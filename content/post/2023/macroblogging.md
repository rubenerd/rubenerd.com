---
title: "Macroblogging"
date: "2023-09-09T08:21:30+10:00"
abstract: "Microblogging implies the existance of this blog style too."
year: "2023"
category: Internet
tag:
- blogging
- weblog
location: Sydney
---
People are scrambling for alternatives with the downfall of Twitter. The service pioneered the concept of brief, concise posts which every other social network soon copied. In a world of short attention spans and instant gratification, the format seemed all but inevitable.

Out of this spawned a new type of writing, dubbed *microblogging*. Like Twitter, they consist of short posts without titles, or much metadata at all beyond a date. There are a few indie services offering these, or in a pinch you can turn a blog like WordPress into one.

But the phrase *microblog* implies the existence of a *macroblog*, and that's what I miss the most. Back when people used to write more than a sentence or two about their day, the flowers they saw, the problems the solved, the people they talked with, the places they travelled.

This is why I've started writing more about my travels and life of late, because its what I miss seeing from other people. It *used* to be that you could subscribe to your friends' blogs and see what they were up to. Now we only get fragments on a social network that will end up deleting them.

Microblogs have their place, but they make me a bit melancholic. It's time to bring back the macroblog!
