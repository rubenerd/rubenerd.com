---
title: "Becoming a Moonafic of @Moonahoshinova"
date: "2023-04-03T08:13:21+10:00"
abstract: "This was long overdue!"
thumb: "https://rubenerd.com/files/2023/moona-membership.jpg"
year: "2023"
category: Anime
tag:
- hololive
- hololive-indonesia
location: Sydney
---
<figure><p><img src="https://rubenerd.com/files/2023/moona-membership.jpg" alt="Membership welcome screen for Moona Hoshinova" style="width:500px; height:300px;" /></p></figure>

This was long overdue! [Moona Hoshinova](https://www.youtube.com/@MoonaHoshinova/streams) of Hololive-ID has become one of Clara's and my regulars, and it was so much fun seeing her perform again at Holofes this year. I had so many Indo friends at school in Singapore, and her voice is surprisingly nostalgic 💜.

The Indonesian branch of Hololive really punch above their weight; they're all excellent. If you want a place to start with Moona, I recommend checking out her Minecraft streams. Her creative flair and eye for detail have been responsible for some jaw-dropping designs. Watching talented people do what they do best is one of the highlights of my life.
