---
title: "R U OK day 2023"
date: "2023-09-15T19:26:22+10:00"
abstract: "They teach us how to ask if you're okay. But not how to respond."
year: "2023"
category: Thoughts
tag:
- health
- mental-health
location: Sydney
---
Australia was abuzz with this year's *R U OK?* campaign, billed to:

> inspire and empower everyone to meaningfully connect with the people around them and start a conversation with those in their world who may be struggling with life.

Clara had the best line about it:

> They teach us how to ask if you're okay. But not how to respond.

I'm not okay. I'm not sure most of us are, to be honest. But whatever noble causes or motivations were behind this movement, it's interesting how it's being met with such widespread cynicism now, even compared to previous years.

I suspect people resent it being used by businesses and other institutions to feign concern without addressing any of the root causes. Your circumstances are getting worse all around you, but don't worry, you can talk about it.
