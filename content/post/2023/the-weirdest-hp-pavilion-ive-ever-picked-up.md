---
title: "The weirdest HP Pavilion I’ve ever picked up"
date: "2023-08-06T11:13:13+10:00"
abstract: "This motherboard clearly wasn’t made for this machine."
thumb: "https://rubenerd.com/files/2023/weird-hp-01@1x.jpg"
year: "2023"
category: Hardware
tag:
- cpus
- ewaste
- recycling
location: Sydney
---
I regularly pick up electronics abandoned on the side of the road. Mostly for nostalgia, but also so I can properly send non-functional kit to e-waste. The last thing we need is more of this stuff in landfills, leaching all manner of horrible stuff into the ground.

I've been out of the pre-built PC desktop game for many years now, but this Pavilion tower I recently picked up was downright weird to me. What first grabs your attention looking at this?

<figure><p><img src="https://rubenerd.com/files/2023/weird-hp-01@1x.jpg" alt="View of the front of the HP, showing its vertical optical drive mounted in the middle of the case" srcset="https://rubenerd.com/files/2023/weird-hp-01@2x.jpg" style="width:500px; height:333px;" /></p></figure>

I was first struck by the absurdity of the silver optical drive tray. The case clearly has space to mount it normally, but instead they positioned it vertically. This isn't that unusual compared to other contemporary manufacturers, but HP took it a step further and placed it in the middle, thereby wasting as much internal volume as possible. *Why* would you do this, beyond a misplaced sense of aesthetics?

Let's turn this suspiciously light case around to see:

<figure><p><img src="https://rubenerd.com/files/2023/weird-hp-02@1x.jpg" alt="View of the mostly-empty back of the HP" srcset="https://rubenerd.com/files/2023/weird-hp-02@2x.jpg" style="width:500px; height:333px;" /></p></figure>

Wow, there's really not much going on here. This has clearly been cost cut as much as humanly possible; the entire back of the chassis is stamped from a single sheet of thin metal. There's no motherboard IO shield, those are holes punched out for the ports. If you want to use another board in the future, better break out your Dremmel. Those also aren't screws holding the black folded panel to the chassis, it's riveted in place. I hope you don't need access to the back!

That power supply has to be the weirdest part of all. That positioning above the motherboard makes absolutely no sense, especially when you have that blank panel above you could use. The shape suggests a proprietary unit as well, which all but guarantees this machine's trip to e-waste if it fails, unless you were able to source an identical part.

It's now time to open the one side panel we can, and check the inside. I'm *genuinely* curious now what this could be.

<figure><p><img src="https://rubenerd.com/files/2023/weird-hp-03@1x.jpg" alt="View of the mostly-empty inside of the HP, without a power supply in sight,and a tiny Mini-ITX board" srcset="https://rubenerd.com/files/2023/weird-hp-03@2x.jpg" style="width:500px; height:333px;" /></p></figure>

Well, I can truthfully say I was not prepared for this... though maybe I should have been. As I suspected, the optical drive is hanging out right in the middle of the case using as much space as possible, though clearly that's not a concern in this empty computer.

As you can see in the lower right, *that fan wasn't a power supply.* If you look back to the second photo, you can see there's clearly a blank space where an ATX power supply would go, but instead the entire machine is powered from a proprietary barrel-like jack below the VGA socket. I wouldn't even know where to source a plug like that, let alone what brick it would need. And why do that when you literally have a space to fit a regular ATX supply?

The case fan is otherwise a standard black Delta DC brushless, which I actually quite like at 80mm+ sizes. They're no Noctua or Arctic, but they're extremely reliable, easy to clean, and relatively quiet. This will find a home in my Pentium 1 machine.

The CPU's anemic heatsink and tiny, sideways-mounted fan also belie what performance we could expect. I suspect it's an Atom or a low-end Celeron in a BGA package, but I guess we'll find out shortly. Time to extracate the board:

<figure><p><img src="https://rubenerd.com/files/2023/weird-hp-04@1x.jpg" alt="View of the tiny motherboard on our balcony table, with heatsink for the CPU removed" srcset="https://rubenerd.com/files/2023/weird-hp-04@2x.jpg" style="width:500px; height:333px;" /></p></figure>

The board was affixed to the standoffs on the riveted chassis with a set of those nasty Torx/flathead combo screws, which somehow make both driver heads a pain to use. I recycle these even on 1990s machines I attempt to keep stock, and replace them with ISO 8764 heads. Life is too short for bad screws.

The board's RAM and Wi-Fi module were harvested by the previous owner, though the coin-cell battery is still in its holder, which might prove useful for my 386 project. A pin header for "SATA Power" suggests this machine used an older 2.5-inch drive for storage, which is also missing. 

I also took the opportunity to pop off the CPU heatsync to uncover the brains of this operation. The CPU has an **SR2KQ** (gesundheit) marking, which looks to be this souped-up operator:

[Intel Pentium Processor J3710 (2M Cache, up to 2.64 GHz)](https://www.intel.com/content/www/us/en/products/sku/91532/intel-pentium-processor-j3710-2m-cache-up-to-2-64-ghz/ordering.html)

Here's a closeup of the CPU, having had its thermal paste cleaned off.

<figure><p><img src="https://rubenerd.com/files/2023/weird-hp-05@1x.jpg" alt="Close view of the CPU, showing an abbreviated version of the old Intel circle logo, and the SR2KQ marking." srcset="https://rubenerd.com/files/2023/weird-hp-05@2x.jpg" style="width:500px; height:333px;" /></p></figure>

Without a way to validate it works, and little inclination to sink money and time into sourcing and buying a power supply, I think I might hit this with a hot air rework station to remove it from the board, and add it to my odd CPU collection. The only other BGA CPU I have is an old Crusoe that powered an ill-fated Sony VAIO laptop from my childhood. It still frustrates me that I almost had a rare SPARC CPU in a similar package, but it got lost in the post.

To bring this post to a close, what can we conclude from this hour or so digging around on this weird, underwhelming machine?

The nonsensical power delivery, empty chassis, laptop RAM sockets, and tiny CPU heatsink point to this board being from one of those mini desktops, which is fascinating to me. How did it end up in a massive, empty box? All I can think of is HP's consumer division were asked to deliver a mini desktop with an optical drive, but the bean counters wouldn't let engineering build a new machine for an increasingly-legacy part. Instead, the owner of this desktop got a tiny motherboard with zero upgrade or maintenance potential in a massive old chassis.

These sorts of weird hybrids don't make sense to me on paper. Surely the point of these tiny machines is to reduce shipping and storage costs, which would be completely negated here. I suppose they figured they still came out ahead not retooling for another SKU.

Regardless of how this compromised box came into existence, it made for an interesting afternoon, and I was able to get some parts out of it. The rest is off to e-waste.

