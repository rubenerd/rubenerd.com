---
title: "Using WordPress for a self-hosted blog"
date: "2023-05-31T10:10:48+10:00"
abstract: "It’s fine, but run it as stock as you can."
year: "2023"
category: Software
tag:
- blogging
- reviews
- wordpress
location: Sydney
---
While I long since moved off WordPress for my own blog, I've mentioned that I still maintain various installs for others. This has prompted a few of you to ask if it’s worth it for a personal blog.

I’d say so. If you're not tempted by static site generators, or prefer running server-side software (as I keep being tempted back to), it works fine. There are *so many* benefits to hosting your own material if you can, regardless of what you use. In the words of my late mum: *stop worrying about the chisel, and get carving!*

I only have one caveat: **run it as stock as you can**. For example:

* Limit yourself to the essential Classic Editor plugin (because the Gutenberg page builder is awful for writers). WordPress becomes a bit more brittle, slow, ugly, and insecure for each modification you make. Every breath you take. Every move you make. And so on.

* Only used WordPress-supplied or open-source themes. This isn't to say people shouldn't be paid for their services, but closed-source themes tend to not be written as well, are regularly abandoned or lag behind supported WordPress releases, and make upgrades and ongoing maintenance more difficult than they need to be.

The vast majority of WordPress issues I'm asked to troubleshoot are related to these two points. Run stock, and you'll have an infinitely nicer experience.

If you're installing on your own server, or have access to a shell on your shared web host, I also recommend using the Subversion install to make upgrades easier.

Most importantly of all, if you start writing, please tell me!

[WordPress Codex: Installing with Subversion](https://codex.wordpress.org/Installing/Updating_WordPress_with_Subversion)   
[WordPress plugins: Classic Editor](https://wordpress.org/plugins/classic-editor/)   
[The Police: Every Breath You Take](https://www.youtube.com/watch?v=OMOGaugKpzs)
