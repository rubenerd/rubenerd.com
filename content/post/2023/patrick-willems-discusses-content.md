---
title: "Patrick (H) Willems discusses “content”"
date: "2023-12-16T08:14:22+11:00"
abstract: "Why art is more than just content."
year: "2023"
category: Media
tag:
- content-creation
- videos
location: Sydney
---
I've mentioned a few times now why I push back on the label [content creator](https://rubenerd.com/stop-calling-people-content-creators/ "Stop calling people content creators"). My assertion that *Michelangelo wasn't creating content* is the most popular quote from it, but I want to emphasise this isn't limited to famous or globally influential people. If you're an artist, writer, developer, designer, whomever, your artistic expression isn't merely *content*. To label it as such is disrespectful, flippant, and a bit cynical.

I talked about this with Clara, and she pointed me to this recent video by her favourite film critic Patrick Willems where he does a deep dive on the subject. I almost needed neck surgery for all the nodding I was doing.

<figure><p><a target=_BLANK href="https://www.youtube.com/watch?v=hAtbFwzZp6Y" title="Play Everything Is Content Now"><img src="https://rubenerd.com/files/2023/yt-hAtbFwzZp6Y@1x.jpg" srcset="https://rubenerd.com/files/2023/yt-hAtbFwzZp6Y@1x.jpg 1x, https://rubenerd.com/files/2023/yt-hAtbFwzZp6Y@2x.jpg 2x" alt="Play Everything Is Content Now" style="width:500px;height:281px;" /></a></p></figure>

Patrick discusses the history of the term *content* in academia and media, and even some modern business context I hadn't realised. I'm not sure I'm completely on board with his assertion that we need "stars", but the implications of labelling everything as *content* to fill a container are clear.

