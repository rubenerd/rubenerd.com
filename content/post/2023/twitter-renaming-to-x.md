---
title: "Twitter renaming to X"
date: "2023-07-25T16:36:02+10:00"
abstract: "Just kidding, I’m not going to post about that."
year: "2023"
category: Internet
tag:
- lists
- pointless
location: Sydney
---
Just kidding, I'm not going to post about that. Instead, here are a list of words that don't have the letter X in them:

* Grilled cheese sandwiches
* Disenfranchisement
* Extreme

Wait, damn it.
