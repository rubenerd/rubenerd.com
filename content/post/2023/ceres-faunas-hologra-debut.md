---
title: "Ceres Fauna’s HoloGra Debut"
date: "2023-06-07T20:33:09+10:00"
abstract: "It was everything I hope it’d be"
thumb: "https://rubenerd.com/files/2023/yt-ag8sE5mh1BU@1x.jpg"
year: "2023"
category: Anime
tag:
- ceres-fauna
- hololive
location: Sydney
---
My HoloEN Council oshi has made her first appearance in Hololive's long-running series of anime shorts! It was everything I hoped it'd be.

[Ceres Fauna Ch. hololive-EN](https://www.youtube.com/@CeresFauna/streams)

If you've never watched any Hololive, or don't even know what a vTuber is, this is still absolutely ridiculous and a lot of fun. Fauna definitely has the best acting skills in EN.

<figure><p><a target=_BLANK href="https://www.youtube.com/watch?v=ag8sE5mh1BU" title="Play 【アニメ】大自然の脅威を知るがよい"><img src="https://rubenerd.com/files/2023/yt-ag8sE5mh1BU@1x.jpg" srcset="https://rubenerd.com/files/2023/yt-ag8sE5mh1BU@1x.jpg 1x, https://rubenerd.com/files/2023/yt-ag8sE5mh1BU@2x.jpg 2x" alt="Play 【アニメ】大自然の脅威を知るがよい" style="width:500px;height:281px;" /></a></p></figure>

