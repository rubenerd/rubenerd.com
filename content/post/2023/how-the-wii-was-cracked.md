---
title: "Remembering how the Nintendo Wii was cracked"
date: "2023-05-28T10:24:40+10:00"
abstract: "I heard you like buffer overflows!"
year: "2023"
category: Hardware
tag:
- cryptography
- games
- nintendo
- security
location: Sydney
---
[Modern Vintage Computing](https://www.youtube.com/watch?v=4BlpONgj74A)

> Nintendo's RSA implementation contained a critical flaw. They used the C `strncmp()` [string compare] function that has the side-effect of terminating when `null` is found. Nintendo was passing byte values to the `strncmp()`, so in the event of null bytes found early in the hash, brute-forcing the SHA1 hash could be performed in minutes. That, in turn, meant that digital signatures could be easily faked.
> 
> ... they could [now] run code unsigned ...

This was my favourite nugget in a great video about how Team Tweezers circumvented protections in the Wii console to run homebrew software. It proves how tenuous all this stuff can be when your cryptography is let down by one of the most fundamental C programming mistakes they teach you at uni.

Hold on, I just heard someone shout *RUST!* in the distance. Maybe they shouldn't have dunked their Nintendo Wii in water.
