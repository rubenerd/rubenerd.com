---
title: "“What do you want me to do?”"
date: "2023-07-01T10:47:52+10:00"
abstract: "Exploring this versatile and useful question."
year: "2023"
category: Thoughts
tag:
- advice
- overanalysis
- work
location: Sydney
---
While sitting at a lobby coffee shop this morning (shocking!), I saw a delivery worker arrive with a package. After failing to receive any acknowledgement from the downstairs intercom, he whipped out his phone and had a lengthy call with the recipient.

I'm paraphrasing, but it went like this:

> I'm downstairs with your package. Uh huh. Yes. Can I deliver it? Yes? Where to? Okay, but are you here? Do you want me to leave it here? No? Okay, can you buzz me up then? No, I'm downstairs. The address on the package. I can come up, or leave it here, which one?

After what seemed like minutes of back-and-forth with no progress, and an increasingly shrill voice over the phone that could be heard across the room, he threw down the gauntlet the way only a consummate professional can, and asked calmly but firmly:

> What do you want me to do?

The recipient's voice went quiet enough to be inaudible to me again, he thanked them, and he returned to his van with the package in tow. Well, technically his arm. Mmm, that's a good pun.

Long-term readers of my silly writing here know that I never pass up an opportunity to overanalyse everything! But there was something amazing to that exchange, and I think there's something we can learn from it.

One of the *many* reasons I have such deep respect for service and hospitality workers is for their ability to negotiate, diffuse, and resolve awkward situations, often times with a hostile party. They shouldn't *have* to, but we've all seen entitled people making unreasonable demands.

Asking someone *what they want to do* is one of those phrases I was always taught to keep in my back pocket in case something got silly. It's been especially useful when I assist on technical support queries (we're a small company and we wear multiple hats).

It's great for a couple of reasons:

* It helps reframe a derailed conversation away from complaints and back to something actionable. This is what both parties want, but it can't happen when people are talking past each other.

* In more aggressive exchanges, it pushes the burden back onto the person to justify and explain their demands. This makes it easier to either defend your position, or to give concrete reasons why you can't do something. This is where you can also offer alternatives that you *can* do.

Kudos to that delivery worker. I can only imagine how many of those exchanges he must have in a given day.

