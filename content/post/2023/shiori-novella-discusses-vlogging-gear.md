---
title: "Shiori Novella discusses vlogging gear"
date: "2023-12-21T08:40:23+11:00"
thumb: "https://rubenerd.com/files/2023/yt-HWyIexYZHNk@1x.jpg"
year: "2023"
category: Hardware
tag:
- cameras
- hololive
- hololive-english
- shiori-novella
- video
location: Sydney
---
Hololive English's Shiori is my favourite of the latest EN generation. She recently did a fun five minute video discussing the various [portable cameras and kit](https://www.youtube.com/watch?v=HWyIexYZHNk "Play Shiori Talks Tech -　Vlogging Gear for Content Creators.") she's used in the past, and how she records her "hand cams" as a vtuber. Lots of great tips and info, and a great introduction if you've ever been curious about vtubers!

*(My only gripe is she uses the term [content creator](https://rubenerd.com/stop-calling-people-content-creators/ "Stop calling people content creators"). Undoing this Valley Speak for creative people is going to take a lot of work)*.

I ended up recording a ton of impromptu video during my last business trip to Melbourne; it was way more fun than I expected, and I'm not even a person playing a cute Live 2D anime character. I'm genuinely considering getting something optimised for video next time, in lieu of another still camera I always get. Clara and I did have a DJI camera, but it was such a monumental PITA to use I ended up... not using it.

<figure><p><a target=_BLANK href="https://www.youtube.com/watch?v=HWyIexYZHNk" title="Play Shiori Talks Tech -　Vlogging Gear for Content Creators. #tech #vtuber #contentcreator"><img src="https://rubenerd.com/files/2023/yt-HWyIexYZHNk@1x.jpg" srcset="https://rubenerd.com/files/2023/yt-HWyIexYZHNk@1x.jpg 1x, https://rubenerd.com/files/2023/yt-HWyIexYZHNk@2x.jpg 2x" alt="Play Shiori Talks Tech -　Vlogging Gear for Content Creators. #tech #vtuber #contentcreator" style="width:500px;height:281px;" /></a></p></figure>

