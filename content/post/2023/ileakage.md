---
title: "iLeakage"
date: "2023-10-27T08:10:18+11:00"
abstract: "Spectre hits Apple’s silicon as well."
year: "2023"
category: Hardware
tag:
- apple
- cpus
- security
location: Sydney
---
[iLeakage: Browser-based Timerless Speculative Execution Attacks on Apple Devices](https://ileakage.com/)\: 

> We present iLeakage, a transient execution side channel targeting the Safari web browser present on Macs, iPads and iPhones. iLeakage shows that the Spectre attack is still relevant and exploitable, even after nearly 6 years of effort to mitigate it since its discovery. We show how an attacker can induce Safari to render an arbitrary webpage, subsequently recovering sensitive information present within it using speculative execution. In particular, we demonstrate how Safari allows a malicious webpage to recover secrets from popular high-value targets, such as Gmail inbox content. Finally, we demonstrate the recovery of passwords, in case these are autofilled by credential managers.

Fun! Just in case you thought Apple's Silicon was somehow immunue.
