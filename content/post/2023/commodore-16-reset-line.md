---
title: "A strange Commodore 16 reset line"
date: "2023-07-06T14:45:12+10:00"
abstract: "It goes high immediately after boot. Not sure it’s supposed to do that?"
thumb: "https://rubenerd.com/files/2023/c16-scope@1x.jpg"
year: "2023"
category: Hardware
tag:
- commodore
- commodore-16
- retrocomputing
location: Sydney
---
I've mentioned a few times that my Commodore 16 has some strange boot behaviour, though its gradually getting worse. The machine won't power on immediately, but doing a power cycle after a couple of minutes (usually) brings it to life. The hardware reset switch also does nothing.

I checked the power regulator and VCC on the CPU and confirmed 4.90 V on my cute little ZEEWEI scope. I thought that might be a bit low, but I checked my fully-functional Plus/4, and it operates fine at 4.91 V.

I next tried tried substituting the PLA, CPU, and TED one by one from the C16 to the Plus/4, all of which worked perfectly. I also have a 64K memory upgrade below the TED, which I removed and re-added with no difference. 

Given the reset button also doesn't work, I decided to do a bit of digging on the reset line. I've watched enough 8-bit Commodore repair videos to know they typically start low for half a second(ish) before going high. I wasn't sure if this applied to the 264-series of machines, so I probed pin 40 on the 8501 CPU in my Commodore Plus/4, and got a reset pulse of about half a second before it went high, as expected.

<figure><p><img src="https://rubenerd.com/files/2023/c16-scope@1x.jpg" alt="Photo of my tiny scope showing the reset line high on pin 40 of the CPU inside this C16." style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2023/c16-scope@2x.jpg 2x" /></p></figure>

But here's where it gets interesting (or not, I'm not sure)! I probed the exact same pin on the Commodore 16's 8501 CPU (shown above), and the signal goes high *immediately* when the machine is started, and stays there. Sometimes the machine boots on first try, other times I need to power cycle it a few times. The only constant is the reset line stays pinned at 4.90 V, seemingly regardless of what I do.

When I'm able to boot into Rob Clarke's excellent 264 diagnostic cart, it passes with (literal!) flying colours. So I'm wondering if its something as simple as a timing issue? But I'm not even sure what components are responsible for setting the duration or width of that reset pulse on the C16 specifically.

Anyone got any ideas?
