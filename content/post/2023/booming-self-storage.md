---
title: "The trap of booming self-storage"
date: "2023-06-17T08:46:17+10:00"
abstract: "An article in the Economist got me thinking about this business model again."
year: "2023"
category: Thoughts
tag:
- decluttering
- junk
- self-storage
location: Sydney
---
The Economist ran a surprising story that self-storage has outperformed other real estate in the United States in the past few years. They cite pressures faced from working at home during Covid, and the necessity of converting small spaces into home offices. Stuff, as they point out, has to go somewhere!

[Why self storage is turning into hot property](https://www.economist.com/business/2023/06/15/why-self-storage-is-turning-into-hot-property)

This is another effect of the pandemic we'll be feeling for years. People bought and rented spaces assuming they'd be living and resting there, not working or coordinating business from them. It's easier for people like Clara and I, but I can't imagine the stress of young parents trying to work in spaces with young kids, for example. Renters are screwed over in markets like Australia, but at least they're not tied to the layout of the house they spent years researching and getting a mortgage for.

Clearly we should have all invested in self-storage real estate, and used the profits to buy a place to live. But I digress. Wait, me!? Never.

Self-storage is the perfect business model. It's a tax levied on anxiety and indecision, both of which are abundant in modern society. Going through stuff is hard and emotionally draining, so in comes these businesses that can make that problem disappear in a puff of magic floorspace... for a monthly fee. You get an effective extension of your home, which you can now fill with more stuff. Then, when it comes time to renegotiate terms, its easier to pay the increase than think about it. Few businesses have such a captive client base.

I kid about the stuff Clara and I have bought for hobbies, but the tedious part I don't discuss is the *umms* and *aaahs* that come from getting rid of other stuff to make room for it. I refuse to fall into the self-storage trap, because I know I'll be tricked into thinking I have more space than I really do. Having everything here forces me to confront it and make decisions.

The only way it's backfired thus far has been buying things for their smaller size, despite being functionally less useful. Desoldering stations, for example. But one day I'll have space when our massive home sever is scaled down for smaller-sized SSDs, r-right?
