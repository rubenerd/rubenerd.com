---
title: "What detractors get wrong about induced demand"
date: "2023-05-28T08:50:19+10:00"
abstract: "Cars are great, traffic is liquid, and induced demand is only a negative."
year: "2023"
category: Travel
tag:
- economics
- environment
- traffic
- urbanism
- urban-design
location: Sydney
---
I'm heartened to see all the interest in public transport and urban design lately. These discussions have shifted the public debate away from arguing whether car dependency exists, to what alternatives we want for urban spaces. It also crosses political lines, from progressives who want more equitable, healthy, and environmentally friendly alternatives, to fiscal conservatives who demonstrate that car-centric infrastructure is financially unsustainable, to humans who want to live in a functional biosphere.

In other words, detractors don't have much room to wriggle any more. Though that doesn't stop them from trying!

One approach I've seen gain popularity is pushing back against *induced demand*, or the "build it and they'll come" philosophy. Fellow Australian creator *Economics Explained* isn't the only channel that regularly gets this wrong, though they're among the more influential.

[Economics Explained on induced demand](https://www.youtube.com/watch?v=zOYLiTj4vag)   
[Alan Fisher: Economics Explained is mislead about induced demand](https://www.youtube.com/watch?v=oDGNNxY56k0)

His video veers all over the stroad, but I think he makes three key mistakes: cars are great; traffic is liquid; and induced demand is only a negative.


### Cars are great

The *cars go vroooom vroooom* hobby defence is entirely reasonable from a personal perspective. I collect retrocomputers, FreeBSD memorabilia, and cute anime figures, and my dad loves cars and motorbikes. But just because I love computers, doesn't mean I enjoy being woken up by a monitoring alert at 03:00. I'm also not doing my day jobs on a C128.

Likewise, most motorists aren't driving for leisure. It sucks that so many people don't have a choice but to to spend thousands on a metal commuting box. It's a tax imposed by poor urban design, and it disproportionally affects lower-income earners who can't afford to live above a station or near work. People should be a lot angrier about being sold up a creek here, but suburbanites have been duped by marketing to think their car is a symbol of freedom. It’s hard to see it this way when owning one is basically mandatory, and its use is required to perform essential tasks.

Car infrastructure is expensive to build, represents a huge maintenance liability, is heavily subsidised by people who don't drive, and wastes an obscene amount of land. Emergency services, deliveries, vehicles used by the disabled, and public transport like buses and trams are also negatively impacted when *everyone* has to drive on that limited space.

Is it any wonder why car advertisements always feature empty, windswept coast lines? Being in a car is awesome (or so I'm told). Being around lots of people in their cars sucks. Motoring enthusiasts should be the first to want more of us on trains.


### Traffic is liquid

A common refrain on these videos is that induced demand is a blinkered view of traffic, because adding more highway lanes pours cars out from other roads. This is pitched as desirable, because you're funnelling traffic away from sensitive areas like residential streets.

This mental gymnastics isn't half as entertaining as the real thing (I would know, I did it and still watch it sometimes), and would be dangerous to do in a car. Or a train, really.

The fundamental mistake is assuming that traffic is a fixed, amorphous blob of fumes and metal that can be "reassigned". Even ignoring that traffic grows with an expanding population, building more road infrastructure *incentivises* people to use it. Directly, and via development of more unsustainable suburban sprawl.

Alan Fischer makes the point in his rebuttal video (linked above) that wider highways can't do anything to relieve inner-urban bottlenecks either, which would still be true even if traffic was a liquid. Or as my dad used to say, *freeways get you to the traffic jam faster.* You can move cars around to an extent, but eventually people largely need to go to the same places.

I also resent the idea that these roads are improving cities by taking traffic away from sensitive places like residential streets. Often times road widening displaces people, and once again its poorer residents who cop it the hardest. Highways are awful to live near, and represent a legitimate opportunity cost to sustainable urban transport.

Traffic isn't a liquid; I liken it to vapour. A rank vapour that smells of exhaust and rubber dust.


### Induced demand is only a negative

Only seeing induced demand as a negative belies where some of the misunderstanding comes from. We induce demand by building things, but we can use this to our advantage.

As mentioned above, we can build more public transit, accessible infrastructure for bikes and walking, and encourage mixed, medium-density development around transport. Inducing demand for more roads means more cars, more sprawl, and more wasted money.

Pretending induced demand for roads is a lie doesn't make the problem go away, just as adding two more lanes doesn't. And again, road users like delivery truck drivers, emergency services, and even motoring enthusiasts like *Economics Explained* are negatively impacted by this.

We need to have a serious think about how we want people to get around. We should be inducing demand for healthier, more sustainable, and more environmentally friendly options. For the health of our planet and our finances, we don't have a choice.
