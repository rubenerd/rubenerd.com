---
title: "The ESS AudioDrive ES1868F for sound and IDE"
date: "2023-04-01T13:43:46+11:00"
abstract: "I’ve never run IDE off a sound card before!"
thumb: "https://rubenerd.com/files/2023/ess-cd-ide@1x.jpg"
year: "2023"
category: Hardware
tag:
- am386
- retrocomputing
location: Sydney
---
In my continuing series on building a functional computer from a [repaired Am386SX motherboard](https://rubenerd.com/tag/am386/), today we're looking at getting sound working; and for bonus points, maybe even another IDE controller for free.

The ISA ESS AudioDrive ES1868F was the first expansion card I bought as a kid. Our first family machine had Sound Blaster cards for which I'm quite nostalgic, but the ESS remains my favourite. It has Sound Blaster 2.0 compatibility, sounds clear without any background hiss, and has the easiest drivers of any non-Plug and Play cards I've used.

<figure><img src="https://rubenerd.com/files/2023/ess-audiodrive@1x.jpg" alt="Top-down view of the highly-integrated ES1868 ISA card." style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2023/ess-audiodrive@2x.jpg 2x" /></figure>

As you can see on the left, the card also sports a secondary IDE controller. These were common at the time; early cards came with proprietary connectors for various optical drives, but thankfully they standardised. I've never had a use for them before, but given this Am386 board has no on-board IDE, and the [Multi-IO card](https://rubenerd.com/my-386sxs-working-acer-mio-400-io-card/) I'm using only has one IDE channel, I figured it was worth trying to offload the CD-ROM to it.


### Configuring sound

But I'm getting ahead of myself! To get sound working, I [downloaded the drivers](https://archive.org/details/soundcarddriversESSAudiodrive), extracted to a disk, then run `INSTALL.EXE`. This wrote the requisite driver lines to `AUTOEXEC.BAT` and `CONFIG.SYS`.

As I don't have a fixed hard drive yet, I had to run `SUBST` to map the disk drive to C, so the installer would work:

    A:\>SUBST C: .

To write the `BLASTER` variable for `AUTOEXEC.BAT`, I ran `ESSCFG.EXE` which printed the Base I/O address (`A`), the IRQ channel (`I`), the DMA channel (`D`), and the MIDI MPU-401 address (`P`). According to the [DOS Fandom Wiki](https://dos.fandom.com/wiki/BLASTER_Variable), the type variable (`T`) should be 3, as the ESS cards emulate Sound Blaster 2:

    :: AUTOEXEC.BAT Sound Blaster config
    SET BLASTER=A220 I10 D0 T3 P300

I rebooted, and a quick test with Apogee's *Word Rescue* played the MIDI music perfectly!


### Configuring IDE

This was tricker, because I'd never done it before. Before you can use your DOS CD-ROM drivers, you need to load a driver for the sound card's IDE controller. `ESSCFG.EXE` showed the controller was disabled:

    ESSCFG ( Version V4.3 ) found ES1868 at address 220.
    ...
    IDE CD-ROM controller is disabled.
      
    Select function:
        ...
        6) Change IDE CD-ROM I/O Address
        7) Change IDE CD-ROM IRQ Channel
        8) Display current settings
        Press ESC to quit

I pressed `6`, and chose `168` as an I/O address. It's the most common I've seen for secondary IDE controllers, and is often the default for many DOS CD-ROM drivers.

Next I pressed `7`, and chose `11` as the IRQ. IRQ assignments were always a black art, and it was common to have to choose different ones until stuff worked. If an IRQ doesn't work, try another one!

Pressing `ESC` to quit wrote the required line to `CONFIG.SYS`. I rebooted the machine, and was happy to see it reflected:

    ESS IDE CD-ROM controller ( ES1868 ) enable program V4.3
    Copyright (C) ESS Technology, Inc 1995-1996


### Loading the CD-ROM drivers

Now that we had IDE, I opened `CONFIG.SYS`, and below the ESS line I added `VIDE-CDD.SYS`, my [favourite CD-ROM driver](https://archive.org/details/dosdrivers)\:

    ;; CONFIG.SYS
    DEVICE=C:\AUDIODRV\ES1868.COM /A:168 /I:B
    DEVICE=C:\DRIVERS\VIDE-CDD.SYS /D:RUBENCD /P:168,11

This didn't work at first, but according to the docs bundled with the driver, we can pass the I/O address and IRQ of the ESS IDE controller with the `/P` option. This worked! Note that the `ES1878.COM` driver has the IRQ set in hex instead of decimal (B == 11).

Now I could open `AUTOEXEC.BAT` and add `SHSUCDX.COM` you can [download here](http://adoxa.altervista.org/shsucdx/), with the same device name as my CD-ROM driver. I also appended `D` to the variable so it would get that drive letter.

    :: AUTOEXEC.BAT
    C:\DRIVERS\SHSUCDX.COM /D:RUBENCD

I rebooted the machine, and it worked! The ISA CD-ROM controller was detected, the CD-ROM driver was able to detect the drive connected to it, and I was able to browse CDs.

<figure><img src="https://rubenerd.com/files/2023/ess-cd-ide@1x.jpg" alt="Photo showing the optical drive, the ESS IDE CD-ROM controller loaded, and the VIDE-CDD driver loaded." style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2023/ess-cd-ide@2x.jpg 2x" /></figure>

This might not be the most performant setup, but I'm chuffed that I was able to get this working for the first time. It also saved my precious two other IDE channels for use with hard drives, or more likely CF cards.

If you have a limited number of IDE channels in your old PC, give your sound card a try. It might give you another option.
