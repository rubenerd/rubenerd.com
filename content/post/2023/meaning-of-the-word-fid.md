---
title: "Meaning of the word “fid”"
date: "2023-07-18T17:24:11+10:00"
abstract: "A wooden or metal bar or pin, used to support or steady anything."
year: "2023"
category: Thoughts
tag:
- english
- language
location: Sydney
---
I was typing the abbreviation *fwd* in a document, and typo'd the word *fid* instead. It didn't autocorrect me, so I was intrigued about what it meant.

Wordnet didn't have anything:

    $ wn fid
    ==> No information available for noun fid

[According to Wiktionary](https://en.wiktionary.org/wiki/fid#Noun)

> fid (nautical): pointed tool without any sharp edges, used in weaving or knotwork to tighten and form up weaves or complex knots; used in sailing ships to open the strands of a rope before splicing. Compare marlinespike.

Definition five really caught my eye:

> A wooden or metal bar or pin, used to support or steady anything.

I'm going to call my next helper script a *fid*.
