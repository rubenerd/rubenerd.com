---
title: "Delayed gratification and fear"
date: "2023-12-10T12:33:08+11:00"
abstract: "Fearing an event for which I have no control has helped me schedule things for which I do."
year: "2023"
category: Thoughts
tag:
- books
- psychology
location: Sydney
---
One of the poignant observations James Clear makes in *Atomic Habits* is that we crave instant gratification, but we live in a delayed gratification society. An ice cream or expensive phone feels great now, but health and savings reward us in the long term. We need our lizard brains to stay alive, but sometimes we need to exert willpower to keep it in check.

James didn't mention it in the book, but I've noticed I've traditionally tackled anxiety in the same way. When there's an event with a timetime for which I have control, I crave the instant fix of distracting myself with an unrelated task, or other delaying tactics. Pushing it out only compounds the worry. I know this, but as I've come to say here many times this year: *emotions aren't rational!*

Conversely, I can't stand the anticipation of a pending scheduled event for which I'm nervous, because I know I'll be *catastrophising* and worrying about it the whole time, amping myself up for fear and failure. I find myself wishing I could bring the event forward *to get it over with*.

It only occurred to me recently that these two forms of anxiety are polar opposites! Which means I can use one to neutralise the other.

When I'm scheduling an event I'm nervous about, I feel those pangs of wanting to delay it. In that case, I remind myself how dreadful I feel when I can't bring something forward. It sounds ridiculous, but it's helped me short circuit that negativity, and *just do it*.

Does any of that make sense? Probably not. But it's helped me a lot.
