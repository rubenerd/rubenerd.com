---
title: "Starflight III Minstrels"
date: "2023-09-22T23:39:07+10:00"
abstract: "“Time, it seems, doesn't flow.”"
year: "2023"
category: Thoughts
tag:
- quotes
location: Sydney
---
[Via Wikia](https://starflight3.fandom.com/wiki/Minstrels)

> Time, it seems, doesn't flow. For some it's fast, for others slow. In what to one race is no time at all, another race can rise and fall.
