---
title: "Transit is about priorities, not just funding"
date: "2023-12-19T08:07:39+11:00"
abstract: "Discussing a video about Boston’s public transport that didn’t mention the Big Dig."
year: "2023"
category: Thoughts
tag:
- boston
- cars
- public-transport
- trains
- united-states
location: Sydney
---
Wendover Productions recently did a video [discussing the challenges facing Boston's MBTA 📺](https://www.youtube.com/watch?v=nDXsVhFG7TE), the state transit authority that oversees its subway system, among others. I have friends in Boston who complain about its system regularly, but I had no idea it had got that bad.

The video touches on a few key reasons for the system's current problems, from its winding design built prior to modern engineering standards, old rolling stock, an over-reliance on commuters which collapsed when COVID hit, its radial topology without interconnections, its lack of representation among planners and politicians, and how wealthier Bostonians tend to use the commuter rail system out to the suburbs (that's foreshadowing). It's a similar story in other old transit systems.

I appreciated the video's attention to the core point of transit, which was summarised towards the end:

> In the US, transit systems are pitches as alternatives to cars, rather than creators of opportunity [...] physical mobility represents economic mobility.

It also touched on investment, at around 12:00 and 14:00:

> Boston even understands [the ring concept] on the transit level too, as it once had unfunded plans to develop a ring-rail system, but eventually it couldn't even get funding to [even] develop the **bus** routes that comprised phase one of that plan.
>
> The MBTA doesn't come close to paying all of its expenses through fare-revenue [...] even before COVID, it was in a dire financial situation.

How did it get to this point? The video touches on decades of underinvestment and deferred maintenance, but I was surprised that the *Big Dig* nay warranted a mention. I tend to find large omissions like this in most Wendover videos (I'm sorry to say), but this one stuck out like a hulking green overpass.

Everyone has heard of the *Big Dig*, the multi-decade project to replace Boston's hated inner central artery with a complex system of tunnels and bridges. We studied it in classes, and I've watched more than a few documentaries about it. Most tend to focus on the myriad of technical and financial challenges it faced, but [Alan Fisher's summarised the real problem 📺](https://www.youtube.com/watch?v=d5pPKfzzL54 "Why Burying your Highways Underground Doesn't Work") with the project:

> The reason the Big Dig failed was ideological [...] it never "fixed traffic". 
>
> It went so over-budget the Massachusetts [Department of Transportation] quickly realised they had to do something with another agency to save their asses, and that agency was unfortunately the MBTA [images of broken-down trains].
>
> They didn't get to this point overnight. Rewind the clock, and you learn that from the Big Dig, Massdot basically forced the MBTA to take some of the debt of the project. They also told the MBTA to redirect their funds from maintenance to instead build new stations on the commuter rail network. The did this for the commuters who'd ultimately get stuck in the new I-93 tunnel.

His comment about debt [was simplistic](https://www.bostonmagazine.com/news/2012/02/29/mbta-big-dig-debt/ "Boston Magazine: What Exactly is the MBTA's 'Big Dig Debt'?"), but his broader point stands. Ideology informs the construction of a massive freeway system at the expense of other construction works. Say, for expanded public transit. It's a massive opportunity cost Bostonians will be paying out for decades.

This is important context to understanding how Boston's transit system got into this current predicament, and how it can be solved in the future. It's a cautionary tale in how building ruinously expensive roads don't solve traffic, and how other civil engineering works are impacted by such poor decisions. Boston's transit system didn't get to this point in isolation. 

Sydney has [just learned this lesson](https://www.theguardian.com/australia-news/2023/dec/02/a-tsunami-of-traffic-chaos-the-new-sydney-motorway-prompting-calls-for-a-royal-commission "‘A tsunami of traffic chaos’: the new Sydney motorway prompting calls for a royal commission: Peter Hannam") the hard way as well, though at least they're *also* building new rail.
