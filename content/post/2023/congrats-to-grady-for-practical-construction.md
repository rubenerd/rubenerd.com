---
title: "Congrats to Grady for Practical Construction!"
date: "2023-09-26T12:32:40+10:00"
abstract: "His spinoff series about construction is gorgeous."
thumb: "https://rubenerd.com/files/2023/yt-PdcXkmvXXwU@1x.jpg"
year: "2023"
category: Media
tag:
- engineering
- videos
- youtube
location: Sydney
---
Grady Hillhouse produces *Practical Engineering*, one of my favourite engineering channels. I love seeing his garage demonstrations of civil engineering concepts, and his enthusiasm for everything he discusses is infectious.

[Practical Engineering website](https://practical.engineering/about])   
[Nebula channel](https://nebula.tv/practical-engineering)   
[YouTube channel](https://www.youtube.com/channel/UCMOqf8ab-42UUQIdVoKwjlQ)

Recently Grady started a spin-off called *Practical Construction*, which he inaugurated with a series of videos showing the construction of a sewer pump station. The production values are gorgeous.

<figure><p><a target=_BLANK href="https://www.youtube.com/watch?v=PdcXkmvXXwU" title="Play HEAVY CONSTRUCTION of a Sewage Pump Station - Ep 1"><img src="https://rubenerd.com/files/2023/yt-PdcXkmvXXwU@1x.jpg" srcset="https://rubenerd.com/files/2023/yt-PdcXkmvXXwU@1x.jpg 1x, https://rubenerd.com/files/2023/yt-PdcXkmvXXwU@2x.jpg 2x" alt="Play HEAVY CONSTRUCTION of a Sewage Pump Station - Ep 1" style="width:500px;height:281px;" /></a><br /><a target=_BLANK href="https://www.youtube.com/watch?v=vFAaXef49U0" title="Play HEAVY CONSTRUCTION of a Sewage Pump Station - Ep 2"><img src="https://rubenerd.com/files/2023/yt-vFAaXef49U0@1x.jpg" srcset="https://rubenerd.com/files/2023/yt-vFAaXef49U0@1x.jpg 1x, https://rubenerd.com/files/2023/yt-vFAaXef49U0@2x.jpg 2x" alt="Play HEAVY CONSTRUCTION of a Sewage Pump Station - Ep 2" style="width:500px;height:281px;" /></a></p></figure>

If you're a fan of those old school long-form videos that explain how something was built, do check these out.

