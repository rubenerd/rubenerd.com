---
title: "What counts as authentic retrocomputing"
date: "2023-10-14T17:46:14+11:00"
abstract: "Every hobby has its gatekeepers."
year: "2023"
category: Hardware
tag:
- retrocomputing
location: Sydney
---
*My post last week about [authentic foods](https://rubenerd.com/drawing-the-line-for-authenticity/) was supposed to only be an intro to this post, but it ended up taking a life of its own. It's not necessary to read that one first, but maybe it might hint at where I approach this too.*

There's an interesting&mdash;and at times, frustrating&mdash;debate right now over what constitutes authentic retrocomputing. Where do you draw the line between what's real or some modern abomonation? I've found myself falling afoul of certain people's exceptions a few times, and I think it's worth exploring why.

Like food, there seems to be a gradient:

1. Purists who demand **stock configuration** to be authentic. I dub these people *moneybags*, because you'd have to be to only buy functional, unaltered hardware. I also don't see this as a tenable position long term.

2. Those who claim replacements, upgrades, and expansions are permitted, but only from **period-correct hardware** or new-old stock. Like *moneybags* above, this will only get more expensive.

3. Those who accept modern reproductions for certain hardware replacements, as long as they're **faithful to the original designs**, schematics, and drivers. These people accept there's a finite and diminishing amount of old hardware, but want to maintain the system as stock as possible.

This is where the first major schism starts.

4. Those willing to accept drop-in replacements that are **functionally equivalent**, but operate differently from original components. These include modern ICs or even full FPGA re-implementations. The latter especially causes consternation, with detractors arguing you're introducing chips more powerful than the original, but for a secondary role. They're probably against maths coprocessors, too.

5. People who use vintage machines, but replace or augment entire components with **modern conveniences**. These include SD card readers, Wi-Fi NICs, and modern graphics output in lieu of old or broken RF cans. This is where I've seemed to land.

6. People who board the **Ship of Thesus** and build entirely new machines, arguing they'd be no different than a classic machine that's slowly been replaced over time. Think reproduction motherboards, modern fabs for CPUs, and only falling back on old parts when a new one isn't available.

There another great schism after this.

7. Entire classic hardware reproductions **on single chips**. Some people get *really* precious about this idea, going as far as to even call machines like the Apple IIgs a fake Apple II, despite being released by the original first-party manufacturer.

8. **Emulators on modern machines**, such as VICE for the Commodore family of machines, or 86Box.

I tend to think *any* of these count!

I think we'd all agree that classic hardware would be more fun if you have the money and space, but someone learning 6502 assembler in an emulator on a modern machine is still performing an art that people in the 1980s did. It's also the worst kept secret in retrocomputing that most modern classic games are written on contemporary hardware, just as people weren't necessarily developing software for these systems *on those systems* at the time.

I guess retrocomputing has its gatekeepers, same as any other niche technical hobby or interest.
