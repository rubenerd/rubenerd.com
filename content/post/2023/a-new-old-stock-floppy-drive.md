---
title: "A new old-stock floppy drive"
date: "2023-05-26T14:01:21+10:00"
abstract: "It’s beautiful! And works great in my 386."
thumb: "https://rubenerd.com/files/2023/panasonic-drive@1x.jpg"
year: "2023"
category: Hardware
tag:
- am386
- floppy-disks
- retrocomputing
location: Sydney
---
This will be a quick post in my Am386 board adventures today, but I received my new old stock Panasonic 3.5-inch high density floppy drive today this week, and it's beautiful!

[View all posts in the Am386 series](https://rubenerd.com/tag/am386/)

There's something weirdly humbling about unwrapping something that sat in a box undisturbed for two decades. You get used to receiving old computer parts that smell of mildew, or have scuff marks or rust you need to scrub off. Getting something old that's also *immaculate* is highly excellent. If only we all looked as good as this age. 

<figure><p><img src="https://rubenerd.com/files/2023/panasonic-drive@1x.jpg" alt="Photo of the beige Panasonic disk drive." style="width:500px;" srcset="https://rubenerd.com/files/2023/panasonic-drive@2x.jpg 2x" /></p></figure>

I've had Mitsumi, Sony, Mitsubishi, and Samsung 3.5-inch drives growing up, but this is my first Panasonic that isn't a 5.25-inch unit. One little Easter Egg that doesn't come up well on camera is that the eject button is *glossly!* Maybe they figured the matt beige plastic doesn't stand up to the rigours of regular pressing, so they buffed it to a shine to make it look deliberate.

Regarding this Am386 machine, my original plan was to hook up the Gotek floppy emulator as drive A, and use my TEAC 5.25 360 KiB drive as drive B, but the upcoming case for this machine won't have space. I've standardised on 5.25-inch drives with my Commodore hardware, so it makes sense to save the 3.5-inch drives for PCs.

I still hold out hope for an extended density (ED) drive that can read 2.88 MiB disks one day, but I'd also like to eat, save, and pay rent.
