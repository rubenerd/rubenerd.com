---
title: "Indefinite detention ruled unlawful in Australia"
date: "2023-11-10T08:41:11+11:00"
abstract: "Always watch how politicians treat refugees, because it’s how they’d treat us."
year: "2023"
category: Ethics
tag:
- australia
- news
location: Sydney
---
[As reported by Paul Karp](https://www.theguardian.com/australia-news/2023/nov/08/australia-high-court-indefinite-detention-ruling-government)\:

> On Wednesday the chief justice, Stephen Gageler, said that “at least a majority” of the justices agreed that sections of the Migration Act which had been interpreted to authorise indefinite detention were beyond legislative power.

Awesome news! Australia violated international law under the 1951 UN Refugee Convention, and the 1967 Protocol, for decades. This (hopefully) closes another dark chapter in our history.

As Neal Ascherson wrote, we should watch how politicians treat refugees, because it's how they'd treat you and I if they could get away with it.
