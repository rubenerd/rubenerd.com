---
title: "Happy 30th birthday to FreeBSD!"
date: "2023-06-15T08:38:10+10:00"
abstract: "Including a bit of personal history about everyone’s favourite OS."
thumb: "https://rubenerd.com/files/2020/beastie@2x.png"
year: "2023"
category: Software
tag:
- bsd
- freebsd
location: Sydney
---
FreeBSD recently turned thirty, and it continues to grow from strength to strength.

FreeBSD is an open source Unix-like operating system with a unique toolchain, robust reliability, methodical development process, libre licencing, and cute mascot. It traces it lineage back to AT&T UNIX via the original Berkeley System Distribution, and was formally forked from 386BSD in, *drumroll please*... 1993.

*(There's still a part of me that thinks 1993 must have only been twenty years ago... surely. FreeBSD being 30 means I must be in my thirties too, which can't be true. R-right?)*

<p><img src="https://rubenerd.com/files/2020/beastie@1x.png" style="width:128px; float:right; margin:0 0 1em 1em;" srcset="https://rubenerd.com/files/2020/beastie@2x.png 2x" /></p>

I first ran FreeBSD on an iBook G3, upon discovering that much of Mac OS X's userland was derived from it. A box set of Red Hat Linux was my first Unix-like experience, and there were enough familiar commands to make me feel at home. When I went back to building x86 machines, and later cloud servers, I brought FreeBSD back with me under the tried and true engineering principle of *if it ain't broke, don't fix it!*

FreeBSD has opened so many doors for me in my career, and in my personal life. The OS has made me a better system administrator (even for penguins), and taught me more about UNIX than I ever learned at university. I've been humbled meeting so many of the people involved in its creation at various BSD events over the years.

FreeBSD has also kept computing fun. I've reached the stage where general software announcements fill me as much with trepidation as interest. What have they changed for ill defined or dubious reasons? What new hotness throws out lessons from the past? Who does this feature really serve: me, a big corporate, or an advertiser? FreeBSD is that dependable friend who has my back, even when I feel the industry loses the plot sometimes. I can't stress how important this is.

I use all the usual suspects for my **$DAYJOBS**, though I continue to find uses for FreeBSD and advocate for others to do the same. I also maintain a FreeBSD VM template at work, and may subtly suggest Linux users give it a try at every available opportunity.

Thank you to everyone involved in the project!

[FreeBSD website](https://www.freebsd.org/)   
[FreeBSD wiki](https://wiki.freebsd.org/)   
[FreeBSD Foundation](https://freebsdfoundation.org/)   
[Donate to FreeBSD](https://freebsdfoundation.org/donate/)
