---
title: "Fight or flight mode in your computer chair"
date: "2023-07-16T09:26:31+10:00"
abstract: "My desk has to face into the room, or the fight-or-flight response is activated."
year: "2023"
category: Thoughts
tag:
- living
- productivity
- remote-work
location: Sydney
---
If your job entails sitting or standing at a computer all day, or if you have a home office, how do you orient yourself? Assuming you have a choice.

I've never liked having my desk against a wall, as so much furniture is designed to be. It makes optimum use of limited space, but it means my view away from the computer monitor is of a wall. If you live in a rental, that's probably going to be a plain white wall devoid of artwork. It's... grim!

But there's a far bigger reason, though it took me until I was in my twenties to appreciate why. Having my back turned to the room makes me *deeply* anxious. It doesn't matter if I'm there by myself, or in a crowded office, the same thing happens. The closest way I can describe it is the feeling of a sniper's laser sight on the back of my head, or someone hovering over me about to grab my shoulders. I can even feel my resting heart rate is higher.

Not to get all Malcolm Gladwell on you, but *turns out* this is a well-understood phenomena. When certain people have their backs turned, it triggers a natural fight-or-flight response that manifests as anxiety or fear, depending on the person. Aside from not feeling especially nice, it's also incredibly disruptive, and not conducive to any deep thought. 

*(This is why I'm so unproductive in open-plan offices, but that's for another post).*

The solution I've found is to rotate the desk such that I'm sitting on the other side, facing *into* the room instead of away from it. The wall is behind me, and in the best circumstances I can see the door. I don't place much stock in the claims of philosophies like Fung Shui, but this is one thing they absolutely get right.

What's curious is that the wall doesn't even need to be opaque. My favourite coffee shop in Adelaide had a huge mirror behind my favourite table, and my local coffee shop in Sydney has a window. There's no hiding what I'm doing on my laptop with these, and in the case of a window, it'd be pretty easy for someone to smash through and "take me out".

This proves to me just how irrational this anxiety is! But like all emotional states, logical inconsistency doesn't make them any less real. I can be in the plushest, most relaxing airport lounge in the world on the company card, but if my back is facing the buffet, I'm going to be a nervous wreck when I make the flight.

I'm lucky that Clara is so accommodating of this. I'm sure we'd have twice the space in our loungeroom if I didn't insist on our computer tables being in the middle of the room. I'd probably fit more retrocomputers and FreeBSD servers in too.
