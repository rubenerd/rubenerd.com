---
title: "A disk-on-module with my Am386"
date: "2023-04-11T08:38:33+10:00"
abstract: "DOS can use it, but the BIOS doesn’t like it, and it conflicts with my sound card. But progress!"
thumb: "https://rubenerd.com/files/2023/dom-installed@1x.jpg"
year: "2023"
category: Hardware
tag:
- am386
- retrocomputing
- storage
location: Sydney
---
In my continuing series about this 1991 Am386 motherboard I got working, over the long weekend I got around to tinkering with a 512 MiB IDE disk-on-module (DOM) storage device. DOMs are small, solid-state drives that connect directly to the IDE bus, negating the need for cables, adaptors, or converters.

This is my first time using a DOM, because they've never been the most cost-effective solution per MiB. But I'm already in love with the form factor, and when you're dealing with an ancient machine that only works with lower-capacity devices, why not give it a try?

Join me if you'd like as I walk through another "live blog" attempting to get this thing working. Spoiler: DOS is fine, but the BIOS isn't!

[List of all the Am386 posts thus far](https://rubenerd.com/tag/am386/)  

<figure><p><img src="https://rubenerd.com/files/2023/dom-installed@1x.jpg" alt="Photo showing the DOM installed directly onto the ISA Multi-IO card" style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2023/dom-installed@2x.jpg 2x" /></p></figure>


### Configuring the BIOS for the DOM

As designed, I was able to connect it directly to the IDE header of my ISA Multi-IO card, and its power cable to a spare Molex connector. I'd need to use an IDE ribbon cable and a gender changer if I ever wanted a secondary device on this bus, but for now this works great. The anime figure is optional, though encouraged.

I powered on the machine, then quickly realised I'd plugged it into the card backwards! **FSDFGSNJNGNG!** I turned it off, flipped it around, then crossed my fingers that I hadn't caused any damage. We're spoiled thesedays with keyed connectors; old MFM and IDE cables happily let you physically connect them in either orientation.

Booting the machine into the 1991 AMIBIOS, I realised it pre-dated drive auto-detection, which isn't surprising. I checked the handy table at the back of the manufacturer's documentation, and punched in the following: 

* **Type**: 47 (USER)
* **Cylinders**: 497
* **Sectors**: 63
* **Heads**: 32
* **WPcom**: 65535
* **LZone**: 65535

I completely forgot about the latter two settings, but a hardware guide below described them as *Write Pre-compensation* and *Landing Zone*, which were required on older MFA drives. The BIOS had some handy inline documentation when entering the settings, which told me to set both to 65535 if they weren't applicable for my drive.

[The Hardware Guide: BIOS](https://documentation.help/The-Hardware-Guide/bios.htm)  
[Acceed's DiskOnModule documentation](https://acceed.com/media/manual/manuals/dom-manual.pdf)


### IDE detection in the BIOS

I rebooted the machine, and the POST screen reported an **HDD controller failure** which wasn't encouraging. Worse, when MS-DOS 5.0 began booting off the floppy disk, the ESS AudioDrive IDE driver reported no card present. Maybe an IRQ conflict or something to check later, I thought.

Running `fdisk` after MS-DOS 5.0 booted, it reported **Error reading fixed disk**, and the blinking cursor had stopped. The machine had crashed enough to require a physical power cycle. Not a good start!

After turning the machine off, I unplugged the ISA sound card I'm also using as an IDE controller for the CD-ROM, and booted again. It showed the exact same behaviour, which was oddly reassuring given this is my favourite ever sound card. But what to do?

Taking a closer look at the DOM itself, I noticed it had a toggle switch showing S/M, which I *assume* tells the internal circuitry whether it's connected directly to the IDE bus (single) or sharing it with another device (multiple). Despite being the only device, I flicked it to M to see what would happen. Again, no difference.

The last thing I changed was to set the **WPcom** and **LZone** BIOS values from the recommended 65535 to 0 instead, on the advice of a forum post I've since lost the link for. It made no difference, so I reverted the change.


### Assessing potential issues

I went to deal with a family issue for a while, which also gave my subconscious a chance to chew on the problem. I thought about how `fdisk` reported an error reading the fixed disk. This suggested to me the drive was *detected*, just not usable. I remember before I installed the DOM and ran `fdisk`, it specifically reported no fixed drive present.

Something else I only realised was that manufacturer's documentation referred to the disk as an *LBA* device, and everything 256 MiB and below as *normal*. I'd bought the 512 MiB module because it was only a few dollars more than the smaller-capacity models, but this might not have been the best idea with hindsight.

Being in my mid-thirties, I'm also *just* old enough to remember CHS addressing for IDE drives! I knew for a fact that getting these wrong would cause all sorts of problems, but again, I was using the manufacturer's recommended settings. But was it worth trying different values?

[The Macarena, which 30-somethings also remember](https://www.youtube.com/watch?v=zJ2hqQWIx64)   
[Wikipedia article on cylinder-head-sector addressing](https://en.wikipedia.org/wiki/Cylinder-head-sector)


### Troubleshooting with other DOS and SpinRite

I'd been using my trusty, well-worn MS-DOS 5.0 boot disk until this point, but I tried booting PC DOS 2000 instead to try its version of `fdisk`. It reported the same **Error reading fixed disk** error, though curiously it didn't crash the machine this time. Rebooting back into MS-DOS 5.0, and `fdisk` crashed again. I didn't grow up on the (superior!) DR-DOS, nor do I have a boot disk of it handy, but I'd also be interested to see how it would fare. Insert mental note here to try in the future.

But here's where things started to get weird!

I had SpinRite 5.0 and 6.0 on my MS-DOS 5.0 disk, so for fun I decided to see what they'd do. Not only did 6.0 detect the DOM, and even print its serial numbers and technical features, it was able to read and verify every sector at L1, and even complete a benchmark test.

This was a huge relief, and lead me to think the issue wasn't hardware, but configuration somewhere. As an aside, I really need to get a VGA or CGA capture device at some point.

<figure><p><img src="https://rubenerd.com/files/2023/dom-spinrite@1x.jpg" alt="Photo showing SpinRite's initial config screen, with the DOM correctly identified." style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2023/dom-spinrite@2x.jpg 2x" /></p></figure>


### A working (albeit unbootable) DOM

I exited SpinRite and rebooted the machine... and this time the BIOS reported **C: drive failure** instead of **HDD controller failure**. I... wait, what? As far as I knew, SpinRite hadn't written anything permanent to the DOM, and I'd literally changed *nothing* else. Did Steve Gibson travel down to Australia and smack it with his Northgate OmniKey for me?

Booting into MS-DOS 5.0 again, and this time `fdisk` started. I was able to partition the drive, mark the primary DOS partition as active, reboot, and format it with the `/S` flag to copy system files. I could copy files across from the floppy drive and GOTEK unit on drive B to the DOM, and could run software on the DOM with surprising speed.

Unfortunately, the DOM remains unbootable. Presumably because of that aforementioned BIOS drive failure, I still need to boot off the floppy disk to then access drive C. It's not ideal, and might make installing other OSs I had in mind much more difficult, but I can work with it for now.

Just to see what would happen, I tried using `fdisk` to create smaller partitions on the DOM, and tried booting with or without smaller drive geometries in the BIOS. The few I tried made no difference, but I still suspect getting this right might be the key to getting it detected and working.

I might try doing a format with another DOS tool like Super Fdisk or SeaTools to limit the size of the drive next.


### Conclusion

Things are still a bit up in the air, but I've made progress. I can't use the ESS AudioDrive IDE controller for the CD-ROM while the DOM is using the IDE controller on the Multi-IO card, though I chalk that up to a resource conflict I might be able to work around. I also can't rule out having damaged it by plugging it in backwards, but at least I have a fixed disk to run software from.

I also can't overemphasise just how *fast* this DOM is, even on an ancient ISA IDE card. It outperforms the consumer and industrial CF cards on my Pentium tower, a computer with *ten times* the CPU clock speed and an onboard IDE controller. I thought I was going crazy with this Pentium, but this proves that machine definitely has other problems I need to work out.

I won't be touching this machine again for a few weeks, but at least I've taken two steps forward with the one step back :).
