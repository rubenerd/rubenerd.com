---
title: "How BlackBerry failed to respond to the iPhone"
date: "2023-09-30T08:27:48+10:00"
abstract: "Insiders talk about the challenge of shifting RIM’s internal culture."
year: "2023"
category: Hardware
tag:
- android
- blackberry
- iphone
- phones
location: Sydney
---
Dare Obasanjo read an article about the rise and fall of BlackBerry back in 2016, and highlighted one of the more interesting observations:

[Culture Eats Strategy for Breakfast](http://www.25hoursaday.com/weblog/2016/01/11/CultureEatsStrategyForBreakfast.aspx)

> In reading the article it is clear that at that the BlackBerry leadership decided that it was important for them strategically to address the rise of the iPhone. However as the unnamed insider makes clear above, their organizational culture was simply not set up to make the mental shift to view their product and needs customers differently after the iPhone launched.

<a href="http://www.theglobeandmail.com/report-on-business/the-inside-story-of-why-blackberry-is-failing/article14563602/">And from the original Globe and Mail article</a>  

> Once a fast-moving innovator that kept two steps ahead of the competition, RIM grew into a stumbling corporation, blinded by its own success and unable to replicate it.

How many times has this happened?

> Several years ago, it owned the smartphone world: Even U.S. President Barack Obama was a BlackBerry addict. But after new rivals redefined the market, RIM responded with a string of devices that were late to market, missed the mark with consumers, and opened dangerous fault lines across the organization.

It really is amazing how much good will they burned and failed to capitalise on, seemingly overnight. The iPhone is painted in history books and retrospectives as this grand, unifying device that came in, disrupted carriers, and gave people devices they always wanted. But people *loved* their BlackBerry handsets.

It does make me wonder if Android hadn't pivoted from a BlackBerry to iPhone clone, whether that form factor and design would have had a longer life. People still talk fondly about physical keyboards and pocketability. But I suppose as BlackBerry themselves learned, the world has moved on. My beloved Palm met the same fate too, though at least webOS was a decent attempt.

I'm realising now just how much I hate modern smartphones. They're big, heavy, breakable, glossy, distracting, and come with surveillance crap. I did think it'd take til I was in my forties before entering *get off my lawn* territory though... that's a bit scary.
