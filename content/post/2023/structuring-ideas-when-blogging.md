---
title: "Structuring ideas when blogging"
date: "2023-10-05T17:25:57+11:00"
abstract: "Blogs added structure to ideas, though we need not be bound so strictly to them."
year: "2023"
category: Internet
tag:
- blogging
- design
- history
- metadata
- nostalgia
- weblog
location: Sydney
---
The freeform web made it easier than ever for people to share their written ideas. Blogging added structure, which had two important benefits:

* It made sites easier to understand, navigate, and explore. Once you understand the rough layout and structure of one blog, you could grok others.

* They could be parsed by machines, and presented in other ways. CSS promised to separate presentation from content; in reality it was RSS that did it.

Social media kinda threw a wrench into both of these. People now mostly associate sharing ideas with the dank viewport of a social media app on their phone, or a narrow set of websites. Some of the dark patterns pioneered by these social networks have even seeped back into blogging, such as infinite scrolling. Others, like newsletter popups, were self-inflicted.

But I think one benefit has been the reimagining of how ideas are structured. I love using titles and metadata like tags, but social media proved that there's a use case for not having them, or having them be optional.

It's interesting to think how some of these ideas have circled back. If you wanted tags on your WordPress site in 2005, you probably used something like SimpleTags that would convert inline tags into clickable Technorati metadata, not too dissimilar to how hashtags work today. Many of my earliest posts still have these tags, even though the links are long-since dead.

[SimpleTags plugin](https://www.broobles.com/scripts/simpletags/)   
[A FreeBSD post from 2006 with SimpleTags](https://rubenerd.com/freebsd-61-error-on-parallels-3036/)

It makes me think what makes a blog today. It used to be that a calendar, a sidebar of links, and a friendly orange RSS button made a blog. I'll admit I miss that design, to the point where I'm half-tempted to retrocon an even earlier theme than the one I currently have. But none of that stuck around.

Another interesting example is the use of dates where other bloggers might put titles. This was more prevalent in the early days of blogging, perhaps because it more closely mimics a personal diary. But I'm seeing it come back, and I'm also intrigued.

I suppose some people also expect blogs to consist of *posts*, all of which have a satisfying conclusion. Does this sentence count as one?
