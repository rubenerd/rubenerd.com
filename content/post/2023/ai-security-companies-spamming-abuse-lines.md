---
title: "AI companies spamming abuse email addresses"
date: "2023-05-05T10:16:40+10:00"
abstract: "Another way "
year: "2023"
category: Internet
tag:
- ai
- economics
- externalities
- spam
location: Sydney
---
I work for a cloud infrastructure provider, and host sites for friends and non-profits. I investigate each and every report sent to our abuse lines; not just out of professional duty, but because I don't want our IP ranges or hostnames to be listed in spam databases.

A spate of new AI security companies are making this more difficult. I receive dozens of emails a week claiming to have found fraudulent material, such as phishing sites. I spend time checking each one, and they're always benign.

**That's a false-positive rate of 100%!**

Here are some other things that happen 100% of the time:

* I mention something pointless on a blog post
* I take a breath at least once a day

These range from trivial or critical. I'd consider the behaviour of these services somewhere in the realm of *counterproductive*. Worse than being additional, annoying work I didn't ask for, notification fatigue reduces the effectiveness of other alerts that we *do* need to pay attention to. They're a net negative to security.

Now I know what you're thinking: *Ruben, I don't receive these notices*, or *Ruben, I get them sometimes, and they're legitimate*. That's great, I hope it continues being so for you! Because they've been nothing but a *monumental* waste of my time.

This is another example of how AI companies externalise their costs. If you didn't take economics at uni, these sites explain the concept:

[Britannica](https://www.britannica.com/topic/negative-externality)   
[International Monetary Fund](https://www.imf.org/external/pubs/ft/fandd/2010/12/basics.htm)   
[Wikipedia](https://en.wikipedia.org/wiki/Externality)

For now, I have to live with it. I'm not going to put a blanket ban on security notices coming from domains ending in **ai**, because that would still be irresponsible. But they'll have to start proving their worth pretty soon.
