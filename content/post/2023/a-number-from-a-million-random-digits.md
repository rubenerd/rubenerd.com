---
title: "A number from A Million Random Digits"
date: "2023-06-30T22:04:26+10:00"
abstract: "1.049"
year: "2023"
category: Software
tag:
- a-million-random-digits
- random-numbers
location: Sydney
---
This comes from line 2,530 in the legendary book *A Million Random Digits, with 100,000 Normal Deviates*:

    1.049

Thank you.

[Download PDFs from the RAND Corporation](https://www.rand.org/pubs/monograph_reports/MR1418.html)

