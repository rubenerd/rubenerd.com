---
title: "Adrian Black takes apart a 1980s scope"
date: "2023-08-10T14:16:19+10:00"
abstract: "This isn’t only engineering, it’s art."
thumb: "https://rubenerd.com/files/2023/adrian-scope-1@1x.jpg"
year: "2023"
category: Hardware
tag:
- adrian-black
- art
- design
- science
- video
- youtube
location: Sydney
---
This was such a fun video! Today has been quite *meh*, but I couldn't stop grinning after hearing Adrian's audible excitement at uncovering the 1980s motherboard.

[Tektronix 2445: My first vintage oscilloscope repair attempt](https://www.youtube.com/watch?v=DX6ipBe3WGQ)

Check out the stunning internals of this scope. This is where engineering and science meet art.

<figure><p><img src="https://rubenerd.com/files/2023/adrian-scope-1@1x.jpg" alt="Screenshot showing the motherboard on the side of the unit." style="width:500px;" srcset="https://rubenerd.com/files/2023/adrian-scope-1@2x.jpg 2x" /><br /><img src="https://rubenerd.com/files/2023/adrian-scope-2@1x.jpg" alt="Screenshot showing the primary motherboard and large heatsinks on the bottom of the unit." style="width:500px;" srcset="https://rubenerd.com/files/2023/adrian-scope-2@2x.jpg 2x" /></p></figure>
