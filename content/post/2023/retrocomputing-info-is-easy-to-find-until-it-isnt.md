---
title: "Retrocomputing info is easy to find, until it isn’t"
date: "2023-05-03T21:30:30+10:00"
abstract: "I agree that it’s a great time for the hobby, but there are still challenges."
year: "2023"
category: Hardware
tag:
- retrocomputing
location: Sydney
---
It's a common refrain among bloggers and video creators that retrocomputing as a hobby has never been easier, thanks to the modern Internet. It's true to an extent, from the explosion in hobbyist hardware replacements, to scans of available documentation, to the sheer size of retrocomputing communities that can offer help.

But I'd caution anyone entering from thinking it'll be easy. Even leaving aside the increasing failure rate of old components, two of the biggest challenges remain documentation and drivers. These are less of an issue if you're using popular hardware with limited numbers of SKUs, like a Commodore 64 or an Apple IIc.  But if you have an ISA modem in a generic PC clone? Not so much.

We're spoiled on the modern web that we can ask a search engine for a specific error message or general problem, and we can usually find it. But I regularly search for error codes spat out of a Commodore 128 productivity application, or a DOS memory manager, or even a Windows 95 driver, and receive absolutely nothing. And because the original companies don't exist, you can't just go to their website to search.

*(I say that, but even sites run by Microsoft and HP are now being scrubbed of their old documentation and downloads).*

It's why I encourage anyone who's getting into the hobby to start a blog, podcast, or video channel! If you figure out how to fix something on your own, we need your expertise and experience.
