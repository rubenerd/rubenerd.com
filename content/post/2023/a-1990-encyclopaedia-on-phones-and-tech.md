---
title: "A 1990 encyclopædia on phones and packet switching"
date: "2023-03-07T09:48:32+11:00"
abstract: "Did you know there are 3 million mobile subscribers in the US alone!?"
year: "2023"
category: Hardware
tag:
- encyclopaedias
- history
- phones
- networking
- nostalgia
location: Sydney
---
IKEA tend to stock Swedish translations of popular titles on their bookshelf displays, but over the weekend Clara and I chanced upon a 1990 volume of the *Lexicon Universal Encyclopedia*. I'd never heard of it before, so of course I had to rifle through it for some goodies!

Below a handsome photo of the Motorola *Micro TAC* handset, we get to this section titled **Transmission and Switching Technology** on page 188:

> The rapid growth of mobile and portable radio telephone service was evidence of a strong demand for communication away from a fixed base. At present, there are about 3 million mobile subscribers in vehicles operating in 300 metropolitan areas in the United States. There also was rapid growth in the number of portal telephones that could operate away from a vehicle.

People using portable phones, *outside* their car!?

> The year 1989 was one of further development and commercial introduction of wide-band packet-switching technology, in which voice and data signals are converted to digital format and grouped into "packets", or short bursts of digital bits. These packets are marked with timing and routing data and sent together with interleaved packets from other customers over a high-speed transmission line. At the receiving end, the packets are separated out, decoded, and reconstructed into the original messages.

The page also goes into exquisite detail about this exciting new development called *undersea fibre-optic cables*, the first of which had only just been laid.

It's incredible to me how so much of the stuff we use and expect today is based on work sowed so recently.
