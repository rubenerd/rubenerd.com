---
title: "Avoiding coffee namespace collisions"
date: "2023-06-06T08:28:38+10:00"
abstract: "B for black, S for skim, and Y for… what?"
year: "2023"
category: Thoughts
tag:
- coffee
- databases
- normalisation
location: Sydney
---
Speaking of coffee shops, I noticed something else fun recently.

I was ordering takeaway for myself and a few others, like a gentleman. Two of them wanted a skim flat white, and another wanted soy. As the barista was writing the orders on the cups, she wrote **B** for me, **S** on two more, and **Y** on the other.

The meaning of these eluded me for a moment... **B** might be for Black coffee, and **S** was for either Soy or Skim, but what was **Y**? But then the caffeine hit, and it became as obvious to me as I'm sure it is to you reading this right now.

It got me thinking what other drinks might be. They probably abbreviated Soy to **Y** instead of Skim to **M**, because that'd be reserved for Mocha. The only coffee I could think of that *started* with Y was a *Yeeted* coffee, which I strongly doubt most coffee shops would be willing to sell for liability reasons.

I get a kick out of decoding and overthinking basic stuff like this. You can take the boy out of being a DBA, but you can't take the DBA out of the boy.

**DBA**... Doppio, Black, Affogato? Decaf, Biscotti, Americano? Wait, biscottis are what you *have* with coffee. A DBA coffee, served on a table. Don't drop it! Thank you, I'm here all week. Weak?
