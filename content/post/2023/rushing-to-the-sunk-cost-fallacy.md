---
title: "Rushing to the sunk cost station"
date: "2023-05-25T08:41:07+10:00"
abstract: "Figure out where we’re going first."
year: "2023"
category: Thoughts
tag:
- fallacies
- work
location: Sydney
---
This was one of the conversations I had with Jim Kloss years ago. It seems fitting to come back with it as a thought.

Years ago on a family holiday in Germany, I rushed to a station before my dad caught my arm and said "no, let's figure out where we're going first".

That line has stuck with me since. The compulsion to do something, *anything*, can cloud our judgement, especially when we're stressed or feeling pressured. Taking the time to understand what we want to do, and where we want to go, can be the difference between a successful project and falling into a heap. It's also why procedures and training are important.

IT engineers and managers seem especially vulnerable to this line of reactionary thinking. Worse, we often pair it with our predilection for sunk cost thinking, where we don't want to give up something for all the time and resources already spent. It's a fallacy because throwing good money (and time) after bad only exacerbates the issue, which perpetuates the cycle and renders it even more difficult to extricate oneself from.

I see this calamitous confluence of crap happen all the time (not one of my better alliterations). We rush back to a familiar pattern or project, as I did to that station, because we mistakenly believe movement&mdash;any movement&mdash;equals progress. Not only is that false and destructive, the opportunity cost means we're not using that time to figure out something better.

I first wrote that as *batter*, and now I want pancakes. But wait, I can't have pancakes, because there are the mouldy remnants of last week's waffles on the table. We spent so much time and money making them, we have to eat them!

