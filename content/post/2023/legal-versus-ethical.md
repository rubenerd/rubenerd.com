---
title: "Legal versus ethical"
date: "2023-03-06T08:15:37+11:00"
abstract: "A worrying number of IT people argue that it is, or don’t consider it."
year: "2023"
category: Internet
tag:
- ethics
location: Sydney
---
A worrying number of IT engineers and business folk don't consider the ethical impact of their actions, or only judge them on based on legality. I've [noticed this with AI training](https://bsd.network/@rubenerd/109963302955844685), but it's played out with [blockchains](https://rubenerd.com/cryptocurrency-waste/), online tracking, DRM, and... well, there's a lot!

Suffice to say, I'm unconvinced. The law isn't an ethical code, and we'd be in *deep* trouble if it was. This should be transparently obvious to anyone who spends more than a few seconds thinking about what was legal before.

Business ethics educator [Chris MacDonald put it like this](https://businessethicsblog.com/2011/12/22/whats-legal-isnt-always-ethical/)\:

> Anyone who tells you, or simply implies, that whatever is legal is also ethical is most likely indulging in self-serving rationalizations. When that idea comes up in the private sector, it’s likely that someone is trying to justify some profitable behaviour that is unethical but not-yet illegal.

If someone defends their actions as legal, ask them if they think it's right. Most will immediately obfuscate, go quiet, or start arguing in bad faith. I *dare* them to do better.
