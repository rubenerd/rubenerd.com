---
title: "Tower Records in Japan!"
date: "2023-04-14T09:57:37+09:00"
abstract: "They’re still being opened, and they’re amazing!"
thumb: "https://rubenerd.com/files/2023/japan2023-tower@1x.jpg"
year: "2023"
category: Travel
tag:
- japan2023
- music
location: Osaka
---
We're in Osaka again! We've been based out of Tokyo for conferences and other reasons the last few times we've been back to Japan, but the Kansai region is our favourite part of the country. It almost doesn't seem real that we've been able to come back, especially after these last few years of things that have effected everyone, and our family specifically.

There's a whole bunch to write about this amazing part of the world, but for now I have to talk about something specific and nostalgic that we experienced again today.

<figure><p><img src="https://rubenerd.com/files/2023/japan2023-tower@1x.jpg" alt="View outside the brand new Tower Records in Namba Parks, showing the familiar yellow and red logo." style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2023/japan2023-tower@2x.jpg 2x" /></p></figure>

I've joked before that Japan is where Western companies go to retire, but it's really true. And few places typify it as well as Tower Records, the American music chain and cultural icon. While it failed everywhere else, we got to explore a *brand new* store in Namba Parks. That's right, in 2023, there's enough of a market to justify one.

Seeing the *No Music No Life* merchandise and contemporary English music interspersed with K-Pop and Japanese virtual streamers... it was *uncanny.*

<figure><p><img src="https://rubenerd.com/files/2023/japan2023-towerinside@1x.jpg" alt="View inside later one evening, showing the rows of shelves and K-pop posters" style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2023/japan2023-towerinside@2x.jpg 2x" /></p></figure>

Music was an important part of my parents' lives, and they encouraged my sister and I to explore it as kids. Many happy memories were spent wandering around the sprawling Singapore branches of HMV and Tower Records, using those shelf-mounted CD players and headphones to preview the latest releases, and seeing all the posters and art. Both my parents were excited by my budding interest in jazz and blues, and were keen to help me find things I liked. It was a real bonding exercise, and one I treasure and miss as an adult.

Being able to do that, again, with Clara this time... it meant the world.

Japan, *amirite!?*

