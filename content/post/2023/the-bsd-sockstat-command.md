---
title: "The BSD sockstat(1) command"
date: "2023-02-14T07:48:47+11:00"
abstract: "Show open sockets and troubleshoot when things report accepting connections when they aren’t."
year: "2023"
category: Software
tag:
- bsd
- freebsd
- netbsd
location: Sydney
---
In today's installment of [things you already know, unless you don't](https://rubenerd.com/tag/things-you-already-know-unless-you-dont/), you can use **sockstat(1)** on the BSDs to list open sockets.

From the <a href="https://man.freebsd.org/cgi/man.cgi?query=sockstat">manpage(1)</a>, you can issue this command to show IPv4 sockets listening on port 22 using protocol TCP:

    $ sockstat -4 -l -P tcp -p 22

I can't tell you how many times this has saved me during a late-night troubleshooting session, when a service claims to be up but isn't.

