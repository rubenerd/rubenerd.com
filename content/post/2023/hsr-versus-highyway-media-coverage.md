---
title: "HSR versus highway media coverage"
date: "2023-05-05T17:18:34+10:00"
abstract: "A new interstate highway has higher costs, disruption, and environmental impact, but let’s lament high speed rail instead."
year: "2023"
category: Media
tag:
- australia
- california
- public-transport
- trains
- urbanism
- united-states
location: Sydney
---
Even as a train and public transport gentleman such as myself, I'm still duped by how the media presents (distorts?) coverage of infrastructure projects.

Case in point, Alan Fisher did a great video that caught my attention because it mentions California's High Speed Rail system. The HSR is mired in controversy, with much hand-wringing in the press over its utility and value. Yet a new interstate with *significantly* higher cost overruns, disruption, and environmental impact warrants nay a peep in the American media. Alan does a deep dive into why he thinks that is.

[Alan Fisher: California High Speed Rail is Fine; And the Wild Scrutiny of Transit Projects in the US](https://www.youtube.com/watch?v=PwNthD-LRTQ)

It's easy to dismiss this as *whataboutism*, but these are two projects in the same country with the same objectives that should be evaluated on the same metrics. The media also invite the comparison by being so lopsided in their criticism.

Australia handles this a *bit* better. Okay, maybe 5% better. Better enough that people are aware of the existence of maybe kinda sort of something else maybe. Demand-inducing motorways are met with opposition, and the press do report on the opportunity cost to public transit. But it gets built anyway, because that's what car-dependent cities do. Build more lanes though, that'll fix it!

[Sydney Morning Herald: NSW government understates true cost of WestConnex by billions](https://www.smh.com.au/national/nsw/nsw-government-understates-true-cost-of-westconnex-by-billions-20210617-p581yc.html)

You see this dichotomy everywhere. We're told we don't have the funds to raise unemployment benefits for people struggling to eat, yet nobody talks fiscal responsibility when it comes to tax cuts. Regardless of your politician persuasion, it comes down to what governments want to prioritise, and how the media frame the debate. They feed each other.

At least if it were a train, you'd could do more feeding in less space, with lower resource use, in a more sustainable way, and with a vastly reduced maintenance liability. Mmm, commuters.
