---
title: "I fixed my 1991 386SX-16/20CN motherboard!"
date: "2023-03-25T15:09:15+11:00"
abstract: "It POSTs! But not much else."
thumb: "https://rubenerd.com/files/2023/386sx-16-20cn@1x.jpg"
year: "2023"
category: Hardware
tag:
- am386
- challenger
- retrocomputing
- singapore
location: Sydney
---
This is a post about a beloved old motherboard, and my journey to get it working. I love seeing the process of people on YouTube doing this, so I thought I'd try something similar by "live blogging" the experience. It was a lot of fun :).


### How I got this board

In the early 2000s, the Challenger computer store in Singapore had an awesome clearance department in their flagship Funan branch. They sold everything you can imagine, including the odd museum piece. The *[auntie](http://www.mysmu.edu/faculty/jacklee/singlish_A.htm#auntie)* who worked the weekend counter was also one of the kindest people I've ever met; I wish I could remember her name and say hello today.

It was that store where I picked up this unusual specimen, for less than the cost of the lunch I had later. Click if you want to zoom in.

<figure><p><a href="https://rubenerd.com/files/2023/386sx-closeup.jpg"><img src="https://rubenerd.com/files/2023/386sx-16-20cn@1x.jpg" alt="Photo of the 386SX-16/20CN REV:0 board" style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2023/386sx-16-20cn@2x.jpg 2x" /></a></p></figure>

Aside from some dust, it was in remarkable physical condition. As you can see in the lower-right corner near the AT keyboard connector, the Ni-Cd battery was long since removed, which thankfully saved the board from any leaking acid. All the manufacturer date codes I can see show 1991, and the AMI BIOS sticker is from 1989.

In the lower-centre we have the AMD Am386SX/SXL-25 CPU. If I remember correctly, the SXs were "binned" versions of the DX that didn't include a floating point unit. A *math coprocessor* could be added retroactively on boards that supported them, such as this one with the empty brown socket between the CPU and rectangular timing crystals. While being a 32-bit part, the external bus also only ran at 16-bit.

The inclusion of this CPU is interesting. When I looked the board up on sites like [The Retro Web](https://theretroweb.com/motherboards/s/dfi-386sx-16cn-20cn), it only shows photos and technical spec sheets with Intel silicon. Whether there was another AMD-specific version the Internet isn't aware of, or the manufacturer substituted AMD parts on some production runs, I'm not sure. Functionally they should be the same.

The board came populated from the shop with four 1 MiB 32-pin SIMM modules, with a mix of M511000C-70J and V53C404BK70 ICs. I'm surprised there are eight RAM banks; most SX boards I've seen only have four. Most of them looked to be in good condition, though the second SIMM looks a bit ratty. *Foreshadowing!*


### But... does it work?

<span style="text-decoration:line-through">Not entirely, but there are signs of life</span>. YES!

Given the success I've had with diagnostic carts on Commodore hardware, I  recently bought an [ISA/PCI "POST card"](my-new-isa-pc-diagnostic-card/) for assisting in troubleshooting. After plugging it in the *correct way* (whoops) and turning the board on, it POSTed! This is the first time the machine has turned on in 21 years.

According to the LEDs, it reports power on all the rails, and the board correctly comes out of reset shortly after booting. The Num Lock light also flashed on my AT keyboard. These are all good signs that we have functional power delivery system and CPU, which is *awesome*.

<figure><p><img src="https://rubenerd.com/files/2023/386-board-post@1x.jpg" alt="Photo of the motherboard with the ISA post card connected, showing seven-segment dispays with 04 03." style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2023/386-board-post@2x.jpg 2x" /></p></figure>

The card has two pairs of seven-segment displays. The first pair showed 04, for which the bootlet provided no details for AMI BIOSs. The 04 code on [BIOS Central](https://www.bioscentral.com/postcodes/amibios.htm) showed "8259 programmable interrupt controller has initialized OK", and "passed keyboard controller test with and without mouse" for releases prior to 1990, and after. Good to know either way, but not super useful.

After fixing the orientation of the PC speaker cable leading to the motherboard, I turned it back on and heard three loud beeps. The supplied booklet claimed this was a “base 64K RAM failure.” I removed all the SIMMs from the board and powered it on, with the same result.


### Focusing in on the RAM

Next step, as [Jan Beta](https://www.youtube.com/channel/UCftUpOO4h9EgH0eDOZtjzcA) always suggests trying, and as Adrian Black from [Adrian's Digital Basement](https://www.youtube.com/user/craig1black) is fond of saying: *DeOxit that socket!* I cleaned the RAM sockets and applied DeOxit to them, then carefully to each SIMM. One of the modules in particular had a ton of nasty crud on its pins, but some magic eraser and a wipe later, and they looked better.

I repopulated the cleaned SIMMs into different positions, with no difference. One of the modules definitely had more worn pins than the other three, so I put it to one side and tried various combinations of the others until I had a pair that worked. This lead the machine to beep *six* times. Progress! According to the POST card booklet, this means *Keyboard Controller 8402 failure*. I plugged my AT keyboard back in, and crossed my fingers.

The next beep code was 1 long, 8 short. It was like the music of angels, because I knew from the [BIOS Central](https://www.bioscentral.com/postcodes/amibios.htm) beep codes that it meant a missing graphics card. As far as I'm aware, it would only report this if the other checks had completed fine.

<figure><p><img src="https://rubenerd.com/files/2023/386-working@1x.jpg" alt="View of the working system, showing Copyright 1990, Oak Technology VGA BIOS and 2048 KB of RAM!" style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2023/386-working@2x.jpg 2x" /></p></figure>

I've been carrying around an old Oak ISA VGA card for even longer than this 386 board, so I plugged it in not even knowing if it worked itself. I wasn't prepared for what I got. The VGA card *and* the motherboard worked! I was so excited, I couldn't even keep the camera straight!


### Where to go from here

I legitimately didn't think this would work. I thought I was validating my suspicions that the board and/or CPU were toast, and that I'd either be trying to sell it for parts, taking it to ewaste, or relegating it to being a display item.

Now I have the *exact opposite problem!* I need to get a drive controller! A floppy disk drive! A replacement CMOS battery! And then... what do I put this in!? Do I get a new case? Build one out of something silly? Do a reverse-sleeper case and mod a new ATX gamer case to accommodate it? Do I fullfill another childhood idea of building a computer... *in a breadbox?*

One thing is for sure, this glorified test bench I have here will now be carefully packaged into anti-static bags while I decide how best to install it. After some tinkering :).

<figure><p><img src="https://rubenerd.com/files/2023/386-done@1x.jpg" alt="Photo after the system config screen, requesting a floppy disk that doesn't exist!" style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2023/386-done@2x.jpg 2x" /></p></figure>
