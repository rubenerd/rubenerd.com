---
title: "Rearranging spaces for more… space!"
date: "2023-10-16T08:43:28+11:00"
abstract: "This apartment is smaller than my bedroom was in Malaysia."
year: "2023"
category: Thoughts
tag:
- architecture
- design
- life
- public-transport
location: Sydney
---
Clara and I spent Sunday rearranging the apartment to try out a new position for our computer desks. It's still a work-in-progress, but I'm already amazed how much bigger the space feels. It seems logically ridiculous; it's the same tables, the same stuff, in the same confined space. How could this make any meaningful difference whatsoever?

For the couple of years we lived in Malaysia when I was growing up, my dad's company put us up in a McMansion on the outskirts of Kuala Lumpur. It was ugly, poorly constructed, and not connected with any good public transport. Parts of KL have great train service, especially with the new MRT lines, but this wasn't one of them. I felt more isolated there than at any point in my life, and I'm sure it contributed to my mum's poor mental health at the time too.

But I digress! One thing that's struck me since was that my *bedroom* in that house was the size of our entire apartment now, even ignoring the *en suite* bathroom. But it was such a large box, you ended up with a mass of wasted space in the middle. The upshot was I had the bed, tables, and such lining the walls, resulting in a room that somehow managed to achieve feeling cluttered but empty. The whole house was like that; it was massive, yet somehow there wasn't *space* for anything. It was a reverse Tardis, or a reverse Secret Squirrel briefcase.

It amazes me that Clara and I fit all our stuff in this tiny apartment now, including a kitchen and bedroom. With rental and house prices in Sydney, it's also not like we have a choice if we don't want a three hour commute. But it's kudos to the architect who designed this place that we can live here comfortably without feeling (too) hemmed in. All our apartments in Singapore were the same.

I crashed an architecture lecture at uni with a friend once, and their guest speaker talked about how good designs can invent space out of nothing, and that bad designs use it up on nothing. Even with the same building blocks, the difference between having a space that feels open and efficient, or a hobbled mess, can be vanishingly small. It's also probably why every SUV I've been in has managed to feel so small for the size it is.
