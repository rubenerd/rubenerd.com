---
title: "Figuring out input selection on a Dell 4K panel"
date: "2023-11-22T09:42:24+11:00"
abstract: "The Auto setting isn’t."
year: "2023"
category: Hardware
tag:
- accessibility
- displays
- design
- monitors
- usability
location: Sydney
---
This post is another thought experiment, though one grounded in something a bit more concrete than the last few times. I'm going to present an interface to you, and I want you to take a guess at what it does, and how it works.

Today's interface is on a Dell 27-inch 4K panel, which I have connected to my FreeBSD tower via HDMI, and my MacBook Air via USB-C (carrying a DisplayPort signal). Turning on the panel presents the following menu:

> **Dell 27 Monitor**
>
> Input Source:   
> • Auto   
> • USB-C   
> • HDMI 1   
> • HDMI 2

What do you think happens when you select these options? Give it some thought, we're not in a hurry here. Unless you are, in which case, you'd better hurry.

Starting from the bottom, you'd be correct for thinking **HDMI 2** switches over to that input, and the same with **HDMI 1** and **USB-C**. Your chosen input also *persists* between power cycles, or even with the connection of other devices. This is ideal, because it'd be frustrating if the monitor changed its inputs each time you turned it on.

That leaves us with **Auto**. What do you think *that* does? If you're like me, you might have thought it referred to auto input selection. The monitor would turn on, detect which of its connections has a signal, and transfer to that. Simple! Right?

Here we come to an edge case. What if you have **Auto** selected, but *two* active devices connected? Would it default to the first one in the list, or the last device used? I'd expect either, depending on the context. The point is, I'd expect **Auto** to be a dynamic setting, otherwise I'd stick with manually changing the input each time.

But that's *not* how **Auto** works. Unlike every other input source, **Auto** doesn't persist. Instead, it detects which input is connected **once**, then sets the monitor permanently for that input. That's it.

Did you change from **USB-C** to **HDMI 2**? Tough! **Auto** configured the monitor for **USB-C**. Changing input requires going back into the menu again, and either choosing the new input, or selecting **Auto** again.

What we have here isn't auto *selection*, but auto *initialisation*. Which isn't entirely useless; most people probably only connect one device to their monitor and are done with it. But for those using it as a pseudo KVM (as its additional USB ports and power pass-through enable), the **Auto** setting is functionally a waste of time.

Monitor manufacturers! I know you all read my posts here, so I implore you to be a bit smarter with features like this. Your actions would have a *profound* impact on the quality of life of the five of us who use your devices with multiple inputs, because we don't have the desk space for an additional monitor. Or would, if we didn't have all these dang retrocomputers.
