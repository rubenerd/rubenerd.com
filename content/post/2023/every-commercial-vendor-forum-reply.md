---
title: "Every forum reply on a commercial vendor site"
date: "2023-09-29T07:02:26+10:00"
abstract: "As they say in Belgium, lots of waffle, not much substance. Except Belgians still have substance."
year: "2023"
category: Internet
tag:
- forums
- spam
location: Sydney
---
> Dear customer,
>
> Thank you for contacting the $VENDOR support page. My name is $NAME, a $VENDOR Independent Advisor, and I'm here to assist in your enqiury and to solve whatever problem you may be facing at the present time with your system, game, application, phone, or other device. We take your concerns seriously. I'll be happy to help you out today. Let’s see what I can do.
> 
> I understand your frustration there and I apologise for any inconvenience it may cause you. Rest assured that I will do my best to help you.
>
> **One-line answer and URL that doesn't address the question.**
>
> Note: This is a third-party link and we do not have any guarantees on this website. This is just for your convenience. $VENDOR does not make any guarantees about the content, nor does it represent the views of the $VENDOR, its subsidiaries, or partners.
>
> We trust this resolves your issue. If you have any questions, concerns, comments, suggestions, tips, or fashion advice, please contact $VENDOR support and cite the question and the thread ID. This site is built on feedback from people like you, and we continue to strive to improve our excellent service in response to your queries.
>
> Most sincerely, thank you, all the best, salutations, and best wishes for the future with your newly fixed device, product, software, or service!
>
> $NAME   
> $VENDER Certified Professional   
> $VENDER Accredidation 1   
> $VENDER Accredidation 2
> 
> Disclaimer: This posting is provided "AS IS" with no warranties or guarantees, expressed or implied, and confers no rights.
>
> Please remember to mark the reply as an answer if they help so it can be closed on our system.
