---
title: "Getting stuck in the middle of a stroad"
date: "2023-06-03T09:27:21+10:00"
abstract: "Stroads try being streets and roads, and fail at both."
thumb: "https://rubenerd.com/files/2023/yt-ORzNZUeUHAM@1x.jpg"
year: "2023"
category: Thoughts
tag:
- australia
- canada
- design
- environment
- malaysia
- united-states
- urbanism
location: Sydney
---
*Stroad* is a deliberately-ugly portmanteau given by Strong Towns to thoroughfares that fail at being streets and roads. Roads are high-speed connections, and streets are complex environments with driveways and pedestrians. Trying to be both, they argue, makes stroads ineffective, expensive, ugly, and dangerous. And they have the data to prove it.

[Strong Towns website](https://www.strongtowns.org/)

From personal experience, Australia's stroads aren't as scary as those that blight North America or Malaysia, though they're not far behind. It's common for Australian stroads to at least have a median strip of concrete, and our cars are (currently) much smaller than American monster trucks. But they'll often not have pedestrian crossings on all sides, and they're just as miserable to live and work near.

<figure><p><a target=_BLANK href="https://www.youtube.com/watch?v=ORzNZUeUHAM" title="Play Stroads are Ugly, Expensive, and Dangerous (and they're everywhere) [ST05]"><img src="https://rubenerd.com/files/2023/yt-ORzNZUeUHAM@1x.jpg" srcset="https://rubenerd.com/files/2023/yt-ORzNZUeUHAM@1x.jpg 1x, https://rubenerd.com/files/2023/yt-ORzNZUeUHAM@2x.jpg 2x" alt="Play Stroads are Ugly, Expensive, and Dangerous (and they're everywhere) [ST05]" style="width:500px;height:281px;" /></a></p></figure>

Recently I was crossing a six-lane *stroad* in the Sydney suburb of Mascot, like a gentleman, and I ended up in a sticky situation.

The pedestrian light went green, so I started crossing. About halfway along, I heard the siren from a fire truck and stopped. To everyone's credit at the busy intersection, the truck was able to zip through despite having a red signal, and the cars began flowing again soon after. Having lived and travelled through a bunch of places, I'd rate Australia close to the top for respecting emergency vehicles; it's always an honour to witness.

The problem was, I'd run out the clock for the pedestrian light waiting there for that fire truck. I then found myself perched on a concrete median barely half a metre across, in the middle of the stroad, with a red pedestrian light, and six lanes of peak-hour cars flying behind and in front of me. Scarier still, much of the traffic were large articulated trucks carrying containers for the nearby freight terminals and airport, which displaced enough air to almost wash me off the median. I even felt something lightly brush against my backpack, which I couldn't even reach across to take off because *there were cars flying past me!*

It was loud, smelly, and I feared for my safety.

It probably lasted less than a couple of minutes before the pedestrian light went green, but it felt like an eternity. As I made it to the other side, my fear changed to frustration; not at any one driver, but at the situation, the structure, and all the dull bureaucrats who commissioned and signed off on this lazy infrastructure.

Much as poverty is a policy failure, these *stroads* represent a failure of urban planning and design. They fail motorists, they fail pedestrians, and they fail the environment. I don't drive, but I can see why if I had to navigate such roads on a regular basis I'd want to start, instead of being a pedestrian or cyclist. Yay, induced demand!
