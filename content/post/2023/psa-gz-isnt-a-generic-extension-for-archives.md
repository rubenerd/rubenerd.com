---
title: "PSA: gz isn’t a generic compressed file extension"
date: "2023-05-03T14:45:09+10:00"
abstract: "Extracing a xz that was labelled as a gz"
year: "2023"
category: Software
tag:
- compression
- psa
location: Sydney
---
Take a look at this filename, and take a guess what it is:

    rootfs.gz

If you're like me, you assumed **gz** meant **gzip**, and therefore I could extract it with **gunzip(1)**:

    $ gunzip rootfs.gz
    ==> gzip: rootfs.gz: not in gzip format

Wait, so what is this file file?

    $ file rootfs.gz
    ==> XZ compressed data

This happens constantly. Is it a mild form of obfuscation? Do people think **gz** is a generic catch-all for all compressed data? Was a directive passed down to convert things to use **xz**, but tooling wasn't updated to output the correct extension?

If I had a cent for each time this has happened, I'd still need many more to afford a coffee. But it's more than nothing, which is what I hope people reading this blog conclude about the quality of my posts.
