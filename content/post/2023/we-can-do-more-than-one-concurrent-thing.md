---
title: "We can do more than one concurrent thing"
date: "2023-07-25T07:25:28+10:00"
abstract: "This rhetorical tactic is a popular form of misdirection, but it’s logically flawed."
year: "2023"
category: Thoughts
tag:
- debates
- politics
location: Sydney
---
How old were you when you were finally able to rub your stomach and pat your head... *concurrently!?* Or was it rub your head and pat your stomach? Either way, I remember a few of us getting the hang of it quickly, when others in the class struggled. My music teacher said I should be a drummer on this basis, which I now sadly regret not picking up. Maybe I need an apartment-friendly electronic set.

*But I digress.* Some of those people who struggled with patting their knees and rubbing their elbows grew up, and entered project management, public relations, or politics. You can spot them because they claim we can't do something *because we're doing something else*.

This rhetorical tactic is a wildly-popular form of misdirection, because it so easily frames a debate away from something the subject finds inconvenient. Rather than admit they *don't* want to do something, they claim they *can't* because their attention and/or financing are currently directed elsewhere. At best it demonstrates an inability to think wholistically about a problem. At worst, it's knowingly dishonest.

It's also logically flawed if you spend even a moment thinking about it. Our governments and business are, in fact, capable of doing more than one thing at a time. It's part of the whole point of being in a team, rather than relying on a single figurehead.

It's also *everywhere* once you learn to spot it. Journalists seem especially vulnerable to accepting this talking point at face value, which is then passed onto readers and wider social circles as fact. Before you know it, we're living under the assumption that heads are for patting, stomachs are for rubbing, but they must be done consecutively.

There's only a *bit* of subtext here! But I saw another example of this zero-sum thinking flare up yesterday in a specific IT circle, and I was struck by how simplistic and absurd the whole situation was. Yes **foo** is import, but that has no bearing on our ability to think about or do **bar**.

Granted, multitasking can cause its own problems, and we can be distracted by pulling ourselves into too many directions. Here comes the proverbial posterior prognostication: *but*... its simplistic to think therefore we should throw up our hands and say that anything but a stack is tenable. Say for example, governments claiming they can't tackle a housing issue because they're too busy building a freeway, or a project manager not having time for testing because they're directing the addition of an ancillary feature.

We should expect and demand more from our managers and leaders.
