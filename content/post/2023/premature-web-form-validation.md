---
title: "Premature web form validation"
date: "2023-09-23T08:01:02+10:00"
abstract: "It’s invalid because I didn’t finish typing it!"
year: "2023"
category: Internet
tag:
- accessibility
- design
- webdev
location: Sydney
---
Have you ever started entering information into a web form, only to be told in bold, angry text that it's invalid?

    Email: me@ru [continues typing]
    ==> INVALID EMAIL ADDRESS!

They're right, it's invalid. *Because I haven't finished typing it!*

I suspect there's a breed of web devs who feed a bunch of malformed and valid email addresses through forms they write, check they passes their unit tests, then go grab a coffee. I don't blame them for the latter; I'm drinking one right now.

Except, context is king. Catching someone submitting a form with a malformed email address is one thing, but interrupting someone in the process of typing is something else entirely. It's even more frustrating and visually distracting when these alerts briefly push down or reflow all the other elements on a page.

I've already been told that being frustrated about this is silly, because reasons. To those I say, was doing this an improvement over what came before?
