---
title: "A new Asuna fig by Emontoys"
date: "2023-06-28T13:59:15+10:00"
abstract: "Her outfit is spectacular!"
thumb: "https://rubenerd.com/files/2023/asuna-emontoys@1x.jpg"
year: "2023"
category: Anime
tag:
- figs
- isekai
- sword-art-online
location: Sydney
---
Admitting to watching *Sword Art Online* is tantamount to heresy among the crowds too cool for the isekai genre it spawned. I harbour no such reservations; it was an iconic series, and if you enjoyed it, who cares?

Unfortunately, this does predispose me to wanting this latest fig of Asuna in her *Goddess of Creation Stacia* version. A sculptor *finally* got her determined facial expression right... I'm looking at you, Good Smile Company! The detail her outfit is also spectacular, right down to the folds on her gloves, and the ripples in her skirt/cape thing. Can you tell I know nothing about fashion?

<figure><p><img src="https://rubenerd.com/files/2023/asuna-emontoys@1x.jpg" alt="Front press photo showing her pose and sword." style="width:500px;" srcset="https://rubenerd.com/files/2023/asuna-emontoys@2x.jpg 2x" /><img src="https://rubenerd.com/files/2023/asuna-emontoys-2@1x.jpg" alt="Side press photo, showing detail of her face, dress, and her billowing hair" style="width:500px;" srcset="https://rubenerd.com/files/2023/asuna-emontoys-2@2x.jpg 2x" /></p></figure>

I'm seeing her slowly appear in all the usual places, with pre-orders closing in October. That's a long time for me to ignore the order button so I don't make a silly purchase. At least I can be confident knowing I haven't written about her, and therefore tempted anyone else too.
