---
title: "Coffee culture in Australia and Europe"
date: "2023-06-04T22:08:59+10:00"
abstract: "Espressos, the milk hour, and other conventions they have that we do differently."
year: "2023"
category: Thoughts
tag:
- coffee
- drink
- food
location: Sydney
---
I met up with my dad a few days ago for coffee and lunch, and we got to talking about his recent trip to Europe. It was fun hearing his first-hand experience having coffee in Italy, which he'd always wanted to try.

Australia brands itself as a country of easy-going rural larrikins in tourism ads, but we're one of the most urbanised countries in the world, and we do take certain things *extremely* seriously. We get tea and phrases like "put the kettle on" from our English colonial past, but just as serious is our coffee culture, which was moulded from decades of Greek and Italian immigration. Coffee in Australia is spectacular, eclipsed only by New Zealand across the pond.

[CNBC on why Starbucks failed in Australia](https://rubenerd.com/cnbc-on-why-starbucks-failed-in-australia/)

My dad's impressions of coffee in Europe, particularly Italy, were they were just as good as what we had here, provided you knew where to go. But most interestingly was what he called the milk hour, which occurred late in the morning. Ordering anything with milk after this would be met with bewilderment; like asking for a bowl of cereal at 16:00.

I tend to order black coffee because I crave that sharp taste, and for someone who drinks 1-3 cups a day, I didn't need all that extra dairy in my diet. But *occasionally* I treat myself to a cap. If Clara and I ever go to Italy, I'll need to keep this in mind!

Another comment was that *espressos* are far more common over there. Our part of the world literally invented the *flat white* to make a commute-friendly beverage one can sip, but it's far more common over there for people to order a shot or two, drink it in one gulp, and move on. Come to think of it, I don't think I've ever seen an Australian order an espresso.

[Dutch milk bread and spreads](https://rubenerd.com/dutch-milk-bread/)

I wonder how the Low Countries do coffee? Again, given Clara's and my current obsession with the place. You would think the Dutch could make great coffee given there's so many wonderful beans from Indonesia and South Africa, but maybe not. If I'm going to warm a stroopwafel over a brew, it'd better be good!

*Update:* Just as I was about to post this, a friend told me about this coffee shop back in South Australia. I need to head back there one day :).

[Dutch Coffee Lab in Adelaide](https://www.facebook.com/dutchcoffeelab/)
