---
title: "What if the Brits French’d their royal family?"
date: "2023-07-22T09:51:41+10:00"
abstract: "Would Australia have a co-prince as a head of state?"
year: "2023"
category: Thoughts
tag:
- australia
- canada
- new-zealand
- politics
location: Sydney
---
If the stories are to be believed about the royals receiving a large pay rise in the UK's current economic climate, the timing does strike me as especially cynical. But then, royals are supposed to laud over their indentured subjects, right?

But imagine for a moment the Brits finally got fed up with these inbred dullards poncing around their historic landmarks, and got in touch with their French or American friends about effecting a solution. What would happen to the Commonwealth?

*(Poncing is such a great term).*

Places like Australia, New Zealand, Canada, and a dozen other Commonwealth realms still retain the British monarch as their head of state in a bizarre situation that self-professed Monarchists actually defend on the basis of serfdom being a desirable tradition, or something. I've yet to encounter Charles' mug on a local coin, but that might be down to me not using cash during Liz's reign.

If the Brits suddenly elect a President, Chancellor, or whomever, would they become our head of state too? We'd be in this strange situation where we have a democratically-appointed leader we'd still had no say in choosing. Maybe we should be able to send representatives to Westminster? I should ask my Scottish friends how that's working out.

There's precedent for this, somewhat? The Co-Princes of Andorra are drawn from the Bishop of Urgell in Spain, and the holder of the Crown of France, which in modern times has been interpreted as their President. Anointing the President of the UK and Northern Ireland the title of Prince of Australia does sound a *bit* silly, but then, nothing about this arrangement makes any sense whatsoever.

Things get even murkier when you consider that some countries have taken the rational step of eschewing (gesundheit) the position of a traditional head of state entirely, like Switzerland's Federal Council. Say for example the UK decided to lop off their Monarchy and... not replace it with anything. Would Australia have *two* Prime Ministers?

And finally, we get to the conspiracy theory (that I've been told earnestly) that the UK will be retaken by the European Union by force, and it will be federalised into a subject under a supreme EU emperor. I proposed they call it the *Holy Roman Empire 2.0* or *Charlemagnea* but they didn't seem receptive. Would *they* then become our head of state?

Inertia is a powerful force, so I doubt we'll be tackling this any time soon. But the absurdity highlights how pointless this all is.
