---
title: "The elusive textInput form in RSS feeds"
date: "2023-01-23T07:32:12+11:00"
abstract: "Kyle Lanchman writes in with a patch he submitted to fix parsers handling my RSS feed."
year: "2023"
category: Internet
tag:
- feedback
- rss
- troubleshooting
location: Sydney
---
[Kyle Lanchman](https://blog.kylelanchman.com/) wrote in to let me know of an RSS rendering bug my blog introduced to a few Mac clients, which he's since [written a patch for](https://github.com/Ranchero-Software/RSParser/pull/76)\:

> Fixes an issue where the feed's title would get set to the textInput's title instead of the channel's title. For the feed I've added to the tests, NetNewsWire incorrectly showed the feed's title as "Search" instead of "Rubenerd".

The RSS specification includes the provision for a **[textInput](https://cyber.harvard.edu/rss/rss.html#lttextinputgtSubelementOfLtchannelgt)** form, though its origins and use remain "a mystery". I added it back to my feed recently to let people do searches, as the spec suggests:

	<textInput>
		<title>Search</title>
		<description>Search Rubenerd.com</description>
		<name>q</name>
		<link>https://html.duckduckgo.com/html/</link>
	</textInput>

I thought it was innocuous enough, but some parsers really don't like seeing elements they're not expecting. This bug caused software using the RSParser to render the title of the whole feed as *Search*, which was nested under **textInput**.

I'll keep the form until Kyle's pull request is approved, because he's using the feed as an example. I've changed the title to say "Search Rubenerd" instead though, so at least malfunctioning parsers at least show a functional title.
