---
title: "Some August housekeeping"
date: "2023-08-17T08:42:30+10:00"
abstract: "Fixed some permalinks and missing metadata"
year: "2023"
category: Internet
tag:
- weblog
location: Sydney
---
I squashed a few bugs:

* My post yesterday about ethics with larger audiences had a malformed permalink. I've fixed this, but it also changed the identifier in the RSS. Apologies if you get this again.

* I realised I had a couple of duplicate posts that didn't appear in the main page, but did in the archives.

* A few recent posts were missing metadata, but we went looking for it together and found it all.

I asked a while ago if people thought these sort of posts were interesting, and most of you said yes. I'm still unconvinced, so here's a Ukrainian sunflower too 🌻.
