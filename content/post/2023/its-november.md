---
title: "It’s November"
date: "2023-11-01T11:00:43+11:00"
abstract: "A history of the name, and what I’d like to get done this month."
year: "2023"
category: Thoughts
tag:
- apple-iie
- compaq-presario
- friends
- lists
- personal
location: Sydney
---
November derives its name from the Latin *novem*, or ninth month. This made more sense before we added January and February to our Gregorian calendars, which pushed November down to number 11. This also lead me to finding and correcting a mistake [Wikipedia's November page](https://en.wikipedia.org/wiki/November "Wikipedia article on November") while researching this pointless paragraph, like a boss.

November is the *de facto* start of the holiday season, judging from all the decorations around our local shopping area. It's also the last month with 30 days, and the only month that stars with the netter N.

With those fascinating (cough) factoids out of the way, here are some random things I'd like to get done this month:

* Re-negotiate rent. Goodie!

* Get around to finishing my 1998 Presario 5060 build. As with most things, I got it 90% of the way there, but I need to validate the floppy drive works after I noticed some weird sounds and behavior. It also needs a CF card, and a bit more memory.

* Go on some nice, long walks.

* Meet with more friends iRL. My socially-awkward and introverted tendencies have been on a rampage again of late, and I need to correct it.

* Update my FreeBSD tower and server/bhyve/jail machine with proper VNETs, something I've put off for far too long because cloned interfaces have "just worked".

* Finish researching VGA options for the Apple IIe. There are a few aftermarket VGA options which would get me better picture quality, but more importantly would me connect it directly to the KVM.

I'm sure I forgot something important, which I should do less of in November too.
