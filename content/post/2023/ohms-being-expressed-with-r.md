---
title: "Ohms being expressed with R instead of Ω"
date: "2023-08-07T08:23:49+10:00"
abstract: "I didn’t realise they were equivilent in this context."
year: "2023"
category: Hardware
tag:
- electronics
- retrocomputing
location: Sydney
---
I was on an online store looking for electrical components last night, like a gentleman, when I became increasingly frustrated at the lack of stores selling the correct resister I needed for my attempted Commodore VC-20 S-video mod. That's a topic for another post.

It was only in the clear light of morning that I realised what I needed was staring me right in the face. **75R** is equivilent to **75 Ω**!

[According to Wikipedia RKM code article](https://en.wikipedia.org/wiki/RKM_code)

> When the value can be expressed without the need for a prefix, an "R" is used instead of the decimal separator [for resistance]. For example, 1R2 indicates 1.2 Ω, and 18R indicates 18 Ω.

I still regret not doing an electrical engineering elective at uni when I had the chance. Everybody gotta start somewhere.
