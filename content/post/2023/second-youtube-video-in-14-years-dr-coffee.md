---
title: "My second YouTube video in 14 years: Dr. Coffee"
date: "2023-05-29T23:14:40+10:00"
abstract: "I think my creativity has peaked."
thumb: "https://rubenerd.com/files/2023/yt-KhytbrZwI3E@1x.jpg"
year: "2023"
category: Media
tag:
- coffee
- pointless
- video
- youtube
location: Sydney
---
<figure><p><a target=_BLANK href="https://www.youtube.com/watch?v=KhytbrZwI3E" title="Play Dr. Coffee"><img src="https://rubenerd.com/files/2023/yt-KhytbrZwI3E@1x.jpg" srcset="https://rubenerd.com/files/2023/yt-KhytbrZwI3E@1x.jpg 1x, https://rubenerd.com/files/2023/yt-KhytbrZwI3E@2x.jpg 2x" alt="Play Dr. Coffee" style="width:500px;height:281px;" /></a></p></figure>

This is dedicated to Scotland's favourite coffee aficionado and writer James. I... think my creativity has peaked.

[James’ Coffee Blog](https://jamesg.blog/)

