---
title: "Fixing DDR4-1867 on a Supermicro server"
date: "2023-06-09T09:04:01+10:00"
abstract: "Disable POR"
year: "2023"
category: Hardware
tag:
- troubleshooting
- servers
location: Sydney
---
If you're refurbishing an old box that's only detecting your DDR4-2133 or 2400 as 1867 MHz, get into the BIOS and disable POR:

1. BIOS
2. Advanced Tab
3. Chipset Configuration
4. North Bridge
5. Memory Configuration
6. Enforce POR &rarr; Disabled

A reboot later, and you should have the correct speed your DIMMs support.
