---
title: "What IMS stands for"
date: "2023-01-25T16:02:29+11:00"
abstract: "Eat a salad while looking at all my Idolmaster merch in an inventory management system on DOS, somehow."
thumb: "https://rubenerd.com/files/2023/ims@1x.jpg"
year: "2023"
category: Anime
tag:
- idolmaster
- nostalgia
- shibuya-rin
location: Sydney
---
Off the top of my head, or something inside my head:

* Intelligent Micro Software
* Inventory management system
* Idolm@ster
* I make salads

You could potentially track your Idolm@ster merch in an inventory management system running on Multiuser DOS by Intelligent Micro Software... while eating a salad I made.

And people say this blog doesn't have enough practical, real-world advice that's helpful. They're likely right.

<figure><p><img src="https://rubenerd.com/files/2023/ims@1x.jpg" alt="Key visual of the idols from Idolm@ster Shiny Colours against a modern Japanese skyline with sakura petals, because of course." srcset="https://rubenerd.com/files/2023/ims@2x.jpg 2x" style="width:500px; height:333px;" /></p></figure>

As an aside, this key visual from *Idolm@ster Shiny Colours* featuring everyone's favourite idol from the series Shibuya Rin (cough) came out in 2015. It already look retro somehow, and not just on account of the smartphone with the small camera. Kronii and Watson are cruel mistresses.
