---
title: "Music Monday: Paul McCartney, Take it Away"
date: "2023-06-05T08:37:35+10:00"
abstract: "This song from his 1982 album Tug of War has been living in my head for weeks."
thumb: "https://rubenerd.com/files/2023/yt-C-9761kYfzU@1x.jpg"
year: "2023"
category: Media
tag:
- music
- music-monday
- paul-mccartney
location: Sydney
---
It's worth its own post, but Clara and I were chuffed at the number of incredible second-hand music stores in Japan, especially vinyl. We got immaculate Japanese Odeon pressings of *Rubber Soul*, and Paul McCartney's 1982 masterpiece *Tug of War*, for half of what JB Hi-Fi in Australia are charging for a single re-issue! There's something special about having an English album with Japanese inserts and sleeves.

Every song on *Tug of War* sparkles, but this is the one that's been living in my head for the last month. My musical theory that "everything sounds better with brass" is on full display here. Ringo even supplies the drums!

<figure><p><a target=_BLANK href="https://www.youtube.com/watch?v=C-9761kYfzU" title="Play Take It Away (Remixed 2015)"><img src="https://rubenerd.com/files/2023/yt-C-9761kYfzU@1x.jpg" srcset="https://rubenerd.com/files/2023/yt-C-9761kYfzU@1x.jpg 1x, https://rubenerd.com/files/2023/yt-C-9761kYfzU@2x.jpg 2x" alt="Play Take It Away (Remixed 2015)" style="width:500px;height:281px;" /></a></p></figure>

