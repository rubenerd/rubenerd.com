---
title: "Missing my 876-page milestone"
date: "2023-08-28T16:36:04+10:00"
abstract: "It’s a fun number!"
year: "2023"
category: Internet
tag:
- numerology
- pointless
- pointless-milestone
location: Sydney
---
I just checked my site footer, and sure enough we've ticked over to **877** pages. Alamak.

**876** is a fun number. The digits are in descending order, or ascending if you read it backwards! It's the calling code for Jamaica! And... okay, that's all I've got.

Thanks for reading whatever subset of these pages you have, it means a lot. 

[View all past pointless milestones](https://rubenerd.com/tag/pointless-milestone/)
