---
title: "Hiding MediaWiki meta generator version"
date: "2023-02-03T10:50:26+11:00"
abstract: "Update includes/OutputPage.php"
year: "2023"
category: Internet 
tag:
- guides
- mediawiki
- servers
- wikis
location: Sydney
---
MediaWiki includes this header meta tag in pages:

	<meta name="generator" content="MediaWiki x.x.x"/>

Someone asking on the official Phabricator [didn't get any useful answers](https://phabricator.wikimedia.org/T270555), especially if you need it for compliance.

As of the time of writing, you can change this block in **./includes/OutputPage.php** to remove the version:

	$tags['meta-generator'] = Html::element( 'meta', [
    	'name' => 'generator',
		'content' => 'MediaWiki',
	] );

You'll also want to redirect or block the **Special:Version** page and **README** files in your web server config, and review [API access](https://www.mediawiki.org/wiki/API:Restricting_API_usage). 

You should still credit MediaWiki, given their server software is running your site. 
