---
title: "Variety in PCs, after years of the Mac"
date: "2023-09-20T15:32:48+10:00"
abstract: "The computer industry hadn’t become boring, I just thought it had."
thumb: "https://rubenerd.com/files/2023/yt-YvvEFoygXOM@1x.jpg"
year: "2023"
category: Hardware
tag:
- apple
- mac
- pcs
location: Sydney
---
This post is a bit disjointed, because it was three half-finished drafts and an attempted *Music Monday* smooshed into one. But I *think* there's something cohesive here... if not, we can pretend together. R-right?

> You're so beautiful, but oh so boring;   
> I'm wondering what am I doing here.

<figure><p><a target=_BLANK href="https://www.youtube.com/watch?v=YvvEFoygXOM" title="Play So Beautiful"><img src="https://rubenerd.com/files/2023/yt-YvvEFoygXOM@1x.jpg" srcset="https://rubenerd.com/files/2023/yt-YvvEFoygXOM@1x.jpg 1x, https://rubenerd.com/files/2023/yt-YvvEFoygXOM@2x.jpg 2x" alt="Play So Beautiful" style="width:500px;height:281px;" /></a></p></figure>

Macs have been my primary work machines for at least the last decade, mostly for Office and a few other proprietary tools on which the professional world depends. I also used to like the Mac, so I had personal Mac laptops before. They were kickarse UNIX workstations in a time when that was becoming rarer.

You get used to the way Apple wants things to work when you're a macOS user. It's not like you can grab the OS and put it on another machine; Hackintoshes notwithstanding. Some Apple hardware is striking, and their M chips are so far ahead in performance and battery life it's embarrassing.

But conversely, you accept certain limitations as being inevitable. Need more ports? A better graphics card? An 11-inch display again? Tough!

It's also, as I quoted Simply Red above, boring. People are lamenting the recent TiPhone [sic], but the truth is MacBooks and iPhones have been dull iterations of their former selves for years. For someone who used to wake up early to see what the latest PowerBook looked like, I can't remember the last Apple Keynote I bothered watching.

It's a bit sad.

🌲 🌲 🌲 

Anyway, for years I'd ignored the rows of PC laptops and desktops whenever I'd walk into a large retail store. I only buy personal FreeBSD and NetBSD notebooks used, and it's not like any of these new laptops in a store can run macOS, so I tuned them out.

I'd almost forgot just how much variety the PC ecosystem has, from manufactures to product ranges. Strolling through the PC section of JB Hi-Fi took me right back to my childhood, when I'd pour over price lists and hoarded copies of PC Magazine to get technical specifications to figure out *exactly* the one to save my precious pocket money and after-school jobs for.

Don't get me wrong, most PCs are still rubbish. Crappy keyboards, low-resolution screens with poor colour, cheap plastic bodies, underpowered specs, tinny speakers, poorly-placed webcams, unresponsive trackpads, and chintzy designs. People like to mock Apple for being expensive, but there's no question its baseline of quality is far above what PC makers churn out. That's a grim indictment of the PC industry given how little Apple cares about the Mac thesedays.

But then out of the fray of mediocrity come a few standout devices. I was bewitched by an Asus laptop which has a proper second screen embedded above the keyboard, and a trackpad to the side rather than below. It didn't ape Apple's chicklet keyboard, and it's industrial design was like nothing I'd seen before. It was trying something different, to great effect. *Tech is cool!*

The computer industry hadn't become boring, I just thought it had.
