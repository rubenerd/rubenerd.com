---
title: "Apple’s integrity-free APFS turns six"
date: "2023-03-31T09:12:50+11:00"
abstract: "How this was released still floors me."
year: "2023"
category: Software
tag:
- apple
- file-systems
location: Sydney
---
Both APFS and I were released on the 27th of March. I like to think I have integrity, but Howard Oakley [wrote a great retrospective](https://eclecticlight.co/2023/03/27/apples-big-gamble-the-6th-birthday-of-apfs/) that reminds us that APFS doesn't:

> From its first announcement, APFS has been criticised for its lack of integrity checks on data storage, and lack of redundancy. While file system metadata in APFS do use checksums, there’s no option to protect data using them, nor does APFS provide any scope for a third-party to add integrity checks or redundancy as an extension.

APFS is copy-on-write, so that's a start. But it's boggles the mind that a company could seriously launch a file system in 2017 without integrity checks... or *at least* the ability to enable it. But then, Apple have lost the plot with their desktop OSs for a number of years now.
