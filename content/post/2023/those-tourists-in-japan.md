---
title: "“Those” tourists in Japan"
date: "2023-04-19T12:00:44+09:00"
abstract: "Don’t be the guy who listed a million things to a Japanese person... in English."
year: "2023"
category: Travel
tag:
- kindless
- tourism
location: Osaka
---
Being a tourist is wonderful. You get to trade in your daily grind for some novelty, you get to explore a new part of the world, and you might even learn something if you have an open mind. I'm privileged and honoured to be able to dedicate much of my life to this.

But being a tourist comes with a glaring downside: you're a tourist. Nobody likes tourists. Other tourists don't like tourists. We're one of the original self-hating groups. Even those who's livelihoods depend on tourists are more likely to say they like tourist *money*.

Why is that? In aggregate, tourists bring noise, pollution, and traffic. But I think the bigger issue, especially for hospitality and retail staff, are the *arrogance* of tourists. The financial and power imbalance is especially notable in poorer parts of the world, though a vocal subset of tourists are insufferable regardless of where they are. Some tourists only take photographs and leave footprints, others aren't as considerate.

Case in point, Clara and I were having breakfast at a beautiful local Osaka coffee shop, when the peace was interrupted by a loud Australian tourist rambling off the most complicated order I'd ever heard... in English. I'd have scarcely remembered half of it in our common language, but the poor Japanese barista didn't stand a chance. When it was clear the tourist wasn't getting through to him, he continued to speak English, but *slowly*. Fortunately for everyone involved, he eventually gave up with a "never mind, mate" and walked out.

[Osaka Point page for the UPGREEN cafe](https://omdm.osaka-point.jp/shop/shop-list/542/)

I caught the barista's eye from our table, shook my head, and apologised in (broken!) Japanese for the other *gaijins* he has to deal with. He laughed it off like the consummate professional he was, but you could tell it had a tinge of frustration to it. Less than fifteen minutes later, the same thing happened with a few tourists speaking Mandarin.

I'll admit, I don't know enough Japanese to hold a conversation, but the *least* you can do when going to a new place is to learn pleasantries and how to order something! Even if you don't get it completely right, you've at least demonstrated you respect them enough to try. And worse case, you can devolve to sign language and share a laugh about it! In other words, be kind.

You'd be doing other tourists a favour too.
