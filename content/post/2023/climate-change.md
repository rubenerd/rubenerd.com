---
title: "Human-induced climate change"
date: "2023-12-05T14:20:18+11:00"
abstract: "It’s happening whether you believe it or not."
year: "2023"
category: Thoughts
tag:
- environment
location: Sydney
---
The COP28 summit reminds me that some people need a primer:

1. We, our environment, and our food supply evolved for a narrow band of conditions from which we're now drifting.

2. The planet will survive regardless of what we do to it. It is indifferent to our plight, priorities, or politics.

3. The universe doesn't owe us a good answer to our problems, but the challenge isn't insurmountable.

And a bonus if you're religious:

4. This place was created for us. We should show respect for what we've been given.
