---
title: "Caught Covid"
date: "2023-02-16T10:50:33+11:00"
abstract: "See you again soon 👋"
year: "2023"
category: Thoughts
tag:
- covid
- health
location: Sydney
---
We avoided it for three years, but I suppose it was our turn. We're 4x vaccinated, so thanking my lucky stars it could be a *lot* worse.

It hurts to look at a screen and type, so logging off for now. See you again soon. 👋

