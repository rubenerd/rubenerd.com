---
title: "Science and art: Bens Worx casts a dichroic cube"
date: "2023-06-03T21:36:08+10:00"
abstract: "Beautiful!"
thumb: "https://rubenerd.com/files/2023/yt-Ii0OdI3QsQ4@1x.jpg"
year: "2023"
category: Media
tag:
- colour
- light
- science
- video
- youtube
location: Sydney
---
Australian artist and video creator Ben from *Bens Worx* has been hitting it for six lately with all his projects, but this latest episode might be among his most impressive and beautiful works yet.

[Bens Worx: Turning a Dichroic Cube into a Sphere](https://www.youtube.com/watch?v=Ii0OdI3QsQ4)

<figure><p><a target=_BLANK href="https://www.youtube.com/watch?v=Ii0OdI3QsQ4" title="Play Turning a Dichroic Cube into a Sphere"><img src="https://rubenerd.com/files/2023/yt-Ii0OdI3QsQ4@1x.jpg" srcset="https://rubenerd.com/files/2023/yt-Ii0OdI3QsQ4@1x.jpg 1x, https://rubenerd.com/files/2023/yt-Ii0OdI3QsQ4@2x.jpg 2x" alt="Play Turning a Dichroic Cube into a Sphere" style="width:500px;height:281px;" /></a></p></figure>

Clara's and my favourite moment was seeing the prism shining through the unfinished, translucent blank as Ben polished it on the lathe. It was as though he'd embedded an LED, it was that vibrant. Ben expressed worry that the resin would hinder the refraction, but if anything it accentuated it!

As an aside, my irrational urge to buy a light prism set has only intensified. Some of my favourite experiments in high school science classes were splitting, mixing, reflecting, and playing with light. I'm sure Pink Floyd and my SLR would agree.
