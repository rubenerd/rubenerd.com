---
title: "Dave Winer on “thinking out loud”"
date: "2023-10-23T13:34:00+11:00"
abstract: ""
thumb: ""
year: "2023"
category: Thoughts
tag:
-
location: Sydney
---
[He nails it](http://scripting.com/2023/10/22/011943.html), emphasis added:

> It's weird that people have the idea that thinking out loud is a new idea for blogging. That's because when journalists first heard about blogging they decided that it's what they do. People who write publicly for money, mostly without any principle other than getting paid for writing, or perhaps "building a brand."

I still catch myself writing I'm *thinking out loud* at the start of posts, as though I have to defend thinking about something. The friction also comes from the perception of journalists producing finished articles, whereas people writing their thoughts are... writing their thoughts. Thoughts are malleable.

Conversely, you can tell when someone maintains a personal or business blog, but their heart isn't in it. It's the writing equivilent to going to one of those [corporate events](https://rubenerd.com/your-tech-event-doesnt-have-to-be-exciting "Your tech event doesn’t have to be exciting") that pretend so hard to be something they're not. Enthusiasm, joy, and inspiration are even harder to fake than expertise.

Wouter wrote a [great meta post about blogging](https://brainbaking.com/post/2023/10/blogging-nets-more-than-just-text/ "Blogging Nets More Than Just Text") recently too, and not just on account of linking to me (cough).
