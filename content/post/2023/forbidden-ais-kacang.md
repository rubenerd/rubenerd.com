---
title: "Forbidden ais kacang"
date: "2023-11-19T10:05:54+11:00"
abstract: "A prehnite crystal looked like the Malay desert!"
thumb: "https://rubenerd.com/files/2023/prehnite@1x.jpg"
year: "2023"
category: Thoughts
tag:
- gemstones
- food
- malaysia
location: Sydney
---
<figure><p><img src="https://rubenerd.com/files/2023/prehnite@1x.jpg" alt="Photo of a prehnite crystal by " srcset="https://rubenerd.com/files/2023/prehnite@2x.jpg 2x" style="width:500px;" /></p></figure>

I saw [this photo by Ivar Leidus](https://commons.wikimedia.org/wiki/File:Prehnite_-_Southbury,_Connecticut,_USA.jpg "Wikimedia Commons gallery page of Prehnite from Southbury, Connecticut, USA") featured on Wikimedia Commons, and immediately thought of the [Southeast Asian desert](https://en.wikipedia.org/wiki/Ais_kacang "Wikipedia article on Ais Kacang"). Prehnite is a crystal sillicate formed from calcium and aluminium, and not cendol.

I don't care much for jewellery, but I've always loved geology. According to Wikipedia, gemstone-quality prehnite can be found in Australia's Northern Territory.
