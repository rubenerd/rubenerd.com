---
title: "This driver must get around"
date: "2023-01-08T09:43:36+11:00"
abstract: "Last week I saw someone with a LAN licence plate too."
thumb: "https://rubenerd.com/files/2023/car-wan-licence-plate@1x.jpg"
year: "2023"
category: Internet
tag:
- australia
- networking
- photos
- pointless
location: Sydney
---
<figure><p><img src="https://rubenerd.com/files/2023/car-wan-licence-plate@1x.jpg" alt="Car with a licence plate starting with Wide Area Network." style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2023/car-wan-licence-plate@2x.jpg 2x" /></p></figure>

