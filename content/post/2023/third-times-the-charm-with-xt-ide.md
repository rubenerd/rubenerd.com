---
title: "Third time’s the charm with XTIDE"
date: "2023-12-23T18:01:23+11:00"
thumb: "https://rubenerd.com/files/2023/saber-doublerom@1x.jpg"
year: "2023"
category: Hardware
tag:
- compact-flash
- hard-drives
- retrocomputing
- ssds
- storage
location: Sydney
---
I've had pretty good luck with retrocomputers over the last few years, but storage has been the bane of my existence. The datassette and disk drives on my 8-bit Commodore and Apple computers have proven more reliable than drives fitted to my 1990s PCs! [Finding working storage](https://rubenerd.com/list-of-drives-that-dont-work-in-my-pentium-1/ "List of drives that don’t work on my Pentium 1") for PCs was becoming so ridiculous, I was ready to throw in the towel.


### Enter XTIDE

Back in April this year I watched an episode of Adrian's Digital Basement where he used an [XTIDE ROM](https://www.xtideuniversalbios.org/ "XTIDE Universal BIOS") that let his older 80286 machine detect newer drives. I was awe-struck!

<figure><p><a target=_BLANK href="https://www.youtube.com/watch?v=B0tp-I-iJ1k" title="Play Using large hard drives on a 286, 386 or 486 with the XT-IDE BIOS"><img src="https://rubenerd.com/files/2023/yt-B0tp-I-iJ1k@1x.jpg" srcset="https://rubenerd.com/files/2023/yt-B0tp-I-iJ1k@1x.jpg 1x, https://rubenerd.com/files/2023/yt-B0tp-I-iJ1k@2x.jpg 2x" alt="Play Using large hard drives on a 286, 386 or 486 with the XT-IDE BIOS" style="width:500px;height:281px;" /></a></p></figure>

Those of your in retro PC circles have likely long heard of this project, but in a nutshell is a phrase with four words. XTIDE ROM images can be configured with a DOS utility, burned onto an EEPROM, and then installed into a host card that supports bootable ROMs.

Once installed, the XTIDE ROM loads after your PC POSTs, and detects additional drives and capacities that may otherwise not be supported by the host system. 8-bit ISA XTIDE cards with integrated IDE controllers exist to enable modern drives on computers which may have only supported MFM hard drives, or none at all.

The real magic for me was in later machines that have IDE controllers, but are limited by what drives and capacities they support. I've [tried so many combinations of drives](https://rubenerd.com/list-of-drives-that-dont-work-in-my-pentium-1/ "List of drives that don’t work on my Pentium 1"), and thus far have only got expensive and flaky disk-on-module devices working, or CompactFlash cards without UDMA which limits OS boot options.

What's cool is that XTIDE can surface devices connected to integrated IDE controllers, whether they're on ISA, PCI, or VLB. This would improve performance over the limited 8-bit ISA bus those XTIDE cards use.


### Attempt one: burning my own ROM

My first attempt at getting XTIDE working involved doing something similar to what Adrian Black did in his video. The high-level steps were:

1. Downloading the XTIDE images from the site for your hardware; in my case, the 386 for modern machines.

2. Using the DOS XTIDE configuration utility to configure the image for the target machine.

3. Burning that resulting image on my Minipro to a 28C64 EEPROM.

4. Popping that EEPROM into a NIC, and using its DOS configuration tool to permit booting from its internal ROM.

I couldn't get this working. The XTIDE configuration tool was able to successfully detect the XTIDE ROM was installed on the seven NICs I tested, but I could never get them booting. This was irrespective of whether the NIC was a 3com, SMC, or Compex, on either ISA or PCI, on a 386, Pentium 1, or K6-II with a mix of chipsets.

I could spent more time troubleshooting, but in the end I decided it wasn't worth the hassle. I liked the idea of using the spare ROM socket on NICs that all my retrocomputers already had, but maybe I'd have more luck with a dedicated XTIDE ISA card. These would also support burning the EEPROMs directly from the host machine.


### Attempt two: a DoubleROM card

After paying the Australia Tax and waiting a few million years, I eventually received a [DoubleROM](https://monotech.fwscart.com/product/doublerom---ide-size-limit-remover---dual-bootable-rom-isa-card) by Monotech. This is a cool variant of the XTIDE card that eschews (gesundheit) the integrated IDE controller that I didn't need, and even has support for *two* separate ROMs. I could see this being really useful for trying a few different configuration options.

It's a beautiful piece of kit, easily the best build indie card I've ever bought. Note the silk screened warnings for installation; the reverse has even more prominant arrows indicating which direction in the ISA slot the card needs to be installed.

<figure><p><img src="https://rubenerd.com/files/2023/saber-doublerom@1x.jpg" alt="Photo of the DoubleROM alongside an Xmas Saber!" srcset="https://rubenerd.com/files/2023/saber-doublerom@2x.jpg 2x" style="width:500px; height:333px;" /></p></figure>

You can probably see where this is going. I *knew* which direction it had to be installed, but **somehow** like an **absolute schmuck** I still managed to put it in backwards. I knew within seconds of turning the host machine on that I'd made a massive mistake. Flipping the card the correct orientation, and it didn't work.

Thinking maybe I could salvage something from the board, I popped out the EEPROM and added it to one of my 3Com NICs. Maybe my earlier attempts at burning the image had failed, and this would work. It didn't. I was *crestfallen*, a term that describes the sensation of sitting on top of a wave of optimism only to come crashing back down to Earth.


### Attempt three: a DoubleROM card, again!

Six months into this XTIDE journey, and I finally received another DoubleROM to try. I was tempted to try another XTIDE card, but they're surprisingly difficult to find and very expensive. I took care to install it correctly yesterday, and crossed my fingers.

For the first time since running it in emulation using 86box, I saw that XTIDE BIOS actually boot! I almost couldn't believe it!

<figure><p><img src="https://rubenerd.com/files/2023/xtide-boot@1x.jpg" alt="Boot screen showing the XTIDE ROM detecting a Bigfoot hard drive." srcset="https://rubenerd.com/files/2023/xtide-boot@2x.jpg 2x" style="width:500px; height:333px;" /></p></figure>

I have [half a dozen PCs](http://www.sasara.moe/retro/ "My retrocomputer page on Clara's and my retro site") missing storage devices that I'll now be testing with this card. My aim is to still figure out how to get NICs booting with XTIDE, because getting a card like this for each unit is way out of my budget right now. But the best thing you can have in computing is a working, reference implementation to which you can refer. My hope is I can learn a bit more how all this works, and in the meantime I have something I can swap across machines.

I'll admit, this is the best Xmas present I've had in years! Next step: BeOS on my Pentium 1.
