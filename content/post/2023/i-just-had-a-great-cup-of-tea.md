---
title: "Post titles, and I just had a great cup of tea"
date: "2023-10-18T16:15:57+11:00"
abstract: "A post about tea wouldn’t need a title. Or an abstract, really."
year: "2023"
category: Internet
tag:
- blogging
- tea
- weblog
location: Sydney
---
It was a basic hot cup of black tea from a pedestrian tea bag, but it was *good!* I would have shared this on Mastodon, but I'm putting it here instead. I talk about coffee a lot here, but I drink way more tea so I can do this thing called sleeping that seems to be the rage thesedays. 🍵

I'm starting to see the appeal of having posts without titles. If this post were just about having tea, the title on this post would be redundant. But I've spent 18 years writing posts with titles, and have made so many assumptions based on that. Permalinks include them, pages on my Git repo for this blog are named with the title, and archive pages have lists of titles.

How would I even render an archive page without titles? Would those be shown with dates instead? Maybe the first few words? Would posts without titles be considered more disposable or ancillary, and therefore wouldn't appear on specific archives?

I remember John Walkenbach had post titles, but also had headings for dates. Maybe that's a good middle ground?
