---
title: "The Pacific Motorway, and building for trucks"
date: "2023-09-04T12:07:58+10:00"
abstract: "It’s such a wasted opportunity, not to mention lives and money."
year: "2023"
category: Travel
tag:
- cars
- environment
- sustainability
- trains
location: Sydney
---
A few years ago, I was in the car with friends as we drove from Sydney to Newcastle on the Pacific Motorway. It's a long, wide stretch of road that meanders through a national park, with some jaw dropping scenery. Check out the Wikipedia article for some photos if you're interested.

<a href="https://en.wikipedia.org/wiki/Pacific_Motorway_(Sydney%E2%80%93Newcastle)">Wikipedia: Pacific Motorway (Sydney–Newcastle)</a>

It was late in the evening, and my brother-in-law was increasingly agitated at the number of large trucks boxing us in on all sides. To make matters worse, it was clear some of the drivers were exhausted, inattentive, or bored, based on how they veered in and out of their lanes like iron serpents. There's a reason this stretch of road has so many fatalities and accidents, which are only exacerbated by the road's relative remoteness.

Anyway, he slowed the car down, moved to another lane, and attempted to distance us from the hoard. As we settled into our new position, someone in the car (I forget who) shouted "why are these things on the road!?" We all had a nervous laugh... and I haven't been able to get the though out of my head since.

Owing to topographic constraints, the Pacific Motorway is the only major route between Sydney and Newcastle, the second-largest city in the state. A mix of heavy trucks and cars use it 24/7, with peak periods on Fridays and Sundays as city folk make the trip up north for the weekends. The introduction of those "b-doubles" with two containers in tow have made these trucks even bigger, both in size and danger.

I've made no secret of how much I hate suburban 4WDs, SUVs, and American-style light trucks, but rollover concerns aside, I wouldn't fault people who'd want to drive them on a road like this. A hatchback felt practically invisible among the scores of tall, heavy boxes on wheels, driven by people faking logbooks to appease their bosses.

Increasing cargo lengths is the only way to gain further efficiency from a truck, but it highlights the absurdity of the whole situation. "Road trains" in remote, rural areas with little infrastructure make sense, but they're a recipe for disaster on a crowded suburban motorway. As is proved time, and time again.

Seeing the situation of traffic and safety, state governments' only solutions have been to permit these larger truck sizes, and widen the roads at extreme cost; in lives lost, construction, and never-ending maintenance. I've only been on the road a handful of times since we moved back to Australia (I prefer the slow but comfortable NSW TrainLink service when I can), but there's always at least one part being redirected, repaired, or resurfaced.

While trucks all have different starting points and destinations, they're all sharing this one corridor for an extended stretch of their route. A train would be a perfect mode of transport for this, especially given the bulk of this truck transport is containerised already. Yet aside from one NRMA study quoted in the Wikipedia article I linked to above, the idea hasn't received much attention.

It does strike me as short sighted. You'd save lives, reduce your ongoing maintenance bill from heavy trucks grinding your bitumen and concrete to dust, and take thousands of sooty, diesel-belching engines off the road. If building a freight line through mountains sounds difficult, ask the Swiss!

While we're at it, a high-speed train between Newcastle, Sydney, Woolongong, and Canberra would also be smashing. Wait, poor choice of words. Super? Brilliant? Why do I sound so English all of a sudden? 'gov?

*We have the technology*, we just need the will. Or Sandra, or whomever would be overseeing the construction.
