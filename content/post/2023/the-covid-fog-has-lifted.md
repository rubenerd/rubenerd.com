---
title: "The Covid fog has (mostly) lifted"
date: "2023-02-23T09:17:52+11:00"
abstract: "I was existing before, but now I can live again."
year: "2023"
category: Thoughts
tag:
- covid-19
- health
location: Sydney
---
Hi! How are you? Wow, it's weird typing in a text editor again.

My throat and head still hurt after getting [the spicy cough](https://rubenerd.com/caught-covid/), but I feel as though a mental InfiniBand cable was plugged back in last night, after subsisting on low-baud RS232 for the last week. *I can think again!*

I empathise with those of you who've talked about Covid "brain fog" now. Even when I began to physically get better this week, my brain felt sluggish. I didn't know where to begin with most tasks, and I was incapable of holding more than a few thoughts in my head at the same time. My mum used to say the same about chemotherapy.

It shows how precious a functioning mind is, and how one's entire life can change so drastically when disconnected from one. More than the painful and uncomfortable physical symptoms, it's what I found so unsettling and scary about the whole experience.

Thanks to all of you who've sent messages as well, I apologise if I haven't got to replying to yours yet.
