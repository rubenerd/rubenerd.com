---
title: "CityNerd on the necessity of urbanist comedy"
date: "2023-05-31T09:47:45+10:00"
abstract: "If you didn’t laugh you'd have to cry"
year: "2023"
category: Media
tag:
- comments
- urbanism
- youtube
location: Sydney
---
[Comment on his Power Centre video](https://www.youtube.com/watch?v=D2Pj6TcNP6Y)

> If you didn't laugh you'd have to cry
