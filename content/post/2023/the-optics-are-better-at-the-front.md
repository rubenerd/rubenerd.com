---
title: "“The optics are better at the front”"
date: "2023-03-08T15:22:18+11:00"
abstract: "The back would be too hard to see."
year: "2023"
category: Thoughts
tag:
- pointless
- work
location: Sydney
---
A colleague made the remark in the title of the post.

This makes sense. Having the optics in the back of your head wouldn't do much to improve your visibility, and would be obstructed by a skull, grey matter, and potentially a varying volume of hair and epidermis. It would also scarcely assist with peripheral vision if worn on the side, because lenses are optimised to direct light into the retina from the front. It could also potentially cause issues with hearing if the operators ears were blocked by the lenses.

Worse, opticians tend to fit glasses assuming you'll wear them on the front, which therefore could lead to a level of discomfort if worn anywhere else. An attempt by the author of this post to wear glasses backwards also resulted in damage to his dignity, before remembering he lacked a sufficient amount at the outset to lose.

The only time such inappropriate wearing of speculation equipment could be appropriate would be for a countercultural fashion statement. I'm sure Yoko Ono must have done it at some point, presumably while jumping on a squeaky purple mushroom and screaming the lyrics to an incomprehensible poem or chutney recipe that mentions the progress of satiated inevitability.

**Update:** I've been advised my colleague was referring to a house, not his head.
