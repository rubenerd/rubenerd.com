---
title: "Raising voices on video calls"
date: "2023-06-15T08:26:40+10:00"
abstract: "Research does show we compensate for drops in resolution, or higher latency."
year: "2023"
category: Internet
tag:
- audio
- covid-19
- remote-work
- work
location: Sydney
---
I've been fascinated by this global shift towards mass remote work since Covid, and how we've adapted to video conferencing calls being a normal, routine, and expected part of so many of our lives.

I wrote back in 2020 about a study that indicated that video calls place a higher cognitive overhead on participants than phone calls, and that we don't deal with latency well. I've witnessed the horror of one where Zoom's mute button didn't work, and others have responded with glee when a video conferencing service (pardon, *product!*) goes down. They've even changed the perception of work.

[Video conference fatigue](https://rubenerd.com/video-conference-call-fatigue/)   
[A video call with a failed mute button](https://rubenerd.com/a-video-call-with-a-failed-mute-button/)   
[When your customers want your service down](https://rubenerd.com/when-your-customers-want-your-service-down/)   
[Perceptions of remote work](https://rubenerd.com/perceptions-of-remote-work/)

Dr James Trujillo's research is interesting in this context, with a potential explanation for why people are more animated and louder on video calls:

> “It seems that there is a general tendency for us to put more effort into our speech and gestures when our communication is disrupted by something, such as noise or a poor video connection,” he said.
>
> “To compensate [for poorer video quality], people ‘exaggerate’ the form of their gestures in order to help their partner recognise the meaning of the gesture, even when they cannot see it as well as normal,” he said.

[The Guardian: Can you hear me now?](https://www.theguardian.com/science/2022/apr/13/voices-raised-video-calls-study-can-you-hear-me-now)

I absolutely put more emphasis on gestures like hand waves, nodding, and smiles when on video calls. I didn't even realise I was doing it at first, but even the best webcam won't have the visual resolution of physically being there. I hadn't considered that we're naturally predisposed to doing this further when resolution drops. I suppose its a similar phenomena to shouting in fog.

It's another reason why I tend to prefer in-person meetings or audio calls for most meetings, especially for engineering discussions that are heavy on technical detail. I don't need (or want) to be spending extra effort decoding what I'm seeing through a narrow pipe when I'm trying to understand something complicated. Either give me sufficient resolution where its not a problem (aka, real life), or take out the visuals altogether.

