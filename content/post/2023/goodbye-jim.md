---
title: "Goodbye Jim Kloss ♡ 1956–2023"
date: "2023-05-08T09:05:22+10:00"
abstract: "He was a mentor, a friend, and a member of the family. I’ll miss him dearly. ♡"
year: "2023"
category: Thoughts
tag:
- esther-golton
- goodbye
- jim-kloss
- whole-wheat-radio
location: Sydney
---
I'm *incredibly* awkward and nervous in photos, which was a real shame given how photogenic Jim and Esther were when we met in 2016!

<figure><p><img src="https://rubenerd.com/files/2023/jim-esther-me@1x.jpg" alt="" style="width:500px;" srcset="https://rubenerd.com/files/2023/jim-esther-me@2x.jpg 2x" /></p></figure>

Where do you being talking about one of the most important people in your life, and one of your dearest friends?

I met Jim online when he ran his independent radio station *Whole Wheat Radio*. He fascinated me from the start: here's this geologist and IT gentleman who retired to a tiny log cabin with his talented partner Esther in rural Alaska. I'd load his breakfast shows onto my iPod for listening on my morning commute, and with my sister as my witness, would burst into fits of uncontrollable laughter on the crowded Singapore train.

> I was on a **forty-five degree pitch**! I was trying not to fall down! And the bees were **flying around Neil Armstrong!** 

Over time I started contributing to his radio wiki and chat, then over emails and calls got to know the real Jim. I learned about his past life in Ohio, and how he navigated the moral concerns of his job. I respected that one could make meaningful contributions while maintaining a strong ethical core; something precious few IT people voice or consider. *What would Jim do?* is among the first questions I ask to this day before embarking on a new project.

Not to put too fine a point on it, but Jim was a source of strength during a painful and self-destructive period of my life; I quite literally wouldn't be here today without his wisdom and kindness. To anyone who'd listen, I said that *if I turned out like Jim, I know I'll be doing alright*.

He was a friend, a mentor, and someone whom I aspired to be. I'm not sure if he knew just how relieved and optimistic I always was after our chats and email exchanges. He had that uncanny ability to tell you what you needed to hear, but with compassion, silliness, and computer metaphors.

Thinking back to all the travelling Clara and I have done together over the last decade, the highlight remains hanging out with Jim and Esther in Philadelphia in 2016. I knew I'd made it when I mentioned something that had happened a few years prior, and he leaped out of his chair and said *SEE, YOU GET IT! WHY WERE YOU EVER IN DOUBT!?* 

Jim made a mark on the lives of so many people, and I'm honoured to have been among them. Thank you for all you've done for me, independent musicians, and the community that grew so effortlessly around you.

They say you can't pick your family, but I did. I love you Jim. Say hi to my mum for me, she knows about you. ♡

[Jim's website](http://jimkloss.com/)   
[Wikipedia article on Whole Wheat Radio I wrote for Jim](https://en.wikipedia.org/wiki/Whole_Wheat_Radio)   
[Music Monday from 2021: Singer Songwriter Heaven](https://rubenerd.com/wwr-singer-songwriter-heaven/)   
[Whole Wheat Radio whipped the llama's arse](https://rubenerd.com/whole-wheat-radio-whipped-the-llamas-arse/)   
[All posts tagged with Whole Wheat Radio](https://rubenerd.com/tag/whole-wheat-radio/)

*Jim was a regular reader and contributor to the blog, so out of respect for his memory I'm going to leave this as the top post for the week. I've also got some ideas about how I'm going to make something more permanent to remember him by, which I'll share when the time is right. Thank you.*
