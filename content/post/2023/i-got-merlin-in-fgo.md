---
title: "I got Merlin in FGO!"
date: "2023-07-31T08:42:24+10:00"
abstract: "The GOAT from the game I’ve been trying to get for years is now mine!"
thumb: "https://rubenerd.com/files/2023/merlin-fgo@1x.jpg"
year: "2023"
category: Anime
tag:
- fate
- fate-grand-order
- games
location: Sydney
---
Long-time readers here would know that *Fate/Grand Order* is my own mobile game vice. I got into it during Clara's and my first trip to Japan, and it's been an on-off again fixture of my life ever since. I've been tempted by the likes of *Honkai Star Rail* and *Ark Nights*, but I guess I was always predisposed to loving FGO as a *Type Mooner*.

My waifus have long been Mashu, Nero, and Summer Jeanne Alter, but I've wanted Merlin since the beginning. He is the **GOAT** for arts-heavy, defensive players, to the point where he's been a fixture of my lineup. He's also gorgeous, but that goes without saying.

I've tried summoning him for *years*. I've chosen casters for my guaranteed yearly summons, have saved quartz and splurged when he comes around, but after more than a dozen separate events... nothing. Who's counting though?

Then last night... *boom!*

<figure><p><img src="https://rubenerd.com/files/2023/merlin-fgo@1x.jpg" alt="Merlin after being summoned in the game." style="width:500px;" srcset="https://rubenerd.com/files/2023/merlin-fgo@2x.jpg 2x" /></p></figure>

I won't tell you how much better this has made me feel this morning. Aside from being a tad embarrassing, I'll be too busy levelling him up with all my hoarded materials.
