---
title: "Six months into my smartphone awareness exercise"
date: "2023-06-16T12:07:25+11:00"
abstract: "I haven’t made as much progress as wanted, but it’s a start."
year: "2023"
category: Hardware
tag:
- anxiety
- phones
location: Sydney
---
Zoomers, Millennials, and even younger members of Gen X are regularly accused of having a smartphone "addiction", and that we could simply turn them off. We're also called slackers for not having our work phone 24/7, and dismissed as being rude for not immediately answering calls. How one can receive calls on an off phone is never explained, but I suspect they never thought that far. Something to do with avocado toast.

[Time spent looking at my phone](https://rubenerd.com/time-spent-looking-at-my-phone/)

But they do have a *bit* of a point. At the start of this year, I recognised my unhealthy relationship with smartphones. I suspected they were feeding my anxiety, and were contributing to my overall reduction in attention span. I decided to be more mindful of how I use them, and to take note of when, and for what reason, they interrupted my life.

It took less than a day to appreciate just how often I reflexively reach for the phones in my pocket. It happened walking down a flight of stairs, waiting for a pedestrian crossing, standing in line in a short queue for coffee,  monitoring a file transfer... and it'd be in hand before I knew it. It's the default way I fill time.

*Fill time...* now there's a concept.

Depending on our state of mind, the absence of activities in a block of time is construed as wasted, empty, or boring. Time isn't to be savoured, it's to be "allocated". I mentioned in January that I'd become more aware of my surroundings when I stopped taking the phone out, and this realisation about time explains the impact this mental shift has had. I didn't need to *fill* time, because it was never empty in the first place.

But I shouldn't get ahead of myself! How much has my behaviour changed over three months since I started making a mental note of every time I used my phone?

The short answer is, not as much as I'd have hoped. I still notice myself reaching for the phone, constantly. It's such a rote, ingrained action that I even glanced at the phone on the table in the process of writing this sentence. I even feel phantom vibrations in my pocket for a phone that isn't there.

I've so thoroughly trained my reward centres to feel a short, sharp hit when I reach for the phone to check it, that not doing so feels uncomfortable. That feeling hasn't subsided one iota since January, suggesting just how entrenched these neural pathways have become.

With that said, I have picked up on a few things that have helped:

* Turn off *all* notifications, and only re-enable them for important things. Your phone isn't entitled to demand your attention any more than you let it.

* Put social network icons on page two. There's perhaps a bigger lesson around removing them entirely, but for better or worse they're my primary method of communication with friends and family.

* Choose websites over apps. This breaks another phone dependency, and makes using the phone for that task less appealing.

I'm disappointed in myself that I still haven't unlearned these habits half a year later. But I suppose it's a journey.
