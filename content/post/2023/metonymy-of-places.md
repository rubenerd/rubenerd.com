---
title: "Metonymy of objects and places"
date: "2023-10-23T08:25:13+11:00"
abstract: "English has this strange shorthand where we routinely reference a subset of something to refer to the whole."
year: "2023"
category: Thoughts
tag:
- language
location: Sydney
---
I was listening to an old Whole Wheat Radio stream this morning (rest in peace, Jim), and I was reminded of this strange feature of English.

We routinely reference a subset of something to refer to the whole. We might refer to a bicycle as a new set of wheels, even though it's clearly more than that. We still refer to graphics cards as GPUs, and some people even still call desktop towers CPUs, despite there being far more components involved. The *technically accurate* crowd blanch at such use, but it's common linguistic shorthand.

Generally though, is a phrase with two words. We mostly seem to apply this to places and governments. News articles will say that Tokyo and Wellington are building relations, or that Yerevan and Tblisi are working something out. In reality, these capital cities don't do anything themselves; they represent governments that are made of people.

I'll admit these sorts of articles still read really weirdly to me, even after all this time. Why am I congratulating Warsaw on an election, not Polish people? Who is Nairobi? Does Riga like coffee? It's almost a form of personification, and I often wonder of its implications. It's also funny when you start talking about city-states: does Singapore talk to Lithuania, or Vilnius? Where do you draw the line?

*(Yes, the Baltic states are on my mind again lately. Clara and I are trying to sort out our budget for travel over the next couple of years, and my mind is made up)!*

You also get into strange situations when the capital of a country isn't as well known as a larger city. Many people I've read and talk with mistake Sydney for being the capital of Australia, and Toronto as the capital of Canada. Or... are they referring to New South Wales or Ontario? They're probably mistaken, but how can you be sure?

It's actually one of the things I got a kick out of thinking about way back when I was doing OOP at university. I always thought OOP (as it was implemented in popular languages) was a hot mess of state and spaghetti code, but at least it spawned some interesting linguistic discussions! What identifier should we use for this object? Do we all agree on what it means?

I should ask *Rubenerd* what he thinks... though he's probably already distracted with something else. Mmm, Dutch coffee.
