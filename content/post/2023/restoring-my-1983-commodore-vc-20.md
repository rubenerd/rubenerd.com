---
title: "Restoring my 1983 “Aldi” Commodore VC-20"
date: "2023-02-26T12:30:58+11:00"
abstract: "I love this new colour combination, and the retrobrighting worked a treat."
thumb: "https://rubenerd.com/files/2023/commodore-vc20-cleaned@1x.jpg"
year: "2023"
category: Hardware
tag:
- commodore
- commodore-vc-20
- commodore-vic-20
- retrobright
- retrocomputing
location: Sydney
---
Earlier this month I became the [proud owner of a Commodore VC-20](https://rubenerd.com/buying-a-commodore-vc-20/), the German-badged VIC-20 that predated the famed Commodore 64. Here she was in her original condition:

<figure><p><img src="https://rubenerd.com/files/2023/commodore-vc20-top@1x.jpg" alt="Photo of the VC-20 on our coffee table, showing its black and brown keyboard, and yellowed case with blotchy white patches." style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2023/commodore-vc20-top@2x.jpg 2x" /></p></figure>

The machine powers on, with surprisingly decent composite picture quality for a forty-year old computer (that's a topic for another post). The keyboard wasn't in working order, and the case was dirty and yellowed, so while I was at home [stuck with Covid](https://rubenerd.com/the-covid-fog-has-lifted/) and could barely think or function anyway, I set about the tedious task of cleaning the machine. Turns out, it was the *perfect* activity to take my mind off things; not least the pain from having house bricks shoved up my nose.

The first step was to retrobright the case, which I did a couple of weeks ago using the same technique that worked well for my [Commodore 128](https://rubenerd.com/final-commodore-128-retr0bright-post/) and [Commodore 64C](https://rubenerd.com/i-bought-a-commodore-64c/). An afternoon in the Australian sun, and with [regular agitation and rotation](https://rubenerd.com/preventing-streaks-when-retrobrighting/), and the yellowing and blotchy patches are nearly imperceptible. Another afternoon could have got rid of them completely, but I like maintaining a little bit of the machine's history.

The broken keyboard was next. I theorised in the previous post that it wasn't an issue with the board itself, but rather the VIA chips which had clearly been serviced and replaced before. Connecting another keyboard however, and I was able to confirm all the silicon on the board worked. This was a huge relief.

I said in that first post that this machine was almost perfect for my use case, given I'd already bought a keyboard I intended to use in a "new" VIC-20 build. This board originally came from an *Aldi 64*, the Commodore 64 that the German supermarket chain sold. They use the same font, PETSCII character positions, and double-shot keycaps ons as the original VIC-20 and C64 boards, but invert the colour scheme.

I decided to go for a hybrid approach, with the function keys from the original VC-20 board, and the inverted keycaps from the Aldi 64 board. I think it looks awesome!

<figure><p><img src="https://rubenerd.com/files/2023/commodore-vc20-cleaned@1x.jpg" alt="Photo of the VC-20 on our coffee table, showing its black and brown keyboard, and yellowed case with blotchy white patches." style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2023/commodore-vc20-cleaned@2x.jpg 2x" /></p></figure>

I agree with what many of you said on Mastodon: the Commodore breadbin case looked its best in this eggshell colour scheme. I like the subtle contrast between the keys and the case as well, almost like an inverted 64G case that was slightly darker than the beige keys. And those orange function keys *pop!*

I have my Penultimate+ RAM expansion cart and software loader, so next step is to see what I can do to further improve the picture quality of the output. I'm tempted to go with the s-video mod, but even bypassing some of the circuitry to improve the composite output might be worth a try. The board in this machine has been *heavily* worked on already, so I have fewer qualms with modifying it than, say, my Plus/4 or C16 that are basically still stock.
