---
title: "A cool Adaptec AHA-1542CP ISA SCSI card"
date: "2023-05-27T11:02:11+10:00"
abstract: "It has a Zilog Z80 CPU!"
thumb: "https://rubenerd.com/files/2023/scsi-zilog@1x.jpg"
year: "2023"
category: Hardware
tag:
- am386
- retrocomputing
- scsi
- zilog-z80
location: Sydney
---
All the parts I ordered months ago for my impromptu Am386 build have stared arriving at once. Hot on the heels of the Panasonic floppy drive last week, we have another awesome new-old stock device, this time a classic 16-bit ISA SCSI controller.

[View all posts in the Am386 series](https://rubenerd.com/tag/am386/)

And here she is, in all her populated glory. No cost-cutting here!

<figure><p><img src="https://rubenerd.com/files/2023/scsi-card@1x.jpg" alt="Top-down view of the Adaptec ACA-1542CP ISA SCSI card. Read the description below for what this card has." style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2023/scsi-card@2x.jpg 2x" /></p></figure>

It's in near immaculate condition, having only been taken out of its anti-static bag by the seller to perform a functional test. If I'd time-travelled to Edpol Systems in Singapore's Funan Centre in the mid-1990s to buy this, I'd have come back with a card that looked identical.

I almost want to put it in a 3D frame instead, it's absolutely beautiful. It also has an Easter Egg I wasn't expecting, which is even more awesome.


### My background with SCSI

Before we dive into why this card is a bit unusual, maybe a bit of explanation is in order. The PCs I grew up with only had IDE drives, though by the late 1990s we used a PCI Adaptac SCSI card and an Epson GT-7000 scanner (it still amuses me that I can't remember my current work mobile number, but I can tell you the model of every electronic device I had as a kid). We also had an external HP SCSI CD-ROM that we used on laptops, but it connected via a dedicated SCSI CardBus card that, as far as I was aware, didn't work with generic SCSI devices.

Point is, I didn't really use SCSI as a storage controller at the time. It always interested me, but IDE drives were cheaper and more common in the PC world (or PC Magazine, as I'd come to read). Even our external Zip drive used a Centronics/parallel port, which was slow but ubiquitous. 

In the last few years though, I've come around to SCSI as an alternative storage mechanism for vintage computers. Most of these solutions are targeted at the Macintosh, but they work just as well on PCs with an appropriate SCSI controller. Getting legacy IDE storage working has been such a pain in the arse (though to be fair, I haven't ever touched MFM!), but thus far SCSI has *just worked*. For shiggs and gittles, I wanted to see if it'd work on a 386.


### This SCSI card

Adaptec ruled the roost when it came to PC SCSI cards. You paid extra for them for their wide OS support and excellent drivers. On DOS and Windows 9x, you could load EZ-SCSI, then whatever device driver you needed. Their SCSI Select software is also among the easier hardware config tools I've used. Contrast those with my experience getting an Advansys PCI card working, and there's no contest.

[The uncommon Advansys Iomega Jaz Jet SCSI II card](https://rubenerd.com/the-uncommon-advansys-iomega-jaz-jet-scsi-ii-card/)

The AHA-1542CP has a couple of interesting features. The most important to me was the inclusion of a BIOS ROM, meaning that devices on the card are bootable. This wasn't always a given with SCSI cards; cut down or budget versions would often have unpopulated sockets, or even empty silkscreened rectangles for where these ICs should go. My ISA Etherlink III is one such card; it has the Ethernet controller, 10BaseT and Thinnet connectors, and that's it.

Like most SCSI cards, this example also has a 4-pin LED header. If your case supported it, you could unplug the hard drive activity light from your motherboard, and use this instead. This would be useful if you were upgrading from an IDE drive.

And speaking of useful for upgrading, the most visually unusual feature has to be the floppy drive header. This might seem odd, but it was fairly common for cards at the tail end of the ISA era to consolidate features like this. Most people were sporting multi-IO cards for their IDE and floppy interfaces by the early 1990s, so if you swapped it out for a SCSI card, you would also lose that floppy interface. This SCSI card was a true drop-in replacement.

But now we come to the single coolest thing on this card, for which I was not prepared. It has a Zilog Z80 CPU, specifically a CMOS Z84C00 in a cute quad-surface mount package. What the!?

<figure><p><img src="https://rubenerd.com/files/2023/scsi-zilog@1x.jpg" alt="Close-up photo of the Zilog Z80 CPU, showing a model number of Z84C0010VEC." style="width:500px;" srcset="https://rubenerd.com/files/2023/scsi-zilog@2x.jpg 2x" /></p></figure>

[Zilog's datasheet on the Z8400/Z84C00](https://www.zilog.com/docs/z80/z8400.pdf)

The Z80 was a mainstay of 1980s microcomputers, from CP/M business machines and Sharp pocket computers, to the ZX Spectrum and the Commodore 128. I'm not sure what function its serving on this board, but it *tickles* me knowing that even my Am386 machine now has a functional Z80 inside as well. Maybe I need a "Zilog Inside" sticker to go above the AMD one.

Now I just need to build this machine into its new case, and give it a try. As it turns out, I also (accidentally) got a second-hand SCSI optical drive from Japan, which now I'm keen to try with it.
