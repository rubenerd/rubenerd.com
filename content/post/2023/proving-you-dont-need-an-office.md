---
title: "Proving you don’t need an office"
date: "2023-07-11T08:57:43+10:00"
abstract: "You didn’t need one for six months, so why do you need one now?"
year: "2023"
category: Thoughts
tag:
- remote-work
- work
location: Sydney
---
Someone I spend my life with has worked remote, every day, for the last six months. I won't name her specifically, but you can guess.

Now let's imagine, hypothetically, why this might be. Was she sick with COVID this whole time? Does she work for a forward-thinking company that recognised office space as a huge and wasteful operational expense that could be put towards salaries and extra workers? Did she get a special arrangement from her manager? Does she enjoy listening to me on client calls while she tries to listen in on hers?

If you answered *no* to all of those, you're right! So what was the real reason? That's a question with six words.

The real, *real* reason? The office has been under renovation. For six months. That's half a year, last time I checked.

This means their staff were all working remote, that entire time. The business didn't falter, their "numbers" were up, and people were productive. At what point does a manager put two and two together, and realise the office wasn't needed in the first place?

I remember during the start of COVID when we all wondered if this could become the norm, yet middle managers everywhere couldn't wait to get proverbial butts back into seats once it was viable. It makes you question the point of it.
