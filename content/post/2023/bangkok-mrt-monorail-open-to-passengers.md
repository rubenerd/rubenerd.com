---
title: "Bangkok MRT monorail open to passengers"
date: "2023-12-12T12:03:51+11:00"
abstract: "Super happy to see more investment in public transport."
year: "2023"
category: Thoughts
tag:
- bangkok
- southeast-asia
- thailand
location: Sydney
---
[Cat Vitale reported in Railway Technology](https://www.railway-technology.com/news/bangkoks-mrt-opens-monorail-to-passengers-for-the-first-time/?cf-view)\:

> French locomotive manufacturer Alstom has launched the passenger trials of its automated Innovia monorail on Bangkok’s Mass Rapid Transit (MRT) Pink Line.
> 
> Inaugurated by Srettha Thavisin, Prime Minister of Thailand on 21 November 2023 the “free public trial” will run until January.
>
> The 34.5 km Pink Line connects over 30 stations and will integrate five other rail lines.

Bangkok is an incredible city, and their [BTS Skytrain](https://en.wikipedia.org/wiki/BTS_Skytrain) was nothing short of transformative. Pardon the adjective spam spam, but it's wonderful system, and I'm glad to see more public transport modes integrate into it.

Giving people a viable alternative to driving is the *only* solution to traffic problems, and boy does Bangkok need it. I'm *stoked* to see continued investment in public transport, juast as I was to see the new MRT lines in KL.
