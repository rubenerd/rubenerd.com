---
title: "First impressions of MacOS 14 Sonoma are good"
date: "2023-11-27T07:11:32+11:00"
abstract: "They fixed all the bugs that infuriated me with Ventura."
year: "2023"
category: Software
tag:
- apple
- hololive
- macos
- ninomae-inanis
- reviews
- watson-amelia
location: Sydney
---
MacOS 13 Vista (pardon, *Ventura!*) has to be among the worst releases of Apple's desktop OS I've used in years. Repeated reinstalls and updates resulted in the same frustrating bugs and instability, to the point where I was tempted to downgrade back to 12 Monterey a month before Sonoma was due to be released.

<figure><p><img src="https://rubenerd.com/files/2023/sonoma@1x.jpg" srcset="https://rubenerd.com/files/2023/sonoma@2x.jpg 2x" style="width:500px;" /></p></figure>

I held out, took the plunge, posted some metaphors, and installed Sonoma over the weekend. It's been incredible:

* **The Bin can be emptied**. I put stuff in the Bin, press *Empty Bin*, and the files disappear instead of having an "Emptying the Bin" prompt sit there for days. I know right!? Luxury!

* **I don't have phantom mountpoints** persisting for every NFS and SMB share I've ever attached to, all with the same name in the Finder, because of course I want fifteen identical folders called *Backup*.

* **Search works in System Settings**, though it's sad Apple haven't taken the negative feedback of the new UI to heart.

* **External monitors mostly work**, not just when it feels like it. My success rate on macOS Vista was around 50%, now its 80%.

* **Wi-Fi connects** when I open the laptop lid, tethers correctly without prompting, and I swear it performs better as well.

It's *embarrassing* what a quality of life improvement this represents. Now we just need Apple to admit that a yearly cadence is unnecessary and counterproductive, and we might get some stability back!

*Holiday wallpaper by [障礙貓 on Pixiv](https://www.pixiv.net/en/artworks/86526409), from 2020. I miss Myth, I need to watch more of their streams again soon.*
