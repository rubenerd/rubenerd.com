---
title: "URL changes and password managers"
date: "2023-07-31T15:44:26+10:00"
abstract: "When websites change their URLs, they break password managers. These things were already so brittle."
year: "2023"
category: Internet
tag:
- security
location: Sydney
---
I was autofilling a passphrase of pseudorandom goodness this morning, like a gentleman, when the plugin in Firefox complained that the site didn't match any credentials on file. I knew this couldn't be true; I'd logged into this site many, many times before. So many times that I had to write many twice.

Wait, that's four total times. Many. Five. Damn it.

I logged into KeePassXC, and sure enough I could see the record for the site, so plainly demonstrated that even *I* could see it before coffee. So why wasn't it being detected? Was it a case of Monday-itis?

*Turns out*, as the title of the post already gave away, the site changed its URL... or *URI*, for those of you at the W3C, because TLAs FTW. The site had rebranded a while back after being acquired, but they finally changed the endpoint for the legacy service. My password manager, with its static addresses, was none the wiser.

I mean, I'm not the wiser half the time either, and nothing in my head is static. Being static is not intrinsic to intelligence or usefulness, unless you're making a Van de Graaff generator. When I was a kid I called those Van de Giraffes on account of their height. I know, *shocking.*

If you'll stop interrupting me, my closing thought would be that password managers are a kludge. They're at best a BandAid&trade; over an increasingly creaky authentication system that should long have been replaced, yet inertia has kept it going indefinitely. They're the IPv4 of authentication; forever destined to live in our machines because so much stuff has been written for it, and the uptake of replacements has been so poor.

This is another example of its brittleness; and not tasty brittleness like a *Violet Crumble*. Maybe it's a Cadbury *Crunchie* bar, or another lesser confectionery. Not that I eat much sugar these days anyway.
