---
title: "My Commodore 16 keyboard is fixed!"
date: "2023-08-02T08:25:59+10:00"
abstract: "A long time in the making, and it was relatively easy!"
thumb: "https://rubenerd.com/files/2023/c16-repair-02@1x.jpg"
year: "2023"
category: Hardware
tag:
- commodore
- commodore-16
- retrocomputing
- repair
location: Sydney
---
A few of months ago I broke the number 6 key on my beautiful 1984 Commdore 16, which made me feel a bit degected. In July I managed to drill out the broken stem from the keycap, and sourced a replacement stem from a donor Commodore 64 in Germany. 

[Breaking a Commodore 16 key, and retrocomputer storage](https://rubenerd.com/breaking-a-key-on-a-commodore-16/)    
[Post discussing removing the broken stem](https://rubenerd.com/a-small-step-can-be-a-catalyst/)

The replacement stem arrived, so over the weekend I set about finally fixing this keyboard. The VIC-20, C64, and C16 all share the same keyboard mechanisms, despite having subtly different layouts. This is very fortunate for C16 owners, as those other machines sold in huge quantities. Sourcing replacement parts for the Plus/4 and C128 are much more difficult.

The first step was to unscrew and open the case, disconnect the power LED and keyboard cable from the motherboard, then unscrew the keyboard plate from the top case shell. The creaking sound of the plastic suggests this was the first time this was done since this machine was manufacturered fourty years ago, which is pretty incredible. My 64C did the same thing... my VC-20 definitely did not!

<figure><p><img src="https://rubenerd.com/files/2023/c16-repair-02@1x.jpg" alt="Photo showing the relatively empty Commodore 16 shell, with the keyboard upside down below it." style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2023/c16-repair-02@2x.jpg 2x" /></p></figure>

Flipping the keyboard over shows the two primary challenges working on these machines. There are dozens of tiny screws holding the backplate to the keyboard assembly, which you'd better not lose! The second is the mechanical switch for the **Shift Lock** key, which must be desoldered to remove the brown backplate underneath. This is what the carbon pads that register keypresses are mounted.

Oh but wait, could I get away with not soldering at all? After removing the screws, the backplate had far more give than I first thought. *Gently* lifting it up from one side granted me access to the broken stem, which popped out easily. I was able to use a pair of tweezers to gently place the new stem in place, then lower the backplate into place. *Huzzah!*

<figure><p><img src="https://rubenerd.com/files/2023/c16-repair-03@1x.jpg" alt="Photo showing the backplate at an angle away from the keyboard bracket assembly. The new and broken switches are below." style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2023/c16-repair-03@2x.jpg 2x" /></p></figure>

As an aside, I thought it was interesting that the C16 key stem used pink plastic, but the replacement from the German C64 was blue. I wonder if Commodore boards from different generations or factories used different colours? They otherwise feel and look identical.

After reassembling the keyboard and screwing it back into the case with Pavolia Reine from Hololive Indonesia supervising (terima kasih), I finally took the opportunity to heat sink the TED and 8501 CPU. This is probably of limited use, but I figured why not.

<figure><p><img src="https://rubenerd.com/files/2023/c16-repair-04@1x.jpg" alt="Photo inside the C16 showing the motherboard and heat sinks." style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2023/c16-repair-04@2x.jpg 2x" /></p></figure>

It might be a bit hard to see, but note my SaRuMAN 64 KiB memory expansion under the TED. This makes the machine almost as capable as a Plus/4, save for its missing user port. The Plus/4 has the best industrial design of any 8-bit home computer, but I'll admit I far prefer using the C16's keyboard. Pundits might not have a seen a point to this tiny board in a massive breadbin case, but I love it.

I closed the case up, connected it to my test monitor via the RetroTINK 2x, and ran it through the DIAG264 cart tests. When it came time to test the keyboard, the 6 key responded! It feels *exactly* the same as every other key on the board.

<figure><p><img src="https://rubenerd.com/files/2023/c16-repair-05@1x.jpg" alt="" style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2023/c16-repair-05@2x.jpg 2x" /></p></figure>

I'm so happy this works! Now I can get to work figuring out why the reset line on this machine is unresponsive. The irony was that the BASIC codes one uses to test reset functionality *has a six in it!*
