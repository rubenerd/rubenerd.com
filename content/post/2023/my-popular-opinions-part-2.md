---
title: "My popular opinions, part two"
date: "2023-11-10T20:26:46+10:00"
abstract: "Another list of ten opinions we can all agree with."
year: "2023"
category: Thoughts
tag:
- facts
- lies
- lists
- misinformation
- popular-opinions
- socks
- trains
location: Sydney
---
It's time for another installment of *[Popular Opinions](https://rubenerd.com/tag/popular-opinions/)*. Too many people post their *unpopular* opinions, so I'm here to bring us all back together. In no particular order:

* Friendly people are more attractive.

* A weekend without plans is the best kind.

* A shocking amount of stress can be relieved by washing your face, or even splashing it with a bit of cold water.

* Misinformation is a lie in a cheap suit.

* Nobody outside government still uses the term *cyber*, and even they should probably stop.

* Sometimes it *is* worth doing the same thing twice and seeing if you get different results. The world isn't predictable.

* People who don't blast music on trains are considerate, and are probably great in other ways too.

* Fresh pairs of socks!

* It makes no sense that uncomplicated is more complicated than complicated.

* It'd be an absolute travesty if there isn't a railway podcast called *Train Torque*.
