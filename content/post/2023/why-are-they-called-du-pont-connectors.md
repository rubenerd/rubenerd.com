---
title: "Why are they called du Pont connectors?"
date: "2023-03-19T12:42:25+11:00"
abstract: "du Pont owned Berg that invented the connector, though they divested themselves of them and the name stuck."
year: "2023"
category: Hardware
tag:
- history
- language
location: Sydney
---
You know those tiny header connectors that computers use to attach power buttons and speakers to motherboards? I've always known them as *du Pont connectors*. I never really thought why, though I assumed it was for the same reason they're called *Molex connectors*. 

*Turns out*, it's a bit more complicated. [Matt Millman wrote a great post](https://www.mattmillman.com/why-do-we-call-these-dupont-connectors/) back in 2021 that goes into some history, including sourcing the identification plates on the crimping tools used to make them. He concludes:

> For most of its existence, Berg was a division of du Pont, giving us the first (albeit cryptic) clue as to the origin of this term. Adding further confusion to the picture, “Berg connector” was already an established vernacular term by that point. It must be a matter of timing.
>
> Once again, looking at serial numbers on Mini-PV crimp tool identification plates, we can see that around the time when clones seem to have first appeared, the Berg brand name is dumped, replaced with Du Pont. A historic news article tells us this change likely occurred in 1987, just before the first clones are seen.
>
> This looks to be the story. The Du Pont name was adopted because at the time they were first seen, that is the brand the closest resembling connector was sold under, and clearly people who were discussing them were aware of it ... the term should be regarded as purely vernacular as du Pont did not have a hand in the creation of the clone type connectors this now refers to.

I love it when people take the time to do deep dives like this.
