---
title: "Configuring Adaptec SCSI CD-ROMs on DOS"
date: "2023-08-28T08:20:37+10:00"
abstract: "Using EZ-SCSI and ASPI4DOS.SYS among other drivers to get a functional SCSI CD-ROM."
thumb: "https://rubenerd.com/files/2023/scsi-drive-side@1x.jpg"
year: "2023"
category: Hardware
tag:
- am386
- retrocomputing
- scsi
location: Sydney
---
I've been experimenting with various CD-ROMs in my Am386's cute but weird AT case. The vertical orientation makes traditional tray loading drives difficult to use, even ones sporting those special clips. I found a slot-loading SCSI Pioneer drive that might do the trick, so this weekend I set about configuring it.

[View all posts in the Am386 series](https://rubenerd.com/tag/am386/)   
[A cool Adaptec AHA-1542CP ISA SCSI card](https://rubenerd.com/a-cool-adaptec-aha-1542cp-isa-scsi-card/)   

<figure><p><img src="https://rubenerd.com/files/2023/scsi-drive-side@1x.jpg" alt="Photo showing the SCSI drive sitting on its side and connected." style="width:500px; height:375px;" srcset="https://rubenerd.com/files/2023/scsi-drive-side@2x.jpg 2x" /></p></figure>

### Sourcing drivers

Adaptec's site still maintains downloads for all their classic ISA SCSI cards, which is frankly incredible. This is the download page for my AHA-1542CP, but you can search to find your specific model.

[Microchip: AHA-1542](https://adaptec.com/en-us/support/_eol/isa/aha-1542/)

Once you download the drivers and extract to a floppy, you'll need to install the ones appropriate for your card. Check the included docs and note whether you need **ASPI2DOS.SYS** or **ASPI4DOS.SYS**. Worst case scenario, you can try both and see which works.

These work great for me, I've read some people having issues with these 2000-era drivers. This page on the Internet Archive has older versions you can try:

[Collection of Useful DOS Drivers](https://archive.org/details/dosdrivers)

You'll also need a copy of **MSCDEX.EXE** which you can get from most classic Windows boot disks. Extract all these to a local directory, such as **C:\DRIVERS**.


### Installing drivers

Open **CONFIG.SYS** and add these lines:

	DRIVERHIGH=C:\DRIVERS\ASPI4DOS.SYS /D /P330
	DRIVERHIGH=C:\DRIVERS\ASPICD.SYS /D:JIMKLOSS
	;;DEVICEHIGH=C:\DRIVERS\ASPIBUF.SYS

1. The **/P** switch for **ASPIxDOS.SYS** refers to the IRQ for your Adaptec SCSI card: 330h is the default, but you can see what your card uses by pressing CTRL+A when your machine boots to enter the Adaptec SCSISelect tool.

2. The **/D** switch for **ASPICD.SYS** is a unique identifier.

3. **ASPIBUF.SYS** is needed for machines with more than 16 MiB of memory. My 386 maxes out at 8 MiB, so I have it commented out.

Then load **MSCDEX** in **AUTOEXEC.BAT** with this identifier:

	LOADHIGH C:\DRIVERS\MSCDEX.EXE /D:JIMKLOSS

My personal experience is that the regular **MSCDEX** replacements we all use for IDE drives don't work with SCSI, but you may have better luck.


### Rebooting

If the drivers worked, you should see output similar to this:

	AHA-1540/1542/1640 ASPI Manager for DOS
	Version 3.36S
	Copyright 1989-1997 Adaptec, Inc.
	   
	ASPI4DOS.SYS Installation Successful
	   
	ASPI CD-ROM Driver for DOS
	Version 4.01S
	Copyright 1990-1996 Adaptec, Inc
	   
	   Host adaptor 0, Target SCSI ID = 4 - LUN 0: Pioneer CD-ROM DR-U16S  1.01
	   
	CD-ROM driver installed: 1 host adapter(s),  1 target(s)
	MSCDEX Version 2.25
	Copyright (C) Microsoft Corp. 1986-1995. All rights reserved
	       Drive D: = Driver PIONEER unit 0

From now on, the CD-ROM will behave the same as any IDE drive in DOS or Windows, albiet a bit faster if you were like me and using one of those Multi-IO cards before.
