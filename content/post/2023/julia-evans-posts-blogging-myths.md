---
title: "Julia Evans posts about blogging myths"
date: "2023-06-19T11:42:29+10:00"
abstract: "Being original, an expert, explaining every concept, and why writing can seem harder the more you do it."
year: "2023"
category: Internet
tag:
- writing
- weblog
location: Sydney
---
[Julia Evans](https://jvns.ca/blog/2023/06/05/some-blogging-myths/)

> A few years ago I gave a short talk (slides) about myths that discourage people from blogging. I was chatting with a friend about blogging the other day and it made me want to write up that talk as a blog post.

There are some great points here, including the myth that you need be original, an absolute expert, that you must explain every concept, and that page views matter. She also raises the counter-intuitive idea that writing feels *harder* as you get more experienced, with which I can relate.

It was this comment she wrote about writing "boring" posts I most appreciated:

> Also it’s hard to guess in advance what people will think is interesting, so I try to not worry too much about predicting that in advance.

And why she loves personal stories:

> I think the reason I keep writing these blog posts encouraging people to blog is that I love reading people’s personal stories about how they do stuff with computers, and I want more of them.

She also makes the case that not everyone wants to be a writer, which I should acknowledge too. I happen to think *anyone* is interesting enough to be one if they want, but it's not fun if you're forced. Just read any corporate blog.

If you've been thinking about blogging but have been put off for one reason or another, give her post a read. It might give you some food for thought.
