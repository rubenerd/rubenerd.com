---
title: "Music Monday: End of the Line"
date: "2023-08-07T08:36:00+10:00"
abstract: "Well it’s alllllll right!"
thumb: "https://rubenerd.com/files/2023/yt-UMVjToYOjbM@1x.jpg"
year: "2023"
category: Media
tag:
- the-beatles
- george-harrison
- music
- music-monday
location: Sydney
---
It's *Music Monday* time. Each and every Monday without fail, except when I fail, I share a song that has contributed audible joy to my life. I then post it on a Monday, or sometimes another day by mistake, because if there's one thing I always do, without fail, it's to sometimes do this.

[Archive of all past past Music Mondays](https://rubenerd.com/tag/music-monday/)

Clara and continue to explore our beloved *Fab Four* and their solo albums. This recently lead us to George Harrison's 1988 album *Cloud Nine*, and then onto the *Traveling Wilburies*. The supergroup comprised of George Harison, Bob Dylan, Jeff Lynne, Roy Orbison, and Tom Petty, and continued the same successful rock/blues/folk sound of *Cloud Nine*. All we could say was *wow*.

*End of the Line* was the final track on their first album, and it took me right back to my childhood in the 1990s. The empty chair and picture for Roy Orbison was a nice touch too. ♡

<figure><p><a target=_BLANK href="https://www.youtube.com/watch?v=UMVjToYOjbM" title="Play The Traveling Wilburys - End Of The Line (Official Video)"><img src="https://rubenerd.com/files/2023/yt-UMVjToYOjbM@1x.jpg" srcset="https://rubenerd.com/files/2023/yt-UMVjToYOjbM@1x.jpg 1x, https://rubenerd.com/files/2023/yt-UMVjToYOjbM@2x.jpg 2x" alt="Play The Traveling Wilburys - End Of The Line (Official Video)" style="width:500px;height:281px;" /></a></p></figure>

[The Traveling Wilburys: End of the Line (Official Video)](https://www.youtube.com/watch?v=UMVjToYOjbM)


