---
title: "What’s that FreeBSD/NetBSD/Mac/etc drive?"
date: "2023-01-12T16:31:42+11:00"
abstract: "Using gpart, fdisk, geli, and zpool to determine old partitions."
year: "2023"
category: Software
tag:
- bsd
- freebsd
- netbsd
location: Sydney
---
I feel like I've been playing a game show the last few days. I unearthed a box of old hard drives and SSDs, and have been spending time figuring out what they are so I can securely dispose of them.

From my FreeBSD tower, **gpart(8) list** will often report that it contains a GPT layout with a familiar type:

	Providers:
	1. Name: ada6p1
		[..]
   		label: 4tb-wdblue-4e0052995
   		type: freebsd-zfs

Sometimes I'm able to run **zpool(8) import** to get a list of pools the drive might have, or other times I need to run it through a **geli(8) attach** first to decrypt it. So far these all predate OpenZFS native encryption on FreeBSD.

When **gpart(8)** draws a blank that doesn't show anything, I've fallen back on **fdisk(8)** which can report on older MBR partition layouts. Here I can see this older drive has a Windows FAT32 partition which I must have used to do some sneakernetting back in the day:

	Information from DOS bootblock is:
	The data for partition 1 is:
	sysid 12 (0x0c),(DOS or Windows 95 with 32 bit FAT (LBA))

I'm fine with doing a gpart(8) destroy on drives that used encryption and be done with it, but for these other ones I'm fine running a few zero passes across with dd.
