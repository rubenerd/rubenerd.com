---
title: "The Economist fails to defend car dependency"
date: "2023-11-12T09:49:40+11:00"
abstract: "They claim sprawl is more convenient, more accessible, and better for society. Really!"
year: "2023"
category: Thoughts
tag:
- cars
- environment
- health
- urbanism
location: Sydney
---
*With thanks to [Not Just Bikes](https://www.youtube.com/channel/UC0intLFzLaudFG-xAvUEO-A) and [CityNerd](https://www.youtube.com/channel/UCfgtNfWCtsLKutY-BHzIb9Q)*.

It's been illuminating seeing car lobbies respond to the growing trend of urbanist authors, videographers, and other commentators pushing back on car-centric infrastructure being the default in cities. They're clearly rattled in their seats, though the best defences they've offered is *cars go vroom*, and *public transport riders are gross*.

The Economist has gone off the rails themselves recently, with an article fretting over World War III's potential impact on... [investors](https://www.economist.com/finance-and-economics/2023/10/30/what-a-third-world-war-would-mean-for-investors "What a third world war would mean for investors"). But their recent post takes the cake, or whatever they the ate between free-flow G&Ts at the company's London Christmas party:

> [In praise of America's car addiction](https://www.economist.com/finance-and-economics/2023/11/09/in-praise-of-americas-car-addiction). How vehicle-dependence makes the country fairer and more efficient. 

No, really! It's like being addicted to broccoli!

It actually starts on solid footing, with the journalist painting a dystopian picture of children forced to go trick-or-treating from the boots of cars. Or should I say trunks? They even let themselves admit the problem in a few places:

> This, in turn, is linked to many ills: obesity, pollution, suburban sprawl and so on. [...] Owning or renting one costs plenty of money, and is an especially big burden for the working poor. 

"And so on" carrying a lot of weight there. But then it quickly devolves into a rambling, incoherent mess, leaping from one unrelated idea to the next as though it were written by a chatbot or student who didn't read the book. They claim car dependency is more convenient, more accessible, and better for society.

If you'll forgive the analogy, let's take a look at this car wreck.


### Convenience

> Start with convenience. [...] Car-centric urban designs became dominant throughout the country, involving wide roads, ample access to expressways and parking galore. To varying degrees, other countries have copied that model. Yet America has come closest to perfecting it.

I mean, that sounds bloody awful from an environmental standpoint, let alone the steep maintenance liability that's literally [bankrupting American cities](https://www.youtube.com/watch?v=7IsMeKl-Sv0 "Not Just Bikes: Why American Cities Are Broke - The Growth Ponzi Scheme"). I've also lived in far-flung suburbia in Malaysia and Australia, and they were the exact *opposite* of convenient. Has the author never been to a mixed-use development before?

But I'm getting ahead of myself. How they defend this being convenient? Where's my Bloody Mary?

> [...] a group of economists examined road speeds in 152 countries. Unsurprisingly, wealthy countries outpace poor ones. And within the rich world, America is streets ahead: its traffic is about 27% faster than that of other members of the OECD club of mostly rich countries. Of the 20 fastest cities in the world, 19 are in America.

That's right, in the same paragraph they jump from convenience to speed. Naturally they talk about how higher speeds [kill more people](https://research.qut.edu.au/carrsq/wp-content/uploads/sites/296/2020/12/Speeding.pdf "QUT Centre for Accident Research and Road Safety") too, right? And how larger vehicles like [American light trucks and SUVs](https://www.youtube.com/watch?v=aIy5uv5-VrE "CityNerd: Problems with Pickup Trucks:") are resulting in a [steep rise in American road fatalities](https://www.iihs.org/news/detail/suvs-other-large-vehicles-often-hit-pedestrians-while-turning "IIHS: SUVs, other large vehicles often hit pedestrians while turning")? Of course not. This is journalistic malpractice.

They get *close* to a point here, emphasis added:

> One fashionable concept among urban planners these days is the “15-minute city”, the goal of building neighbourhoods that let people get to work, school and recreation **within 15 minutes by foot or bike**. Many Americans may simply fail to see the need for this innovation, for they already live in 15-minute cities, so long, that is, as they get around by car. Most of the essentials—groceries, school, restaurants, parks, doctors and more—are a quick drive away for suburbanites.

Again, I posit that living in far-flung suburbia away from amenities is less convenient than being able to walk or cycle down the road to get essentials. The 15-minute city movement is about human scale and sustainable development for all, not just for those who can drive. I'd think that's blindingly obvious, but maybe it's hard to see thanks to the limited visibility of Range Rovers.


### Accessibility

> The car's ubiquity has another rarely appreciated benefit.

Another study with a tenuous link to your point, perhaps?

> A recent study by Lucas Conwell of Yale University and colleagues examined urban regions in America and Europe. They calculated “accessibility zones”, defined as the area from which city centres can be readily reached. Although European cities have better public transport, American cities are on the whole more accessible.

Accessible... *to whom!?* People who can't drive? Like children, teenagers, the elderly, those who can't afford to drive, or those like me who don't? Aka, large swaths of the population?

Oh but see, twelve-lane freeways are good actually, because you know what else can drive on them?

> Although America could do more to improve its bus services within its urban cores, the crucial point is that cities designed for cars can also support mass transit.

Not even Shoko Miyata could reach that far without permanent injury! This is car self-justification at its most transparent.

I feel like I'm flogging a dead horse at this stage, but car infrastructure&mdash;and the suburban sprawl it perpetuates&mdash;is ruinously expensive, wastes huge amounts of land, and makes delivering mass transit far more difficult. Good public transit can't simply be tacked onto road infrastructure as an afterthought, for the same reason painted bicycle gutters don't get people out of their cars.

You know what really sucks, aside from this article? Getting off a bus [on the side of a stroad](https://www.youtube.com/watch?v=ORzNZUeUHAM "Not Just Bikes: Stroads are Ugly, Expensive, and Dangerous, and they're everywhere"), where there's nowhere to cross because the street has been optimised for speeding metal boxes that will kill me. Or what about a new train station that drops me off at a parking garage, or a field?

I suspect the journalist has never taken a train outside the London Underground, and certainly not a suburban bus or train in the US. Or the martinis were really that good, in which case I'm jealous.


### Sociology

> However, there is a more positive way of looking at this phenomenon

Oh I'm sure you'll try! Where's my scotch and water? And hold the water!

> [It] is precisely such accessibility that has put larger homes and quieter streets within reach for a remarkably wide cross-section of the country. In his analysis of the census from 2020, William Frey of the Brookings Institution, a think-tank, showed that suburbia has become far more diverse over the years. In 1990 roughly 20% of suburbanites were non-white. That rose to 30% in 2000 and 45% in 2020.

If you want to live in a quiet, detached house in the middle of nowhere, that's fine. The whole point is they're often your only choice. And as detached, badly-built suburban neighbourhoods begin to fall apart and developers move elsewhere, its the urban poor who are forced to live in them as their communities go bankrupt maintaining all the infrastructure.

When you're stuck out there, what do you have to do? You get a car. Hey, The Economist's definition of 15-minute cities! Mind those repayments, and fuel bills, and social isolation, and traffic, "and so on".

Ray Delahanty from CityNerd [refers to this](https://www.youtube.com/watch?v=YfY_durVCEU "Miami: Ultra-Livable Paradise or Car-Dependent Nightmare?") as urbanism for the rich, and sprawl for the poor. To even think you can equate greater suburban diversity with a broad improvement in living conditions is a farce, and to paint it as a success story for car dependency? Pass the Bordeaux.

What gets me about this is even motorists should be frustrated. If you love cars, or need to drive for your work, or appreciate quick ambulance trips, you want as few other people stuck in cars as possible. Everyone forced to drive is paying a tax on sprawl, and making the lives of other road users miserable. It's a negative-sum game.


### Conclusion

This whole article was a lesson in lies and statistics. By hand waving about speed, accessibility, and diversity, they attempted to paint what they admit is an "addiction" as a good thing, by saying it makes society "fairer and more efficient". They failed.

One could argue the Economist got what they wanted out of this, thanks to a click-bait title and links to their journalism (such as it as). You might be right. But we need to tackle bad ideas like this.

