---
title: "The King’s birthday in Australia"
date: "2023-06-12T10:42:03+10:00"
abstract: "French readers, any ideas?"
year: "2023"
category: Thoughts
tag:
- australia
- canada
- new-zealand
- politics
- united-kingdom
location: Sydney
---
For most of us in the Commonwealth, this is our first *King's* Birthday. This isn't true in Canada, New Zealand, or the UK, but apparently it's logically accepted to celebrate the birth of our shared monarch on entirely different days.

It's all a farce of course, but I'll take the public holiday. I'll use it to recover from this headache that started on Saturday, and to think of what we could spend the money on instead of Charles. My French readers among you, any ideas? 🇫🇷
