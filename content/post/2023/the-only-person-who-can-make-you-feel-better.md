---
title: "Sana on forgiveness and feeling better"
date: "2023-03-09T15:14:08+10:00"
abstract: "The only person who can forgive you, or make you feel better, is you."
year: "2023"
category: Thoughts
tag:
- hololive
- quotes
location: Sydney
---
Hololive-EN streamer Sana [mentioned this](https://www.youtube.com/watch?v=8l15LokJG5c) on her Pokémon stream in December 2021:

> The only person who can forgive you, or make you feel better, is you.

I [miss Sana](https://cover-corp.com/news/detail/20220712b/) a great deal. 🪐
