---
title: "Ben Sidran, Moose the Mooche"
date: "2023-01-16T08:49:35+11:00"
abstract: "Well when he got to town, all he found, was he couldn’t speak French."
thumb: "https://rubenerd.com/files/2023/yt-D60HcYHNAfE@1x.jpg"
year: "2023"
category: Media
tag:
- ben-sidran
- jazz
- music
- music-monday
location: Sydney
---
It's *[Music Monday](https://rubenerd.com/tag/music-monday/)* time! Each and every Monday, except when I don't, I share a song and tag it with *Music Monday*; hence the timing and name of the aforementioned series. Though I was tempted to call it *Tuesday Tunes*, and have it only discuss kuih flavours and my favourite pkgsrc installation from that week.

Funny story, a childhood partner in crime wanted to open a hot dog stand and call it *Just Burgers*. Jono Augustus, how are you going my friend? Did you know there's a chain of Western breakfast restaurants in Japan that bear your name? Bare? But I digress from this digression.

<figure><p><a target=_BLANK href="https://www.youtube.com/watch?v=D60HcYHNAfE" title="Play Ben Sidran - Moose The Mooch"><img src="https://rubenerd.com/files/2023/yt-D60HcYHNAfE@1x.jpg" srcset="https://rubenerd.com/files/2023/yt-D60HcYHNAfE@1x.jpg 1x, https://rubenerd.com/files/2023/yt-D60HcYHNAfE@2x.jpg 2x" alt="Play Ben Sidran - Moose The Mooch" style="width:500px;height:281px;" /></a></p></figure>

Today's *Music Monday* takes us back to a recording studio in 1978, to hear one of my favourite lyrical deliveries of any jazz song ever.

> He thought he could find a new love in Paris, France   
> He's gonna do the Paris dance!   
> 
> Well when he got to town   
> All he found   
> Was he couldn't speak French
