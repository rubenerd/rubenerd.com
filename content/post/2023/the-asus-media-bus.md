---
title: "The ASUS Media Bus"
date: "2023-08-14T17:38:20+10:00"
abstract: "VESA local bus wasn’t weird enough for you? How about a PCI and ISA card in one!"
thumb: "https://rubenerd.com/files/2023/asus-media-bus@1x.jpg"
year: "2023"
category: Hardware
tag:
- computer-buses
- isa
- pci
- retrocomputing
location: Sydney
---
Remember my letter-salad post talking about the various PC hardware bus interfaces between ISA and PCI?

[Between ISA and PCI, PCs had EISA and VLB](https://rubenerd.com/between-isa-and-pci-we-had-vlb/)

I'm surprised I didn't make a comment about TLAs and ETLAs there; ETLAs being *extended* three-letter abbreviations. Technically they're only acronyms if you pronounce them as words, like laser or COVID. Otherwise, they're initialisms. TL;DR, etc.

I was browsing eBay, like a gentleman, and chanced upon this unusual ASUS board listed in Germany. Anything look unusual?

[deoptimus: Mainboards ASUS P/I-p55t2p4 Rev 3.0 Socket 7](https://www.ebay.com.au/itm/404405639833)

<figure><p><img src="https://rubenerd.com/files/2023/asus-media-bus@1x.jpg" alt="Photo of an ASUS motherboard with a Socket 7 interface, and a shorter, brown PCI-like connector in line with one of the PCI slots" srcset="https://rubenerd.com/files/2023/asus-media-bus@2x.jpg 2x" /></p></figure>

If you look closely at the forth white PCI bus sitting alongside the first black ISA bus, you'll see it has smaller brown bus in line with it. What on Earth is it? I did some digging, and fortunately Wikipedia had an article.

[According to Wikipedia](https://en.wikipedia.org/wiki/Asus_Media_Bus)

> The Asus Media Bus is a proprietary computer bus developed by Asus, which was used on some Socket 7 motherboards in the middle 1990s. It is a combined PCI and ISA slot. It was developed to provide a cost-efficient solution to a complete multimedia system.
>
> Expansion cards designed for this interface included primarily combined audio and video cards, but also some combined SCSI and audio cards.

VESA Local Bus (VLB) worked by adding a PCI-like connector inline with ISA to extend its bandwidth for controller cards, graphics cards, and the like. It looks like ASUS did this in reverse here, only it was done to consolidate cards rather than offer additional bandwidth. I wonder why they didn't add another PCI connector inline, rather than making a hybrid PCI/ISA card? Compatibility with older software, perhaps?

This leads me to one conclusion. Somewhere out there, someone must have an ASUS board with VLB, PCI, ISA, and Media Bus. It might even have mixed DIMM and SIMM slots. If such a board exists, it would be so vanishingly rare, and so utterly pointless, that I have an irrational urge to seek it out.
