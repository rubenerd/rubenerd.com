---
title: "Desktop email, and from Alpine to Thunderbird"
date: "2023-01-20T09:01:23+11:00"
abstract: "Comparing desktop to web email, and consolidating everything into one application."
year: "2023"
category: Software
tag:
- alpine
- mail
- thunderbird
location: Sydney
---
In my experience, fewer phrases elicit as much surprise in technical circles as *desktop email client*. Even among those who still use them at work&mdash;by choice or otherwise&mdash;fewer do at home. It's akin to admitting you still use floppy disks or, perhaps a more adjacent analogy, Lotus Notes.

Web email is good enough for most people, and probably has been for a long time. You run it in most browsers and devices, you don't need to worry about syncing, and it's one fewer application to configure, upgrade, and maintain. That first one is key; email autoconfiguration *just works* sometimes, but there are enough edge cases to keep it from being as foolproof as logging into a website like Hotmail. Is that still a thing?

I'd also guess that most people don't know how email works under the hood, such as the protocols (or the fact its sent and stored in plaintext, but that's a separate issue). To them, email is another glorified social network, which also lends itself to being loaded in a browser like their social media data harvester of choice.

Shocking nobody who's dismissed me as a contrarian before, I still use desktop email clients. Even worse, I only made the switch to IMAP from POP for personal email a few years ago, in part owing to the principle of *if it ain't broke, don't fix it*.

Desktop email clients for me serve four purposes:

* They read email. Thank you, I'm here all week.

* They present a proper threaded interface for newsgroups and mailing lists. Web email clients do a middling job at best.

* They let me archive email locally, which reduces my footprint on a remote third party server somewhere. It's better for privacy, and potentially cost.

* And, at least to me, they're nicer to use.

I've used a mix of Thunderbird and Alpine for years, but I made the fateful decision last weekend to merge everything into Thunderbird and call it a day. I'll continue to recommend Alpine for those who want to live out of a terminal, but I find a high-resolution, bitmapped display with a mouse is an easier, denser interface for email tasks.

HTML email though, that's something I still refuse to budge on... because I'm a gentleman.
