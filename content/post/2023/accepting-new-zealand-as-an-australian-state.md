---
title: "Accepting New Zealand as an Australian state"
date: "2023-08-24T19:24:17+10:00"
abstract: "My only request would be to cede Australian sovereignty to Wellington instead."
year: "2023"
category: Thoughts
tag:
- australia
- new-zealand
- politics
location: Sydney
---
[The Australian Associated Press](https://www.theguardian.com/world/2023/aug/24/new-zealand-should-consider-joining-australia-mp-urges-in-valedictory-speech)

> New Zealand has been urged to reconsider membership of Australia by an outgoing MP, citing cost-saving and economic benefits.
> 
> Jamie Strange, Labour’s outgoing Hamilton East MP, offered the suggestion that Kiwis should rethink an offer to become an Australian state.
> 
> “Every time I visit Australia I often ponder the thought, ‘Will we ever become one country, Australia and New Zealand?’” he said.
> 
> “My personal view – and it’s only a personal view – is that New Zealanders shouldn’t rule that out.
>
> “In fact, technically the option remains open for New Zealand to join Australia under their constitution.

Sounds great to me. Most people think we're one country anyway, and can't tell our accents or senses of humour apart. Our ugly *handmedown* flags are barely distinguishable, and we have much common modern history. New Zealand is basically a second Tasmania, albeit with even better food and skiing... and more sheep.

As an Australian citizen, my only request would be to cede Australian sovereignty to Wellington, instead of the reverse. I for one would welcome our new Kiwi overlords, even if I were compelled to pronounce it *fush and chups*. 🇳🇿

