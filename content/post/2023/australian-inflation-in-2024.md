---
title: "Australian inflation in 2024"
date: "2023-12-21T08:26:20+11:00"
abstract: "Corporate profits, surprising nobody."
year: "2023"
category: Thoughts
tag:
- economics
- finance
- news
location: Sydney
---
*Update: Usually I mistype the previous year in the current one when a new year comes around. This may be the first time where I've mistyped the year in advance! This was supposed to be 2023.*

[Greg Jericho pulled no punches yesterday](https://www.theguardian.com/business/grogonomics/2023/dec/20/inflation-was-2023s-unavoidable-topic-will-2024-bring-a-change-of-tack-from-the-rba "Inflation was 2023’s unavoidable topic. Will 2024 bring a change of tack from the RBA? Greg Jericho")\:

> The big debate about inflation this year was what caused it. My colleagues at the Australia Institute and I analysed the national accounts to [argue that corporate profits were the main determinant](https://www.theguardian.com/business/2023/feb/24/an-economic-fairytale-australias-inflation-being-driven-by-company-profits-and-not-wages-analysis-finds "‘An economic fairytale’: Australia’s inflation being driven by company profits and not wages, analysis finds"). This accorded with similar research done by the [OECD](https://www.smh.com.au/politics/federal/corporate-profits-heat-up-inflation-oecd-20230607-p5deou.html "Corporate profits heat up inflation: OECD"), the IMF, the [European Central Bank](https://www.theguardian.com/business/2023/jun/27/corporate-profits-driving-up-prices-ecb-president-christine-lagarde "Corporate profits drove up prices last year, says ECB president") and the [US Federal Reserve](https://www.npr.org/2023/05/19/1177180972/economists-are-reconsidering-how-much-corporate-profits-drive-inflation "Economists are reconsidering how much corporate profits drive inflation").
>
> The RBA disagreed, suggesting that you can’t include mining profits because Australians don’t pay [for things produced by] miners (which might be news to everyone who has heating, cooking and electricity). The RBA prefers to believe inflation is due to households having too much money. This allows it to use the one tool it has of raising interest rates.
> 
> With luck, next year the RBA will realise that when real per capita household incomes are down by 6.6% over the past year and our living standards are back to where they were in 2014, inflation is probably not about us all feeling very flush.

I thought [on my last post](https://rubenerd.com/georgina-on-shopping-being-fun/ "Shopping, featuring Georgina") that our spending was up this year, but I checked my budget spreadsheets and it's almost entirely due to food and utilities. The difference has been stark (and you know farmers aren't getting any of that action either).

