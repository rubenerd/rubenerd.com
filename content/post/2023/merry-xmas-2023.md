---
title: "Merry Xmas 2023, and a (positive!) status update"
date: "2023-12-25T11:02:16+11:00"
year: "2023"
category: Thoughts
tag:
- family
- personal
- yuletide
location: Sydney
---
Whether you were with friends, family, or going solo, I hope you had a lovely day. If you didn't, here's to things getting better. ❤️‍🩹

I wanted to thank so many of you who messaged in response to my [recent post about my mum](https://rubenerd.com/remembering-our-mum-with-family-and-cake/ "Remembering our mum with family and cake") as well. You're all wonderful.

*This post was shamelessly written on Boxing Day, but I'm backdating it for the 25th. It's still that day on half the planet, so that counts, right?*

<figure><p><img src="https://rubenerd.com/files/uploads/media.tree-glitter.gif" alt="An old stock image Xmas tree I've used for years" style="image-rendering: pixelated;" /></p></figure>
