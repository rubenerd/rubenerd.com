---
title: "Peter Mulvey’s YouTube Channel"
date: "2023-03-13T08:35:31+11:00"
abstract: "I’ve been binging his music again since last week."
thumb: "https://rubenerd.com/files/2023/yt-poAGAaDc-gU@1x.jpg"
year: "2023"
category: Media
tag:
- music
- music-monday
location: Sydney
contributer:
- https://www.youtube.com/@PeterMulveyMusic43/videos
---
Today's [Music Monday](https://rubenerd.com/tag/music-monday) is a bit of a  confession. One of my favourite folk singer/songwriters, poets, and hat wearers has a [YouTube channel](https://www.youtube.com/@PeterMulveyMusic43/videos), and I've been, as the kids say, *binging* his videos since last week.

Here's a song he released last year called *Green and Grey.*

<figure><p><a target=_BLANK href="https://www.youtube.com/watch?v=poAGAaDc-gU" title="Play New Song: Green and Grey"><img src="https://rubenerd.com/files/2023/yt-poAGAaDc-gU@1x.jpg" srcset="https://rubenerd.com/files/2023/yt-poAGAaDc-gU@1x.jpg 1x, https://rubenerd.com/files/2023/yt-poAGAaDc-gU@2x.jpg 2x" alt="Play New Song: Green and Grey" style="width:500px;height:375px;" /></a></p></figure>

