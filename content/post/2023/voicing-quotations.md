---
title: "Voicing quotation marks"
date: "2023-08-30T22:39:43+10:00"
abstract: "We speak quotes the way they’re written, but Americans say quote unquote at the start."
year: "2023"
category: Thoughts
tag:
- australia
- canada
- commonwealth
- english
- language
- linguistics
- united-states
location: Sydney
---
I only noticed this difference recently. In the Commonwealth, we voice quote marks like this this:

> She said she'd **quote** get it done **unquote**.

This is the same way it's written: "get it done".

But Americans say it like this:

> She said she'd **quote unquote** get it done.

This would be written as “”Get it done, which makes no sense on the surface. But rather than use the quotation marks as bookends, Americans use intonation in their voice to signal when the quote ends.

I wonder when that split happened, and why?
