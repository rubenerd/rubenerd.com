---
title: "A new sign in was detected"
date: "2023-01-12T16:38:02+11:00"
abstract: "Every day, a dozen of these emails!"
year: "2023"
category: Internet
tag:
- email
- privacy
- spam
- tracking
location: Sydney
---
Last year I asked if [anyone logs out of sites anymore](https://rubenerd.com/do-you-log-out-of-sites/), because every time I log back into a service I'm spammed with emails like this:

> A new sign-in on Mac: We noticed a new sign-in to your Google Account on a Mac device. If this was you, you don’t need to do anything. If not, we’ll help you secure your account.

Then in a separate email:

> Thanks for signing into Google on your Mac device. Please confirm your Google Account settings are still right for you.

Some other services send upwards of four. Every single time. I'm flattered that they think I'm worthy of that attention, but I fear it's misplaced! And counter-productively, it [reduces security](https://www.atlassian.com/incident-management/on-call/alert-fatigue#What-is-alert-fatigue?).

I suspect modern sites consider it unusual behaviour to log out, or not have their cookies. If you run your browser in private mode, or use security extensions that periodically clear your cookies, or are using VPNs for certain tasks, you know what I'm talking about.

There's a security case for notifying people when a login is occurring in an environment the service doesn't recognise, but I wish there was a bit of basic intelligence behind this heuristic. If this person appears to come from a different environment *every time*, then that behavior is normal for that account.
