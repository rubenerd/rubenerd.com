---
title: "My professor on unhelpful advice"
date: "2023-03-22T18:48:43+11:00"
abstract: "Those who tell a drowning person they lack oxygen aren't half as helpful as they think they are."
year: "2023"
category: Thoughts
tag:
- advice
- quotes
- philosophy
location: Sydney
---
I found this quote among some old notes. It's *Reply Guys* in a nutshell:

> Those who tell a drowning person they lack oxygen aren't half as helpful as they think they are.
