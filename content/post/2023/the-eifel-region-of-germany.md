---
title: "The Eifel region of Germany"
date: "2023-12-07T14:36:06+11:00"
abstract: "Discussing the region of Germany that looks beautiful."
thumb: "https://rubenerd.com/files/2023/elz@1x.jpg"
year: "2023"
category: Travel
tag:
- architecture
- europe
- germany
- photos
location: Sydney
---
[@Mirabilos shared](https://toot.mirbsd.org/@mirabilos/statuses/01HH0Q0EQEVX5S35YKDRX498MK "I don’t want to head east though, it’s nicest on the west bank of the Rhein, in the Eifel region") a desire to go to the Eifel region of western Germany, which sent me down the rabbit hole over lunch of [looking at photos](https://commons.wikimedia.org/wiki/Category:Eifel "Wikimedia Commons category of photos").

All I can say is *wow*. Well, clearly not, I've managed to write a whole post, shaddup. There are some gorgeous looking towns, mountains, and lakes, but I found myself staring at this photo of the Elz Waterfall for the longest time. It's so serene, it might just be my desktop background after the holidays.

<figure><p><img src="https://rubenerd.com/files/2023/elz@1x.jpg" alt="Photo of the Elz Waterfall showing a beautiful lake with a small log traversing it, and some traditional architecture in the background." srcset="https://rubenerd.com/files/2023/elz@2x.jpg 2x" /></p></figure>

<span xml:lang="de" lang="de" style="font-style:italic;">Danke schön</span>, <a title="Photo by Gisele871 on Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:20220428131826_522A8183_(1).jpg">Gisele871</a> and @Mirabilos!
