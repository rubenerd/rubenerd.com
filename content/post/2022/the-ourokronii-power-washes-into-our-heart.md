---
title: "The @ourokronii power washes into our hearts"
date: "2022-03-19T22:46:45+11:00"
abstract: "I’m going to add this stream to my recommended-for-newbies list :)"
thumb: "https://rubenerd.com/files/2022/kronii-thumb.jpg"
year: "2022"
category: Media
tag:
- hololive
- ouro-kronii
location: Sydney
---
Clara and I needed something chill to have on tonight, and [this recent video](https://www.youtube.com/watch?v=kay32mA4eUc) by Kronii absolutely hit the spot.

<p><a href="https://www.youtube.com/watch?v=kay32mA4eUc" title="Play 【Powerwash Simulator】Brace Yourself, Filth"><img src="https://rubenerd.com/files/2022/powerwash-simulator-kronii@1x.jpg" srcset="https://rubenerd.com/files/2022/powerwash-simulator-kronii@1x.jpg 1x, https://rubenerd.com/files/2022/powerwash-simulator-kronii@2x.jpg 2x" alt="Play 【Powerwash Simulator】" style="width:500px;height:281px;" /></a></p>

If you'd like a gentle, relaxing, and slightly silly introduction to Hololove, watching everyone's other favourite timelord power wash a subway station may be just what the doctor ordered. She even got into a discussion with her live chat about classic anime and manga again.

I'd love to rewatch *Angelic Layer* too!
