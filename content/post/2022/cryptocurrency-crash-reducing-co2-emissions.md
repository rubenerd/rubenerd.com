---
title: "Cryptocurrency crash reducing CO₂ emissions"
date: "2022-06-15T11:13:53+1000"
abstract: "Some good news from Digiconomist. Let’s make efficiency a metric again."
year: "2022"
category: Software
tag:
- bitcoin
- cryptocurrency
- environment
- scams
location: Sydney
---
Here's some [great news from Digiconomist](https://twitter.com/DigiEconomist/status/1536614407580569600), even if the projection is optimistic:

> As the Bitcoin/Ethereum crash continues, the combined reduction in energy consumption of these networks now equals that of a country like Austria
>
> The reduction in global carbon emissions could be 110,000 metric tons of CO2 per day - almost as much as global CO2 reductions by EVs.

Let's make efficiency a metric again.
