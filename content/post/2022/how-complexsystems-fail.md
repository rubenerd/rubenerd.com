---
title: "how.complexsystems.fail"
date: "2022-09-26T08:51:27+10:00"
abstract: "Because overt failure requires multiple faults, there is no isolated ‘cause’ of an accident."
year: "2022"
category: Software
tag:
- design
- security
location: Sydney
---
[This is such a great webpage](https://how.complexsystems.fail), especially in light of the adventures Optus and their customers are having right now. This is my favourite point, because even the tech press routinely get this wrong:

> Because overt failure requires multiple faults, **there is no isolated ‘cause’ of an accident**. There are multiple contributors to accidents. Each of these is necessarily insufficient in itself to create an accident. Only jointly are these causes sufficient to create an accident. Indeed, it is the linking of these causes together that creates the circumstances required for the accident. Thus, no isolation of the ‘root cause’ of an accident is possible. The evaluations based on such reasoning as ‘root cause’ do not reflect a technical understanding of the nature of failure but rather the social, cultural need to blame specific, localized forces or events for outcomes.

See also: James T. Reason's [Swiss cheese model of accident causation](https://en.wikipedia.org/wiki/Swiss_cheese_model). You can blame the human involved in the last slice, but you're neglecting the rest of the stack that should never have let such a failure result in a catastrophe.
