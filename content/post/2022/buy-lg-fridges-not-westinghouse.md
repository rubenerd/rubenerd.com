---
title: "Buy LG fridges, not Westinghouse"
date: "2022-04-08T08:16:44+10:00"
abstract: "A lesson in ridiculous compressor noise."
thumb: "https://rubenerd.com/files/2022/fridge_lg@1x.jpg"
year: "2022"
category: Hardware
tag:
- appliances
- home
- quality
- life
location: Sydney
---
Clara and I decided the time was right for a new fridge. We'd been using the same glorified bar fridge from our first studio apartment we lived in together, which didn't have a freezer so much as a drawer that got a bit colder. There was a lot of nostalgia tied up in that tiny little box, but it hadn't been serving us well for a while.

We did some quick research&mdash;*whoops!*&mdash;and decided a <a rel="nofollow" href="https://www.westinghouse.com.au/fridges-and-freezers/fridges/wtb3400wh/">Westinghouse WTB3400WH</a> fit our requirements and budget. Did I mention *whoops?* We had it delivered, and the Appliances Online team carted away the old fridge for free.

The fun started within minutes of turning it on. This fridge was *loud*... easily the noisiest I've ever used. The compressor rattled each time it kicked in, and its shrill, high-pitched whine managed to travel to all corners of our tiny apartment. As I complained on social media at the time, the tone hit *just* the right frequency to make any headache much, much worse. We could even hear it when we tried to sleep.

Conventional wisdom is to leave fridges for a few days while they settle after shipment, but after four days it was only getting louder. The food inside was still cold, so I didn't *think* the compressor was broken, it was just... if I can use the technical terminology... rubbish.

After a few days of this, we started unplugging it when we were at home, defeating its entire purpose! I began documenting when the compressor would kick in by filming it on my phone, and using a decibel meter on the phone to roughly compare ambient noise to what the fridge was putting out. I expected to be told that it's functional if it still cools things, so I wanted as much evidence as I could to make a case.

I called up Appliances Online the following Monday and described the problems, and they were happy to ship us a new one without any questions. I [agree with Gamers Nexus](https://www.youtube.com/watch?v=CL-eB_Bv5Ik "Newegg's Shocking Incompetence"), how a company handles returns and RMAs makes a big impression on who I'll buy from in the future. They were excellent!

<p><img src="https://rubenerd.com/files/2022/fridge_lg@1x.jpg" srcset="https://rubenerd.com/files/2022/fridge_lg@1x.jpg 1x, https://rubenerd.com/files/2022/fridge_lg@2x.jpg 2x" alt="Photo of our new fridge showing the magnets that are so integral to the fridge's operation" style="width:500px; height:333px;" /></p>

We paid the extra price and got an [LG delivered](https://www.lg.com/au/fridges/lg-gb-335pl). The difference in built quality was stark; the shelves didn't rattle in place, the door seals were noticeably stronger, and it was rigid when moving it around. It was also rated as being more energy efficient, which will pay off long term.

The biggest life improvement though was how *whisper* quiet it is. I was worried it wasn't working until I opened the door and was hit in the face with cool air. Not once have I heard its compressor kick in, even when standing right next to us in our tiny kitchen.

I've since done more research into this, and online reviews for Westinghouse units regularly mention how noisy their modern fridges are. That might not be a problem if you have a large house or a separate kitchen, but spare yourself and your ears if you live in a small space.
