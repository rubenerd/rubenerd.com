---
title: "FreeBSD 12.4-RELEASE available"
date: "2022-12-07T08:30:45+11:00"
abstract: "Multithreaded if_epair(4) and other improvements."
thumb: "https://rubenerd.com/files/2020/beastie@2x.png"
year: "2022"
category: Software
tag:
- bsd
- freebsd
- networking
location: Sydney
---
If you're on the 12.x branch, get it while it's hot! From the [release announcement](https://www.freebsd.org/releases/12.4R/announce/) page:

> The FreeBSD Release Engineering Team is pleased to announce the availability of FreeBSD 12.4-RELEASE. This is the fifth release of the stable/12 branch.
>
> For a complete list of new features and known problems, please see the online [release notes](https://www.freebsd.org/releases/12.4R/relnotes/) and [errata list](https://www.freebsd.org/releases/12.4R/errata/).

Of note to me is that **if_epair(4)** can now use multiple threads, which should help in jailed environments.

Thanks to the release engineering team :).
