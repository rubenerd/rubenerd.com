---
title: "Are DisplayPort or HDMI KVMs any good?"
date: "2022-03-07T18:55:08+11:00"
abstract: "Using a desktop and MacBook Pro using the same screen, keyboard, and mouse."
year: "2022"
category: Hardware
tag:
- displayport
- hdmi
- kvm
location: Sydney
---
I still haven't got around to my most recent feedback and emails from some of you, for which I apologise! So let's solicit even more.

My recent FreeBSD workstation/Linux game machine upgrade has meant I've been able to move the last of my personal stuff off my nominally "work" MacBook Pro. This formal separation across computers and phones has been great for my anxiety, and given me licence to partake in [distro hopping](https://rubenerd.com/trying-and-liking-kde-neon/) and other silliness.

There's just one issue: is a phrase with four words. Right now I have to do the USB shuffle each evening between the laptop and the workstation with the one 4K display, keyboard, and mouse. I'm thinking a KVM might help alleviate this first-world problem.

The ATEN 4-port VGA KVM box I have for my Pentium 1, AGP Compaq desktop, docked Toshiba Libretto, and the upscaler for my Commodore 128, Plus/4 and 16 works great, with a button selector to choose which machine I'm using. It's somewhat redundant for the latter, as the Commodore machines have their own keyboard and mouse, but easily swapping video output on the one 15-inch VGA LCD still makes things easier. There's no way I'd have space to use this gear in our tiny apartment otherwise.

There are a few ATEN and StarTech boxes that purport to support 4K at 60 Hz, but I've held off because of mixed reviews. Enough people say the display cutover is intermittent, regardless of HDMI or DisplayPort. The biggest issue seems to be Macs refusing to wake up from sleep sometimes, perhaps because the KVM isn't sending the appropriate signal.

Anyone have any real world experience or advice?
