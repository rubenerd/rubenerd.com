---
title: "Happy 80th, Paul McCartney!"
date: "2022-06-20T08:29:45+10:00"
abstract: "Haaaaaaaands across the water!"
thumb: "https://rubenerd.com/files/2022/mccartney@1x.jpg"
year: "2022"
category: Media
tag:
- the-beatles
- music
- paul-mccartney
location: Sydney
---
Clara and I have been diving headfirst into Paul's discography of late, and now I learn the chap just hit the big 80 yesterday.

I have a lot of memories tied up with Beatles music, and especially Paul's and George's. There's were the soundtrack to my childhood, though their creative output was so vast I'm still learning about albums I never heard before.

<figure><p><img src="https://rubenerd.com/files/2022/mccartney@1x.jpg" alt="LP covers for McCartney, McCartney II, and McCartney III... with Clara's bear cat" style="width:500px; height:333px;" srcset="https://rubenerd.com/files/mccartney@1x.jpg 1x, https://rubenerd.com/files/2022/mccartney@2x.jpg 2x" /></p></figure>

Right now his self-titled albums are on regular rotation on our linear tracking, quartz locked, direct drive turntable, and his *Ram* and *Memory Almost Full* albums are piped through our Sony Discman to the amplifier. It's become the highlight of our day choosing which to listen to, and sing along with smiles as we dig in our shared Minecraft world.

I'm realising as I get older how valuable it is to have things in your life that make you happy, and to thank the creators responsible for imparting that joy. Paul will unlikely ever read this, but on the off chance, thank you :) ♡.

