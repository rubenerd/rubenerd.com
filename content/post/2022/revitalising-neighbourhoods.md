---
title: "Revitalising neighbourhoods"
date: "2022-02-07T10:31:23+11:00"
abstract: "A post by Jon Udell about post-lockdown community changes has some interesting observations "
year: "2022"
category: Thoughts
tag:
- design
- urbanism
location: Sydney
---
Jon Udell [wrote a post](https://blog.jonudell.net/2022/01/30/life-in-the-neighborhood/ "Life in the neighborhood") at the end of January talking about another effect lockdowns and Covid have had:

> I realized that commuter culture had, for several generations, sucked the daytime life out of neighborhoods. What we initially called telecommuting wasn’t just a way to save time, reduce stress, and burn less fossil fuel. It held the promise of restoring that daytime life.
>
> All this came back to me powerfully at the height of the pandemic lockdown. Walking around the neighborhood on a weekday afternoon I’d see families hanging out, kids playing, parents working on landscaping projects and tinkering in garages, neighbors talking to one another. This was even better than my experience in the 2000s because more people shared it.
>
> Let’s hold that thought. Even if many return to offices on some days of the week, I believe and hope that we’ve normalized working from home on other days. By inhabiting our neighborhoods more fully on weekdays, we can perhaps begin to repair a social fabric frayed by generations of commuter culture.

Save for a few years in my childhood that I barely remember, I've usually only lived in apartment buildings in what my American friends would call downtown areas. But I can imagine those neighbourhoods would feel more alive with people treating them as places to live, rather than the terminating stop on a commute route. That can only be a healthy thing for a community.

*(It's probably why I've long found the idea of living in suburbia so depressing. Their environmental impact aside, they just feel so disconnected and isolated).*

It's been interesting witnessing this in Chatswood. The area is one of Sydney's commercial and high-density residential suburbs, physically split by a commuter train line. I've felt little difference living on the residential and shopping side, but the business district has felt like a ghost town since mid-2020. It's clear people were commuting to these skyscrapers from other areas, and those works are now all WFH. I walk around the area for evening exercise, and there are scant few lights on anywhere.

Life snapped back to normal quickly after Australia's first set of lockdowns, but I feel like there's been a shift in the last twelve months as Omicron became endemic. Even people who've since returned to offices only do so part time, and with the understanding that home is the default.

It raises some interesting questions. Will we get to the point where we're repurposing business parks? Will councils acknowledge that mixed-use zoning has become the *de facto* standard, and work to integrate the needs of remote workers into their plans? Will public transport be seen as a general public good, and not a funnel for commuters wishing to all go to one place?

This also dovetails nicely with [ideas like this](https://rubenerd.com/city-beautiful-on-the-need-for-corner-stores/ "City Beautiful on the need for corner stores") for more local shopping.
