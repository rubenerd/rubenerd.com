---
title: "Thinking about AMD’s 7000 series CPUs"
date: "2022-10-08T07:48:23+11:00"
abstract: "They’re more efficient compared to previous generation CPUs, but you can’t escape the higher power draw."
year: "2022"
category: Hardware
tag:
- amd
- cpus
- energy
- engineering
- environment
- intel
location: Sydney
---
Now that we're getting more reviews and benchmarks of AMD's latest CPU generation, my thoughts around the tech and what it represents are less clear cut than before.

On the one hand we have a more efficient CPU that takes the performance crown from Intel, as much as there's value in that. It can also deliver the same performance across all its cores, instead of splitting workloads across performance and efficiency cores like Intel and Apple Silicon. That's no small feat; it's actually microscopic at 5nm. Mmm, that's quality humour.

But that performance improvement comes with costs that those us in the AMD camp aren't as familiar with... at least, not since the [Bulldozer days](https://web.archive.org/web/20120113020207/http://www.xbitlabs.com/articles/cpu/display/amd-fx-8150_13.html#sect0). These chips are thirstier, hotter, and start at a higher cost base.

Reviewers praised the previous generation Ryzen CPUs for their relatively [low power draw](https://www.pcmag.com/reviews/amd-ryzen-9-5950x) for their performance, something [Intel far surpassed](https://www.hardwaretimes.com/intel-12th-gen-alder-lake-cpus-increase-peak-power-consumption-to-468w-15-more-than-11th-gen-rocket-lake/) to beat AMD with their 12-gen chips. I used to joke that nobody had done more to sell Noctua tower coolers, AIOs, and CPU frames than Intel with their hot silicon. These new Ryzen chips now have the potential to draw even more, and are designed to operate at their thermal limit from the start.


### It's more power, but it's more efficient. Right?

This is defended on the basis of efficiency. These chips do more work per watt than their predecessors, so it's not as though we're going back to the days of the Pentium 4 or PowerMac G5, where performance wasn't scaling with increased power and heat. Put another way, you could think of it as a chip using equivalent power as a previous CPU, but in a compressed timeframe to complete work faster.

That's true, but one can't ignore that increased power requirement, which has to come from somewhere. [Steve from GamersNexus](https://www.youtube.com/watch?v=nRaJXZMOMPU) summarised my thoughts:

> Now all the parts are going to very high power consumption. [&hellip;] Efficiency becomes irrelevant when all you care about is absolute power draw because that will affect your power supply choice and your cooling.

Not to mention noise and dust, especially if you live in a hot, humid, and/or arid climate.


### The bang for buck

Building a computer has always been a balancing act between where to allocate funds and power. My tower uses an entry-level Ryzen 5600X, because it sips power like a gentleman, performs well on the single-threaded and poorly-optimised games I play, and it let me allocate money and power to a hot and expensive RTX 3070. A Ryzen 7000 series chip with a modern GPU will have an even greater power ceiling, and will generate even more heat.

This also has implications for cost. DDR5 and Ryzen 7000 compatible motherboards are expensive, the entry-level 7600X starts the family from a higher price than before, and its thermal design even precludes a cooler being included. Someone bootstrapping a machine with the intent to upgrade will need a much larger power supply and cooling system from the outset, which will price out more people. That sucks.

The machine I'm typing on now will likely remain with these basket of parts for the foreseeable future. I might go down the avenue of a 5800X3D, but as it stands I like the balance of performance, power, and heat that I currently have.
