---
title: "Daniel Pennac on learning for fun"
date: "2022-02-22T08:09:17+11:00"
abstract: "“Making students learn techniques and write essays, while proscribing treading for pleasure.”"
year: "2022"
category: Thoughts
tag:
- education
- philosophy
- work
location: Sydney
---
I haven't been able to stop thinking about [this quote](https://en.wikiquote.org/wiki/Daniel_Pennac) by French author Daniel Pennac:

> Schools everywhere have always confined themselves to making students learn techniques and write essays, while proscribing treading for pleasure. It seems to be established in perpetuity, in every part of the world, that enjoyment has no part to play in the curriculum, and that knowledge can only be the fruit of suffering.

Everyone has that book, play, or even subject that school ruined for them. For me it was Catch-22, To Kill a Mockingbird, and any time the Bard waved a pen in the vicinity of paper, parchment, or whichever. Maybe I need to revisit them when I can enjoy and learn from them on my terms, rather than smash an exam.

There are great teachers, but they'd be the first to admit that the system is still designed to force information, not nurture exploration.

I think this also raises our perverse societal conditioning to see being stressed, overworked, and perma-tired as being virtuous, and the corollary that those who aren't are lazy or hedonistic.
