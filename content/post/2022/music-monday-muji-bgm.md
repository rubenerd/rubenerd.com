---
title: "Music  Monday: Muji BGM"
date: "2022-07-04T14:18:19+10:00"
abstract: "You can buy CDs from their homeware stores now"
year: "2022"
category: Media
tag:
- music
- music-monday
location: Sydney
---
Today's [Music Monday](https://rubenerd.com/tag/music-monday/) takes place in the Japanese Muji homeware store.

The background music they play has always been the best part of shopping there, and now you can [bring it home](https://www.muji.com/au/products/list/Furniture%20and%20Interior+Home%20Appliances%20and%20Lighting+Audio). Or rip it and put on your [new Sony Walkman](https://rubenerd.com/my-new-sony-nw-a55-walkman/).

It's all mastered so well; the tracks are clear, bright, and cheerful. Clara and I have had [BGM9](https://www.muji.com/au/products/cmdty/detail/4548076769484) from Italy on a loop while we work from home today. Add in one of their oil diffusers and some indirect lamp light during this stormy afternoon, and *bagus*. Wait, wrong language.
