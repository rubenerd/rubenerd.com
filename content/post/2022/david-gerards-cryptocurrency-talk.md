---
title: "David Gerard’s cryptocurrency talk"
date: "2022-09-29T08:38:35+10:00"
abstract: "“Don't worry about the tech, look at the financial instrument, and the people and the flows of cash.”"
year: "2022"
category: Internet
tag:
- cryptocurrency
- scams
location: Sydney
---
David Gerard of [Attack of the Fifty Foot Blockchain](https://davidgerard.co.uk/blockchain/) fame [attended a conference](https://davidgerard.co.uk/blockchain/2022/09/22/nasaa-nashville-protecting-investors-and-consumers-in-the-age-of-digital-assets/) in Nashville last week, where he talked with senior-level American state and provincial regulators about this <span style="text-decoration:line-through">brave</span> new world in which we live.

I thought this was a helpful reminder next time you get bogged down talking about the utility of someone's chosen cryptocurrency:

> My bit was my basic stuff: don't worry about the tech, look at the financial instrument, and the people and the flows of cash.

He also offered a bit of advice:

> The general lesson for innovative financial entrepreneurs is: if you rip off enough of the public, you don't get just the SEC after you, but sixty state and provincial regulators as well. Celsius and Voyager have met that bar.

