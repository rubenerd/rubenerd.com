---
title: "Go Jazz Nakano Miku BlueSCSI, in a box!"
date: "2022-02-21T14:27:07+11:00"
abstract: "Our latest Japanese proxy service haul."
thumb: "https://rubenerd.com/files/2022/proxy-service@1x.jpg"
year: "2022"
category: Media
tag:
- ben-sidran
- japan
- jazz
- music
- quintessential-quintuplets
location: Sydney
---
Clara and I send our Japanese wares to the same proxy shipping account, so we can pool our stuff and get it sent in the same box back to Australia. It's like receiving a present from our past selves every few months. Thanks, past Ruben!

<p><img src="https://rubenerd.com/files/2022/proxy-service@1x.jpg" srcset="https://rubenerd.com/files/2022/proxy-service@1x.jpg 1x, https://rubenerd.com/files/2022/proxy-service@2x.jpg 2x" alt="A photo of our latest wares." style="width:500px; height:333px;" /></p>

From left to right we have:

* An immaculately-assembled [BlueSCSI](https://scsi.blue/) desktop card by [infinity5750](https://auctions.yahoo.co.jp/seller/infinity5750), which permits attaching SD cards to an old SCSI interface. It even came with some cool stickers!

* A Go Jazz compilation album from 1991. This was the label Ben Sidran started in the 1980s, but I've never been able to track down any of their compilations. It has tunes from Ben, Georgie Fame, *and* Bob Malach!

* A cute Nakano Miku acrylic stand, my favourite character from the *Quintessential Quintuplets*. She's taller than we expected, but flatter than a fig, which for our shelves is a good thing.

* A *Cool Sounds from the Go Jazz Vault* released much more recently in 2007 for the Tullys coffee chain in Japan. We've got Ben Sidran, the Bob Rockwell Quartet, David Hazeltine, and Bill Carrothers.

Clara and I like to think we're at the Tullys in Arashiyma having coffee right now and listening to these CDs on the sound system. Miku is an audiophile, and I'm sure there's an old sound system out there with SCSI.
