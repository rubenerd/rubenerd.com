---
title: "Cryptocurrency doesn’t solve fraud"
date: "2022-05-21T13:34:19+1000"
abstract: "Nor are technical solutions to meatspace problems always viable."
year: "2022"
category: Software
tag:
- bitcoun
- cryptocurrency
- ethics
location: Sydney
---
I'm relieved to see more people seeing the [emperor naked](https://en.wikipedia.org/wiki/The_Emperor%27s_New_Clothes) under his new cryptocurrency clothing, but proponents still point to issues with current systems and claim it's a superior solution. This is a slight of hand.

I rewatched [The Problem with NFTs](https://www.youtube.com/watch?v=YQ_xWvX1n9g) by Folding Ideas again last week, and Dan addresses this point in part one on Bitcoin; emphasis added:

> As far as banking is concerned, Bitcoin was never designed to solve the actual problems created by the banking industry, only to be the new medium by which they operated. The principal offering wasn't revolution, but a changing of the guard. The gripe is not with the outcomes of [the 2008 financial crisis], but the fact you had to be well-connected in order to get in on the grift in 2006.
> 
> Cryptocurrency does nothing to address 99% of the problems with the banking industry, because **those problems are patterns of behaviour**.

He comes back to this point about the purported protections offered by blockchains in part three:

> In the pantheon of fraud, man-in-the-middle attacks [blockchains prevent] are rare &hellip; the majority of fraud doesn't come from altering information as it passes between parties, rather from colluding parties entering bad information from the start. Con artists don't *hack the Gibson* to transfer your funds to their offshore accounts, they convince you to give them your password.

It's a combination of *garbage in, garbage out*, and [this classic xkcd](https://xkcd.com/538/) about security.

You wouldn't need to scroll far in my archives to see my views of the global financial system. But the greed, deception, and lies told by people in these systems aren't addressed by this new generation of tokens representing wasted power. In fact, these fraudsters that bankrupted American families and retirees to gamble on speculative assets in which people live are now *heavily* into cryptocurrency themselves. Why would this be?

Realising that technical solutions aren't always the answer to human problems has been the biggest (and frankly most shocking) challenge to my world view since I hit my thirties, and I'm still coping with the fallout.

I empathise with starry-eyed nerds who thinks immutable ledgers and code will solve or route around issues in the global financial system. Cryptography has done wonders to secure modern communications and give people living under repressed regimes a voice. I can *feel* the heady optimism that such a similar system could be cleanly and meaningfully applied to finance. 

But, and this cannot be overstressed, it can't. Not while behaviour can and does exist for which such a ledger cannot account for.... which turns out to be almost everything. At that point, a tamper-proof ledger is the least of your concerns.
