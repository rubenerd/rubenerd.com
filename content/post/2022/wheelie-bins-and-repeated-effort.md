---
title: "Suburban wheelie bins and repeated effort"
date: "2022-08-01T08:46:58+1000"
abstract: "An apartment block I lived in had its rubbish taken out not with a skip or two, but ROWS of tiny residential bins. Why!?"
year: "2022"
category: Thoughts
tag:
- australia
- government
- economics
- environment
- music
- music-monday
- pet-shop-boys
- politics
location: Sydney
---
Save for a few houses in Malaysia and Australia, I grew up in apartment buildings. You get used to the different dynamic of living in closer quarters to your neighbours, sharing lifts with them, and evacuating with everyone downstairs when Eugene burns his toast and sets off the smoke alarm.

That communal nature also extends to rubbish and recycling. It was a common sight growing up seeing garbage trucks in the morning lifting and tipping large skips into their bellies, then moving on. The guys in Singapore would often be there when I'd be heading to school, and I'd always get the biggest grins when I waved to them.

You know what, that's a memory I almost completely forgot about. I'm glad I didn't; it made me smile again more than a decade later.

The only apartment complex where this didn't happen was one I shared with my sister for a few years in Mascot, just south of Sydney. For reasons I couldn't fathom, come garbage day there would be lines upon lines of [tiny residential wheelie bins](https://sulo.com.au/) parked on the kerb; the same ones you see along the endless streets of Australian suburbia.

[In suburbia](). There in the distance like a roll call. Of all the suburban dreams. Hey, we got a *[Music Monday](https://rubenerd.com/tag/music-monday/)* in!

<p><a target=_BLANK href="https://www.youtube.com/watch?v=hUYfz_cEFzs" title="Play Pet Shop Boys - Suburbia (Official Audio)"><img src="https://rubenerd.com/files/2022/yt-hUYfz_cEFzs@1x.jpg" srcset="https://rubenerd.com/files/2022/yt-hUYfz_cEFzs@1x.jpg 1x, https://rubenerd.com/files/2022/yt-hUYfz_cEFzs@2x.jpg 2x" alt="Play Pet Shop Boys - Suburbia (Official Audio)" style="width:500px;height:281px;" /></a></p>

It was a ridiculous sight. Rather than taking a skip or two out, the poor sanitation workers were having to load one of these tiny things at a time, not to mention the people in charge of managing the garbage chutes to these thing things. It amplified the effort required to remove the same amount of refuse, and for what?

I never figured out why, but I had theories. Maybe the street was a bit too narrow for a skip; but then, rows of redundant wheelie bins took up more space. Perhaps residential wheelie bins are offered for free by the council, whereas a skip would need to be provided by the building management or strata... and they were cheap.

If that's the case, it's the perfect metaphor for throwing time away in the interests of saving a bit of money. And I'd like to think people's effort and energy are worth more.

It also makes me think why we need to consume things that generate so much rubbish in the first place, but that's a separate issue. Or, is it?
