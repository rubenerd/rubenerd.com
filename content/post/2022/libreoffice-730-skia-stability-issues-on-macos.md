---
title: "LibreOffice 7.3.x Skia stability issues on macOS"
date: "2022-02-21T08:32:20+11:00"
abstract: "Disable Skia rendering and you get stability back."
year: "2022"
category: Software
tag:
- bugs
- libreoffice
- macos
location: Sydney
---
LibreOffice has been crashing every few minutes on my 5500M Intel MacBook Pro since I upgraded to the most recent branch. I haven't had stability issues for years, so this was a surprise.

I'd noticed the UI of LibreOffice Calc looked a bit different, with thinner cell borders and some strange new dropdown buttons. So I went to check the rendering options.

Under Preferences &rarr; LibreOffice &rarr; View, I saw a checkbox for *Use Skia for all rendering* that was checked now. I unticked this and restarted, and it hasn't crashed again since.

I haven't had a chance to reproduce on other Macs yet, but passing on in case you've noticed the same issue.
