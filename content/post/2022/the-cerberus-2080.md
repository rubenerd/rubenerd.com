---
title: "The CERBERUS 2080"
date: "2022-06-07T10:43:11+10:00"
abstract: "A new computer kit featuring a 65C02 and a Z80!"
year: "2022"
category: Hardware
tag:
- commodore
- nostalgia
- retrocomputing
location: Sydney
---
I just learned about [this computer](https://www.thebyteattic.com/p/cerberus-2080.html) from [Jan Beta](https://www.youtube.com/watch?v=YxW3LQ6DE-k). This looks amazing!

> CERBERUS 2080™ is a complete, innovative, fully-functional, multi-processor 8-bit microcomputer. Featured on HACKADAY, it is meant as an open-source educational platform for students of computer engineering and advanced hobbyists, supporting both the Z80 and the 65C02 CPUs. As such, it has been designed correctly, so to provide an appropriate didactic example. Unlike many hobby computers, CERBERUS is a self-contained system and rock-solid in terms of robustness and reliability. It's also pretty darn fast!

For those who didn't recognise them, the Zilog Z80 and MOS 6502 families powered most of the home computer revolution of the 1980s, including the likes of Apple and Atari. My Commodore 16 and Plus/4 have compatible variants of the 6502, and my Commodore 128 has both!

I already have a mountain of vintage and retro-inspired computer stuff on the desk next to me, but this looks amazing. This could be my entry into proper assembler and soldering kits that I've wanted.
