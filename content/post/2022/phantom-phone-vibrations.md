---
title: "Phantom phone vibrations"
date: "2022-02-18T10:55:23+11:00"
abstract: "...bzzt!"
year: "2022"
category: Hardware
tag:
- health
location: Sydney
---
Picture (or I guess feel?) the scene. You're sitting there, or walking around, or otherwise existing in a state where you're not checking your phone, only to have it vibrate in your pocket. You reach in to check it, only to discover its not there. What you felt was a phantom; a vibration you've trained yourself to feel after years of having these devices close to your body.

Anxiety aside, it's also another reason why I've avoided smartwatches that want to shove notifications onto your wrist. I don't need more of these!

These phantom vibrations happen to me on a daily basis, sometimes *multiple* times a day. It's strong enough that I'd swear something is there, poking me to check my notifications. And it always happens on my right side, because that's where the phones go.

I had a friend in Singapore who was an amputee, and among her biggest frustrations was feeling the need to scratch an itch on her limb that no longer existed. That's obviously infinitely worse, but I wonder if there's a similar physiological cause? Clearly there's a miscommunication going on.

I did a quick search, and there are already studies in the medical literature exploring this phenomena. [Amrita Deb in Asian Pac Psychiatry](https://pubmed.ncbi.nlm.nih.gov/25408384/) even draws a connection to the potential mental health effects:

> The last decade has witnessed considerable interest in pathological conditions stemming from misuse or overuse of technology, a condition commonly referred to as technopathology. Of the several complaints reported, phantom vibration or phantom ringing is one that has not yet been widely explored. ... According to findings obtained, phantom vibration or phantom ringing was commonly experienced by mobile phone users; however, few found it bothersome and hence took no steps to eliminate it.

I feel like I'm *starting* to cross that threshold now into it being a nuisance, but I'm wondering what can be done about it.

I'm going to make a conscious effort to put it in the other pocket for a month, and see if it either results in phantoms on that side, and/or if it reduces them on my right. I'll report back in a month!
