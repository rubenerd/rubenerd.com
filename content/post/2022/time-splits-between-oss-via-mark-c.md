---
title: "Time splits between various OSs, via Mark C."
date: "2022-05-19T19:34:39+1000"
abstract: "Mac, FreeBSD, NetBSD, Linux, and some very old nonsense!"
year: "2022"
category: Software
tag:
- bsd
- fedora
- feedback
- freebsd
- linux
- netbsd
location: Sydney
---
A question from Mark C. in email this morning, among all the rude explanations about [what .gitignore files](https://rubenerd.com/git-ignores-gitignore-with-gitignore-in-gitignore/) are:

> What percentage of time do you spend on your computers? Is it Linux mostly and some Mac? What makes you choose one over the other? Love your blog!

Thank you! I needed that :).

It's interesting how computers and our perceptions have changed over time. I mostly used Mac laptops and Windows desktops growing up, because the Mac was the fun tool with good Wi-Fi, and Windows was for the serious desktop machine. Thesedays the Mac is my work machine, and the fun desktop at home runs FreeBSD and Linux.

I only arrived back on the Linux desktop recently [in the context of games](https://rubenerd.com/why-my-game-pc-also-runs-freebsd/ "Why my game PC runs FreeBSD and Kubuntu"), thanks to all the work Valve and Wine have put into Windows compatibility (though I've since switched back to [Fedora Workstation](https://rubenerd.com/my-journey-back-to-fedora-workstation/)). Otherwise it's FreeBSD and NetBSD, the former because the tooling is awesome and I prefer OpenZFS, and the latter because I still harbour a soft spot for its clean design and community.

For the sake of completeness, my vintage machines run a combination of old, pointless OSs like DOS, Windows for Workgroups, and Commodore Basic. My aim is to eventually have my childhood Pentium 1 also booting BeOS and OS/2, but they've proven more challenging than I expected with my IDE to CF adaptors, and even my new SCSI to SD.

I'm not sure what exact ratios each OS would have, but I'll bet if I added all those up as percentages, I'd get 100 :). 
