---
title: "The difference between good and bad people"
date: "2022-06-26T15:57:33+10:00"
abstract: "Good people have compassion for those they haven't met."
year: "2022"
category: Thoughts
tag:
- politics
location: Sydney
---
Good people have compassion for those they haven't met.

