---
title: "Trying (and liking!) KDE Neon"
date: "2022-03-03T08:28:15+11:00"
abstract: "If you want a stable Ubuntu LTS base for Steam, but still want the latest Plasma, this is the perfect mix."
thumb: "https://rubenerd.com/files/2022/kde-neon@1x.jpg"
year: "2022"
category: Software
tag:
- kde
- kde-neon
- linux
location: Sydney
---
Last Sunday I talked about [running FreeBSD and Kubuntu](https://rubenerd.com/why-my-game-pc-also-runs-freebsd/) on the same machine. I originally ran Steam games on Debian, but decided to move to Kubuntu during my last upgrade cycle. Steam officially supports Ubuntu's Long Term Support releases, which makes my life easier.

But then I remembered [KDE Neon](https://neon.kde.org/), released by the KDE team themselves. The alternative distro is also built on Ubuntu LTS, but includes rolling releases of the latest Plasma desktop and tools. This seemed like the perfect combination for someone who wants a stable, supported base, but the latest desktop goodies.

Installation was simple, though I wish it included OpenZFS as an option. I used the manual partitioner so I could use LUKS and XFS instead of ext4. You can ignore all of this if it sounds French.

<p><a href="https://rubenerd.com/files/2022/kde-neon@full.jpg"><img src="https://rubenerd.com/files/2022/kde-neon@1x.jpg" srcset="https://rubenerd.com/files/2022/kde-neon@1x.jpg 1x, https://rubenerd.com/files/2022/kde-neon@2x.jpg 2x" alt="Screenshot showing my tweaked KDE Neon desktop, with my preferred panel position on the left. Running NVIDIA's server settings, Info Centre, and Steam" style="width:500px; height:281px;" /></a></p>

Booting into KDE Neon didn't feel much different from Kubuntu, or even FreeBSD with KDE. It's that familiar Plasma desktop, with all the integrations working out of the box for sound, Wi-Fi, screen resolution adjustment, suspend/resume, and swapping audio sources. I still find it funny that macOS refuses to let you adjust volume over HDMI, but the *PulseAudio of OSs* can.

The user edition of KDE Neon comes on an ISO that's almost half the size of Kubuntu, given that Plasma packages would be stale and need upgrading anyway. It also doesn't come with a bunch of applications, but they're easy enough to install with the graphical Discover application, or the `pkcon(8)` terminal utility. You'll want to use the latter over `apt-get(8)` for most tasks, including updates.

I've only been running it for less than a week, but thus far I'm impressed with the polish, stability, and ease of use. I also like that I see those Plasma updates come through soon after reading a blog post or changelog about a bug fix or new feature. As much as I'd prefer to be using FreeBSD everywhere, KDE Neon has made desktop Linux attractive enough a proposition that I enjoy using my game machine again. That's no small feat! Mostly because I wear US size 11s.

If I had only one other quibble, it'd be that I wish it came with more KDE applications by default. KDE Neon could be a real showcase of what the KDE ecosystem has to offer, especially for those who've "tried Linux" before, but don't know about Kate or Kdenlive, for example.
