---
title: "Remote railfanning, and a live GP38-3 in Florida"
date: "2022-04-12T13:24:44+10:00"
abstract: "Watching remote streams from Japan and the US."
thumb: "https://rubenerd.com/files/2022/FQD5BktakAIxYte@1x.jpg"
year: "2022"
category: Travel
tag:
- railfan
- streaming
- trains
- united-states
location: Sydney
---
Picture the scene: is a phrase with three words. Clara and I are camped out at a unique double-diamond rail crossing waiting for locomotives to pass us. Just as the morning sun crested the horizon, we heard the sound of two rebuilt CSX GP38-3s pulling an intermodal train. We hurriedly took a bunch of pictures and looked up the locomotive number online.

Granted the title gave away the punch line here, but Clara and I weren't in Plant City, Florida on their chilly morning; we were half the planet away the following evening in Australia. It was equal parts surreal and awesome.

I grew up obsessed with trains, but I've had a resurgent interest in it of late, due in no small part to games like Train Simulator, and delightful YouTube channels like [Distant Signal](https://www.youtube.com/user/distantsignal). When Danny suggested we follow [Virtual Railfan's](https://www.youtube.com/watch?v=OHVJNyp1Yvc) web streams from around the US, we had no choice. It was so much fun seeing the very lines he explored in his videos, live!

This was the second locomotive in this consist, but the first I was able to screenshot by the time I saw it:

<p><img src="https://rubenerd.com/files/2022/FQD5BktakAIxYte@1x.jpg" srcset="https://rubenerd.com/files/2022/FQD5BktakAIxYte@1x.jpg 1x, https://rubenerd.com/files/2022/FQD5BktakAIxYte@2x.jpg 2x" alt="View of the CSX loco passing the double diamond in Plant City in early morning." style="width:500px; height:281px;" /></p>

I looked up the loco number, and it's a [rebuilt GP38-3](http://www.rrpicturearchives.net/locoPicture.aspx?id=229000). I have a soft spot for GP38-2s from the original Train Simulator, so it seemed fitting that it'd be the first American loco I'd spot for real :).

We've had [Japanese commuter railway cameras](https://rubenerd.com/live-video-feeds-of-tokyo-trains/) up for a while, but rural America has been a fun change of pace.
