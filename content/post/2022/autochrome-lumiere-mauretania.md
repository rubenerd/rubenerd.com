---
title: "Autochrome Lumière Mauretania"
date: "2022-07-19T06:55:45+10:00"
abstract: "An early form of additive colour photography shows the RMS Mauretania as she was in her heyday in 1926"
thumb: "https://rubenerd.com/files/2022/autochrome-mauretania.jpg"
year: "2022"
category: Media
tag:
- colour
- ocean-liners
- photography
location: Sydney
---
I love when several of my interests intersect in ways I didn't expect!

I've been reading up on the Autochrome Lumière process, which was an early form of colour photograpy. Rather than using a subtractive process like we see on film, these instead superimposed screen plates of differing colours to create the final image.

If you squint, you could *almost* pretend the [pattern of coloured grains](https://commons.wikimedia.org/wiki/File:Microphoto_of_Autochrome_plate.jpg) on an autochrome plate was a CMOS sensor on a modern digital camera! Insead of light being detected by various segments however, these colours permitted specific wavelengths of light to shine through to the exposure underneath, thereby creating a colour image.

It produced results with a very specific aesthetic; almost as though they've been run on a VHS tape. Lots of early photos I assumed had been colourised were instead made using this process, especially during the 1920s and 30s.

<figure><img src="https://rubenerd.com/files/2022/autochrome-mauretania.jpg" alt="Public domain photo of the Mauretania in dry dock in 1928, by Clifton R.Adams" style="width:500px;" /></figure>

I noticed Wikipedia's article on the original *RMS Mauretania* had a [color photo](https://commons.wikimedia.org/wiki/File:Mauretania_in_drydock_1928_autochrome_process_starboard_side.jpg) using the process. Its the only other time I've seen a colour image that wasn't from her last days before scrapping, and the only one while she still had her standard ocean liner colours.

I wonder how many other *colour* photos from the turn of the century are out there? I'd love to see them.

