---
title: "Rebuilding the Antonov Antonov An-225 Mryia"
date: "2022-03-25T13:09:35+11:00"
abstract: "I'd absolutely be down for donating to this."
year: "2022"
category: Thoughts
tag:
- aviation
- news
- ukraine
location: Sydney
---
Among all the senseless carnage and destruction wrought by Putin's illegal and unjustified invasion of Ukraine, we also lost one of aviations most iconic aircraft. [Andrew Curran reported for Simple Flying](https://simpleflying.com/antonov-an-225-rebuild-fundraiser/)\:

> Antonov is asking for donations to get the An-225 into the air again. The world's biggest plane was destroyed in the early days of the Ukraine invasion. Until now, Antonov has remained relatively circumspect about its flagship aircraft. But late on Thursday, Antonov CEO Sergii Bychkov acknowledged the An-225's destruction while kickstarting a campaign to revive the plane.
>
> “&hellip; We propose to establish the International Fund for the revival of the transport aircraft An-225.”

I'm *absolutely* down for this. The An-225 had carved out a special niche in commercial aviation, and was a continued showcase of Ukrainian engineering excellence.

It does raise the larger question of rebuilding. We should confiscate the wealth of Putin's cronies, and direct it to humanitarian aid and projects like this.
