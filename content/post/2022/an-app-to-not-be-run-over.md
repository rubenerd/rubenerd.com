---
title: "An app to not be run over"
date: "2022-11-10T04:15:24+11:00"
abstract: "A car company is proposing people install an app to not be run over. I have issues with this!"
year: "2022"
category: Internet
tag:
- privacy
- safety
- security
location: Sydney
---
I'm tired, can't sleep, and remembered something I needed to write about.

A large American automaker [published a press release](https://web.archive.org/web/20220927123049/https://media.ford.com/content/fordmedia/fna/us/en/news/2022/09/19/ford-research-tech-for-vulnerable-road-users.html "Explores Smartphone-Based Tech That Could Help Alert Drivers of Hard-to-See Pedestrians, Bicyclists and More") discussing a proposed new mobile app designed to make roads safer. Paragraph six contains the most pertinent details:

> The concept smartphone app running on a pedestrian’s phone uses Bluetooth Low Energy (BLE) messaging to communicate their location to a connected Ford vehicle. If the vehicle calculates a potential crash risk, [it] can alert drivers by the in-vehicle screen showing graphics of pedestrians, bicyclists or more with audio alerts sounding. 

The <a href="https://techcrunch.com/2022/09/19/ford-drivers-could-get-alerts-from-nearby-pedestrians-phones/" rel="nofollow">popular press have been vague</a> on how this would work, and there are plenty of confused people on threads about the topic. But it's clear from the PR that the car company intends for this to be a smartphone app that would communicate over Bluetooth Low Energy to vehicle "infotainment" systems.

*Infotainment* has to be among the most obnoxious new words we use, right up there with *zine*, *sheeple*,  and *utilise*.

It sounds dystopian on its surface, and I was tempted to leave it at that grim  observation! But the more I peeled back the idea, the more nonsensical it became. Let's take a drive.


### Playing devil's advocate

Assuming this car company is sincere, it sounds reasonable that we'd want to give people as much situational awareness as possible when driving.

Human minds don't rationally weigh the safety of one form of transport over another; otherwise nobody would have fears about commercial aviation or Evangelions. But it's still scary to think about how something as mundane and routine a part of our lives as car traffic kills millions of people a year, and injures scores more. We'd be screaming at aviation manufacturers and throwing their managing directors in gaol if their machines approached even a tenth of this. I'm looking at you, Shinji.

But I'm getting ahead of myself. I don't own a car, to which I'm sure comes as no surprise! But I concede that I see plenty of irresponsible pedestrians walking across roads with their heads buried in their smartphones, or otherwise blissfully unaware of their surroundings. If those devices can signal to a motorist, or an "autopilot", that they're in a vehicle's path, the motorist could take action and save their life; such as a vehicle may determine their [life is worth saving](https://www.spiegeloog.amsterdam/the-need-for-speed/) when plotted among other variables.

Western society has [prioritised cars over people](https://www.kobo.com/au/en/ebook/walkable-city) in most urban settings for a century, so this could be a reasonable and more cost-effective mitigation for safety. Addressing root causes takes work, something I inexplicably find myself putting into this rambling post at 4 in the morning because I can't sleep and am scared of cars.

Well, it'd be a good mitigation for those rich enough for a smartphone... that's a whole separate kettle of burned rubber and potholes.


### Taken in context

You may have noticed I coached the above section with plenty of weasel adverbs. The truth is, there's not enough evidence presented in the press release to conclude such tech will improve safety. It also drives in plenty of new problems, unloads them, then gets snitty when a pedestrian has to walk around it because its blocking their path.

Sensors to detect pedestrians aren't new, as [this article by Jon Glasco](https://hub.beesmart.city/en/solutions/smart-mobility/unsafe-to-cross-the-need-for-pedestrian-safety-solutions) in February demonstrates. Smarter cars also have LiDAR and other tools to monitor their surroundings because, turns out, [humans didn't evolve](https://www.spiegeloog.amsterdam/the-need-for-speed/) to process information at the speed presented to them in cars. But while those reported systems rely on sensors embedded in crossings to alert approaching vehicles, or passive scans of their environment, this proposal *actively* involves smartphones of those who "opt in" to being tracked.

"Opt in" is in scare quotes, because we're coming back to it! Like a metaphor for something you come back to. Ruben, please remember to replace this with an actual metaphor. Asuka?

Jon's article provides some important context, emphasis added:

> A study based on US road crash data revealed that “vehicle speed when approaching a pedestrian crossing is critical to pedestrian safety.” Although Americans drove less during 2020, “speeding drivers taking advantage of empty streets and the **increasing presence of larger, more dangerous vehicles** have made it much easier for drivers to kill pedestrians.

And what's the [best selling personal vehicle](https://www.finder.com/us-best-selling-cars) in the US? A light truck made by the same company proposing this app, which study after study shows [raises fatality rates](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2486272/):

> Overall, light trucks pose a significant hazard to other users of the highway system but on average provide no additional protection to their own occupants.

Funny how that works! In a macabre, grim way, not in an outwardly haha way. It's not hard to see why, given the hideous grills of some of these penile compensators are now taller than most people.

Safety can't be assessed in isolation... Shinji. I can't take their PR as sincere or in good faith when they're also *actively* making cars larger and therefore less safe for pedestrians. I literally saw an ad for this company on the side of a Sydney bus last week that had the proud tagline that *it gets bigger*. Wow they *really* invite the overcompensation angle on themselves; that's why I'm impervious to it.

*(Yes, I'm writing this from Sydney, Australia. I thought the tagline at the top of every page would indicate my location, but I'm sure I'll still get comments from people raging that I'm a hapless Californian or New Yorker who doesn't know how important these increasingly dangerous vehicles are to the rest of the US. In their defence, is a phrase with three words).*

Instead, this PR reads like someone pushing responsibly onto victims for a problem they're causing, or at least exacerbating. The report makes no mention of any of this.


### The legal and ethical aspects

As one of my professors always cautioned to say, **tech must be assessed on its potential for abuse**. This is true even if well intentioned, and must include societal impacts if it's to be deployed in the real world. It's why the whole ecosystem of web3, NFTs, and crypo-currency get an F out the gate, even if there are technically interesting ideas buried under the weight of nonsense.

Pedestrians have an obligation to look both ways before being a proverbial chicken, to make use of pedestrian crossings, and to obey traffic lights. There's a *whole* angle about the history of jaywalking we won't get into here, but suffice to say I'm tired and should probably get back to sleep.

This proposed system expects pedestrians to don the electronic equivalent of high-viz to make themselves known to larger and more dangerous vehicles. Once introduced, it's not hard to imagine that those who don't opt in will be seen as reckless; maybe even irresponsible. Why don't *you* want to be safe?

This sets a worrying precedent. I could envisage insurance companies mandating their customers have these apps installed, or they would refuse to pay out injury or death claims (either buried in their terms of service, or spruiked as a feature to reduce premiums like no claim bonuses). If you think that's a stretch, I envy you having never dealt with an insurance company.

Imagine the tabloid press reporting that someone was maimed or killed not just because of reckless driving, but because the victim didn't have the app installed. I saw an article a few weeks ago on a rag in a coffee shop that alleged a pedestrian deserved to be hit at a level crossing because they were wearing dark clothes at night. I'm surprised they didn't excuse a creep because the victim of their leering wore a short skirt out of an anime convention, or that the second hand smoker should have moved because *it's just that simple!* We're already not on equal footing here.


### The technical aspects

Despite being an engineer, the technical issues of such a proposed system aren't half as interesting or worrying to me. But they're still worth mentioning.

If this car company came out with an app, how many other car companies would follow suit? Would pedestrians or cyclists require a folder full of these apps, which they'd need constantly assess? What potential impact would there be on privacy? Or if it became a standard, would its operation be mandated?

Bluetooth Low Energy isn't precise, or [realtime](https://ieeexplore.ieee.org/document/7793093/), and phones aren't designed to be used (abused?) like this. As one example, assuming one's BLE-enabled phone had a [100 metre range](https://www.bluetooth.com/learn-about-bluetooth/key-attributes/range/), that's not much distance to alert a car and have its driver or "autopilot" take action. The press release mentions the tech would be useful where pedestrians are "hidden behind obstructions", but that would limit the range even further, and shrink that reaction time to a second or less.

The system assumes phones are people, and therefore its potential for abuse and false positives make it a non-starter. Accidentally drop a phone on the street, or [take active measures](https://www.wired.com/story/99-phones-fake-google-maps-traffic-jam/) to stash one under a utility cover or lamp post, and you've rendered the entire system pointless, and potentially even more dangerous as people swerve into other traffic to avoid people who don't exist. The PR leads us to believe that it "calculates a potential risk", but how useful or trustworthy is that if it's [fed erroneous data](https://en.wikipedia.org/wiki/Garbage_in,_garbage_out)?

What about phones in the pockets of other motorists? Would pedestrians be expected to enable walking mode to be granted permission to use a footpath? You could program some smarts into it to only report someone moving at walking speed, but now you have *another* system making assumptions about its operating environment.

We're also trusting a car company has our privacy in mind when designing a smartphone app we'd be installing, and presumably leaving active permanently, to protect us from their contraptions, much to the chagrin of our batteries. What data are being transmitted? Given the [industry's track record](https://eandt.theiet.org/content/articles/2020/04/serious-cyber-security-flaws-uncovered-in-ford-and-volkswagen-cars-that-could-endanger-drivers/), I'd consider any of their security claims dubious.


### Conclusion

Even without further technical information, I think it's fair to judge the app's effectiveness based on our current understanding of the capabilities of current smartphone hardware, what it represents for the industry, and how it conveniently reframes the safety debate away from increasingly dangerous vehicles and onto victims. I'd say it's convenient, because it is.

I'm willing to be proven wrong; being hit by a large car while walking on a suburban footpath in broad daylight, and again while crossing a street on a green pedestrian light, is enough for me. It's two more times than I'd want anyone to get. Maybe Putin. Notice though how I even had to defend my own actions as a pedestrian, as though I'd otherwise deserve it or be culpable in my own demise at the hands of a motorist for the audacity of walking. *WALKING!? Who does THAT!?*

That's the real takeaway. I resent the idea that we need to actively broadcast *don't run us over*. Surely we deserve more than this.

