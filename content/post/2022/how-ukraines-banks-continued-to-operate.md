---
title: "How Ukraine’s banks continued to operate"
date: "2022-05-14T09:02:44+10:00"
abstract: " One thing I've learned from working in infocomm: Ukrainians are among the best in the industry."
year: "2022"
category: Internet
tag:
- europe
- finance
- security
- ukraine
location: Sydney
---
The Kyiv Independent [has an interesting analysis](https://kyivindependent.com/opinion/how-national-bank-managed-to-ensure-routine-operation-during-russias-full-scale-war/) of how Ukraine's National Bank maintained customer operations and liquidity during Russia's invasion:

> [The] regulator has a clear framework for responding to various events that ensure the reliable and constant operation of Ukraine’s banking system, the preservation of public funds, and the continuity of payments &hellip; the regulator also requires business continuity plans from banks to be drafted in advance. 
> 
> The Ukrainian banking sector’s high degree of digitalization and the broad usage of remote financial services has also played a role.

It's sobering to think businesses have to factor in foreign aggression into their business continuity plans, and abhorrent that they've had to be invoked now thanks to Putin and his cadre of sycophants.

Frankly, I think it's incredible that these systems have held up so well in the face of what they've been dealing with. One thing I've learned for certain from working in infocomm: Ukrainian engineers are among the best in the industry. Don't bet against them. 🌻

