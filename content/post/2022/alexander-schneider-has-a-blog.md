---
title: "Alexander Schneider has a blog!"
date: "2022-04-04T13:24:36+10:00"
abstract: "Thus far he’s talked about C++, cognition, and backups. You should write too!"
year: "2022"
category: Internet
tag:
- bloggers
- feedback
location: Sydney
---
I got a great email from Alexander saying that my [silly post about blogging](https://rubenerd.com/starting-a-blog-and-unoriginal-ideas/) finally convinced him to start writing. That's great news!

> And while I thought about creating my own little blog many times in the past, and definitely for several years, this post gave me the final push to actually try it. Just because you worry that nobody is going to visit your blog, and all the hard work you've put in was just wasted for nothing, that shouldn't be a reason to quit. I guess it's the same feeling people have when opening a YouTube channel or any other social media type of web presence (although this is a blog, and not social media).

So far he's discussed an adventure [his father had with backups](https://lexlblog.de/?postid=5), and some [pontifications about types](https://lexlblog.de/?postid=2) in the C++ standard library with some surprising performance results.

[Please check him out](https://lexlblog.de) and subscribe to his [web feed](https://lexlblog.de/rss.php). His domain is in Germany, but he writes in English. And he didn't even bring up my unfortunate last name :).
