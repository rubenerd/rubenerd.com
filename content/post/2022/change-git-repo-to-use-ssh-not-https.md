---
title: "Change git repo to use SSH not HTTPS"
date: "2022-02-07T08:53:18+11:00"
abstract: "git remote set-url origin"
year: "2022"
category: Software
tag:
- git
- gitlab
- guides
- source-control
location: Sydney
---
I cloned a repo from GitLab this morning, made some changes, and was about to commit when I got this:

	remote: HTTP Basic: Access denied
	remote: You must use a personal access token with 'read_repository'
		or 'write_repository' scope for Git over HTTP.
	remote: You can generate one at
		https://gitlab.com/-/profile/personal_access_tokens

Sure enough, listing the remote sources showed HTTPS not SSH:

	$ git remote -v
	==> origin https://gitlab.com/rubenerd/repo_name (fetch)
	==> origin https://gitlab.com/rubenerd/repo_name (push)

So I changed:

	$ git remote set-url origin git@gitlab.com:rubenerd/repo_name.git

Donezo.
