---
title: "Do I need line numbers? Yes"
date: "2022-09-30T10:52:42+10:00"
abstract: "Turns out, I do! But maybe they are a bit of an anachronism."
year: "2022"
category: Software
tag:
- design
- interfaces
location: Sydney
---
I've been having fun lately challenging assumptions about my operating environment. I've got into the habit of blindly enabling settings without stopping to think if I need them, or for what reason. I've been surprised how much stuff was either built on sand, or that have been held over by sheer inertia and force of habit.

Line numbers are one of these. No matter if I'm starting with Vim, Kate, or a code repository site, I instinctively reach for the tool's respective line number setting or dotfile config, and everything is right in the world.

But... why? What use are they?

Time was, one needed line numbers to do programming. I grew up in primary school with QBasic and QPascal, the former of which had the legendary GOTO statement. But then, those rarely mapped 1:1 with line numbers; those in the know would manually add and pad their line numbers so you could insert intervening lines without having to retroactively renumber everything underneath it. Many an afternoon was spend tediously doing this.

Modern editors and languages gives us line numbers without any work on our part, and no longer require them to function. Which means they're now entirely a human affordance.

I took note of how I used them in Vim, and was a bit surprised:

1. My eyes tend to dart to line numbers to provide a **reference for where I am in a document**, even when there's a scrollbar and Vim's ruler present which reports my position. *(This is also why the trend of hiding scrollbars is counterproductive. They're a visual cue as much as they are a functional component, which UI designers either forget or ignore)*.

2. They let me know **how long each soft-wrapped line is** when writing longform documents, such as in LaTeX or Markdown. I can see the above point wrapped five times in the source file for this post, because there were that many lines between it and the next one. Whether you think soft-wrapping is useful or a sign of the devil is a philosophical discussion beyond the scope of this post!

3. Disabling line numbers made the editor feel... **empty**. Which is ridiculous. But then, so are all emotional attachments to inanimate objects.

Then there are line numbers in shell tools like `diff(1)` and `git(1)`, which report differences based on where they are in a file. I'd say those are more functionally important than in a text editor, but I include them here as another example.

Perhaps they're a bit of an anachronism in a interactive environment like a text editor today, but I'm happy to keep them around. If they come for free, why not?
