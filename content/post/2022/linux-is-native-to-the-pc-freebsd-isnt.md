---
title: "Linux is native to the PC, FreeBSD isn’t?"
date: "2022-06-02T07:45:58+1000"
abstract: "If being written on the PC from the start had end-user advantages, I’m not seeing them."
thumb: "https://rubenerd.com/files/2020/beastie@2x.png"
year: "2022"
category: Software
tag:
- bsd
- freebsd
- netbsd
- news
location: Sydney
---
The Register [ran a story about FreeBSD 13.1](https://www.theregister.com/2022/05/20/freebsd_131/), which contained a thought-provoking section about the practical differences between Linux and FreeBSD. Rather than distilling each into a set of features, or treating it like another Linux distro, the author Liam Proven makes a broader case based on their history:

> The other thing to point out is a more subtle difference, and one we’ve found that BSD types don’t really get. Linux is a native PC OS: it was first developed on x86 PCs, and only later ported to other architectures. It feels at home on a PC. It uses standard PC partition types, it happily co-exists with other OSes, it uses native screen resolutions and control keys and other things as a matter of course.
> 
> FreeBSD, like the other BSDs, is different. It is not native to the PC, and while it runs fine, it expects a substantial primary partition to itself, inside which it builds its own partitioning system. Its console runs in plain VGA mode, and simple things like Ctrl-left and Ctrl-right don’t work to edit the command line.

I see where he's coming from, though it's worth pointing out:

* FreeBSD and NetBSD *also* first started on the PC via 386BSD, as he writes later. Like Linux, it runs natively on i386/amd64, ARM, and other architectures. I think he means that BSD was *ported*, as opposed to having been written on the PC. I'm willing to chalk this up to semantics.

* It's reasonable to expect a primary OS to use an entire drive, unless you want to dual-boot. FreeBSD also "happily co-exists with other OSs" with a boot loader (it's how I started using it). FreeBSD also has kernel PV drivers for running in Xen guests, and can run as a Xen and bhyve hypervisor.

* FreeBSD also installs to a standard PC partition, or it couldn't boot. Primary partitions can also replace BSD slices if you prefer, though the former limit you just as they do with Linux and DOS. Linux equally has LUKS, btrfs, and OpenZFS (ne. ZFS on Linux) that have their own datasets in lieu of standard partitions. These distinctions are moot in the world of UEFI and GPT.

* FreeBSD booted with EFI usually uses the full screen resolution. VESA can be configured for other systems, otherwise it falls back to PC native VGA. As another example, Solaris usually booted with full screen resolution by default, and that OS *definitely* didn't start on Intel!

But let’s get to the good stuff. Niggling technical issues aside, does Linux feel like you’re using a PC OS, and does FreeBSD make your machine feel more like a big iron UNIX box from back in the day? Liam writes:

> When you run Linux on a non-PC machine, such as a Raspberry Pi, it makes the computer act a bit like a PC. When you run FreeBSD on a PC, it makes a PC act a bit more like a Sun workstation or a DEC minicomputer.

I’ll admit, I hadn’t thought about it that explicitly before. I’ve written before about how I think BSD is more Unix-like than Linux, which certain Hacker News readers lambasted me for in the past. But it’s true; as Linux moves further away (with systemd the prototypical example), the BSDs have remained closer to the "Unix Philosophy". It's up to you whether you think that makes them inflexible, or if they've avoided reinventing Unix poorly, as [Henry Spencer warned](https://quotes.yourdictionary.com/author/quote/551418).

FreeBSD itself answered this question many years ago [on its art page](https://www.freebsd.org/art/), with banners like the one below. Why settle for a PC OS like Linux, when you can have a workstation?

<figure><p><img src="https://rubenerd.com/files/2022/freebsd_3@1x.png" alt="Classic banner from the FreeBSD website saying: FreeBSD, Turning PCs Into Workstations" style="width:306px; height:94px;" srcset="https://rubenerd.com/files/2022/freebsd_3@1x.png 1x, https://rubenerd.com/files/2022/freebsd_3@2x.png 2x" /></p></figure>

I take the point that Linux, being written for and tightly coupled to the PC, has the potential to be more PC-like in general usage. But I don't think I've encountered any user-facing examples after using both OSs for many years. I'd be keen to hear if your experience differs, because there might still be something to this.

That said, I do think it’s awesome that I can have a little DEC-like machine humming away in the corner of my room delivering my Plex, bhyve VMs, file shares, Minecraft server, and VPN gateway. Nobody my age during the minicomputer era could have afforded one! Things will really come around full circle when I finally buy a [PiDP-11](https://obsolescence.wixsite.com/obsolescence/pidp-11) panel kit that’s powered by a Raspberry Pi, upon which I install FreeBSD.

I appreciate Liam giving me food for thought about this. As Michael Dexter pointed out on The Bird Site, it's far better to have sites talking about this than wrapping release announcements with ads and calling it a day.
