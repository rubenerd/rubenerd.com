---
title: "Sydney rain adventures"
date: "2022-03-08T08:51:22+11:00"
abstract: "I think we've had enough rain, everyone."
thumb: "https://rubenerd.com/files/2022/2022-03-08-rain.png"
year: "2022"
category: Thoughts
tag:
- weather
location: Sydney
---
I grew up in the tropics, and spent three days in Hong Kong during their monsoon season, and this is the most rain I've seen and felt in my life. It feels like the entire eastern seaboard of Australia has been under water for weeks.

This is the [latest MetEye](http://www.bom.gov.au/australia/meteye/) image from the Bureau of Meteorology showing rainfall and wind speeds. Last night the wind was so strong it knocked over furniture on our balcony.

<p><img src="https://rubenerd.com/files/2022/2022-03-08-rain.png" alt="Radar image showing a light shower this morning after days of storms." style="width:460px; height:295px;" /></p>

View the [Bureau's warning summaries here](http://www.bom.gov.au/australia/warnings/), and follow the [NSW State Emergency Service](https://twitter.com/NSWSES/status/1500732436858413058) for evacuation warnings.
