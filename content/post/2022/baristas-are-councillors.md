---
title: "Baristas are councillors"
date: "2022-08-06T10:25:26+10:00"
abstract: "I didn’t appreciate just how much this soft skill matters to their work."
year: "2022"
category: Thoughts
tag:
- coffee
- psychology
location: Sydney
---
I don't appreciate just how much baristas act like councillors. I wonder if they're vetted by their managers for additional soft skills like this? They must be.

Every morning I go to my favourite local coffee shop, and the baristas are always talking with customers about their day, the weather, what problems they're currently facing, how the long weekend wasn't long enough. Some lend a sympathetic ear, others are actively involved in mediation or working out solutions in between running the coffee grinders, the espresso machines, the filter drips, and asking if they wanted almond milk instead of skim lactose free.

My cognitive ability limits my multitasking to unpacking dishwashers while tailing server logs and listening to a podcast, and those require no external human input! Imagine if I was trying to figure out how Alice can talk with her estranged son, or if I had to offer inspiring words of encouragement to Bob for his job interview.

I'm enough of a regular customer that they even remember conversations we had yesterday, and can tell when I've had a bad night. Sometimes that does more for my outlook and mood than the beverage they brewed for me.

I've talked about how much I respect retail workers, and the abuse they cop from terrible people. Baristas are up there too.

One memory from Clara's and my last trip to New York was visiting Battery Park City and seeing all the bankers lining up inside a Starbucks. The sullen faces and grumbling voices could have easily brought the place down, had it not been for the cheerful baristas. I wonder how much of the modern world would collapse if not for their dedication?
