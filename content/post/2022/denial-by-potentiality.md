---
title: "Denial by Potentiality"
date: "2022-07-13T11:22:54+10:00"
abstract: "A move where you take some criticism or the description of a problem and argue with potential solutions as if they existed."
year: "2022"
category: Thoughts
tag:
- philosophy
location: Sydney
---
I originally read this phrase as *denial by [potentiometer](https://en.wikipedia.org/wiki/Potentiometer)*, which I assume would be equivalent to turning a dissenting voice down to zero so one can't hear them.

If I awarded phrases of the week, it'd have to go to this post on [tante.cc](https://tante.cc/2022/05/30/denial-by-potentiality/) from back in May:

> Denial by Potentiality is a move where you take some criticism or the description of a problem and argue with potential solutions as if they existed. This strategy lets you appear forward-looking, solution-oriented and lets you even "accept" and at the same time ignore the problem or criticism without having to do anything about it. It's a hidden form of denial.
>
> ... This form of denialism is so tempting because it not just allows people to continue doing the harmful stuff they are already doing but because it allows critics to attach to it as well

This could easily apply so many technical and social endeavours. Point out that something isn't good or needs improvement, and you'll always have someone cherry picking an unrealised alternative and saying it's worse.

Wait, you don't like current streaming services? What do you suggest, we force everyone to buy Walkmans and steam them in hot dog buns? What purpose would *that* serve, good sir!?
