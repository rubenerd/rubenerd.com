---
title: "Using sudoedit to sudo… edit"
date: "2022-06-07T15:23:24+10:00"
abstract: "sudoedit file, instead of sudo vi file."
year: "2022"
category: Software
tag:
- editors
- things-you-already-know-unless-you-dont
location: Sydney
---
I got into a [lot of trouble](https://rubenerd.com/git-ignores-gitignore-with-gitignore-in-gitignore/) last time I wrote a post with a title like this. Let's tempt fate again!

We haven't had a [things you already know, unless you don't](https://rubenerd.com/tag/things-you-already-know-unless-you-dont/) post for at least a few months, so here's something that still surprises some people.

Instead of doing this:

	$ sudo vi ./file

You can do this:

	$ sudoedit ./file

Safer, easier, better :).

From the [manpage(8)](https://www.sudo.ws/docs/man/sudo.man/), the tool creates a temporary copy of the file, then invokes whichever editor you have defined in `SUDO_EDITOR`, `VISUAL`, or `EDITOR`. When you're done editing, the temporary modified file is copied back to the original location.

