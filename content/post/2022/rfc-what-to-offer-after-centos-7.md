---
title: "RFC: What to offer after CentOS 7?"
date: "2022-03-31T20:16:53+11:00"
abstract: "Should we offer AlmaLinux, Rocky, or Scientific by default?"
year: "2022"
category: Software
tag:
- centos
- feedback
location: Sydney
---
I don't bring work up here often, but we're trying to figure out what to to after CentOS 7. The platform provides VM templates on a rebrandable portal for MSPs, SIs, telcos and the like. These can be enabled or disabled depending on what the partner wants their customers to deploy.

We were tempted to have CentOS 8 be the new default and let partners decide, but the universal feedback is that its no longer fit for their purpose.

Right now we're debating which of these to make our officially recommended upgrade path:

* [AlmaLinux](https://almalinux.org/)
* [Rocky Linux](https://rockylinux.org/)
* [Scientific Linux](https://scientificlinux.org/)

I prefer OpenSUSE for RPM-based workloads, but I empathise with the need for drop-in RHEL compatibility. Not everyone can afford support contracts or certified hardware.

What are [your thoughts](https://rubenerd.com/about/#contact)? What would make your life easier?
