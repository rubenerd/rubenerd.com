---
title: "Education cuts are a national security threat"
date: "2022-07-14T15:46:57+10:00"
abstract: "Some people don’t care about helping others, so here’s my other angle."
year: "2021"
category: Thoughts
tag:
- education
- politics
location: Sydney
---
I'm an egalitarian. I worked hard to be where I am, but I also want to give people the same opportunities I had. Free, universal education is one of the hallmarks of a compassionate, civilised society; one in which your character, skills, and interests are all that matter in the pursuit of knowledge and improvement of lives.

But I also appreciate that love for strangers doesn't shift opinion among large swaths of the population. So let me make the case for ubiquitous education from an angle that people *do* get into a hot lather about: defence.

Making higher education difficult deprives us of talented minds. Students either aren't able to reach their full potential, or locate to other countries where they can. Smart, educated, and connected people build our defences.

That's it. If you're against education spending, you're against national security. You don't want *The Terrorisms* to win... do you?

