---
title: "The Queen’s Birthday, and the Whitlam dismissal"
date: "2022-06-13T10:32:20+1000"
abstract: "Hey, at least we get a public holiday and commemorative cookie tins."
thumb: "https://rubenerd.com/files/2022/Royal_Standard_of_Australia.png"
year: "2022"
category: Thoughts
tag:
- australia
- politics
location: Sydney
---
Today's the Queens Birthday in Australia. That, coupled with her recent Diamond Jubilee celebrations, lead me to read up again on the [Whitlam Dismissal](https://www.whitlam.org/explainers/the-dismissal)\:

> Governor General Sir John Kerr removed Whitlam as Prime Minister [on the 11th November 1975], the first and only time this has ever happened in Australian history. John Kerr's Secretary David Smith read a proclamation dissolving parliament and Kerr appointed Liberal Malcolm Fraser as caretaker PM.

For overseas readers, the "Liberals" here are the nominally right-of-centre party which govern in coalition with the Count-ry/Nationals. They also tend to be the most enthusiastic about the monarchy, whereas the Labor and Greens parties lean towards republicanism.

> The dismissal was seen as an "assault on democracy", and shocked the country. Whitlam famously said on the steps of Parliament House, "Well may we say God save the Queen because nothing will save the Governor General."

There's a [video on YouTube of that address](https://www.youtube.com/watch?v=CZGE4tVdsOk). It surely ranks among the most memorable in Australian history. My parents told me they were furious, and couldn't quite believe it.

> The decision to remove Whitlam was highly controversial. John Kerr was in close touch with the Palace during this period, but under the cover of personal correspondence, all documents were locked away and embargoed by the Queen. After a protracted legal campaign, the correspondence between John Kerr and the Palace was finally released last year.

Australia, Canada, New Zealand, and other Commonwealth monarchies have a Governor General representing the Crown domestically. They're largely seen as figureheads, but the Whitlam dismissal reminded us that an unelected British monarch is still above our democracies. You could even still appeal an Australian or Canadian court decision in the UK as late as the 1980s. But hey, we get a public holiday and commemorative cookie tins.

I think former PM Paul Keating [put it best in an interview](https://www.youtube.com/watch?v=pyaClCClH-A) when Whitman died in 2014:

> He changed the way Australia thought about itself.
