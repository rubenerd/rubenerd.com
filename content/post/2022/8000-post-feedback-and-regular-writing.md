---
title: "8,000 post feedback, and regular writing"
date: "2022-03-20T08:27:59+11:00"
abstract: "Questions about how to write regularly, though it’s not the only way to write."
year: "2022"
category: Internet
tag:
- feedback
- weblog
location: Sydney
---
Thanks everyone for your email and tweets regarding my [silly milestone](https://rubenerd.com/8000-blog-posts/)!

A question that's come up in a few tweets and emails is how I keep up with doing this regularly. I wouldn't profess to be a writing expert, or even an especially good blogger, so take the rambling contained herein with an iceberg of salt. Wait, that wouldn't work.

I wish I had a more concrete answer. Writing is a pleasant distraction for me, a way to prepare myself for the day, and lets me focus on something I'm learning or thinking about. It's also often a sneaky way to remember *how* I did something. I'm not sure what I'd fill my writing time with if I *wasn't* doing this.

If I had to be prescriptive, I'd say spend some of your social media time on writing instead. Write about what's on your mind, regardless of whether you think it's interesting, engaging, or worthwhile. Trust me when I say, there's always *someone* else out there who can relate! Don't be shy, or convince yourself you're boring, or aren't contributing anything... if I took my constant self-doubt seriously, I'd never write anything.

If you need some prompts, think about what you've read or watched recently. Have you solved a problem? What's something you've changed your mind on recently? How would you do something differently? Sometimes a post with a single quote can be a spark to create something bigger.

Authors always say the best way to write better is to read more, which I can vouch for in blogging. Building up some RSS feeds in an aggregator like The Old Reader can be a great source of inspiration.

But it's also important to stress that this is only one approach. 8,000 posts sounds superficially impressive, but doesn't speak to quality or detail. Writing a few posts a year is just as valid and worthwhile. If you're having fun doing it, who cares.
