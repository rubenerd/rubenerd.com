---
title: "Winter coffee shops compared to home"
date: "2022-06-18T09:38:48+10:00"
abstract: "Who knew that temperature perception depends on context?"
year: "2022"
category: Thoughts
tag:
- coffee
location: Sydney
---
Last year I wrote about how weird it is to realise that our perceptions of temperature depend on context (though for some reason I can't find the link right now).

It also applies to coffee shops. I've been sitting here for the better part of an hour, and while I've crossed my legs perhaps a bit tighter than I may otherwise, I'm still writing out here with my scarf, hat, and puffy jacket.

If I were at home with this chilly breeze and temperatures, I'd be shivering and whinging loudly enough to Clara that she'd probably put on headphones or head to the library across the street. But out here it's all good. Okay, mostly good.

Maybe it's to do with the novelty, or the fact I've *volunteered* for this ridiculous arrangement, or it's the promise of a caffeinated reward. The neural pathways wired to transmit enjoyment of a hot beverage are more established than the rational, and arguably more important temperature sensors that are telling me to leave this place and sit next to a campfire somewhere. Those primal instincts are also what feed my permanent fight-or-flight anxiety though, so they can take a hike!

I'm sure I have readers among you who also think shivering with 12 degree weather to be cute, given you probably shovel snow off moose antlers (not a nice thing to call your inlaws, but I won't judge). But for someone who grew up thinking [28 degree humid heat](https://en.wikipedia.org/wiki/Singapore#Climate) in the evening was a wonderful reprieve from 32, I may as well be camped out on an icy tundra somewhere, complaining about the lack of Wi-Fi reception to a seal that's had the misfortune of popping up right where I am.

I suppose it's no different than thinking a mug of coffee is lukewarm in your hand, but would feel hot if poured on your skin.
