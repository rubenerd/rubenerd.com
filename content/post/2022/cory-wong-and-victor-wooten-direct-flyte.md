---
title: "Cory Wong and Victor Wooten, Direct Flyte"
date: "2022-07-18T08:54:23+1000"
abstract: "Get some funk and brass into your Monday morning ♡."
thumb: "https://rubenerd.com/files/2022/yt-lPSbmC2OxEE@1x.jpg"
year: "2022"
category: Media
tag:
- music
- music-monday
location: Sydney
---
*[Power Station](https://corywong.bandcamp.com/album/power-station-deluxe-edition)* might be my new favourite Cory Wong album. The ensemble of musicians and guests he has is *absurdly* good.

Today's [Music Monday](https://rubenerd.com/tag/music-monday/) is his song *Direct Flyte*, featuring Victor Wooten. Get some funk and brass energy into your morning and watch this.

<p><a target=_BLANK href="https://www.youtube.com/watch?v=lPSbmC2OxEE" title="Play Cory Wong // Direct Flyte (feat. Victor Wooten)"><img src="https://rubenerd.com/files/2022/yt-lPSbmC2OxEE@1x.jpg" srcset="https://rubenerd.com/files/2022/yt-lPSbmC2OxEE@1x.jpg 1x, https://rubenerd.com/files/2022/yt-lPSbmC2OxEE@2x.jpg 2x" alt="Play Cory Wong // Direct Flyte (feat. Victor Wooten)" style="width:500px;height:281px;" /></a></p>
