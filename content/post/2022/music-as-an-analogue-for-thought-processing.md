---
title: "Music as an analogue for thought processing"
date: "2022-12-05T10:39:44+11:00"
abstract: "I thought I didn’t “hear” lyrics in music most of the time, but I can idly recall them later!?"
year: "2022"
category: Media
tag:
- music
- music-monday
- psychology
location: Sydney
---
Today's [Music Monday](https://rubenerd.com/tag/music-monday) is a bit more philosophical, which is always dangerous.

I can't remember if I've talked about this somewhere before, but I tend to hear melodies and remember tunes rather than lyrics. It's why I love everything from bossa nova to J-pop, without speaking any Portuguese or much Japanese. Lisa Ono can even speak both.

But something interesting does happen when I hear songs in English. I'll be in the kitchen idly humming a song while unpacking the dishwaster, or performing some other menial task with low cognitive effort, and it finally occurs to me what the lyrics actually are. This can be either fun or startling, and can completely change the tone and feelings for a piece of music that, up until that point, I've enjoyed.

This suggests to me that I *do* remember lyrics, but that I store them in my mental data bank and only process them after the fact. If you asked me what the lyrics are to the song before I did this, I'd truthfully claim to have no knowledge of them.

Isn't that... weird!?

It makes me think what other information I receive on a daily basis that sits there in the mental queue, waiting to be processed and understood. How much of that is retained and forgotten? What's the shelf life for an unprocessed thought before it gets dropped, like a proverbial Ethernet packet retained by a swamped router?

Then I went down the rabbit hole of thinking about quantities. It it *some* external stimuli that end up in this state? *Most* of them? It's scary to think there's stuff in our brains we're not aware of; but then, how different is that to our subconscious mind telling our heart to beat without our monitoring?

I wonder if this taps into the same part of our brain that manes mnemonics work. Or to use another analogy, its like our brain has a degraded RAID or ZFS pool with sufficient data and hashes to reconstruct the data, we just need to do a mental resilver. Until then, they're just signals in grey matter.
