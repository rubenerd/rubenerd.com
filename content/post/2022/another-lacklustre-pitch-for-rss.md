---
title: "Another lacklustre RSS pitch"
date: "2022-12-10T08:32:31+11:00"
abstract: "It’s another post that describes audience being fed content. We can do better."
year: "2022"
category: Internet
tag:
- blogging
- rss
location: Sydney
---
WP Engine published a [resource centre article in September](https://wpengine.com/resources/wordpress-rss-feeds/) that explains what RSS is. It's about what you'd expect thesedays; emphasis added:

> Getting **content** in front of your **audience** is a key concern, regardless of your niche. RSS feeds are a perfect way to do this. One of the main reasons is that users have to do practically nothing in order to view your **content** (aside from clicking on a link).
> 
> Since the feed does all the work, **users** don’t even have to memorize your URLs. This, in addition to the enhanced portability the feature provides, means that RSS feeds can become a central tool in **promoting** your site’s **content**.

Their technical explanation isn't half bad by modern web standards, though it's missing a lot. RSS *does* let you read people's ideas in an aggregator, river, or other service, and it *does* make reading easier.

But it's the pitch that gets me every time. It reminds me of the Ghost blogging platform promising to [turn your audience into a business](https://rubenerd.com/turn-your-audience-into-a-business/) (and the subsequent <a href="https://rubenerd.com/feedback-on-my-blog-as-a-business-post/">follow-up</a>). Note the use of the term *audience* in lieu of community, *users* in lieu of people, and summarising what we do as *[content](https://rubenerd.com/content-engagement-and-extraction/)* to be *promoted*. The rush to see everything through this lens is so sad, and I'd argue is contributing to many of the modern web's ills.

RSS is [more than plumbing](https://rubenerd.com/rss-is-more-than-plumbing/), just as blogging is more than advertising. It's a shame one of the world's largest blog hosting platforms misses this potential. Heck... they may even be more money it for them! *cough*.
