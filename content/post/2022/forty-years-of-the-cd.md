---
title: "DW: Forty years of CDs"
date: "2022-08-24T07:23:31+10:00"
abstract: "Silke Wünsch does a great retrospective, and talks about LPs in the modern age."
year: "2022"
category: Media
tag:
- music
- vinyl
location: Sydney
---
Silke Wünsch did a [great retrospective](https://www.dw.com/en/40-years-of-cds-from-listening-pleasure-to-useless-trash/a-62821523) on the CD and vinyl for DW, including a bit of their personal history and the introduction of the new format:

> In 1981, the CD was presented at the Berlin Radio Exhibition. The first industrially produced discs rolled off the production line on August 17, 1982, and legend has it that the ABBA album "The Visitors" was burned on them. Perhaps it was also a recording of Richard Strauss' "An Alpine Symphony," conducted by Herbert von Karajan, who had outed himself as a big fan of the CD from the very beginning, describing it as "a miracle." Another legend says it was waltzes by Chopin that were pressed onto the first CDs.
>
> In 1984, 3 million CDs were sold in Germany alone; in 1989, the figure was 54 million.

Silke waxes lyrical about large CD collections, but you can hear the laughter in this observation:

> Even more tempting than Spotify playlists, however, is the vinyl record, to which I have once again become addicted after parting with my CD collection — as have numerous music fans around the world. In 2021, for example, more vinyl records were sold in the US than CDs for the first time since 1991. And at these prices: €30-40 for a 180 gram vinyl record is standard. A record today costs more than twice as much as a CD — this sounds familiar…

Granted this has more to do with the precipitous decline of CD sales; vinyl isn't approaching the peaks CDs were a few years ago. But it's still floors me that this observation is true! It could be because people buy media to have a physical token of music they care about, and a large vinyl cover is far more compelling than a CD jewel case. 

Clara and I started collecting CDs (albeit in folders) again recently once we started noticing more acts aren't releasing them anymore. Our little sound system now plays CDs, LPs, cassettes, 45s, and Minidiscs, in descending order of how many we have of each type. I'd *love* a DAT player and a reel-to-reel, but even for a nostalgic fool like me it's hard to justify when you only have two tapes of each!

It's funny to think that we save LP re-releases for our favourite albums, and CDs for everything else where we can, when I'm sure in the 90s people couldn't wait to *crackle* do *pop* the exact opposite.
