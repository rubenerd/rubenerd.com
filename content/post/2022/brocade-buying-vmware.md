---
title: "Broadcom buying VMware"
date: "2022-06-03T12:52:57+1000"
abstract: "I feel for the people who’ve been passed around so many times at this stage."
year: "2022"
category: Software
tag:
- business
- vmware
location: Sydney
---
I feel for the people who work at VMware.

The hypervisor company has spent so much of its history being moved around the plate like that proverbial brussels sprout; something thought to be nutritious and tasty only to be regretted and shifted to the next person, or washed up into the disposal. It's been bought, its owner bought, merged, spun off, divested entirely, and bought again, with all the expected layoffs and uncertainty.

But even with all that, being bought by Broadcom wasn't on my bingo card, despite having excellent alliteration. It didn't think it made much sense, though Patrick Kennedy added some context at [Serve The Home](https://www.servethehome.com/broadcom-agrees-to-purchase-vmware-shaking-up-the-industry/):

> The company purchased CA Technologies and Symantec’s enterprise security business now building a software unit that VMware will fit into.

I hope the remaining people there finally get some job security, but for how long *this* phase of VMware's life will last is anyone's guess.

*(Full disclosure: I work at a company that produces a hypervisor stack that somewhat competes with VMware, though we also run it internally for certain clients).*
