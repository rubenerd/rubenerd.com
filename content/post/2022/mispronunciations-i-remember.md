---
title: "Mispronunciations I remember"
date: "2022-10-12T21:35:37+11:00"
abstract: "It was a network tropology!"
year: "2022"
category: Thoughts
tag:
- language
- networking
location: Sydney
---
It's funny how we forget so many important things, and remember such trivial ones. Here are some that I remember, some going back to my childhood:

* In primary school I had a friend who called the sport *shutput*. Maybe he was my first interaction with a Kiwi!

* My networking lecturer at university called it a LAN *tropology*. He also had a habit of starting each sentence with *also too...*, which would lead to *also too, this tropology...*

* An Adelaide Channel 7 weather forecaster used to say it'd be *chwenty* degrees.

* A [2006 Australian TV ad](https://rubenerd.com/mybudget-bills/) used to pronounce bills as *biwls*.

* Anyone who's ever said *expresso* or *ex-cetera*. Neither of those words contain the letter X.

* I pronounced Arkansas as *Arkansas* on an old podcast episode, which an American listener clarified for me. It's pronounced *Arkan-saw*.
