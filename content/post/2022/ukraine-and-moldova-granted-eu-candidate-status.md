---
title: "Ukraine and Moldova granted EU candidate status"
date: "2022-06-24T06:33:59+10:00"
abstract: "🎉"
year: "2022"
category: Thoughts
tag:
- moldova
- ukraine
location: Sydney
---
Finally some [good news](https://www.abc.net.au/news/2022-06-24/eu-leaders-grant-ukraine-moldova-candidate-status/101179400) in a world that increasingly needs it right now:

> European Union leaders have formally accepted Ukraine as a candidate to join the 27-nation bloc, a bold geopolitical move hailed by Ukraine and the EU itself as a "historic moment".
> 
> Although it could take Ukraine and neighbouring Moldova more than a decade to qualify for membership, the decision at a two-day EU summit is a symbolic step that signals the bloc's intention to reach deep into the former Soviet Union.
>
> The EU's leaders also also agreed on Thursday to recognise a "European perspective" for yet another former Soviet republic, Georgia.

The best time for this would have been 2014. The second best time is now.

If I had to nitpick, most of the coverage ignores fact the Baltic states were also in the USSR; albeit against their will.

I'm also not sure that framing is useful beyond historical curiosity. Countries should be able to decide their own fates irrespective of who ran them before, and the USSR dissolved three decades ago. To emphasise otherwise plays into Putin's hand that he's losing something, rather than getting him to ask what he was doing to keep them happily in his circle.

**Update:** Ukranian President Volodymyr Zelenskyy and [advisor Ihor Zhovkva](https://www.euronews.com/my-europe/2022/04/12/ukraine-deserves-eu-membership-because-we-are-fighting-for-europe-says-zelenskyy-advisor) reminded us all that EU candidate status wasn't granted, it was earned and deserved. This is absolutely correct; I apologise for my own framing here.
