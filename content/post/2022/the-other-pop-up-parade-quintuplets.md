---
title: "The other Pop Up Parade Quintuplets"
date: "2022-05-30T07:43:38+10:00"
abstract: "Naturally I was most excited for Nakano (cough)."
thumb: "https://rubenerd.com/files/2021/quintuplets-nakano-yotsuba@1x.jpg"
year: "2022"
category: Anime
tag:
- anime-figs
- quintessential-quintuplets
location: Sydney
---
Everyone's favourite manifestation of Azusa from *K-On!* [tweeted about a humerous categorisation](https://twitter.com/azusa__cat/status/1530508999757545473) of anime figures on a large online retailer, which reminded me that I never did past two of my [Pop Up Parade Quintuplets](https://rubenerd.com/pop-up-parades-quintessential-quintuplets/ "Pop Up Parade’s Quintessential Quintuplets").

I've now seen Ichika (below left) and Nino, who are captured equally well. I can only imagine the sinister plot Ichika has concocted behind that smile, and the biting rhetort Nino delivered with that smug expression!

<p><img src="https://rubenerd.com/files/2021/quintuplets-ichica-nino@1x.jpg" srcset="https://rubenerd.com/files/2021/quintuplets-ichica-nino@1x.jpg 1x, https://rubenerd.com/files/2021/quintuplets-ichica-nino@2x.jpg 2x" alt="Photo of Ichika and Nino's Pop Up Parade renditions" style="width:500px;height:390px;" /></p>

Naturally though, and not surprising Clara in the least, I was most excited to see Nakano (left), pictured again with my other favourite Yotsuba! *Oh you say you’re a history buff... humu humu?!*

<p><img src="https://rubenerd.com/files/2021/quintuplets-nakano-yotsuba@1x.jpg" srcset="https://rubenerd.com/files/2021/quintuplets-nakano-yotsuba@1x.jpg 1x, https://rubenerd.com/files/2021/quintuplets-nakano-yotsuba@2x.jpg 2x" alt="Photo of Nakano and Yotuba's Pop Up Parade renditions" style="width:500px; height:390px;" /></p>

Pop Up Parade have done such a great job making this hobby accessible with affordable prices and decent quality. Even what I'd consider "game prize" companies have been lifting their game. Don't get me wrong, I resent all of this, because I have no space.

For what it's worth, I've also been tearing through the *Quintessential Quintuplets* manga on my iPad. I'll save a proper review for when I'm finished, but it's been so much fun. I've laughed out loud so many times I think I made Clara worry. It makes me appreciate what a difference *good translations* make; it would have been a lot of work to maintain the same level of wit in their devastating put-downs.
