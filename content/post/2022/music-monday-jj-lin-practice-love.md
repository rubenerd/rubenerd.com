---
title: "Music Monday: JJ Lin, Practice Love"
date: "2022-05-02T19:10:24+10:00"
abstract: "This song was EVERYWHERE in Singapore in 2014!"
thumb: "https://rubenerd.com/files/2022/yt-LWV-f6dMN3Q@1x.jpg"
year: "2022"
category: Media
tag:
- music
- music-monday
- singapore
location: Sydney
---
Today's return to blogging happens to be a *[Music Monday](https://rubenerd.com/tag/music-monday)*, which is rather delightful. This song came right through the speakers of a bubble tea shop in Sydney, and instantly transported me back to the mid-2010s during happier times.

<p><a href="https://www.youtube.com/watch?v=LWV-f6dMN3Q" title="Play 林俊傑 JJ Lin - 修煉愛情 Practice Love (華納official 高畫質HD官方完整版MV)"><img src="https://rubenerd.com/files/2022/yt-LWV-f6dMN3Q@1x.jpg" srcset="https://rubenerd.com/files/2022/yt-LWV-f6dMN3Q@1x.jpg 1x, https://rubenerd.com/files/2022/yt-LWV-f6dMN3Q@2x.jpg 2x" alt="Play 林俊傑 JJ Lin - 修煉愛情 Practice Love (華納official 高畫質HD官方完整版MV)" style="width:500px;height:281px;" /></a></p>

*Practice Love* was seemingly *everywhere* when we went back to Singapore in 2014, from shopping centres to a taxi we got from the airport. The official music video is something else.

JJ Lin is a Singaporean musician, and one of those frustrating people who can sing in multiple languages. I'd happily accept talent in just English, let alone Mandarin, Hokkien, and Cantonese.
