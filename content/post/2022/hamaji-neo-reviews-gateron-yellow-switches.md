---
title: "Hamaji Neo reviews Gateron Yellow switches"
date: "2022-07-06T15:14:38+10:00"
abstract: "Finding another great indie YouTube channel."
thumb: "https://rubenerd.com/files/2022/yt-qd6Eo96P0Ew@1x.jpg"
year: "2022"
category: Hardware
tag:
- keyboards
- video
- youtube
location: Sydney
---
I've learned a lot about key switches from [Hamaji Neo](https://www.youtube.com/c/hamajineo/videos), including the [NovelKeys Creams](https://www.youtube.com/watch?v=iICikICuWa4) and the [Gateron Black Inks](https://www.youtube.com/watch?v=h9QlGRQpFdI). But this one convinced me to give the king of budget linears a try.

I haven't ever seen the TF2 streams for which he's most famous, but Hamaji's keyboard review videos are top notch. He's lots of fun to watch, and does a great job of making custom mechanical keyboards approachable for mere mortals like me. Give him a like and a subscribe :).

<p><a href="https://www.youtube.com/watch?v=qd6Eo96P0Ew" title="Play The ULTIMATE Budget Linear: Gateron Yellows Review"><img src="https://rubenerd.com/files/2022/yt-qd6Eo96P0Ew@1x.jpg" srcset="https://rubenerd.com/files/2022/yt-qd6Eo96P0Ew@1x.jpg 1x, https://rubenerd.com/files/2022/yt-qd6Eo96P0Ew@2x.jpg 2x" alt="Play The ULTIMATE Budget Linear: Gateron Yellows Review" style="width:500px;height:281px;" /></a></p>

