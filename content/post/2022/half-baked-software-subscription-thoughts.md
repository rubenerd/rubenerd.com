---
title: "Half-baked thoughts on software subscriptions"
date: "2022-12-15T15:30:07+11:00"
abstract: "Not quite sure what my point here was, I was just thinking out loud. Hey, that’s my whole site!"
year: "2022"
category: Software
tag:
- finance
- licencing
- open-source
location: Sydney
---
I've been thinking about this problem on and off for ages. Subscription pricing is a point of consternation, a word I had to look up because that doesn't look like the correct spelling.

Software subscriptions and online tracking are two sides of the same coin: they're attempts to make regular income off continual effort. Depending on your perspective, they either make software and websites cheaper to use, or they come with nasty strings attached.

I tend to use open-source software when I can; it's generally easier to install, has better cross-platform support, and respects open file formats and standards. But I also don't begrudge people wanting to charge money for their labours, whether it be through a licence or a support package. And unfortunately, thanks I'm sure to mobile app stores and race-to-the-bottom pricing, the market signal shows that *most* people aren't willing to spend much upfront for software (if they ever were).

<p><img src="https://rubenerd.com/files/2022/save.svg" alt="Save icon, from the Gnome Desktop Project" style="width:128px; height:128px; float:right; margin:0 0 1em 2em" /></p>

But are subscriptions the answer? I don't know.

Say what you will about Steve Jobs, but he nailed it when he said you have to think about the customer first, not attempt to graft a business model or device onto someone. One could argue that subscription pricing brings expensive software within the reach of more people. But were users clamouring to track and ever-increasing list of credit card charges each month, especially for software they could previously licence outright?

I appreciate the argument that subscriptions bring in more regular, predictable income to developers who need it, and reduces pressure to come up with constant upgrades. We've all been in the situation where software we rely upon has been upgraded with features of limited utility to us, but nevertheless requires us to pay lest we lose support. In that light, subscriptions almost seem more honest.

At last count I voluntarily send money to eighteen open-source software foundations, Patreons, and indie developers. Is that any different to a subscription? I suppose it is, because most people don't care... and I'm sure I should be doing more.

A few weeks ago I read a Mastodon post saying the universe doesn't owe you a good or ethical answer to problems; this looks to be an example of it. As long as people don't value the efforts of software developers, while relying on them to do everything in the modern world, we'll face this issue. In which case, I suspect the ire against subscriptions is a classic case of attacking the symptom. 
