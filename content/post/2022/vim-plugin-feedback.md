---
title: "Vim plugin feedback"
date: "2022-05-18T08:29:05+1000"
abstract: "Responses from Ben Oliver and an anonymous reader about loading Vim plugins"
year: "2022"
category: Software
tag:
- feedback
- vim
location: Sydney
---
My post earlier this month [about vim-plug](https://rubenerd.com/trying-vim-plug/) generated a few interesting comments and suggestions.

[Ben Oliver](https://www.bfoliver.com/) ([Atom feed](https://www.bfoliver.com/feed.xml)) let me know of another useful vim-plug feature, especially if you're like me and used to being able to lock packages in FreeBSD:

> The coolest thing about vim-plug (ok perhaps not cool) is that you can create a lockfile of all your plugins in their current state. So you run the install like you did already. Then once you are happy everything is working:
>
> <pre><code>:PlugSnapshot foo-bar.lock</code></pre>
>
> Then if an update happens and something breaks, you can roll it back with (from CLI not vim):
>
> <pre><code>vim -S foo-bar.lock</code></pre>
> 
> It's also useful for deploying to multiple systems, instead of having 
different versions of plugins on different machines.

Another gentleman who asked to remain anonymous:

> I’ve been using vim-plug for years now, but recently it came to my attention that starting from version 8 Vim (and Neovim) has it's own barebones package managing system. As such, now Vim autoloads any plugins that you put in "~/.vim/pack/*/start/“ folder ("~/.local/share/nvim/site/pack/*/start/” for Neovim). To automate package installation using this built-in package manager, I’ve put the following at the top of my "vimrc":

<pre>let s:packs = expand(has('nvim') ? '~/.local/share/nvim/site/pack' : '~/.vim/pack')

function! s:InstallPlugin(type, name, repository)
    let l:path = a:type . '/start/' . a:name
    if empty(glob(s:packs . '/' . l:path))
        silent execute '!cd ' . s:packs . ' && git submodule add ' . a:repository . ' ' . l:path
    endif
endfunction

if empty(glob(s:packs))
    silent execute '!mkdir -p ' . s:packs . ' && cd ' . s:packs . ' && git init’
endif
</pre>

> And now, to specify which plugins I want installed, I just call that "s:InstallPlugin" function later on in my "vimrc":

<pre>
call s:InstallPlugin('colors', 'noctu', 'https://github.com/noahfrederick/vim-noctu.git')
call s:InstallPlugin('syntax', 'erlang', 'https://github.com/slarwise/vim-erlang-syntax-simple.git’)
</pre>

> As such, my Vim now automatically upon startup checks which plugins it has not yet installed, and installs them as git submodules in a “~/.vim/pack” git repo.
> 
> Just wanted to let you know that nowadays you can go a long way without using any external Vim package managers like vim-plug at all, Vim provides pretty much everything that is needed out of the box.
