---
title: "Teaching history"
date: "2022-03-15T08:13:32+11:00"
abstract: "“the state has an obligation to welcome children into that entire history”."
year: "2022"
category: Thoughts
tag:
- ethics
- history
location: Sydney
---
[From the New Yorker](https://www.newyorker.com/magazine/2022/03/21/why-the-school-wars-still-rage), via Merlin Mann:

> That's why parents don't have a right to choose the version of American history they like best, a story of only their own family's origins. Instead, the state has an obligation to welcome children into that entire history, their entire inheritance.

Easily applies to Australia too.
