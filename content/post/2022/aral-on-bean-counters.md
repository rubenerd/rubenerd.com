---
title: "Aral Balkan on bean counters"
date: "2022-02-25T20:32:25+11:00"
abstract: "When you’re led by bean counters, don’t be surprised when all they end up doing is count beans."
year: "2022"
category: Thoughts
tag:
- business
location: Sydney
---
[Via his Mastodon instance](https://mastodon.ar.al/@aral):

> When you’re led by bean counters, don’t be surprised when all they end up doing is count beans.
