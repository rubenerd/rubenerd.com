---
title: "Goodies from @GamersNexus arrived!"
date: "2022-05-20T08:25:43+1000"
abstract: "These modmats, coasters, and a new mousepad made it around the world faster than some domestic packages!"
thumb: "https://rubenerd.com/files/2022/gamersnexus-thumb.jpg"
year: "2022"
category: Hardware
tag:
- merchandise
- pc-building
- youtube
location: Sydney
---
Clara's and my merchandise from the [GamersNexus store](https://www.youtube.com/user/GamersNexus) just arrived! It included a brand new anti-static modmat work surface, Mini-ITX mouse pad, and this fun new coaster set. It was packed *extremely* well, and arrived internationally to Australia faster than some of our domestic parcels.

<figure><p><img src="https://rubenerd.com/files/2022/gamersnexus@1x.jpg" alt="Photo of the new modmad with PC components, coasters, and motherboard mousepad, with Clara's blue bear cat." style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2022/gamersnexus@1x.jpg 1x, https://rubenerd.com/files/2022/gamersnexus@2x.jpg 2x" /></p></figure>

Steve and his team at [GamersNexus](https://www.gamersnexus.net/) have done so much for my mental health over the last year. Their thorough and principled PC component reviews, coupled with their entertaining and thoughtful personalities have largely been responsible for getting me back into building computers again. My optimistic, PC-obsessed childhood seemed so distant as an anxious guy in his mid-thirties, but he's back with a vengeance!

This wasn't just to support their channel though; the quality of this stuff is as good as you'd expect. The mousepad is also larger than I expected, which *turns out* works well with my 60% Topre keyboard. Maybe I need to buy another one for the mouse too.

Thank you :).
