---
title: "The Stanley Parable Ultra Deluxe: Please play it!"
date: "2022-05-16T08:35:50+10:00"
abstract: "I don’t care if you’re not a gamer (whatever that means), you need to get this! Please. Do it for Stanley."
thumb: "https://rubenerd.com/files/2022/parable@1x.jpg"
year: "2022"
category: Software
tag:
- games
- reviews
location: Sydney
---
For all my nostalgia over Commodore and Win16 platforms, we're living in the golden age of surrealist puzzle games right now. If you're like me and not into headline-grabbing AAA first-person shooters, they might have slipped you by.

[The Stanley Parable](https://www.stanleyparable.com/) may be the best example. It's made it onto a few of my game lists, but I've never reviewed it. This was deliberate; it's nigh impossible to do it justice without divulging spoilers.

You start as Stanley, an office drone who discovers his coworkers are missing. It's your job to wander around and figure out why. The British narrator directs you to certain choices, but it's up to you whether you follow his lead or mess up his story. The forth wall isn't broken as much as its *utterly* obliterated.

Clara and I haven't jumped or laughed this hard over a game in years.

<figure><p><img src="https://rubenerd.com/files/2022/parable@1x.jpg" alt="Screenshot of the initial scene in The Stanley Parable, showing Stanley's blank computer console and office desk with his 427 employee number." style="width:500px; height:280px;" srcset="https://rubenerd.com/files/2022/parable@1x.jpg 1x, https://rubenerd.com/files/2022/parable@2x.jpg 2x" /></p></figure>

It's hard to describe, but despite being an American game, it wouldn't be out of place among surrealist English comedies on a movie night. You'll love this game if you're into that sense of humour, which you should be. *Are you... still in the broom closet Stanley?*

The game was re-released with bonus content last month, and I was *not prepared for how good it'd be*. [Buy and play it](https://www.stanleyparable.com/), right now! I'd still recommend playing on a desktop for the original authentic experience, but it also works great on the Nintendo Switch.
