---
title: "Tech you like that’s gone"
date: "2022-07-24T10:20:07+10:00"
abstract: "A great discussion started by Lucas Holt."
year: "2022"
category: Software
tag:
- apple
- beos
- firewire
- fonts
- lotus-organiser
- nostalgia
- palm
- palm-iiix
- palm-lifedrive
- powermac-g5
- sun
- sparcstation
- thinkpads
- twitter
- windows
- windows-2000
- visual-basic
location: Sydney
---
[Lucas Holt asked](https://twitter.com/laffer1/status/1550467654900760577) what tech people liked that's gone. I never miss an opportunity to engage in some tech nostalgia, so here's a selection of mine:


### Hardware

* **Palm, in all its generations**. I use a IIIx and LifeDrive today, which I HotSync to my Pentium I tower to Palm Desktop.

* **The 11-inch MacBook Air**. Modern ARM MacBook Airs still don't have the form factor, keyboard, or versatility of these machines. Apple wants to sell iPads in that size now.

* **FireWire**. Its ease of use and performance kicked *arse* over USB and SCSI. FireWire 400 and 800 drives were what I built my first ZFS arrays from.


* **ThinkPads**. Modern ones are still above average for PC hardware (which admittedly wouldn't be hard thesedays), but the original keyboard and design language are long gone.

* **Sun SPARC hardware**. I made the fateful decision to get a PowerMac G5 instead of a SPARCStation during high school, and I still regret it.


### Software

* **Serif fonts on product packaging!** Modern tech logos and marks are so boring.

* **Lotus Organiser**. Apple made it trendy to hate skeuomorphic design a decade ago, but this Filofax-style binder with pages for calendars, contacts, notes, to do list items, and journal entries was so well executed. PIM could be *fun!* I've got a post pending about it.

* **iTunes**. The original one that Apple bought and refined was an *awesome* music organiser and player. Honourable mention: Winamp.

* **BeOS**. I didn't use it much, but I miss what it represented. The industry can rarely stomach two options in anything (iOS/Android, Mac/Windows, etc), but at least someone gave it a shot. And the UI was awesome.

* **The Cobind Desktop**. There's no much online about this classic RPM Linux distro, but it was my introduction to the Xfce desktop in the day. Back when Linux was distributed on dozens of CDs, Cobind took Red Hat, chose a few best-of-breed programs, and distributed it on a single disc. My FreeBSD desktops for *years* were shameless clones of what Cobind did on Linux.

* **Windows 2000's UI**. It was clean, simple, consistent, relatively easy to use, and I'd even say quite attractive. It was the last time Microsoft refined the Windows 95-era interface (itself aped from other systems).

* **Classic Visual Basic**. I wouldn't use it now if it existed, but there's no question I had a ton of fun with it as a kid. I moved onto to Borland stuff when I got older, but I made so many silly little games and pointless utilities in it. There was even a DOS version, if you can believe it.
