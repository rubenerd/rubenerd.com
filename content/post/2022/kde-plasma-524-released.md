---
title: "KDE Plasma 5.24 released"
date: "2022-02-09T08:55:36+11:00"
abstract: "Everyone’s favourite desktop environment has some interesting new features, and a new theme."
thumb: "https://rubenerd.com/files/2022/kde-desktop@1x.jpg"
year: "2022"
category: Software
tag:
- bsd
- freebsd
- netbsd
- kde
location: Sydney
---
I just finished [reading the announcement](https://kde.org/announcements/plasma/5/5.24.0/) for the latest version of the KDE Plasma desktop. They have a bunch of helpful videos showing new aspects of the design, and how updated features work.

There's a bunch of goodies in here, but I'm most keen to try the new Overview effect task switcher, and the new Breeze Light and Dark themes. The team have also done work to improve positioning Desktop Panels, something that has been a bit clunky in the past. Even the launcher looks a little better organised.

<p><img src="https://rubenerd.com/files/2022/kde-desktop@1x.jpg" srcset="https://rubenerd.com/files/2022/kde-desktop@1x.jpg 1x, https://rubenerd.com/files/2022/kde-desktop@2x.jpg 2x" alt="Screenshot from the KDE announcement, showing the new desktop background and launcher." style="width:500px; height:281px;" /></p>

I love the polish and attention to detail, from the screenshots and features themselves, to the writing of the announcement post. KDE are firing on all cylinders, and it's a delight to see.

If you're interested in trying KDE, [FreeBSD has it in ports](https://docs.freebsd.org/en/books/handbook/x11/#x11-wm-kde), or I can also recommend [KDE Neon](https://neon.kde.org) or [Kubuntu](https://kubuntu.org/) if you want to give it a try.
