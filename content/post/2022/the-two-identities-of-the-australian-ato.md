---
title: "The two identities of the Australian ATO"
date: "2022-05-27T07:48:28+1000"
abstract: "Australian Taxation Office... “ato”, and other word marks."
year: "2022"
category: Internet
tag:
- australia
- design
- typography
location: Sydney
---
*(I hope you'll forgive my **light laser ATM machine PIN number** above. I thought the location was important, but I didn't want to spell the entire thing out. Is there room for a grammar exception on compassionate grounds)?*

I logged into the Australia Tax Office Online Service this morning, which is the Online Service of the Australian Tax Office. I set up to do what I knew would be some riveting reconciling work, and saw the familiar government department logo:

<figure><p><img src="https://rubenerd.com/files/2022/logo_ato_retina.png" alt="Logo of the Australian Taxation Office rendered in bold, serif Times font" style="width:250px; height:60px;" /></p></figure>

But then I shrank the window to make room for a spreadsheet, and it transformed into this:

<figure><p><img src="https://rubenerd.com/files/2022/logo_ato_shrunk.png" alt="Logo of the Australian Taxation Office, rendered as a lowercase initialism with a sans-serif font." style="width:245px; height:59px;" /></p></figure>

I'm familiar with this mark as well, but there's still such a bizarre dichotomy between these. One looks professional, if dated. The other looks like the typesetting on an informal blog that has an anime mascot drawn by his long-term girlfriend in the sidebar.

ATO is also an initialism, not an acronym, because it's not pronounced as a word. But written as "ato", it looks like it *should* be a word. Att-oh? Ate-oh? Att-ooh? *Eyyy-too?* It could even be onomatopoeic with a sniffle of embellishment. A... A... AH... *AH-TOOOO!* (Gesundheit)! Thanks, sorry my HECS indexation allergy is especially bad this year.

And people say this blog doesn't contain any useful <span style="text-decoration:line-through">financial</span> advice. They're probably right.

