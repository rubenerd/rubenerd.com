---
title: "Cancelling my Presario Sleeper PC project"
date: "2022-03-05T22:23:24+11:00"
abstract: "The thermals didn’t work out, unfortunately."
year: "2022"
category: Hardware
tag:
- sleeper-pc
location: Sydney
---
After more than a year of sourcing parts for my 1998 Compaq Presario sleeper PC build, I've made the decision to refurbish it instead of put modern parts in it again. Yes, my [sleeper PC curse](https://rubenerd.com/my-sleeper-pc-curse/) struck again!

This time it's due to my conflicting needs for the case. I want to preserve as much of it as possible, which means no cutting of metal on the sides, or altering the front panel. I see people who take 1990s cases, cut holes into the sides, and add perspex and RGB lights. To each their own, but if you have a particular attachment to a certain hardware design, I don't feel like this respects it.

Unfortunately, this limited the thermals more than I expected. The Zotac RTX 3070 Twin Edge that I bought to fit the dimensions of the case does work well, but pumps out too much heat for the case to dissipate. This choked even my underpowered old Skylake CPU, and the fans kick into high gear which makes the setup noisy even under what should be less stressful workloads.

I'd have loved to use a blower style GPU like I had with my 970, but the case is too narrow. Water cooling the CPU to compensate was also out of the question, even with the smallest AIO on the market. Even with the smallest radiator possible, there's simply no space.

The final nail in the coffin was where to fit the SFX power supply. I'd measured the width and height of my little Corsair 750 W unit, but hadn't accounted for the depth with all the modular cables! Without modifying the case too much, this meant it could only reasonably fit in front of the CPU, which is the biggest space I had to add a fan.

Right now the parts are back in my NCASE M1 while I decide what to do. I'm lucky that this wide Zotac card fits into it with millimetres to spare... a post about that is incoming. It's also funny that it reminded me how much I love this little case too.

Knowing me, the Presario will probably end up being a Windows 98 and NetBSD tower. I'd got used to playing games like Midtown Madness and SimCity 3000 in VMs, but having a dedicated machine with a period-correct graphics card might be fun too.

I loved the idea of sleeper PCs, but I'm realising now I like old hardware in those cases even more. Go figure.
