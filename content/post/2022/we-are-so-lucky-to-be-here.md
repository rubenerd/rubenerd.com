---
title: "We are so lucky to be here"
date: "2022-12-13T16:12:15+11:00"
abstract: "♡"
year: "2022"
category: Thoughts
tag:
- health
- mental-health
- love
location: Sydney
---
Call it [follow up](https://rubenerd.com/goodbye-chris-seaton/) or something else. It was just a thought that popped into my head over coffee this afternoon.

As an aside to this aside, thanks for reading this too. You have your choice of blogs to read and subscribe to, so it means a lot. ♡

