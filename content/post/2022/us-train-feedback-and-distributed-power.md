---
title: "US train feedback, and distributed power"
date: "2022-01-27T11:45:35+11:00"
abstract: "Feedback from Mike Harley and Andrew Feinberg about overnight shipping and distributed power trains in long consists."
thumb: "https://rubenerd.com/files/2022/yt-8M9pErSaElk@1x.jpg"
year: "2022"
category: Travel
tag:
- trains
- united-states
location: Sydney
---
I got some fun feedback about my posts on [battery-powered passenger trains](https://rubenerd.com/battery-powered-trains/), and Jeb Brooks’ video on the [California Zephyr](https://rubenerd.com/battery-powered-trains/).

Mike Harley of [Obsolete29](https://obsolete29.com/) recommended Alan Fisher, who does some excellent takedowns of [silly ideas](https://www.youtube.com/watch?v=nJNvpG5gktM) that only a Silicon Valley tech person could have concocted, and discussions of [regional trains in Pennsylvania](https://www.youtube.com/watch?v=ENait4eRKa4) where he lives. But it was his [poignant commentary](https://www.youtube.com/watch?v=_909DbOblvU) about shipping waste that I appreciated the most:

> All of these [delivery] steps, except for the home delivery part, is done by a truck ...  These are the vehicles that transport everything that you buy online to the warehouses that hold the goods. And they're a *huge* problem.

Andrew Feinberg recommended Distant Signal out of Florida. Like Jeb's channel I blogged about recently, it's incredibly well produced with some great commentary and video.

<p><a href="https://www.youtube.com/watch?v=8M9pErSaElk" title="Play How Distributed Power Works"><img src="https://rubenerd.com/files/2022/yt-8M9pErSaElk@1x.jpg" srcset="https://rubenerd.com/files/2022/yt-8M9pErSaElk@1x.jpg 1x, https://rubenerd.com/files/2022/yt-8M9pErSaElk@2x.jpg 2x" alt="Play How Distributed Power Works" style="width:500px;height:281px;" /></a></p>

I just finished his video on how distributed power (DP) works:

> ... with two engines leading, and another engine mid-train DP. DP helps makes these two mile beats possible, By minimising slack runout and bunching en route. A single change in throttle notch on a train powered only on the head end can result in a severe slack-action wave. DP maintains a much better level of slack action and control throughout the train.
>
> When you have motive power at mid-train, that also serves as another air compressor. The brakes respond more quickly and efficiently. Distributed power not only improves train handling, it also improves fuel economy and reduces rail wear.
>
> I got through all that technical gab and QO26 is *STILL* coming out of the yard!

There's so much not to like about These Times&trade;, but we're spoiled by so much independently produced video now, made by people who care about their interests. Enthusiasm is infectious... and not the bad kind.
