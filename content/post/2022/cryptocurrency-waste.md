---
title: "Cryptocurrency waste, in all senses of the word"
date: "2022-06-19T09:05:07+10:00"
abstract: "I’m not happy about people ignoring warnings and getting hurt, but they should direct their ire to those who hurt them."
year: "2022"
category: Internet
tag:
- cryptocurrency
- scams
location: Sydney
---
Cryptocurrencies continue their precipitous free fall, assuming they were even worth something in the first place. The bandits and whales are long out, and the marks have switched from arguing about utility to claiming that w-well everything else is a Ponzi scheme too in a desperate attempt to shore up their positions and retain what little credibility they can.

I see some people revelling in the crumbling of this smoke-belching edifice, citing how nauseating some of the conduct of these crypto bros have been. "Have fun being poor" is the quote most often thrown back at them.

I'll admit there's a *sliver* of schadenfreude felt towards some of the worst people in their community. I've copped my fair share of abuse from them. But otherwise, all I see is waste. Wasted pension funds, wasted lives, wasted power, wasted time, wasted potential.

I don't rub my hands with glee at the person who ignored warnings and got hurt; I want them to stand up and get angry at the person who hurt them. Direct that ire into something productive, or at least ensure that this nonsense doesn't happen again. They have a choice to be a pawn, or to stand up for themselves.

That's the opportunity I see here. So much engineering, hardware, and mental space was spent on this pointless tech. Let's start fixing problems again, and relegate scammers to, as conservative Australian MP Michelle Landry so eloquently put it, the *anals of history*.

