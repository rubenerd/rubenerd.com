---
title: "The pushback against decluttering"
date: "2022-09-17T09:26:58+10:00"
abstract: "The memes about Marie Kondo were telling, if not surprising."
year: "2022"
category: Thoughts
tag:
- decluttering
- junk
location: Sydney
---
Marie Kondo took a polite but well-orchestrated torch to people's junk piles a few years ago, [with a book](http://www.mariekondobooks.com/) and American TV series that challenged people to think about what they're carrying around. I'd been an avid reader of The Minimalists at this time, and had read about people who's lives were crippled by hoarding. Marie Kondo spoke to the middle, which is why I think so many took it personally.

The Internet's general reaction was telling, if not surprising. Most people memed and sarcastically summarised her view into "does this spark joy?" because it was easier than answering her pointed and difficult questions, some of which cut to the core of how we're conditioned to live our lives.

There are entire industries that are built on the backs of, as George Carlin put it, people spending money they don't have, on stuff they don't need, to impress people they don't like. How much of our economy would collapse overnight if people didn't overextend themselves?

Marie's words resonated with me, because I'd seen first hand the negative impact of junk. My parents grew up in working class families, so when they made it big in their careers they spent the money on mountains of *stuff*. I pass no judgement; I'd have probably done the same. But it meant my sister's and my childhood was dominated by stuff, whether it was constantly packing to move to larger places, or organising storage lockers, or buying new things because we could never remember which storage locker something was in. It also meant living in an isolated, gated community when we moved to Malaysia, which I'm sure had an effect on my mum's mental health.

Loss aversion is real. Couple that with feeling like you need to hold onto things out of obligation, and to get that dopamine hit when you reach the checkout or press that online purchase button, and you can see why people end up like this... and why pointing it out can lead to hostility and defensiveness.

But that's where the positives come in. Clara and I got rid of most of our stuff a few years ago, which means we can live in a smaller space in the [Sydney suburb](https://en.wikipedia.org/wiki/Chatswood,_New_South_Wales) we wanted to, with walking distance access to supermarkets, coffee shops, Asian food, and a large train interchange. Consolidating and throwing away all those storage lockers spread across Australia and Singapore brought my dad's expenses and junk down enough that he could buy a house.

You don't need to read Marie Kondo's books specifically, and there's a bit of an irony in buying stuff to get rid of stuff. But leave open the possibility that you don't need as much shit, and maybe ask whether you need something, or if it makes you happy, or how you could organise it so you could actually use and enjoy it, rather than stuffing it away to deal with on another day we both know will never come.

If your answer is to make a meme about it and mock the idea, ask yourself what you're defending.
