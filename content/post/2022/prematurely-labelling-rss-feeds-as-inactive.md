---
title: "Prematurely labelling RSS feeds as inactive"
date: "2022-05-26T08:49:01+1000"
abstract: "A blog that’s written to each year isn’t inactive, it means the author is thoughtful and has other things going on too."
year: "2022"
category: Internet
tag:
- blogging
location: Sydney
---
I love and recommend [The Old Reader](https://theoldreader.com/) for anyone who wants to get started reading RSS feeds. But there's one feature I think does more harm than good, at least in how its currently implemented.

Check out this on the landing page:

> **Inactive feeds**   
> * [brian d foy](https://www.effectiveperlprogramming.com/) a year ago   
> * [Halestrom.net](https://halestrom.net/darksleep) 9 months ago   
> * [Blogs on Alex Gaynor](https://alexgaynor.net/blog/) 8 months ago   
> * [BADCATBAD](https://badcatbad.weebly.com/blog-and-tutorials) 6 months ago

Would you consider any of these inactive, based on those reported timelines since their last post? I wouldn't!

I've long reached for [John Siracusa's Hypercritical](https://hypercritical.co/) as an example. He writes a few posts a year, sometimes even fewer. But they're always worth a read, and genuinely exciting to see when they pop up.

I tend to write every day, but that's only one style. Some may even say it's worth *less*, especially compared to someone who spends six months on a personal project and writes about it.

Please don't let this be another hang up if you've considered writing but are worried about how often you could post. A well written post about a project every other year is already *hugely* valuable.
