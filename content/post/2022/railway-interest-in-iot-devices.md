---
title: "Railway interest in IoT devices growing"
date: "2022-08-29T08:40:01+10:00"
abstract: "But it hasn’t translated into a meaningful increase in security hires."
year: "2022"
category: Internet
tag:
- iot
- security
location: Sydney
---
Railway Technology [noted a trend in railway company filings](https://www.railway-technology.com/dashboards/filings/filings-buzz-in-the-railway-industry-37-increase-in-the-internet-of-things-mentions-in-q2-of-2022/) in the first half of 2022:

> Mentions of the internet of things [&hellip;] rose 37% between the first and second quarters of 2022. In total, the frequency of sentences related to the internet of things between July 2021 and June 2022 was 23% higher than in 2016 when GlobalData, from whom our data for this article is taken, first began to track the key issues referred to in company filings.

Unfortunately, this hasn't translated to a [meaningful increase in security hires](https://www.railway-technology.com/dashboards/jobs/cybersecurity-hiring-levels-in-the-railway-industry-dropped-in-july-2022/), which you'd want when faced with the potential for a new threat model like this.

Industrial IoT has amazing potential. But in the words of Mikko Hyppönen, anything smart is vulnerable. Implementation will be key.
