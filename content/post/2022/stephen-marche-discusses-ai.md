---
title: "Stephen Marche discusses AI"
date: "2022-06-21T10:45:57+1000"
abstract: "“The fantasy of sentience through artificial intelligence is not just wrong; it’s boring.”"
year: "2022"
category: Software
tag:
- ai
- ethics
location: Sydney
---
This was [food for thought in The Atlantic](https://www.theatlantic.com/technology/archive/2022/06/google-palm-ai-artificial-consciousness/661329/) last week:

> [&hellip;] the silly fantasy of machine sentience has once again been allowed to dominate the artificial-intelligence conversation when much stranger and richer, and more potentially dangerous and beautiful, developments are under way.
> 
> The fantasy of sentience through artificial intelligence is not just wrong; it’s boring. It’s the dream of innovation by way of received ideas, the future for people whose minds never escaped the spell of 1930s science-fiction serials. The questions forced on us by the latest AI technology are the most profound and the most simple; they are questions that, as ever, we are completely unprepared to face. I worry that human beings may simply not have the intelligence to deal with the fallout from artificial intelligence. The line between our language and the language of the machines is blurring, and our capacity to understand the distinction is dissolving inside the blur.
