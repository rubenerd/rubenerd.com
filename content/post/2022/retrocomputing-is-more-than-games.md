---
title: "Retrocomputing can be more than games"
date: "2022-01-29T09:28:32+11:00"
abstract: "Ads about writing your own software, and the multimedia CD-ROM."
year: "2022"
category: Thoughts
tag:
- commodore
- dos
- cd-roms
- retrocomputing
location: Sydney
---
As much as I love all the new retrocomputing YouTube channels and bloggers that are rediscovering all this fun old tech, the focus is nearly always on games. Even people who repair or tinker with hardware see games as the end product.

That's fine. Games are fun! But curiosity about this stuff doesn't have to be limited by it.

People forget that the entire marketing message behind the original Commodore VIC-20 and later machines were that they were computers that could play games, rather than a game console. Advertisements claiming the devices had *full typewriter-style keyboards* weren't just a subtle dig at the Atari 2600 or the Sinclair, they were an all-out assault. They weren't saying their machines were more capable, they were saying *you* were too.

*This* is why I love Commodore kit in 2022: it was an empowering message that still resonates with me today. Other companies will tout their developer communities, but modern console makers aren't selling you a device with an interpreter and instructions on how to build software yourself. I love my little Switch Lite and my iPad, but they'll never tickle me in the same way.

CD-ROMs are another interesting case. While they were used to distribute immersive and complicated games by the turn of the millennium, their introduction in the 1980s was seen more narrowly as a way to store and distribute vast stores of information. This was used to mercifully distribute the likes of Office that previously required dozens of floppy disks!

But it introduced a new market segment not seen before or since: the multimedia CD-ROM. Before (more) ubiquitous and fast Internet access, these discs gave home computer access to half a gigabyte of text, sounds, music, animations, and videos. It's hard to overstate just *how cool* this was! A few inventions in my childhood made me feel like I was living in the future, and this was one of them.

*(Alongside the introduction of Apple's HyperCard, they also trained the public to understand hyperlinked pages which paved the way for the World Wide Web. I don't think they get enough recognition for this).*

I grew up reading encyclopedias and atlases, so having access to a thirty-volume set of books on a single shiny disk was a dream come true. Many afternoons after school were spent reading about random topics on Worldbook, Bookshelf, and Encarta... while telling friends I was gaming (cough)!

Which leads us back to retrocomputing. I love these discs for their "ROM" part. The web is a constantly evolving and expanding medium, but CD-ROMs are snapshots in time. If I want to see what the world was like in 1996, I can fire up DK's Multimedia Reference Atlas and cruise around. Other things like quotations and almanacs are evergreen, but it's always interesting to see how they're presented in that context of time.

They're also interesting for *how* they're presented. Microsoft was up to all manner of shenanigans at the time, but their *Microsoft Home* titles were consistently above average. DK's *Eyewitness Guides* and encyclopedias were the most polished and fun. IBM's *Worldbook* almost felt like you were using a web browser, and *Bookshelf's* angle of being bundled with an office suite was interesting, even if now I just read it standalone.

I've been told by more than a few people that being interested in this stuff is boring. I might need to write more!
