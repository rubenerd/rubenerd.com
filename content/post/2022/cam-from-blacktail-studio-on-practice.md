---
title: "Cam from Blacktail Studio on practice"
date: "2022-10-09T21:02:22+11:00"
abstract: "Don’t practice until you get it right, practice until you can’t get it wrong"
year: "2022"
category: Thoughts
tag:
- blacktail-studio
- philosophy
- video
- youtube
location: Sydney
---
Clara and I have been watching [Blacktail Studio's](https://www.youtube.com/c/BlacktailStudio) back catalogue over dinner the last few nights. We know nothing about furniture making or design, but it's therapeutic watching an expert craftsman applying their trade with such genuine passion and interest.

We [liked this gem](https://www.youtube.com/watch?v=vRasdg_Vm_0) from a build video he released last year:

> Nick Saban, the coach of Alabama, says that they don't practice until they get it right, they practice until they can't get it wrong.
