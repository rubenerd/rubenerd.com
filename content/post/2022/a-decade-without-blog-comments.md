---
title: "A decade without blog comments"
date: "2022-07-07T17:31:05+10:00"
abstract: "I turned them off in June 2012. I don’t miss them (much)."
year: "2022"
category: Internet
tag:
- comments
- weblog
location: Sydney
---
This anniversary completely slipped me by, if one can call it that. I was still on [WordPress in June 2012](https://rubenerd.com/goodbye-blog-comments/ "Goodbye blog comments!"), and I made the decision to turn off web comments permanently.

I wrote that it was due to spam, which certainly was escalating on my tiny self-hosted site at the time. But trolls were the real reason. I was tiring of cleaning up the mess they defaced my pages with, and didn't want to deal with moderation queues.

This worked well: no more spam comments! And wouldn't you know it, when trolls have to put *effort* into emailing stuff as opposed to filling out a web form for instant gratification, most lose interest. I feel like there's a larger lesson there.

I suppose over time I've engineered a new problem. People now routinely email me with interesting, useful, fun, and friendly comments; and now I have a backlog to reply to and quote here. I apologise if that includes yours. I do read every email I receive, unless Fastmail's aggressive filters block it and I fish it out of spam a month later.

Still though, is a phrase with two words. I *kinda* miss inline blog comments, but nowhere near enough to turn such a feature back on, however that may be implemented.
