---
title: "Migraine advice and treatments"
date: "2022-07-31T09:45:00+1000"
abstract: "An article in the New York Times discussing how to recognise symptoms, and what to do about them."
year: "2022"
category: Thoughts
tag:
- health
- personal
location: Sydney
---
Channel NewsAsia republished a [New York Times article by Melinda Wenner Moyer](https://cnalifestyle.channelnewsasia.com/wellness/migraines-headaches-new-treatment-324811) on the current state of migraine treatment, how to tell whether you suffer from them, and what steps you can take to try and get on top of them.

She break it down into three parts:

* Recognising the symptoms and getting a diagnosis. If you have two out of these four criteria: It throbs or pulsates; it is on one side of the head; it is moderate to severe; or it worsens with activity, you could suffer from migraines and should see someone about it. She claims only 30% of sufferers do.

* Try lifestyle changes and first-line treatments to start. A headache diary has been a huge help for my doctor and I to isolate potential triggers. For example, I avoid wine now.

* If you can’t get relief, discuss new treatments with your doctor. New treatments have become available even in the last five years, some of which are far less invasive, addictive, or have as many side effects as ones before.

I used to get nausea migraines constantly when I was younger, but fortunately they're every other month now, thanks in part to some newer treatments. They render me a pile of barely-conscious goo, but the relief is indescribable.

Human brains are weird. Let's evolve with these massive things to compensate for our physical abilities, then have them misfire and cause more problems.
