---
title: "Street-running trains"
date: "2022-03-23T20:40:32+11:00"
abstract: "I’ve been a train fan since I was a kid, but I still forget just how BIG they are."
thumb: "https://rubenerd.com/files/2022/CSXT_LaGrange_1@1x.jpg"
year: "2022"
category: Thoughts
tag:
- trains
location: Sydney
---
A recent map and scenario pack I downloaded for [Train Simulator](https://store.steampowered.com/app/24010/Train_Simulator_2022/) had me doing some street running, like a gentleman. It's a bizarre feeling driving a train down the main street of a town alongside cars! I'll save that for a future post.

In the meantime, I went looking for some photos in the real world where these giant diesel electrics drive down the street. This was a great example from 2007 in LaGrange in Kentucky. Thanks to [SimRacin40](https://commons.wikimedia.org/wiki/File:CSXT_LaGrange_1.jpg) for sharing this with us.

<p><img src="https://rubenerd.com/files/2022/CSXT_LaGrange_1@1x.jpg" srcset="https://rubenerd.com/files/2022/CSXT_LaGrange_1@1x.jpg 1x, https://rubenerd.com/files/2022/CSXT_LaGrange_1@2x.jpg 2x" alt="" style="width:500px; height:375px;" /></p>

Now you can see why you wouldn't want to get in the way of one of these things, either as a pedestrian or a car. I've been a train fan since I was a kid, but I still forget just how *big* they are. Even if they were idling their engines and coasting down the street, there's a lot of inertia in that steel.
