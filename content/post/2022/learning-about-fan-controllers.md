---
title: "Learning about fan controllers"
date: "2022-03-10T08:17:00+11:00"
abstract: "For when your motherboard doesn’t have sufficient headers."
year: 2022
category: Hardware
tag:
- diy-pc
---
What do you do when you have a tiny computer case, and not enough fan headers on your motherboard? You can daisy chain connectors, or you can deploy a fan controller, a device I didn't know existed until recently.

For some context, I've been upgrading my old NCASE M1 build, most recently with a dual-slot Zotac RTX 3070. Unlike my blower GTX 970, the 3070 uses a traditional top-down cooler, meaning it dumps its heat up into the case, rather than expelling it from the PCI slots.

As I soon discovered, the NCASE M1 can more than handle cooling this hot GPU, but the hot air forced up into the case from bottom-mounted fans raises the CPU temperature significantly. This will only get worse when I upgrade the ancient Skylake silicon with hopefully a Ryzen later this year.

Whereas I could rely on convection and negative pressure before, I've since invested in a few more Noctua fans as exhaust. But the motherboard only has one system fan header, and I was already using a Noctua-supplied splitter cable to connect both bottom fans. Online guides suggested you could daisy chain more, but I was wary of putting too much load on one header.

I thought my only option was to buy additional fans that connect directly to Molex plugs off the power supply (which I'd done with my Antec 300 homelab server), or wait for a Ryzen board that have far more headers. But then I learned of fan controllers.

Modern motherboards include fan headers with pulse-width modulation (PWM), which lets them adjust connected fan speeds in response to changes in temperature. Fan controllers connect to this same fan header, and relay those PWM signals to multiple other fans. The first fan connector will usually be the one the controller reports the speed of back to the motherboard.

<p><img src="https://rubenerd.com/files/2022/11-984-030-V04@1x.jpg" srcset="https://rubenerd.com/files/2022/11-984-030-V04@1x.jpg 1x, https://rubenerd.com/files/2022/11-984-030-V04@2x.jpg 2x" alt="Phanteks Universal Fan Controller in its packing box, with remote control." style="width:500px; height:375px;" /></p>

After some basic research I bought a Phanteks Universal Fan Controller and stashed it in the corner of the NCASE M1. Other controllers had RGB light support for which I have no interest, and this box was relatively unassuming and small.

More importantly for my current use case, it came with a tiny remote control that I can have outside the case to control fan speeds. I've been concerned how hot the case can get before the mini-ITX board kicks the fans up. Noise is barely an issue when using these Noctua fans, so I'd rather err on the side of cooling. When I get a new Ryzen board with better thermal controls, I'll attach it there instead.
