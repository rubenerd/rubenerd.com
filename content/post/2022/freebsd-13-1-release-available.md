---
title: "FreeBSD 13.1-RELEASE available"
date: "2022-05-18T09:46:49+1000"
abstract: "Dedicated to Bill Jolitz ♡"
thumb: "http://rubenerd.com/files/2020/beastie@2x.png"
year: "2022"
category: Software
tag:
- bsd
- freebsd
location: Sydney
---
Congratulations and a big thank you to the release engineering team. [FreeBSD 13.1-RELEASE](https://www.freebsd.org/where/#download-rel131) includes some newer Wi-Fi driver support (awesome!), OpenZFS has been updated to 2.1.4, and [plenty more](https://www.freebsd.org/releases/13.1R/announce/).

The release is dedicated to American software engineer Bill Jolitz, who left us in March this year:

> The FreeBSD Project dedicates the FreeBSD 13.1-RELEASE to the memory of Bill Jolitz, co-creator of 386BSD, which formed the basis of FreeBSD 1.0. We stand on the shoulders of giants.

I poured out one for him earlier this year, but I might do again while upgrading my homelab this weekend. ♡
