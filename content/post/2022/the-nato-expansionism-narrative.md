---
title: "The NATO expansionism narrative"
date: "2022-03-21T08:32:09+11:00"
abstract: "I bristle when human beings are reduced to chess pieces."
year: "2022"
category: Thoughts
tag:
- politics
- ukraine
location: Sydney
---
I'm reading more journalists claim the Ukrainian invasion was due to "NATO expansionism". This is not a justification. Putin would be proud, but such takes never address two unavoidable corollaries:

1. Why would a country feel the need to join a defence alliance in the first place? Why were the Baltic states so keen to?

2. Why isn't the democratic will of people in countries like Ukraine ever a factor? Why does their fate always get decided for them?

I bristle when human beings are reduced to chess pieces, and especially when the pundits haven't bothered to think even one move ahead in the game they purport to cover. This isn't just laziness in wartime, it's journalistic malpractice.

*(UNICEF are running a [global Ukrainian support programme](https://donate.unicef.org/donate/now), which will redirect you to your local country's donation page 🌻)*.
