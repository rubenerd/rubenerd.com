---
title: "Chilling effects on defensive social media posts"
date: "2022-02-23T08:40:49+11:00"
abstract: "“I don’t want to deal with the bullies, the vitriol, the anger, the rage, the random creeps attacking me, the hate.”"
year: "2022"
category: Internet
tag:
- social-media
location: Sydney
---
Last Saturday I wrote about the need to [even post on social media defensively](https://rubenerd.com/defensive-social-media-posts/), because there will always be people reading your words in bad faith to troll and score Internet points.

Mike Harley of [Obsolete29](https://obsolete29.com/) chimed in:

> It's almost as if we're trying to include the anticipated rebuttal, right in our original post... a pre-buttal if you will. I do think your approach is about the only sane thing to do. Don't let bad faith actors occupy space in your head. Act in good faith. Be kind. Communicate your truth.
>
> Plus, who knows what's going on in people's lives, you know? Maybe some of them are miserable cunts or they're off their meds or their cat is missing.

That's true. I've written here before about giving people the benefit of the doubt, but it can still feel frustrating having the most innocuous posts turned into storms of criticism. The best we can do, as Mike says, is tell the truth and move on.

He also linked to [this post by Tracy Durnell](https://tracydurnell.com/2022/02/20/chilling-effects-vs-network-effects/) that quoted [What Facebook’s Crumbling Empire Teaches Us](https://eand.co/what-facebooks-crumbling-empire-teaches-us-fac133bd3201), emphasis added:

> What happened on Facebook was that chilling effects dominated network effects. Chilling effects meaning just the above: “Hey, maybe I shouldn’t post this today, because **I don’t want to deal with the bullies, the vitriol, the anger, the rage, the random creeps attacking me, the hate.**” And then that thought happens more and more often. Until it’s the norm. And suddenly, chilling effects have overcome network effects.

That's it in a nutshell. It's a learned behaviour, and discriminates against the timid and shy, not those in the wrong.
