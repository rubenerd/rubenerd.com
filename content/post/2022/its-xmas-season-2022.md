---
title: "It’s Xmas Season 2022"
date: "2022-12-01T14:59:48+11:00"
abstract: "What a yeah, huh?"
thumb: "https://rubenerd.com/files/uploads/media.tree-glitter.gif"
year: "2022"
category: Thoughts
tag:
- weblog
- xmas
location: Sydney
---
Time to break out the decorations once more! What a yeah, huh?

<figure><p><img src="https://rubenerd.com/files/uploads/media.tree-glitter.gif" alt="A festive Xmas tree, complete with distracting decorations" style="width:100px;"></p></figure>

