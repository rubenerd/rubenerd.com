---
title: "Brad Alexander on hostname schemes, sci-fi"
date: "2022-05-25T08:58:08+1000"
abstract: "Using ship registrations and classes is brilliant!"
year: "2022"
category: Software
tag:
- andromeda
- feedback
- hololive
- scifi
- star-trek
location: Sydney
---
I'm still getting through everyone's feedback over the last week, appreciate your patience! Today's comes from Brad concerns hostnames:

> I'll go you one better on the host naming scheme...Name your physical boxes after Star Trek ships... And the use their registry number as the pool name.
> 
> For instance, I have the following machines in my network:
> 
> defiant, 2 pools, NX70205, NCC1765   
> luna, pool NX80101   
> intrepid, 2 pools, NCC1631, NCC38907   
> 
> I would use Babylon 5, since it is clearly superior, but there were only a handful of ship names, and the White Stars were all numbered. :D

That's brilliant. There's an opportunity for fun when we start dealing with hostnames for hypervisors with guests, or for storage pools. The logical, rational, and boring thing would be to name them for their intended use, application, or a [FQDN](https://en.wikipedia.org/wiki/Fully_qualified_domain_name "Wikipedia: Fully-qualified domain name"). But where's the fun in that?

My most recent homelab machine got an upgrade from Xen to bhyve, which I've named holo for Hololive. This lets all the guests be named for my favourite Hololive streamers, like Ina, Watson, Bae, Kronii, Moona, Suisei, and Reine. Maybe Brad does something similar with ship names, but I also try to match the hostname with the personality of the streamer.

And as for Brad's assertion about science fiction, I don't *entirely* agree (!) but I can relate. I put Trek first because it's where I grew up, but I'd put Babylon 5, Andromeda, Red Dwarf, both Stargates, and far too much sci-fi anime like Battleship Yamato above Star Wars, for example. But that might just be because I don't like Star Wars.
