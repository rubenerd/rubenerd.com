---
title: "Waycross derailment in the US"
date: "2022-05-02T18:45:48+10:00"
abstract: "A train waiting in Folkston, Georgia had us looking up what was going on."
thumb: "https://rubenerd.com/files/2022/waycross-derailment-hoppers@1x.jpg"
year: "2022"
category: Thoughts
tag:
- railfan
- trains
- united-states
location: Sydney
---
[Remote railfanning](https://rubenerd.com/catching-a-live-gp38-3-in-florida/) took an unexpected turn last night US time, or early afternoon in Sydney.

Clara and I had [Virtual Railfan's Folkston camera](https://www.youtube.com/watch?v=LKQdO3H9F-8 "Folkston, Georgia, USA (Sponsored through May, 2022) | Virtual Railfan LIVE") in Georgia up on our TV while we worked today, and we couldn't help but notice a mixed-freight CSX train come to a stop. I saw the boom gates for the level crossing raise, indicating the train was going to be there for a while.

<p><img src="https://rubenerd.com/files/2022/waycross-derailment-folkston@1x.jpg" srcset="https://rubenerd.com/files/2022/waycross-derailment-folkston@1x.jpg 1x, https://rubenerd.com/files/2022/waycross-derailment-folkston@2x.jpg 2x" alt="Screenshot from Virtual Railfan's Folkston, Georgia webcam, showing a CSX freight diesel locomotive waiting on a rail line behind a level crossing." style="width:500px; height:280px;" /></p>

I was about to jump on and write a post about how I'd been learning more about how such signals work, before I saw someone in the live stream's chat window comment about a derailment up the line in Waycross.

We jumped over to [that stream by Virtual Railfan](https://www.youtube.com/watch?v=0tZ1LK7ckhQ "Waycross, Georgia USA | Virtual Railfan Live"), and saw several uncovered wagons sitting in the dirt next to the main line. Crews were already there with trucks and excavators. Note the exposed bogie wheels in the far left, just above the blue and white truck.

<p><img src="https://rubenerd.com/files/2022/waycross-derailment-hoppers@1x.jpg" srcset="https://rubenerd.com/files/2022/waycross-derailment-hoppers@1x.jpg 1x, https://rubenerd.com/files/2022/waycross-derailment-hoppers@2x.jpg 2x" alt="Screenshot from Virtual Railfan's Waycross, Georgia webcam, showing three derailed hoppers with an excavator and engineers in hard hats." style="width:500px; height:280px;" /></p>

It wasn't clear from the video what the train was carrying. I've watched enough rail video around Georgia and Florida by now to know it could have had ammonium nitrate fertiliser which... would not have been fun in a derailed car. That might have explained the crews hosing down the hoppers, not sure.

This is the [second time this year](https://www.trains.com/trn/news-reviews/news-wire/csx-train-derails-after-grade-crossing-accident/) a goods train has had an incident in the US state of Georgia. I hope the drivers were okay.

*Update:* Almost ten hours since the derailment, that other train is still waiting at that [Folkston level crossing](https://www.youtube.com/watch?v=LKQdO3H9F-8). That's a long night.
