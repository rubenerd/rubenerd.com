---
title: "Waking up to coffee shop chatter"
date: "2022-11-10T07:43:45+11:00"
abstract: "Overheard conversations, and a lovely painted mug."
thumb: "https://rubenerd.com/files/2022/pixloom-coffee@2x.jpg"
year: "2022"
category: Thoughts
tag:
- coffee
- life
location: Sydney
---
<p id="side-image"><img style="width:152px; height:164px; float:right; margin:0 0 1em 1em;" src="https://rubenerd.com/files/2022/pixloom-coffee@1x.jpg" srcset="https://rubenerd.com/files/2022/pixloom-coffee@1x.jpg 1x, https://rubenerd.com/files/2022/pixloom-coffee@2x.jpg 2x" alt="Photo of a blue painted coffee mug by Pixloom on Wikimedia Commons" /></p>

I [overheard](https://rubenerd.com/tag/overheard/) another conversation between the manager of this [coffee shop](https://www.apothecarycoffee.com.au/) and a customer this morning:

> I woke up today without back pain, so it's a great day.
>
> Beautiful. At my age, I *woke up today*, so it's a great day.

I miss when I used to use images on posts, so I went digging around Wikimedia Commons and found this lovely [hand-painted specimen](https://commons.wikimedia.org/wiki/File:A_coffee_cup_hand_painted.jpg) by Pixloom. I imagine Sparx from the old WWR days would have posted something like that.
