---
title: "The “fundamental problem” with NFTs"
date: "2022-02-14T16:14:09+11:00"
abstract: "Selling tokens of content that did not belong to them"
year: "2022"
category: Internet
tag:
- art
- blockchain
- scams
location: Sydney
---
[Reuters on the 11th of February](https://www.reuters.com/business/finance/nft-marketplace-shuts-citing-rampant-fakes-plagiarism-problem-2022-02-11/):

> The platform which sold an NFT of Jack Dorsey's first tweet for $2.9 million has halted most transactions because people were selling tokens of content that did not belong to them, its founder said, calling this a "fundamental problem" in the fast-growing digital assets market.

That's the understatement of the century! But it gets better:

> The biggest NFT marketplace [redacted], valued at $13.3 billion after its latest round of venture funding, said last month more than 80% of the NFTs minted for free on its platform were "plagiarized works, fake collections and spam".

Who'd have thunk it!? Next they'll be telling me my hats look silly. Wait, shaddup.
