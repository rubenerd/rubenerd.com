---
title: "Switching to black coffee/tea as we get older"
date: "2022-08-29T10:08:30+10:00"
abstract: "I made the switch for coffee in my early 20s, but I haven’t ever had milk in tea I make at home."
year: "2022"
category: Thoughts
tag:
- coffee
- tea
location: Sydney
---
Part of our morning company meeting was spent talking about coffee and tea preferences, and how they change as we get older.

I switched to black coffee in my early 20s for specific reasons, but it's also been wonderful in other ways. Being able to taste the differences in beans, regions, roasting techniques, and preparation approaches is *fun!* You can make milky coffee that's optimised for specific bean types, but I feel a lot more of the profile and uniqueness of each cup shines through when you have it straight.

"Optimised"... I just made coffee sound like software.

That said, even a few years ago I would have still considered a cappuccino or mocha a sneaky treat to have on special occasions, or for cheering up. Today, the regret sets in almost immediately after I've taken the last sip. It's *heavy*. I wonder if that has anything to do with not eating cereal anymore, or living with a Cantonese person and having a reduced dairy intake in general?

Tea is interesting, because the preparation also makes all the difference. I do have a sneaky [teh halia](https://en.wikipedia.org/wiki/Teh_tarik) or bubble tea sometimes, but tea I brew at home has never had milk. This might be more to do with me being used to bitter drinks, but every time I've made milky tea all I can taste is the former.

I've never really thought about how amazing it is that we've come up with so many ways to prepare these drinks.
