---
title: "BitTorrent language"
date: "2022-11-28T14:34:50+11:00"
abstract: "“You unchocked the peer but the peer is not interested.”"
year: "2022"
category: Software
tag:
- bittorrent
- language
location: Sydney
---
The nomenclature surrounding the BitTorrent protocol has always been a bit weird; much like the word *nomenclature*. 

From the Transmission client today:

> You unchocked the peer but the peer is not interested.

I took the Chevy to the levee but the levee was dry? Sounds a bit like kink shaming to me. Nomenclature.
