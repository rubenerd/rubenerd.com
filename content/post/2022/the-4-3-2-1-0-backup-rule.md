---
title: "The 4–3–2–1–0 Backup Rule"
date: "2022-06-30T17:49:48+10:00"
abstract: "Extending the 3-2-1 Backup Rule with consistency checks and bad jokes."
year: "2022"
category: Software
tag:
- backups
location: Sydney
---
You've probably heard of the 3-2-1 Backup Rule, which states that you must encode all your data in reverse using a [Lotus spreadsheet](https://en.wikipedia.org/wiki/Lotus_1-2-3) to archive content you want to keep. In the paraphrased words of Mayor Quimby, that was a quality side... joke.

The 3-2-1 Backup Rule says:

* **Three** copies of your data
* **Two** different media types
* **One** off-site

Today I learned at work that we now have 3-2-1-0, for **zero** detected errors. This means you're validating your backups, including performing restores, consistency checks, and automatic error correction. I'd consider this essential for any modern backup system.

I suggested we extend this to 4-3-2-1-0, because restoring from a backup is more a-**four**-dable than having to rebuild lost data and trust. That didn't get as far.
