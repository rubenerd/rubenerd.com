---
title: "Portable digital audio players still exist"
date: "2022-06-11T07:49:27+10:00"
abstract: "Learning about the world of high-resolution DAPs"
year: "2022"
category: Hardware
tag:
- apple
- dap
- music
- sony
location: Sydney
---
I've been overjoyed at the level of feedback and interest so many of you still have for portable music players.

Unbeknownst to me when I wrote my most recent post, there's still an entire industry of what are now dubbed *digital audio players*, or DAPs. They've continued to evolve and add features since the iPod blitzed the industry and left with a whimper.

DAP manufacturers have (largely) conceded the general portable music market to the almighty smartphone, just as music stores are starting to with streaming services. I've said this was inevitable, though I don't have to like it.

I was worried that the market solution would be mass-produced knockoffs, which admittedly has happened (just look at eBay). But manufacturers like Sony have continued to remain viable and even increase their install bases for music players by moving upmarket, and innovating with features even phones don't have.

The biggest of these is high-resolution playback, and the kit to render them with high fidelity. FLAC files now regularly exceed the bit and sample rates of compact discs, and you can now play these on the go with good headphones. Reviewers compare the sound quality of the players based on their DACs, chipsets, and other circuitry, just as people reviewed hi-fi setups in years past.

Another *very* intriguing feature is their ability to be used as external USB DAC. This is brilliant; someone buying a DAP in 2022 is interested in sound quality, so giving them the option to improve output from their computer serves an additional need.

Had Apple continued to innovate rather than resting on its anodized laurels, I wonder if they could have continued to add meaningfully to this space as well? It's interesting to think what a true 2020s iPod DAC would look like. Likewise, the moribund iTunes Store could have been upgraded with premium lossless files for those who don't want streaming. They're probably one of the few companies that *could* pull it off, assuming they got their interest in the pro and enthusiast markets back.

I've admitted here that I can't tell the difference between a FLAC file, and a 320 kb/s MP3 encoded from the same source when performing a double-blind test, even with AKG monitors and a home hi-fi setup. But while I won't be loading up a portable player with lossless audio files any time soon, it's *exciting* to see such interest. It means people do still care about buying and listening to music, which is worth even more than extra bits.

Based on some feedback you all provided, I bought a modern Sony Walkman DAP recently, a review of which is coming soon. While I wait for it to arrive, I'm organising my local music collection in preparation for syncing with it. Music is fun!

