---
title: "The encouraging pushback against game NFTs"
date: "2022-12-28T08:10:19+11:00"
abstract: "They’re not an asset to a game, they’re a liability."
year: "2022"
category: Internet
tag:
- blockchain
- nfts
- scams
location: Sydney
---
Brian Feldman wrote a [great article in October](https://www.bloomberg.com/news/features/2022-10-17/how-gamers-beat-nfts) about the game industry's struggle to incorporate NFTs into their offerings... and not just for technical reasons:

> The NFTs would cost players real money and, he said, cost him both customers and his self-respect.

This is exactly what we all said would happen, but I'm still encouraged seeing it called out. The article goes on to explore the kinds of businesses that made bets on this stuff:

> But for the better part of the past year and a half, a whole lot of people in the games business thought NFTs sounded great.

Those people were misinformed, unqualified, or had ulterior motives. There was enough information about NFTs, web3, and other psuedo-decentralised puffery that anyone in a position of authority or responsibility voicing any kind of interest should be immediately dismissed. It's the same people who question vaccines in 2022, or think the jury is still out on homeopathy.

> Established companies have often framed NFT projects as decentralized, revolutionary, bottom-up wealth creation. They are, somehow, not being ironic.

[Folding Ideas did a great video](https://www.youtube.com/watch?v=YQ_xWvX1n9g) deconstructing how NFTs were exclusionary, entrenched existing inequalities and power structures, and would just as likely be used by the large corporations they're trying to disrupt to lock people in. The technical limitations, constraints, and shortcomings are almost secondary.

I vote with my wallet and free speech; I avoid any game or system with an NFT component, and encourage others to do the same. Fortunately, this has been trivial to do!

This system is going to collapse under the weight of its own technical debt and hubris, and it'll hurt a lot of people in the process. Let's get it over with so we can start solving problems earnestly again.
