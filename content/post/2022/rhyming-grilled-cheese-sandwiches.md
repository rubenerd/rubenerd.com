---
title: "Rhyming grilled cheese sandwiches"
date: "2022-05-12T20:19:25+10:00"
abstract: "Synergistic, disruptive paradigms."
year: "2022"
category: Thoughts
tag:
- grilled-cheese-sandwiches
location: Sydney
---
It's been over half a year since a post mentioned [grilled cheese sandwiches](https://rubenerd.com/tag/grilled-cheese-sandwiches/) here, and more than fourteen years since it was first mentioned.

This is a list of all the words and phrases I can think of that don't rhyme with the aforementioned comestible:

* Metonymy
* Progression
* Synergistic, disruptive paradigms

And while I'm at it, is a phrase with five words.
