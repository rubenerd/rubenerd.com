---
title: "Superficial Linux distribution reviews"
date: "2022-08-03T16:51:06+10:00"
abstract: "Rising above the fray of churn factories producing low-quality clickbait!"
year: "2022"
category: Software
tag:
- linux
- videos
- youtube
location: Sydney
---
Google's search results for software projects, technical announcements, and questions have steadily been reduced to a mountain of spam and duplicate content wrapped with ads. The remaining original material also tends to be hastily produced, with only a superficial exploration and grasp of the topic they discuss before moving onto the next thing.

This was inevitable, given how online incentives are structured. It's also slowly creeping into other places, as [nixCraft commented](https://twitter.com/nixcraft/status/1554168420358270980)\:

> Most Linux distro reviews on YouTube are really only desktop environment reviews.

It's true. Search for popular Linux distributions like Ubuntu or Fedora, and you'll be shown around a desktop running in a virtual machine.

They rarely mention:

* how they're booted (UEFI, BIOS, laptops, desktops, etc)
* how they're installed, beyond mounting an ISO
* physical hardware compatibility (does suspend/resume work?)
* release schedules (rolling, snapshots, based on RHEL...)
* package managers, or alternative processes
* drivers (open source and binary blobs)
* toolchains
* documentation (official, wikis)
* the community (where they are, friendly to newcomers?)
* practical licencing considerations
* does it run popular application XYZ
* what the upgrade process looks like
* and so on.

These define the ethos and practical applications of a distribution, and get to the interesting questions and points of difference people care about.

I don't mean to criticise everyone here. For every churn factory producing low-quality clickbait (the Linux equivalent of those 5 Minute Craft lifehack videos), there are others who are breaking into the space and wanting to share their journey. We should encourage this! I just think with only minor tweaks and a few additional ideas, this content could be *way* more useful.

We need more authentic voices if we're ever going to be heard over spammers.
