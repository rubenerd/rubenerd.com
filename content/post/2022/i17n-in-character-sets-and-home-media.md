---
title: "I17n in character sets and home media"
date: "2022-03-20T08:15:10+11:00"
abstract: "It’s one of those things you don’t notice when it’s done well, and becomes painfully obvious when it isn’t."
year: "2022"
category: Software
tag:
- asia
- cantonese
- chinese
- japanese
- korean
- internationalisation
- utf-8
location: Sydney
---
Internationalisation is one of those things you don't need to think about when it's done well, and becomes painfully obvious when it isn't.

Clara's and my home media server runs a combination of MusikCube, Plex, and some simple Netatalk and Samba shares for exporting content to our various devices. I didn't realise just how much of the [CJK](https://en.wikipedia.org/wiki/CJK_characters "Chinese, Japanese, and Korean characters") spectrum we use:

* Hong Kong cinema, which uses traditional Chinese
* Singaporean drama, which uses simplified Chiniese
* Anime and live-action Japanese shows and movies
* Korean movies

The underlying FreeBSD OS and storage pools are all configured for UTF-8 and Unicode Form D normalisation, so I haven't run into any issues. I think this is worth calling out and appreciating.

It doesn't seem that long ago where even having reliable Hangul on an English/Latin character set machine was a dicey proposition, with plenty of question marks and squares if you attempted to list directories containing them. Every part of the chain has to gracefully handle UTF and i17n, from the OS and file system, to the application and networking, or the entire framework collapses.
