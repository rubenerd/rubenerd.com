---
title: "A local planter now has a lovely plant"
date: "2022-08-08T10:18:47+10:00"
abstract: "These concrete alcoves used to be everywhere, but end up becoming voids."
thumb: "https://rubenerd.com/files/2022/planter@1x.jpg"
year: "2022"
category: Thoughts
tag:
- nature
- plants
location: Sydney
---
An building I walk past every day either has a new friend, or I'm only just noticing it. Either way, this is great:

<figure><p><img src="https://rubenerd.com/files/2022/planter@1x.jpg" alt="Photo of a small plant growing out of mulch on the ground floor of a building." style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2022/planter@1x.jpg 1x, https://rubenerd.com/files/2022/planter@2x.jpg 2x" /></p></figure>

These concrete alcoves walls seemed to be a fixture of 1980s and 1990s architecture. My school in Singapore had them, as have a few of the apartment buildings I've lived in over there and in Australia.

The problem is, they tend not to age well. As people stop maintaining the plants, or the sprinkler systems fall into disrepair, the plants disappear and are replaced with nothing. This leads to concrete voids which end up attracting litter, dust, and echoes (echoes... echoes). Seeing an older building with honest to goodness plants again made me smile.

