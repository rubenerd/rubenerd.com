---
title: "Long forms select for those who can write"
date: "2022-03-22T10:06:06+11:00"
abstract: "Ilsem’s Canonicial job application form is a perfect illustration of infocomm’s broken hiring process"
year: "2022"
category: Software
tag:
- ethics
- employment
- infocomm
location: Sydney
---
By now you might have seen <a rel="nofollow" href="https://old.reddit.com/r/WorkReform/comments/th5eou/this_was_the_first_step_in_the_interview_process/">Ilsem's Canonicial job application form</a>, with its millions of questions. If this screenshot is true, it's emblematic of the wider broken infocomm hiring process in the States which is starting to leak elsewhere. If it's fake, it perfectly captures people's frustration and despair over this process.

Even if they were deemed necessary, these sorts of forms don't select for good candidates. Qualified people will do what the original poster did and close the window. I imagine Brad Alexander (whom I still owe an email reply!) would mention such a form contains nothing but red flags, and you should move on immediately.

I'm lucky to have avoided such processes, despite having worked at large corporates before. But having rented in Australia, I'm familiar with filling out longwinded forms with nonsense.

*Why do I think I'd be a good tenant for this property? Well you see, I'm not after mere accommodation; someone of my stature and financial prudence engages in synergistic paradigm shifts using a disruptive mix of reliable rent payments and unlocked value potential!*

Forms with open-ended questions select for those who can write. As you've no doubt noticed on this blog, I can smash out paragraphs quickly without too much mental overhead or thought. I could probably fill out that entire job form in less than an hour, with all the shallow buzzwords and insincere fluff they're looking for. Would I mean any of it? Unlikely; I wouldn't even *remember* most of it after writing.

I get that recruitment is difficult. I work at a small company right now where I've seen the process from the beginning to end, including when it goes wrong. But it has to start with respect.
