---
title: "What a FreeBSD/KDE user misses on macOS"
date: "2022-01-29T10:14:34+11:00"
abstract: "Responding to a few Twitter comments."
year: "2022"
category: Software
tag:
- bsd
- freebsd
- mac
- macos
location: Sydney
---
[Ed Maste asked](https://twitter.com/ed_maste/status/1486756241431007251) what everyone's favourite changes were to FreeBSD over the last year. I replied that FreeBSD 13 was a great release for the desktop, and that coupled with KDE even made me miss stuff when I'm working on macOS.

A [few people asked](https://twitter.com/Rubenerd/status/1486813114041470978) me to expand on what I meant by that. There's probably nothing surprising here, but here goes:

* **OpenZFS 2.0** in base is a game changer. I use datasets and snapshots so frequently when testing and building new things, going back to the Mac feels like I'm stepping back to DOS. Yes you can use ZFS on Mac, but it's not as seamless or integrated.

* Plasma lets me **arrange my desktop how I want** For the most part I'm fine with macOS's layout, but Plasma panels and the [Latte dock](https://store.kde.org/p/1169519) are more flexible.

* **KDE's application ecosystem**. Dolphin runs laps around the modern macOS Finder, for example. I use so many KDE and Qt applications on macOS, but they'll always have better integration on Plasma.

* For the most part, each release of Plasma and FreeBSD feel like they're **heading in the right direction**, technically and from a design perspective. I used to feel that way about the Mac, but not for a while.

* **Feedback**. I know that if I hit a problem, I can contact the FreeBSD or KDE maintainers.

Some of this also applies to KDE on Debian, [which I run for games](https://rubenerd.com/the-current-state-of-linux-gaming/).

As with all this stuff, your mileage may vary. I'm only speaking as someone who came back to KDE after many years, and after running macOS since the mid-1990s. But that's the great thing about anecdotes, everyone has one.
