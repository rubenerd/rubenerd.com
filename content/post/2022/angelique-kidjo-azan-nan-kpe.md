---
title: "Angélique Kidjo, Azan Nan Kpe"
date: "2022-05-23T19:56:06+10:00"
abstract: "This week’s Music Monday."
thumb: "https://rubenerd.com/files/2022/yt-aDDK5GBlPVk@1x.jpg"
year: "2022"
category: Media
tag:
- music
- music-monday
location: Sydney
---
Today's *[Music Monday](https://rubenerd.com/tag/music-monday/)* comes via the random button in my music player, and to my childhood in the 1990s. I always thought the harmony was beautiful.

<p><a target="_BLANK" href="https://www.youtube.com/watch?v=aDDK5GBlPVk" title="Play Azan Nan Kpe"><img src="https://rubenerd.com/files/2022/yt-aDDK5GBlPVk@1x.jpg" srcset="https://rubenerd.com/files/2022/yt-aDDK5GBlPVk@1x.jpg 1x, https://rubenerd.com/files/2022/yt-aDDK5GBlPVk@2x.jpg 2x" alt="Play Azan Nan Kpe" style="width:500px;height:281px;" /></a></p>

This was my favourite of her albums [back in 2007](https://rubenerd.com/all-work-and-no-play-makes-ruben-need-coffee/), and it might still be.

