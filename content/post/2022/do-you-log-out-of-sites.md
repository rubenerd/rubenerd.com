---
title: "Do you log out of sites?"
date: "2022-10-02T11:00:17+10:00"
abstract: "I do, but I doubt many others do anymore."
year: "2022"
category: Internet
tag:
- security
location: Sydney
---
Here's a mental exercise. When was the last time you logged out of a site? Not closed the tab, or put your laptop to sleep, or waited for a timeout; but pressed a *log off* button to end a website session?

If your answered *recently*, do you think any of your colleagues, classmates, friends, or family still do?

I ask, because I'm starting to think it's unusual. It used to be accepted security wisdom to log out when you were done with a session, but once again I feel like I'm an outlier doing it.

I can see three reasons for this:

1. Sensitive sites, like online banking and the tax office, will **log you out automatically** after a period in activity. People have become used to this. I worry, because it trains people to think *all* sites do this, when they most definitely don't.

2. Sites like Facebook are **incentivised to keep you logged in** to track and monetise your activity. This is why Messenger was such a masterstroke; you *have* to be logged in all the time to get IM-style messages on their site.

3. Logging in has **become a pain.** [Split login forms](https://rubenerd.com/update-on-the-separate-username-password-form-anti-pattern/), CAPTCHAs, insecure SMS-based MFA systems that time out delivering codes, and confirmation emails because... wait, you log out sometimes? *That's unusual activity!* And the solution? Use your federated login from point 2!

Some of this comes down to security or bad design, either [deliberately](https://www.deceptive.design/), or because people [don't prioritise accessibility](http://www.takingnotes.co/blog/2018/11/09/react-native-accessibility/) with alarming regularity.

Still, I thought it was an interesting shift.

