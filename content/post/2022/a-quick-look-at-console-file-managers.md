---
title: "A quick look at console file managers"
date: "2022-05-17T07:23:59+10:00"
abstract: "Midnight Commander, Ranger, nnn, fff, and lf."
year: "2022"
category: Software
tag:
- bsd
- freebsd
- illumos
- linux
- netbsd
- file-managers
- lists
location: Sydney
---
I still consider [XTreeGold](https://en.wikipedia.org/wiki/XTree) some of the best utility software ever written, and it still makes using my vintage PCs a joy. I've been looking again if there are modern equivalents, especially for exploring file systems and performing batch tasks.

Here's what I have so far:

* [Midnight Commander](https://midnight-commander.org/) mimics the orginal Norton Commander for DOS, with a text editor and familiar blue interface. I love that this exists, though I'll admit I keep it around mostly for nostalgia.

* [Ranger](https://github.com/ranger/ranger) arguably spawned the current next-gen console file managers, and its where I've stuck around. It uses a column interface like the macOS Finder which makes visualising directory trees easy, and I still think it's the easiest to configure.

* [nnn](https://github.com/jarun/nnn) is a multi-modal file manager with numbered tabs, file icons, and even a disk usage visualiser. I need to give it more of a look.

* [fff](https://github.com/dylanaraps/fff) is is the most recent one I've looked at. It's genuinely impressive how much functionality and performance has been wrung out of pure bash scripts.

* [lf](https://github.com/gokcehan/lf) feels like a snappier Ranger with a broad subset of its features included in a single binary file. I could see myself installing this on servers, and using Ranger on the desktop.

Let me know if you have other suggestions, I may be slightly obsessed with the concept right now.
