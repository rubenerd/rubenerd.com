---
title: "Comparing vaccine hesitancy to tobacco"
date: "2022-06-16T11:13:20+10:00"
abstract: "It’s not a perfect analogy, but it could be useful to approach the two in a similar way."
year: "2022"
category: Thoughts
tag:
- covid-19
- health
location: Sydney
---
Benjamin Mazer [wrote a column in The Atlantic](https://www.theatlantic.com/health/archive/2022/02/covid-anti-vaccine-smoking/622819/) back in February that I haven't been able to stop thinking about. He puts forward the case that avoiding Covid vaccines, in light of overwhelming medical evidence, can be compared to smoking.

> This is where the “new normal” of COVID might come to resemble our decades-long battle with tobacco. We should neither expect that every stubbornly unvaccinated person will get jabbed before next winter nor despair that none of them will ever change their mind. 

He admits it's not a perfect analogy; tobacco is addictive, and second-hand smoke is a somewhat less direct consequence than actively, and willingly, spreading a dangerous virus among other people. But it could be useful to approach the two in a similar way.

>  We haven’t banned tobacco outright—in fact, most states protect smokers from job discrimination—but we have embarked on a permanent, society-wide campaign of disincentivizing its use. Long-term actions for COVID might include charging the unvaccinated a premium on their health insurance, just as we do for smokers, or distributing frightening health warnings about the perils of remaining uninoculated. And once the political furor dies down, COVID shots will probably be added to the lists of required vaccinations for many more schools and workplaces.

Societies have agreed to tolerate smoking, because the freedom to light up trumps the freedom of bystanders to not breathe carcinogens in public. But rights come with responsibilities. Or put another way, if you choose to be a danger to yourself and others, you should expect consequences proportional to your impact.

The education campaign around smoking was also effective in reframing the debate. Tobacco companies lied for decades to downplay health effects; now smokers talk about their freedom to engage in the habit, rather than arguing that it's healthy.

Covid is still in that early lobbying state, where people are denying it exists, that its effects are overblown, or that the vaccines will give them 5G. The debate needs to shift to the same informed consent that smokers have arrived at.

