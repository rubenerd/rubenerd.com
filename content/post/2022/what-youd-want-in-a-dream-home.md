---
title: "What you’d want in a dream home"
date: "2022-10-14T11:53:23+11:00"
abstract: "A teleportation room would be rather neat."
year: "2022"
category: Thoughts
tag:
- fantasy
- lists
location: Sydney
---
@yaakov_h shared what [he'd want in his dream home](https://twitter.com/yaakov_h/status/1580716150756737024), regardless of budget or worldly constraints. I answered with my own:

1. **A view of the [Kansai region](https://en.wikipedia.org/wiki/Kansai_region) or [Outram Park](https://en.wikipedia.org/wiki/Outram_Park)**. I wouldn't be able to decide, so I leave that as an exercise for the person granting me this ridiculous residence.

2. **A secret retro computer room** you get to through a bookshelf or something. It'd need to have period-correct wood veneer furniture, plenty of Thinnet cable, and a proper frame for my broken Amiga 500 lightbox. A dedicated set of peripherals for each computer, rather than my current awful KVM spider web would be *glorious*.

3. **His spaceport idea**. I believe this goes without saying.

I stand by these, but I'd also include:

1. **Interior decoration by [@baxters](https://twitter.com/baxters)**. If I have a say in the floor plan, I'd also defer this entirely to his expertise and eye. If you've never seen photos of his space on Twitter, it's beautiful. Like him.

2. An **automatic bed-making machine** that also handles folding fitted sheets and putting on pillow covers.

3. A four rack **mini data centre room** with raised floors and redundant cooling. I have the hardware, hypervisor, and storage stuff down, but I may also need a network engineer on retainer.

4. A bar fridge-sized, self-contained **fusion reactor** for power and central heating/cooling. I could stash it in the laundry; the humming would just sound like the washing machine or dryer.

5. A brick wall in the main loungeroom, some of which are **fake bricks with little drawers**! I had a dream about this as a kid, and I've been fixated on the idea ever since.

And for connections:

1. **A private train station doorway** direct to a [Hankyū rail line](https://en.wikipedia.org/wiki/Hankyu#Rail_lines) or [MRT](https://en.wikipedia.org/wiki/Mass_Rapid_Transit_(Singapore)). Again, depending on which one I get in the first point above. I'd even accept a ticket gantry for the convenience.

2. **A teleportation room** that can be reprogrammed for our favourite cities. Right now that'd be Adelaide, Arashiyama, Kudanshita in Tōkyō, KL, Manhattan, Namba in Osaka, San Francisco, and Sydney. This may need to be reprogrammed and expanded as we visit more places.

3. **A Minecraft portal** that bridges Clara's and my Most Serene State of Kiriben virtual world with the real world. We have mobs disabled, so I couldn't ever foresee\* an issue here.

I asked Clara what else she'd want in this place:

1. A small **Prince Cat room**, full of nothing but Prince Cat soft toys and wardrobes of their clothing.

2. A **manga library** with comfy couches.

3. A self-cleaning **craft room** with a giant resin/wood table commissioned from Blacktail Studios, with her sewing machines, Wacom tablets, pens/markers, fabric, and All The Things.&trade;

