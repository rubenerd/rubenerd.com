---
title: "Client-side name and location validation"
date: "2022-02-24T09:28:16+11:00"
abstract: "Also applies to bad client-side validation code for locations."
year: "2022"
category: Internet
tag:
- accessibility
- design
- singapore
location: Sydney
---
There are still infocomm designers, developers, and managers who believe tech is intrinsically neutral, despite what I'd consider a transparent preponderance of evidence to the contrary!

NetBSD extraordinaire Benz of [Bentsukun.ch](https://bentsukun.ch/) ([RSS feed](https://bentsukun.ch/index.xml)) reminded me of Patrick McKenzie's 2010 post about [false assumptions made of names](https://www.kalzumeus.com/2010/06/17/falsehoods-programmers-believe-about-names/). These were partly informed by Patrick's experience living in Japan; I've had a similar experience from time in Southeast Asia. I'm sure Benz has the added fun of living in a multilingual country who's forced to interact with software developed by English-speakers.

The example I always cite is the last name Ng, which is common in Singapore. I remember one CRM back in the mid-2010s that refused to accept it based on:

* insufficient length, and
* the fact it "wasn't a valid name"

Digging into the rats nest of client-side validation code, I realised it was checking for the presence of a vowel if the supplied name used Latin characters. Because naturally, every word and name has to have one. Except the ones that don't.

*Trombone sounds!*

This also applies to locations. So many systems assume people have a city, state, and country. It's a running joke among Singaporeans that you need to receive deliveries for Singapore City, Singapore State, Singapore, or Singapore City, Singapore, Republic of Singapore, because a surprising number of sites still assume:

* all three exist in one jurisdiction, and that
* a city and country can't be synonymous or similar

Luxembourg and Singapore are out of sight and out of mind for most of these developers, but you'd think Mexico City, Mexico would be an obvious counterexample to them that's closer to home. What about Texas City, Texas? Or Kansas City, Kansas? Wait, scratch that last one.

The defence I hear of these systems is that they're edge cases, as if being wrong about a chunk of the world's population is technically excusable, if not morally defensible. In the formal vernacular of my two home countries: *yeah, nah* and *wa lao eh*.
