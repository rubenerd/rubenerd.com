---
title: "rjc shows Minecraft running on OpenBSD too"
date: "2022-07-20T08:56:11+10:00"
abstract: "They also include rc.d init files."
year: "2022"
category: Software
tag:
- bsd
- games
- minecraft
- openbsd
location: Sydney
---
I wrote about how Clara and I run a [Minecraft server on FreeBSD](https://rubenerd.com/how-we-run-a-minecraft-server/), and how it [also works great on NetBSD](https://rubenerd.com/netbsd-can-also-run-a-minecraft-server/). But this leads to an obvious corollary: does it run on OpenBSD?

Fortunately, [rjc wrote an entire process](https://dataswamp.org/~rjc/running_minecraft_server_on_openbsd.html). They even go through the process of setting up an `rc.d` file to start and stop. Awesome :).



