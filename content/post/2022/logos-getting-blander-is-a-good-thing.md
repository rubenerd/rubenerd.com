---
title: "Blander logos are good, via @NeilIreland"
date: "2022-09-30T13:47:41+10:00"
abstract: "“Brands exist to trick us into parting with our money or our good judgement.”"
year: "2022"
category: Thoughts
tag:
- economics
- neil-ocarroll
location: Sydney
---
Last year I wrote about [MediaWiki's new logo](https://rubenerd.com/mediawikis-new-logo/), and talked of the general trend among logo designers towards bland, dull, uninteresting, unoriginal, and largely interchangeable marks. 

Neil of the imitable [Matchstick Cats](https://www.matchstickcats.com/) and the late great [IntoYourHead](https://www.matchstickcats.com/into-your-head-podcast-2006-2016/) podcast (I miss joining him at those incredibly exciting moments), [sees the positive](https://twitter.com/NealIreland/status/1575682131569029121)\:

> Good. Brands exist to trick us into parting with our money or our good judgement. If they're less good at creating then that's a welcome development.

He's onto something. [Just Creative](https://justcreative.com/why-do-everyones-logo-fonts-look-the-same/) linked logo design to the current global climate, dubbing them "reblands":

> [&hellip;] the Great Recession also contributed to a reduced appetite for all things excessive. While this can easily be seen on the fashion and decor side, all aspects of design are inextricably linked — logo fonts included.

I happen to think (that's dangerous) that's a good thing. Less stuff we don't need, spent with money we don't have, to impress people we don't like, the better.
