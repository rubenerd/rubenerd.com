---
title: "Handwritten HTML sites"
date: "2022-08-28T11:59:34+10:00"
abstract: "Ton Zijlstra ponders whether Notepad is still the answer. It kinda is."
year: "2022"
category: Internet
tag:
- html
location: Sydney
---
Back in late June, Ton Zijlstra was pondering the question about [how best to make a small site](https://www.zylstra.org/blog/2022/06/after-3-decades-notepad-cant-still-be-the-answer-can-it/)\:

> I was mildly shocked that my own first instinctive answer [&hellip;] is “write my own html in Notepad". That answer is almost 30 years old, it’s how I made my very first web page. And handwriting html is still my first answer! No other path immediately comes to mind. Of course, I wouldn’t want to hand write this weblog in html, but ‘small site’ as in a few simple pages, yes, I would do that by hand in some plain text app like Notepad.
>
> Can it be that three decades on the closest answer to ‘making a website is as easy as making a plain text note’ is still hand written html?

Mine answer is the same, though like him I don't run modern Windows. The tool might have changed, but I still write basic HTML sites by hand. It has to span dozens of pages, need editing by other people, or require interactive features for me to do it any other way.

It's comforting knowing that it's still possible to use a plain text editor to create a page, then upload it, and have it viewable by people around the world.

