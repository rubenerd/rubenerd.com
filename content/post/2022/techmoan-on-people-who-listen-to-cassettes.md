---
title: "Techmoan on people who listen to cassettes"
date: "2022-06-06T12:32:42+1000"
abstract: "Who wants to play cassettes anymore!? People who want to play cassettes do."
thumb: "https://rubenerd.com/files/2022/techmoan-cassettes-02@1x.jpg"
year: "2022"
category: Media
tag:
- comments
- music
- quotes
- techmoan
- trolls
location: Sydney
---
<figure><p><a href="https://youtu.be/rfC8TGROZRw?t=1653"><img src="https://rubenerd.com/files/2022/techmoan-cassettes-01@1x.jpg" alt="There's always that chap in the comments that says: who wants to play cassettes anymore?!" style="width:500px; height:281px;" srcset="https://rubenerd.com/files/2022/techmoan-cassettes-01@1x.jpg 1x, https://rubenerd.com/files/2022/techmoan-cassettes-01@2x.jpg 2x" /></a><br /><img src="https://rubenerd.com/files/2022/techmoan-cassettes-02@1x.jpg" alt="And my answer to that is always: people who want to play cassettes do." style="width:500px; height:281px;" srcset="https://rubenerd.com/files/2022/techmoan-cassettes-02@1x.jpg 1x, https://rubenerd.com/files/2022/techmoan-cassettes-02@2x.jpg 2x" /></p></figure>

