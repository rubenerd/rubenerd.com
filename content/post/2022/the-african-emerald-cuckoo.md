---
title: "The African emerald cuckoo"
date: "2022-04-03T19:22:17+10:00"
abstract: "We haven’t had a featured bird picture from Wikipedia in a while."
thumb: "https://rubenerd.com/files/2022/african-emerald-cuckoo@1x.jpg"
year: "2022"
category: Media
tag:
- birds
location: Sydney
---
We haven't had a [featured bird picture](https://rubenerd.com/tag/birds/) from Wikipedia in a while! This beautiful avian specimen is the African emerald cuckoo, taken by Charles J. Sharp.

<p><img src="https://rubenerd.com/files/2022/african-emerald-cuckoo@1x.jpg" srcset="https://rubenerd.com/files/2022/african-emerald-cuckoo@1x.jpg 1x, https://rubenerd.com/files/2022/african-emerald-cuckoo@2x.jpg 2x" alt="Photo of the African emerald cuckoo by Charles J. Sharp" style="width:320px; height:320px;" /></p>

[From Wikipedia](https://en.wikipedia.org/wiki/Bruszewo-Borkowizna "Wikipedia article on Bruszewo-Borkowizna")\:

> Bruszewo-Borkowizna [bruˈʂɛvɔ bɔrkɔˈvizna] is a village in the administrative district of Gmina Sokoły, within Wysokie Mazowieckie County, Podlaskie Voivodeship, in north-eastern Poland.

That's clearly the wrong article. [Let's try again](https://en.wikipedia.org/wiki/African_emerald_cuckoo "Wikipedia article on the African emerald cuckoo")\:

> The African emerald cuckoo (Chrysococcyx cupreus) is a species of cuckoo that is native to Africa. As a member of the Cuculidae genus, the African emerald cuckoo is an Old World cuckoo. There are four subspecies ... [and] its range covers most of sub-Saharan Africa.
