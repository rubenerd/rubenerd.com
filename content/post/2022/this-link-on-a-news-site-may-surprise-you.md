---
title: "This news site link may surprise you"
date: "2022-09-26T17:23:33+10:00"
abstract: "These Credit Cards That Don't Require A Credit Check!"
year: "2022"
category: Internet
tag:
- spam
location: Sydney
---
Remember when [chumboxes](https://en.wikipedia.org/wiki/Chumbox) like Outbrain and Taboola started infesting news sites with low effort clickbait at the bottom of articles? I just went to another news site with my content blockers disabled, and here are some highlights:

* This SUV Sets A New Standard

* The Most Addictive Series Since 50 Shades of Grey

* These Credit Cards That Don't Require A Credit Check

* Always Tired? Just Do THIS Once Each Day

* Solar Panels: 7 in 10 Aussies Don't Know This

Sites employ dark patterns because they generate income. Until the financial incentives are flipped or no longer exist, expect to see more of these amazing, trendsetting standards that twelve out of five of us don't know about.
