---
title: "Starting a blog, and unoriginal ideas"
date: "2022-02-15T21:24:34+11:00"
abstract: "You should start a blog, now!"
year: "2022"
category: Thoughts
tag:
- writing
location: Sydney
---
Alexey Guzey laid out [a powerful case](https://guzey.com/personal/why-have-a-blog/)\:

> Summary: in this post I explain why you should start a blog (to help others and to help yourself), what to write about, and how to start it. I hope to persuade you that you should start a blog even if you feel that you have nothing to say and even if almost nobody will read it.

The post is chock full of great advice, links, and writing prompts.

But that wasn't the most useful part for me. In 2020 I talked about how [uncomfortable I was](https://rubenerd.com/with-whom-i-normally-disagree/) sharing a few opinions with unpleasant people:

> I’ve grappled with this idea that I can agree with people with whom I otherwise don’t, and even those I consider reprehensible. How does one reconcile this?

Jim Kloss of Whole Wheat Radio replied saying there isn't an original thought in the universe, and that we're the sum of what we've absorbed and lived. Rebecca Hales noted that even stopped clocks are right twice a day.

Alexey takes this one step further to say that your ideas are more original than you think. He quotes Gleb Posobin:

> Your own ideas mostly seem trivial to you because you have the right concept structures in place to support them. You wouldn’t come up with these ideas otherwise. So it’s easier to notice your own ideas in a dialogue: your friend has different concept structures and notices them.

But even if you don't buy that, or are firmly trapped in imposter syndrome, Alexey asserts that unoriginal ideas are themselves useful:

> Does this post contain a single original idea? I don’t think it does. Is it useful? Well, yeah, it is. But this brings me to a more general idea…
>
> Unoriginal writing is useful because because it helps in the process of discovery and in the process of supporting underappreciated ideas.

He likens it to a university professor giving a lecture. We don't assume lecturers have original ideas or content, yet we consider them valuable and necessary. I hadn't considered that.

Also, you should definitely start a blog. Then let me know, so I can add you [to the list](https://rubenerd.com/omake.opml)! I still recommend [Wordpress.com](https://wordpress.com/) because its relatively easy to use, and they make it easy to export if and when you need to.
