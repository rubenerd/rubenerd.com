---
title: "Peter Millard compares and contrasts sanders"
date: "2022-09-16T15:59:04+10:00"
abstract: "A professional with a soothing voice talking about sanders? Yes please!"
thumb: "https://rubenerd.com/files/2022/yt-uBSOan7D1Ow@1x.jpg"
year: "2022"
category: Hardware
tag:
- repairs
- video
- youtube
location: Sydney
---
I've been looking at cleaning and repainting some vintage computer cases and other stuff, as well as painting some modern ones with bold colours. Some of which involves sanding. Hey, research time! 

The first video I found ended up becoming an insta-subscribe :). [Peter Millard did a great video](https://www.youtube.com/watch?v=uBSOan7D1Ow) introducing all the different types of sanders, some of which I knew, but others I'd never heard of prior to his explanation.

<figure><a href="https://www.youtube.com/watch?v=uBSOan7D1Ow" title="Watch Peter Millard: Which Sander When?"><img src="https://rubenerd.com/files/2022/yt-uBSOan7D1Ow@1x.jpg" srcset="https://rubenerd.com/files/2022/yt-uBSOan7D1Ow@1x.jpg 1x, https://rubenerd.com/files/2022/yt-uBSOan7D1Ow@2x.jpg 2x" style="width:500px; height:281px;" alt="Peter Millard: Which Sander When?"/></a></figure>

I love watching enthusiasts and experts who really know their craft, and are enthusiastic to share it. Peter also has a lovely voice and delivery; I could listen to his hardware videos all day. You know what, I just might.
