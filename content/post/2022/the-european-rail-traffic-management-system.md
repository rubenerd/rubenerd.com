---
title: "The European Rail Traffic Management System"
date: "2022-05-11T08:21:08+1000"
abstract: "It might finally be getting traction."
year: "2022"
category: Thoughts
tag:
- europe
- news
- trains
location: Sydney
---
The ERTMS is one of those technical things rail nerds and industry insiders hear about every few years, but it never seems to go anywhere. It's been off the rails. It hasn't got traction. It's been station-ary. Engines.

Integrating Europe's railways into one cohesive, interoperable network with common signalling, communication, electrical, and management systems sounds great; even inevitable. Europe has integrated in other areas, so it stands to reason transport would also benefit, both for passengers and freight.

ERTMS was a bold proposal. It encompasses everything from standardised driver accreditation, to measurements and security systems. It even assets such commonality will reduce fleet prices thanks to economies of scale, and improve export possibilities.

The stakes also suddenly seem much higher. Effective intercity transit will be key to combatting climate change and oil dependency, including purchases from shitty regimes. As part of Ukraine's European Union accession, I'd love to see their rail systems further integrated into this system. It will greatly assist with their post-invasion recovery. 🇺🇦

But there's a *lot* to do. The impetus for this post came from an article [Kevin Smith write for the International Railway Journal](https://www.railjournal.com/analysis/is-europe-finally-falling-in-love-with-ertms/) on the 25th of April, in which he discuses the current status of the rollout, and how much is left to do:

> This mirrors the experience in Denmark, which followed Switzerland and Luxembourg in committing to a full rollout of ERTMS and to replace analogue relays with digital interlockings. Belgium, Norway and Luxembourg soon followed, and they have been joined more recently by the Netherlands, Italy, the Czech Republic and Sweden. Even Germany now expects to complete its rollout by 2040.

He quoted Matthias Ruete, Europe’s ERTMS coordinator:

> Overall, we are pretty confident for the whole TEN-T network. There is only around 20,000km missing, which is why I think we were quite influential in convincing people that 2040 was the deadline to go for.

French rail systems have traditionally worried about adopting new systems owing to the price, though Kevin notes the EU has allocated funds to hasten its adoption.

The other question I have from a technical perspective is how such systems work. I see the word "digital" in places, and my head immediately drifts towards thinking what could go wrong, what could be compromised, and what standards are involved. There's a lot riding on this being done right.

