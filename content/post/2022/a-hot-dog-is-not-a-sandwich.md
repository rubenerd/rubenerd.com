---
title: "A hot dog is not a sandwich"
date: "2022-10-04T08:01:17+10:00"
abstract: "A quote by Eric Mittenthal, president of the National Hot Dog and Sausage Council."
year: "2022"
category: Thoughts
tag:
- food
- language
location: Sydney
---
Few etymological debates stir up as much furore as whether a hot dog constitutes a sandwich. It doesn't, but that reality doesn't dissuade descenting delicatessen doyens demanding the dictionary definition of a sandwich is satisfied by a hot dog.

Eric Mittenthal, president of the National Hot Dog and Sausage Council, [exposes the flaw](https://www.allrecipes.com/article/is-a-hot-dog-a-sandwich/) in this logic:

> "A hot dog is not a sandwich," he said. "If you go to a hot dog vendor and you say give me a sandwich, they're going to look at you like you're crazy.

