---
title: "Uses for tangled earbud cables"
date: "2022-07-08T13:10:16+10:00"
abstract: "A source of entropy!"
year: "2022"
category: Hardware
tag:
- ergonomics
- headphones
- music
- randomness
- pointless
location: Sydney
---
Have you ever pulled earbuds out of your pocket, or headphones from your desk, and marvelled at how ridiculous the coiled mess of cables is? Here are some things I thought we could use them for:

* An impromptu puzzle. Bonus points for needing to solve it in a hurry, especially if you're supposed to be on a conference call!

* An entropy source for a random-number generator. Take a photo of the headphones right out of your pocket every day, and watch with glee at all the failed attempts to crack your hashes with your non-deterministic goodness.

* A source of inspiration for new and undiscovered kinds of rope knots. They may not be useful, or even functional, but that's what they said about this site. Hey, wait.

* A way for people to say *you should use a device to wrap your cables around* 
or wireless earbud people to say *doesn't happen to me!*

* A prompt to start a [Bob Dylan karaoke session](https://www.youtube.com/watch?v=YwSZvHqf9qM). Don't worry, he couldn't sing either.

