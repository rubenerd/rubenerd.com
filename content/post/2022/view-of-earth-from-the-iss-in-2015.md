---
title: "View of Earth from the ISS in 2015"
date: "2022-03-15T19:02:47+11:00"
abstract: "I've been staring at this photo for the last fifteen minutes."
thumb: "https://rubenerd.com/files/2022/iss-2015@2x.jpg"
year: "2022"
category: Media
tag:
- environment
- photos
- nasa
- space
location: Sydney
---
I've been staring at this photo for the last fifteen minutes. The clouds, the light, the curvature of the atmosphere. Wow.

<p><img src="https://rubenerd.com/files/2022/iss-2015@2x.jpg" alt="View of the Earth from the International Space Station during ISS Expedition 42." style="width:500px; height:333px;" /></p>

Catalogued by the Earth Science and Remote Sensing Unit, NASA Johnson Space Center, [via Wikimedia Commons](https://commons.wikimedia.org/wiki/File:ISS042-E-182310_-_View_of_Earth.jpg).
