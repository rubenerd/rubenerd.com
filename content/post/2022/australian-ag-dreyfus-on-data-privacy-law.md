---
title: "Australian AG Dreyfus on data privacy law"
date: "2022-07-03T08:33:17+10:00"
abstract: "Labor don’t have a good track record here. Fingers crossed."
year: "2022"
category: Thoughts
tag:
- australia
- politics
location: Sydney
---
Tom Burton [interviewed Australia's latest Attorney General](https://www.afr.com/politics/federal/dreyfus-pledges-sweeping-data-privacy-reforms-20220627-p5awvw) in the Australian Financial Review.

> After more than a decade of reviews and reports calling for an overhaul of Australia’s anachronistic privacy regime, Attorney-General Mark Dreyfus has committed to “sweeping reforms” to data privacy laws in the life of the current parliament.

It details the disparate jurisdictions and overlapping laws among the states, and the opportunity to reform and modernise Australian privacy laws to international standards.

And that's what makes me nervous. The pattern among Western democracies over the last few decades has generally been to curtail, infringe, or backtrack on privacy, usually for concocted or overblown reasons. My mind goes nowhere positive when I hear a modern politician say they want to overhaul or improve privacy law!

As much as I'm [overwhelmed with relief](https://rubenerd.com/australian-prime-minister-morrison-is-out/) that Morrison and his merry band of halfwits are gone, Labor doesn't have a great record when it comes to privacy and digital rights. It was Steven Conroy who proposed the [Great Firewall of Australia](https://rubenerd.com/tag/great-firewall-of-australia/), and they were nothing but enablers for the previous mob's AA bill.

I'll be waiting with baited breath and crossed fingers.

