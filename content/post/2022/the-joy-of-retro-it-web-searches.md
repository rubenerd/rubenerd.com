---
title: "The “joy” of retro IT web searches"
date: "2022-11-14T11:35:12+11:00"
abstract: "Google for something, be told to Google it, then have a link to a device that doesn’t exit."
year: "2022"
category: Hardware
tag:
- retrocomputing
- vintage
location: Sydney
---
People have mentioned how sites like Google feel like they're getting worse of late, thanks to SEO shenanigans and the rise of spammy, mass-produced content farms that spew pages of shallow, re-baked text that contribute nothing to the web beyond being a space for ads to be sold. One could accuse my site of stooping to the same level of quality, both in its meandering length of introductions, and the fact these aforementioned introductions often contain duplicated information in its meandering length.

But a subtler, more insidious change has been the disappearance of content that it indexes in the first place. Call it the *Digital Dark Age*, or the reality that besets ephemeral computers run by fallible humans who care not for archiving and availability, it only feels like it's accelerating of late.

Take the scenario where you have an unusual or old piece of computer hardware, and are trying to troubleshoot, upgrade, or replace it. The process for finding information looks, a little something, like this:

1. Do a web search for the required component.

2. Receive ten pages of results to old forum posts and newsgroups, with someone saying "you should Google this". By golly, gee whiz, thanks mister, you contributed absolutely nothing! These are probably the same people writing for spam farms today.

3. Among those rare, helpful people, most say "you should try this", with a link to a device that doesn't exist, or to another forum that has been taken down.

4. For bonus points, try and find on the Wayback Machine, only for it to not come up as listed because it was behind a login or something else.

I'm half-tempted to start a wiki or knowledge base just for the retro computer stuff that I've learned or has fixed something. I could even brand it *Top Five Ways in 2022 to troubleshoot Adaptec ISA SCSI card IRQ conficts... you won't believe how with this one simple hack 8/13 people don't know it!*
