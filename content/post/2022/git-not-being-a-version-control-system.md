---
title: "Git not being a version control system"
date: "2022-02-10T17:03:45+11:00"
abstract: "Comments from Trix Farrar and phk."
year: "2022"
category: Software
tag:
- git
- svn
- version-control
location: Sydney
---
[Trix Farrar](https://twitter.com/BasementTrix/status/1491629510441242625):

> Git is not version control!  It is a means with which file trees are to be kept in sync.  The fact that it allows mutable history invalidates it as "version control".

It reminds me of Poul-Henning Kamp's comments, as [quoted on Makefile.feld](https://blog.feld.me/posts/2018/01/git-is-not-revision-control/):

> Git is a tool which allows people and projects to manage, modify,
fork and merge the many different views, instances, variations and
modifications of a work in progress across barriers of distrust.
>
> The crucial word there was "many different", which is the exact
opposite of what a VCS strives for.
>
> A lot of the features Git provides, features which are what makes
it great as a colaboration tool, flies in the face or or directly
invalidates the guarantees you normally expect from a VCS, most
notably progression of time & version, immutability and consistency
of view.
