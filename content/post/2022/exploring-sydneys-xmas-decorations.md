---
title: "Exploring Sydney’s Xmas decorations"
date: "2022-12-18T08:30:30+11:00"
abstract: "Photos around the QVB and GPO. That’s some TLAs."
thumb: "https://rubenerd.com/files/2022/xmas-gpo@1x.jpg"
year: "2022"
category: Travel
tag:
- photos
- xmas
location: Sydney
---
[La Niña](https://www.abc.net.au/news/2022-09-13/bom-declares-third-la-nina-summer-weather-flood-risk/101424100 "BOM declares La Niña, increasing flood risk for third year in a row") continues to soak Eastern Australia for the third year running, which has lead to the weird sensation of being *cold* around Christmas in the Southern Hemisphere. We're supposed to be on the beach enjoying summer, listening to *Let it Snow, Let it Snow, Let it Snow* with a wink.

*(Funny story, first or second year I was back in Australia, I was at a coffee shop with uni friends when a bunch of women wearing bikinis with santa hats walked in. We weren't even near a beach, because I don't like them. Too much sand).*

Instead, Clara and I braved the cold and rain to wander down to the Sydney CBD to check out the holiday decorations. The multi-story tree at the Queen Victoria Building was a bit understated compared to previous years, but I loved these flower lights.

<figure><p><img src="https://rubenerd.com/files/2022/xmas-qvb@1x.jpg" alt="Base of the large Xmas tree at the QVB" style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2022/xmas-qvb@2x.jpg 2x" /></p></figure>

But our favourite was the tree near Martin Place, which was full of native Australian fauna including bottlebrushes and wattle. I thought it was really well executed, unlike this overexposed photo of it near the GPO:

<figure><p><img src="https://rubenerd.com/files/2022/xmas-gpo@1x.jpg" alt="Photo of a tall Xmas tree foregrounding a clock tower in Sydney." style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2022/xmas-gpo@2x.jpg 2x" /></p></figure>

Then it was off to [Industry Beans](https://industrybeans.com/pages/york-st-cafe) on York Street for a break, bought for Clara and I by the lovely [Asherah](https://kivikakk.ee/) via the donate coffee button. Nice day, even with the rain :).
