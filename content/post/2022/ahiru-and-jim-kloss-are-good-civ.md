---
title: "Ahiru and Jim Kloss are Good Civ"
date: "2022-09-28T09:00:21+10:00"
abstract: "The world isn’t a zero-sum game, so any act of kindness makes life better for everyone."
year: "2022"
category: Thoughts
tag:
- feedback
- jim-kloss
location: Sydney
---
I've mentioned a few times how bad I've become at responding to comments lately. I can tell there are at least a dozen of you waiting for feedback, and I appreciate your patience.

Today I wanted to call out these two gentleman specifically. Ahiru wrote a lovely (and I'd argue undeserved!) email in response to my bad news post. His [recent blog entry](https://ahiru.pl/posts/mdp/) about the [music player daemon](https://mpd.readthedocs.io/en/stable/user.html) has also given me food for thought in my quest to find the ultimate cross-platform LAN music playing system.

And then there's Jim Kloss, who's gentle words of encouragement on The Bird Site never go unnoticed, though often unacknowledged. It means the world, thank you.

This experience has taught me, once again, to email and contact people who you think make your life better, whether it be an author, a package maintainer, a developer, or whomever. The world isn't a zero-sum game, and these acts of kindness lift everyone.
