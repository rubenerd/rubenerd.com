---
title: "Alan Kohler explores remote work"
date: "2022-10-14T09:10:32+11:00"
abstract: "“the obvious fact that the bank’s management is powerless to decide the level of attendance of their staff”"
year: "2022"
category: Thoughts
tag:
- economics
- remote-work
location: Sydney
---
Australian economist and ABC finance fixture Alan Kohler had a [clear and concise article](https://thenewdaily.com.au/finance/2022/10/13/working-from-home-kohler/) in The New Daily about the rise of remote work. He used a meeting of one of Australia's largest banks as a lens to describe the trend:

> Two and a half years after the pandemic began, half of this bank’s staff are still working from home.
> 
> Second is the fact that the bosses aren’t happy about it and are pleased (ecstatic) that half of them made the journey into Melbourne’s Docklands.
> 
> And third, perhaps most striking of all, is the obvious fact that the bank’s management is powerless to decide the level of attendance of their staff – they are passive observers, counting the number of people who show up, wishing it was more but unable to control it.

That last one is key. One or two people asking for it would have been scoffed at, but if half or more of your workforce is? Especially given these businesses survived and thrived when people were forced to work remote? It's a demonstration that all we need to affect change is sufficient numbers.

Alan also discusses some of the ideas people are pitching for getting people back into the office. Most of them come down to making offices nicer, giving people free food, and so on. It strikes me as superficial. The way you get people in is to give them meaningful and engaging work.

The cold truth for managers, and more broadly economically, is that entire classes of necessary jobs are only done because people are compensated for it, regardless of the bubbly or optimistic façades workers throw up during performance reviews. It's literally why we have the concept of retirement.

I know two people working IT at large financial service companies that seethe with hatred for their jobs and colleagues, but the excellent pay keeps their mortgages paid, food on the table, and nice holidays coming. If they can get their work done remotely, then dedicate their mental recovery time to their families and hobbies over a commute, I doubt free sushi in a glass and steel office tower will cut it.
