---
title: "Adventures with mould and dehumidifiers"
date: "2022-06-26T09:46:08+10:00"
abstract: "We saved some old computer stuff and soft toys before it got too bad, fortunately."
year: "2022"
category: Hardware
tag:
- environment
- health
location: Sydney
---
It's been a wet year in Sydney; we recorded more than twelve months worth of rainfall in one quarter.

Unbeknownst to Clara and I, mould had a field day in some of our cupboards, and behind a curtain in the bedroom. I guess there wasn't ever enough time between storms for the air to dry out. It attacked window sills, the surfaces of [some of my vintage tech](https://bsd.network/@rubenerd/108503421448018098), and a shelf of Clara's soft toys.

Our first step was to put everything affected out on the balcony to get a dose of sunlight, then use some anti-mould spray on the shelf surfaces. Even with open windows and plenty of forced ventilation with fans, the smell of this noxious stuff wasn't fun. But to its credit, everything looks fine now.

The next step was to buy a couple of dehumidifiers, which turned out to be more of an adventure than we realised. Who'd have thunk it, but an entire city of people rushed out to buy them after unseasonable rainfall! Ideally we wanted some with built in charcoal air filters, but we settled on a couple of small [AusClimate units](https://www.ausclimate.com.au/), and kept our other air filters running instead.

In Singapore we'd run dehumidifiers in some of the rooms when we were on holidays to prevent mould breakouts. I think I only had some clothes, a leather computer chair, and a few VHS tapes get damaged (yay for VCDs)! Presumably construction standards and building materials over there account for permanent high humidity, and we tended to either run the aircon or have the windows open so mould was generally not an issue.

We ran the dehumidifiers in the affected rooms at full power for a week, to perform what the manual called a "deep dry". They got the ambient humidity down from 70 at the peak to an 30, and pumped more than 10 litres a day based on how often we were emptying them. Now we have them on medium settings, so they only kick in when humidity gets especially high.

Fingers crossed this has done it, but we haven't seen any mould return since. We probably saved the landlords walls too; I can't imagine how expensive and dangerous it'd be to let that stuff grow and require a deep clean and repainting.
