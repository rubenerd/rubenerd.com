---
title: "The SMS turns thirty"
date: "2022-12-04T07:37:33+11:00"
abstract: "The text message is celebrating its 30th birthday"
year: "2022"
category: Thoughts
tag:
- history
- phones
location: Sydney
---
Zoe Kleinman [had a fun article](https://www.bbc.com/news/technology-63825894?at_medium=RSS) for BBC Tech:

> The text message is celebrating its 30th birthday - the first was sent to a mobile phone by a Vodafone engineer in Berkshire in the UK on 3 December 1992.
> 
> It was sent in order to test out the tech, and read "Merry Christmas".
>
> Neil Papworth sent it to one of the firm's bosses, Richard Jarvis, who was at a Christmas party. He did not get a reply.

My first SMS would have been to my dad in 1998. I'm pretty sure it was something riveting like "test!"
