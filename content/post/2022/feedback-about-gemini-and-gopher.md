---
title: "Feedback about Gemini and Gopher"
date: "2022-11-30T07:16:39+11:00"
abstract: "Martin, Wouter from BrainBaking, @rjc, and Jonathan W weigh in on the benefits of running these servers."
year: "2022"
category: Internet
tag:
- gemini
- gopher
location: Sydney
---
On Monday I mulled the idea of [starting a Gemini or Gopher server](https://rubenerd.com/thinking-aloud-about-gemini-and-gopher/), and either hosting a version of this site on it, or starting something new.

Martin emailed with a suggestion for Gopher:

> I very much doubt Gemini is going anywhere. Gopher has been around since forever and still has an active community. Please join us :)

Wouter from Brain Baking [referred me](https://chat.brainbaking.com/objects/d398fa74-1d06-4d75-a99d-4c217eaa9768) to a post he wrote last year where [he did exactly that](https://brainbaking.com/post/2021/04/using-hugo-to-launch-a-gemini-capsule/). He recommends [Agate](https://github.com/mbrubeck/agate), a simple Gemini server written in Rust. He also links to [Sylvian Durand's method](https://sylvaindurand.org/gemini-and-hugo/) for templating out Gemini pages in Hugo, the static site generator I use. Most importantly for me, he also talked through his mental process for deciding *what* to publish! In the end he decided it was a lot of extra boilerplate and work, similar to his recent post about metadata.

In terms of other people running dual or triple stacks, [rjc](https://bsd.network/@rjc/) also referred me to the [Boston Diaries by Sean Conner](http://boston.conman.org/), and Jonathan W. emailed saying he used to run a Gemini site before he moved on as Wouter did. I miss your writing Jonathan; though given how you were treated I don't blame you for going offline.

I was originally leaning towards Gopher, but exploring Gemini in the Lagrange browser reminded me just how much I love it. Rather than boiling the ocean, I might try running my own server with some basic stuff on it first.
