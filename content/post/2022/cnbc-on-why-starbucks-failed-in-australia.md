---
title: "CNBC on why Starbucks failed in Australia"
date: "2022-07-24T08:29:54+10:00"
abstract: "It’s burnt floor sweepings, but I think there’s more to it."
year: "2022"
category: Thoughts
tag:
- australia
- business
- coffee
- starbucks
location: Sydney
---
CNBC ran a story and video back in 2018 about [why Starbucks failed in Australia](https://www.youtube.com/watch?v=_FGUkxn5kZQ). This was pre-COVID pandemic, and more Starbucks stores have opened since, but the general message still applies.

They interview two local Australian business experts and some local coffee shop owners about why they thought this was the case. Their arguments largely percolated into:

* Australia having been introduced to cafe culture by Greek and Italian immigrants in the 1950s, including knowing your barista by name and hanging out with friends at your local.

* The fact the menus weren't tailored to local tastes. Sugary drinks are less popular here, and Australians have self-professed "sophisticated" preferences.

* Their rapid expansion meant it was too easy to get, and thus they couldn't form a "connection" to the brand. This is in contrast to the US, where growth was more organic.

The last one was marketing speak I'm not qualified to decode, but the first two sound about right. But there are a few more things at play.

First, the report downplayed just how much most Australians *hate* Starbucks. Mention the chain in polite company, and one will be ridiculed for drinking burnt floor sweepings imported from a country that puts high-fructose corn syrup into everything. Such an opinion isn't logically consistent; other American chains like McDonald's are so entrenched that they even have a [local name](https://www.urbandictionary.com/define.php?term=maccas). Certain Australians had fierce loyalties to locally manufactured cars, despite them being owned by Detroit.

My experience as an outsider moving back is that Australians tend to treat *new* American imports with scepticism. It has to be significantly better for people to take it seriously, and Starbucks simply wasn't.

But that might not always hold true. Australia's demographics continue to evolve, and people who grew up with ubiquitous Starbucks and Coffee Bean in Asia are now filling branches here. I can see the Chatswood Starbucks out our apartment window, and it's packed on a Sunday night. Opening a branch in Newtown or Surry Hills however would make fedoras and penny farthings roll.

A decade ago I wrote about [why Starbucks was so popular in Singapore](https://rubenerd.com/starbucks-internet-in-singapore/), but I've noticed even that's starting to change a bit. More independent cafes are springing up across the island, and local kopitiam-style chains are also asserting their mix of nostalgia and local flavour into their offerings.

It seems to me the real problem was a lack of market research, and overestimating how many chain coffee shops an already saturated Australian coffee landscape could absorb.

I love Australian coffee, and miss it dearly when I travel overseas, but I also go to Starbucks sometimes. The warm couches and general ambience remind me of those times growing up in Singapore and sharing a brew with my mum before she went to chemo, or those precious few times I'd hang out with my dad before he'd be off on another month-long business trip. Yes it doesn't taste amazing, but that's not the point.

As my dad used to say, Starbucks is less of a coffee shop and more a chair rental service with free drinks. Maybe their ads needs to push that!
