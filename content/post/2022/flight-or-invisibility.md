---
title: "Flight or invisibility, with Clara!"
date: "2022-02-12T21:28:27+11:00"
abstract: "Easy, flight! But Clara disagrees"
year: "2022"
category: Travel
tag:
- clara-tse
- hypothetical
location: Sydney
---
Clara's company runs a weekly games afternoon for team building, which looks like fun. It was her turn to come up with an activity, so she gave each person a hypothetical to explore. This was one of them:

> Would you prefer the ability of flight, or invisibility?

I asked Clara, who's siting opposite from me right now, to write a response.

While we wait, I'd easily prefer to fly. That surprises me, because as an introvert I'd love to fade into the background <span style="text-decoration:line-through">sometimes</span> most of the time and be left alone. But being able to see the world from that perspective, and travel with ease would be a boon.

Clara's done now:

> I would definitely prefer to turn invisible, to avoid embarrassing situations and having to look in the mirror! Besides, if you could fly imagine ending up in restricted airspace, and the pigeon strikes!

Nooooo! *smacks into pigeon.* 🐦
