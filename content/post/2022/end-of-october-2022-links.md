---
title: "End of October 2022 links"
date: "2022-10-31T14:44:36+11:00"
abstract: "Self-care, Wouter talks Visual Studio 6, secret Japanese restaurants, EuroBSDCon, 86Box, and Komi Can’t Communicate."
thumb: "https://rubenerd.com/files/2022/komi-can-t-communicate-vol-1-cover.jpg"
year: "2022"
category: Internet
tag:
- 86box
- bsd
- eurobsdcon
- japan
- food
- komi-cant-communicate
- links
- manga
- mental-health
- nostalgia
- virtualisation
- visual-basic
location: Sydney
---
I've been reading a lot to [keep my mind off things](https://rubenerd.com/taking-a-break-e312643b/). Here's a selection:

* This article by [Philippa Perry in the Guardian](https://www.theguardian.com/lifeandstyle/2022/oct/30/ask-philippa-perry-i-have-a-loving-wife-and-child-but-i-dont-want-to-exist) is one of the best I've read about self-care in a long time. I feel the guy's guilt at feeling selfish.

* Wouter over on Brain Baking had such a [fun trip down memory lane](https://brainbaking.com/post/2022/10/a-visual-studio-6-nostalgia-trip/) with Visual Studio 6. I had the academic version of the short-lived Visual Basic 5.0, but it looks similar. It makes me want to break out my old Mastering VB and Delphi CDs!

* Did you know there's a secret staff cafeteria at [Nankai Namba station](https://soranews24.com/2022/10/31/secret-staff-cafeteria-at-this-japanese-train-station-is-still-open-for-those-in-the-know/), and at [Osaka Airport](https://soranews24.com/2022/09/15/the-secret-staff-cafeteria-at-osaka-airport-that-few-people-know-about/)? Something tells me a gaijin like me would stick out like a sore thumb, but Clara could probably sneak in undetected.

* I've started watching the videos from EuroBSDCon 2022. [This one about memory barriers](https://www.youtube.com/watch?v=jnb4k1h6h1o) by Taylor R Campbell was fascinating.

* [ozzmosis shared](https://mastodon.cloud/@ozzmosis/109245756467401663) with me the [86box](http://86box.net/), a fork of PCem that faithfully emulates classic IBM hardware. Building our childhood 486 down to the CPU and BIOS while I'm in waiting rooms has been a genuine joy.

* The [Komi Can't Communicate](https://www.kobo.com/au/en/ebook/komi-can-t-communicate-vol-1) manga is a lovely, good natured look into awkward people trying to find their footing in the world.
