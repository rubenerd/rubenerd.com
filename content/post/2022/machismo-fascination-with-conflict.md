---
title: "Machismo fascination with conflict"
date: "2022-08-10T08:56:35+1000"
abstract: "A quote from a recent Johnny Harris video about conflict, and how some people think of it as sexy and cool. It isn’t."
year: "2022"
category: Media
tag:
- politics
- war
location: Sydney
---
[Johnny Harris](https://www.youtube.com/watch?v=VNZ0so0LCoM)\:

> I feel very against the sort of machismo fascination with conflict, like it's sort of a cool, good thing. When what we're talking about are people's lives. We're talking about the future of entire societies being ripped apart by a power struggle.
>
> This is not sexy and cool.
>
> This is a nightmare.
