---
title: "Why bought sites get taken offline"
date: "2022-05-30T07:18:07+1000"
abstract: "Sites are built and maintained by humans."
year: "2022"
category: Internet
tag:
- business
location: Sydney
---
What happens when a website or service gets purchased by a larger company, with assurances and promises that it’ll remain operational? We all know the drill! The effort the original owners put into claiming a site will remain online scales linearly with the chances of it vanishing as soon as the ink has dried on their new contracts. This sounds cynical, but it's a modern web reality, and very human.

This can happen for a few reasons; some more truthful than others.

Often times the teams that build and operate such sites are bought more for their talent, in what the industry refers to as an “aqui-hire” (for acquisition and hiring). The team have proven themselves, and the new owners might need their expertise and experience to deploy something else. It's easier to bring in a team that's known to work well together than to assemble a new one from scratch; though whether everyone will stay around after the acquisition isn't guaranteed. Either way, the original site may no longer have the resources or priority to remain a [going concern](https://en.wikipedia.org/wiki/Going_concern) internally within the business.

There could be differences in expectations. Depending on the acquisition arrangements, the new owners might want to take the site in a new direction that the original owners didn't want or envisage. The original owners might have also expected to retain more control than they end up having, either through naïvety, assumptions, or vague settlement terms. It's hard to remain motivated to contribute to something that has changed beyond your initial idea, or when you're being pulled in multiple directions. New businesses bring new interpersonal dynamics, procedures, and structure which someone brought up in a startup culture or small business might not gel with.

On the other side, the biggest issue I hear about is the original owners misrepresenting the state of a site's backend, or have different or more lax standards than the purchasing business. I've heard friends lament horror codebases, servers held together with tape, and a disregard for security or basic processes. The new business might otherwise have wanted to keep operating the site, but think the technical debt is insurmountable, or not worth investing time into to bring up to code. It could be a simple case of management deciding they don't want to take on the liability!

But let's not kid ourselves either. The owners may have sought a buyout in the first place to stave off their own issues, in which case the original site may not have survived anyway. No business is perfect, and all the glossy PR in the world can't change an intractable situation, or make tough choices simple.

Sites are built and run by humans who, when moved in with other humans, have a whole new dynamic. None of this is unusual behaviour; though it should remind us to have a healthy scepticism in the lead up to an acquisition. If someone is writing a long post explaining why something is going to remain the same, it might be worth asking who they're trying to convince.
