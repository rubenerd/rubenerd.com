---
title: "David Hazeltine, Look What I Found"
date: "2022-03-21T19:29:23+11:00"
abstract: "This is toe-tappingly fun!"
thumb: "https://rubenerd.com/files/2022/yt-1Uh5jCtyi2U@1x.jpg"
year: "2022"
category: Media
tag:
- jazz
- music
- music-monday
location: Sydney
---
I love that I *found* this! Today's [Music Monday](https://rubenerd.com/tag/music-monday/) was a treat from my recent [Tully's Coffee Go Jazz CD](https://rubenerd.com/gojazz-nakano-miku-bluescsi/) from Japan that toe-tappingly fun.

<p><a href="https://www.youtube.com/watch?v=1Uh5jCtyi2U" title="Play David Hazeltine - Look What I Found"><img src="https://rubenerd.com/files/2022/yt-1Uh5jCtyi2U@1x.jpg" srcset="https://rubenerd.com/files/2022/yt-1Uh5jCtyi2U@1x.jpg 1x, https://rubenerd.com/files/2022/yt-1Uh5jCtyi2U@2x.jpg 2x" alt="Play David Hazeltine - Look What I Found" style="width:500px;height:281px;" /></a></p>
