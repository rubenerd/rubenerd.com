---
title: "Figuring out things before the Internet"
date: "2022-05-17T14:03:13+10:00"
abstract: "Thinking about how much more difficult it used to be."
year: "2022"
category: Internet
tag:
- cd-roms
- information
- nostalgia
location: Sydney
---
I was part of the last generation that grew up without ubiquitous Internet access, and probably the *only* one that used multimedia CD-ROMs to research school assignments. Maybe that's why I've been collecting them of late; who knows when Worldbook, Bookshelf, Encarta, or Grolier will have my back again?

Okay, realistically the chances are as low as my vitamin D levels. Oh, *burn!* Wait, that's the problem, I'm not getting enough sunlight. But I digress.

It makes me wonder how I ever found out things before the Internet, and what the impact of that has been.

Did you know [Perinthalmanna](https://en.wikipedia.org/wiki/Perinthalmanna) became a municipality in 1990, or that the [Empogona](https://en.wikipedia.org/wiki/Empogona) genus was originally a subgenius of Tricalysia?

Almost every question I have now gets put into a search engine or Wikipedia first, from how different laundry powders work, to whether the `allexport` option is available on all Kornshell variants. Often times that will lead me to a book to buy, or a video to watch, but the Internet was the spark.

Would would I have done before? Some of this is handled with local documentation, like the excellent BSD manual pages. But would I have done as much research into esoteric or unusual topics if it meant going to a library first? Would the library even have an approachable book about laundry surfactants?

But on the flipside, the Internet lets us enter the dangerous realm of Dunning Kruger of assuming we're knowledgeable on subjects we're not. It also assumes we're capable of processing all this additional mental information, and that we need to have an opinion about everything.

I don't want to say life used to be simpler... but it *kind* of was. In good ways and bad.
