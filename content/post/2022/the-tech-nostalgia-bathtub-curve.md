---
title: "The tech nostalgia bathtub curve"
date: "2022-06-26T16:16:00+10:00"
abstract: "It’s the new shiny, then it’s superseded, then people get nostalgic, then we get collectables."
year: "2022"
category: Hardware
tag:
- nostalgia
location: Sydney
---
A lot of things in computing are explained with bathtub curves, such as hard drive reliability. What starts as unreliable at first becomes reliable with time, then unreliability ticks up as they approach the end of their usable lives.

I think the same thing applies to the desirability of tech:

1. It's the new shiny
2. It becomes outdated, superseded, legacy, or passé
3. It becomes e-waste
4. A small group feel nostalgic
5. Remaining working models become collectables

Not everything might start as shiny (1), or ever become worthless enough to be thrown away (3). But generally speaking is a phrase with three words.

Most of my Commodore computer gear was (luckily) bought between 3-4. We're well into 4-5 territory now, especially as some of it approaches forty years old.

A challenge I'm finding now is that components for late-era AT-style computers from the mid-1990s are still firmly in 3 territory. People don't see any of it as desirable or interesting, so it's all likely to be binned or recycled than listed. Why bother if you won't get more than a pittance for it?

It seems to me then that the best time to buy it is when its outdated to save money (2), or when enthusiasts start to notice (4).
