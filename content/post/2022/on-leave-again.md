---
title: "On leave again"
date: "2022-11-13T08:11:34+11:00"
abstract: "It doesn’t seem real, though we’re not going to Brazil."
year: "2022"
category: Thoughts
tag:
- covid-19
- personal
location: Sydney
---
I'm sitting here on this dreary, overcast Sydney morning, setting my *out of office* notice! I've also redirected my work phone number, and have removed some of my VPNs. We fly out next Wednesday.

It doesn't seem real... though we're not going to Brazil. That's a quality currency joke right there.
