---
title: "I… bought a Commodore 64C"
date: "2022-12-14T09:08:22+11:00"
abstract: "I blame Screenbeard and Jan Beta"
thumb: "https://rubenerd.com/files/2022/commodore-64c-close@1x.jpg"
year: "2022"
category: Hardware
tag:
- 1980s
- commodore
- commodore-16
- commodore-64
- commodore-64c
- commodore-128
- commodore-plus4
- nostalgia
- retrocomputing
location: Sydney
---
I did something a bit silly last week. Clara and I were watching another of [Jan Beta's](https://www.youtube.com/@JanBeta) fun [Commodore 64 restoration](https://www.youtube.com/watch?v=Myxf8LIunwo) videos, when on a lark I went on eBay. This proved to be a fateful mistake: someone near to me in Australia was selling their PAL 64C, and for less than a third of our last power bill!


### The Commodore 128 factor

I blame Jan Beta for this ridiculous purchase, but Screenbeard of [The Geekorium](https://the.geekorium.au/) and [aus.social](aus.social/@screenbeard) also shares the blame. I'd tinkered with my TED-family 16 and Plus/4 I got second-hand for my 18th birthday, but it was the [Commodore 128](https://rubenerd.com/fixing-my-non-booting-commodore-128/) he gave me that made things click. It taught me how to read and use service manuals, perform multimeter tests, socket new chips, perform very simple (de)soldering, how Commodore disk drives work, and how to write basic (hah!) Commodore programs. It also gave me my first experience with CP/M on physical hardware, and 80-column text on an 8-bit system.

For a machine that was supposed to serve as my final 8-bit machine (on account of being the best one ever made), it instead reinvigorated my interest in this hobby, to the point where I've actively searched for other machines from the time period. I have my own nostalgia for boring 486 and Pentium PC clones having grown up on those, but the 1980s were such a fascinating time for home computing. I even saw a 130 XE going for a similar price, and it took *tremendous* willpower to check my budget before wrecking my budget, as Gen Z says.

<figure><p><img src="https://rubenerd.com/files/2022/commodore-64c-close@1x.jpg" alt="A closeup of the Commodore 64 Personal Computer badge, which was new on the 64C" style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2022/commodore-64c-close@2x.jpg" /></p></figure>


### The design of the 64C

My impression of reading community posts and watching YouTube videos is that the 64C doesn't share the cult following of the original 64. The chonky breadbox is a cultural icon of the 1980s, whereas this machine comes in dual-tone beige. Its SID chip is backwards compatible with the original, but doesn't sound the same. Its dimensions, while arguably more ergonomic and contemporary, don't fit many of the same hardware mods and addons of the original (hence part of the reason for the 64G in Europe).

While the 64C's design and reliability benefited from half a decade of lithographic and fabrication improvements, there's no question the earlier machines have their own nostalgic charm which people remember and love. I empathise; if you're buying these machines to relive your childhood, only the original will suffice.

I didn't have a 64 growing up, so I love the 64C! The machine takes its design cues from the Commodore 128 released a year prior, pictured below in the lower-left. I may be in the minority here, but I happen to think the wedge shape and muted colour scheme make the 64C one of the more beautiful machines from the time period. It tickles me that a machine from around the time I was born shares the same colour scheme as the first computer I built as a kid... *in 1998*. I can see them together right now, and without a second glance, you'd think the 64C is the keyboard for that Pentium 1.

Looking at the 64C next to my other machines, I can also see its family resemblance to the 128, and even the Plus/4. That said, I'm glad I have a 16 to have a breadbox form factor too (which I acknowledge easily has the most space for addons).

<figure><p><img src="https://rubenerd.com/files/2022/commodores-2022-yay@1x.jpg" alt="My Commodore 64C, C128, Plus/4, C16, datasette drive, 1541 disk drive, and 1571 disk drive!" style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2022/commodores-2022-yay@2x.jpg" /></p></figure>


### Powering it up

The 64C came with the original power brick, which Jan Beta advises replacing as early as humanly possible. I have a replacement in the mail, but I used it to test the machine powered on for a few seconds, along with the S-Video cable from the C128.

*It works!* And more ridiculously, it even has the original Commodore warranty sticker intact, meaning it's unlikely anyone has ever opened the machine, or modded it with aftermarket components. This is either a testament to improvements in design and fabbing of the shortboards, or perhaps a sign this machine didn't get much use. Either way, I'm chuffed.

I will need to break the seal eventually though (unless there's a sneaky way to remove it). The machine is *extremely* musty; that dank, mildewy odour that comes from being in a damp basement for many years. It's unpleasant to smell, but I'm also slightly concerned what the internal state of the machine might be. I'll also want to replace the electrolytic capacitors, and add some heatsinks to the hotter ICs, for which I'll defer to Jan's videos for advice.

Unfortunately my S-Video to VGA box also released some magic smoke recently, so I had to connect to our primary TV to test. The Dutch angle is to give the impression of an action shot... and because there isn't much room to manoeuvrer in our tiny apartment right now. I think Ryza wanted to signal her approval too.

<figure><p><img src="https://rubenerd.com/files/2022/commodore-64c-action-shot@1x.jpg" alt="The Commodore 64c at a stylish angle showing the output of its BASIC screen on our TV" style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2022/commodore-64c-action-shot@2x.jpg" /></p></figure>


### Naming it

This might be the most important consideration. I tend to use Star Trek ships and anime characters for modern machines, but I decided to name my 8-bit computers after people who've meant a lot to me. Thus, we have:

* **Commodore 128**: `screenbeard`, for the person who gave it to me, and for his words of encouragement over many years.

* **Commodore 64C**: `janbeta`, for his videos, enthusiasm, and technical assistance.

* **Commodore 16**: `mrdunham`, for my high school computer teacher and 10-pin bowling coach who was surprised and happy to talk about 8-bit computers with me between frames.

* **Commodore Plus/4**: `bilherd`, for the designer of the TED line and 128, who took the time to read and respond to my fanmail :).

* *I'm reserving `chuckpeddle` for a Micro-PET kit, once my soldering skills improve!*


### Next steps

Work is busy this week leading up to the holidays, so I'll probably be taking a closer look on the weekend and blogging my findings. I've also got an EasyFlash cart in the mail which I'll be able to use to load up stuff. I'll be soldering it, so we'll see how that goes!
