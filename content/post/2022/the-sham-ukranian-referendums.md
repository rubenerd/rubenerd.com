---
title: "The sham referendums in Ukraine"
date: "2022-10-01T09:43:08+10:00"
abstract: "99%... are you serious?"
year: "2022"
category: Thoughts
tag:
- news
- ukraine
location: Sydney
---
The news as reported by the [Kyiv Independent](https://kyivindependent.com/national/russia-annexes-4-ukrainian-oblasts-after-staged-referendums), which you can [support here](https://www.patreon.com/kyivindependent/posts)\:

> The announcement [of annexation] comes after Russia's proxies held sham referendums in the occupied parts of these regions and, on Sept. 27, declared nearly 100% of people living in the occupied territories of Ukraine "voted" to join Russia. 

These numbers look North Korean. They're so ridiculous, and so divorced from reality, that no rational person would take them seriously. Even in the most unified and functional of democracies, a 60% win is seen as a landslide.

99% of Americans can't vote against shooting kids, and we'd never get 99% of Australians to admit our [gambling crisis](https://www.australianethical.com.au/blog/australia-needs-to-kick-its-pokies-addiction/). Yet we're supposed to believe a wartorn oblast you don't fully control, and where your invaders have killed, pillaged, raped, and tortured thousands of innocent people, agrees with your invasion at those numbers?

Despots, dictators, and deplorables make this obvious mistake *every single time*. 99% doesn't make you look strong, it exposes your scam.

As a reminder, the President’s [United24](https://u24.gov.ua/) initiative includes links for where you can send donations for defence, demining, medical aid, and reconstruction. Please consider if you can. Sláva Ukrayíni. 🌻

