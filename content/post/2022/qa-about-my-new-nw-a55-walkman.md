---
title: "Q&A about my NW-A55 Walkman"
date: "2022-07-26T08:19:38+10:00"
abstract: "Questions about audio books, anxiety, price, indexing media, and others."
thumb: "https://rubenerd.com/files/2022/sony-nw-a55m-hand@1x.jpg"
year: "2022"
category: Hardware
tag:
- feedback
- music
- walkman
location: Sydney
---
My [recent Walkman post](https://rubenerd.com/my-new-sony-nw-a55-walkman/) generated a ton of email and comments; thanks to most of you for sharing your ideas and questions!

I say most, because it seemed to attract more trolls than anything I've written in a long time. It's disheartening to have something you enjoyed (and tried to share with people) immediately shat on by those who don't even bother to read it. At times it makes me wonder why I even blog in the first place; but then I remember all the rest of you :).

I’ve aggregated the most common questions below.

**Can you expand on your comment about device anxiety?**
: <p>This was overwhelmingly what I got asked the most via email, but not on public forums. This tells me people are worried about it, but are too shy to admit it. Either way, I’d be happy to. I’ll dedicate a proper post or podcast to it.</p>

**Does it work well with audiobooks or playlists?**
: <p>I didn’t buy with these in mind, and haven’t tested it with them. Going by what others said, it might not be well suited to it.</p>

**I got one and didn’t like the interface**
: <p>I do, but I agree it’s subjective, and not as good as a classic player with fully-hardware controls. I listen to albums though, so I choose one, pocket it, and use the hardware side controls.</p>

**Doesn’t it take a long time to index media?**
: <p>It can. It’s a trade-off I’m willing to make, given the flexibility in being able to transfer files like a USB drive. I also don’t usually listen to FLAC or similarly huge formats, so it’s less of an issue.</p>

**This Walkman isn’t cheap!**
: <p>I said it in the context of their $4,000 players, but you’re right, you need a few hundred bucks spare. If you do want one but are cash-strapped, you can also buy them second-hand on Japanese auction sites too. Which leads to&hellip;</p>

**How do shipping proxies work?**
: <p>We use <a href="https://buyee.jp/">Buyee</a>. Clara regularly gets shipments from Japan, and has tried a bunch of different proxies, so I trust her experience. The site lets you bid on items, or you can submit a request for an online store. They buy it on your behalf, send it to their warehouse, and notify you. We’ll often wait until we have a few items, then get them to send it. I use similar services to buy music from domestic US stores.</p>

**Your iPhone’s DAC is just as good, isn’t it?**
: <p>I don’t know; as I said I’m not an audiophile, so I can only go by my subjective experience. Audio files sound better to me. The player might have a better DAC, or better signal processing, or Sony has more smarts in equalisation.</p>

**Why not something like a modern SanDisk player?**
: <p>This was as much about Walkman nostalgia. Most budget players also use OLEDs, which flicker in my eyes and <a href="https://rubenerd.com/why-oled-phone-screens-suck-for-some-of-us/">give me headaches</a> even with brief interactions. The A50 and A100 series devices use crisp, high-res LCDs.</p>

**What don’t you like about streaming services? They work for me**
: <p>I’ve talked about this a few times before, but it’s more to do with why I prefer buying music. Collecting physical albums (and ripping them) is fun, it directly supports the artist, and I get something that can’t be revoked, replaced, or relicenced without my knowledge or permission.</p>


