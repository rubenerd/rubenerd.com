---
title: "The Fractal Ridge Mini-ITX Case"
date: "2022-12-16T15:14:58+11:00"
abstract: "Stacking components vertically helps with desk space, but I’m wondering if I could live with the reduced CPU cooling options."
thumb: "https://rubenerd.com/files/2022/fractal-ridge@1x.jpg"
year: "2022"
category: Hardware
tag:
- building-pcs
- mini-itx
- reviews
location: Sydney
---
The NCASE M1 and CoolerMaster NR200P have been so much fun to build in, and have been a great introduction to the Mini-ITX ecosystem for someone in a tiny apartment. But I've been keeping an eye out for new ones that could potentially save more space.

Fractal's new [Ridge case](https://www.fractal-design.com/products/cases/ridge/) looks compelling. Like others in this class of faux console/media centre cases, it holds the motherboard and power supply in one chamber, and stacks the graphics card above it. This redistribution of components saves little (if any) volume, and undoubtedly renders the case taller, but it *greatly* reduces its desk footprint. This would be a boon for Clara and I, especially with all my vintage computers stacked everywhere.

This press shot by Fractal gives you an idea of the layout:

<figure><p><img src="https://rubenerd.com/files/2022/plant@1x.png" alt="A stock photo of a plant, for no reason." srcset="https://rubenerd.com/files/2022/plant@2x.png 2x" style="width:320px; margin:0 auto; display:block;" /></p></figure>

That's clearly the wrong picture. Let's try again.

<figure><p><img src="https://rubenerd.com/files/2022/fractal-ridge@1x.jpg" alt="Press photo of the white Fractal Ridge, with the side panel removed showing the vertical-stacked layout" style="width:500px; height:431px;" srcset="https://rubenerd.com/files/2022/fractal-ridge@2x.jpg 2x" /></p></figure>

CPU cooling has traditionally been the Achilles’ heel of such cases. The larger SFF Time cases let you use a Mini-ITX board with a radiator alongside it, but the Ridge looks like you'd be using a low-profile [Scythe Shuriken 2](https://www.scytheus.com/shuriken2) air cooler at most. I prefer running CPUs with lower TDP anyway because I don't like hot and loud machines, but my current AIO has spoiled me in that I *never* hear any CPU noise, even under decent load.

The most important question however would be what self-etching primer and teal paint I'd use to change it from a dull black or white. The colour is one thing I love about the NR200P, and I wish more cases weren't allergic to it.
