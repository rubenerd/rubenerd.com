---
title: "A lot happening in this lift"
date: "2022-08-15T12:48:01+1000"
abstract: "Some delightful signs placed together."
thumb: "https://rubenerd.com/files/2022/caution-lift@1x.jpg"
year: "2022"
category: Media
tag:
- photos
- pointless
location: Sydney
---
<figure><p><img src="https://rubenerd.com/files/2022/caution-lift@1x.jpg" alt="Photo taken in a lift lobby, with two signs. The first says: Caution, Lift out of order. The second says: Do not use lift, move in progress." style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2022/caution-lift@1x.jpg@1x.jpg 1x, https://rubenerd.com/files/2022/caution-lift@2x.jpg 2x" /></p></figure>
