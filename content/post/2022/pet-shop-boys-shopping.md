---
title: "Pet Shop Boys, Shopping"
date: "2022-04-02T17:19:28+11:00"
abstract: "We check it with the city; Then change the law."
year: "2022"
category: Media
tag:
- music
- pet-shop-boys
location: Sydney
---
From their 1987 album *Actually*:

> We check it with the city;<br />
> Then change the law.
