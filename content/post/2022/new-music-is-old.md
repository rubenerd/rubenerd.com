---
title: "New music is old"
date: "2022-12-19T13:32:56+11:00"
abstract: "I overheard a top 40 station at a coffee shop, and most of the songs are remakes. Huh!"
year: "2022"
category: Media
tag:
- music
- music-monday
location: Sydney
---
This [Music Monday](https://rubenerd.com/tag/music-monday/) is more an observation. Clara and I started going to a new coffee shop in the mid-morning to work, which sometimes streams one of those Top 40 radio stations.

What struck me is how awful the radio is! Regardless of your views of current music, everything from the advertisements to the DJs themselves were obnoxious. Was it always this grim, or have I become desensitised?

But for the real observation here: at least half of the songs we heard were remakes! In a one hour sitting we heard rebaked Eiffel 65, Mark Morrison, Elton John, and Fleetwood Mac. They introduced new lyrics, recycled hooks, or only went as far as adding new backing drum loops.

You could read this one of two ways:

* New acts are rediscovering older tunes, and want to introduce them to new audiences.

* New acts are lazy and/or bereft of ideas.

People have always done this, but I'll bet my answer as to whether its good or not will depend on my mood, outlook, and how much coffee I've had.
