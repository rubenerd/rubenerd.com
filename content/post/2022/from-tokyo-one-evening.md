---
title: "From Tokyo one evening"
date: "2022-11-22T19:43:20+09:00"
abstract: "A rambling post about travel, finally!"
thumb: "https://rubenerd.com/files/2022/akiba@1x.jpg"
year: "2022"
category: Travel
tag:
- akihabara
- atelier-ryza
- japan
- osaka
- tokyo
location: Tokyo
---
Hi there! I'm typing this from a cute little coffee shop down the road from our hotel in Kudanshita this evening. The street is mostly quiet, but there are a row of cute keicar-sized vans making a delivery to the combini, and occasionally someone whizzes by on their bike.

I'm so relieved that Japan is starting to accept cards in more places, so I don't need to fumble for coins and notes each time like the baka gaijin that I know I am. I'm *slowly* getting more confident asking for things!

Back in January 2020 the biggest source of travel frustration in Australia was [cancelled flights](https://rubenerd.com/cancelled-flights-between-sydney-and-melbourne/ "Cancelled flights between Sydney and Melbourne"). That day, I'd flown up from Sydney to the Gold Coast in Queensland to present at the [inaugural FreeBSD track](https://rubenerd.com/the-first-freebsd-conference-in-australia/) at Linux.conf.au, and didn't grant the travel second thought. It seems like another time ago, and one we won't ever quite go back to.

Wow, they're playing *Can you Feel the Love Tonight* on the speakers now from the original Lion King; talk about a layer of childhood nostalgia. A twist in the kaleidoscope, moves us all in turn. My mum hated Elton John, but loved that song.

<figure><p><img src="https://rubenerd.com/files/2022/akiba@1x.jpg" alt="An overcast photo around Akihabara with Atelier Ryza among other posters" srcset="https://rubenerd.com/files/2022/akiba@2x.jpg 2x" style="width:500px; height:333px;" /></p></figure>

Most of the world did it far tougher (and continues to do so) than Clara and I did during Post-Covid times, but we also had our share of misfortune and melancholy. I don't know about you, but I felt this year was far tougher than 2020 and 2021. Maybe it was the sheer monotony, coupled with a steady drumbeat of mixed or bad news among friends, family, and the world in general.

Clara and I have always been cautious people, which can be a good when it comes to finances and planning, but it can be stifling when you don't feel like you have control. Losing agency is a deeply unsettling feeling, like standing up from a couple of Kirin Ichibans and walking into a wall like the baka gaijin that you are.

It's also weird sitting among people here right now who'd probably like nothing more than to be somewhere else. Japan to them is monotony and lockdowns, just as Australia was to me before we flew out here last week. Clearly some of us need novelty, or at least something *different!*

<figure><p><img src="https://rubenerd.com/files/2022/namba-parks-3@1x.jpg" alt="A bad but meaningful photo at night at the top of Namba Parks, with some cans of lovely drink from the machine next to us." srcset="https://rubenerd.com/files/2022/namba-parks-3@2x.jpg 2x" style="width:500px; height:333px;" /></p></figure>

So far we've been back to Shibuya, Shinjuku, Nakano, Akihabara, and back down to Osaka on the Shinkansen to check out Namba and Den-Den Town. The latter was especially meaningful; we found again what has become our [favourite park bench in the world](https://rubenerd.com/the-best-blog-park-bench-in-the-world/) among the trees in Namba Parks, then cracked open a hot chocolate from the vending machine next to it. I had photos from that area as my laptop wallpaper since Covid first hit, originally because it was pretty, but soon because it gave me motivation to push on and return one day. It seemed like such a distant and unobtainable possibility, so to be back here felt unreal.

It's also been a joy having my sister and my new brother-in-law here with us too this time; they'd never been to Japan before, and they're enthralled with everything we've seen and done. Excitement is infectious, and they've also encouraged us to try new places we wouldn't have by ourselves. 

My sister jokes that we have delayed emotional responses to things. It was weird; after almost a week of being here, it finally hit that we'd closed a difficult chapter of our lives with our dad's health and lockdown blues. And now I'm back at a Japanese cafe worrying about whether I can remember how to say "I'd like to dine in please."

<figure><p><img src="https://rubenerd.com/files/2022/osaka-castle@1x.jpg" alt="Osaka Castle!" srcset="https://rubenerd.com/files/2022/osaka-castle@2x.jpg 2x" style="width:500px; height:333px;" /></p></figure>

Thanks for making it to the end of this rambling post. Hey look, another bike.
