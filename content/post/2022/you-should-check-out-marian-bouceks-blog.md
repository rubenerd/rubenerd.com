---
title: "You should check out Marian Bouček’s blog"
date: "2022-02-16T07:58:35+11:00"
abstract: "Computers and horse riding :)"
year: "2022"
category: Thoughts
tag:
- blogging
- feedback
location: Sydney
---
Marian Bouček has [started a blog](https://www.boucek.me/blog/), and you should read it and [subscribe](https://www.boucek.me/index.xml)!

I just finished this post about [moving from Apple to FreeBSD](https://www.boucek.me/blog/from-mac-to-freebsd/), which I'm sure you wouldn't be surprised spoke to me, especially of late.

I remember years ago one of those self-anointed writing experts saying blogs should be limited to one topic, but my favourites are ones that aren't. It's a joy being drawn into a site based on a topic I know about, then discovering a whole other world I didn't know. For example, Marian rides horses, and [has a beautiful post about it](https://www.boucek.me/blog/personal-mentor/).
