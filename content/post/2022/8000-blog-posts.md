---
title: "8,000 blog posts!?"
date: "2022-03-18T17:10:55+11:00"
abstract: "This milestone completely slipped me by! What the ever loving…"
thumb: "https://rubenerd.com/files/2022/8000posts@1x.jpg"
year: "2022"
category: Internet
tag:
- pointless-milestone
- weblog
location: Sydney
---
This silly milestone aside, [wc(1)](https://www.freebsd.org/cgi/man.cgi?query=wc "wc FreeBSD manpage reference") reports I've also written 881,118 words. That's delightful.

I started this blog as a teenager in [late 2004](https://rubenerd.com/the-first-post/), and it's among the longest-running projects I've ever maintained. Sitting in a coffee shop in my thirties writing it, and reviewing back at home, it doesn't seem real.

Thank you for taking time out of your day to read my disparate ramblings, whether you're a new arrival or someone who's been coming back for years. You don't know how much it means to me. 💙

<p><img src="https://rubenerd.com/files/2022/8000posts@1x.jpg" srcset="https://rubenerd.com/files/2022/8000posts@1x.jpg 1x, https://rubenerd.com/files/2022/8000posts@2x.jpg 2x" alt="Photo of my secondary dest at home, with my Japanese Panasonic Let's Note showing this site." style="width:500px; height:333px;" /></p>

