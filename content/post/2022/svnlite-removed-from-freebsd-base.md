---
title: "svnlite(1) removed from FreeBSD base"
date: "2022-08-08T12:18:46+10:00"
abstract: "This move to git was decided a year ago, but it still feels like the end of an era."
year: "2022"
category: Software
tag:
- bsd
- freebsd
- svn
location: Sydney
---
This was [decided a year ago](https://reviews.freebsd.org/D30737), but still felt like the end of an era in an old VM over the weekend:

	The following files will be removed as part of updating to
	13.1-RELEASE-p0:
	/usr/bin/svnlite
	/usr/bin/svnliteadmin
	/usr/bin/svnlitebench
	/usr/bin/svnlitedumpfilter
	/usr/bin/svnlitefsfs
	/usr/bin/svnlitelook
	/usr/bin/svnlitemucc
	/usr/bin/svnliterdump
	/usr/bin/svnliteserve
	/usr/bin/svnlitesync
	/usr/bin/svnliteversion

FreeBSD [moved to git](https://wiki.freebsd.org/action/show/Git) starting in 2019. I miss [and prefer](https://svnvsgit.com/) svn, but I empathise why it was necessary. In some alternate universe, Linus moved to it instead of building git, just as he adopted BSD instead of writing the Linux kernel. One can dream.
