---
title: "Social media and tourists"
date: "2022-07-20T09:48:50+10:00"
abstract: "Their behaviour is nothing new, but TikTok is also a special case."
year: "2022"
category: Travel
tag:
- social-media
- sociology
location: Sydney
---
Abhaya Raj Joshi reported on some [unsurprising but still annoying behaviour](https://restofworld.org/2022/nepals-historic-sites-banning-tiktok-creators/) from tourists in Nepal:

> They come in hordes, strike funny poses, dance to loud music, trample over crops, and often stir up unmanageable crowds that cause traffic jams. TikTok creators in Nepal have earned a reputation for disrespecting religious and historic places in their quest to create viral videos, and are now facing a backlash.

Narcissistic tourists are nothing new. I'm sure also I've contributed to faux pas and increased traffic in culturally significant places. Respect is just important a currency as the money you spend as a tourist, and is the only investment that preserves the beauty and viability of the places you want to go.

I feel the need to call that out whenever a story like this comes up, because it's easy to fall into the trap of blaming the latest hotness, whether it be a social media platform, or rebellious teens with their rock 'n' roll.

Having said that, TikTok (and platforms like it) are a unique case. Unlike your parents' camcorder, or even YouTube videos, these platforms incentivise producing short, sharp clips that grab people before their limited attention spans compel them to swipe away. The result is group that can't behave in public, even in comparison to other tourists!

You shouldn't use TikTok for a [whole host](https://www.theguardian.com/technology/2019/sep/25/revealed-how-tiktok-censors-videos-that-do-not-please-beijing) of [other reasons](https://www.theguardian.com/technology/2022/jul/19/tiktok-has-been-accused-of-aggressive-data-harvesting-is-your-information-at-risk) (inb4 [whataboutery](https://rationalwiki.org/wiki/Whataboutism)), but if the platform's users have demonstrated they can't act appropriately in culturally important sites, I think banning smartphones is reasonable.

Now get off my lawn, I'm off to check on my Zooomr. Does that still exist?
