---
title: "(Initial) Vitamin D"
date: "2022-07-21T08:36:24+10:00"
abstract: "Chances are if you read this stuff, you need more of it too."
year: "2022"
category: Thoughts
tag:
- health
- personal
location: Sydney
---
If you're the kind of person who reads a rambling blog that sometimes even discusses technical matters, this may apply to you as well.

My vitamin D wasn't low during my last few blood tests, as much as it didn't register. I was warned that it could lead to all sorts of health problems later in life, and could even get me into trouble if I caught something serious like Covid.

I don't know why, but I would hear that advice, file it under "Ruben needs more exercise and sunlight" and... proceed to do nothing about it. A year would go by, I'd have another test, and be told the same thing. It was as though taking active measures was an admission that something was wrong, and that scared me? It makes no sense, but then, emotions rarely do.

It took Clara being told the same thing that I realised we needed to get this sorted out. The doctor recommended a few supplements, and we've been accountable to each other about taking them since.

That was about a week ago, and I can already tell a dramatic difference in my energy, *especially* in the evenings. It's been a dreary, overcast, dark year in Sydney, and it was getting to the point where I'd often feel overwhelmingly fatigued by 18:00. I now sleep at 22:00 without any problems.

It doesn't feel like like I can run marathons or do an overnight coding session like I would have in my teens. But I feel *normal* again. Now I'm just kicking myself I didn't do it sooner.

*(I probably also chalk this up to why I always felt happier in Southeast Asia, and not just for the novelty. Being bathed in sunlight for a predictable, consistent amount of time during the year more than makes up for the humidity).*
