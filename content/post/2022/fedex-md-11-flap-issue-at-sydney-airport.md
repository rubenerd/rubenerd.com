---
title: "FedEx MD-11 flap issue at Sydney Airport"
date: "2022-03-16T13:04:28+11:00"
abstract: "Some good news from last weekend."
year: "2022"
category: Thoughts
tag:
- aviation
- australia
- news
location: Sydney
---
I'm just getting around to reading about this. A FedEx MD-11 freighter had an aborted landing at Sydney Airport last weekend, with reports of stuck flaps limiting their ability to slow down on their second approach. The pilots issued a mayday, and notified Sydney air traffic control that they'd "require the full length of the runway".

The plane landed at 365 km/h. To get an idea just how fast that is, Adipasqu on the [Airliners.net forum](https://www.airliners.net/forum/viewtopic.php?t=1469989) linked to Boeing's [FAA Reference Code and Approach Speed document](https://www.boeing.com/assets/pdf/commercial/airports/faqs/arcandapproachspeeds.pdf), which lists the approach speed for the MD-11F at 155 knots, or about 287 km/h (not that far off a 747-400). Even if we assume a higher landing weight and faster approach speed, that's a huge delta.

No, not that [delta](https://en.wikipedia.org/wiki/Delta_wing "Wikipedia article on delta wings"). Or that [Delta](https://en.wikipedia.org/wiki/Delta_Air_Lines "Wikipedia article on Delta Air Lines"). Or even... thank you.

Aviation journalist Andrew Curran [reported the potential reasons at Simple Flying](https://simpleflying.com/fedex-md-11-mayday-emergency-sydney/)\:

> Possible problems onboard the FedEx MD-11 on Saturday evening include flap asymmetry (where there is a difference between left and right side flap positions); split flaps (where the flaps may be symmetric, but either the inner flap or outer flap pair has not reached the commanded position); and stuck flaps (where one flap fails to do as commanded, causing the opposite flaps to stop automatically).

Michael Evans at the The Sydney Morning Herald [published part of the exchange](https://www.smh.com.au/national/nsw/mayday-acknowledged-sydney-airport-emergency-after-flight-control-malfunction-20220313-p5a48q.html) of the crew and air traffic control which made me smile. Job well done!

> “I really appreciate the help, sir. We had a flight control malfunction.” The pilot expressed concerns about “a flap” and treated it as a “flight control issue”.
>
> The air traffic controller congratulated the pilot: “Very well handled,” he said.
