---
title: "Music Monday: My favourite theme songs"
date: "2022-09-26T06:42:31+10:00"
abstract: "Frasier, Monk, Haruhi, Evangelion...!"
year: "2022"
category: Thoughts
tag:
- music
- music-monday
- star-trek
- suzumiya-haruhi
location: Sydney
---
<p><img src="https://rubenerd.com/files/2022/haruhi-pointing@1x.png" alt="Suzumiya Haruhi" style="width:128px; float:right; margin:0 0 1em 1em;" srcset="https://rubenerd.com/files/2022/haruhi-pointing@1x.png 1x, https://rubenerd.com/files/2022/haruhi-pointing@2x.png 2x" /></p>

Today's [Music Monday](https://rubenerd.com/tag/music-monday/) is a bit different. These are some of my favourite theme songs:

* [Frasier](https://www.youtube.com/watch?v=dQx-H2av1PM)
* [Suzumiya Haruhi no Yūutsu (ED)](https://youtu.be/u_9D7S5qGK0?t=11)
* [Neon Genesis Evangelion](https://www.youtube.com/watch?v=k8ozVkIkr-g)
* [Monk](https://www.youtube.com/watch?v=kXEMxPzxE68)
* [Star Trek: The Next Generation](https://www.youtube.com/watch?v=enPAs_g0T5c)
* [Twin Peaks](https://www.youtube.com/watch?v=nCn3LYqCnrk)
* [Seinfeld](https://www.youtube.com/watch?v=_V2sBURgUBI)

*Haruhi hears the blues a-calling... tossed salads and scrambled eggs. Oh my. And maybe I seem a bit confused. Yeah maybe, but I got you pegged... hah <strong>HA</strong> hah ha!*
