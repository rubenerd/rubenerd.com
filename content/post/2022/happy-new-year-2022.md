---
title: "Happy New Year 2022"
date: "2022-01-01T10:00:16+11:00"
abstract: "I’m taking this as a challenge."
year: "2022"
category: Thoughts
tag:
- pointless-milestone
location: Sydney
---
Shamelessly [reposting myself from Mastodon](https://bsd.network/@rubenerd/107541234988123436) last night:

> I’m logging off for the night. Happy New Year everyone. I’m taking that as a challenge. Let’s do this 💪👋

What have you got planned for today? I'm finally going to upgrade my last Mac to Monterey Jack, commit all the stuff I didn't get around to in various personal Git repos, and have a long, socially-distanced walk with Clara.

A cup of coffee sounds like a good idea, too.

