---
title: "HOLOSTARS-EN TEMPUS"
date: "2022-08-02T08:25:12+1000"
abstract: "These gents have now become a regular part of Clara's and my evenings."
thumb: "https://rubenerd.com/files/2022/holostars@1x.jpg"
year: "2022"
category: Media
tag:
- hololive
- holostars
- youtube
- video
- vtubers
location: Sydney
---
The Myth girls broke new ground when they [introduced Hololive to an English audience](https://rubenerd.com/live-lessons-from-hololives-amelia/ "Life lessons from Hololive's Amelia") in 2020. Any doubts people had for their success were swiftly overcome by their creativity, personas, and effort they put into their streams, back stories, collaborations, art, music, and community. [IRyS](https://rubenerd.com/iryss-first-collaboration-with-holoen/ "IRyS's first collaboration with HoloEN") and [Council](https://rubenerd.com/ouro-kroniis-debut-stream-krotime/ "Ouro Kronii's debut") continued boldly in their footsteps.

TEMPUS have now done this for the dapper HOLOSTARS community, and I can already tell it's going to be fun. Thus far I've only seen [Axel Syrios's debut stream](https://www.youtube.com/watch?v=JRTOfhbdbn0), but their [first collab](https://www.youtube.com/watch?v=vKUAtIT1O9s) and [Magni Dezmond's Spore stream](https://www.youtube.com/watch?v=slZW5rR6CHM) had Clara and I crying with laughter.

They're **so good**. Cover's casting process picked a group that has such great chemistry. I love that they've already forged their own direction; the vibe, conversational style, and how they interact with chat feels like a departure from Hololive. They also bring a lot of knowledge and experience into what they play, and take the time to explain for newbies how they do things. This isn't "just" an English male version of Hololive, it's a completely different experience.

*(I've only seen clips of HOLOSTARS-JP, so this might be more more in keeping with what their senpais do. Maybe someone who actually speaks Japanese can comment).*

<figure><p><img src="https://rubenerd.com/files/2022/holostars@1x.jpg" alt="The TEMPUS gentleman in their debut stream." style="width:500px; height:281px;" srcset="https://rubenerd.com/files/2022/holostars@1x.jpg 1x, https://rubenerd.com/files/2022/holostars@2x.jpg 2x" /></p></figure>

The elephant in the room is whether they'll engage with Hololive. Japanese idol culture and [music](https://rubenerd.com/ghost-by-suisei/ "Ghost by Suisei") is fun, but it also comes with an implicit assumption that their stars are "available" (you know what I mean). Vtubers have a bit more insulation between their online and iRL personas, but they still have to tread carefully.

As a concrete example, [Hololive Minecraft streams](https://rubenerd.com/the-ceresfauna-built-a-minecraft-kitchen/ "The @ceresfauna built a Minecraft kitchen") are Clara's and my favourite, but while Hololive English, Indonesia, and Japan now share the same server, I'm fairly sure HOLOSTARS don't.

There's reason to think English and Indonesia might not be limited in the same way. From what I've seen of the community that's already sprung up around TEMPUS, there's significant overlap that might not exist in Japan. Bae and Mori's streams with male vtubers have also met with widespread acclaim and praise.

Either way, I wish the gents all the best. They're part of our evening rotation now ^_^/.
