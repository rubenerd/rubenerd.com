---
title: "Sri Lanka in 2022"
date: "2022-07-12T12:00:50+10:00"
abstract: "With links to the The Foundation of Goodness where you can donate to send food to people who need it."
year: "2022"
category: Thoughts
tag:
- asia
- politics
location: Sydney
---
Alongside the illegal and unjustified invasion of Ukraine, I've felt nothing but frustration and anger at what's happening in Sri Lanka. DW News Asia [presented a sobering summary](https://www.youtube.com/watch?v=XdcfWLyFC48) documenting the resignation of President Gotabaya Rajapaksa, and the deteriorating economic and social situation.

[The Foundation of Goodness](https://unconditionalcompassion.org/donate/) is a registered charity in Sri Lanka, the United States, and Australia helping to distribute food and other vital supplies. Please consider helping if you can.

Please also [let me know](https://rubenerd.com/about/#contact) if there are other charities and organisations I should be linking to as well, it's been harder than I expected finding them.
