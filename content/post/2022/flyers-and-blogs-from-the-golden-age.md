---
title: "Flyers, and blogs from the golden age"
date: "2022-03-23T22:06:44+10:00"
abstract: "WordArt and Calibri doesn’t a convincing flyer make."
year: "2022"
category: Thoughts
tag:
- blogging
- politics
location: Sydney
---
There's something bittersweet about finding an abandoned blog. They're windows into another time like a news article, but personal. So many were started in the golden age of blogging in the mid-2000s, replete with that header-sidebar-content design that I miss. There's also the question about why so many of them stopped, and why a precious few kept going.

I may or may not archive their posts into a local WordPress install for exploring at a later date. Copyright prevents me from republishing this amalgamated site, even if I doubt their original authors would mind, care, or even remember.

This morning I found [Cori's parenthetical](https://cori.wordpress.com/), which ran from October 2005 to May 2012, almost a decade ago. He had active conversations with Dave Winer back in the day about RSS and OPML, though it was his comment about a [flyer his wife received](https://cori.wordpress.com/2007/05/30/the-importance-of-transparency-in-activism/) that piqued my interest:

> No where on this document does it say who printed it or who’s handing it out.  Who are these people?  Do they have an interest in this company aside from concerns for the safety of the community?  maybe they work for a company that lost the bid?  Or maybe there’s a disgruntled ex-employee behind it all.  Taken as a sum, the anonymity of the source of the flyer and the alarmist language and graphics make me pretty much have to dismiss the whole thing.

I used to get similar flyers walking around Sydney. Some were carefully written and presented, but almost all had 1990s-era WordArt, default fonts, and generous amounts of spelling mistakes. I always wondered why people put so little effort into the design; surely they must know people dismiss their message out of hand when they look like that. *I guess not!*

Cori also gives food for thought [on the scientific method](https://cori.wordpress.com/2010/08/19/experiments-always-succeed/), which journalists often fail to communicate in their research reporting:

> [A]n experiment *only* fails if you approach it with the intent to prove a specific hypothesis. In the truest spirit of the scientific method this should never be the case – you undertake an experiment in order to form a hypothesis. If the experiment doesn’t support your conclusion then either your conclusion is incorrect, you have misinterpreted the data, or the experiment you designed isn’t testing what you thought it was. The experiment *always* does what it’s designed to do, and any failure is with the experimenter.
