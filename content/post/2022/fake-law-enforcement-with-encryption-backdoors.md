---
title: "Fake law enforcement with encryption backdoors"
date: "2022-04-07T08:10:40+10:00"
abstract: "Bruce Schneier connects the dots with big company failures, and encryption backdoors."
year: "2022"
category: Internet
tag:
- bruce-schneier
- news
- security
location: Sydney
---
[Brian Krebs reported](https://krebsonsecurity.com/2022/03/hackers-gaining-power-of-subpoena-via-fake-emergency-data-requests/) a growing trend of fake law enforcement requests to hand over user data, which companies are falling for:

> [&hellip;] some hackers have figured out there is no quick and easy way for a company that receives one of these [Emergency Data Requests] to know whether it is legitimate. Using their illicit access to police email systems, the hackers will send a fake EDR along with an attestation that innocent people will likely suffer greatly or die unless the requested data is provided immediately.

[Bruce Schnier connects](https://www.schneier.com/blog/archives/2022/04/hackers-using-fake-police-data-requests-against-tech-companies.html) the logical dots with a related issue we've been warning about ever since former prime minister Malcolm Turnbull said the "laws of maths don't trump the laws of Australia":

> The “credentials” are even more insecure than we could have imagined: access to an email address. And the data, of course, isn’t very secure. But imagine how this kind of thing could be abused with a law enforcement encryption backdoor.
