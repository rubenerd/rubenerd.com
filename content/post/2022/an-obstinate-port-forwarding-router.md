---
title: "An obstinate port-forwarding router"
date: "2022-08-07T10:50:19+1000"
abstract: "The cobbler’s son walks barefoot."
year: "2022"
category: Hardware
tag:
- networking
location: Sydney
---
I had reason to port forward through my home router's NAT to our bhyve box this weekend. There are some updates to Minecraft and Plex, and I wanted to do my Sunday maintenance from a coffee shop over SSH, like a gentleman.

Before I left, I opened the requisite port and enabled the port forward on the router. I tested it from an external IP and... nothing. I rebooted it to confirm the setting was correct and had been committed... still nothing. OpenSSH dutifully timed out each time.

I was in a hurry and couldn't be bothered doing a port scan or any further troubleshooting, so I opened a remote SSH tunnel to my external jump box and left.

You know the saying that the cobbler's son walks barefoot? Well we use the crappy home router our ISP gave us when we moved; albeit one I keep regularly patched. Its Wi-Fi range is more than sufficient for our tiny apartment, and I haven't ever been bothered to replace it because *if it ain't broke, don't fix it*. But the fact I couldn't get a console up or even do basic troubleshooting in a pinch may be enough to convince me otherwise.

In the old days I ran a router directly on my homelab server, and relegated IPoE duties of our Australian NBN connection to an external modem. I'm tempted to do this again, or get some MikroTik kit 🇱🇻. I like the idea of an all-in-one box for a modem, router, switch, and access point, especially given our limited space. Something small, quiet, and easy to configure sounds fantastic.

I just wish there was a single box that did everything *and* 2/5G Ethernet. Well, 10G Ethernet too, but that's also only existed for *two decades* at this stage and is still too expensive for me to prioritise.
