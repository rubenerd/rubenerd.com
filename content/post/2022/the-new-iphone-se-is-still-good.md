---
title: "The new iPhone SE is still good"
date: "2022-03-09T08:41:07+11:00"
abstract: "Touch ID and LCD, phew!"
year: "2022"
category: Hardware
tag:
- iphone
- iphone-se
location: Sydney
---
*What? The new iPhone SE still has that dated enclosure!?* Yay!

I skimmed the product page for the new, third-generation iPhone SE that was just announced, worried about what it could be. It still has Touch ID, and an LCD. I'm *unreasonably* relieved. Apple may consider those with OLED sensitivity as an afterthought, but at least we still warrant a thought at all.

I'll still be running my iPhone 8 and 6S for personal and work respectively, but will upgrade once updates aren't available. The irony is, Android would have forced me to upgrade years ago.
