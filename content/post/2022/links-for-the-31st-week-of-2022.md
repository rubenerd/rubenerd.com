---
title: "Links for week 31, 2022"
date: "2022-08-05T08:51:43+1000"
abstract: "Web design, Bitcoin, airport security, Mahathir’s Malaysian gov aspirations (again) and Intel Arc GPUs."
year: "2022"
category: Thoughts
tag:
- listpost
location: Sydney
---
We haven't had a [listpost](https://rubenerd.com/tag/listpost/) for a while. Here's some stuff I've read this week:

[Should I Use a Carousel?](https://shouldiuseacarousel.com/)
: <p>The answer may surprise you!</p>

[Stories from the Cavelab: Making planter boxes for Mona](https://blog.cavelab.dev/2022/07/planter-boxes/)
: <p>It’s been so much fun watching Thomas’ latest house additions come together. 🌷</p>

[Amy Castor and David Gerard: Bitcoin mining in the crypto crash — the mining companies’ creative accounting](https://amycastor.com/2022/08/04/bitcoin-mining-in-the-crypto-crash-the-mining-companies-creative-accounting/)
: <p>Rather than selling their bitcoins and tanking the market further, miners have been using them as collateral for loans. It’s going as well as you’d expect.</p>

[SoraNews24: JAL system makes air travel easier and lets you keep laptops, liquids in bag for security check](https://soranews24.com/2022/08/05/jal-system-makes-air-travel-easier-and-lets-you-keep-laptops-liquids-in-bag-for-security-check/)
: <p>Most airport security is still theatre, but I’m all for making the process easier. Most flights from Australia to Tokyo go to Narita; here’s hoping the system is extended from Haneda when we’re (finally!) allowed to return.</p>

[Rubenerd: Links for the 31st week of 2022](https://rubenerd.com/links-for-the-31st-week-of-2022/)
: <p>Meta! Damn it, Facebook even ruined <em>that</em> word.</p>

[Nikkei Asia: Mahathir launches Malay political alliance with election in sights](https://asia.nikkei.com/Politics/Malaysia-in-transition/Mahathir-launches-Malay-political-alliance-with-election-in-sights)
: <p>Mahathir is Malaysia’s proverbial whack-a-mole. Without the support of Anwar Ibrahim's progressive coalition this time though, I don’t like his chances. My friends I made over there are more bemused than anything else.</p>

[Tom's Hardware: Intel Arc Board Partner Ceasing Production](https://www.tomshardware.com/news/intel-arc-issues-igors-lab-reporting)
: <p>The discrete GPU market needs Intel’s competition, but production delays, reported driver issues, and now news like this makes me think it won’t be a substantial force anytime soon.</p>
