---
title: "That elusive Commodore 128 80-column mode"
date: "2022-12-30T09:20:35+11:00"
abstract: "A new converter box, MOS 8563, and 64 K memory upgrade didn’t make a difference."
year: "2022"
category: Hardware
tag:
- commodore
- commodore-128
- retrocomputing
- troubleshooting
location: Sydney
---
The 80-column mode on my Commodore 128 from the [imitable Screenbeard](https://the.geekorium.au/) continues to produce no output, regardless of whether its involved with the 40/80 switch, or using **GRAPHIC 5** in the functional 40-column mode. But I think I've made progress since [blogging about it last year](https://rubenerd.com/troubleshooting-my-commodore-128s-80-column-mode-part-1).

So far I've:

* Replaced my MGA/CGA to VGA converter with one using a different FPGA, because I like lots of initialisms. Somewhat redundant given I confirmed I wasn't getting any monochrome composite output either from the RS 232 connector, but tried just in case.

* Replaced the MOS 8563 R9 with two other known-good chips; I got an extra from a gentleman in Germany who sadly couldn't revive his spare 128D.

* Added a 64 K RAM upgrade daughter board, which overrides the dedicated RAM for the 8563 in case any of that was fried. I also have a more modern SaRuMan replacement that uses less power to try.

All the voltages across the machine look fine, though running the 128 diagnostic cart [recently showed RAM errors](https://bsd.network/@rubenerd/109566668564839615). The 80-column mode has its own memory, but maybe it still can't initialise correctly. I have a SaRuMan 128 RAM replacement on its way, so we'll see if that makes a difference.

The other possibility is a faulty timing crystal. The 8563 has its own discrete crystal separate from the rest of the system, but I've seen a few YouTubers report issues with the 80-column display that disappeared when they replaced it. I've got a new one in the post for that too.

Now that I have a [dedicated Commodore 64C](https://rubenerd.com/i-bought-a-commodore-64c/), my hope was to transform the 128 into my primary CP/M and DesTerm128 tinkering machine, as well as learning more about the Z80. Getting 80-column working on it would be absolutely *glorious!*

