---
title: "Black Friday as an opportunity to unsubscribe"
date: "2022-12-02T08:21:22+11:00"
abstract: "It’s an excellent signal these businesses voluntarily use to remove yourself from their lists."
year: "2022"
category: Internet
tag:
- email
- spam
location: Sydney
---
*Black Friday* is a relatively recent cultural import into places like Singapore and Australia that I wished had been left in the US, for reasons that are beyond the scope of this post.

But the term is an *incredibly* useful signal that an email sender can be unsubscribed from! You couldn't pick a better method for filtering, especially considering these businesses volunteer to use it.

I searched in Fastmail for this one term, and I've already unsubscribed from more than two dozen lists. *Cyber Monday* didn't return as many, but was still useful for the same reason.

If you're drowning in email&mdash;and who isn't?&mdash;give it a try. You might be as surprised as I was.
