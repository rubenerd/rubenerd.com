---
title: "Installing stable Perl with Perlbrew"
date: "2022-07-10T13:00:08+10:00"
abstract: "perlbrew install --switch stable"
thumb: "https://rubenerd.com/files/uploads/Mimetypes-text-x-perl-icon.png"
year: "2022"
category: Software
tag:
- europe
- food
- package-managers
- perl
- poland
location: Sydney
---
Today I learned you can install the most recent stable version of Perl in Perlbrew, and switch to it from one line:

    $ perlbrew install --switch stable

Perl is still my favourite programming language, and [Perlbrew](https://perlbrew.pl/) is still my preferred method to install it (with [pkgsrc](https://pkgsrc.se/lang/perl5) coming a close second, depending on the environment). And not just because Perlbrew, like other Perl tools, uses the <abbr title="top-level domain">TLD</abbr> for a country I wish to [visit one day](https://en.wikipedia.org/wiki/.pl)!

As an aside, if you're in Sydney, [Alchemy](https://www.alchemy-restaurant.com/) in Surry Hills has the best Polish food in Australia. The owner is also lovely.

