---
title: "A great relationship with his garbage contractor"
date: "2022-09-25T09:13:08+10:00"
abstract: "I loved this observation by a friend on Mastodon."
year: "2022"
category: Thoughts
tag:
- economics
- human-rights
- sociology
location: Sydney
---
Someone with a private account on Mastodon shared a lovely story about how he has the mobile number of his garbage collector, and could easily arrange to have his garbage bin replaced. He surmised this sort of relationship must be a rural Australian thing, because people in the city would be unlikely to know their collector by name and number.

He's right. I don't know our garbage or recycling collectors. I'm even a step removed living in an apartment building; we also have sanitation workers who maintain the chutes and take out the skips, in addition to the people who operate the trucks and align the bins. But that's not from avoiding them, I simply never see them.

But they raise an interesting point, and it was something I noticed more in Singapore. I *did* know our garbage collectors at our last apartment building, to the point where I'd wave to them on the way to school each week, and they'd shout out and wave back. I got to know the security guards in our apartment and office buildings by name, and would often bring them back a coffee or snack if I was heading that way. There was a gentleman who'd sweep the road of leaves that I'd also wave to, and would shake their hand if I happened to be walking nearby. He barely spoke English, I barely spoke Hokkien, but smiles transcend language.

Australia isn't as egalitarian as people think, but I do see more of this camaraderie among strangers than in most places I've visited. The old joke among New York City taxi drivers was that they could tell if an Australian was hailing them, because we get into the front seat to talk with them instead of the back. I did this all the time in Singapore too.

There's this attitude among people that one's occupation, expertise, standing, or income should dictate how you treat other people. I don't just think that's bunk, I actively push back on it. People are people. Retail workers earning minimum wage don't work any less hard than I do, for example. Likewise, I'm going to talk to a professor or managing director the same way I'd talk with anyone else.

I didn't entirely agree with George Carlin's line that respect is earned, not automatic. I think everyone starts with a baseline, and it's yours to grow or lose. Punching or talking down is an immediate red card for me. I'm sure there was a point among all those analogies.
