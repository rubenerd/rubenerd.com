---
title: "Doing things before you need to"
date: "2022-05-10T17:23:37+10:00"
abstract: "Cook food before you’re hungry, or you’ll make poor choices."
year: "2022"
category: Thoughts
tag:
- health
- philosophy
location: Sydney
---
My late mum used to say you should start cooking dinner *before* you're hungry, otherwise you make poor decisions about nutrition to satiate your immediate appetite. The same applies for grocery shopping on an empty stomach.

My dad used to say that Formula 1 drivers have to react to turns *before* they see them, or it's already too late.

I only realised recently they were saying the same thing.

