---
title: "Social media filters need image recognition"
date: "2022-03-29T08:38:54+11:00"
abstract: "I don’t care what an actor does. Block *all* the memes!"
thumb: "https://rubenerd.com/files/2022/6afqd4.jpg"
year: "2022"
category: Internet
tag:
- social-media
- twitter
location: Sydney
---
Have you ever wished you could block specific images, not just keywords or text from social media? Or even RSS feeds?

Some actors did something yesterday, and it's all the news outlets and social media cares about. Last week it was an Australian politician doing something ridiculous, with equally tedious results. One can filter such news by muting words, phrases, and names, but that doesn't stop reposted images without alt text.

<p><img src="https://rubenerd.com/files/2022/6afqd4.jpg" alt="One does not simply block memes" style="width:325px; height:192px;" alt="One does not simply block memes" /></p>

In some ways, memes are the perfect format for image blocking. The bulk of the photo necessarily doesn't change, so the fuzziest of pattern matching tools could detect and block them.

I'd be willing to pay extra to get this! You could call it *Twitter Clear*, or Mastodon and RSS clients could highlight this as another differentiating feature.
