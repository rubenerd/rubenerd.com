---
title: "Real Engineering on natural gas and renewables"
date: "2022-01-31T13:06:29+11:00"
abstract: "Natural gas is not an ally in our fight. It’s not clean at all, and is even more potent a greenhouse gas."
year: "2022"
category: Thoughts
tag:
- environment
location: Sydney
---
Real Engineering produced another excellent video, this time [about carbon taxes](https://nebula.app/videos/realengineering-the-truth-about-carbon-taxes). Citing two MIT studies, he discusses the economic impact our carbon gluttony is having on the world, especially on those who can least afford it. He then explores four scenarios in which a carbon tax is introduced, and how it would affect a more rapid shift to lower-carbon fuel sources.

If we assume the video portrayed these studies fairly (and there's no reason not to, Real Engineering is one of the most well-cited YouTubers I've ever watched), the scenarios result in natural gas playing a significant part in our future energy mix. Natural gas emits far less carbon per joule than competing fossil fuels, and isn't as susceptible to changes in weather that renewables like wind and solar are.

This uncritical approach to natural gas has coloured a few of Real Engineering's videos, but I fear it's misplaced.

Natural gas isn't an ally in our fight against climate change; or at least, it comes with plenty of its own problems. It's far more potent (albeit shorter lived) as a greenhouse gas than carbon dioxide. Even if burned cleanly, its extraction, processing, and delivery unavoidably leaks it in significant quantities, to say nothing of the threats posed by fracking.

It should be left in the ground, naturally and efficiently sequestering the carbon it's held for millions of years.

I'd be far more interested to see studies that compared the climate impact of replacing coal and oil with another fossil fuel, rather than a blinkered view of carbon emissions. Because like my own glowing reviews of my site here, I'm seeing a lot of unchallenged assumptions.
