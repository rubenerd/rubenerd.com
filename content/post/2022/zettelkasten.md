---
title: "Zettelkasten"
date: "2022-12-12T08:26:09+11:00"
abstract: "The system of storing ideas on paper cards, and how it relates to computers."
year: "2022"
category: Thoughts
tag:
- cardfile
- notes
- studying
location: Sydney
---
@Ploum@mamot.fr [reminded me](https://mamot.fr/@ploum/109495124073671057) of *Zettelkasten* when talking about writing and organising research.

> A Zettelkasten (German: "slip box", plural Zettelkästen) or card file consists of small items of information stored on paper slips or cards that may be linked to each other through subject headings or other metadata such as numbers and tags. It has often been used as a system of note-taking and personal knowledge management for research, study, and writing.

I used an approach like this when studying. The thing I loved about was that it grew organically without defining a formal structure, but was still simple to navigate and use. Most of my teachers thought it looked chaotic and disorganised, but I remember my year 12 chemistry teacher being extremely interested. Of course, one of his parents were German too!

<p><img src="https://rubenerd.com/files/2022/cardfile@1x.png" alt="CardFile's application icon from Windows 3.1" style="width:128px; height:128px; float:right; margin:0 0 1em 2em;" srcset="https://rubenerd.com/files/2022/cardfile@2x.png" /></p>

I joke that SkiFree was my favourite software Microsoft released, but in reality it might be CardFile. It was barebones and missing a bunch of features to make it great, but even in primary school I had tons of random things stored on them. Emacs Orgmode reminds me of it in some ways too, which is why I keep flirting with the idea of using the editor full time.

I used to call my personal wiki and folder of text files my *Zettelkasten*, the latter of which I'm renaming back to right now :).

