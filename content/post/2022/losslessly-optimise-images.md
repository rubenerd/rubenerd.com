---
title: "Losslessly optimising images"
date: "2022-02-01T08:44:57+11:00"
abstract: "pngcrush, jpegoptim, gifsicle, and scour for SVGs"
year: "2022"
category: Software
tag:
- images
- scripting
- things-you-already-know-unless-you-dont
location: Sydney
---
In today's installment of [things you already know unless you don't](https://rubenerd.com/tag/things-you-already-know-unless-you-dont/), there are a few tools you can use to losslessly optimise and reduce file sizes of various image formats. Yes, even lossy formats have some slack.

Glenn Randers-Pehrson's [pngcrush](https://pmt.sourceforge.io/pngcrush/):

	$ pngcrush -reduce -verbose $IN.png $OUT.png

Timo Kokkonen's [jpegoptim](https://github.com/tjko/jpegoptim)\:

	$ jpegoptim --all-normal --verbose $IN.jpg $OUT.jpg

Eddie Kohler's [gifsicle](https://www.lcdf.org/gifsicle/)\:

	$ gifsicle -O3 --verbose -i $IN.gif -o $OUT.gif

Jeff Schiller and Louis Simard's [scour](https://github.com/scour-project/scour):

	$ scour -i $IN.svg -o $OUT.svg

I have a glorified shell script that invokes the right optimiser depending on an input file's extension. Every file that ends up here goes through one of these.

It's funny to think about video sites serving terabytes of content a second, and here I am shaving a few kilobytes here and there from your downloads on my silly little blog. But every bit (hah!) helps.
