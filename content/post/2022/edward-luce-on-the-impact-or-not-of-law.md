---
title: "Edward Luce on the impact (or not) of law"
date: "2022-06-24T10:42:57+1000"
abstract: "The law cannot be indifferent to the impact of its restraint."
year: "2022"
category: Thoughts
tag:
- politics
location: Sydney
---
This was an article he wrote about the [6th of January hearings](https://www.channelnewsasia.com/commentary/trump-won-election-us-president-capitol-riot-hearings-2765446) in the United States, but this perfectly-written assertion could apply to so many things, not least those in the digital realm:

> The risk of doing nothing is great. The law cannot be indifferent to the impact of its restraint.
