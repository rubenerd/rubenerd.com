---
title: "Walter E. Williams, “All It Takes is Guts”"
date: "2022-03-04T06:46:39+11:00"
abstract: "You keep what you earn... assuming you earned it."
year: "2022"
category: Thoughts
tag:
- economics
- philosophy
location: Sydney
---
[Via Wikiquote](https://en.wikiquote.org/wiki/Social_justice#W):

> Let me offer you my definition of social justice: I keep what I earn and you keep what you earn. Do you disagree? (1) Well then tell me how much of what I earn belongs to you (2) - and why? (3)

1. Yes. You presuppose we earned all that we kept. We didn't. Acknowledging this doesn't invalidate the effort we also contributed to our circumstances.

2. Whatever it costs to nurture the society that made our earnings possible, which belongs to *us*.

3. Because anything less is dishonest for those with ethics, and still counterproductive for the selfish.

There's no such thing as a self-made person, unless we invent human cloning machines.
