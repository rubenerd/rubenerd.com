---
title: "Heroku cancelling free and inactive accounts"
date: "2022-08-27T08:42:31+10:00"
abstract: "Closing accounts, marking and deleting others as inactive, and the inevitability of such announcements"
year: "2022"
category: Internet
tag:
- uh-oh
location: Sydney
---
[Heroku's announcement](https://blog.heroku.com/next-chapter)\:

> Back in May, I wrote about my enthusiasm to be part of the Heroku story, and I remain just as passionate today about helping write the next chapter. [&hellip;] We want to be clear: The priority going forward is to support customers of all sizes

<figure class="smaller"><p><img src="https://rubenerd.com/files/2022/say-the-line-1@1x.jpg" alt="Say the line, Bart!" style="width:320px; height:192px;" srcset="https://rubenerd.com/files/2022/say-the-line-1@1x.jpg 1x, https://rubenerd.com/files/2022/say-the-line-1@2x.jpg 2x" /></p></figure>

> Starting October 26, 2022, we will begin deleting inactive accounts and associated storage for accounts that have been inactive for over a year. Starting November 28, 2022, we plan to stop offering free product plans and plan to start shutting down free dynos and data services.

<figure class="smaller"><p><img src="https://rubenerd.com/files/2022/say-the-line-2@1x.jpg" alt="Yaay!" style="width:320px; height:192px;" srcset="https://rubenerd.com/files/2022/say-the-line-2@1x.jpg 1x, https://rubenerd.com/files/2022/say-the-line-2@2x.jpg 2x" /></p></figure>
