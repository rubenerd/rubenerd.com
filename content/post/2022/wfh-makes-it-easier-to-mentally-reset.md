---
title: "Mentally resetting during the day"
date: "2022-05-19T19:19:12+10:00"
abstract: "A quick afternoon shower and a hot tea does wonders. Also, coffee."
year: "2022"
category: Thoughts
tag:
- psychology
location: Sydney
---
How do you mentally reset, and a [microbreak](https://rubenerd.com/microbreaks/) doesn't cut it? Say you're looking at a terminal prompt, or a blank email, or an empty text file, and your brain is mush. Or maybe you're an introvert who's just finished a long client meeting.

When I worked in an office five days a week, I got into an afternoon habit of stretching my legs and getting a coffee. This had the cumulative negative side effect of taxing my weekly budget more than I expected, not to mention the added time taken to wash my reusable takeaway cups. The humanity! But I always came back to my desk feeling pumped and refreshed, and I'll bet it wasn't just the caffeine.

Working from home (WFH) provides even more flexibility. I love taking brief showers after long calls, or after a mammoth technical writing session. It's the same reason I buck the Southeast Asian trend and shower first thing in the morning as well; it forces my brain to transition from sleep state to (something closer resembling) one of awareness. When time doesn't permit, even splashing cold water on my face makes a big difference, then walking to the kitchen and making a hot cup of tea.

The world is slowly moving back to working in offices again, albeit hopefully with more flexibility. I enjoy spending time with my colleagues, but I'll also miss my sneaky showers.
