---
title: "Canon Canoscan LiDE220 review"
date: "2022-10-13T08:45:07+11:00"
abstract: "This has been the most robust piece of kit I’ve had in years."
thumb: "https://rubenerd.com/files/2022/lide220@1x.jpg"
year: "2022"
category: Hardware
tag:
- canon
- reviews
- scanners
location: Sydney
---
Clara and I scan a *lot* of stuff. More than any rational person would. I'm so afraid of clutter and paper than *anything* we get goes through the scanner and archived. When we're not doing that, we're scanning boxes of old stuff, either ours or for my old man.

We literally wore out or broke three previous scanners, but this Canon LiDE 220 has been running non-stop for more than four years. Thousands of pages, in some cases hundreds in an afternoon... *and it still works flawlessly*.

<figure><p><img src="https://rubenerd.com/files/2022/lide220@1x.jpg" alt="Photo of the Canon LiDE 220" srcset="https://rubenerd.com/files/2022/lide220@1x.jpg 1x, https://rubenerd.com/files/2022/lide220@2x.jpg 2x" style="width:459px; height:273px;"/></p></figure>

If you do some light scanning, this probably won't matter. If you scan things to the edge of death, you need this scanner, or whichever newer model Canon is selling as a replacement. It's been the most robust, dependable piece of kit I've used in years.

