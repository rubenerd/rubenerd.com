---
title: "Grailed Jeanne Alter in FGO quotes Daft Punk"
date: "2022-12-12T07:56:07+11:00"
abstract: "Stronger, Better!"
thumb: "https://rubenerd.com/files/2022/jeanne-alter-not-impressed@1x.jpg"
year: "2022"
category: Media
tag:
- fate-grand-order
- games
- music
- music-monday
location: Sydney
---
Today's [Music Monday](https://rubenerd.com/tag/music-monday/) has Jeanne Alter quoting the legendary French electronica band. *Kinda*. And... why does she *still* look so unimpressed!?

<figure><p><img src="https://rubenerd.com/files/2022/jeanne-alter-not-impressed@1x.jpg" srcset="https://rubenerd.com/files/2022/jeanne-alter-not-impressed@1x.jpg 1x, https://rubenerd.com/files/2022/jeanne-alter-not-impressed@2x.jpg 2x" alt="Jeanne Alter Beserker saying: Stronger, Better" /></p></figure>

<a href="https://grandorder.wiki/Jeanne_d%27Arc_(Berserker_Alter)">Jeanne Alter</a> is still my favourite Beserker in <a href="https://fate-go.us/">Fate/Grand Order</a>. I always play defensive in everything (you could probably read something into that), but having a tank with an awesome NP and strength does help. Not to mention her [gap moe](https://anime.stackexchange.com/a/61600) in the summer event story a couple of years ago.

And as for the *Daft Punk* song, I still can't believe how well the [music video holds up](https://www.youtube.com/watch?v=gAjR4_CbPpQ "Daft Punk - Harder, Better, Faster, Stronger (Official Video)") for it today. What a 1990s icon.
