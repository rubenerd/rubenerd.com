---
title: "Down to my last Aeropress filter"
date: "2022-03-26T09:17:18+11:00"
abstract: "This seems like a big occasion. o7"
thumb: "https://rubenerd.com/files/2022/aeropress-filter@1x.jpg"
year: "2022"
category: Hardware
tag:
- aeropress
- coffee
location: Sydney
---
This seems like a big occasion. I bought a few stacks of Aeropress filter paper right when the Covid pandemic hit Australia in early 2022, and today I used the last one. I'd better order some more!

<p><img src="https://rubenerd.com/files/2022/aeropress-filter@1x.jpg" srcset="https://rubenerd.com/files/2022/aeropress-filter@1x.jpg 1x, https://rubenerd.com/files/2022/aeropress-filter@2x.jpg 2x" alt="Photo of my last Aeropress filter with the device's stand on our kitchen counter." style="width:500px; height:333px;" /></p>

I'd loved the Aeropress before, but it's definitely proved itself over the last couple of years. I've pulled between one and four cups of encouragement juice through it every day for Clara and myself, which has to be thousands by now.

Save for a few occasions where I didn't put the filter paper in properly and had it explode tasty beverage across the counter, it's been one of the few reliable, stedfast things in my life over this time. Between that and my budget K-Mart grinder, every coffee I've made has been delicious. Like precious few things, it hits the marketing triangle of good, easy, *and* affordable.

I'm on a bender to get rid of junk I don't need, but recently I've been flipping this thought process into what I *do* need to the exclusion of other things. The Aeropress would not only be on that latter list, but I'd go as far as to 3D frame it when its no longer tenable to use. It was one of the things that saved us during lockdowns, and for that I'm grateful.
