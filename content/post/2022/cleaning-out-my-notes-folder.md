---
title: "Cleaning out my notes folder, 2022"
date: "2022-12-15T12:25:27+11:00"
abstract: "I do this every year, and there’s always some random stuff."
year: "2022"
category: Thoughts
tag:
- bsd
- freebsd
- hololive
- notes
- random
- taiwan
- time
location: Sydney
---
This is one of my end-of-year rituals. Here are a bunch of disjointed thoughts from the last twelve months, with as much context given here as I gave myself:

> I had an epiphany recently on why religious and political officials don’t have qualms with hypocrisy: when they do it, it’s in the service of preventing something bad happening. It's evil when *others* do it.

> じゃんばら 秋葉原4 sells Japanese Panasonic laptops. FreeBSD Wireless on them: add `if_iwm_load="YES"` and `iwm8265fw_load="YES"` to `/boot/loader.conf`.

> See in Taiwan: [Taroko Gorge](https://lifeoftaiwan.com/places-to-go/taroko-gorge/), and [Shifen Old Streets](https://guidetotaipei.com/visit/shifen-old-streets-%E5%8D%81%E5%88%86%E8%80%81%E8%A1%97)

> We take the privacy of your data seriously so before you visit the Preference Management Center please enter your details below so we can send you an email to confirm your identity. Business email address. Reuben Shcade.

> Shirts I've seen: Saving lives changes lives. The Anti-Shirt Shirt Club. I Run Kubernetes, So I Drink. My Other Shirt is Clean.

> I had reason last week to pour an entire cup of coffee onto my Kensington Orbit trackball mouse. The Kensington Orbit trackball mouse is the best mouse currently available, and the only trackball that I know of that has a scroll wheel. Don’t email me!

> Holo quotes: But I’d rather be in a noisy cab, than one that smells like a goat.

> No more SGCafe.com, that sucks. [Facebook still there](https://www.facebook.com/sgcafe.cosplay/).

> Phrase to add to email filters: "important updates to our legal agreements"

> Original in the R720 was a 1TB, replacement was a 1.2. [I assume this is a Dell R720 ?]

> He fumbled with the virtual notes on his tablet. The lectern upon which it was perched was impossibly small, as if to complicate the presentation of anyone who dared speak at this fine orbital facility. Was it bad design? Malice? Had he overprepared?
> 
> He closed his eyes and breathed deeply. Slowly, calmly, relax. He could do this.
> 
> He opened his eyes. No, that didn’t help.

> Aeroplanes Trains Minecraft Travel Japan Singapore BSD Hololive Anime Books Coffee/cafes Blueberry iMac Jazz Hiking Trail New York food FGO

> Times of day that don't feel right: 16:00. But how often do you think is all the time?
