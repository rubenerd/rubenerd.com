---
title: "Countdown with Keith Olbermann is back"
date: "2022-09-20T09:42:11+10:00"
abstract: "One of my favourite American journalists is podcasting again."
year: "2022"
category: Media
tag:
- keith-olbermann
- news
location: Sydney
---
I noticed last week that [Keith Olbermann's Countdown](https://podcasts.apple.com/au/podcast/countdown-with-keith-olbermann/id1633301179) podcast is back, this time as an independent show. I used to watch the video podcast of his syndicated American news show in the 2000s, and it's a pleasure hearing his voice and sharp commentary again. I still have his official bobble head next to my anime figs.

The [postcast feed is here](https://www.omnycontent.com/d/playlist/e73c998e-6e60-432f-8610-ae210140c5b1/4cab918b-b4e7-4a21-8300-aec3011bf3f1/e7682aaf-443f-41d8-945f-aec3011c5127/podcast.rss), or you should be able to search for the title in your podcast client.

