---
title: "Challenges with mini-ITX builds in 2022"
date: "2022-03-18T09:18:13+11:00"
abstract: "They’re no longer seen as sacrifices, or style over substance, but the segment is still expensive and limited."
thumb: "https://rubenerd.com/files/2022/yt-5mUwDozIcbM@1x.jpg"
year: "2022"
category: Hardware
tag:
- cases
- mini-itx
location: Sydney
---
I love that mini-ITX builds are becoming more popular. It wasn't that long ago when people would scoff at the idea of making small computers, perceiving them to be compromised builds that valued style over substance. Thanks to increasing density and more hardware availability, this isn't the case... as much.

[Hardware Canucks](https://www.youtube.com/watch?v=QxqX86xRqno) did a great video exploring some of the history of the mini-ITX form factor, going back to those original VIA boards. He explored the challenges of size, price, and performance, and called out the disconnect between mini-ITX case and component manufacturers. I agree that while the sales volume will never be there for these small machines, I'm still surprised there aren't more options from the larger companies that *do* have the scale and budgets to do something good.

<p><a href="https://www.youtube.com/watch?v=QxqX86xRqno" title="Play The ITX Market is BROKEN - This is what needs to Change!"><img src="https://rubenerd.com/files/2022/yt-QxqX86xRqno@1x.jpg" srcset="https://rubenerd.com/files/2022/yt-QxqX86xRqno@1x.jpg 1x, https://rubenerd.com/files/2022/yt-QxqX86xRqno@2x.jpg 2x" alt="Play The ITX Market is BROKEN - This is what needs to Change!" style="width:500px;height:281px;" /></a></p>

[Optimum Tech](https://www.youtube.com/watch?v=5mUwDozIcbM) honed in on compatibility in his Intel 12900K build video. I was especially struck by how the boards themselves have evolved over time, with taller components, passive cooling for board power, and stacks for M.2 storage. This has packed more into this smaller size, but complicates cooling options given the limited available clearance. I wouldn't be at all surprised if we start to see [AIO liquid coolers](https://en.wikipedia.org/wiki/Computer_cooling#Liquid_cooling "All-in-one liquid coolers, on Wikipedia") becoming standard or required equipment for some of these boards, given large air cooling stacks will become harder and harder to fit around all the board components.

<p><a href="https://www.youtube.com/watch?v=5mUwDozIcbM" title="Play The 12900K + ITX Problem"><img src="https://rubenerd.com/files/2022/yt-5mUwDozIcbM@1x.jpg" srcset="https://rubenerd.com/files/2022/yt-5mUwDozIcbM@1x.jpg 1x, https://rubenerd.com/files/2022/yt-5mUwDozIcbM@2x.jpg 2x" alt="Play The 12900K + ITX Problem" style="width:500px;height:281px;" /></a></p>

My hope is we start to see more manufacturers take this segment seriously after the huge success and positive press CoolerMaster had with their colourful and fun [NR200P mini-ITX cases](https://www.coolermaster.com/au/en-au/catalog/cases/mini-itx/masterbox-nr200p-color/).
