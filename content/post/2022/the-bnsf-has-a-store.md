---
title: "The BNSF has a store"
date: "2022-03-28T16:55:39+11:00"
abstract: "This is potentially a dangerous discovery"
thumb: "https://rubenerd.com/files/2022/bnsf-sign@1x.jpg"
year: "2022"
category: Travel
tag:
- bnsf
- merchandise
- trains
- train-simulator
location: Sydney
---
My second-favourite railway after the LNER, the Burlington Northern and Santa Fe in the northwest United States, [has an online store](https://bnsfstore.com/) that accepts international railfans. This is potentially a dangerous discovery.

I'm a fan of drinkware, in particular this [orange insulated stainless steel camper mug](https://bnsfstore.com/Product/1516719-BNSF_Insulated_Stainless_Camper_Mug?cat=DRINK) and [classic Burlington Northern mug](https://bnsfstore.com/Product/1445382-BNSF_Historical_BN_Green_Glossy_Mug_11_oz?cat=DRINK). This [power bank](https://bnsfstore.com/Product/1448300-Custom_Locomotive_Power_Bank?cat=GIFT) in the shape of one of their locomotives is both adorable and practical.

But I think I'd have to get [this 40 cm wall-mounted sign](https://bnsfstore.com/Product/1446853-Railroad_Tin_Tacker_Sign?cat=GIFT) for no reason whatsoever. That would look *handsome* in our kitchen.

<p><img src="https://rubenerd.com/files/2022/bnsf-sign@1x.jpg" srcset="https://rubenerd.com/files/2022/bnsf-sign@1x.jpg 1x, https://rubenerd.com/files/2022/bnsf-sign@2x.jpg 2x" alt="Photo of an aluminium BNSF wall-mounted sign with a simplified track motif and the words BNSF RAILWAY." style="width:500px; height:328px;" /></p>
