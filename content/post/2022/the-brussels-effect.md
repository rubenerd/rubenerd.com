---
title: "The Brussels Effect"
date: "2022-07-08T16:28:33+1000"
abstract: "A video by TLDR News EU discusses the concept from a book by Anu Bradford"
year: "2022"
category: Media
tag:
- europe
- politics
- privacy
- video
- youtube
location: Sydney
---
I watched [this video by TLDR News Europe](https://www.youtube.com/watch?v=5rTNBn7aDpI) over lunch, and they discuss an interesting idea Anu Bradford put forward in her 2012 book *The Brussels Effect*:

> The EU is unique in its ability to unilaterally transform global markets, be it through its ability to set the standards in competition policy, food safety or the protection of data privacy. So, in today’s video we will be looking at how the EU rules the world through regulation

It's the latter impact I have most visibility and knowledge of, but it hadn't occurred to me their impacts were just as far reaching in other areas.
