---
title: "Ukraine UN Ambassador Sergiy Kyslytsya"
date: "2022-02-24T20:19:19+11:00"
abstract: "There is no purgatory for war criminals. They go straight to hell, Ambassador."
year: "2022"
category: Thoughts
tag:
- politics
- ukraine
location: Sydney
---
[To his Russian counterpart](https://twitter.com/TODAYonline/status/1496740371073617921)\:

> Relinquish your duties as a chair. Call Putin, call Lavrov, to stop aggression.
>
> And I welcome the decision of some members of the council to meet as soon as possible to consider the necessary decision that would condemn the aggression that you will launch on my people.
>
> There is no purgatory for war criminals. They go straight to hell, Ambassador.
