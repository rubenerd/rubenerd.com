---
title: "My retrocomputer projects, Q1 2022"
date: "2022-02-05T09:53:15+11:00"
abstract: "Commodore, DOS, Mac, and Hi-Fi stuff."
thumb: "https://rubenerd.com/files/2021/commodore-1571@1x.jpg"
year: "2022"
category: Hardware
tag:
- commodore
- commodore-128
- commodore-16
- commodore-plus4
- compaq-contura
- dos
- ibook-g3
- imac
- imac-dv
- mmx-machine
- toshiba-libretto
location: Sydney
---
I haven't done a proper update for a while. At best I get half hour chunks of time these days to tackle these, but I'm *slowly* making progress. I also didn't appreciate how much silly stuff was here until I wrote it all down.


### Commodore stuff

* Fixing 80-column mode in my Commodore 128. I've confirmed my RGBi to VGA converter works, so it must be an issue with the IC or board itself. I got the service manual handy on my iPad Mini now, so I just need a solid afternoon to work through it.

* Recapping my NTSC Commodore 16. She still works, but there's one cap that's starting to look a bit rotund, so I've kept her powered off. I ordered a kit in December, hopefully it arrives soon. I know the Plus/4 has more ports and features, but I might like using the 16 more.

* Finding a home for my Commodore 1541. I have a 1571 disk drive that acts like as a 1541 on my Plus/4 and 16, and I don't have space. She also needs some attention; I think the motor that moves the head is kaput.


### DOS stuff

* Installing another 32 GiB CompactFlash card for my Pentium 1 tower. Turns out that while Windows NT and BeOS can see larger capacities than the BIOS, they're incredibly unstable when running on them. Ironically, DOS/Windows 3.1x and Windows 95 work fine with a smaller partition.

* Fixing the sound on my Toshiba Libretto 70CT. I suspect it's just the 2.5 mm [sic] jack has come loose, which means it should be an easy fix.

* Testing EMM386 in the latest QEMU. It's always been unstable for me, which has limited my ability to write DOS stuff on my modern machines. Thank you, but yes I know about `$other_emulator`.

* Taking stock of all my spare SCSI paraphernalia, now that I've installed everything I need. I think I'll keep a spare ribbon cable and terminator, but the rest can probably be sold or donated.

* Swapping out the dying hard drive in my Compaq Contura Aero 4/25 subnotebook when it arrives. Did I mention I got a tiny monochrome laptop from 1994 with the same specs as our first family computer? That's for another post!


### Mac stuff

* Replace the busted hinge in my iBook G3. Every time I look at the iFixit guide I blanch a bit and put it away. I need to just do it!

* Test why I'm not getting VGA output in my iMac DV. I get the feeling the logic board might have been damaged during a house move. Maybe just a cracked trace?


### Hi-Fi stuff

* Tackling the power supply for my LaserDisc player. Hales has offered to walk me through it because he's a gentleman, need to figure out when I have a proper block of time.

* Disassembling and testing my cassette Walkman to see if I can get auto-reverse working. It plays tapes flawlessly, but it jams when it hits the end of a tape. Belts and gears all look fine.

* Researching a decent pair of powered speakers. Clara and I don't play loud music, and we're thinking of replacing the heavy, hot amplifier with a small input selector and a basic vinyl preamp. Not sure if that counts as sacrilege.

