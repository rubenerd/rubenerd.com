---
title: "Windows 3.1: CD audio MCI driver not installed"
date: "2022-01-30T14:50:49+11:00"
abstract: "Use Drivers in Control Panel to install the right driver first."
year: "2022"
category: Software
tag:
- cds
- music
- mmx-machine
- retrocomputing
- windows-31
location: Sydney
---
Now I know what you're thinking: Who plays CDs anymore? And on a Windows for Workgroups machine!? These are legitimate questions that deserve an answer.

If you launch Media Player or Creative CD, you might get this error:

> CD not inserted into drive or CD Audio MCI driver not installed

To solve:

1. Launch **Control Panel**
2. Open **Drivers**
3. Click **Add**, and select **[MCI] CD Audio**
4. Supply your Windows 3.1x installation disks

If all goes well, you'll get this confirmation notice:

> **Redbook CD Audio Configuration**<br />
> One CDROM drive was detected. Installation is complete

I haven't ever had to do this before. My hunch is that I installed Windows before setting up `SHSUCDX` (an `MSCDEX` replacement). Had that been done before, Windows may have already installed the driver during setup.

