---
title: "Clarke and Dawe, Australia’s energy market"
date: "2022-06-30T13:33:51+10:00"
abstract: "Choice of what? The watts were exactly the same Brian."
year: "2022"
category: Thoughts
tag:
- australia
- economics
- environment
location: Sydney
---
This [video was recorded in 2017](https://www.youtube.com/watch?v=ELaBzj7cn14), and its just as relevant now. Rest in peace, John Clarke.

> **Wal Socket:** There were so many other players coming into the market.
> 
> **Brian Dawe:** More electricity producers?
>
> **Wal:** No, more billing companies Brian. But the key element was *choice*. They were being offered a greater *choice*.
> 
> **Brian:** A choice of... what?
> 
> **Wal:** Choice of watt? The watts were exactly the same Brian. The choice was in who you could buy them from.

