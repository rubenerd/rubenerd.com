---
title: "A dream without a phone"
date: "2022-07-18T08:31:32+10:00"
abstract: "No phone or knowledge where you are."
year: "2022"
category: Thoughts
tag:
- dreams
- japan
location: Sydney
---
I had a dream last night that wasn't so much scary as it was confusing, though I'm sure it tapped into some anxiety of some sort.

I'd gone to a new country with my sister, checked into a hotel she's booked for us, then taken the local metro down to a massive central station building. I turned around, and both my sister and my smartphone were gone.

There I was, in an unfamiliar place, and no idea how to get back. I couldn't ask for directions, because I didn't know the name of the hotel. I couldn't remember which line we'd taken. I had no money to buy a new phone because I'd used Apple Pay. And even if I did, I wouldn't know my sister's new foreign SIM mobile number.

It reminded me of the time Clara and I were trying to find each other in Shibuya. I'd come from AsiaBSDCon, and she was coming from an outer area of Tokyo, and we were trying to figure out an exit number or *something* to meet at. My phone battery was near dead, so I was running the screen at close to 0 brightness while we tried to coordinate. In the end I had to take a photo of the exit and send, then let my phone turn off. She found me eventually.

The dream didn't get that far, but I figured my best chance was to stay put until my sister realised we were apart and came back. While I waited, I saw several large dolphins swimming in the fountain which was rather nice, and someone cosplaying Rem from Re:Zero (or Rem herself) handed me an eggette waffle, before deciding my hat was too big and running into the sky.

If there's a lesson here, it's that smartphones are a hell of a drug. Coming down from them must be even worse.
