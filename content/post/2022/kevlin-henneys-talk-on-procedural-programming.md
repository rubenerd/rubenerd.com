---
title: "Kevlin Henney’s procedural programming talk"
date: "2022-03-16T17:38:20+11:00"
abstract: "Who knew Shakespeare was a developer?"
thumb: "https://rubenerd.com/files/2022/yt-eEBOvqMfPoI@1x.jpg"
year: "2022"
category: Software
tag:
- programming
- video
location: Sydney
---
I've been getting stuck into watching videos about computer history again, from 1980s home computers and core memory, to fundamental concepts about programming languages and system design.

I just finished watching Kevlin’s talk from 2017, which took us through Algol-68 to golang, with as much explanation given for the why as much as the how (everything that's old is new again)! Also, who knew Shakespeare was a developer?

<p><a target="_BLANK" href="https://www.youtube.com/watch?v=eEBOvqMfPoI" title="Play Procedural Programming: It's Back? It Never Went Away"><img src="https://rubenerd.com/files/2022/yt-eEBOvqMfPoI@1x.jpg" srcset="https://rubenerd.com/files/2022/yt-eEBOvqMfPoI@1x.jpg 1x, https://rubenerd.com/files/2022/yt-eEBOvqMfPoI@2x.jpg 2x" alt="Play Procedural Programming: It's Back? It Never Went Away" style="width:500px;height:281px;" /></a></p>

This is one for rewatching on the couch next weekend.
