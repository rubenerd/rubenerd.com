---
title: "Slow Debian 11 installs with IDE to SATA adaptors"
date: "2022-02-18T08:45:58+11:00"
abstract: "Unplugged this old optical drive, and it worked fine."
year: "2022"
category: Software
tag:
- debian
- linux
- presario-rocket
location: Sydney
---
My [graphics card post yesterday](https://rubenerd.com/buying-a-new-graphics-card/) was part of a broader project to rebuild my desktop into [this silly Compaq case](https://rubenerd.com/finally-buying-a-compaq-spaceship/). I decided while I was thinking about it, I'd do a clean install of Debian with the newest version. I'm lucky that Debian with Steam and Proton run almost all the non-native Linux games I care about. We owe these developers a *huge* debt of gratitude!

Back on the Debian front, the installer ran *glacially* slow. FreeBSD 13 installed in less than fifteen minutes on my other boot drive, and I remembered Windows 10 had previously installed quickly as well. Something was a bit weird.

I opted for the text-based interface, but it still took upwards of half an hour to progress beyond each step. After two hours I decided to reboot and check what `dmesg(8)` showed.

During boot I saw a flash of text that mentioned "sda", so I unplugged the FreeBSD SSD and the slim Blu Ray burner drive from the SATA bus. Same issue. I unplugged the *target* SSD, and same issue.

I then realised my decision to build this into a 90s-era Compaq case was fateful one. I wanted to keep as much of the case stock as possible, including the original optical and floppy drives. To make them work on the new motherboard, I bought a FDD to USB header, and employed an old IDE to SATA converter for the CD-ROM. Both worked great on my FreeBSD install.

I unplugged the IDE to SATA converter, and Debian booted as fast as I'd expect.

Having this optical drive disconnected isn't the end of the world; it would have been fun, but I have an alternative drive underneath for all my multimedia CD-ROM sillyness. I might do a bit more digging to see what that IDE to SATA converter uses. The fact FreeBSD boots fine with it suggests there's a Linux driver issue somewhere.
