---
title: "The NCR-80 vintage mechanical keyboard kit"
date: "2022-08-19T07:46:28+1000"
abstract: "This with some two-tone retro keycaps would be awesome."
thumb: "https://rubenerd.com/files/2022/ncr-80@1x.jpg"
year: "2022"
category: Hardware
tag:
- colour
- keyboards
- nostalgia
- retrocomputing
location: Sydney
---
Every time I rationalise why I don't need another mechanical keyboard, I [see something like this](https://en.zfrontier.com/collections/inventory-release-august-2022/products/in-stock-ncr-80-r2-vintage-mechanical-keyboard-kit?variant=39941116166222 "[In Stock] NCR-80 R2 Vintage Mechanical Keyboard Kit")\:

> Reimagination of the classic rebranded Cherry G81-3000 by NCR. NCR-80 is an affordable keyboard kit inspired by iconic vintage keyboards. R2 brings in a cooler shade of gray (from NCR-3007) and a vintage beige for all the retro aesthetics.

This is the case with the included PCB:

<figure><p><img src="https://rubenerd.com/files/2022/ncr-80@1x.jpg" alt="" style="width:500px; height:332px;" srcset="https://rubenerd.com/files/2022/ncr-80@1x.jpg 1x, https://rubenerd.com/files/2022/ncr-80@2x.jpg 2x" /></p></figure>

Most people are probably familiar with NCR for their eponymous cash registers, but they had some beautiful terminals and high-end server hardware back in the day, some of which I remember tinkering with at one of my first jobs.

This case with some dual-tone retro keycaps would be spectacular. I'd have to be careful to match the colour exactly though, or it'd look weird. Maybe I need to check out some completed builds. Any of you made a keyboard in this case?

