---
title: "My favourite USB key ever: The Toshiba PA5003A"
date: "2022-09-14T18:11:38+10:00"
abstract: "They boot anything, and can be read by anything."
thumb: "https://rubenerd.com/files/2022/toshiba-pa5003@2x.jpg"
year: "2022"
category: Hardware
tag:
- reviews
location: Sydney
---
Every engineer, artist, scientist, and medical professional has that *one* tool they've learned to depend on from years of experience. They might not remember where they first picked it up, but it's become so indispensable that they swear by it, often to the point of sounding fanatical. I'll bet you've picked up one or more of these.

This is one of mine. Well, two in fact:

<figure><p><img src="https://rubenerd.com/files/2022/toshiba-pa5003@1x.jpg" alt="View of the keys from the front and back." style="width:316px; height:85px;" srcset="https://rubenerd.com/files/2022/toshiba-pa5003@1x.jpg 1x, https://rubenerd.com/files/2022/toshiba-pa5003@2x.jpg 2x" /></p></figure>

This is the Toshiba PA5003A Mini USB Key. It also comes in a black/yellow housing with 16 and 32 GiB capacities, though the bulk of mine are the 8 GiB white/red model shown.

The transfer speeds on this key are on the slow side, even among other USB 2.0 devices. Their completely plastic housing and tiny size give them a disposable, cheap look. Throw a few of these on your desk, and nobody would give them a second glance.

But of the hundreds of keys I've used since they usurped the CD-R and Zip drive, *none* comes close to being as versatile, dependable, or dang useful!

Everything recognises them. I've used them to boot macOS on modern MacBooks with USB-C adaptors, to ESXi on a Dell R620 server in a data centre at one in the morning, to helping a friend with the diagnostic port on a car. I know that no matter what OS, partition layout, or target device I use it on, these keys will *always* be readable, and *always* bootable. Whatever firmware these specific keys have, it's friggen awesome.

They've also been the most reliable and surprisingly robust keys I've ever used. I've rolled over them with computer chairs, squished them under a blade chassis, and had them rattling around in my IT utility pouch with my assortment of coiled Ethernet cables, serial console adapters, army knives, and other junk. They don't retract or come with caps, but it doesn't matter. They're simple, lightweight, and just work.

I keep a dozen or so of them around with the latest bootable images of BSD, Linux, VMware, and Windows Server that I need for work, alongside live CD images, memtest, and even my absolutely ancient PC DOS 2000 troubleshooting environment. I love that they're the *perfect* dimensions to fit a small label-printed label with the OS, tool, or function they have.

There are increasingly few certainties in IT, so I latch onto whichever ones I find. I don't think they make these keys anymore, and Toshiba have spun off their storage division to Kioxia, but I'll still snap these up when they're on special anywhere.
