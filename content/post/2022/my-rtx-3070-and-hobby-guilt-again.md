---
title: "The RTX 3070, and chasing the shiny"
date: "2022-03-17T12:56:29+11:00"
abstract: "A week after buying my 3070, 3080s are now the same price. It shouldn’t matter."
thumb: "https://rubenerd.com/files/2022/3070-minecraft@1x.jpg"
year: "2022"
category: Hardware
tag:
- cities-skylines
- games
- graphics-cards
- nvidia
- train-simulator
- xplane
location: Sydney
---
I'd spent the better part of a year deciding which GPU to get for my FreeBSD/Linux tower, whether I should wait, and where prices would need to be for me to buy. I did *all the research* and determined the RTX 3060 Ti or 3070 hit the sweet spot for my requirements. Finally I shouted *screw it* into the ether, and hit the buy button with the determination of someone buying an overpriced graphics card.

As I cynically predicted, GPU prices fell *within days* after! You can now buy a 3070 Ti or even a 3080 in Australia for the price I paid for my 3070, and with much better cooling performance than the ITX-friendly [Zotac 3070 Twin Edge](https://www.zotac.com/product/graphics_card/zotac-gaming-geforce-rtx-3070-twin-edge) I needed to fit my case.

This is good news for the industry, video editors, hobbyists, artists, and gamers who until recently have been robbed blind by scalpers and crytobros converting these cards into bullshit and carbon dioxide. *If only it'd happened a week sooner!*

<p><img src="https://rubenerd.com/files/2022/3070-minecraft@1x.jpg" srcset="https://rubenerd.com/files/2022/3070-minecraft@1x.jpg 1x, https://rubenerd.com/files/2022/3070-minecraft@2x.jpg 2x" alt="Photo of one of Clara’s and my smaller towns in Minecraft at night." style="width:500px; height:281px;" /></p>

It shows how weird our brains are... or maybe just mine. Loading [Minecraft](https://www.minecraft.net/) with [Optifine](https://www.optifine.net/home) and EminGTR's [Complementary Shaders](https://www.curseforge.com/minecraft/customization/complementary-shaders) pack at 4K with my new 3070 made my jaw drop. The light effects off the quartz blocks, the glassy waves and soft clouds, the swaying of leaves, the warm glow of lanterns, it was all spectacular. If Clara and I can't travel, at least we can explore our little world we've built together over the last two years; and this slab of platinum opens a beautiful new window into it. Ditto [railways in Montana](https://store.steampowered.com/app/1055338/Train_Simulator_Montana_HiLine_Shelby__Havre_Route_AddOn/?curator_clanid=958135&curator_listid=43438) that I wander around in my BNSF GP38-II in Train Simulator, and my [Cessna 172SP](https://www.x-plane.com/aircraft/cessna-172sp/) in X-Plane. Even Cities Skylines looks gorgeous.

Knowing cards are cheaper now doesn't change or negate those experiences. It's not like a 3070 starts performing like a 6500XT at the sight of 3080s encroaching on its price point. It feels like an order of magnitude better than the 970 and 5500M I had before.

There is, and will always be, newer and shinier kit. Admittedly the time between realising it for me was a week, not a year or more, but does that matter? Or perhaps more importantly, *should* it matter?
