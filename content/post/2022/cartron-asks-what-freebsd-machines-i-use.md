---
title: "Cartron asks what FreeBSD machines I use"
date: "2022-09-23T08:51:30+10:00"
abstract: ""
thumb: ""
year: "2022"
category: Thoughts
tag:
-
location: Sydney
---
In response to my [Windows 11 game post](https://rubenerd.com/running-windows-11-for-games/) where I mentioned dual-booting into FreeBSD most of the time, the imitable [@Cartron asked](https://twitter.com/cartron/status/1572893545312419840) what machines I use. It's been a while since I've done a post like this.

These are my FreeBSD machines at the time of writing:

* **Fauna, my main FreeBSD tower**. She's a budget AMD Ryzen machine I built earlier this year for games, then realised she's also a great workstation! CoolerMaster NR200P Mini-ITX case, Asus ROG Strix X570-I board, Ryzen 5600X CPU, 32 GiB 3600 MHz (using A-XMP), a Zotac Twin Edge RTX 3070, and a Dell 4K 27-inch monitor.

* **Holo, my FreeBSD server.** Built from used parts to test Xen, before switching to bhyve and jails. Runs Netatalk, Plex, Minecraft, musikcube, and DNS filtering. Supermicro X11SAE-M board, Intel Xeon E3-1275 v6, 16 GiB PC4-19200U ECC memory, and  bunch of mirrored ZFS hard drives.

* **Lum, the Panasonic Let's Note CF-RZ6.** My on-call laptop I bought during AsiaBSDCon 2019. Japanese Panasonic laptops are amazing; she has a higher-res screen than most ThinkPads twice her weight and size, and more ports than my work Macs. I wrote a page on the [FreeBSD wiki](https://wiki.freebsd.org/Laptops/Panasonic_Lets_Note_CF-RZ6) about.

* **Sasara, the cloud VM.** She acts as my live testbed for FreeBSD template releases at work. She runs my blog here, all of my partner Clara's stuff, and an install of Nextcloud, among other things.

And the ones I have on the horizon:

* **A VisionFive 2 RISC-V dev board**, which I [backed on Kickstarter](https://rubenerd.com/backing-the-visionfive-2-risc-v-dev-board/). They claim to have Linux support out of the gate, but I want to get FreeBSD booting on it.

* **A Raspberry Pi 400,** for testing FreeBSD on ARM, and for running some classic games in DOSBox and ScummVM!

I also have a few physical and virtual machines running NetBSD, but that's for another post.

Details about these machines are also on my [Omake outline](https://rubenerd.com/omake.opml) under *Gear*.
