---
title: "How do you create zpools? Via @michaeldexter"
date: "2022-08-15T10:14:04+1000"
abstract: "I use a combination of scripts and orchestration tools."
year: "2022"
category: Software
tag:
- bsd
- freebsd
- scripts
- zfs
location: Sydney
---
[Michael](https://callfortesting.org/) asked in an [online poll](https://twitter.com/michaeldexter/status/1558954661389881344?s=987654321) whether we create zpools:

* Artisanally by hand
* With scripts
* With a GUI
* After cursing and wasting another morning on ext4

For personal and family stuff, I use the (definitely not nasty) script below. I run ZFS on full drives which I've first configured with gpart(8) with a freebsd-zfs partition and a label (though I don't think that's strictly necessary anymore?). Normalisation is a [big deal](https://rubenerd.com/forgetting-to-set-utf-normalisation-on-a-zfs-pool/) for us.

	#!/bin/sh
		
	zpool create -o ashift=12 \
		-O atime=off \
		-O exec=off \
		-O compression=lz4 \
		-O utf8only=on \
		-O normalization=formD \
		-O casesensitivity=insensitive \
		mirror "/dev/gpt/$1" "/dev/gpt/$2"

For work pools, I use orchestration scripts that the client can grok, such as [Saltstack](https://docs.saltproject.io/en/latest/ref/modules/all/salt.modules.zpool.html). Same goes for zfs datasets and, unfortunately, ext4.
