---
title: "Confusing toll-free numbers for 2FA codes"
date: "2022-05-25T16:04:51+1000"
abstract: "Your security code is 012 345. If this was not you, contact support at 987 654."
year: "2022"
category: Internet
tag:
- accessibility
- security
location: Sydney
---
In 2020 I talked about how [chunking two-factor authentication codes](https://rubenerd.com/2fa-auth-codes-need-to-be-chunked-better/) made them easier to remember and type. I'm happy to see more sites doing this.

But now we face a new problem! Australian toll-free numbers are six digits, so I get messages like this:

> Your security code is 012 345. If this was not you, contact support at 987 654.

Guess which number I memorise and type each time.

Humans are squishy, silly creatures. We don't read words, we see patterns. I suspect the same phenomena is happening here. In a hurry to log in, we see a string of numbers and we enter them.

The real answer is diligence. Actually, the real *real* answer is better auth. But in the meantime, I wonder what more we can do to make these obvious. Maybe an emoji telephone before the number, or capitalised text for "security code"? Do SMSs get interpreted as phone numbers with underlines if the word "tel:" is added? Should they?
