---
title: "Don’t invest in cryptocurrencies"
date: "2022-02-04T14:49:24+11:00"
abstract: "Friends have asked me about. The answer is no."
year: "2022"
category: Internet
tag:
- bitcoin
- cryptocurrencies
- scams
location: Sydney
---
As "the tech guy" among his family and friends, I'm regularly asked for my advise on what computers, phones, and other electronics to buy. I take the responsibility seriously. These devices can dip into the thousands of dollars, and I wouldn't want to saddle someone with an expensive mistake that wasn't tailored to their requirements.

I've been asked by people if they should invest in crypto-“currencies” and NFTs.

No.
