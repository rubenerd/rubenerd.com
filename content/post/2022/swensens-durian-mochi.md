---
title: "Swensens durian mochi"
date: "2022-08-01T14:23:53+10:00"
abstract: "I'm not sure I’ve wanted to try something more than this!"
thumb: "https://rubenerd.com/files/2022/swensens-mochi.jpg"
year: "2022"
category: Thoughts
tag:
- food
- singapore
location: Sydney
---
Having lived in a bunch of places means I still get newsletters and promos from stuff that's now beyond my grasp. I can't bear to unsubscribe to any of them; they're fun little memories.

I noticed Singaporean Swensens just sent this. I'm not sure I've ever wanted to try something more than this! 

<figure><p><img src="https://rubenerd.com/files/2022/swensens-mochi.jpg" alt="Advertisement for Swensens durian mochi. Reads: Premium Mao Shan Wang with Sea Salt Gula Melaka. The king of kings, made with 100% highest grade Mao Shan Wang puree with luxurious Sea Salt Gula Melaka. Durian King: Delve into a rich, creamy, fragrant harmony of the exquisite Durian King." style="width:417px; height:590px;" /></p></figure>
