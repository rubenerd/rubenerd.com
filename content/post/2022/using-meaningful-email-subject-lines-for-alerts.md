---
title: "Using meaningful email subject lines for alerts"
date: "2022-06-04T10:08:02+10:00"
abstract: "“Provider service $SERVICE-NAME (or one of its comp...)”"
year: "2022"
category: Internet
tag:
- email
location: Sydney
---
Email is one of those things that we've been using for decades, yet companies that exist to notify us of issues continue to get the basics wrong. I'm not judging them for this; I still burn toast despite using toasters for my entire adult life. But like people who see the mess on my vintage computer table, or my fashion choices, it's an area for improvement.

Here's the subject line for an email sent by an upstream network provider:

> Provider service $SERVICE-NAME (or one of its comp...)

What do you make of that?

I saw that flash up on my work phone while I was on-call, so I swiped to open it. It was a good thing I did; the rest of the gigantic subject line notified us of reduced redundancy on a circuit. That's something we need to know.

Alert emails these days will almost certainly be read on a phone first, so it's important to have a succinct subject that doesn't throw critical details to the *Beast of Truncation*. Here are some examples of what I'd rather see:

> (NOTICE/ALERT/CRITICAL/OFFLINE): $SERVICE

I'm sure it also strikes many of you as ironic that someone as verbose and nonsensical as me would chide someone for not being succinct in an email subject line. To which I have two retorts: first, I'm not a network operator imparting important and critical information; and second, I want some toast.
