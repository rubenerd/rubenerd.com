---
title: "Say You Love Me, Fleetwood Mac"
date: "2022-03-08T09:18:09+11:00"
abstract: "I think we deserve a toe-tapping classic today."
thumb: "https://rubenerd.com/files/2022/yt-4zLQ4ukqMec@1x.jpg"
year: "2022"
category: Media
tag:
- music
- music-monday
location: Sydney
---
Whoops, missed [Music Monday](https://rubenerd.com/tag/music-monday)! That's so unlike me. I think we deserve a toe-tapping classic from one of my all-time favourite bands. That was several hyphens in the same sentence.

<p><a href="https://www.youtube.com/watch?v=4zLQ4ukqMec" title="Play Say You Love Me (2002 Remaster)"><img src="https://rubenerd.com/files/2022/yt-4zLQ4ukqMec@1x.jpg" srcset="https://rubenerd.com/files/2022/yt-4zLQ4ukqMec@1x.jpg 1x, https://rubenerd.com/files/2022/yt-4zLQ4ukqMec@2x.jpg 2x" alt="Play Say You Love Me (2002 Remaster)" style="width:500px;height:281px;" /></a></p>
