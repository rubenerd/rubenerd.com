---
title: "Alan Baxter on writing"
date: "2022-02-06T09:49:04+11:00"
abstract: "What if I told you the ONLY writing rule is this: You must write."
year: "2022"
category: Thoughts
tag:
- blogging
- writing
location: Sydney
---
The [Australian author](https://twitter.com/AlanBaxter/status/1490094414688968704) extraordinaire:

> What if I told you the ONLY writing rule is this: You must write.
>
> Everything else is advice. Everything.

This goes for blogging too, or probably any creative outlet. You don't get good at it yak shaving, like tinkering with content management systems. Believe me on the latter, I would know!

I'm pretty sure [Michael Warren Lucas](https://mwl.io/) has voiced similar sentiment.
