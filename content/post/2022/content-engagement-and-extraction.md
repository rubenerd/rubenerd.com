---
title: "Content, engagement, and extraction"
date: "2022-10-04T08:54:52+11:00"
abstract: "Doc Searls describes how dehuminising so much modern Internet tech is."
year: "2022"
category: Internet
tag:
- advertising
- doc-searls
- ethics
location: Sydney
---
I'm iffy on the language used to describe people, our creativity, and our interactions online. Everything is measured by *engagement*, for the production of *content*, and *extracting value*. And it's hollowing out the web from the inside.

Doc Searls expanded on this last month, in a blog post explaining why [attention is not a commodity](http://blogs.harvard.edu/doc/2022/09/18/attention-is-not-a-commodity/)\:

> My point here is that reducing humans to beings who are only attentive—and passively so—is radically dehumanizing, and it is important to call that out. It’s the same reductionism we get with the word “consumers,” which Jerry Michalski calls “gullets with wallets and eyeballs”: creatures with infinite appetites for everything, constantly swimming upstream through a sea of “content.” (That’s another word that insults the infinite variety of goods it represents.)

He also touches on why so many of us are jaded on much of the innovation happening in the industry right now:

> None of us want our attention extracted, processed, monetized, captured, managed, controlled, held in custody, locked in, or subjected to any of the other verb forms that the advertising world uses without cringing. That the “attention economy” produces $trillions does not mean we want to be part of it, that we like it, or that we wish for it to persist, even though we participate in it.

I've [noticed this on The Bird Site](https://twitter.com/Rubenerd/status/1576374463041454081), with an explosion in the number of accounts designed expressly to solicit comments, grow follower counts, and target people with ads. Sites like Reddit similarly have to deal with bots that repost popular content in order to farm "karma", then flip the account for advertising or propaganda. Decentralised and federated social networks are largely spared from this right now, but there's no reason to believe they wouldn't also fall victim if enough people moved across. Followers and karma == money.

None of this is new; it just feels more acute now.

Doc suggests we get around this by building tools for individuals, and services that leverage those tools. That would be a start, but I worry that we face an uphill battle not just due to the network effect, but because we'd be pushing  against billions of dollars in an economy that treats minds like an amorphous sea to be pumped by oil rigs.

Doc claimed ad tech is the [biggest boycott in history](http://blogs.harvard.edu/doc/2015/09/28/beyond-ad-blocking-the-biggest-boycott-in-human-history/) back in 2015, which [Cory Doctorow also expanded on](https://boingboing.net/2019/07/25/largest-boycott-in-history.html) a few years later. Perhaps that's the key: we need to make this invasive tech and spam as unappealing a business proposition as possible. We also need to stop measuring success based on these metrics.
