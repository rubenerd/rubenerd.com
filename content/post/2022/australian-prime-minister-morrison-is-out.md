---
title: "Australian prime minister Morrison is out"
date: "2022-05-22T15:40:24+1000"
abstract: "I’m still in a state of disbelief. The relief is "
year: "2022"
category: Thoughts
tag:
- australia
- politics
location: Sydney
---
I'm still in a state of disbelief. Australia's populist leader, and his conservative coalition government, have been voted out of office. I'm choking up the same way I did when my American friends voted out Trump.

Labor aren't half as progressive as their proponents postulate, and their record on digital rights is suspect. But they have to be damn sight better than a leader who [sees a crying, burning country](https://rubenerd.com/bushfire-news-is-too-much/) and says "I don't hold a hose", then fucks off to Hawai‘i. It's been a decade of wasted opportunities, scandals, ethical failures, financial irresponsibility, and international gaffes.

On the left we got the biggest Greens result ever, and my right-of-centre friends delivered us an unprecedented slew of new independents who stood for responsible climate action. Prime minister Anthony Albanese leading a minority government with these new MPs, or tempered by a robust Senate crossbench is about the best I could have hoped for.

UK, you're up next.
