---
title: "Number pads"
date: "2022-07-22T10:33:32+10:00"
abstract: "Do I get a USB ThinkPad numpad, or go all out and build something pointless?"
year: "2022"
category: Hardware
tag:
- accounting
- keyboards
location: Sydney
---
I've been getting more into personal accounting stuff, and extending how I track income, expenses, and virtual budget envelopes. I love spreadsheets more than I'm probably supposed to, but I'm also working on an sqlite3 system that will let me ingest CSVs and build reports for envelope forecasts and tax.

There are plenty of open source software packages, but I like building a data model for exactly how my mind works. This, coupled with the joy I feel seeing reconciled numbers match up, perhaps says more about how my mind works than I care to admit.

I belabour all of this on account (HAH!) of being insufferable, and because I've been looking at number pads again. Turns out, IBM was onto something when PC keyboards came bolted with number pads. Typing strings of numbers vertically is so much easier and faster than using the top row of a regular keyboard.

**HUH!**

The irony is I have exactly two keyboards with this numpad arrangement: an IBM Model M keyboard that's too loud to use in the confindes of a tiny apartment, and a 1985 Commodore 128! And while the idea of using a homebrew Commodore BASIC program to track expenses tickles me in all the right ways, I'd like to also do it while at coffee shops to supplement the laptop keyboard, or at my main desk.

A detached number pad appeals to me because I can move it out of the way when not in use, and being portable I can use it with my laptop at coffee shops. For when I'm running my homebrew Commodore BASIC program in VICE, obviously.

I'm of two minds which to get. [Lenovo make a numpad](https://www.lenovo.com/au/en/p/accessories-and-software/keyboards-and-mice/numeric-keyboards/4y40r38905) with ThinkPad switches, which somehow feels right given the computer line is all but mandatory equipment for business types. It's not as cool or retro as the original with non-island keys, but I'm sure it'd suffice.

The alternative is building an over-engineered mechanical numpad, like the [KBDPad Mark II](https://kbdfans.com/products/gb-kbdpad-markii-mechanical-keyboard-kit). This one makes me excited just thinking about: a mechanical keyboard I build myself, that I use for amateur accounting? Yes please!

There's no accounting for some people's tastes. But I'd be keen to hear any of your advice.
