---
title: "Free university course videos"
date: "2022-02-24T09:02:27+11:00"
abstract: "These are great for having in the background when doing menial or rote work during the day."
year: "2022"
category: Media
tag:
- education
- video
- youtube
location: Sydney
---
When I'm not in the mood for music, I love having educational videos running in the background when I'm doing less taxing work. They keep me engaged, and I've learned tons from osmosis.

Many universities have online courses on YouTube you can subscribe to and watch. [MIT OpenCourseWare](https://www.youtube.com/channel/UCEBb1b_L6zDS3xTUrIALZOw) remains my favourite, but I've since found a few more:

* [OpenLearn](https://www.youtube.com/c/OUlearn/videos)
* [Oxford](https://www.youtube.com/c/oxforduniversity/videos)
* [Stanford](https://www.youtube.com/c/stanford/videos)
* [Yale](https://www.youtube.com/user/YaleCourses/videos)

[UCLA](https://www.youtube.com/user/UCLACourses/videos) gets an honourable mention for their back catalogue, but they haven't added anything for a while. I hope to see some more soon.

Let me know if you have any other ones! I'm especially interested in more global institutions.
