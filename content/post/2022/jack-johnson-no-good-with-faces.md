---
title: "Music Monday: Jack Johnson, No Good with Faces"
date: "2022-01-31T08:54:12+11:00"
abstract: "No good with faces and I'm bad with names"
thumb: "https://rubenerd.com/files/2022/yt-vJ0TnCKDFgI@1x.jpg"
year: "2022"
category: Media
tag:
- folk-music
- jack-johnson
- music
location: Sydney
---
Every Monday without fail, except when I fail, I regale readers with a musical interlude titled with a name I stole from our university's anime club blog: [Music Monday](https://rubenerd.com/tag/music-monday/).

Today we go back to 2010 and one of my favourite Jack Johnson songs. *No good with faces and I'm bad with names* is among the most relatable lyrics I've ever heard.

<p><a target="_BLANK" href="https://www.youtube.com/watch?v=4ROAE9tPPro" title="Play No Good With Faces"><img src="https://rubenerd.com/files/2022/yt-vJ0TnCKDFgI@1x.jpg" srcset="https://rubenerd.com/files/2022/yt-vJ0TnCKDFgI@1x.jpg 1x, https://rubenerd.com/files/2022/yt-vJ0TnCKDFgI@2x.jpg 2x" alt="Play No Good With Faces" style="width:500px;height:281px;" /></a></p>
