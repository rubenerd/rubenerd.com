---
title: "Resource use of FreeBSD desktop environments"
date: "2022-07-25T15:47:36+10:00"
abstract: "Comparing MATE, Xfce, Gnome 3, and KDE Plasma."
year: "2022"
category: Software
tag:
- bsd
- freebsd
- gnome
- kde
- xfce
location: Sydney
---
Prolific FreeBSD writer and excellent person Vermaden recently performed a [resource use comparison](https://vermaden.wordpress.com/2022/07/12/desktop-environments-resource-usage-comparison/) between various popular desktop environments.

Recent conventional wisdom suggests that the bigger DEs have been improving their resource use, with some like Plasma getting memory use closer to the traditional lightweight stalwarts like Xfce. This is due to the efforts of the desktop developers, and improvements in the underlying toolkits like Qt.

<p><img src="https://rubenerd.com/files/2020/beastie@2x.png" alt="Beastie" style="float:right; margin:0 0 1em 2em; width:192px; height:208px;" /></p>

Vermaden's experience installing them from FreeBSD ports closer aligns with my expectations and past experience. MATE (the fork of classic Gnome 2) comes out on top for resource use (or lack thereof), followed by Xfce, Gnome 3, and KDE Plasma.

It's an interesting exercise, and I'm all for encouraging efficiency. The industry has a nasty habit of soaking up improvements for little technical or usability benefit, either in the interest of saving money in the short term, or adding bullet points to a marketing deck. Or worse, the new features or design *hinder* usability.

But it's worth remembering that resource use is only one metric; a computer that's turned off isn't using *anything!* I imagine a chart with features plotted against memory: provided both scale linearly, it's easy to pick something that hits the sweet spot for features you use, your available resources, and how much time and inclination you have to spend tinkering with a system.

There's also no shame in wanting something pretty, especially given how much some of us spend in front of these machines in our work and personal lives. Vermaden didn't do this, but I see other people in the open source community poke fun at those who want something nice as well as functional. Technical specifications make something possible, but art makes life worth living. The fact we all have opinions about what that is, and what it should look like, is why we call it subjective.

It might also largely be a moot point thesedays. My low-end Japanese Panasonic Let's Note laptop I use as my on-call machine has 8 GiB of memory, which is more than enough even for a full Plasma environment, and it still feels as fast as my Mac. And with the increasing complexity in modern browsers, and the existence of bloated Electron applications, the DE is becoming a smaller part of the equation.
