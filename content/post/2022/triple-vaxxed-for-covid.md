---
title: "I’m triple-vaxxed for Covid!"
date: "2022-02-01T12:16:48+11:00"
abstract: "A reminder that medical science is amazing."
year: "2022"
category: Thoughts
tag:
- covid-19
- health
location: Sydney
---
Last Saturday I mentioned that CD-ROMs made me feel like I was [living in the future as a kid](https://rubenerd.com/retrocomputing-is-more-than-games/). Today, it's the fact that I was able to have another vaccination for a disease we only knew about a few years ago.

Medical science is amazing.
