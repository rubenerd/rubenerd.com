---
title: "If you want to understand reality, don’t be pedantic"
date: "2022-09-21T10:16:59+10:00"
abstract: "Quotes from Upper_Case and Jim Kloss gave some useful context to why I don’t read social news sites anymore."
year: "2022"
category: Internet
tag:
- feedback
- hacker-news
- pedantry
location: Sydney
---
[Upper_Case wrote this piece of advice](https://interpersonal.stackexchange.com/posts/17645/revisions) on the Interpersonal Stack Exchange, and it perfectly encapsulates an issue I see on a recurring basis online:

> Communicating well is not a matter of stating true facts (though that is important). It is about expressing things that are accurate in a way that the other party will understand. It is common, in my personal experience, for technically-minded people (like engineers) to only focus on the accuracy of their statements.

I've shared this on social media before and, I kid you not, was hounded by people questioning its accuracy. 

My good friend [Jim Kloss shared an article](https://twitter.com/JKloss4/status/1572019205049880579) from [Farnam Street](https://fs.blog/language-not-just-code/) yesterday that expanded on this:

> We communicate more than the definitions of our words would suggest. (Steven Pinker argues language itself as a DNA-level instinct.) And we decode more than the words spoken to us. This is inferential communication, and it means that we understand not only the words spoken, but the context in which they are spoken. Contrary to the languages of other animals, which are decidedly less ambiguous, human language requires a lot of subjective interpretation.

Engineers like you and I like to think we live in a world of certainty, but human prose isn’t programming syntax.

This is another reason I [no longer read sites like Hacker News](https://rubenerd.com/why-i-tend-to-avoid-reading-social-news-comments-now/). Once you recognise this pattern of pedantry and obtuseness, it's impossible not to see it in many of the comments, even if the authors aren't aware they're doing it. I'm sure I've done it myself too. It's a high school debate club, not a genuine attempt at understanding.

You also realise how routinely this happens when you're on the receiving end of it. I can't tell you the number of times a post of mine appears on a social news site, and I get drive-by comments predicated on baseless assumptions that distort the meaning of what I wrote. My favourite is when people assume I'm American because I write in English. Don't get me wrong, I have lots of American friends and have loved my US travels, but you can pry my redundant vowels and SI units from my... frustrated hands.

That's the ultimate irony here. Attempting to score Internet points in pedantry blinds one to broader reality, and in doing so results in a larger error. It's much easier to catch and handle that exception early on, rather than have it snowball into a mental stack trace so long you need `less(1)`.
