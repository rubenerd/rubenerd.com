---
title: "26th of January"
date: "2022-01-26T17:52:10+11:00"
abstract: "♡"
thumb: "https://rubenerd.com/files/2022/Australian_Aboriginal_Flag.png"
year: "2022"
category: Thoughts
tag:
- australia
- human-rights
location: Sydney
---
<p><img src="https://rubenerd.com/files/2022/Australian_Aboriginal_Flag.svg" alt="Australian Aboriginal Flag" style="width:500px" /></p>
