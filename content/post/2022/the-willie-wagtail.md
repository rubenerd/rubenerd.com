---
title: "The Willie Wagtail"
date: "2022-07-01T13:33:21+10:00"
abstract: "We haven’t had a bird of the week for a while."
thumb: "https://rubenerd.com/files/2022/willie-wagtail@1x.jpg"
year: "2022"
category: Media
tag:
- birds
location: Sydney
---
Any time I see a [cute bird](https://rubenerd.com/tag/birds/) as Wikipedia's featured picture, I have to share it. This is a Willie wagtail, [taken by JJ Harrison](https://commons.wikimedia.org/wiki/File:Rhipidura_leucophrys_-_Glen_Davis.jpg) in Lithgow, about a hundred kilometres north-west from where I am in Sydney. It probably flew away by now.

<figure><p><img src="https://rubenerd.com/files/2022/willie-wagtail@1x.jpg" alt="A photo of a willie wagtail, taken by JJ Harrison." style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2022/willie-wagtail@1x.jpg 1x, https://rubenerd.com/files/2022/willie-wagtail@2x.jpg 2x" /></p></figure>

From the [bird's article](https://en.wikipedia.org/wiki/Construction_aggregate "Wikipedia: Construction aggregate"):

> Construction aggregate, or simply aggregate, is a broad category of coarse- to medium-grained particulate material used in construction, including sand, gravel, crushed stone, slag, recycled concrete and geosynthetic aggregates. Aggregates are the most mined materials in the world.

That's clearly the wrong article. [Let's try again](https://en.wikipedia.org/wiki/Willie_wagtail)\:

> The willy (or willie) wagtail (Rhipidura leucophrys) is a passerine bird native to Australia, New Guinea, the Solomon Islands, the Bismarck Archipelago, and Eastern Indonesia. It is a common and familiar bird throughout much of its range, living in most habitats apart from thick forest. Measuring 19–21.5 cm (7+1⁄2–8+1⁄2 in) in length, the willie wagtail is contrastingly coloured with almost entirely black upperparts and white underparts; the male and female have similar plumage.

