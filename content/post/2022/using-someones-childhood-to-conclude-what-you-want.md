---
title: "Using someone’s childhood to conclude anything"
date: "2022-02-13T15:10:02+10:00"
abstract: "A supportive upbringing is used to explain compassion, and entitlement?"
year: "2022"
category: Thoughts
tag:
- psychology
location: Sydney
---
I was reading a thread on social media (please write blog posts instead) that attempted to deconstruct a politician's stance on a certain issue.

The writer reached back to the politician's childhood, as every historical documentary and analysis does, and discovered the person's torturous childhood relationship with their father, and the emotional distance they felt from their mother. Their conclusion, as with all hindsight, was that it was a contributing factor to the politician's xenophobia and Brexit worldview.

This superficial, groundless pop psychology is rife, precisely because you can infer anything you want and have it fit your narrative.

Let's look at some examples. I've seen people's comfortable, supportive upbringing used to explain why they turned out:

* compassionate, kind, and honest, because their parents were to them

* entitled, because they were used to getting what they want

* can't handle real-world pressure, because they were shielded

On the flipside, someone's rough upbringing is used to explain why they're:

* xenophobic, vindictive, or cold, because their parents were to them

* compassionate, kind, and honest, because they have lived experience with how a nasty environment feels

* emotionally detached, or emotionally aware

Take *anyone's* circumstances, and you can explain it with their past. Two people had the same upbringing and did completely different things? Not a problem!

I can't unsee this now each time I read a news article, or read hot take on social media. Unless the person is a psychologist with an intimate knowledge of the person's circumstances (and if so, wouldn't that be private?), I wouldn't take what they said as anything meaningful. Hey, like this blog!
