---
title: "There are still mechanical turks everywhere"
date: "2022-03-25T13:30:44+11:00"
abstract: "Things that appear automated... aren’t!"
year: "2022"
category: Software
tag:
- automation
location: Sydney
---
You know how Arthur C. Clarke said that any sufficiently advanced technology is indistinguishable from magic? I'm staring to think we assume there's more magic going on in the world than there really is.

But let's start with the title. You've probably heard the phrase *Mechanical Turk* before, but did you know it was a real device? As in, it physically existed, though its workings were a glorified magic trick. [Wikipedia summarises](https://en.wikipedia.org/wiki/Mechanical_Turk)\:

> The Turk, also known as the Mechanical Turk or Automaton Chess Player (German: Schachtürke, "chess Turk"; Hungarian: A Török), was a fake chess-playing machine constructed in the late 18th century. From 1770 until its destruction by fire in 1854 it was exhibited by various owners as an automaton, though it was eventually revealed to be an elaborate hoax.

This is why it's such a powerful metaphor. A Mechanical Turk is a physical system that appears *superficially* automated, but its operation requires active human involvement. Mechanical processes gave way to electrical relays and switches, then to the electronics upon which you're reading this meandering post now, but the principle remains the same.

It's always surprising, and a bit disarming, when you realise the extent of that human input in something. It's like the cloak has come off, and you're seeing it for what it really is. Doing so can challenge a *lot* of assumptions, especially if its outside your comfort zone or area of expertise.

I always talk about IT here, as if it's where I work! But this extends even beyond databases, load balancers, application servers, and networks. Tom Nicolas explained one such example [in his rebuttal](https://youtu.be/CM0aohBfUTc?t=1012) of Veritasium's self-driving car video, where autopilot landing was made to appear more automated than it was:

> **Derek:** The pilots didn't [land the plane]. It was a Cat III Autoland procedure: the plane just came in and landed itself essentially.
>
> **Tom:** &hellip; Autoland involves a lot of human input. There are several videos on YouTube of pilots flying the exact same model of plane &hellip; and watching them, one finds it to be an *incredibly* involved process. They're not just monitoring the landing either; pilots are actively engaged in adjusting the speed of the aircraft, the flaps, the landing gear, and much more.

Tom indirectly touches on two points here, there's proactive (monitoring) and reactive (live adjustments) human input in automated systems. The more you look into this, the more you realise *every single industry* has it.

Distant Signal's [excellent video about railway crossing signals](https://www.youtube.com/watch?v=4qdti3atxpw) show that while circuits themselves are bridged by the automated movement of trains, humans are still involved in monitoring certain intersections, and they're predicated on trains operating at the correct and expected speeds. A simple switch circuit will not lower the boom gates in time for a speeding train, for example.

Behind every automated system, there are real people on-call to jump in and take over or alert people in the event of something going awry. This could be triggered by a monitoring system (ideally), or from customers complaining they can't access something! And this doesn't even account for ongoing maintenance and upkeep, for which even more humans are required.

Chances are, something you think is entirely automated, isn't.
