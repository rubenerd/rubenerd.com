---
title: "The @vermaden, @BasementTrix on FreeBSD not being a native PC OS"
date: "2022-06-04T08:41:35+10:00"
abstract: "Linux is native to Finland!"
thumb: "https://rubenerd.com/files/2020/beastie@2x.png"
year: "2022"
category: Software
tag:
- bsd
- feedback
- freebsd
location: Sydney
---
I don't monitor this site's [Twitter feed](https://twitter.com/Rubenerd_Blog), but I saw a few retweets from people I follow about my recent critique about [FreeBSD not being PC-native](https://rubenerd.com/linux-is-native-to-the-pc-freebsd-isnt/). I loved these!

[Vermaden](https://twitter.com/vermaden/status/1532326605028265988)\:

> You may as well say that #Linux is native to #Finland so it works better there - as well as #BSD is native to #USA so it works better there :)

[And Trix Farrar](https://twitter.com/BasementTrix/status/1532339320216072193)\:

> Linux is for people that want their PC to run *NIX. FreeBSD is for people that want *NIX to run on their PC.
