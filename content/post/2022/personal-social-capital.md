---
title: "The limits of personal social capital"
date: "2022-08-03T09:44:33+1000"
abstract: "I want to spend time on people I care about."
year: "2022"
category: Thoughts
tag:
- introversion
- personal
- sociology
location: Sydney
---
I like to consider myself a high-functioning introvert. I do a day job and volunteer some of my evenings, much of which involves talking to people in the real world. Many of these conversations I enjoy, especially when I get to hear how people build systems and processes, and what advise and documentation I can give or build.

The corollary of "if it's not documented, it doesn't exist" means I get to bring things into existence! But I digress.

Despite this, there's a finite amount of social capital I can spend on such things a day before, as Clara puts it, I retreat into my shell.

We all have this to an extent, even those who identify as extroverts. The most outgoing person might feign being exited about joining two dozen calls a day, but there's a reason why corporate retreats and onsens exist. 

Realising one has limited personal social capital does help to frame priorities. There's a real opportunity cost to every nonsense conversation or person, especially those who don't act in good faith. I want to spend this time on people I care about.
