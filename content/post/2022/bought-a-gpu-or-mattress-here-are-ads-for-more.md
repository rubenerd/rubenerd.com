---
title: "Bought a GPU or mattress? Here are ads for more!"
date: "2022-03-07T19:14:12+11:00"
abstract: "This is the best behavioural scientists can come up with?"
year: "2022"
category: Internet
tag:
- shopping
location: Sydney
---
I bought a new graphics card recently, so naturally the retailer that sold it to me keeps suggesting new graphics cards for me. Unless you're a millionaire or using them professionally, nobody is going out and buying another graphics card having just bought one. In the words of Quark from *Star Trek Deep Space Nine*, that's some gold-pressed latinum right there.

It reminds me of the time I bought a mattress, and proceeded to be told about mattresses. Because the thing I really want after buying a mattress is... a mattress.

I fear our dystopian, data-mined future. *But*, if this is all that those brilliant marketing and behavioural scientists can come up with, at least we're out of the woods for now.

*Update:* A friend suggested that with all my privacy and security extensions, these retailers don't have as good or complete a profile of me as they may otherwise do, and so have to rely on fewer metrics. That could be true, but surely even their own data would demonstrate those who buy mattresses don't immediately buy another one.
