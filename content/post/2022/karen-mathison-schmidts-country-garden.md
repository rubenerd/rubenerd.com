---
title: "Karen Mathison Schmidt’s Country Garden"
date: "2022-05-23T08:59:40+1000"
abstract: "Happy to see one of my favourite contemporary artists featured :’)"
thumb: "https://rubenerd.com/files/2022/FTX60QTWUAEqebl@1x.jpg"
year: "2022"
category: Media
tag:
- art
- review
location: Sydney
---
Today [@fraveris shared](https://twitter.com/fraveris/status/1528403435955044353) one of my favourite contemporary artists! Here's her *Country Garden*:

<figure><p><img src="https://rubenerd.com/files/2022/FTX60QTWUAEqebl@1x.jpg" alt="Photo of Country Garden." style="width:500px; height:384px;" srcset="https://rubenerd.com/files/2022/FTX60QTWUAEqebl@1x.jpg 1x, https://rubenerd.com/files/2022/FTX60QTWUAEqebl@2x.jpg 2x" /></p></figure>

Her collection is available for purchase on paper and canvas on [McGaw Graphics](https://www.mcgawgraphics.com/collections/karen-mathison-schmidt). The site describes her technique that I find so compelling:

> Working in oils and acrylics on gessoed hardboard, Karen uses both brushes and painting knives to build up glazes of transparent color, finishing with layers of opaque color painted loosely so that the vivid first layers show through in places; the end result often reminds the viewer of stained glass.

