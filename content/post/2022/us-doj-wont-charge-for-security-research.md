---
title: "US DOJ won’t charge for security research"
date: "2022-06-02T07:34:45+10:00"
abstract: "Punishing white hackers was never a good idea. I’m glad to see this being passed."
year: "2022"
category: Internet
tag:
- bruce-schneier
- security
location: Sydney
---
Bruce Schneier [shared](https://www.schneier.com/blog/archives/2022/05/the-justice-department-will-no-longer-charge-security-researchers-with-criminal-hacking.html) some great news via a [recent United States Department of Justice act](https://www.justice.gov/opa/pr/department-justice-announces-new-policy-charging-cases-under-computer-fraud-and-abuse-act)\:

> The policy for the first time directs that good-faith security research should not be charged. Good faith security research means accessing a computer solely for purposes of good-faith testing, investigation, and/or correction of a security flaw or vulnerability, where such activity is carried out in a manner designed to avoid any harm to individuals or the public, and where the information derived from the activity is used primarily to promote the security or safety of the class of devices, machines, or online services to which the accessed computer belongs, or those who use such devices, machines, or online services. 

This is important for many reasons, but two concern me the most:

* Often where the US legislates, other parts of the world follow. It's easier to say that we shouldn't be doing something here, if even the US has codified against it.

* The deck is increasingly stacked against us, so any additional white hat hackers and researchers, the better for all of us. It's ridiculous to disincentivise honest people from reporting issues with threats of legal action.
