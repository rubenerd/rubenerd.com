---
title: "Choosing audio codecs, via Wouter Groeneveld"
date: "2022-03-15T18:06:07+11:00"
abstract: "I’m still on CBR MP3. It works everywhere, it’s simple, it’s fast, and sounds great."
year: "2022"
category: Media
tag:
- audio
- mp3s
- music
location: Sydney
---
Remember Winamp and iTunes? Remember ripping CDs to a local collection, then burning mix CDs? What about transferring to Minidiscs, or Creative Zens, or iPods? Remember when you'd buy an album, and it'd be yours to play forever, without a media company revoking access when the person in charge of licencing sneezed?

I remember, because I still do it! But I also acknowledge I'm part of a rapidly shrinking minority. Which is a shame, because collecting and playing music is not only safer and more fun, but helps artists far more than the pittance that streaming services provide. But I digress.

I wrote a lot about my own journey back to local audio files a few years ago, but codecs were one aspect I didn't touch on. What's the best audio codec to use in 2022, especially for someone rocking (HAH!) a mix of different OSs and kit?

Wouter Groeneveld did a great post last Sunday about [choosing an audio codec](https://brainbaking.com/post/2022/03/choosing-an-audio-codec/). He discussed the history behind Ogg/Vorbis, MP3, and AAC/MP4, some of his own tests, and his decision to use Vorbis. From a technical, legal, and capacity perspective, it looks like the best choice.

I ended up going with 320 kb/s CBR MP3 for new files:

* MP3 is no longer patent encumbered, so I don't harbour concerns about being locked out of my files.

* It's a simple codec with broad support in retro devices, like the Palm Lifedrive and iPod that I use, and even my Pentium 1 tower with Winamp for nostalgic fun. CBR isn't as space efficient, but it's also universally supported.

* It's fast to encode, and familiar. I already have a toolchain for encoding podcast files, so I know the process and pitfalls very well.

* FLAC files might be useful for archiving or re-encoding, but 320 kb/s gives me an ample quality ceiling for what I can perceive, even with monitor headphones and a decent Hi-Fi. I was tempted to do a series of double-blind tests to get that down to 256, but the space saved wasn't worth worrying about.

Marco Arment [waxed lyrical](https://marco.org/2017/05/15/mp3-isnt-dead) (HAH!) about MP3s in 2017, which also broadly tracked with my views at the time. He compared MP3s to AAC and Vorbis, and concluded the mighty MP3 to be just fine.

> AAC and other newer audio codecs can produce better quality than MP3, but the difference is only significant at low bitrates. [..] Ogg Vorbis and Opus offer similar quality advantages as AAC with (probably) no patent issues, which was necessary to provide audio options to free, open-source software and other contexts that aren’t compatible with patent licensing. But they’re not widely supported, limiting their useful applications.
>
> MP3 is supported by everything, everywhere, and is now patent-free. There has never been another audio format as widely supported as MP3, it’s good enough for almost anything, and now, over twenty years since it took the world by storm, it’s finally free.

*(I'd say WAV is even wider supported than MP3, but his point stands)!*

Maybe at some point I'll switch, but right now MP3s hit the sweet spot for me for practicality, file size, support, and perceived quality. It's the same reason I carry a Ricoh GR III with me now rather than a full-frame SLR, and use a Mini-ITX case for my game machine.

Audiophiles who disagree with Wouter, Marco, and myself, are free to choose another format for their own libraries.
