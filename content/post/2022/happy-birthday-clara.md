---
title: "Happy Birthday, Clara!"
date: "2022-06-23T09:16:27+10:00"
abstract: "It was a big one for her yesterday, so we went and got tea :)."
thumb: "https://rubenerd.com/files/2022/birthday-cat@1x.jpg"
year: "2022"
category: Thoughts
tag:
- clara
- family
- personal
- tea
location: Sydney
---
It was a big one for her yesterday, so we took leave from work and went to the Tea Cosy on The Rocks in Sydney to relax and have some scones. Her little blue Prince Cat joined us for the fun.

It's hard to believe that I've known Clara for twelve years now, and been with her for almost ten. Not a day goes by when I'm not reminded how lucky I am. ♡

<figure><p><img src="https://rubenerd.com/files/2022/birthday-cat@1x.jpg" alt="Clara's blue Prince Cat next to our teacups and pots at The Tea Cosy." style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2022/birthday-cat@1x.jpg 1x, https://rubenerd.com/files/2022/birthday-cat@2x.jpg 2x" /></p></figure>
