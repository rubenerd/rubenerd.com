---
title: "No bad products, only bad prices?"
date: "2022-03-05T11:54:58+11:00"
abstract: "Gamers Nexus did a great debunking of this misleading question."
year: "2022"
category: Hardware
tag:
- ethics
- safety
- shopping
location: Sydney
---
It's time to invoke *Betteridge's Law of Headlines* here, because the answer is a resounding no!

Steve [did a great video back in January](https://www.youtube.com/watch?v=DC3Bky7Qccw) debunking this idea. There are plenty of objectively bad products. There are ones nobody should even accept as giveaways. Heck, there are ones you couldn't *pay* me to take!

I see this paraded out to justify everything from service-level agreement (SLAs) breaches, to *caveat emptor*. An unstable platform sold as rock solid, or a fire hazard sold as a computer case, is false advertising regardless of the attached price.

I guess tacking on a "sometimes" to that platitude isn't as punchy. Wasn't that an [Erasure song](https://www.youtube.com/watch?v=VvU6T2t_n-Q)? *Oooh sometimes, the truth is harder than pain inside&hellip;!*
