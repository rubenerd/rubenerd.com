---
title: "A list of games I’ve enjoyed"
date: "2022-08-11T09:25:12+10:00"
abstract: "Shamelessly ripping off Marcel’s blog post, with GOATs and other honourable mentions."
thumb: "https://rubenerd.com/files/uploads/screenie.commanderkeen1.png"
year: "2022"
category: Software
tag:
- bsd
- commander-keen
- dos
- freebsd
- games
- minecraft
- superliminal
- train-simulator
- the-stanley-parable
- x-plane
location: Sydney
---
Marcel in Hamburg made [a list of games](https://hrbf.de/a-running-list-of-games-i-enjoyed/) on his new blog *Life-Size Models of the Drama*. As an aside, that's a fantastic name. It inspired me to come up with a bit of a list of my own. I... didn't realise I had so many, given I spend most of my life in a text editor for fun!

The listed platforms are where I first played them, not necessarily where they first appeared.

### <abbr style="text-decoration:none;" title="Greatest of all time">GOATs</abbr>

* Commander Keen 1-3 <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[DOS]</span>
* Lemmings <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[DOS]</span>
* Microsoft Train Simulator <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[Win32]</span>
* Minecraft <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[FreeBSD]</span>
* Need for Speed: SE <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[DOS]</span>
* SimCity 3000 <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[Mac OS]</span>
* SimTower <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[Win16]</span>
* The Sims 2 <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[Mac OS]</span>
* The Stanley Parable <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[Steam]</span>


### Also Excellent

* A-Train <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[DOS]</span>
* Age of Empires II <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[Win32]</span>
* Atelier Ryza <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[Steam]</span>
* bsdgames <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[FreeBSD]</span>
* Chessmaster <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[DOS]</span>
* Chip's Challenge <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[Win16]</span>
* Cities Skylines <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[Steam]</span>
* Dovetail Train Simulator <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[Steam]</span>
* Dovetail Train Sim World <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[Steam]</span>
* Elasto Mania <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[Win32]</span>
* Fate/Grand Order <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[iOS]</span>
* Flight Simulator 97 <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[Win32]</span>
* Flight Simulator 2000 <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[Win32]</span>
* Formula 1 Grand Prix <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[DOS]</span>
* The Games: Winter Challenge <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[DOS]</span>
* GNOME Sudoku <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[FreeBSD]</span>
* Humongous Entertainment Franchise <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[Mac OS]</span>
* KNetWalk <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[FreeBSD]</span>
* LEGO Island <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[Win32]</span>
* Microsoft Golf 3.0 <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[Win32]</span>
* Midtown Madness: Chicago Edition <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[Win32]</span>
* Midtown Madness: San Franciso and London <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[Win32]</span>
* Monopoly Deluxe <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[Win16]</span>
* nbsdgames <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[FreeBSD]</span>
* Need for Speed II <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[DOS]</span>
* Need for Speed III <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[DOS]</span>
* Need for Speed: Most Wanted <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[Origin]</span>
* Pinball Deluxe <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[Win32]</span>
* Pipe Dream <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[Win16]</span>
* Pokémon Alpha Sapphire <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[Nintendo 2DS]</span>
* Pokémon Blue <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[Gameboy Colour]</span>
* Pokémon Brilliant Diamond <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[Switch]</span>
* PySolFC <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[Linux]</span>
* Rodent's Revenge <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[Win16]</span>
* SimCity Classic <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[DOS]</span>
* SimCity 2000 <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[Win16]</span>
* SimCity 3000 Unlimited <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[Win32]</span>
* SimPark <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[Win16]</span>
* SkiFree <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[Win16]</span>
* The Sims <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[Mac OS]</span>
* Spider Solitaire <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[Win32]</span>
* Stunts <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[DOS]</span>
* Superliminal <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[Steam]</span>
* Triazzle <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[DOS]</span>
* Where in the World is Carmen Sandiego? <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[Mac OS]</span>
* Wii Sports <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[Nintendo Wii]</span>
* Worms II <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[Win32]</span>
* Worms World Party <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[Win32]</span>
* X-Plane 11 <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[Steam]</span>


### Honourable Mentions

* Brandon's Lunchbox (first I ever played) <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[DOS]</span>
* Emacs Tetris <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[FreeBSD]</span>
* Fuji Golf <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[Win16]</span>
* JezzBall <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[Win16]</span>
* Mavis Beacon (does that count?) <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[Win32]</span>
* Microsoft Excel 97 Flight Simulator <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[Win32]</span>
* QPascal (I wrote silly games in it) <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[DOS]</span>
* Reversi <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[Win16]</span>
* SimTown <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[Win16]</span>
* Star Trek: Bridge Commander <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[Win32]</span>



### #TODO

* Atelier Sophie <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[Steam]</span>
* NetHack <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[\*nix]</span>
* Persona <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[Steam]</span>
* Portal <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[Steam]</span>
* Stardew Valley <span style="font-size:smaller;font-weight:bold;color:#2d75e0;">[Steam]</span>
