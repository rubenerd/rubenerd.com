---
title: "Speeding up FreeBSD jail start times"
date: "2022-12-05T20:01:39+11:00"
abstract: "Hint: check your sendmail(8)!"
thumb: "https://rubenerd.com/files/2020/beastie@2x.png"
year: "2022"
category: Software
tag:
- bsd
- freebsd
- jails
- troubleshooting
location: Sydney
---
<p><img src="https://rubenerd.com/files/2020/beastie@1x.png" style="width:96px; height:104px; float:right; margin:0 0 1em 1em;" srcset="https://rubenerd.com/files/2020/beastie@1x.png 1x" /></p>

I love [FreeBSD jails](https://rubenerd.com/starting-with-freebsd-jails/ "Starting with FreeBSD jails"). They predate Docker et.al. by more than a decade, and I'd say they're still more useful under most circumstances; especially when one uses them in conjunction with OpenZFS.

Their only source of frustration for me has been slow starts, to the point where some jails are beaten by VMs on my bhyve and Xen boxes. It happens so infrequently that I never bothered to check why; it was just something I noticed on my personal machines.

This weekend I was deploying a new VM to consolidate more stuff ([Mastodon post here](https://bsd.network/@rubenerd/109453684301028247)), like a gentleman. For these I wrote out config to **/etc/hosts** within each jail so I could communicate via hostname. These jails booted instantly.

It took me longer than it should to realise that [sendmail(8)](https://docs.freebsd.org/en/books/handbook/mail/#sendmail) was the culprit, which just like on a real server, requires a fully-qualified domain name (FQDN) configured or it will sit there on a new box waiting for an inordinate amount of time. That's it!

I've now updated my jail initialisation scripts to write out a proper hosts file every time. As I said on Mastodon, I've always done this for client deployments, but the [cobbler's kids go barefoot](https://en.wiktionary.org/wiki/the_shoemaker%27s_children_go_barefoot).

