---
title: "The first iPhone, and portable music in 2022"
date: "2022-05-24T07:43:31+10:00"
abstract: "I’m looking to use a dedicated music player again."
year: "2022"
category: Hardware
tag:
- apple
- ipod
- music
location: Sydney
---
This is going to be a bit of a nostalgia post again, but I'm also interested on what people's thoughts are for portable music players in 2022. But let's set the scene.

Steve Jobs announced the first iPhone as:

> a widescreen iPod with touch controls... a revolutionary mobile phone... and a breakthrough Internet communications device.

Retrospective writers routinely miss that the *order* here was just as important as the features themselves. Rumours swirled in the 2000s about Apple releasing their own phone, but the market still saw Apple as the *iMac, iPod, and iTunes* company that popularised digital downloads and massive portable music libraries.

It's hard not to see modern iPhones as having reversed this order entirely. They're Internet communications devices that *happen* to have phones, and maybe even a music player buried somewhere!

Which leads us to a sentence with nine words. Have you also noticed that the music player on modern iOS is a bit... rubbish? A simple, customisable navbar in the footer has been replaced with redundant menus, slideout modal windows that break the back/forward flow, and general sluggishness that's embarrasing considering the order of magnitude better hardware we have now. Simple inline controls on headphones for (un)pause have been flaky enough to be broken for *years*. One need only look at an iPod Classic's menu system, or a version of the original iPhone music app (called *iPod*) to see how far we've slid.

In Apple's defence, it still syncs with the Music app on Macs when it could be bothered, but it's clear the future of the platform is being written by streaming services. Almost nobody collects local music anymore, not least CDs or other analogue formats, and only weird people rip, mix, and sync.

Apple have also been transparent with their (meaningful) departure from the music space, both by removing the 3.5 mm headphone jack, and cancelling the iPod line. Previous phones could use wireless earbuds and their expensive new cans just fine, but these new phones don't work with regular headphones at all. I suppose it's internally consistent behaviour from a company that pours all this effort into the electronics and optics of their phone cameras, but cans Aperture.

*(Android manufacturers also mocked Apple for removing the jack, then did the exact same thing, just as they always do. We can't be seen as falling behind the market leader in reducing accessibility, damn it)!*

All this comes together to make today's phones objectively worse for listening to music than previous generations. It's a shame, because they could have easily maintained backward compatibility and a good local music app if they wanted to. It's not like they're short of cash.

So what to do now. Clara and I have been having so much fun buying and collecting music again, and I want to take it with me. I had huge success with splitting work and personal stuff into separate devices again, and I'm thinking I need to do the same for music. A dedicated, portable player that can't interrupt me with a million notifications sounds like bliss.

I have plenty of gear I can bring out of retirement, but ideally I'd like something that I can just drag and drop audio to, be it my Mac, FreeBSD tower, or Linux. The problem is, capacities in these dedicated players from the likes of SanDisk and Sony haven't budged in more than a decade, or have such poor build quality or acoustics that they may as well be making those plastic Crosley turntables.

I'm willing to concede that I have a niche use case here. But surely I can't be the only one.
