---
title: "Marcel Bischoff discusses commercial radio"
date: "2022-12-21T09:29:37+11:00"
abstract: "It was always that bad! Also touching on incentive structures, and the TikTok effect."
references: "https://mastodon.social/@hrbf/109540061989792253"
year: "2022"
category: Media
tag:
- feedback
- music
location: Sydney
---
On Monday I asked if commercial radio was always as bad as the current hit stations I keep hearing at our local coffee shop. [Marcel weighed in](https://mastodon.social/@hrbf/109540061989792253) with his experience:

> Just read your [Music Monday blog post](https://rubenerd.com/new-music-is-old/). I used to work in radio music promotion, so yeah, it has been this grim for a long, long time. I’m not sure it has ever been any other way. Even when I worked in the sector, I never listened to the actual program. I find general radio intolerable.

I guess we shouldn't be surprised!

He also shared this link to a [Billboard article](https://www.billboard.com/pro/songs-getting-shorter-tiktok-streaming/) on the state of modern music:

> A 2018 study by San Francisco-based engineer Michael Tauberg concluded that songs on the Billboard Hot 100 shed around 40 seconds since 2000, falling from 4:10-ish to roughly 3:30. The average length of the top 50 tracks on Billboard‘s year-end Hot 100 in 2021 was even less, a mere 3:07.

The shorter songs are attributed to a few things, notably:

> [P]latforms like Spotify count 30 seconds of listening as a full play that triggers a royalty payout, so it makes sense to expand a musical idea to that length. But a generation native to TikTok may not require even 30 seconds to engage with the music. With that in mind, it’s easy to imagine that the length of singles will continue to shrink. 

My interest in understanding incentive structures is a recurring theme here, whether it be digital privacy or software development. If shorter, shallower tracks are easier to produce, get higher play counts, and therefore generate more money, the industry will trend in that direction. In the paraphrased words of former Australian Prime Minister Paul Keating, markets shift behaviour.

There are still so many independent acts that are forging their own path and creating wonderful art that bucks this trend; the good news is that the Internet has made distribution and reaching a far-flung niche financially and technically viable. But I still don't envy the financial pressure they must be under, or the industry in which they still need to engage. Same goes for authors, now that I think about it.

Thanks to Marcel for the insight, it was food for thought. 👍
