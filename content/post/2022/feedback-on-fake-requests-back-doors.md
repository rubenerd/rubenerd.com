---
title: "Feedback on fake requests and back doors"
date: "2022-04-11T08:16:30+10:00"
abstract: "The attach against these companies were basic social engineering."
year: "2022"
category: Internet
tag:
- feedback
- security
location: Sydney
---
[Last Thursday I wrote](https://rubenerd.com/fake-law-enforcement-with-encryption-backdoors/ "Fake law enforcement with encryption backdoors") about Brian Krebs's report on fake law enforcement requests for user data, and Bruce Schneier connecting the dots over back doors. It made for some grim, if not altogether surprising reading.

Many of you emailed with what seems like an obvious connection in retrospect: these were classic phishing attacks, or at least social engineering. The only difference is the target were people who were trained and should have known better, instead of your parents logging in and thinking an email attachment has some juicy information.

It reminds me of that [PayPal email I got last year](https://rubenerd.com/paypal-account-spam/) that has to rank among the least professional I've ever received from a company I do (did?) business with. It's hard to feel like we're making ground or winning against fraudsters when even the professionals can't do things properly.
