---
title: "A @SimonWhistler reaction face"
date: "2022-03-09T15:02:53+11:00"
abstract: "Am I right, Peter!?"
thumb: "https://rubenerd.com/files/2022/simon-whistler@1x.jpg"
year: "2022"
category: Media
tag:
- business-blaze
- simon-whistler
location: Sydney
---
I was catching up on some old [Brain Blaze](https://www.youtube.com/channel/UCYY5GWf7MHFJ6DZeHreoXgw) episodes (#OGBB), and couldn't let this reaction pass:

<p><img src="https://rubenerd.com/files/2022/simon-whistler@1x.jpg" srcset="https://rubenerd.com/files/2022/simon-whistler@1x.jpg 1x, https://rubenerd.com/files/2022/simon-whistler@2x.jpg 2x" alt="Simon Whistler holding the bridge of his nose" style="width:500px; height:255px;" /></p>

I think I have my new *Picard Face Palm* image for tweets and company chat applications. *Am I right, Peter!?*
