---
title: "Graphics card feedback"
date: "2022-02-19T10:13:10+11:00"
abstract: "Choosing between the 3060 Ti and 3070, why not an AMD, cooling, and more."
year: "2022"
category: Hardware
tag:
-
location: Sydney
---
My post about graphics cards generated bunch of comments. I've summarised below, but thanks to all of you who sent them in.

**I didn't think you were much of a gamer?**

: I didn't think so either. I don't play AAA titles, but I love flight and train simulators, and world-builders like Minecraft. I think there's this expectation that you have to like shooting people under pulsing RGB lights to be considered a "gamer". It's not true.

**Let me rephrase, I didn't think you were a *modern* gamer**

: Fair! I spend more time on retrocomputing stuff than modern stuff. But while Sim City for DOS and Flight Simulator 98 are still so much fun, Cities Skylines and X-Plane can look spectacular.

**Are you concerned about cooling?**

: Definitely, especially for these space heater GPUs. I think I can fit a 140 mm fan in the back if I cut a panel out, and I can use the gap where the original IO panel went in the front bezel to discreetly add ventilation. That still might not be enough though, I'll need to run thermal tests.

**Why the RTX 3060 Ti or the 3070?**

: These and the 6700 XT (more on that below) are the best you can get in a dual-fan card. My silly [sleeper PC case](https://rubenerd.com/finally-buying-a-compaq-spaceship/) is oddly short, meaning I can only fit 260mm or smaller cards by the time I mount my beloved Noctua fans inside. The 3050 would still have given me a nice bump over my 970, but these other cards are a *huge* leap.

**Did you decide on the 3060 Ti or the 3070?**

: Not sure yet. The 3060 Ti hits that price/performance sweet spot. But I figure I'll buy the best this system can support if I'm burning money in a dumpster fire anyway. I know that for 10-15% better performance (based on reviews), this is ridiculous.

**Wouldn't the extra 3070 money be better spent upgrading the motherboard and CPU of the computer you're putting it into?**

: Almost certainly! But where's the fun in rationality?

**Why not an AMD?**

: I've been so impressed with the 5500M in my MacBook Pro, and considered a dual-fan MSI 6700 XT. Unfortunately Nvidia's binary drivers have always been easier and more consistent for me in FreeBSD, and Linux with Steam and Proton. I'd reconsider if this were a Windows machine.

**Did you hear Intel is finally releasing discrete GPUs again?**

: I saw their announcements, we'll have to see what eventuates. At the very least I hope it adds some competition and supply to the market, both of which might help alleviate price pressures. But I doubt anything will change any time soon.
