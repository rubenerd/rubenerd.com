---
title: "If you can automate it in your life, try"
date: "2022-06-11T07:23:46+10:00"
abstract: "As soon as you rely on a manual process to perform tedious but necessary jobs, you’ll forget."
year: "2022"
category: Software
tag:
- automation
- finance
- life
location: Sydney
---
Years ago I remember reading that backups must be automatic to be useful. As soon as you rely on a manual process to perform tedious but necessary jobs, our squishy human brains *will* forget, and we *will* lose data. In the words of chief engineer Thomas Andrews as portrayed in James Cameron's *Titanic*, it's a mathematical certainty.

*(It's funny that his quotes are all I remember from that movie, even after all these years. I assure you sir, she's made of iron).* 

I'm starting to see parallels to this everywhere. And I take it a step further: something not *quite* as good, efficient, or even affordable still beats the pants off something manual, especially if one has a life outside that particular thing.

**I'm not a financial advisor, and this is not financial advice!** But the sagest words I've heard about investing came from Jim Kloss at a cute coffee shop in Philadelphia. Managed investment accounts with regular deductions you don't need to think about are (probably) better, for the same reason as backups. You want to do these important things regularly, so why not use infrastructure that won't forget, or will mess with it?

*(For all their faults, I think this is why Australia's compulsory [superannuation](https://en.wikipedia.org/wiki/Superannuation_in_Australia) scheme and Singapore's [CPF schemes](https://en.wikipedia.org/wiki/Central_Provident_Fund) are great ideas. How many more people now have retirement savings than would otherwise)?*

I suspect when people think of *automation*, images of complicated [Rube Goldberg](https://en.wikipedia.org/wiki/Rube_Goldberg#Cultural_legacy) machines or spaghetti code scrawling across a screen come to mind. But there's so much low-hanging fruit.


