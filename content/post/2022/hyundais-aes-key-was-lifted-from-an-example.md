---
title: "Hyundai’s AES key was lifted from an example"
date: "2022-08-24T07:07:10+10:00"
abstract: "Copy+pasta isn’t anything new, but examples like this are a stark reminder."
year: "2022"
category: Software
tag:
- encryption
- security
location: Sydney
---
The Register's Thomas Claburn [reported](https://www.theregister.com/2022/08/17/software_developer_cracks_hyundai_encryption/) on a [blog post by greenluigi1](https://programmingwithstyle.com/posts/howihackedmycar/) where they investigate the feasibility of modifying the car's In-Vehicle Infotainment system. It's well written with clear steps and screenshots; I love reading through someone's thought process when it comes to this stuff.

Here's where things get dicey:

> While looking through the source code available from Mobis’s site, I searched for all files that were shell scripts. In the results I found a shell script file called linux_envsetup.sh. This file opened up everything. Turns out this was the script that creates the system update zip file. 
>
> Turns out I had the zip password for the system update on my hard drive the entire time. And helpfully the encryption method, key, and IV was also in the script. The script also hinted at RSA signing being used, but unfortunately the key used for that was not in the source code. But at least I had the encryption key. To verify that no one else had gotten this far I googled the encryption key.

And here's the kicker that everyone is talking about:

> Turns out the encryption key in that script is the first AES 128bit CBC example key listed in the NIST document SP800-38A. 

What worries me is how much such code is out there. People joke that StackOverflow is basically a copy+pasta site at this stage, and if a contractor for a car company doesn't have internal processes to catch this stuff, it could happen almost anywhere. Even [procedurally-generated code](https://voonze.com/copilot-github-copilot-makes-code-less-secure/) makes these mistakes.
