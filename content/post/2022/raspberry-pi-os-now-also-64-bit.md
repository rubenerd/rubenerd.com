---
title: "Raspberry Pi OS is now 64-bit"
date: "2022-02-08T13:42:43+11:00"
abstract: "Via Jeff Geerling."
year: "2022"
category: Hardware
tag:
- raspberry-pi
location: Sydney
---
I missed this news, [via Jeff Geerling](http://www.jeffgeerling.com/blog/2022/its-official-raspberry-pi-os-goes-64-bit)\:

> Yesterday Raspberry Pi announced the 64-bit version is finally official. It's no longer hidden away in a crusty forum link, it's linked straight from their public downloads page and the Raspberry Pi Imager.

The [download page](https://www.raspberrypi.com/software/operating-systems/) still displays the default 32-bit version first, but having 64-bit as an official option is a big step.

Going to 64-bit isn't an automatic win in and of itself; the original few generations of hardware don't support it, and you do lose a bit of memory. But as more software targets 64-bit specifically, I'm sure we'll start to see it listed as a requirement.

64-bit FreeBSD has also been available for a while. Those interested can [check out this page](https://freebsdfoundation.org/freebsd-project/resources/installing-freebsd-for-raspberry-pi/) published by the FreeBSD Foundation.
