---
title: "Demand for ads on the receiving end"
date: "2022-12-04T08:05:25+11:00"
abstract: "Why do we only hear about the supply side for online ads?"
year: "2022"
category: Internet
tag:
- advertising
- doc-searls
- privacy
location: Sydney
---
[Doc Searls wrote a post](https://blogs.harvard.edu/doc/2022/11/06/on-twitter-2-0/ "On Twitter 2.0") about various social media platforms early last month. Some of his functional claims of Twitter are already a bit out of date, but this observation is evergreen; emphasis added:

> Not much discussed, but a real possibility is that advertising overall will at least partially collapse. This has been coming for a long time. (I’ve been predicting it at least since 2008.) First, **there is near-zero (and widespread negative) demand for advertising on the receiving end**. Second, Apple is doing a good job of working for its customers by providing ways to turn off or thwart the tracking that aims most ads online. And Apple, while not a monopoly, is pretty damn huge.

I've mentioned here many times how relieved I am for Apple's advocacy, though even they've succumb to the siren song of advertising of late. The settings screen on my iPhones now have menu items to redeem free trials for subscriptions I don't want, and the iTunes Store has been hollowed out into one large promotion for Apple Music, their streaming music platform I don't use.

Turns out the barrier to being better than advertising tech companies like Facebook isn't that high! In the worlds of William Shakespeare: *who'd have thunk it?*

But it's Doc's observation about the demand side that piqued my interest. We're so regularly told about the supply side, from new tracking technologies to business models. But if you ask someone on the proverbial street if they *like* advertising, or even if they *want* it, you'll get the same answer.

This takes on a whole other dimension online. Platforms attempt to spin tracking as a positive; we're told we should want "personalised ads" and that turning off tracking will result in ads "not tailored to your specific needs or preferences". I heed none of these warnings, and I'll bet you don't either.

Years ago I read a prominent English venture capitalist who should have known better, and who's name escapes me, claim ads delivered via tracking is no different from reading car ads in an automotive magazine. He's right in one aspect: a self-selecting audience on a car site will see car ads. But that same site or magazine isn't then tracking my reading of a newspaper and injecting ads there as well, without my permission or perhaps even knowledge. It makes sense if you reduce people to consumers without agency, but that's not a world in which I want to live.

It's one of the more surreal aspects of the modern web. For all the technical innovation and breathtaking progress happing in this space over the last three decades, most stuff online is still propped up by targeting Jeff with a new mattress after he's bought a new mattress.
