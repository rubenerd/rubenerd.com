---
title: "Pop Up Parade Fate/Grand Carnival figs"
date: "2022-02-03T09:06:27+11:00"
abstract: "CUTE! Damn it."
thumb: "https://rubenerd.com/files/2022/fate-grand-carnival-figs@1x.jpg"
year: "2022"
category: Anime
tag:
- anime-figs
- fate
- fate-grand-order
- mashu
location: Sydney
---
<p><img src="https://rubenerd.com/files/2022/fate-grand-carnival-figs@1x.jpg" srcset="https://rubenerd.com/files/2022/fate-grand-carnival-figs@1x.jpg 1x, https://rubenerd.com/files/2022/fate-grand-carnival-figs@2x.jpg 2x" alt="Photos of Mashu Kyrielight and Fujimaru Ritsuka in fig form by Pop Up Parade" style="width:500px; height:400px;" /></p>

Cute. Cute! **CUTE!**

Preorders are open until the 20th of February. [D-damn it](https://rubenerd.com/tag/decluttering).
