---
title: "Edgar Allan Poe on ciphers"
date: "2022-02-03T08:45:39+11:00"
abstract: "“Human ingenuity cannot concoct a cipher which human ingenuity cannot resolve”."
year: "2022"
category: Software
tag:
- encryption
- quotes
location: Sydney
---
Wikiquote quotes the author's [July 1841 submission](https://en.wikiquote.org/wiki/Edgar_Allan_Poe#Quotes) to Graham's Magazine:

> Few persons can be made to believe that it is not quite an easy thing to invent a method of secret writing which shall baffle investigation. Yet it may be roundly asserted that human ingenuity cannot concoct a cipher which human ingenuity cannot resolve.

I was tempted to say this no longer holds, but I don't think that's true. Every Enigma Machine has its Alan Turing and Bletchley Park.

The question now is timeframe. We've reached the point where our computers can brute force anything, provided we have the heat death of the universe to finish. But clever people will also chip away in the meantime, or invent new computers to speed up the process. It's happened before, and it will again.

As [Stewart Brand observed](https://www.rogerclarke.com/II/IWtbF.html)\:

>  Information Wants To Be Free. Information also wants to be expensive. ...That tension will not go away.

Maybe we need *Triple* Edgar Allan Poe, where we decrypt him with a second key in between encrypting him with his first key. We could call it [Triple DES](https://en.wikipedia.org/wiki/Triple_DES), or Triple Difficult Edgar Sipher. Hmm, that would involve us misspelling cipher.
