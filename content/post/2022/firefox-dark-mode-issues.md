---
title: "Firefox dark mode issues"
date: "2022-02-02T14:05:25+11:00"
abstract: "Works in Safari and Vivaldi, so must be a Firefox for Mac issue?"
year: "2022"
category: Software
tag:
- bugs
- colour
- dark-mode
- themes
location: Sydney
---
A few of you have reported that my [dark mode theme](https://rubenerd.com/dark-mode-now-live/) no longer works automatically. I haven't made any changes, so I was surprised.

I set my Mac to dark mode and refreshed the site in Firefox, and confirmed that it's still showing the light theme. Safari and Vivaldi work, so it must be an issue with a recent Firefox build.

Apologies if you're affected by this; I do try to take accessibility seriously. I'll check if it affects other OSs and file a report.
