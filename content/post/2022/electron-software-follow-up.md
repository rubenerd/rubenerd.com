---
title: "Electron software follow-up"
date: "2022-04-02T08:19:11+10:00"
abstract: "A comment from Miguel A. referred me to an article by Jason Snell about Electron, accessibility, and SwiftUI."
year: "2022"
category: Software
tag:
- accessibility
- electron
location: Sydney
---
Miguel A. emailed me [this article by Jason Snell](https://sixcolors.com/post/2021/08/not-important-enough-1password-abandons-its-native-mac-app/) from last year, who summarises the integration and usability issues with the [latest 1Password](https://rubenerd.com/1password-electron-flareup-is-good-news/), but could just as well apply to any Electron program:

> I think it’s fair to say that most users don’t care about the tools that a developer uses to write the apps we use. But using a system like Electron does have consequences: Electron apps have a reputation for being slow, eating up a lot of system memory, and—perhaps most offensively—failing to behave like proper, “native” apps on whatever platform they operate.

He suggests the move is also a grim assessment of Apple's current development tools like SwiftUI, something I don't know enough to comment on but I take his word. He concluded the section with:

> What’s really causing all this consternation, I think, isn’t 1Password moving to Electron. Electron is a bit of a bogeyman. The root problem is this: 1Password, originally a Mac-forward software developer, has simply decided that the Mac isn’t important enough.

I get where he's coming from, but in practice I [don't see the distinction](https://rubenerd.com/the-issue-isnt-privacy-its-privacy/); both are symptoms of prioritising an internal process over the needs of paying customers.

Several of you emailed with comments saying the bigger issue with Electron isn't one app using a ton more resources, it's the fact they add up quickly. This is especially true for utilities like a password manager that are expected to run continuously. I agree.

I've also seen VSCode listed as either the lone or rare example of a good Electron application, but I'm not convinced. Having 1Password even *match* VSCode would still be a downgrade from what people came to expect from the software before.

Jason ends his post with a broader look at Apple software which is hard to escape, and perhaps goes a long way to explain the visceral reaction the announcement has had:

> A banner Mac app and app developer has abandoned a platform-native app for the same web-app wrapper it’s using on Windows. Even if it’s the best Electron app you’ve ever seen, it won’t be the same—and more than that, it says something painful about the future of Mac software.

There are many competing priorities in software development, and Electron has convinced a sufficient number of businesses that they can avoid some pain by pushing it onto their customers. Unfortunately in many cases, they're right.
