---
title: "Archive it if you care about it"
date: "2022-09-28T16:23:55+10:00"
abstract: "Bad news about a YouTube franchise reminded me about the golden rule of the Internet: If it’s not archived, it won’t exist."
year: "2022"
category: Internet
tag:
- archives
- media
- the-try-guys
- youtube
location: Sydney
---
If Beyoncé were an engineer, she might have suggested that if one were to [like something](https://www.youtube.com/watch?v=4m1EFMoRFvY), they should have put a disk on it. That might be among the worst sentences I've ever typed, and I couldn't be more proud.

> Wha-oh oh, oh, oh-oh, oh, oh, oh, oh oh oh.

Everyone has felt that sting when books, videos, music, podcasts, webcomics, and sites they care about go away, or are threatened. It's normal to feel an attachment to something that formed an important part of your life, regardless of what it is. Cynics feign ignorance of this, with jokes that are perhaps more self-effacing than they intend. Denial, Egyptian rivers, and all that.

Fans of a YouTube franchise got some bad news today about one of the cast. Clara and I loved specific episodes (which happened to not include the person in question), and credited their lighthearted take on life with getting us through Covid lockdowns in 2020. This echos a similar issue with a [popular cooking channel](https://rubenerd.com/the-end-of-the-bon-appetit-test-kitchen/) we watched for similar reasons.

Whether those shows will continue to be available is an open question, but I'm sceptical if the past offers any hints.

Unlike the physical media of yore, streaming and online video platforms are ephemeral places. If misguided copyright law doesn't take something down, it's the heavy-handed and short-sighted whims of a platform, the creators themselves, or even a technical issue or outage. It can affect anyone.

The only way to be sure you can read, listen to, or watch stuff you care about is to archive it. Read a tutorial about yt-dlp for videos. Download webcomics. Archive podcast episodes. If you don't care, [that's fine](https://rubenerd.com/defensive-blogging/). If you do, you should.
