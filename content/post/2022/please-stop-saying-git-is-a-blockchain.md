---
title: "Calling git a blockchain to rebrand bad tech"
date: "2022-08-12T08:59:21+10:00"
abstract: "It’s such a ridiculous claim"
year: "2022"
category: Software
tag:
- bitcoin
- blockchain
- scams
location: Sydney
---
To shore up their [crumbling legitimacy](https://rubenerd.com/cryptocurrency-waste/) in the face of growing and justified scrutiny, blockchain advocates are on a rebranding exercise that's as cynical as it is transparent! Some examples of this include:

* Claiming it might not presently have utility, but boy howdy, it could in the future! I could make such an [unfalsifiable](https://rationalwiki.org/wiki/Falsifiability) claim about my blog. Wait, ouch.

* Defending the record (such as it is) of one chain over another. Bitcoin is slow, brittle, and expensive, but Etherium is better! No no, not Etherium Classic, the other one we hard-forked to after saying the chain is immutable and decentralised.

* Piggybacking off the success and credibility of existing tech by drawing tenuous or misleading comparisons.

It's this latter one that has drawn in git. Like all good red herrings, it *sounds* superficially plausible. But such a comparison is, at best, disingenuous.


### But technical accuracy is the best kind of accuracy!

git employs a Merkel tree structure, and its use is also more centralised in practice than people admit. If that makes it a blockchain, then the sports car downstairs is as practical as a bus on account of its four wheels and a roof.

In reality:

* git doesn't have an [immutable ledger](https://rubenerd.com/git-not-being-a-version-control-system/ "Git not being a version control system").

* git doesn't have an automated consensus mechanism. This wasn't an oversight; it was specifically designed for developers to [act as the interface](https://www.kernel.org/doc/html/latest/process/submitting-patches.html "Guide on the manual process to submit patches").

* Appending records doesn't take [terawatts of energy](https://rubenerd.com/cryptocurrency-crash-reducing-co2-emissions/ "Cryptocurrency crash reducing CO₂ emissions") consumed performing duplicated effort, or stumping up in a complicated, exclusionary, and non-existent proof-of-stake system its advocates claim is just around the corner every six months.

This isn't a simple semantic argument; these are critical distinctions its proponents claim make blockchains resilient in a trustless environment, [prevent fraud](https://rubenerd.com/cryptocurrency-doesnt-solve-fraud "Cryptocurrency doesn't solve fraud"), [address financial ills](https://rubenerd.com/crypto-currency-snakeoil/ "Crypto-currency snakeoil"), and [why you can't undo](https://tante.cc/2022/06/16/but-you-could/). There's an Evangelion reference in there somewhere.

Forks are a perfect illustration of the difference. While they're a desirable feature of git, they're an aberration to blockchains. Forks in a zero-trust environment introduce fundamental and irreconcilable questions about which is the authoritative branch. This is a disaster for anything claiming to be a currency, or a repository of smart contracts designed to operate without meatspace laws. git's users and developers make no such claims, perhaps beyond cryptographically signing commits. But equating the two is New York pizza-cheese levels of stretch.

Even if you think git (and [ZFS](http://www.osdevcon.org/2009/slides/zfs_internals_uli_graef.pdf "ZFS internals talk by Ulrich Gräf"), and...) satisfies a narrow-enough definition to be a blockchain (I'm unconvinced, in case you didn't notice!), it's still utterly disingenuous to take credit for the utility of other tech *if it doesn't operate the way yours does*. This form of tortured mental gymnastics isn't half as entertaining as the [real thing](https://rubenerd.com/katelyn-ohashis-10-point-routine/), and unless you bet on athletics, won't lose you as much money.

It feels ridiculous that *any* of this needs to be called out, but that's what happens when arguing against those acting in bad faith. If only shifting goalposts were their only sin.
