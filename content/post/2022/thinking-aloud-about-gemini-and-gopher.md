---
title: "Thinking aloud about Gemini and Gopher"
date: "2022-11-28T05:04:02+11:00"
abstract: "Using aeroplane Wi-Fi made me think about lightweight stuff again."
year: "2022"
category: Internet
tag:
- gemini
- gopher
location: Tokyo
---
*Uploaded a few days later.*

I'm typing this from a plane cruising at 11,582 metres above the Pacific Ocean between Japan and Australia. It feels so implausible, unreal, and fun:

> [Mastodon post](https://bsd.network/@rubenerd/109410209885193567): Using satellite Internet on a plane is one of those "wow, I'm living in the future!" feelings. 

Yet it's also been a bit of a reality check I wasn't expecting. Much of the world has at least moved on from dialup, but this low-speed, high-latency connection is closer to how more people use the Internet than I probably appreciate.

You *do* use a connection like this differently. You ration what you're downloading, use plugins like NoScript to more actively filter dynamic content and images, and you check the size of a package before downloading it. You even get used to unitasking, because you probably can't be loading too many concurrent things before your primary task gets painful.

My own site loaded faster than most, on account of it being technically simple (albeit with a lot of static metadata). But other news and technical sites required loading in [links(1)](http://links.twibright.com/) in the console for them to load in any reasonable time. Once I did this, I wished I could browse more of the web like this, and that sites were better optimised (or even tested at all) for text-based browsers.

Then I remembered such a world already exists:

* [Gopher](https://en.wikipedia.org/wiki/Gopher_(protocol)) has been online since the early 1990s, and offers a text-based interface with linked media assets and a logical hierarchical structure that lends itself well to sites like blogs with large archives. You know it must still be kicking around with active interest, based on the number of people who dismiss it saying that "nobody uses it". I call it the Netcraft Principle; BSD people know what I'm talking about.

* More recently, [Gemini](https://gemini.circumlunar.space/docs/specification.html) has offered a lightweight protocol and markup language that extends this idea with basic media embeds and support for TLS. My impression of it is a reimagining of the classic web.

It makes me want to run my own.

My site is created with the Hugo static site generator and a bunch of Markdown and HTML files. I can't imagine it'd be difficult to take the same files, and create a Gemini site in parallel. From looking at the spec, the only challenge would be extracting links and including them in their own paragraphs, which Gemini pages expect. It's also an interesting idea for accessibility too.

*(I'm also mulling the idea of going back to a dynamic site using TextPattern or even WordPress, because static sites are a PITA when travelling. In which case, I'd need to find a plugin, or figure out a way to interface Gopher with the site cache somehow).*

It also raises the question of whether I'd want to run such a dual-stack site, or whether I'd host different content on the Gemini (or Gopher) site. There might be something fun about putting more technical or personal ideas on the Gemini site, which would dissuade the more tedious trolls.

Anyone running a dual-stack blog? I'd love to hear about your experiences.

