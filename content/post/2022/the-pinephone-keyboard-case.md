---
title: "The PinePhone keyboard case"
date: "2022-01-30T09:24:43+11:00"
abstract: "This looks awesome!"
thumb: "https://rubenerd.com/files/2022/PP_kb_PlaMo.jpg"
year: "2022"
category: Hardware
tag:
- keyboards
- phones
location: Sydney
---
[This looks so cool](https://pine64.com/product/pinephone-pinephone-pro-keyboard-case/). The original PSION was the only PDA I thought gave my beloved Palms a run for their money, so it's great to see the form-factor still has fans:

> The keyboard case is compatible with both the PinePhone and PinePhone Pro and features a clam-shell design. It uses pogo pins located on the smartphone’s midsection and attaches by replacing the default back cover. This add-on effectively turns the PinePhone (Pro) into a PDA with an in-built LTE modem.
>
> The keyboard case is supported by multiple mobile Linux operating systems including, but not limited to: Manjaro Linux, DanctNIX Arch Linux, postmarketOS and Mobian.

I wonder if anyone has FreeBSD running on one of these things?

<p><img src="https://rubenerd.com/files/2022/PP_kb_PlaMo.jpg" alt="Photo showing the case attached to a PinePhone" style="width:300px; height:300px;" /></p>
