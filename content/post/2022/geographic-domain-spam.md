---
title: "Geographic domain spam"
date: "2022-02-20T09:27:46+11:00"
abstract: "These are always fun."
year: "2022"
category: Internet
tag:
- spam
location: Sydney
---
These are always fun. I'm replying in public here, because surely this legitimate outfit must read this site to be aware of my business, right?

> Dear CEO,
>
> (It's very urgent, please transfer this email to your CEO. If this email affects you, we are very sorry, please ignore this email. Thanks)

We prefer the term *Managing Director* in the glorious Commonwealth of Nations, but yes, I am the fabled CEO of *Rubenerd Enterprises*. But please, call me Your Excellency.

> We are a Network Service Company which is the domain name registration center in China. We received an application from Hai Tong Ltd on December 27, 2021. They want to register " rubenerd " as their Internet Keyword

They want to register *my* Internet Keyword? Do these people know how much money I have to send to The Big Search Engines to keep my Internet Keyword?

> and " rubenerd .cn "、" rubenerd .com.cn " 、" rubenerd .net.cn <"、" rubenerd .org.cn " domain names, they are in China domain names. But after checking it, we find " rubenerd " conflicts with your company. In order to deal with this matter better, so we send you email and confirm whether this company is your distributor or business partner in China or not?

You're right to contact me. This business, which I'm sure is legitimate, is not affiliated with *Rubenerd Enterprises*. The fact you've brought this to my attention is appreciated. You haven't asked for payment, so I can only assume you're willing to do this for free, which is even better.

I consider this issue resolved. Thank you!
