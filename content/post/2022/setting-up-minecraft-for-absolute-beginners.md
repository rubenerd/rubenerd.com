---
title: "Setting up Minecraft, for absolute beginners"
date: "2022-08-18T16:29:35+10:00"
abstract: "Answering more questions from people about Minecraft."
year: "2022"
category: Software
tag:
- bsd
- freebsd
- games
- minecraft
- netbsd
location: Sydney
---
There's a David Bowie reference in there somewhere. I've received more feedback about Minecraft than any other post over the last few months, mostly asking the same questions about how to get started. I hope this post is useful!


### The different versions

Minecraft comes in two broad versions, the names of which have changed over time to make life confusing!

* **Java Edition**. The original and best. It's cross-platform, customisable, supports plugins/mods, and is the easiest to backup and run a server for. This is what streamers and serious players run, and its what I describe below.

* **Minecraft for Windows**, previously known as Bedrock. It has the advantage of running on game consoles, but you can't run your own server (easily), import it into Java edition (easily) or run mods.


### Buying the game

Since Microsoft purchased Minecraft's developer Mojang, you need a Microsoft account to buy and play it. I'll admit, this is the most frustrating part of configuration.

To buy the game:

1. **Create a Microsoft account**. Even if you make a disposable one just to play Minecraft, keep the details handy. Microsoft *do not care* if you lose access. Ask me how I know this!

2. **Buy the game from Minecraft.net**. Click *[Get Minecraft](https://www.minecraft.net/en-us/get-minecraft)*, choose *Computer*, sign in with your Microsoft account, then buy.

3. **Download the Java version**. It should auto-detect your OS and provide the correct launcher.


### Running the game

Once you've bought, downloaded, and installed the game on each device you want to play on, the launcher will prompt you for your Microsoft account details. After this, you don't need to interact with Microsoft anymore.

This lets you run a world on your local machine, and is a great introduction to the game mechanics. If you only intend to run a world for yourself, you're basically set from here.

Let me know if you're interested in a basic introduction about how to *play* Minecraft, but honestly half the fun is learning how to craft and survive yourself!


### Running a server

When you're ready to share a world between players, you can run your own server. It can be on the same machine as one of your players, or a dedicated box. I prefer the latter, because it doesn't chew up local resources, and is easier to backup.

I've written how to do this on [FreeBSD](https://rubenerd.com/how-we-run-a-minecraft-server/), [NetBSD](https://rubenerd.com/netbsd-can-also-run-a-minecraft-server/), and [OpenBSD](https://rubenerd.com/rjc-shows-minecraft-running-on-openbsd-too/), but the broad steps that are applicable to all platforms are:

1. **Download an OpenJDK**. As of writing, you need Java 1.17 or above. The server will inform you if this has changed when you try to launch.

2. **Download the Java Minecraft server**. It's free, and available from the [Minecraft site](https://www.minecraft.net/en-us/download/server).

3. **Launch the server** as per the instructions on the above site.

4. From the Minecraft launcher on your local machines, **choose Multiplayer** and enter the IP address of your server.

The server is self-contained in a folder, so it's easy to backup or transfer to another machine, even one running a different OS. I run Minecraft on FreeBSD, but I copied across to my Mac when Clara and I travel so we can play on the go. The alternative could be running on a cloud instance or a VPS with a VPN.


### Where to go from here

Minecraft is one of those games that can be as simple or complicated as you want. I *really* encourage new players to get used to the game before going wild with mods and addons, as I did. Confusion isn't fun.

I [detailed the extensions and tooling](https://rubenerd.com/how-we-run-a-minecraft-server/) we use in an earlier post, but here are some in brief:

* **[PaperMC](https://papermc.io/)**. This is a high-performance fork of the Minecraft Java server. It's a drop-in replacement, and enables the use of plugins and other mods. You can also revert to standard Minecraft if you want.

* **[Iris](https://irisshaders.net)** modifies your launcher to improve your graphics and enable the installation of shaders. Until recently we used **[OptiFine](https://www.optifine.net/home)**, which is also a good alternative.

* **[Complimentary Shaders](https://www.complementary.dev/shaders-v4/)** are Clara's and my favourite. They really make the game visually pop, though you almost certainly need a discrete graphics card for it to work.

* **[MyWorlds](https://www.spigotmc.org/resources/myworlds.39594/)** lets you run multiple worlds simultaneously, and connect them together with custom portals. Implementing this is well beyond the scope of this post, but I might do a follow-up explaining why this is so cool in a future post.

You should also support the plugin developers with donations if you find their stuff useful.

That's it for now, happy Minecrafting! Just be warned, this is the most addictive and fun game I've ever played.
