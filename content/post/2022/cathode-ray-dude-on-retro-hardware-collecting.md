---
title: "Cathode Ray Dude on retro hardware collecting"
date: "2022-06-19T09:32:15+10:00"
abstract: "It can feel demoralising picking this stuff up from your childhood and not getting it working. Have to take a step at a time."
year: "2022"
category: Hardware
tag:
- nostalgia
- retrocomputing
location: Sydney
---
I haven't ever read this [put so well](https://twitter.com/crdudeyoutube)\:

> One of the things that sucks about retro hardware collecting is when you get in over your head on something - you've wanted it from a distance for ages, you finally get it, and find out you aren't capable or dedicated enough to make it go.

I've felt that for a few pieces of kit lately. I keep it around in the hopes that one day I'll skill up enough to work on them.

This has worked out in a few cases. I was ready to throw away a tape drive years ago, then I learned about belts and how to replace them. A set of DIMMs in my Pentium 1 tower suddenly worked when I learned about timings and voltages. Heck, I even learned recently that a beloved childhood toy has a glorified Z80 in it, which gets me a *step* closer to figuring out how to bring it back to life one day.

*(It can also work out for other people too. I bought a full height Gateway 2000 machine to use as a NAS chassis, then cleaned it up and accidentally got it working. The person I sold it to in Brisbane said it was his childhood machine, and he was over the moon that someone had preserved one. I'm still riding a bit of that high as we speak).*

But it can also feel demoralising at times. You think you understand something, or that you *should* understand it, but it's just out of your grasp. Worse, you *want* to figure it out yourself. It weirdly feels like you're letting your childhood self down. As with anything, I guess it just takes a step at a time.

