---
title: "Gloria Tells: Out of Control"
date: "2022-08-23T08:46:44+1000"
abstract: "This is such a fun soul song!"
thumb: "https://rubenerd.com/files/2022/yt-HKJx07Z7mwk@1x.jpg"
year: "2022"
category: Media
tag:
- music
- music-monday
- soul
location: Sydney
---
Today's *[Music Monday](https://rubenerd.com/tag/music-monday/)* is coming out on... Tuesday. The *aim* was to have it out yesterday, so that count's right? Don't answer that.

<a href="https://www.youtube.com/watch?v=ogI5bRISFHw" title="Play Gloria Tells, Out of Control"><img src="https://rubenerd.com/files/2022/yt-HKJx07Z7mwk@1x.jpg" srcset="https://rubenerd.com/files/2022/yt-HKJx07Z7mwk@1x.jpg 1x, https://rubenerd.com/files/2022/yt-HKJx07Z7mwk@2x.jpg 2x" alt="Play Gloria Tells, Out of Control"></a>

This is [such a fun song](https://www.youtube.com/watch?v=ogI5bRISFHw)! One of our favourite local coffee shops has been playing a modern soul album on repeat, and this one track has been in my head for days. Gloria's vocals, the backing harmony, and the instrumentals, *chef's kiss!*
