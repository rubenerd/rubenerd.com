---
title: "Let’s circle back on this nuanced lede"
date: "2022-09-07T11:05:56+10:00"
abstract: "This list of buzzwords and phrases from Perfect It might be too good."
year: "2022"
category: Media
tag:
- advertising
- buzzwords
- language
location: Sydney
---
*Perfect It* may have [done just that](https://intelligentediting.com/blog/42-super-annoying-buzzwords-and-buzzphrases-and-what-to-do-about-them/)\:

> At the end of the day, ditching buzzwords would be a win-win. There’s no problem getting buy-in—it’s not bleeding-edge; every thought leader is in alignment with it. It should be the low-hanging fruit going forward. When you open the kimono, however, you have to admit you can’t move the needle: corporate groupthink, fads and fetishes, and basic inertia make a perfect storm. Even if you’re giving 110%, you don’t have the bandwidth to boil the ocean like that. It seems like there’s no ROI—it is what it is.
> 
> But if words are your wheelhouse, you can disrupt that and level up. If you think outside the box, you can leverage some next-level technology to pivot and raise the bar to get synergy and make a paradigm shift. You won’t ever get a hard stop on buzzwords, but you can be truly impactful.

I hope what I just had was a hiccup, and not something worse.
