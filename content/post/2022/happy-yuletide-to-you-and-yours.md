---
title: "Happy Yuletide to you and yours :)"
date: "2022-12-25T10:50:00+11:00"
abstract: "🎄"
thumb: "https://rubenerd.com/files/2022/yuletide-2022@1x.jpg"
year: "2022"
category: Thoughts
tag:
- holidays
- yuletide
location: Sydney
---
<figure><p><img src="https://rubenerd.com/files/2022/yuletide-2022@1x.jpg" alt="Saber from Fate standing beside our tree" srcset="https://rubenerd.com/files/2022/yuletide-2022@2x.jpg 2x" style="width:500px; height:333px;" /></p></figure>
