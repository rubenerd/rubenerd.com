---
title: "The Rémilly–Saarbrücken railway"
date: "2022-06-12T15:58:37+10:00"
abstract: "A French and German 55-kilometre long railway line, that connects the French Lorraine region to the German city Saarbrücken"
thumb: "https://rubenerd.com/files/2020/metz-bahnhof@1x.jpg"
year: "2022"
category: Travel
tag:
- europe
- france
- germany
- photos
- random-articles
- trains
- wikipedia
location: Sydney
---
<p><img src="https://rubenerd.com/files/2020/metz-bahnhof@1x.jpg" srcset="https://rubenerd.com/files/2020/metz-bahnhof@1x.jpg 1x, https://rubenerd.com/files/2020/metz-bahnhof@2x.jpg 2x" alt="Photo of Metz station on the line" style="width:500px; height:333px;" /></p>

I love hitting the [Wikipedia random article](https://en.wikipedia.org/wiki/Special:Random) link until something related to computers, jazz, or travel comes up. Today we got the [Rémilly–Saarbrücken railway](https://en.wikipedia.org/wiki/R%C3%A9milly%E2%80%93Saarbr%C3%BCcken_railway), which is:

> a French and German 55-kilometre long railway line, that connects the French Lorraine region to the German city Saarbrücken. The railway was opened between 1851 and 1852. It is part of the international railway connection between Paris and Frankfurt am Main. 

There's no photo on the English Wikipedia page, but the [German](https://de.wikipedia.org/wiki/Forbacher_Bahn) one included the above image by [Lantus](https://commons.wikimedia.org/wiki/File:Metz_Bahnhof.jpg?uselang=de) from April 2008. I'd love to go to that exact spot and take another photo now.

