---
title: "Targeting civilians with outdated maps"
date: "2022-10-14T15:52:17+11:00"
abstract: "A civilian from Odessa talks about outdated maps."
year: "2022"
category: Thoughts
tag:
- ethics
- ukraine
location: Sydney
---
The conventional wisdom among Westerners is that Russian military commanders are targeting civilian infrastructure to lower morale, for revenge, because their guidance systems are compromised as a result of sanctions limiting component supplies, and/or because they don't give a shit. I wouldn't pretend to know either way; nothing about this war makes sense to me, either for the innocent Ukranians being killed, or the Russians dying to protect the big ego of a small man. 

Roland Oliphant [talked with a local living in Odessa](https://youtu.be/y8KI_9ykDzg?t=1531) who had another theory:

> He said, look, I did my national service in 1994, and I've noticed that a lot of these [attacks] seem to land on places that used to belong to the military in Soviet times, or in the early nineties.
> 
> He talked about doing national service in Odessa, and he said that towards the beginning of the war, there was a particular strike that killed a little girl. And he said: "I can remember that thirty years ago, there was an air defence base there" [&hellip;] And his theory is they are chucking around missiles using extremely outdated maps.

It's the most charitable reason I've heard, though it'd still represent a callous disregard for human life. I'd damn well want my intelligence current before I started launching missiles at people who could be innocent civilians, no matter who the enemy is, or how justified I think I am for doing it. Anything less, and you're no better than the people you claim to be defending them from.

Russia's brass aren't the first sycophants to do this, and they sure as hell won't be the last. I just wish people would get a fucking clue for once. *You don't bring people on side by doing this.*
