---
title: "Our sites are now on FreeBSD 13.1-R"
date: "2022-06-12T09:28:50+10:00"
abstract: "If you’re reading this, the upgrade went well."
year: "2022"
category: Software
tag:
- bsd
- freebsd
location: Sydney
---
If the upgrade on my cloud instance went smoothly, you should be reading this. If not, you won't be reading this. Which means, who would I be talking to?

Hmm, that's a bit meagre for a blog post, even one that's an announcement of a job well done. Maybe it'd seem less pointless if I padded it out with a meandering paragraph of redundant prose that contains no meaningful substance whatsoever. But from which words would I construct such a literary device? And surely the modest, attractive, intelligent people who read this blog on a regular basis would see right through such an obvious charade? Fair call, I should probably avoid doing that.

Run FreeBSD!
