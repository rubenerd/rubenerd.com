---
title: "The Dassault Mercure could have succeeded today"
date: "2022-02-08T08:26:55+11:00"
abstract: "The economics didn’t make sense in the 1970s, but they do now."
thumb: "https://rubenerd.com/files/2022/photo-mercure@1x.jpg"
year: "2022"
category: Thoughts
tag:
- airbus
- aviation
- mercure
location: Sydney
---
I've been getting stuck into reading and watching videos about the Dassault Mercure again recently, and it's got me thinking whether the aircraft would have had more success being launched today.

The French Mercure entered service in the 1970s as a fuel efficient narrowbody optimised for short-haul routes. Its conventional appearance belied a surprising number of technical innovations, and its collaborative development helped set the scene for Airbus, one of the two big Western aircraft manufacturers.

Here's a [photo by ignis](https://commons.wikimedia.org/wiki/File:Dassault_Mercure_Le_Bourget_FRA_001.jpg) of one at the Museum of Air and Space in Paris. I happen to think its cockpit and lines look more futuristic and modern than its contemporaries, but that might just be me!

<p><img src="https://rubenerd.com/files/2022/photo-mercure@1x.jpg" srcset="https://rubenerd.com/files/2022/photo-mercure@1x.jpg 1x, https://rubenerd.com/files/2022/photo-mercure@2x.jpg 2x" alt="Photo of a parked Mercure in 2006 by ignis" style="width:500px; height:333px;" /></p>

Disappointingly for aviation fans, it didn't succeed in the market. I've seen the following reasons cited by historians:

* It's short range made sense in France and parts of Western Europe, but limited its utility across North America.

* Airlines with fleets of 737s and DC-9s were happy to trade fuel efficiency for flexibility. In a pinch, both American airframes could be redeployed for routes the Mercure wasn't designed for.

* Airlines couldn't justify spending extra on spare parts, expertise, and pilot training when they had existing narrowbody fleets.

* A falling US dollar made the aircraft comparatively more expensive.

Note that only half those reasons were a direct result of the Mercure's design. Like so many engineering stories, the outcome came down to economics. It's no good having an optimised, technically sophisticated aircraft if the financial model doesn't make sense for operators... even if you have the government backing your project.

But that was then. Airlines are operating under a very different environment than they were in the early 1970s, and part of me thinks an equivalent Mercure built with 2020s tech could succeed today.

Fuel is the biggest difference I can see. It wasn't a concern prior to the first energy crisis, but it commands a significant portion of the operating budgets of modern airline companies (especially those that aren't as shrewd as the likes of SIA with their fuel hedges). Airlines also have larger fleets today, so probably have the capacity and appetite to operate more aircraft types if they can save money in this key area.

This change has *kinda* happened. Airbus partnered with Bombardier to relaunch their CSeries jets as the A220. The A220 has much shorter range than narrowbodies like the re-engineered 737 or the A320, but it offers significant fuel savings. Sound familiar? This time, regional jets in North America and Europe have snapped them up, because the economics make sense.

But there are other considerations. High-speed rail, especially within France and Germany, have made the routes the Mercure was originally designed for obsolete. But budget airlines also exist now, and I could see a Mercure flying with the likes of AirAsia around Indonesia and Malaysia.

It seems to be now, with the incredible perceptive power of hindsight, that the Mercure was the right aircraft at the wrong time. I guess it always pays to be one step ahead, but not two.
