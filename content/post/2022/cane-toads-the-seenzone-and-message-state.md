---
title: "Cane toads, the “seenzone” and message state"
date: "2022-08-21T08:24:02+10:00"
abstract: "Read receipts were designed to solve a problem, but they introduced other ones."
year: "2020"
category: Software
tag:
- sociology
location: Sydney
---
I can't stop thinking about the phrase *seenzone* I learned today. It describes being told a recipient has read a message of yours, but has yet to respond. The phenomena plays into insecurities introduced by the widely implemented *read receipt* feature in modern chat applications. 

When you send a message to someone, most applications today make the following distinctions about its state:

* **Draft**. Alice wrote the message, but it hasn't been sent to Bob. She may have intended to, but she might have network issues.

* **Sent**. Alice sent the message, but it hasn't been acknowledged by Bob's device. It might be turned off, or Bob has network issues.

* **Delivered**. The message was acknowledged by Bob's device, and is sitting in an inbox or thread.

* **Read**. Bob has opened the chat application and seen the delivered message.

The specific wording in that last point is important. But we'll get back to that in a moment.

You can see why differentiating between these different states makes technical sense. Email is an example of a asynchronous protocol where you can never be sure about whether a person has received your message until they send you a response. In the meantime, you could be waiting indefinitely for a reply that never comes, because they either never received your message, or it went to spam.

In networking terms, read receipts establish human communication over something closer to TCP instead of UDP.

Except, this technocratic solution introduces a new social issue: the removal of plausible deniability. And it turns out, humans like having this.

I assert that email remains entrenched not just because it's an open protocol (funny how that's important!), but *because* it has indeterminate state. Attempts have been made to introduce "read receipts" for email, but they've never gone far because&mdash;and here's the kicker&mdash;people saw them as an invasion of privacy. Yet that's exactly what we're doing with modern chat applications.

To the last state above, a phone can't know:

* If you've read a message, only that its been **displayed** in the client application. That means the sender may be being given erroneous data.

* If you **comprehended** a message, because its not the same as reading it. How often have you stared blankly at text, or muddled with words in your head, without it being parsed?

* If you **wanted** to read the message, even if you did. For example, you might not be on call, yet there's now a record of you having read a work message, with the implicit assumption that you're now available to respond.

There are various ways around this, such as reading notifications but not the chat application itself. But that only reinforces my point that this feature comes with baggage.

The good news is, decent chat applications let you turn this feature off. But then, I've been interrogated for why I did this. Do I have something to hide? This is how the introduction of a new technical feature can fundamentally change the way human interactions and work are discussed and understood, and not always for the better.

I'm going to dub such technologies [cane toads](https://en.wikipedia.org/wiki/Cane_toads_in_Australia), because in an attempt to solve one problem, they introduce another. Modern IT has a habit of introducing features with, at best, a disregard for its potential sociological impact. The industry all too often assumes that the tech is neutral, or that its implementation can be detached from its real-world use. Wisdom comes from realising this isn't the case.

Some people see no problem with these features, others see it as an invasive species. It's understanding these differences that we can build IT and communications that respect people, rather than making blanket assumptions.

This ties into all sorts of interrelated issues of online tracking and privacy, but we'll leave it at that for now. I hope you enjoyed this post! I can't tell if you read it, but that's fine by me.
