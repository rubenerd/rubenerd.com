---
title: "Feedback on Vim plugin installs"
date: "2022-05-21T09:22:46+1000"
abstract: "Vim’s native plugin system, and the locking feature of vim-plug"
year: "2022"
category: Software
tag:
- feedback
- text-editors
- vim
location: Sydney
---
My [recent post about vim-plug](https://rubenerd.com/trying-vim-plug/) spawned a couple interesting discussions, including additional features and the need for plugin managers in light of Vim 8's native plugin support.

Andrii Lytvyn emailed:

> I’ve been using vim-plug for years now, but recently it came to my 
attention that starting from version 8 Vim (and Neovim) has it's own 
barebones package managing system. As such, now Vim autoloads any 
plugins that you put in the `~/.vim/pack/*/start/` folder.

Andrii automates this with a few additional lines in vimrc to detect and install plugins, but otherwise runs Vim completely stock. This appeals to me for situations where I don't have that many plugins.

[Ben Oliver](https://www.bfoliver.com) expands on one additional feature of vim-plug I didn't realise:

> The coolest thing about vim-plug (ok perhaps not cool) is that you can create 
a lockfile of all your plugins in their current state. So you run the install like you did already, then once you are happy everything is working:
> 
> <pre><code>:PlugSnapshot foo-bar.lock</code></pre>
>
> Then if an update happens and something breaks, you can roll it back with 
(from CLI not vim)
>
> <pre><code>vim -S foo-bar.lock</code></pre>
>
> It's also useful for deploying to multiple systems, instead of having 
different versions of plugins on different machines.
