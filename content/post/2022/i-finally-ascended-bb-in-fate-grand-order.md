---
title: "Finally ascending BB in Fate/Grand Order"
date: "2022-06-27T08:59:42+10:00"
abstract: "This took so many years."
thumb: "https://rubenerd.com/files/2022/bb-why@1x.jpg"
year: "2022"
category: Anime
tag:
- fate
- fate-grand-order
- games
- wada-arco
location: Sydney
---
There are so many anniversaries to keep track of this week, and I've let some important ones slip. I'm hoping to address all of them in the coming days.

Yesterday was the 5th anniversary of [Fate/Grand Order's English debut](https://fate-go.us/5th_anniversary/). I can still remember sitting outside the Kyoto Town Hall for the year's matsuri parade to start, and eagerly downloading it onto our phones. We'd downsized our apartment, got rid of most of our stuff, and were using the saved money each year to travel. It seems like an age ago now.

Truth be told, I haven't been playing as much  of late. It was the mobile game I played during my commute, which shrunk from five days a week, to nothing in The After Times, to two. But I was *finally* able to get BB's last ascension item after two years, and I'm unreasonably happy.

I'm sure you're all quivering in your fashionably long boots to find out why. If not, is a phrase with two words. Why am I writing about this?

<figure><p><img src="https://rubenerd.com/files/2022/bb-why@1x.jpg" alt="BB: Beats me! I have absolutely no idea!" style="width:500px; height:281px;" srcset="https://rubenerd.com/files/2022/bb-why@1x.jpg 1x, https://rubenerd.com/files/2022/bb-why@2x.jpg 2x" /></p></figure>

Wada Arco is an anime and mobile game illustrator most famous in recent times for her work in the Fate universe, and for designing our [latest time lord](https://rubenerd.com/the-ourokronii-power-washes-into-our-heart/) for Hololive EN's Council. She's one of Clara's and my favourites, and I'm still kicking myself for not getting one of her Fate/Notes last time I saw one for sale in a Mandarake in Osaka.

She also designed the three main characters in the [Abyssal Cyber Paradise, SE.RA.PH interlude]() in FGO. I was stoked to get a chance to collect them, but while I had success getting Melt during a summoning, I didn't finish the event in time, and I couldn't ascend her. You needed four [Nostalgic Ribbons](https://gamepress.gg/grandorder/item/nostalgic-ribbon), and I only managed to collect two before the event finished.

Does this sound French yet?

FGO tends to do reruns of events the following year, for new players and those who missed it the first time. Alas, I had a lot going on and was only able to get *one* more of her ascension ribbons during the SE.RA.PH rerun, leaving one of Wada Arco's best characters permanently stuck at level 60/80. It's a tough life.

Fast forward two years (I think?), and I saw the announcement that we were going to get a *third* attempt at the interlude! Parts of it were abridged, and the time to complete it was much shorter, meaning we had little time to lose if we wanted to get through the story, battles, and complicated item purchases to defeat the final boss.

I wasn't going to let this slip through my fingers yet again, so after a weekend punctured with family issues and a migraine, I was able to get in just enough to finish the main story, defeat the final boss, and collect *one* more of those infernal, **INFERNAL** Nostalgic Ribbons!

Her final ascension hit right in the feels, given just how long it took:

<figure><p><img src="https://rubenerd.com/files/2022/bb-final@1x.jpg" alt="BB: I pestered you so much, but for some reason you still stuck with me." style="width:500px; height:281px;" srcset="https://rubenerd.com/files/2022/bb-final@1x.jpg 1x, https://rubenerd.com/files/2022/bb-final@2x.jpg 2x" /></p></figure>

