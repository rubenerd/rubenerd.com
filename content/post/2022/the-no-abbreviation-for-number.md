---
title: "The No. abbreviation for number"
date: "2022-06-15T08:38:36+10:00"
abstract: "Learning where the O in No came from, and the Unicode ligature."
year: "2022"
category: Thoughts
tag:
- language
- typeography
location: Sydney
---
I was taught in school that *No.* was the abbreviation for number. I saw this used everywhere from tables of figures to [songs](https://en.wikipedia.org/wiki/Mambo_No._5). There's even a Unicode ligature *[&numero;](https://util.unicode.org/UnicodeJsps/character.jsp?a=2116)*.

But it only just occurred to me how nonsensical that appears. The word *number* doesn't contain the letter *O* anywhere. Where did it come from?

According to [Wikipedia](https://en.wikipedia.org/wiki/Numero_sign), &numero; is short for the Latin word *numero*, which means what it sounds like. This has carried through to other romance languages, and eventually to the literary sponge that is English.

The article also states that the pound sign *#* is more commonly used in North America, which explains a ton of examples in technical documentation that I've read of late. I might even prefer it; it's certainly more succinct.
