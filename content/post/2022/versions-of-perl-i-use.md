---
title: "Version of Perl I use, via @pinkopanterata"
date: "2022-03-22T09:38:24+11:00"
abstract: "I’m still on Perl 5, installed via regular package managers or Perlbrew."
year: "2022"
category: Software
tag:
- perl
location: Sydney
---
I mentioned on The Bird Site last night that I [still love Perl](https://twitter.com/Rubenerd/status/1505829198400999426), which prompted [@pinkopanterata to ask](https://twitter.com/Pinkopanterata/status/1505831626298777602):

> What version of Perl do you use?

Good question! I don't think I've ever mentioned this specifically, despite talking about camels here for years.

I'm still on Perl 5. I had fun exploring Raku, but Perl 5 still does everything I want, and uses that familiar syntax. I haven't touched Perl 7 yet, but that's on the personal project pile.

For the longest time I required Perl 5.16 as minimum in my scripts. This seminal version fixed a few longstanding Unicode bugs which made my life difficult in the past, and added a bunch of useful features like **\_\_SUB\_\_**. 5.16 incudes some hashing bugs though, so thesedays I set **use 5.018** at minimum.

In terms of installation, I tend to be conservative and run whatever package managers include. My tools and code tend to be self-contained; I'm not running massive projects that require specific versions from CPAN, for example. I've also run Perl from pkgsrc on Debian in the past, because I was already using it for other things.

That said, I use the excellent [Perlbrew](https://perlbrew.pl/) system on my personal machines because I always want to try the latest goodies.

