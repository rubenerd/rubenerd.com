---
title: "Black★Rock Shooter: Inexhaustible and Dawn Fall"
date: "2022-02-11T09:54:20+11:00"
abstract: "This is the most spectacular fig I’ve ever seen."
thumb: "https://rubenerd.com/files/2022/brs-anniversary-wings@1x.jpg"
year: "2022"
category: Anime
tag:
- black-rock-shooter
- figs
location: Sydney
---
Speaking of feeling old given the [time since something was released](https://rubenerd.com/expelled-from-paradise-seven-year-anniversary/), I saw *[Black★Rock Shooter](https://en.wikipedia.org/wiki/Black_Rock_Shooter)* has been around for a decade and a half now. 2007 was only a few years ago, right?

Anime News Network [summarised it](https://www.animenewsnetwork.com/news/2021-09-15/artist-huke-launches-new-black-rock-shooter-anime-project/.177418) well:

> The Black Rock Shooter franchise began with an illustration by huke of the eponymous character, which he posted on pixiv on December 26, 2007. The illustration inspired a song of the same title by supercell using the Hatsune Miku Vocaloid, and later spawned a 2010 original video anime (OVA) and a 2012 television anime, as well as spinoff manga, games, and figures depicting the character.

It's also set to get another anime in April, which was news to me:

> Illustrator huke revealed on Thursday that a new anime project for the *Black Rock Shooter* franchise is launching. The project is titled *Black Rock Shooter: Dawn Fall*.

The *B★RS* universe is so open, this new series could be about practically anything! Like *Expelled from Paradise*, the 2012 anime also used CGI in its fight scenes, which drew mixed reactions. I thought it was well executed for the time, and even stands up relatively well a decade later.

Speaking of standing up though, that's not the primary reason I bring this up. huke's decade anniversary art from five years ago (cries again in old man!) is getting possibly one of the most intricate figures I've ever seen! They've captured her expression and detail so well here, even down to the scuff marks on the paint in the upper-right:

<p><img src="https://rubenerd.com/files/2022/brs-anniversary-wings@1x.jpg" srcset="https://rubenerd.com/files/2022/brs-anniversary-wings@1x.jpg 1x, https://rubenerd.com/files/2022/brs-anniversary-wings@2x.jpg 2x" alt="Press photo of the fig against a dark background, showing her glossy black clothes, blue eyes, ponytail, and massive mecha-style wings." style="width:500px; height:375px;" /></p>

But this is the most amazing part: check out that base! This is less of an anime figure and more a table centrepiece:

<p><img src="https://rubenerd.com/files/2022/brs-anniversary-flame@1x.jpg" srcset="https://rubenerd.com/files/2022/brs-anniversary-flame@1x.jpg 1x, https://rubenerd.com/files/2022/brs-anniversary-flame@2x.jpg 2x" alt="Press photo of the base showing huge blue flames." style="width:420px; height:746px;" /></p>

From the [official minisite](https://special.goodsmile.info/brs_inexhaustible/en/):

> The base features a light source that acts as indirect lighting for the whole figure. The faint glow of the crystallized blue flames and her mechanical wings have been faithfully brought into figure form.

It says something when these kinds of collectables are even getting their own detailed websites, and even smoke machines for photography. Gone are the days of those marble paper lightboxes.
