---
title: "WHO claims Covid “end in sight”"
date: "2022-09-15T07:42:54+10:00"
abstract: "Some good news for this self-identified cautious optimist."
year: "2022"
category: Thoughts
tag:
- covid-19
- health
- news
location: Sydney
---
There's some good news for this self-identified cautious optimist, [via The Guardian](https://www.theguardian.com/world/2022/sep/14/end-of-covid-pandemic-in-sight-says-world-health-organization)\:

> The end of the Covid-19 pandemic is “in sight”, the World Health Organization has declared, after revealing that weekly deaths from the virus around the world were at the lowest level since March 2020.

It already seems like such a long time ago, but it really wasn't.
 
> The weekly global deaths figure on 5 September 2022 was 11,118, according to the WHO’s website. [...] The WHO also estimated that 19.8m deaths were averted in 2021 due to Covid-19 vaccines being administered and that 12bn doses had been given around the world.

It's still a global emergency, and we still need to take it seriously. We risk a relapse if we don't.

But it's also worth appreciating everyone who developed and received vaccines, not to mention the tireless efforts of all the medical, sanitation, and logistics workers around the world who kept us safe and staved off what could have been a far worse catastrophie. This is a huge achievement.

I also still hold out hope that accelerated RNA vaccine development will lead to further breakthroughs and lives saved. I hope malaria can be our next focus.

