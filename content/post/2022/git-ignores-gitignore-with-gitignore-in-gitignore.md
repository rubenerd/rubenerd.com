---
title: "Git ignores .gitignore with .gitignore in .gitignore"
date: "2022-05-18T12:31:11+10:00"
abstract: "A fun, if predictable and pointless exercise."
year: "2022"
category: Software
tag:
- experiments
- git
location: Sydney
---
I initialised a new Git repository, and added a `.gitignore` file instructing git to ignore `.gitignore`:

	git init .
	echo ".gitignore" > .gitignore
	git status

And the result:

	On branch trunk
	No commits yet
	nothing to commit (create/copy files and use "git add" to track)

Therefore, we can see Git ignores `.gitignore` because its in `.gitignore`, which means it didn't ignore `.gitignore` for instructions.

Is any of this informative, surprising, or useful? *... Absolutely.*

**Update:** Some Hacker News and Reddit readers have missed the silly tone or read this literally. This isn't surprising! But I'll endeavour to be a bit more overt with my nonsense in the future. Maybe I should have linked to yesterday's [Frog and Peach](https://rubenerd.com/pete-and-duds-frog-and-peach/) first.
