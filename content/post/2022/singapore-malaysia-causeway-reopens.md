---
title: "Singapore–Malaysia Causeway reopens"
date: "2022-04-02T09:40:09+11:00"
abstract: "It was a glimmer of wonderful news in a world that’s in sore need of it."
thumb: "https://rubenerd.com/files/2022/lionel-lim-causeway@1x.jpg"
year: "2022"
category: Travel
tag:
- johor
- malaysia
- nostalgia
- singapore
location: Sydney
---
The significance of this cannot be overstated, for people living either side of it!

The island of Singapore is separated from Malaysia by the Strait of Johor, a body of water wider than a river, but narrower than anything else. The British first build the Causeway connecting the two sides with road, rail, telegraph lines, and water pipes. It was destroyed during World War II in a failed attempt to thwart advancing Japanese forces, and since reconstruction has been a focal point of relations between the two sides. There's a *ton* of history behind this crossing, but I'll leave it at that!

People forget that Singapore was a part of Malaysia and its predecessor states. The original island was ceded to the British by the Sultan of Johor, and it briefly became part of the modern Malaysian state in the mid 20th century. Families can trace their histories both sides, and both counties share (at least in part) a linguistic, cultural, and culinary identity.

<p><img src="https://rubenerd.com/files/2022/lionel-lim-causeway@1x.jpg" srcset="https://rubenerd.com/files/2022/lionel-lim-causeway@1x.jpg 1x, https://rubenerd.com/files/2022/lionel-lim-causeway@2x.jpg 2x" alt="Photo of Johor in Malaysia at dusk from the Singapore side, with the Causeway in the middle." style="width:500px; height:333px;" /></p>

*Photo of the empty Causeway from the Singapore side [by Lionel Lim](https://commons.wikimedia.org/wiki/File:Empty_Singapore-Malaysia_Causeway.jpg).*

My economics professor once commented that Johor on the Malaysian side is the closest Singapore has to a hinterland. Workers cross in the thousands every day in trucks, cars, and on motorbikes. You can get the local SBS 170 bus across, and a Malaysian KTMB train. It was normal growing up there to tell my parents "I'm off to JB for the day", or to cross for events, school camps, and trade shows. The traffic situation on the causeway is regularly featured on radio and TV news alongside the Tuas Second Link.

The political situation between the two sides range from warm to tepid, depending on those in charge on either side. But in an alternative universe I can imagine Lee Kuan Yew giving a very different speech to the one he gave when Singapore was ejected from Malaysia. It's interesting to think the Causeway could have remained a state border had certain events played out differently.

The Causeway has only been closed a few times in its history, but the gates were lowered in 2020 as Singapore went into isolation to contain Covid. So it was a joy and relief to see my Twitter and RSS feeds flooded with grateful and excited people crossing again this week. It was a glimmer of wonderful news in a world that's in sore need of it.
