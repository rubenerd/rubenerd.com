---
title: "“Did you bully the weird kids?”"
date: "2022-04-09T17:06:40+10:00"
abstract: "Nope! And chances are, you didn’t either."
year: "2022"
category: Thoughts
tag:
- personal
- school
location: Sydney
---
I can't remember where I read that question, but it's stuck with me like so much electronic velcro.

No, I didn't bully the weird kids. Probably because I was one of them. I'll bet you answered the same if you read blogs like this!

Kids can be cruel. But even with all my family issues and anxiety, at no point did I consider it fun or socially expedient to bully, harass, intimidate, spread rumours, or otherwise make someone else feel miserable. Maybe because I knew how it felt being on the receiving end. Or I just wasn't a cruel kid, who knows? I was too busy being awkward!

Every now and then, a well-adjusted person I knew from my school days will cross my radar, and I'll remember how horrible they used to be. One of them emailed me a few years ago apologising for her abuse, which I appreciate wouldn't have been easy to do.

Being an adult makes a ton of things easier. It's amazing that anyone gets through school with all those pressures, hormones, and energy, let alone the demons everyone has in some form or another! The galaxy brain realisation is that *everyone* was the weird kid.
