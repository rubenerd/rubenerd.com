---
title: "Thanking Yee Cheng Chin for MacVim"
date: "2022-07-30T17:29:22+10:00"
abstract: "The apology in the latest release wasn’t necessary. We appreciate what he does for us."
year: "2022"
category: Software
tag:
- apple
- mac
- vim
location: Sydney
---
The [latest MacVim release notes](https://github.com/macvim-dev/macvim/releases) included this comment by its tireless maintainer and developer:

> MacVim is now updated to Vim 9!
>
> I just want to apologize for the infrequent update cadence for MacVim over the last year and a half. I have not had much time to work on MacVim, which made catching up harder, leading to a bad feedback loop. I would like to move back to a more regular release cadence to keep MacVim more updated relative to Vim, and to be able to start working on macOS 12/13 integrations as well as fixing outstanding bugs and merging the various pull requests that have been waiting to be merged for a while. Please don’t hesitate to poke on GitHub if MacVim starts falling behind again.

I could feel the worry reading this. Maintainers of all stripes have to put up with angry people who demand they fix and update software, often with no financial or development assistance. It sucks that good people are made to feel this way, especially those who develop open-source software.

No apologies necessary. Thank you for all you do for us.
