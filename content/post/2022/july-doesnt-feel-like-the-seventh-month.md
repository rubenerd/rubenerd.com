---
title: "July doesn’t feel like the seventh month"
date: "2022-07-05T09:19:34+1000"
abstract: "It's the sixth."
year: "2022"
category: Thoughts
tag:
- basic
- pointless
location: Sydney
---
I still have to count months sometimes. I know January is 01, December is 12, and May is 05, so I generally count forwards or backwards from those.

And for the record, is a phrase with four words. I don't think July feel like a 07. It should be 06. It's not far enough in the year to be 07. I'm not sure where that leaves June; maybe I conflate the two on account of their similar names.

Similarly, September feels more like an 08 than an 09. There's no way it's right before October. That makes no sense.

I *think* my brain implemented [Option Base 1](https://www.oreilly.com/library/view/vb-vba/1565923588/1565923588_ch07-1671-fm2xml.html) not 0, so that doesn't explain it. Maybe I'm just silly.
