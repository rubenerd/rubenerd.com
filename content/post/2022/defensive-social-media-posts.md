---
title: "Defensive social media posts"
date: "2022-02-19T08:29:27+11:00"
abstract: "Why do we need to do this still."
year: "2022"
category: Internet
tag:
- blogging
- feedback
location: Sydney
---
Last year I wrote about [defensive blogging](https://rubenerd.com/defensive-blogging/). I lamented the attitude of always reading things in bad faith, and the urge to jump on people and wear them down with rudeness and pedantic criticism.

[Rami Ismail](https://twitter.com/tha_rami)\:

> Twitter defensiveness is wanting to tweet "it's nice weather today" and tweeting "While I understand the implications of climate catastrophe & global warming, to me personally right here at the time of posting I personally feel the weather is quite good altho I recognize..

I didn't realise how much I'd internalised that too. Unless you're willing to only ever post bland observations, opinions, or ideas, you'll run into people who want to call you out. It's exhausting.

Frank Nora of The Overnightscape used to talk about *spentgags*, or knee-jerk jokes that everyone knows. I even think this applies to observations. Mention Seagate, and you'll get thousands of links to a Backblaze study.

I've learned the only "solution" here is to ignore, mute, and block. If you're able to do this effectively, *let me know how!*
