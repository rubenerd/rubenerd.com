---
title: "BioGraphics discusses Augustus"
date: "2022-02-05T09:21:35+11:00"
abstract: "I got into Simon Whistler videos for engineering content, but this channel is also excellent."
thumb: "https://rubenerd.com/files/2022/yt-Zw9le2od08k@1x.jpg"
year: "2022"
category: Media
tag:
- simon-whistler
- video
- youtube
location: Sydney
---
I got into Simon Whistler's videos for engineering, but I've started watching his [BioGraphics](https://www.youtube.com/channel/UClnDI2sdehVm1zm_LmUHsjQ) and [GeoGraphics](https://www.youtube.com/channel/UCHKRfxkMTqiiv4pF99qGKIw) channels recently too. This [recent video on Augustus](https://www.youtube.com/watch?v=Zw9le2od08k) was a real treat:

<p><a href="https://www.youtube.com/watch?v=Zw9le2od08k" title="Play Augustus: Rome’s Greatest Emperor"><img src="https://rubenerd.com/files/2022/yt-Zw9le2od08k@1x.jpg" srcset="https://rubenerd.com/files/2022/yt-Zw9le2od08k@1x.jpg 1x, https://rubenerd.com/files/2022/yt-Zw9le2od08k@2x.jpg 2x" alt="Play Augustus: Rome’s Greatest Emperor" style="width:500px;height:281px;" /></a></p>

I know double-length specials are a lot more work, but I think he and his writers really shine with the extra time.
