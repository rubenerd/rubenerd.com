---
title: "High idle power on AMD’s RDNA3 cards"
date: "2022-12-23T15:59:46+11:00"
abstract: "Driver updates are coming, but it’s still concerning how much more power they draw at idle, and in multi-monitor setups."
year: "2022"
category: Hardware
tag:
- amd
- gpus
- graphics
location: Sydney
---
Seeing the higher than expected power consumption on AMD's new Radeon RX 9700 XT(X) cards has been concerning. [Optimum Tech's](https://www.youtube.com/watch?v=tMH9vfvos00) most recent video demonstrates their new drivers clearly need of work, with higher idle and video power consumption than the Nvidia 4000 series.

[Andrew Cunningham reports](https://arstechnica.com/gadgets/2022/12/amd-acknowledges-high-power-use-in-rx-7900-gpus-plans-driver-fixes/?comments=1&comments-page=1) that driver updates are being written to address these issues:

> A driver update released earlier this week (version 22.12.2, the second driver release for the RX 7900-series in as many weeks) addresses high power use during video playback when using the cards' built-in media decoding hardware. Other problems have been acknowledged but not fixed yet, including "high idle power [...] when using select high resolution and high refresh rate displays."
>
> While AMD promised "further power efficiency improvements" for the GPUs in "future [driver] releases," the company didn't provide more information on what specific problems it was planning to fix or how much those fixes would help reduce power consumption. Our review found that the RX 7900 GPUs typically consume as much or slightly more power than Nvidia's competing RTX 4080 while gaming. However, the drivers seem to focus mostly on reducing power in cases where it's supposed to be low, like when it's decoding video or rendering the Windows desktop.

