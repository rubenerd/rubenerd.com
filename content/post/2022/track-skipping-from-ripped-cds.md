---
title: "Track skipping from ripped CDs"
date: "2022-04-01T10:36:27+11:00"
abstract: "Is it weird to keep these tracks around? Yes."
year: "2022"
category: Media
tag:
- audio
- itunes
- michael-franks
- mp3s
- music
- nostalgia
- winamp
location: Sydney
---
The title of this post sounds like someone from the 1990s observing VCR distortion, or a 1970s audiophile worried about tape tension on their reel-to-reel machine. How does a track skip? What's *ripping* a CD? How does doing the latter cause the former?

There are people using streaming services today who've never bought music, or ripped a CD, or browsed an HVM, Tower Records, or Sembawang Music. I've spilled enough electronic ink worrying about why that's a bad thing for artists, so in this case I merely bring it up as a generational observation. Ripping CDs must seem as foreign as faxing a document, tuning in at a specific time on the TV, or having digital privacy.

Wait, ouch.

Mixtapes were before my time, but we used to rip CDs to have a digital copy on our computers. We'd do it so we could import tracks into our Minidiscs, or media players such as the Creative Zen or iPod. Like tape decks of yore, we could also do this to make mix CDs for cars or CD Walkmans. If you had precious little drive space and two optical drives, software like Nero Burning ROM even supported live ripping and recording across discs, letting you swap out albums to make a mix without needing to rip them to your hard drive first. What a time to be alive!

I ripped all my Michael Franks CDs back in the mid-2000s as 320 kb/s CBR MP3, the [same format I use today](https://rubenerd.com/choosing-audio-codes/). Back then I was ripping to [whip a llama's arse](https://en.wikipedia.org/wiki/Winamp), play in iTunes, and transfer to my huge iPod Classic, so compatibility was more of a concern than capacity. They sound as clear and good now as they did back then, even with proper monitors and hi-fi speakers.

But in doing so, I introduced a few audio artefacts I still hear today. Michael's 1999 jazz/vocal album *Barefoot on the Beach* must have had a scratch sufficiently large to overwhelm the optical drive's error correction mechanisms, because in three tracks I hear a few pauses and doubling-up of sounds. My dad and I used to joke they were "bad-rip remixes".

They're *almost* imperceptible, to the point where you might miss them if you weren't paying close attention, and if you didn't know the songs. Because they skip at *just* the right time in the song, they sound like a DJ having a bit of fun with a light touch, not an error.

I've been tempted to re-rip these tracks again with another CD copy of the album, but the LP version of the album already sounds weird *because* it doesn't have those distortions! It's as though I have an exclusive, limited-edition version of these tracks.

In the paraphrased words of Dr Glenn Pierce from the game *Superliminal*, is that weird? Absolutely.
