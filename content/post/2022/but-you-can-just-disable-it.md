---
title: "“But you can just disable it!”"
date: "2022-03-21T15:59:34+11:00"
abstract: "Or: I could, but that's beside the point!"
year: "2022"
category: Software
tag:
- social-media
location: Sydney
---
*Or: I could, but that's beside the point!*

Mike Larkin of OpenBSD hypervisor fame is a lovely person. We shared a lift on the way back from an AsiaBSDCon talk in 2018, and he went out his way to welcome me, ask me what my interests are, and even offered some advice on the best beer around our hotel. I'm not sure if he remembers the exchange, but for an introvert who felt nervous and a bit out of place, he helped me out a lot.

But I digress! Today he tweeted about a feature of a certain Redmond OS, with the [following disclaimer](https://twitter.com/mlarkin2012/status/1505725542003183620) shortly after:

> Yes, I know I can go edit the registry and fix this [UI regression]. But ... why?

This struck me as a perfect example of [defensive posting](https://rubenerd.com/defensive-social-media-posts/) and its [chilling effects](https://rubenerd.com/feedback-on-defensive-social-media-posts/). Mike Harley of [Obsolete29](https://obsolete29.com/) brilliantly characterises these as "pre-buttals", or rebuttals to points you know are forthcoming.

It's the same playbook every time. Critique or share frustration about a software change, or protocol, or piece of hardware, and you're guaranteed to be inundated with replies about disabling, fixing, or mitigating, instead of addressing your point about utility, accessibility, or design. For each person who offers genuine help, as many are in it for [Internet points](https://meta.wikimedia.org/wiki/Cunningham%27s_Law "Cunningham's Law") from my experience.

@ch1ps0h0y (still one of the best handles ever) gave a [great example](https://twitter.com/ch1ps0h0y/status/1505762511781793792)\:

> Ah, this reminds me of a particular fight in ffxiv which uses orange danger markers on an orange field w/ orange BG. Instead of collectively calling for adjustments to base colours, ppl kept insisting other's monitors just needed adjusting.
>
> As if one developer making a single change is somehow harder than 100,000s of people changing display settings for a single fight.

I'm waiting for the next phone to come with a hidden *punch* feature that supporters will claim is fine because "you can just disable it" when you complain of a dislocated jaw. Maybe I should let them borrow it!
