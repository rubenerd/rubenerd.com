---
title: "Wordle for 2020"
date: "2021-01-01T09:12:00+11:00"
abstract: "Based on headings for posts over the last year"
year: "2021"
category: Internet
tag:
- wordle
- weblog
---
I used to do annual Worldles as a fun way to visualise what I'd talked about the previous twelve months; that being the typical duration of a year. Here they were for [2008](https://rubenerd.com/p3616/), [2009](https://rubenerd.com/2009-wordle/), [2010](https://rubenerd.com/goodbye-2010/), [2011](https://rubenerd.com/2011-wordle/), and [2012](https://rubenerd.com/2012-wordle/).

Alas, while the [Wordle.net blog](http://blog.wordle.net/) is still around, the original site is not. But the Wordart.com site still lets you import text and generate some silly fun, and it ended up with something far more detailed:

<img src="https://rubenerd.com/files/2021/worldle-2021@1x.jpg" srcset="https://rubenerd.com/files/2021/worldle-2021@2x.jpg 2x" alt="Word cloud from post titles over the last twelve months" />

FreeBSD didn't come as a surprise, nor did my renewed interest and advocacy for RSS. I talked about music more than I expected, and I'm pleased to see that virus didn't get more of a mention. Lots of new stuff too, it seems.
