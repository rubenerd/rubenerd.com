---
title: "Trying @geofftech's challenge in Sydney!"
date: "2021-12-18T21:44:04+10:00"
abstract: "I think you’d need to start in Kingsgrove and end in Hazelbrook, a 153 minute trip!"
year: "2021"
category: Travel
tag:
- geoff-marshall
- trains
- sydney
- youtube
location: Sydney
---
Geoff Marshall did a fun video where he tried to take the shortest London Tube journey that ticks off the alphabet:

<p><a href="https://www.youtube.com/watch?v=rpY8Pi5aJ9k" title="Play The Shortest Tube Journey that Ticks off the Alphabet"><img src="https://rubenerd.com/files/2021/yt-rpY8Pi5aJ9k@1x.jpg" srcset="https://rubenerd.com/files/2021/yt-rpY8Pi5aJ9k@1x.jpg 1x, https://rubenerd.com/files/2021/yt-rpY8Pi5aJ9k@2x.jpg 2x" alt="Play The Shortest Tube Journey that Ticks off the Alphabet" style="width:500px;height:281px;" /></a></p>

Clara and I thought we'd plan out such a trip for Sydney.

Bexley North is the only station with an X, so we'd start there. Conveniently, these are all clustered together on the [Airport and South Line](https://en.wikipedia.org/wiki/Airport_%26_South_Line) line:

* [Kingsgrove]\: G K L N O R S V
* [Bexley North]\: B E L T X Y
* [Bardwell Park]\: A D P W
* [Green Square]\: Q U
* [St James]\: J
* [Central]\: C

Then you'd have to change at Central for a *long* trip on the [Blue Mountains Line](https://en.wikipedia.org/wiki/Blue_Mountains_Line) to get to Hazelbrook, the only station with a Z:

* [Redfern]\: F
* [Hazelbrook]\: Z

According to Sydney TripView, the total time would be 153 minutes. We might need to give it a try... maybe with a good book in between!

[Kingsgrove]: https://en.wikipedia.org/wiki/Kingsgrove_railway_station
[Bexley North]: https://en.wikipedia.org/wiki/Bexley_North_railway_station
[Bardwell Park]: https://en.wikipedia.org/wiki/Bardwell_Park_railway_station
[Green Square]: https://en.wikipedia.org/wiki/Green_Square_railway_station
[St James]: https://en.wikipedia.org/wiki/St_James_railway_station,_Sydney
[Central]: https://en.wikipedia.org/wiki/Central_railway_station,_Sydney
[Redfern]: https://en.wikipedia.org/wiki/Redfern_railway_station
[Hazelbrook]: https://en.wikipedia.org/wiki/Hazelbrook_railway_station

