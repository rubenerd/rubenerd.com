---
title: "Nuts to that last weekend!"
date: "2021-08-23T08:30:21+10:00"
abstract: "If you laugh really hard, your neighbour might DOOF stop their DOOF!"
year: "2021"
category: Thoughts
tag:
- health
- personal
location: Sydney
---
Last weekend was a bit on the nose. In no particular order:

1. Sydney entered its 87th month of lockdown&mdash;or maybe it just feels like it&mdash;at the same time we passed more than 800 daily Delta cases. That's not high for a city of five million, but it doesn't want to go down.

2. Being stuck at home lead me to working and sitting outside on the balcony for a semblance of fresh air. It also helps with headaches. But backburning bushfire smoke was so thick we couldn't even open the windows the entire time.

3. Our lone apartment bathroom is currently inoperable owing to a plumbing issue which has lead to... complications. Getting it fixed has also been an ongoing disaster. We've been using the shared bathroom downstairs which, given point 1, isn't ideal.

4. The neighbors got new **DOOF** speakers and **DOOF** proceeded to **DOOF** start testing **DOOF** them from **DOOF** early in **DOOF** the morning to late at night. I couldn't leave, I couldn't go on the balcony, and I couldn't **DOOF** sit inside without going around the proverbial **DOOF**.

5. A mild case of bad food lead Clara and I to have stomach aches and associated fun for much of yesterday which, given point 3, isn't ideal.

6. Another tech journalist I used to respect, and who's podcast was among the first I listened to back in 2004, came out as a cryptocurrency fan.

There I was, wallowing in tragic self pity. Then I get a message that a colleague and friend had to have emergency surgery, and another friend in Indonesia just lost two more of his family to Covid. I went out onto the balcony, smothered my headached brow with Tiger Balm, breathed some bushfire smoke, clutched my stomach, drilled some neighbourly dubstep into my ears, and thought *hey, it could be worse!*

I brushed myself off, sneezed, tripped over one of our outdoor plants, and laughed like nobody was listening. Curiously, the music stopped.

