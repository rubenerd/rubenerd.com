---
title: "The SS Golden Eagle"
date: "2021-02-20T17:25:34+11:00"
abstract: "This little ship had a long and important career."
thumb: "https://rubenerd.com/files/2021/ss-golden-eagle@1x.jpg"
year: "2021"
category: Media
tag:
- photos
- ships
- wikipedia
location: Sydney
---
Speaking of the *[SS Martin Mullen](https://rubenerd.com/the-ss-martin-mullen/)*, I saw that Wikipedia recently featured an article about the *SS Golden Eagle*, launched as the *SS Mauna Loa* in 1919. [This photo](https://commons.wikimedia.org/wiki/File:SS_Golden_Eagle_in_Vancouver.jpg) by Walter E. Frost was taken in 1932.

<p><img src="https://rubenerd.com/files/2021/ss-golden-eagle@1x.jpg" srcset="https://rubenerd.com/files/2021/ss-golden-eagle@1x.jpg 1x, https://rubenerd.com/files/2021/ss-golden-eagle@2x.jpg 2x" alt="Photo of the SS Golden Eagle in 1932." style="width:500px" /></p>

This little ship had a long career spanning troop activities to the Philippines during World War I, to shipping pineapples. It's [worth a read](https://en.wikipedia.org/wiki/SS_Mauna_Loa).

