---
title: "What’s a manufear?"
date: "2021-05-15T10:33:46+10:00"
abstract: "A typo on a previous post was pretty funny."
year: "2021"
category: Thoughts
tag:
- typos
location: Sydney
---
I wrote this on a post yesterday about [annoying TV tropes](https://rubenerd.com/your-most-annoying-things-about-tv/)\:

> It’d also get me that they’d bring the cup to their mouth to drink without looking at it. Maybe some people are able to pull off this **manufear** without making a mess, but I sure can’t. It looks unnatural and weird.

You know what else looks unnatural and weird? *Manufear!* What is that word? What does it mean? Or more to the point, where did it come from? I assumed I wrote the word *maneuver*, but this clearly isn't. My brain must have been in a weird place when I typed that out.

I do like it though. It sounds like an abbreviation of *manufactured fear*. Like a TV actor effortlessly drinking coffee from an empty cup.

