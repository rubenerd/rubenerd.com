---
title: "A simple bar of soap"
date: "2021-05-16T09:41:33+10:00"
abstract: "Moving on from wasteful and irrelevant liquid soap."
year: "2021"
category: Thoughts
tag:
- covid-19
- health
- shopping
location: Sydney
---
Happy Sunday! Soap. It's a [access protocol](https://rubenerd.com/remember-soap/); it's used as an industrial lubricant; and some of us even use it for washing hands. It's to our collective benefit that it's so ubiquitous and widely used, save for one unfortunate twist.

Clara and I have always erred on the side of being clean freaks, but it wasn't until Covid that we really started taking stock of how much soap we use in a given month. Not necessarily in cost or supermarket trips, but in the volume of empty plastic bottles it generated.

I try to do the right thing when it comes to rubbish, so I made it a habit of removing the pump spout and thoroughly washing the container so it could be recycled. But then I started taking a closer look at the complicated mechanisms and mixes of plastic, and wondering just how feasible recycling this contraption was. If recycling is at all complicated by something, you know it's been thrown away instead. I thought back to all the bottles I've bought over the years and blanched.

Seeking a *solution* (heh) to this problem, Clara and I first went back to the supermarket to check out refills instead of buying new pump packs each time. They exist, but come in plastic bottles themselves. It reduces the amount the waste by refilling three to four bottles without shipping more of those non-recyclable pump mechanisms. But the irony wasn't lost on us that it's still more plastic.

*Turns out*, there's been a solution available this whole time. Bars of soap are cheap, easy to use, and often come in little cardboard boxes. We bought a cute little pair of draining soap dishes from Muji, and have been using a specific moisturising soap on our hands and bodies ever since.

There's something satisfying, and almost meditative, about working up a bar of soap into a lather in your hands. You're not pushing a button and moving on, you're taking deliberate time out to take care of yourself. It's almost like working clay, only the end result is a lowered risk of getting sick.

Maybe it's another manifestation of my need for more tactile things of late, like LPs and cassettes for music. Only this one is *useful!*

