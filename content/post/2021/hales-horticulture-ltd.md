---
title: "Hales Horticulture Ltd."
date: "2021-12-22T09:04:23+11:00"
abstract: "Feedback about plants growing in the shadow of road infrastructure."
year: "2021"
category: Thoughts
tag:
- environment
- nature
- plants
location: Sydney
---
Another helpful and fun email from [Hales of Haelstrom](http://halestrom.net/darksleep/), this time regarding my comment about [plants growing in the shadow](https://rubenerd.com/proposed-coventry-linear-park/) of road infrastructure:

> Yes there are a lot of plants that prefer indirect light and will die if put in direct sunlight.  Many of them evolved in a "forest floor" environment, go on a bushwalk in a foresty bit and you will see dark, broad-leafed plants everywhere.
>
> Off the top of my head:
>
> * (all?) ferns and treeferns.  We've lost some at my place from sun exposure.  They get "burned" and die back when blazed.  Our tree ferns have limited lifespans (~10 years?) because they grow too tall, then we can't fit the shade sails over them in summer.
>
> * Elephant ear.  Grows crazy fast and like mad inside the house.  Ours is more than 2m and dribbles nectar on us as a sign of affection.  Keeps turning its leaves towards the window nearby, greedy and possibly haunted.
>
> * Lots of climbing plants (these go up the trunks of trees in the forest until they get to the top; so they have to tolerate everything from dim to full sunlight)
>
> In your illustration they use climbers on the pillars and possibly some more normal tress (hopefully shurb/height limited!) in the light gap (centre).  Their "grasses" look a lot like weeds :P but of course that depends on what's endemic to the region.  Good choices will provide some green all year round, bad choices will get replaced by weeds that are better adapted.

I'll admit that I now understand why I killed two houseplants that I put on our balcony to get a bit of greenery during the first Covid lockdowns. Our old balcony downstairs had a roof that shaded them, but our new one doesn't. I read this email from Hales a week ago and moved them back inside, and the happy plant especially is doing much better already.

I'm sure *Hales Horticulture* will add that to my ever-growing bill, or to the piles of coffee or other beverages I owe him when we meet one day.
