---
title: "Covid lockdown decluttering"
date: "2021-08-15T09:01:47+10:00"
abstract: "Made some tough decisions and have 14 bags of rubbish to show for it!"
year: "2021"
category: Thoughts
tag:
- decluttering
- marie-kondo
- mental-health
location: Sydney
---
We're into week `$whatever` of Sydney Delta-strain Covid lockdown, I've lost track. Clara's and my offices mandated we worked from home a few weeks before the official edict came down from the state government, so we're been looking at these walls for months now. I will be shocked if we're out by Christmas.

Something had to give yesterday, and this time it was a hunt for an external hard drive. I realised the plastic tub I found it in consisted almost entirely of junk. By the end of the day, Clara and I had gone through every box we had stored in the bedroom, and had at least 14 rubbish bags of stuff to show for it. We finally got around to making some hard decisions about stuff we've been carting around for years, which felt like a literal and figurative weight off our shoulders. Clothes we hadn't worn for years went to the fabric recycling box downstairs, and I now have another "outbox" of dead or broken computer parts to take to electronics recycling when we're allowed to make the trip.

*(As an aside, someone across the street started practicing their violin on the balcony, as I sit on ours writing this post. She's *good*. That's made my morning).*

Long term readers know of my aversion to *stuff*. My parents were collectors and hoarded *tons* of stuff. We [moved houses](https://rubenerd.com/unpacking-singapore-day-one/) regularly owing to my dad's job, but we even moved within the same city to get a bigger place to store more stuff. My mum's deteriorating health meant she was literally in lockdown between hospital visits for years, so I don't fault her for wanting to have more things that made her happy. Professional help lead me to realise it was a source of anxiety that continues today; I felt mentally burdened by physical clutter which I'm only beginning to process now.

That went dark quick! My point is, since [at least 2016](https://rubenerd.com/goodbye-junk/) I've been trying to be deliberate about what I buy and bring into our home. We obsessively research and plan everything, often times deciding it's not worth it. This has the upshot of saving a ton of money on pointless things we either don't need or that doesn't make us happy, meaning what we *do* bring it can be appreciated.

Taking one example, do you know those sewing boxes that let you organise threads into little compartments? I had five of these things, full to the gills of random trinkets. Rather than deciding what I wanted to throw away or recycle, I started by picking my favourites from each box. After a period of wittling, I have a single box full of stuff I can point to as being significant, beautiful, and/or awesome.

Today Clara and I are going to tackle the rest of the apartment and the proverbal cute elephants in the room: *anime figures*. I had already sold half of them a couple of years ago (for a shocking profit!), but I can definitely reduce them down further to ones I really care about. I might even list some of them here to give to readers in Sydney if there's anyone people want.

It's edgy to dismiss [Marie Kondo](https://rubenerd.com/marie-kondo-would-say-this-kicks-arse/) and people like The Minimalists, but their messages about care and attention strongly resonate with me. I haven't felt this good about our circumstances for months, and we've only just started. Turns out doing something proctive and healthy is better than sitting on my arse during lockdown.

*(Maybe healthy is too strong a term, given the dust we kicked up. Good thing we have air cleaners)!*

