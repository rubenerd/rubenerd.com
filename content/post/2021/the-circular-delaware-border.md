---
title: "The circular Delaware border"
date: "2021-01-29T09:06:51+11:00"
abstract: "Today I learned it exists, and it’s fascinating."
thumb: "https://rubenerd.com/files/2021/delaware@1x.png"
year: "2021"
category: Travel
tag:
- jim-kloss
- penn
- philadelphia
location: Sydney
---
I've talked about my childhood fascination with maps and geography as a kid here many times. [During my most recent leave](https://rubenerd.com/our-belated-leave/) I sat on the couch and fired up OpenStreetMap on my iPad, just like I used to do with my 1986 atlas when I was a kid. People say the iPad isn't *just* a consumption device, but it's surely the best one I've ever used for doing so!

I decided to stalk [Jim Kloss](http://wholewheatradio.org/) from afar one evening, and to trace all the cool places he and [Esther Golton](https://esthergolton.bandcamp.com/) graciously showed us in Philly. We were only there for a few days, but it's one of Clara's and my favourite US cities. Boston is the next major one on our list, but I'd also love to venture over to Pittsburgh one day too.

By chance I drifted a little further south and came across the Pennsylvania state border with Delaware. I knew it was one of the smallest US states in population and size, but it's *tiny*. My famously microscopic home town of Singapore is 14% its size, and Delaware isn't even an island! And check out that northern border:

<p><a title="View Delaware on OpenStreetMap" href="https://www.openstreetmap.org/relation/162110"><img src="https://rubenerd.com/files/2021/delaware@1x.png" srcset="https://rubenerd.com/files/2021/delaware@1x.png 1x, https://rubenerd.com/files/2021/delaware@2x.png 2x" alt="View of the Delaware border from OpenStreetMap" style="width:500px" /></a></p>

The arc is known as the [Twelve-Mile Circle](https://en.wikipedia.org/wiki/Twelve-Mile_Circle) according to The Wikipedias, which would equate to the Nineteen Point Three One-Kilometre Circle in metric. It was roughly centred on the courthouse in New Castle. I almost wrote that as *scented*, which perhaps with a strong breeze could travel to all corners of the arc from the aforementioned structure.

If the border between Maryland and Pennsylvania continued straight across, Wilmington would be in the latter state. Human borders can be so arbitrary and weird!

