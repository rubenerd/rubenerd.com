---
title: "But I’ll just replace them, rent free!"
date: "2021-03-22T16:15:00+11:00"
abstract: "These thought-terminating cliches are weird."
year: "2021"
category: Thoughts
tag:
- economics
- philosophy
- politics
---
There are some *weird* rhetorts to online discussions that transcend political, economic, social, and logical bounds! Richard Dawkins; the scientist who coined *meme*; referred to these as *thought-terminating clich&eacute;s* owing to their overuse and ability to end discussions unproductively. Like my public sneezing fits, which have only become more contentious in public during these troubled times. 

Have you been accused of thinking about someone so much, you're letting them live, rent free, in your head? It's such a fascinating deviation from physics that it bends my mind. Which it may need to if we want to let an entire other human not only fit within our cranial spaces, but sufficient furnishings for a comfortable lodging and subsequent rental. Maybe we're letting the space gratis to afford us a little leeway in what must be an uncomfortable seating arrangement for everyone involved. How one is supposed to challenge an individual's perspective *without* thinking about them at all is equally perplexing, and would only be made harder by them physically residing with their credenzas and kitchen appliances between your ears, in place of your brain, in the interest of saving precious space.

What about when you're told by someone, gleefully, that that they'll replace you if you boycott or stop buying a product? One would think a merchant would prefer *both* of you to be customers to grow their business, but hey, I'm no entreprenurial mathemagician.

What about being told that you've misundertood a phrase as a premise to refuting a point, only they've clearly read the same dictionary. *The phrase survellience is problematic because it implies watching people without their knowledge which, granted this business does, but it's different and therefore can't be called that!* It's comforting to pretend nomenclature is the root of all these issues, but I have a nagging voice in my head that it isn't. Or that might just be Jeff, who lodges there on Wednesdays.

In the paraphrased words of the *Sunscreen Song*, don't be reckless with other people's logic centres, and don't put up with people who are reckless with yours. They might end up not buying your product, which means you'll lose potential business even if someone else offers to buy it instead.
