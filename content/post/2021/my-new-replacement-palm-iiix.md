---
title: "My new (replacement!) Palm IIIx"
date: "2021-05-06T20:47:35+10:00"
abstract: "This was my first portable computer. I’m so happy to have one again!"
thumb: ""
year: "2021"
category: Hardware
tag:
- nostalgia
- palm
- palmos
- palm-iiix
location: Sydney
---
Last week I talked about my love of [old Palm gear](https://rubenerd.com/palm-pilot-nostalgia-on-its-way/) from the 1990s and early 2000s, receiving a [LifeDrive from eBay](https://rubenerd.com/my-palm-lifedrive/), and getting USB passthrough working so I could [HotSync on FreeBSD](https://rubenerd.com/palm-lifedrive-passthru-in-windows-2000-qemu-vm/). It's been so much fun reliving the glory days of not only my favourite mobile computing platform, but among what I consider to be the best ever developed. Our modern smartphones can do so much more, but their UIs and hardware are a far cry in usability, ergonomics, design, practicality, and efficiency to what we once had.

Today I add another chapter to this Palm nostalgia series, having just received a replacement Palm IIIx from eBay for less than $30! Here she is, with her supplied docking cradle, and the same Pentium 1 machine that my original IIIx would have plugged into back in the day:

<p><img src="https://rubenerd.com/files/2021/photo-palmiiix@1x.jpg" srcset="https://rubenerd.com/files/2021/photo-palmiiix@1x.jpg 1x, https://rubenerd.com/files/2021/photo-palmiiix@2x.jpg 2x" alt="Photo of my Palm IIIx, with a Palm LifeDrive to the right, my Pentium tower from my childhood behind it, and a Commodore 128 off to the side." style="width:500px; height:375px;" /></p>

My [Palm nostalgia post](https://rubenerd.com/palm-pilot-nostalgia-on-its-way/) detailed my history with the IIIx, but in a nutshell it was my first mobile computing device. My parents bought me one for Christmas when I was in primary school, after I watched in awe at the Graffiti writing system being demoed at an IT show in Singapore. I carried it around with me for *years*, and long after colour devices with better CPUs and more modern features came around. It was a durable, quirky, fun little computer; something I think Palm's marketing department missed out on in their rush to brand it as a PDA for business users.

The Palm IIIx was a revision on the Palm III, itself an evolution of the PalmPilot and Pilot from the 1990s. It shared the same monochrome screen resolution and size, but was much easier to read thanks to an ingenious new inverted backlight that highlighted text in the dark. It also came with 4 MiB of memory; twice that of the Palm III. Palm software was tiny, so you could stretch that capacity pretty far.

But that wasn't the whole story. Years later I went to uni in Adelaide, and a room mate in my dorm hadn't ever seen a Palm before. He took a keen interest in how it worked, and took it upon himself to borrow it for longer and longer stretches of time. I thought it was a bit odd, but then, we were a dorm of computer nerds. By the time we moved out, he conveniently lost it, and I never saw it again. I wonder if he still has it in a drawer years later, or if he threw it away?

My aim is to eventually add Windows 2000 to this Pentium 1 tower so I can sync my LifeDrive with it over USB, and Windows 95 for this Palm IIIx over serial. But I also have an original Palm USB 1.1 to serial converter, so I might try pushing my luck further and seeing if USB passthrough to a QEMU VM on FreeBSD works for this too. Stay tuned!
