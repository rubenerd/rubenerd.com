---
title: "Trying Perl signatures"
date: "2021-05-21T09:11:37+10:00"
abstract: "sub sumificate($x,$y) { }"
year: "2021"
category: Software
tag:
- perl
- testing
location: Sydney
---
Speaking of [signatures](https://rubenerd.com/named-arguments-in-swift/), Perl 5.20 introduced the concept in an [experimental feature](https://perldoc.perl.org/feature#The-'signatures'-feature) in 2015, and I'm only just getting around to trying it. Prior we would have done this:

    sub sumificate {
        my ($first, $second) = @_;
        return $first + $second;
    }

Or this for a single parameter:

    sub printificate {
        my $message = shift;
        return $message;
    }

Now we can do this:

    use feature qw(say signatures);
    no warnings qw(experimental::signatures);    
        
    sub sumificate($first, $second) {
        return $first + $second;
    }

This is compatible with `use strict`, even without `my` on the parameters which is a nice shorthand. The script is [in my lunchbox](https://codeberg.org/rubenerd/lunchbox/signatures.pl) if you want to give it a try.

