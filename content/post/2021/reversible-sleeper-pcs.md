---
title: "Thinking about a reversible sleeper PC"
date: "2021-08-14T08:06:48+10:00"
abstract: "The fans probably don’t make this possible or easy, but hopefully I can do it with little cosmetic impact."
thumb: "https://rubenerd.com/files/2021/retro-setup@1x.jpg"
year: "2021"
category: Thoughts
tag:
- sleeper-pc
- compaq-spaceship
location: Sydney
---
I wrote last Saturday about my [sleeper PC curse](https://rubenerd.com/my-sleeper-pc-curse/), in which I buy dead old computers to use their cases for nostalgia, but accidently fix them in the process. It happened a third time now with the old Compaq Presario I want to turn into my new FreeBSD workstation, only this time I'm going ahead with the build given the Compaq has very similar specs to my existing retro P1 tower.

It's reframed how I tackle the project. I'm motivated to modify the case as little as possible given I know it all works, just on the off chance that I might want to restore it in the future. I bought a couple more anti-static bags to store the motherboard, cables, and power supply, which will be stashed along with my precious spare Commodore computer parts.

This impacts my plans for cooling and mounting components. Originally I wanted to make only minimal cosmetic changes, and a few holes in the back and inside of the chassis for cooling fans. Now I'm wanting whatever modification I do to be reversible, which is an *entirely* different kettle of fish!

<p><img src="https://rubenerd.com/files/2021/retro-setup@1x.jpg" srcset="https://rubenerd.com/files/2021/retro-setup@1x.jpg 1x, https://rubenerd.com/files/2021/retro-setup@2x.jpg 2x" alt="View of my current home retro setup with my childhood Pentium 1, a PAL Commodore 128, and now a Compaq Presario desktop from 1998" style="width:500px; height:333px;" /></p>

Certain things will need to be added regardless:

* MiniITX boards have different mount points than the proprietary Compaq board, so I'll be drilling standoffs into the motherboard tray. They won't be visibile when a board is in the case, so I'm not fussed.

* The front power switch and indicator LEDs will need to be replaced with a standard ATX connector. They're mounted on a detachable daugherboard, which I'll remove and build my own replacement for.

With hindsight I realise the above photo isn't great for illustrating this, but if you see at the base of the case (Ace of Base?) I removed a small plastic bezel which is sitting next to my spectacular DB9 hamburger mouse! I figured I could use the empty space for extra ventilation, and use negative pressure with fans on the back of the case to force air through. But there's a solid sheet of metal between it and the inside of the case which I'd need to drill out. There's an internal 6cm fan mounted in the front which could be replaced with some Noctua kit, but that wouldn't provide nearly enough air volume by itself.

*(I wonder if I could build a box to make a Microsoft Mouse-compatible serial DB9 device talk to the Commodore 128 sitting under the monitor stand for GEOS. Then I could add it to my KVM! Talk about an esoteric use case).*

Speaking of drilling out, my original plan was to drill a space to fit a GPU, given the chassis is about one centimetre too narrow internally to accomodate a standard-length card (of course)! There's a lot of empty space between where the metal chassis ends and the curvy plastic bezel begins, so it could easily accomodate. Now I'm thinking I'll just get a low-profile GTX 1660 or 1660S which should be more than capable of driving my limited selection of world sim games.

*(I'd love to go AMD, but classic Train Simulator chokes on ATI/AMD for some reason, and [Nvidia's binary FreeBSD drivers](https://rubenerd.com/freebsd-12-1-nvidia-desktop/) are far superior).*

As with the fan above, I will need to drill out some space in the back of the case for a fan; I don't think I have as much of a choice there. I'm going to convince myself I'm doing it so even if I put the old parts back in, they'll have greater longevity with better cooling. Yes.

Overall, is something you wear. I actually think I'll need to change this case less than I thought, which is good. You know what they say about plans though, so watch this space.

