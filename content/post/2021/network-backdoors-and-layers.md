---
title: "Network backdoors and layers"
date: "2021-01-04T08:42:00+11:00"
abstract: "ZyXEL routers and mandated back doors"
year: "2021"
category: Internet
tag:
- security
---
There's a truism in computer security that an undocumented backdoor for a small group of people is a front door for everyone. It **will** be found and exploited, regardless of how sinister or noble the intentions of the designers were. It's one of the reasons people like me get so up in arms about legal requirements for backdoors; an attacker doesn't care if a security hole was deliberate or accidental. And let's be clear: there is no working around the fact these are security holes, legally mandated or otherwise.

I was reminded of this again last week when [Lawrence Abrams reported](https://www.bleepingcomputer.com/news/security/secret-backdoor-discovered-in-zyxel-firewall-and-ap-controllers/) on Dutch security firm [EYE discovering](https://www.eyecontrol.nl/blog/undocumented-user-account-in-zyxel-products.html) a back door in ZyXEL routers. An administrative account was included with hardcoded credentials to install firmware updates via FTP. The company since submitted a patch, and the fact the boxes are getting automatic updates is encouraging. Here comes the proverbial *but:* it's a stark and worrying reminder that these bad security practices still exist, and we all know it's everywhere.

My two favourite quotes from university were that IT is about people not machines, and that security is Swiss cheese; you're fine provided the holes don't line up. A company I worked for briefly after high school used routers from a few different manufacturers in the hopes that deliberate or accidental back doors and security holes wouldn't compromise their networks; a practice I since learned is widespread. [Like cheese](https://www.liveabout.com/blues-brothers-cheez-whiz-3974417 "Dan Aykroyd Unravels a Cheesy Blues Brothers Mystery").
