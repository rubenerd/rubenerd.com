---
title: "Esther Golton, All The Room I Need"
date: "2021-05-31T16:56:19+10:00"
abstract: "My favourite instrumentation and intro in her 2007 album Unfinished Houses"
thumb: "https://rubenerd.com/files/2021/yt-yWT5IJ7Cusc@1x.jpg"
year: "2021"
category: Media
tag:
- esther-golton
- music
- music-monday
location: Sydney
---
This [Music Monday](https://rubenerd.com/tag/music-monday/) came as a bit of a mid-afternoon treat from Eliza, the other KDE music player. I was testing sound on my Japanese Panasonic laptop and this was the first song on random to come up. It has my favourite intro and instrumentation of any song on her 2007 album *Unfinished Houses*, and I've taken the message to heart over the years.

<p><a href="https://www.youtube.com/watch?v=yWT5IJ7Cusc" title="Play All The Room I Need"><img src="https://rubenerd.com/files/2021/yt-yWT5IJ7Cusc@1x.jpg" srcset="https://rubenerd.com/files/2021/yt-yWT5IJ7Cusc@1x.jpg 1x, https://rubenerd.com/files/2021/yt-yWT5IJ7Cusc@2x.jpg 2x" alt="Play All The Room I Need" style="width:500px;height:281px;" /></a></p>

I trust you've *all* [bought her discography on Bandcamp](https://esthergolton.bandcamp.com/album/stay-warm) already, but it's still available for those who need their second or third copies. Better yet, you can use Bandcamp's Send as Gift feature on any album page to send it as a gift. I feel as though you didn't need that specific function explained, but I did anyway for your convenience.

If you're a streaming service subscriber, please consider sending the money to artists directly who make your life better. ♡
