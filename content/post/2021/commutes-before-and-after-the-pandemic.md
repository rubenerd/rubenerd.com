---
title: "Commutes before and after the pandemic"
date: "2021-03-15T08:31:42+11:00"
abstract: "Commutes are for disconnecting, not productivity."
year: "2021"
category: Travel
tag:
- australia
- covid-19
- putrajaya
- malaysia
- work
location: Sydney
---
I overheard this at a coffee shop this morning, paraphrased:

> "A friend manages a team of people. He said that people in the 30s-40s have been the most productive during the pandemic.
> 
> "But what gets me is the idea that I can be extra productive with all the time I'm saving commuting! But people don't realise that commutes are for disconnecting from work and not thinking about it. I realise I love my commute!"

It's an interesting way of viewing it. I used to live across the street from our old office in Mascot, where my commute was measured in seconds... and I hadn't been that consistently tired and late to work in my whole career. Conversely, I don't miss getting up that early for my hour-long commute from Hornsby in Sydney's north, but I got through *so many* audio magazines, New Time Radio shows, podcasts, and audio books.

It also reminds me of the massive, and nearly empty streets in Canberra and Putrajaya, the capitals of Australia and Malaysia. Locals in both places told me the same thing: during peak hour the civil servants and other workers would swarm into their cars and choke the streets, requiring sweeping boulevards. Otherwise, the roads look like a ghost town. I was struck at the absurdity of it.

I don't miss the packed, peak-hour trains, but I'm hoping flexible hours become the norm. Peak-hour makes absolutely no sense.

