---
title: "Mathematician’s answer, via Screenbeard"
date: "2021-05-07T20:56:00+10:00"
abstract: "With apologies to @domossu."
year: "2021"
category: Thoughts
tag:
- communication
- language
- screenbeard
location: Sydney
---
Last Wednesday I asked if anyone had a term for answers that are factually accurate but pointless. [@Screenbeard](https://aus.social/@screenbeard/106191215670217202) of [The Geekorium](https://www.the.geekorium.com.au/) suggests the [Mathematician's Answer](https://tvtropes.org/pmwiki/pmwiki.php/Main/MathematiciansAnswer):

> If you ask someone a question, and they give you an entirely accurate answer that is of no practical use whatsoever, they have just given you a Mathematician's Answer. A common form of this trope is to fully evaluate the logic of the question and give a logically correct answer. Such a response may prove confusing for someone who interpreted what they said colloquially.

And for those in the back:

> People do not usually ask for trivial information or information they already possess, which is how competent speakers know not to provide the Mathematician's Answer.

This is almost perfect! Some of the examples on the page were funny; I'm specifically talking about people who are malicious, wilfully ignorant, or obtuse instead of attempting humour. Maybe I'll refer to these people as obtuse mathematicians. Or acute? *Badumtish!*

And with apologies to [@domossu](https://twitter.com/domossu), Clara's and my favourite mathematician, who *doesn't* do this... unless it's hilarious enough to do so.

