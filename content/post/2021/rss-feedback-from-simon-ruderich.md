---
title: "RSS feedback from Simon Ruderich"
date: "2021-06-30T08:33:30+10:00"
abstract: "Using and retiring Google's Base namespace caused a bit of havoc."
year: "2021"
category: Internet
tag:
- feedback
- rss
- web-feeds
location: Sydney
---
Simon emailed yesterday to say [my blog RSS feed](https://rubenerd.com/feed/) could no longer be parsed properly. He correctly identified it as a missing namespace issue. Thank you!

I'd added Google Base to my feed, but upon seeing RSS validators choke on it, I removed it. Either these validators are outdated, or simply can't parse entirely-valid XML and RSS feeds. I can see now why this caused animosity in the past from the likes of Dave Winer.

I removed the namespace and what I thought was all the other elements, but I hadn't removed <code>&lt;g:from_location&gt;</code> from each post itself. I've fixed this now.

It's a shame, I was looking forward to using the Google Base namespace for declaring personal details in a feed that future applications could pick up, in lieu of using a social network. It's no surprise the [original spec is offline](http://www.google.com/base/rss_specs.html), but you can find the elements [declared elsewhere](https://www.feedforall.com/google-base.htm).

I make it no secret that my blog also serves as a testbed and learning tool. Much of what I know about static site generators, content management systems, TLS, and FreeBSD as a webserver, all come down to my over-engineering and breaking stuff here.
