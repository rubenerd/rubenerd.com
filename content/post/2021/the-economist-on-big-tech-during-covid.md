---
title: "The Economist’s insight into Big Tech during Covid"
date: "2021-10-31T09:31:17+10:00"
abstract: "Growth was slower than people thought. I can’t help but not be surprised."
year: "2021"
category: Internet
tag:
- covid-19
- economics
- news
location: Sydney
---
The most recent Economist ran an article about [tech growth during These Covid Times](https://www.economist.com/business/2021/10/30/how-the-pandemic-has-changed-the-weather-in-the-technology-industry), and had some interesting insights:

> If the latest round of quarterly earnings are any guide, the tech industry is coming back down to earth.

They cite figures saying growth was "only" 26-39%, as opposed to the "stratospheric" heights of the previous years. This ran contrary to what was expected:

> [..] one of the first predictions when Covid-19 hit in early 2020 was that it would make big tech even bigger. Those firms, ran the theory, would be better placed to benefit from an in increased demand for digital offerings.

Instead, that uptick was less than expected, in part due to smaller businesses being more capable of riding the downturn than expected. As someone working at an SME cloud provider, this has been good news.

My personal reservation about IT growth predictions was that they were based on a fantasy that everyone was working from home. Plenty of us were, but people still lost their jobs, shifts, and livelihoods at a tremendous scale. It's hard to justify dozens of subscriptions for web streaming platforms when you're worried about rent, and must surely have had knockon effects. Fewer people with money leads to lower demand for the things people like me working remote are providing. Retail, tourism, and hospitality use a *lot* of computers.

Related to this, I've long been interested in the idea of the "two-speed economy" where information workers benefit at a greater rate than everyone else. This can't be sustainable long term, both in terms of ethics and economic reality.

