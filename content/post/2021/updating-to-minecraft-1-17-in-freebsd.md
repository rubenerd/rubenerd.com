---
title: "Updating to Minecraft 1.17 in FreeBSD"
date: "2021-06-12T12:34:54+10:00"
abstract: "Make sure you're running openjdk16."
year: "2021"
category: Software
tag:
- bsd
- freebsd
- games
- minecraft
location: Sydney
---
Part one of [Caves and Cliffs](https://www.minecraft.net/en-us/updates/caves-and-cliffs) update is out! I updated the [Minecraft server](https://www.minecraft.net/en-us/download/server) in Clara's and my FreeBSD jail, but got this error:

> Error: A JNI error has occurred, please check your installation and try again Exception in thread "main" java.lang.UnsupportedClassVersionError: net/minecraft/server/Main has been compiled by a more recent version of the Java Runtime (class file version 60.0), this version of the Java Runtime only recognizes class file versions up to 52.0

This was quite the runtime leap! Acording to [The Java Version Almanac](https://javaalmanac.io/bytecode/versions/), "60" is Java 16. Thankfully the tireless FreeBSD ports maintainers had our back:

	# pkg remove openjdk8
	# pkg install openjdk16

Now it works. Time for Clara and I to get some [axolotl](https://minecraft.fandom.com/wiki/Axolotl) friends!

