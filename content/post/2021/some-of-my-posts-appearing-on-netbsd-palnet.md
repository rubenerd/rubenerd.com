---
title: "Some of my posts appearing on NetBSD Planet"
date: "2021-06-18T22:00:36+10:00"
abstract: "This made my week, I'm genuinely honoured! "
thumb: "https://rubenerd.com/files/2021/NetBSD.png"
year: "2021"
category: Software
tag:
- bsd
- netbsd
- weblog
location: Sydney
---
<img src="https://rubenerd.com/files/2021/NetBSD.png" alt="NetBSD" style="width:128px; float:right; margin:0 0 1em 1em;" />

I was searching for something related to [pkgsrc](https://www.pkgsrc.org/), and came across some of my posts quoted on [NetBSD Planet](https://netbsd.fi). Sure enough, my name is right there in in the footer, alongside some pretty significant names I recognise.

This made my week, I'm genuinely honoured! This also shows I need to write more about [NetBSD](https://www.netbsd.org/) and pkgsrc.

If this post made it to NetBSD Planet, it'd be a post about NetBSD Planet on NetBSD Planet. Which I'd then have to quote back here. Then if they quoted that, it'd be a NetBSD Planet post on a...

