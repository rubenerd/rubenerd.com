---
title: "Hakos Baelz’s debut stream #ratpack"
date: "2021-08-30T15:36:23+10:00"
abstract: "Her rendition of Fuwa Fuwa Time made me choke up ♡."
thumb: "https://rubenerd.com/files/2021/yt-mJwpVT1WvLg@1x.jpg"
year: "2021"
category: Media
tag:
- hakos-baelz
- hololive
- youtube
location: Sydney
---
Did Clara and I save the best new Hololive Council member's debut stream for last? I don't know, they've all been spectacular in their own way. Today's *painfully* late review is Hakos Baelz, everyone's favourite harbinger of chaos.

<p><a href="https://www.youtube.com/watch?v=mJwpVT1WvLg" title="Play 《DEBUT STREAM》- LET'S GET THIS PARTY STARTED! #hololiveEnglish #holoCouncil"><img src="https://rubenerd.com/files/2021/yt-mJwpVT1WvLg@1x.jpg" srcset="https://rubenerd.com/files/2021/yt-mJwpVT1WvLg@1x.jpg 1x, https://rubenerd.com/files/2021/yt-mJwpVT1WvLg@2x.jpg 2x" alt="Play 《DEBUT STREAM》- LET'S GET THIS PARTY STARTED! #hololiveEnglish #holoCouncil" style="width:500px;height:281px;" /></a></p>

What can I say, is a phrase with four words. Bae’s intro was marked with another familiar accent that dipped into Australian, but her creative energy had her jumping into a mock Californian, and her amazing Japanese (sorry, まだまだです doesn't fly here)! I'd be fascinated to learn if her actor learned English in Australia, or whether she's from here originally. *Yis!*

I thought her debut stream was easily the most creative of all the new Council members. The way she set up those multiple choice questions, only to scribble all over them when it came to pay tribute to her senpais, it was *masterfully* done! Just as we first felt with Gura at the start, we could tell that she'd either done her homework, or was as much of a fan of the franchise as the rest of us. That enthusiasm makes all the difference.

I like to think her character design pays homage to the OG [Kureiji Ollie](https://www.youtube.com/channel/UCYz_5n-uDuChHtLo7My1HnQ) from Hololive Indonesia, but is also thoroughly unique. Her rigging and design were all so expressive, and all the detail around her hair would prove to be a real asset in later Garlic [sic] phone episodes! I think her floofy detached sleeves and socks are my favourite part of her design. Why are you telling me *floofy* isn't a word, Vim? 

She has an 8-bit OP and uses Fixedsys in her graphics! She drinks coffee (you'd have to with that level of energy) and promised to do coffee review streams! She has an infectious laugh and a great sense of humour. But what cinched it for me was her rendition of "Fuwa Fuwa Time" from *K-On!*, one of my all-time favourite anime and manga series, and one that means a lot given what was happening in my life at the time. I won't beat around the bush here, I choked up.

She was advertised as being chaos, and gave off big energy, but this and her subsequent coffee and chill streams have already shown another side that I can appreciate and respecc [sic]. Her streams have been *so* good for so many different reasons, it's fun to see what side we get each time. That's the mark of a fantastic, well-developed character. 🧀

Alphas aside, that's it for the Hololive Council debut stream reviews! I felt bad that I never wrote ones for the original English or second-gen Indonesian characters when I got into the fandom. *I suppose it's never too late to do a retrospective!*

You've likely noticed the world is a bit shit right now. If you're tired of Netflix and need something fun to chill too, consider giving some of the Hololive characters a try. Clara and I are so glad to have fallen down this <del>rabbit</del> rodent hole. ♡

