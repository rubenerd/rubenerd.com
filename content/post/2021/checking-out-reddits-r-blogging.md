---
title: "Reddit’s /r/blogging, and my own advice"
date: "2021-06-09T22:44:31+10:00"
abstract: "It’s mostly SEO."
year: "2021"
category: Internet
tag:
- blogging
- reddit
location: Sydney
---
A gentleman by the name of Miguel emailed asking if I'd read anything on <a rel="nofollow" href="https://reddit.com/r/blogging">/r/blogging</a> on Reddit. I hadn't, so I thought it was worth taking a look.

There's some good stuff around blog themes and choosing a platform to write on. But the bulk of the posts and comments seem to be about SEO, how you can gain traffic, and monetisation. I read comments suggesting you cap the length of posts, with prescriptive detail about how you should format and compose your writing to fit.

I was a little disappointed, but I shouldn't have been. Indie blogging platforms themselves now advertise [turning audiences into businesses](https://rubenerd.com/turn-your-audience-into-a-business/). Another well-known player discusses creating [beautiful websites](https://rubenerd.com/building-beautiful-websites) in their podcast and YouTube video reads, but say nothing about writers beyond saying it does "blogging".

It's all backwards.

The most important thing you can do as a blogger *is write*. Write about topics you're interested in, in whatever form you want. Knowledge, experience, creativity, dedication, enthusiasm, and joy can't be faked (for long).

All the SEO tricks and metadata in the world can't replace or compete with content people want to read and share. I rank highly in search results for a *bunch* of keywords and phrases having written here for sixteen years, but I still derive more inbound traffic from people sharing what I write on social media, mailing lists, and sites like HN. I've been offered my last few jobs as a *direct* result of people reading this silly site, which have become my primary sources of income for at least the last decade. Doc Searls pointed all this out [as far back as 2004](http://scripting.com/manila/bloggerCon/2004/10/19.html).

*(But even that thinking falls into the trap that you're only worth something if you're getting clicks or attention. Writing doesn't need a justification).*

*(While I'm writing asides, I also still think SEO is snake oil. "Search engine optimised" sites comes as a positive side-effect of genuine content, accessible page structure, and correct metadata, in that order. Starting with SEO is yak shaving at best, and textbook premature optimisation at worst, in my experience).*

I worry that people channel all their energy into these recommendations from places like /r/blogging, are disappointed when the results don't meet their expectations, and give up. Then we've then lost another writer who would have contributed something special.


