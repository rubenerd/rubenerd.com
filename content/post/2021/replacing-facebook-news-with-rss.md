---
title: "Replacing Facebook news with RSS feeds"
date: "2021-02-18T12:27:43+11:00"
abstract: "Instructions to give to friends or family that might need help."
year: "2021"
category: Internet
tag:
- news
- rss
- web-feeds
location: Sydney
---
Facebook have begun blocking news in Australia in response to some poorly-conceived, bipartisan legislation. My views of the company, and the politics that lead to this, are beyond the scope of this post. Instead I want to help people who might have relied upon Facebook for news, and offer a far superior alternative.

RSS is a decades-old technology built into most news sites and blogs. You subscribe to these in an aggregator, which let you read news as you would have on a social media platform. You can think of it as email for news, though other ways of presenting the information exists.

It's simple to use. Sign up for a platform such as [The Old Reader](https://theoldreader.com/) or [Feedly](https://feedly.com/), or download a program like [NetNewsWire](https://netnewswire.com/) or [Thunderbird](https://www.thunderbird.net/en-GB/). These tools will have an option to "add" an RSS feed. Most are smart enough to detect it if you give it a news site, such as:

> https://www.sbs.com.au/news/

<img src="https://rubenerd.com/files/stock/gnome-application-rss+xml.svg" alt="RSS icon from the Gnome Colors project" style="width:96px; height:96px; float:right; margin:0 0 1em 1em" />

Another option is to look on pages for the word "Subscribe", or for an icon similar to the one on the right. These contain a link you can paste directly into your aggregator. For example, the SBS News website has a [subscribe page](https://www.sbs.com.au/news/feeds) listing feeds for a number of different topics.

If you need help, here are the state government health departments that are publishing RSS feeds. Right-click these links and choose *Copy* to get the link you need:

* [Australian Capital Territory](https://www.health.act.gov.au/rss.xml)
* [Federal Government](https://www.health.gov.au/news/rss.xml)
* [New South Wales](https://www.health.nsw.gov.au/_layouts/feed.aspx?xsl=1&web=/news&page=4ac47e14-04a9-4016-b501-65a23280e841&wp=baabf81e-a904-44f1-8d59-5f6d56519965&pageurl=/news/Pages/rss-nsw-health.aspx)
* [Tasmania](https://www.health.tas.gov.au/home/all_news_feed)
* [Victoria](https://www2.health.vic.gov.au/rss)

*(The [Queensland](https://www.health.qld.gov.au/news-events/doh-media-releases), [Northern Territory](https://health.nt.gov.au/health-alerts), [South Australia](https://www.sahealth.sa.gov.au/) and [Western Australia](https://healthywa.wa.gov.au/) health sites don't publish RSS feeds that I can see, which to me is a clear breach of their accessibility guidelines. I've sent them feedback, but in the meantime you can read the news directly on their sites).*

And here are some common news sites. If you don't see the sites you read listed, you can probably still paste their home page URL into your feed reader.

* [ABC News Latest](https://www.abc.net.au/news/feed/51120/rss.xml)
* [The Age](https://www.theage.com.au/rss/feed.xml)
* [The Canberra Times](https://canberratimes.com.au/rss.xml)
* [Crikey](https://www.crikey.com.au/feed/)
* [The Guardian Australia](https://www.theguardian.com/au/rss)
* [Indepedent Australia](http://feeds.feedburner.com/IndependentAustralia)
* [Michael West](https://www.michaelwest.com.au/feed/)
* [NT News](https://www.ntnews.com.au/news/rss)
* [SBS News](https://www.sbs.com.au/news/feed)
* [The Sydney Morning Herald](https://www.smh.com.au/rssheadlines)

Please [contact me on @Twitter](https://twitter.com/rubenerd) or send me an email at *me at rubenschade dot com* (not spelled out) if you need help.

