---
title: "Concatenating images with ImageMagick"
date: "2021-01-05T09:52:00+11:00"
abstract: "magick convert *.png +append out.png"
year: "2021"
category: Software
tag:
- imagemagick
- photos
- scripting
---
It sounds counter-intuitive, but I do almost all my image processing for this blog using my almost two-decade old *Swiss Army Knife* shell script library. Every image is scaled at multiple resolutions to save non-Retina readers bandwidth, and is sent through the excellent [pngcrush](https://pmt.sourceforge.io/pngcrush/) and [jpegoptim](https://github.com/tjko/jpegoptim) tools to losslessly reduce their size.

On the weekend I learned that ImageMagick's [append option](https://imagemagick.org/script/command-line-options.php#append) can also be used to place images side by side, either horizontally or vertically. Note the operator changes from a minus to a plus:

	$ magick convert \*.png -append vertical.png
	$ magick convert \*.png +append horizontal.png
	
My mnemonic is to imagine the `-` is a dividing line, with images above and below.

Some of you may now be reasonably asking why I haven't given a photographic example. That's a great question. GraphicsMagick also has a [similarly-named option](http://www.graphicsmagick.org/GraphicsMagick.html#details-append), though I haven't tested.
