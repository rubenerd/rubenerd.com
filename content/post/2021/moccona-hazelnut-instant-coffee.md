---
title: "Moccona Hazelnut instant coffee"
date: "2021-04-24T09:30:54+10:00"
abstract: "Surprisingly okay!"
thumb: "https://rubenerd.com/files/2021/moccona-instant@1x.jpg"
year: "2021"
category: Thoughts
tag:
- coffee
- food
- reviews
location: Sydney
---
Darjeeling is judging me with her cool expression, not only for stooping to the level of instant coffee, but for not even having... darjeeling. Such is her silent disdain she's oriented her cup to scald her thigh with the contents of it if she's not careful.

As an aside to anime figure collectors who have the [Darjeeling Beach Queen](https://myfigurecollection.net/item/183872), have any of you figured out a way for her to hold her tea upright? This is the closest I could get without involving superglue, which I'd rather avoid as it sounds painful for her, and messy for me.

<p><img src="https://rubenerd.com/files/2021/moccona-instant@1x.jpg" srcset="https://rubenerd.com/files/2021/moccona-instant@1x.jpg 1x, https://rubenerd.com/files/2021/moccona-instant@2x.jpg 2x" alt="Photo showing Darjeeling holding a tea next to my cup of Moccona coffee." style="width:500px" /></p>

*But I digress.* Everyone and their anime figs have an opinion about coffee, to the point where discussion of said aqueous comestible is fraught with peril. I drink single origin coffee from local roasteries, and have my own opinions about which regions of the world produce the best ones. I'm rather partial to Costa Rica and Colombia.

But I also have what I call a coffee stratificaion system, the details of which are for a future post. In a <del>nutshell</del> coffee fruit, I don't judge instant coffee or Starbucks on the same scale as those aforementioned beans. Not because they're any better or worse, but because they're entirely different. I don't say a whiskey is better than a specific beer when discussing alcohol, or a [text editor with a package manager](https://www.slant.co/versus/69/1674/~spacemacs_vs_homebrew). The comparison doesn't make sense. 

Sure, instant coffee and a masterfully-roasted single origin are nominally the same type of beverage, but coffee is about the beans, the skill of the barista, the equipment, the quality of the water, the atmosphere, even your mood. The Aeropress in our tiny apartment works absolute magic, but sometimes I'm bitten by nostalgia and am too tired to futz around with a machine. I do objectively think International Roast and Nescafe are horrid, so what else is out there?

It's with all those disclaimers that I mention that Moccona's instant coffee with "a hint of natural hazelnut swirl" is nicer than I expected, and a bit of a treat.

Wait, that was the whole review? Yes, such is the nature of the Internets in 2021, you have to defend yourself from any incoming arguments that are as predictable as they are volumentric. I'm pretty sure I didn't use that word correctly.
