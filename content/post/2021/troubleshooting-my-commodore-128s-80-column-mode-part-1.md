---
title: "Troubleshooting my Commodore 128’s 80-column mode, part one"
date: "2021-03-06T15:08:28+11:00"
abstract: "No luck so far, though I did make a cable for it."
thumb: "https://rubenerd.com/files/2021/c128-80-column-plugs@1x.jpg"
year: "2021"
category: Hardware
tag:
- commodore
- commodore-128
- commodore-128-series
- retrocomputing
- soldering
- troubleshooting
location: Sydney
---
Among the *many* unique features of the Commodore 128 was that it had two separate video systems! It's 40-column mode was used when running in C128 and C64 compatibility mode, but a separate 80-column display could be invoked with a toggle keyswitch or the use of a `GRAPHIC5` BASIC command.

Unlike the RF and composite video inherited from the C64, the C128's native 80-column mode was broadly similar to IBM CGA. It achieved this with a separate RGBI output and MOS 8563 VDC chip. The connector looks superficially like VGA, but look closer and it only has two sets of pins, like a classic serial DB9 plug. The photo below shows the RF RCA connector and composite barrel plug for the C64-style 40 column, and the RGBI:

<p><img src="https://rubenerd.com/files/2021/c128-80-column-plugs@1x.jpg" srcset="https://rubenerd.com/files/2021/c128-80-column-plugs@1x.jpg 1x, https://rubenerd.com/files/2021/c128-80-column-plugs@2x.jpg 2x" alt="Photo showing the C128's composite, RF, and RGBI connectors." style="width:500px" /></p>

The 80-column mode was fantastic for BBSs, text adventures, and business software, and along with the Z80 CPU let the C128 run CP/M. But its lack of hardware sprites and separate, limited memory space prevented its use in the sorts of games people expected from Commodore systems. DesTerm128 was the most widely-used 80-column mode program, from what I can tell.

This is a [screenshot I took in VICE](https://rubenerd.com/commodore-128s-80-column-mode-in-vice/) of what the modes are supposed to look like:

<p><img src="https://rubenerd.com/files/2021/vice-c128-80-columns@1x.jpg" srcset="https://rubenerd.com/files/2021/vice-c128-80-columns@1x.jpg 1x, https://rubenerd.com/files/2021/vice-c128-80-columns@2x.jpg 2x" alt="Screenshot showing x128 with the Commodore 128 easter egg in 80-column mode from the emulated VDC." style="width:500px; height:350px;" /></p>

Which leads me to my C128! I've been so happy with the crisp, cheerful colours of her 40-column mode over a high quality composite cable and VGA converter (for a future post!), but I've never been able to get 80-columns working. I mostly live in text not games, so last month I decided to finally figure out why.


### The MGA2VGA converter box

I got an MGA2VGA converter box preassembled that takes the digital RGBI signals and convert them via a framebuffer to VGA, with the correct signals that VGA hardware recognise. [LGR had tremendous success](https://www.youtube.com/watch?v=9SII7ujB3FY) with his, so I figured it was worth a try. Wiring it up is straight forward: 

1. RGBI connector &rarr; Serial cable
2. Serial cable &rarr; MGA2VGA box
3. VGA cable &rarr; Monitor

But it didn't work for me. I swapped out each cable, used my multimeter to confirm I wasn't using null modem serial cables by accident, and used four separate monitors and TVs. None detected a signal.

The seller assured me that he'd tested it before sending, but without any native CGA hardware I still couldn't rule it out as being the faulty link.


### Composite monochrome on the RGBI

But then I learned something, which is always dangerous. Dave's Retro Desktop on YouTube [uncovered documentation](https://www.youtube.com/watch?v=snk7cn9oLgI) in a LoadStar 128 cart that showed that the RGBI connector also includes a monochrome composite signal on pin 7. He was using it as his primary monitor input, but I thought it'd be useful to troubleshoot by bypassing the converter box:

<p><img src="https://rubenerd.com/files/2021/loadstar-pins@2x.jpg" alt="Diagram showing pin 1 and 2 for ground, and pin 7 for signal." style="width:250px" /></p>

Clara and I went to Jaycar, and I spent an evening soldering a RCA cable with a serial connector on one side. I chopped off the female RCA plug, soldered pin 1 to the shielding of the cable for ground, and pin 7 to the core cable. It was actually my first time soldering *anything*, so that was an entire learning experience :).

It wasn't the best job, but I used my multimeter and confirmed that I had continuity for the signal and ground, and that I hadn't accidently bridged two of the pins. I crossed my fingers, plugged the cable into the TV and the RGBI connector on the C128, and invoked 80-column mode.

<p><img src="https://rubenerd.com/files/2021/c128-80-column-serial@1x.jpg" srcset="https://rubenerd.com/files/2021/c128-80-column-serial@1x.jpg 1x, https://rubenerd.com/files/2021/c128-80-column-serial@2x.jpg 2x" alt="My new serial connector." style="width:500px" /></p>

Would it surprise you that it still didn't work. The good news was I was able to confirm that the MGA2VGA box wasn't the root cause.


### Opening the C128

I was wanting to avoid opening the C128 enclosure again, because I'd just added fresh thermal grease to the RF shield. I'm thinking of replacing it with proper heatsinks on each IC, but for now it works.

I opened up the case, popped off the separate metal shield that enclosed the C128's two separate video systems, and noticed something I didn't before. The MOS 8563 video chip that drives the 80-column mode was riding higher in its socket than the VIC-II, almost to the point where you could see the slender pins poking out. I was excited to think that all I'd need to do was reseat the IC to make it work.

But just as I was about to push it back in, I noticed one of the pins was bent out of alignment, and a darker colour than the others. I was worried that if I pushed the chip back in, that pin would snap. 

I gingerly used my IC extractor tool, and the tiny pin almost disintegrated into a powder. I'm not sure whether it was corrosion or it had electrical damage, but the good news was the socket itself looked shiny and clean.

*(As an aside, I really need to get better at documenting these steps. You can barely see the missing pin below!)*

<p><img src="https://rubenerd.com/files/2021/c128-80-vdc@1x.jpg" srcset="https://rubenerd.com/files/2021/c128-80-vdc@1x.jpg 1x, https://rubenerd.com/files/2021/c128-80-vdc@2x.jpg 2x" alt="Photo of the VDC showing missing edge pin." style="width:500px" /></p>

A colleague who's into retro hi-fi equipment had a few helpful suggestions about how to patch this pin, but being an ameateur I didn't want to risk what has now become one of my favourite computers of all time! Fortunately, these MOS 8563 chips aren't anywhere *near* as rare and/or expensive as those legendary SID chips, and I was able to find a replacement for less than $30 on eBay.


### Conclusions

Thus far, there isn't one! Consider this part one. I'm actually happy that I found the root cause, though I might take out the multimeter and run through some more tests from the official [Commodore diagnostic guide](https://archive.org/details/C128DiagnosticInstructionandTroubleshootingManual3) to make sure I have the correct voltages everywhere in the meantime.

