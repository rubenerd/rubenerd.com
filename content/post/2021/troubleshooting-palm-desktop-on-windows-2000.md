---
title: "Troubleshooing Palm Desktop on Windows 2000"
date: "2021-05-19T09:04:19+10:00"
abstract: "Fixing MSVCP60.dll errors."
thumb: "https://rubenerd.com/files/2021/qemu-palm-msvcp60@1x.png"
year: "2021"
category: Software
tag:
- lifedrive
- palm
- virtualisation
- windows-2000
location: Sydney
---
Earlier this month I talked about getting a [Palm LiveDrive syncing with a Windows 2000 QEMU VM](https://rubenerd.com/palm-lifedrive-passthru-in-windows-2000-qemu-vm/). A few people asked me if it works on other hypervisors, so I tried in Parallels Desktop with the same USB passthrough. It worked, though it had a couple of surprises I wasn't expecting.

I uploaded my version of the Palm LifeDrive software to the Internet Archive for those missing it. Then attached the extracted ISO to my VM:

    -cdrom "Palm LifeDrive Software for Windows.iso"

Autorun showed the palmOne welcome splash screen, and I installed as normal. When it came to detect the Palm though, nothing happened. I tried launching Palm Desktop but got this dialog box:

> The dynamic link library MSVCP60.dll could not be found in the specified path C:\Program Files\palmOne;C:\WINNT\system32;C:\WINNT\system;C:\WINNT;[..]

This DLL looked suspiciously like the Visual C++ Runtime, but [installing the most recent version](https://www.microsoft.com/en-au/download/details.aspx?id=14431) that runs on Windows 2000 didn't resolve this. `SYSTEM32` included `MSVCP50.DLL`, but not version 6. I ended up finding an old newsgroup thread where a Microsoft technician suggested pulling the DLL from the installer:

1. Mount your Windows 2000 installer
2. Go to `D:\SUPPORT\TOOLS` in Windows Explorer
3. Open `SUPPORT.CAB` in Windows Explorer and locate `MSVCP60.DLL`
4. Copy this to `C:\Program Files\palmOne`

You could put it directly in `SYSTEM32`, but disk space is cheap now, and I know this specific version works with the Palm software. One does not want a repeat of **DLL Hell** in 2021!

<p><img src="https://rubenerd.com/files/2021/qemu-palm-msvcp60@1x.png" srcset="https://rubenerd.com/files/2021/qemu-palm-msvcp60@1x.png 1x, https://rubenerd.com/files/2021/qemu-palm-msvcp60@2x.png 2x" alt="Screenshot showing the MSVCP60.DLL file in the SUPPORT.CAB folder, and the palmOne folder destination" style="width:500px" /></p>

Now I was able to launch the Palm software. Not sure why this happens specifically in Parallels; maybe their Tools package clobbers something. I'm still impressed they still support Windows 2000 at all.

