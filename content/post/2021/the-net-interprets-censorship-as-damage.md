---
title: "The Internet interprets censorship as damage?"
date: "2021-02-26T19:24:58+11:00"
abstract: "I’d like to think this still holds true."
year: "2021"
category: Internet
tag:
- censorship
- politics
- privacy
location: Sydney
---
I'm shamelessly and deliberately invoking *[Betteridge's Law of Headlines](https://en.wikipedia.org/wiki/Betteridge%27s_law_of_headlines)* here, which states that any heading asking a yes/no question can be answered emphatically and unambiguously with the latter. Does it conform with the law if the author confesses to it though, rather than being coy? I can sense another silly post soon.

Here's another question: have you ever read the phrase "the Internet interprets censorship as damage and routes around it?". Variations have circulated the intertubes since the 1990s, but the quote was first attributed to John Gillmore in a pivotal [1993 issue of Time](http://kirste.userpage.fu-berlin.de/outerspace/internet-article.html). If his name sounds familiar, he was one of the founders of the Electronic Frontier Foundation, the American advocacy and education organisation nominally focused on the United States, but has global impact.

Fellow EFF co-founder John Perry Barlow echoed these sentiments a few years later in 1996, and attached a moral argument in his [declaration](https://www.eff.org/cyberspace-independence) of the Internet's independence from government interference. It remains one of the more bold and forward-thinking challenges to bad legislation and regimes, and has been widely quoted.

These ideas built upon Steward Brand's comments in the 1980s where he said "information wants to be free", in a paper subtitled "intellectual property and the mythologies of control". The rest of the quote is [often ignored](https://www.stewartmader.com/stewart-brand-information-wants-to-be-free-it-also-wants-to-be-expensive/), but it's used to justify the position that there's only so much you can do to repress information before it finds its way out. The Streisand Effect is one extreme case where it can even backfire with spectacular and self-defeating results.

All of this is still conventional wisdom among technical people I talk with. It was in my course work at university. It's stated with derision and glee everywhere from mailing lists to social media whenever a new law is proposed. But outside in the real world, the censorship damage has widened enough that I'm worried even John Gillmor's reported routing is struggling to bridge the gap... both technically, and in the minds of Internet users.

A generation of people have grown up since those papers were written, many of whom have only known a censored Internet. The majority of these Internet citizens are either unaware they're living in a sanitised subset of alliteration, or are entirely apathetic. I read the Western press gleefully reporting VPNs and underground peer to peer messaging networks in repressive countries, but barring a few precious exceptions, they're not nearly as far reaching or ubiquitous as they claim.

Censorship programmes have also widened in scale and scope. More jurisdictions are doing it, including ones we wouldn't have classed as dictatorships before. Limited legislation proposed in democracies, whether argued for in good faith or not, have succumbed to the feature creep we all warned about. This is the *exact opposite* to the world these authors wrote about and wished for, and unfortunately there's every reason to think we'll continue down this trajectory.

*(The Facebook news debacle in Australia is another example. There were a clued-in minority who understood why news suddenly wasn't appearing in their feeds, but I'd wager most people had no idea. Worse, they may have assumed digital versions of the news simply weren't available *at all*, not that Facebook was just blocking it from being posted. People have been so conditioned by social networks to think they're the only windows into the world. This isn't the censorship I'm discussing here, but it's impossible not to draw parallels).*

It was a nice idea to think there was something intrinsic to information that meant it couldn't be repressed, or that packet-switched networks would automatically treat censorship as it would a failed or misconfigured router. But I think the position is idealistic now, and I don't think it holds up to scrutiny. It's on us to do the rerouting, whether it be through advocating for legislative changes or technical measures.

