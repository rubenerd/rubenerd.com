---
title: "Gauguin, The Market Guardens of Vaugirard"
date: "2021-11-11T09:33:07+10:00"
abstract: "One of my all-time favourites."
thumb: "https://rubenerd.com/files/2021/Paul_Gauguin_064@1x.jpg"
year: "2021"
category: Media
tag:
- art
- painting
location: Sydney
---
<p><img src="https://rubenerd.com/files/2021/Paul_Gauguin_064@1x.jpg" srcset="https://rubenerd.com/files/2021/Paul_Gauguin_064@1x.jpg 1x, https://rubenerd.com/files/2021/Paul_Gauguin_064@2x.jpg 2x" alt="A post-impressionist paining of a riverside town, by artist Paul Gauguin." style="width:500px; height:324px;" /></p>

I needed a mental break today from stuff, so I started reading about Paul Gauguin again. [Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Paul_Gauguin_064.jpg) had one of my favourites.
