---
title: "A point to consider before retr0brigting"
date: "2021-02-08T08:40:01+11:00"
abstract: "The look of the machine speaks to its history."
year: "2021"
category: Hardware
tag:
- retr0bright
- retrocomputing
location: Sydney
---
I'm surprised I haven't ever talked about [Retr0bright](http://retr0bright.com/) here given my fascination with vintage computers! I checked the archives and nada. Surely I would have at least mentioned it in passing.

Retr0bright is a published technique discovered a decade ago to remove yellowing from vintage computer cases. The flame-retardant bromides in specific plastics are susceptible to yellowing over time given the right circumstances. We used to think it was irreversible, but hydrogen peroxide and sunlight or heat yield amazing results. Some of the computers I've seen look as good as new.

*(I've held back doing it to any of my gear, mostly so I could see how the process shook out on other peoples' hardware over time. I've read reports that the effect is temporary, that certain additives can weaken the plastic, and that it can lead to inconsistent blotches when care isn't taken to apply the peroxide solution evenly. Yet others, including prominent YouTubers with dozens of machines and a perfected process, report no ill effects at all, and only mild subsequent yellowing. My dad is an industrial chemist, and has said it's almost certainly fine to do).*

But [Modern Classic](https://www.youtube.com/watch?v=xBS_UEV35W4) also had a beautiful take I hadn't considered:

> \#3: All that yellowing is yours, if you've owned something for a long time.
> 
> My Apple IIc for example has been with me since 1985 when it was new.And all this yellowing you see here? That happened as I owned it, and I actually remember the different stages of it yellowing. 
> 
> Now I don't particularly *like* the yellowing. It's not that. It does speak to the history of this machine. And I want to keep it that way. I want it to look like the older machine that it is. That I've had for my whole adult life, and even much of my childhood.

I'm still considering Retr0brighting some kit, but I think my vintage tower from my childhood will be retaining its yellowing for this reason. I like that I can point to how it yellowed down one side more owing to being near a window in my childhood bedroom, or the bit on the top from when we lived in Malaysia.

