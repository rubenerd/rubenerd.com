---
title: "Australia’s Internet fun this morning"
date: "2021-08-02T10:34:08+10:00"
abstract: "That's a bit of a spike."
thumb: "https://rubenerd.com/files/2021/nbn-graph-spike.png"
year: "2021"
category: Internet
tag:
- australia
- nbn
- outages
location: Sydney
---
<p><img src="https://rubenerd.com/files/2021/nbn-graph@2x.png" alt="Network graph showing huge spike in latency from about 10:00" style="width:500px; height:174px;" /></p>

How's your Monday going?

**Update:** Reports stating there was a fibre optic cut from Perth to Singapore. That'll do it! 

