---
title: "Bear identification"
date: "2021-10-23T09:01:31+10:00"
abstract: "A list of words that don’t include “bear”"
year: "2021"
category: Thoughts
tag:
- identification
- language
- pointless
location: Sydney
---
The following words don't include *bear*:

* bear

Wait, damn it. Let's try again.

* ...
* ...
* ... bear?

Clearly the coffee hasn't kicked in yet.

**Update:** Bear.
