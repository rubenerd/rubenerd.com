---
title: "Amit Kapila on PostgreSQL 14 Logical Replication"
date: "2021-09-26T09:20:17+10:00"
abstract: "Great follow-up post to his July article on in-progress transactions. That was two hyphens. Two-hyphens? Wait, damn it."
year: "2021"
category: Software
tag:
- blogs
- databases
- postgresql
location: Sydney
---
Amit Kapila has done a great job summarising some of the [Logical Replication improvements](http://amitkapila16.blogspot.com/2021/09/logical-replication-improvements-in.html) coming in the next Postgres, with concice examples and output. Clearer monitoring of replication slot state? Yes please! His post in July was also among the most approachable I'd ever read about how [replication handles in-progress transactions](http://amitkapila16.blogspot.com/2021/07/logical-replication-of-in-progress.html).

In an alternative world where this blog you're reading now hadn't started as a side project by a teenager to discuss random thoughts, and instead become a serious place for technical discussion, his writing is what I'd aspire to.

No blockquotes in this post, I just wanted to direct people to [Amit's blog](http://amitkapila16.blogspot.com/) and [web feed](http://amitkapila16.blogspot.com/feeds/posts/default) if you're at all interested in this stuff.

