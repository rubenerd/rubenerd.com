---
title: "The @JKloss4 on rails"
date: "2021-07-19T21:31:48+10:00"
abstract: "With some Philadelphia travel nostalgia ♡"
year: "2021"
category: Travel
tag:
- jim-kloss
- trains
- philadelphia
- united-states
location: Sydney
---
I forgot to respond to <a href="https://twitter.com/JKloss4/status/1416037893265633287" title="Jim Kloss's tweet">this charge</a> by my dear friend Jim Kloss on The Bird site a last Saturday:

<blockquote cite="https://twitter.com/JKloss4/status/1416037893265633287"><p>P.S. With all respect to @Rubenerd who can quickly point out both historic and wildly futuristic attributes of rails.  Here in Philadelphia, the streets are littered with old rusted and abandoned rails that harken back to the 19th century but, sadly, remain unused today.</p></blockquote>

Alas, today I spend far more time thinking about <a title="+3.3 V and +5 V rails" href="https://en.wikipedia.org/wiki/Power_supply_unit_(computer)#+3.3_V_and_+5_V_rails">those rails</a>, or <a href="https://rubyonrails.org/">these rails</a>, than these parallel iron and steel conveyance enablers! *Conveyence enablers?*

Sydney's Covid lockdown did get me thinking though: this is probably among the longest stretches of my life where I haven't travelled on a train. Including Clara's and my east coast US trip a few years ago, when we met Esther, Jim, and Beta in their beautiful city. Boston is the next US city on my list, but I'd love to spend more time exploring the history of Philadelphia.
