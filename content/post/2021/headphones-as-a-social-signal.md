---
title: "Headphones as a social signal"
date: "2021-03-04T10:35:05+11:00"
abstract: "They say to leave us alone!"
year: "2021"
category: Hardware
tag:
- apple
- music
- sociology
location: Sydney
---
*Get it?* As opposed to headphones *receiving* a signal?! I'd ask you to humour me, but you might not be listening to comedy at all.

Make enquires as to the purpose and function of headphones, and you'll undoubtedly be judged as being a few notes short of a symphony. *Of course* they're for listening to music! Maybe podcasts too, and audio books. And unlike speakers, headphones and studio monitors let you do it in relative privacy, a fact that people who blast music on public transport must conveniently forget as they play music well suited to narcissists with poor taste.

But they serve other functions. Back in the day they were an invitation to be robbed for your precious Walkman or iPod connected to the end of the headphone cable (the "white slab of gold", as the New York Times once referred to them). That's perhaps not a function as much as a side effect, but worth mentioning.

For introverts or those trying to thrive and concentrate in troubling environments like open plan offices, headphones serve as a vital "leave me alone" signal. It's normal for me to have nothing playing in my headphones so I can maintain situational awareness, but I'm letting people know around me that I'm not free to talk. I've not come across any other prop that is as effective in my decade working in IT; people don't respect overt signs that say "busy".

This lack of availability for attention is also useful in the street. I know that someone wearing headphones doesn't have that full situational awareness I just mentioned, so they may be distracted and not paying attention. Motorists and cyclists know to stay well clear of headphone zombies crossing the road. This might sound&mdash;heh&mdash;counter-intuitive to praise headphones for this, given they're *the source* of the distraction. But hear&mdash;heh&mdash;me out.

Apple's AirPods, and all of the subsequent shameless clones, remove the useful bar that connects the two sides in addition to the cable, so one can lose "half" their headphones. But this class of device has also made it more difficult to see at a glance if someone is aware of their surroundings or not. I don't have any data that demonstrates that they've caused more accidents, but I suspect so.

I don't own AirPods, and the fact I use a mix of macOS and FreeBSD equipment means I'm unlikely to anytime soon. Headphone cables are a pain, but they mean its one fewer thing I have to remember to charge every night. They also serve as a useful social signal that discrete pods in my ears don't.

Maybe I'm weird for finding the social impact of these sorts of technical changes interesting. 
