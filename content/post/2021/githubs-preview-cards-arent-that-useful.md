---
title: "GitHub’s preview cards aren’t that useful"
date: "2021-10-22T09:16:06+10:00"
abstract: "Redesigning featured images for “overscan”."
thumb: "https://rubenerd.com/files/2021/github-crop@1x.jpg"
year: "2021"
category: Thoughts
tag:
- 
location: Sydney
---
Featured images are a common piece of metadata you can add to a website or blog post. It's a way for people to preview your content with a still image when linked to on social media, and can be more enticing and useful than just a static URL. Schema metadata, Dublin Core RDF, OpenGraph, and Twitter Cards all have metadata to define these images.

It's not always as simple as defining an image and calling it a day, though. Care has to be taken that these images make sense in a wide array of contexts. Some viewports show the image in its entirety, but might render it small enough that tiny details are lost. Most often these images are cropped, so just like overscan on an old TV, you need to make sure the image makes sense when shown as a square, or if the corners are rounded off.

GitHub recently implemented a feature that autogenerates preview images with useful information about repositories and pull requests, such as the number of files, commits, changes, and the developer. Here's an example:

<p><img src="https://rubenerd.com/files/2021/github-not-crop.jpg" alt="" style="width:500px; height:250px; border:1px solid lightgrey" /></p>

Unfortunately, this is all I ever see on my clients:

<p><img src="https://rubenerd.com/files/2021/github-crop@2x.jpg" style="width:250px; height:250px; border:1px solid lightgrey" /></p>

These images aren't essential; one can click through to see the repository. But you may as well optimise them if you're going to go to the trouble of auto-generating them in the first place. Placing important details in the centre would make them useful for more people, with ancillary items like avatars and language breakdowns flanking either side.

I have other reservations about GitHub which are beyond the scope of this post, but I thought this was a useful example of a wider issue.
