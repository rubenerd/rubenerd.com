---
title: "Weather outside you wouldn’t endure inside"
date: "2021-12-25T09:43:05+11:00"
abstract: "So much of our perception comes down to expectations."
year: "2021"
category: Thoughts
tag:
- personal
- weather
location: Sydney
---
Merry Christmas and Happy Holidays! I'm a morning person, so I'm letting Clara sleep in while I sit on our balcony and do some reading.

The human body is weird. One thing I learned from working at home over the last few years is how important changes in scenery are; even if it amounts to sitting on the balcony a few metres away from my desk. I also need to be outside as soon as I wake up, or I never do.

But in doing so I made a bizarre observation: I'm willing to tolerate winter chills, and the scorching summer weather I'm enduring right now, if I'm outside. If it was this hot *inside*, I'd be moaning at how oppressive it is, and smashing the buttons on our aircon remote like there's no tomorrow.

*(Let's also acknowledge though that there are limits. I don't think I'd much enjoy doing this in Dubai, or the middle of Greenland)!*

Is it proof that so much of our perception comes down to expectations? That's almost certainly simplistic, but I've got to think there's something to it.

I suppose its the same phenomena at play when I get that bitter taste from a black coffee as opposed to a fruit. The former is satisfying and wakes me up; the latter makes my face shrivel.

