---
title: "Researching GPU options for my sleeper PC"
date: "2021-08-30T10:36:10+10:00"
abstract: "I’m thinking a second-hand 1660 or 1660 Super, unless anyone has suggestions."
year: "2021"
category: Hardware
tag:
- compaq-spaceship
- gpus
- shopping
location: Sydney
---
Back in July I wrote about that [silly little Compaq tower](https://rubenerd.com/finally-buying-a-compaq-spaceship/) I always wanted as a kid, and my process to [convert the empty case](https://rubenerd.com/reversible-sleeper-pcs/) into a sleeper PC. On the weekend I decided to review GPU options, like a gentleman.

My aim is to have:

* The ish factor. A budget(ish) card that can play the few world building and simulation games I care about on medium(ish) settings, and otherwise work well for daily desktop use.

* 230 cm or shorter (ideally 220 cm), to fit inside the case. My OC'd GTX 960 from my last build would probably have still been fine here, but alas it's too long.

* Comperable or better performance than the 8 GiB Radeon Pro 5500M in my MacBook Pro, or the aforementioned "4 GiB" GTX 960. I figured that shouldn't be too hard.

* Nvidia, not AMD. Regardless of which you think is better, Nvidia have better binary FreeBSD drivers, and certain older games like Train Simulator hang on modern Radeon cards, which sucks.

2019's GTX 1660 Super looks like it'd be the best balance of the above. It is *slightly* above what I'd be willing to pay, though the prices get *much* sillier after that. There also seem to be a lot of second-hand cards with a shorter profile, without going all-in with a low-profile card that would have less (and therefore louder) cooling.

*Insert obligatory comment about how much crypo-"currency" miners have ruined the world here!*

