---
title: "Genetics behind black coffee drinkers"
date: "2021-12-21T08:44:52+11:00"
abstract: "A study published by Nature asks whether people are predisposed to it, and what learned behaviors result from drinking black coffee. Interesting!"
year: "2021"
category: Thoughts
tag:
- coffee
- food
location: Sydney
---
[Nature published an intruiging study](https://www.nature.com/articles/s41598-021-03153-7) into whether people were genetically predisposed to drinking black coffee, among other bitter foods and beverages.

I haven't had time to read the whole paper yet, but their summary piqued my interest:

> [O]ur genetic analysis suggests the psychostimulant effects of caffeine outweighs the bitterness of caffeine. A greater preference for caffeine based on genetic differences in the physiological effects of caffeine leads to a stronger preference for the taste/smell of coffee and dark chocolate. Similarly, greater sensitivity to the adverse physiological effects of caffeine was associated with avoiding the taste of coffee.

"Avoiding the taste of coffee" in this case refers to its masking with milk and sweeteners. I adore the taste of coffee, so will generally have it black.

The authors also touch on learned behaviours, which positively corrolate the bitterness of black coffee with the buzz one feels after having caffeine. I suspect this is the mechanism by which my afternoon decaf still wakes me up to an extent, though the authors note:

> ... the caffeine-taste sensitive variant (rs2708377 C) is associated with consumption of coffee regardless of how it is prepared but tends to be more strongly associated with caffeinated than with decaffeinated coffee.
