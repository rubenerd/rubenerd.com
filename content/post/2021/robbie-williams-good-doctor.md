---
title: "Robbie Williams, Good Doctor"
date: "2021-07-23T09:56:27+10:00"
abstract: "The world carries on spinning, we're mad, innit?"
year: "2021"
category: Media
tag:
- lyrics
- music
- pointless
location: Sydney
---
> The world carries on spinning, we're mad, innit?   
> (Take that, take that) ...innit? ♫
