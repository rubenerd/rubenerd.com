---
title: "The 747-300"
date: "2021-10-14T08:13:51+10:00"
abstract: "Looking into why this model didn’t fly off the shelves."
thumb: "https://rubenerd.com/files/2021/eth-bibliothek-747-300@1x.jpg"
year: "2021"
category: Travel
tag:
- aviation
- boeing
- boeing-747
- business
- economics
- history
location: Sydney
---
I've long been curious why the 747-300 sold in such small numbers, at least relative to other Boeing jumbo jet variants. It pioneered the stretched-upper deck (SUD) which made its way into the successful -400 series. It also boasted minor aerodynamic improvements which gave it a range and efficiency bump. Yet it barely warrants a footnote in most aviation restrospectives, with most airlines flying by. It didn't fly off the shelves. Jumbo.

<p><img src="https://rubenerd.com/files/2021/eth-bibliothek-747-300@1x.jpg" srcset="https://rubenerd.com/files/2021/eth-bibliothek-747-300@1x.jpg 1x, https://rubenerd.com/files/2021/eth-bibliothek-747-300@2x.jpg 2x" alt="Photo of a Swissair 747-300 against an alpine landscape" style="width:500px; height:333px;" /></p>

*Photo of a Swissair 747-300 in 1984 taken by Georg Gerster, part of the ETH-Bibliothek collection, <a href="https://commons.wikimedia.org/wiki/File:Georg_Gerster_ETH-Bibliothek_LBS_SR04-002256_(cropped).tif">uploaded to Wikimedia Commons</a>. That's desktop background material.*

Justin Hayward recently broke down some reasons for [Simply Flying](https://simpleflying.com/boeing-747-300-popularity/)\:

> The additional offering of the 747-300 over the 747-200 was in many ways minimal – especially given the higher price. Many airlines that wanted 747s had already ordered the 747-200. There was also a strong second-hand market in place by now.

Alexander Pask quantified the price delta in [International Aviation HQ](https://internationalaviationhq.com/2021/03/13/boeing-747-300-unsuccessful/), even though his site pictures a -200 under the title!

> Whilst they appreciated the developments Boeing made, they weren’t enough to justify the $82 million ($223 million today) price tag, compared to the $66 million ($165.5 million) for a brand-new 747-200.

That's not that much of a capacity improvement for $16 million. It also foreshadowed the airline industry's lacklustre response to the -500X and -600X.

The 747-400 introduced barely a few years later included winglets, improved fuel capacity, more automation, and a two-person glass cockpit which airlines were demanding by the mid-1980s. By the time the last -200s had been delivered, most airlines jumped to the -400.

Another point I'd considered was that the -300 might not have made as good a freighter. The SUD would have been all but useless in that Dutch Roll (hah, so good), so it would have made financial and logical sense to stick with the -200. The fact all subsequent freighter variants still shipped *without* a SUD would seem to support that.

That's not to say the -300 was just a stopgap. A friend of mine back in the day was training to be an airline mechanic for Qantas, and he told me their crews loved the relative simplicity of the airframe. I'll bet flight engineers preferred it as well! I've also read comments on mailing lists saying the -300 is what the -200 *should* have been. SUD upgrades were made available for these earlier airframes, which suggests a degree of commonality between the airframes or it wouldn't have been economically feasible.

I'm sure I flew on Qantas 747-300s at least a few times growing up, but the -400 *Longreach* had become the workhorse of the fleet by the time I got into aviation.
