---
title: "DOS-friendly bootable ISOs with mkisofs"
date: "2021-04-15T10:21:50+10:00"
abstract: "Setting the appropriate options in mkisofs to make it work."
year: "2021"
category: Software
tag:
- cds
- dos
- guides
- mmx-machine
location: Sydney
---
I've used WinImage and WinISO in a Windows 2000 virtual machine for years to create and modify ISO images, but I decided in the [spirit of the occasion](https://rubenerd.com/freebsd-13-0-release-available/ "FreeBSD 13.0-RELEASE available!") to learn how to do the same thing on FreeBSD. The experience also taught me a few things about how bootable CDs actually work.

My biggest surprise was realising that ISO 9660 makes no provision for bootable media. This makes sense given earlier machines couldn't boot from CDs, but I had long assumed this was only a BIOS limitation. Bootable CD support came with the *El-Torito* extension, so named for the restaurant where the developers first conceived of its design. It can even include multiple boot records for various architectures.

Jörg Schilling's excellent [cdrecord](http://cdrtools.sourceforge.net/private/cdrecord.html) tools includes mkisofs, which can be used to create bootable ISOs that support this. The FreeBSD Handbook's storage chapter includes a section on [creating CDs with it](https://docs.freebsd.org/en/books/handbook/disks/#creating-cds), but I wanted to be able to boot on legacy x86 machines like 486s and early Pentiums. I scoured the [mkisofs manpage(8)](https://www.freebsd.org/cgi/man.cgi?query=mkisofs&sektion=8) and came up with these options:

    mkisofs                             \
        -eltorito-boot "BOOTFLOPPY.IMG" \
        -input-charset "cp437"          \
        -iso-level 1                    \
        -joliet                         \
        -output "OUTPUT.ISO"            \
        -rational-rock                  \
        -volume-set "DISKNAME"          \
        "$_source file"

The options are:

* `-eltorito-boot` is the floppy image you want to boot from
* `-input-charset "cp437"` for DOS-based systems
* `-iso-level 1` enforces DOS-style 8.3 filenames
* `-joliet` isn't ISO standard, but is useful for 95/NT
* `-output` is the exported ISO image
* `-rational-rock` adds Rock Ridge extensions
* `-volume-set` is the CD volume name the OS displays

*Rock Ridge* extensions aren't used on DOS or 16-bit Windows as far as I can tell, but doesn't impact its use and is valuate when attached to other systems, so I still include anyway.

From my experience with WinISO, I knew that bootable CDs for early machines used floppy disk images, which would be emulated by the optical drive on boot. One common trick was to embed a Windows 98 boot disk with CD-ROM drivers into an otherwise-unbootable CD, like those for Windows 95 or earlier OS/2. In an age where blank CDs are easier to find than blank floppy disks, I've even been know to create bootable DOS installers.

I've wrapped this in a [shell script on my lunchbox](https://codeberg.org/rubenerd/lunchbox/src/branch/trunk/bootable-dos-iso.sh) if you want to use.

