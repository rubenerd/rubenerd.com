---
title: "One year of Hololive-EN! #mythiversary"
date: "2021-09-13T08:45:41+10:00"
abstract: "Thanks to this lovable group of people for getting through some tough times. ♡"
thumb: "https://rubenerd.com/files/2021/1year-holoen@1x.jpg"
year: "2021"
category: Media
tag:
- amelia-watson
- hololive
- ninomae-inanis
location: Sydney
---
The girls described their [musical collaboration](https://rubenerd.com/hololive-english-take-me-home-country-roads/) earlier this year with:

> "We are here for you to overcome the difficult times."

This time last year was looking pretty grim, but in the pits of our global despair five VTubers stepped up and lifted our spirits. Their timing couldn't have been better; their enthusiasm and creativity have done so much for us during Covid lockdown melancholy and despair.

<p><img src="https://rubenerd.com/files/2021/mythiversary@1x.jpg" srcset="https://rubenerd.com/files/2021/mythiversary@1x.jpg 1x, https://rubenerd.com/files/2021/mythiversary@2x.jpg 2x" alt="Screenshot from the mythiversary stream" style="width:500px; height:281px;" /></p>

Clara and I started watching their streams shortly after Amelia Watson's debut, not quite sure what to expect. That was probably was true of everyone: would an *English* Hololive work? How would they interact with their Japanese and Indonesian senpais? But it's safe to say they blew us all away, and their success paved the way for [IRyS](https://www.youtube.com/channel/UC8rcEBzJSleTkf_-agPM20g) and the current Council who wouldn't exist without their efforts.

Our boy [Cali's](https://www.youtube.com/channel/UCL_qhgtOy0dy1Agp8vkySQg) music is now a fixture of family roadtrips. [Kiara's](https://www.youtube.com/channel/UCHsx4Hqa-1ORjQTh9TYDhww) enthusiasm is infectious. [Watson](https://www.youtube.com/channel/UCyl1z3jo3XHR1riLFKG5UAg) is our huggable gremlin who's mannerisms have seeped into everything we say (kuh-yuute!). [Gura's](https://www.youtube.com/channel/UCoSrY_IQQVpmIRZ9Xf-y93g) bubbly persona and bathtub pizza can't mask her genuine heart. And [Ina](https://www.youtube.com/channel/UCMwGHR0BTZuLsmjY_NT5Pwg), now Clara's and my favourite ever streamer (!), continues to be a welcome, reassuring presence of comfiness. 💜

<p><img src="https://rubenerd.com/files/2021/1year-holoen@1x.jpg" srcset="https://rubenerd.com/files/2021/1year-holoen@1x.jpg 1x, https://rubenerd.com/files/2021/1year-holoen@2x.jpg 2x" alt="One of my favourite screenshots, from their recent TTRPG stream" style="width:500px; height:281px;" /></p>

Perhaps the best part of all has been seeing their chemistry develop over the last twelve months. I hope to see more of it :). ありがとう~

