---
title: "Feedback on Braun shaver cables"
date: "2021-09-12T07:35:54+10:00"
abstract: "Braun intended it as a safety feature for people trying to use them in the shower."
year: "2021"
category: Hardware
tag:
- electicity
- feedback
- safety
location: Sydney
---
Last Monday I [wrote about a Braun electric shaver](https://rubenerd.com/what-apple-magic-mouses-and-braun-shavers-share/) I use to shave, funny though it may seem. In particular I wondered why it would refuse to turn on after it was plugged in. A few of you were equally bemuxed, a word I was positive existed, but I can't find it anywhere else.

Hans Dorn and Dale Smith both wrote in to say the Braun instructions made it clear that it was a safety feature of their electric shavers, to prevent people using it in the shower while it was connected to mains. That makes sense if that's a design concern. I'll bet there's regulations involved as well.

I've never thought to use a corded appliance in a shower, whether it be a toaster, hair dryer, or shaver (and definitely not at the same time, before you get any weird ideas). I don't think I've ever shaved *in* a shower either, even when I was still on acoustic shavers, so I wasn't even ware people did that. The supplied cable is also far too short to reach a power socket from any shower I've ever used; that's probably also a safety feature.

Yes, I called non-electric shavers **acoustic shavers**. I call non-motorbikes **acoustic bikes** too, because they must be. Fling as much justifiable frustration in the direction of the English language as you want for its weird spelling, loanwords, and overuse of idioms, but at least it's *consistent*. Wait, no it isn't.

But if you'll stop interrupting me, I can see why this safety feature is a Good Thing. Just because you and I wouldn't do something silly like use an electric shaver in the shower with a power cord somehow dangling over the railing with an extension cable to the power outlet on the other side of the room while trying to shove a hair dryer into a toaster, doesn't mean someone else hasn't given it a thought. I'll bet [Medhi](https://www.youtube.com/user/msadaghd) has.
