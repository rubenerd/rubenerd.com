---
title: "Keanu Reeves on NFTs"
date: "2021-12-11T11:01:08+10:00"
abstract: "Tee hee hee!"
year: "2021"
category: Internet
tag:
- blockchain-bullshit
- keanu-reeves
- nfts
location: Sydney
---
Speaking of Keanu Reeves, [this clip from a Verge interview](https://www.theverge.com/2021/12/11/22829383/matrix-nft-keanu-reeves-interview-carrie-ann-moss) was spectacular:

> **Interviewer:** They made NFTs for the new movie [..]
>
> **Keanu:** *Strokes beard*
>
> **Interviewer:** When you think about the concept of digital scarcity and things that are, y'know, can't be copied...
> 
> **Keanu:** That are usually reproduced? *Laughs*
