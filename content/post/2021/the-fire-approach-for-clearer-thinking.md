---
title: "The FIRE approach for clearer thinking"
date: "2021-04-07T10:22:46+10:00"
abstract: "From an interview with Spencer Greenberg."
year: "2021"
category: Thoughts
tag:
- cognition
- philosophy
- podcasts
- you-are-no-so-smart
location: Sydney
---
This [podcast episode](https://youarenotsosmart.com/2020/12/15/yanss-195-tools-for-creating-positive-habits-making-better-decisions-and-understanding-your-thoughts-feelings-emotions-and-self/) of *[You Are Not So Smart](https://youarenotsosmart.com/podcast/)* where David McRaney interviews Spencer Greenberg was *so* good. Spencer discusses the difference between "system one" and "two" thought processes:

> When should I use my gut, or intuition, or system one, to make a decision, and when should you not use your gut, and use what I call reflective decision making? This doesn’t mean you ignore your gut, it means your gut isn’t in charge. You’re using your reflective mind, your system two, to make the decision, and you’re querying your system one in useful ways.

FIRE is his criteria for deciding, which he defined as:

<blockquote>
<dl>

<dt><strong>Fast</strong></dt>
<dd>You should use your gut if it’s a fast decision. A car suddenly swerves into your lane and is about to hit you: you do not have time to use your reflection. Do you swerve away? Do you hit the brake?<p></p></dd>

<dt><strong>Irrelevant</strong></dt>
<dd>Let’s say you’re at a salad place, and you think <em>oh man</em>, should I get carrots on my salad, I don’t know, and you’re racking your brain. It’s an irrelevant decision, it’s just not worth using your reflection on, so just go with your gut and pick something.<p></p></dd>

<dt><strong>Repetition</strong></dt>
<dd>Our intuition and gut can get very good if we have a lot of repetition making a type of decision, <em>with feedback</em>. My favourite metaphor is archery: imagine you're practising, you see where your arrow hits the target, let's say it's a little to the left. Okay, now you'll try again and you'll adjust your stance. That's learning with feedback, and over time you can get an intuition about where that arrow will go when I pull back on the bow. Now imagine doing that blindfolded; shooting arrows all day like that, you'll never get good at it, because you have no feedback. Chess is another example: masters are good at using their reflective mind, but their intuition is so good that it’s mind-blowing.<p></p></dd>

<dt><strong>Evolutionary</strong></dt>
<dd>We are animals, and there are certain types of predictions and decisions that are built into us. If you hear a loud noise close to your head, you'll have a startled response, and you'll jump away. It's probably a pretty good decision most of the time; maybe something's about to fall on you.<p></p></dd>
</dl>

</blockquote>

This ties into the idea I've been obsessed with lately that we all have a finite amount of willpower each day, which is what our conscious minds use to make informed decisions. It's not worth expending effort on irrelevant questions, or questions with low stakes, when there are bigger fish to fry.

