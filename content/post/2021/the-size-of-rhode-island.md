---
title: "… the size of Rhode Island!"
date: "2021-07-08T10:25:09+10:00"
abstract: ""
thumb: ""
year: "2021"
category: Thoughts
tag:
- 
location: Sydney
---
The standard units of measurement for most American documentaries I watch aren't feet, inches, or some other archaic unit based on the width of a king's left bicep in the 14th century, but football fields and Rhode Islands.

It made me wonder just how big Rhode Island is, and how I could make it relatable to what I understand, such as SI units and how many Singapores and Tasmanias such a geographic approximation represents.

The first question was trickier to answer than I expected. Rhode Island is an American state, formerly known as the *State of Rhode Island and Providence Plantations*. Despite the name, most of the *Ocean State* resides on the US mainland, a fact that makes as much sense to an outsider as Kansas City being in Missouri, or the fact *everything is bigger in Texas* is a meme despite it not being the largest state in the US, and its size being diminutive by global subdivision standards. But I digress.

The entire State of Rhode Island covers 3,144 square kilometres, which can be expressed in miles as another number. This dwarfs Singapore's measly 728 square kilometres even with its extensive reclamation works, though it's well shy of Tasmania's 90,758. Placed over a map of itself though, and the ratio would be exactly 1:1.

But this area is a bit misleading. 407 square kilometres of the state's area is water, a aqueous substance of which I keep small portions of by my bedside for late night consumption. This means the total landmass is only about 2,700 square kilometres, assuming the Dutch don't invade and put dikes everywhere as they are want to do. Amster-*damn* that was a good joke.

Then we get to the Rhode Island... island itself, also known as [Aquidneck](https://en.wikipedia.org/wiki/Aquidneck_Island). This island island is connected to the mainland mainland via a series of bridges and ferries, granting access to the English-sounding settlements of Newport, Portsmouth, and the delightfully geographically-accurate Middletown. This comes in at a paultry 98 square kilometres, which a hastily-written Perl script tells me is smaller than the state with which it shares a name.

Stood on end, Rhode Island Island (... island) and its neihbouring island of Prudence would stand the same height as they would lengthwise when positioned as they regularly are. Everything would likely fall off too, including [Newport State Airport](https://en.wikipedia.org/wiki/Newport_State_Airport_(Rhode_Island)) which reported 20,238 aircraft operations in 2011. I'm assuming they're not counting each door closing and each prop spinning up as seperate operations; that'd be a bit misleading. But isn't that the lesson of this whole post?

Now I know that next time someone says *it's about the size of Rhode Island*, I have three measurements to keep in mind and not just one. Maybe saying a few thousand football fields would be easier.

