---
title: "Kronii, Bae, and Mumei discuss flower"
date: "2021-09-08T16:29:00+10:00"
abstract: "Flower."
thumb: "https://rubenerd.com/files/2021/flower@1x.jpg"
year: "2021"
category: Media
tag:
- flowers
- hakos-baelz
- hololive
- ouro-kronii
- nanashi-mumei
location: Sydney
---
Clara and I caught up on [Bae's Minecraft stream](https://youtu.be/Qud3gcINOiU?t=7372) earlier in the week, and caught this exchange that will surely go down in Internet history. FLOWER!? *Flower.*

<p><a href="https://youtu.be/Qud3gcINOiU?t=7372"><img src="https://rubenerd.com/files/2021/flower@1x.jpg" srcset="https://rubenerd.com/files/2021/flower@1x.jpg 1x, https://rubenerd.com/files/2021/flower@2x.jpg 2x" alt="... flower." style="width:500px; height:281px;" /></a></p>

I love that inexplicable things like this exist, and that it's already a meme with fanart and thousands of tweets. The Internet can be a great place. 🌷
