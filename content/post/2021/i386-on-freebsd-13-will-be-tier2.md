---
title: "i386 on FreeBSD 13 will be Tier 2"
date: "2021-04-02T11:04:01+11:00"
abstract: "I thought I wrote about this. Can’t pass up a nostalgia trip!"
thumb: "https://rubenerd.com/files/2021/vmware-fusion-2007@1x.png"
year: "2021"
category: Software
tag:
- bsd
- freebsd
- nostalgia
location: Sydney
---
Michael Dexter over on [FreeBSD News](https://www.freebsdnews.com/2021/02/02/i386-a-tier-2-platform-in-freebsd-13/) reminded us in February that the i386 architecture is being demoted to Tier 2 status. He linked to the original [freebsd-announce mailing list thread](https://lists.freebsd.org/pipermail/freebsd-announce/2021-January/002006.html), written by John Baldwin:

> FreeBSD is designating i386 as a Tier 2 architecture starting with
FreeBSD 13.0.  The Project will continue to provide release images,
binary updates, and pre-built packages for the 13.x branch.  However,
i386-specific issues (including SAs) may not be addressed in 13.x.
The i386 platform will remain Tier 1 on FreeBSD 11.x and 12.x.

I absolutely empathise with their position, but it still feels like the end of an era.

My first experience with FreeBSD was on PowerPC, but I still have my 6.1-RELEASE and 6.2-STABLE ISOs from when I first ran it on x86. I even used it to test the beta releases of [Parallels](https://rubenerd.com/parallels-desktop-freebsd-issues/) and [VMware Fusion](https://rubenerd.com/vmware-fusion-beta-3-released/) in the mid-2000s.

<p><img src="https://rubenerd.com/files/2021/vmware-fusion-2007@1x.png" srcset="https://rubenerd.com/files/2021/vmware-fusion-2007@1x.png 1x, https://rubenerd.com/files/uploads/screenie.mbp_vmware.png 2x" alt="Screenshot showing the VMware Fusion beta running FreeBSD 6.2 STABLE." style="width:500px" /></p>

I also liked that I could still put a contemporary version on my vintage x86 machines, partly for the warm fuzzies, but also so I could boot into them to transfer files over Ethernet without resorting to floppies. I had plan to move those to [NetBSD/i386](https://wiki.netbsd.org/ports/i386/) anyway though.

Thanks to the FreeBSD maintainers and developers for all the work maintaining this architecture.

