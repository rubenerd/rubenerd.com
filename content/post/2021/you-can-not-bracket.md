---
title: "You can (not) bracket"
date: "2021-03-17T09:04:21+11:00"
abstract: "Why I learned to love the bracket after being told we should use dashes."
year: "2021"
category: Thoughts
tag:
- english
- language
- weblog
location: Sydney
---
I made the mistake of reading one of those "you're doing grammar and punctuation wrong" posts last year, and internalised one of its recommendations. The author claimed that readers ignore words between brackets when written in the middle of a paragraph, so you should use em-dashes instead. You can probably tell when I started using them here. 

*(Here's an example of brackets used around an entire paragraph, which that author tolerated. It makes more sense to me to include optinal text inline and in context, rather than having people jump around between footnotes and back, or reading text in tiny popup bubbles. To each their own).* 

Here's an example with brackets:

> The anime *Hyouka* (that Clara and I watched when we started dating) is among the prettiest I've ever watched.

And with em dashses:

> The anime *Hyouka*&mdash;that Clara and I watched when we started dating&mdash;is among the prettiest I've ever watched.

I read the second paragraph differently in my head. There's a pause and change in tone between the dashes that's omitted with the brackets. But I also *didn't skim what's between the brackets*. The text is still right there in both cases, akin to someone telling you not to think of an elephant.

So many of these style guides seem to be based more on writer opinion than verifiable evidence about human behaviour. Which is fine, but it shows how such edicts handed down to the plebeians under the guise that you're *doing something wrong* are so often... wrong.

I have *so* many thoughts about brackets, including why I think the Japanese and continental Europeans have it figured out better than us in the Anglosphere. But I'll spare you those ramblings until next time.
