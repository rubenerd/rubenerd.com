---
title: "Dover and Dover again"
date: "2021-03-23T22:21:34+11:00"
abstract: "Complete with early 2000s tech!"
year: "2021"
category: Media
tag:
- music
- nostalgia
location: Sydney
---
Music has the uncanny ability to transport us back to places and time like precious few other things, whether it be the tune or even the props in the music videos themselves. For me this ability opearates independently of whether I liked the song at the time.

Early this week I heard someone blasting "[Over and Over](https://www.youtube.com/watch?v=n3htOCjafTc)" by Nelly and Tim McGraw from back in 2004. It took me immediately back to high school, when I'd irritate people singing the chorus with emphasis in the wrong place:

> Because it's all in my head   
> I think about it *Dover* an‘ *Dover* again   
> I replay *Dover* an‘ *Dover* again

Those weren't the lyrics, but it sounded like it was. [Dover MRT](https://en.wikipedia.org/wiki/Dover_MRT_station) also opened a few years prior.

And while I'm on this *train* of thought&mdash;ah, so good&mdash;the music video was also an amazing technical time capsule. Those alarm clocks at the start scream early 2000s, but not as much as their phones at the end.

<p><img src="https://rubenerd.com/files/2021/dover-and-dover-clocks@1x.jpg" srcset="https://rubenerd.com/files/2021/dover-and-dover-clocks@1x.jpg 1x, https://rubenerd.com/files/2021/dover-and-dover-clocks@2x.jpg 2x" alt="Photo from the music video showing Nelly's alarm clock that resembles an MP3 player, and Tim's high end early-2000s Hi-Fi alarm clock." style="width:500px; height:281px;" /><br /><img src="https://rubenerd.com/files/2021/dover-and-dover-phones@1x.jpg" srcset="https://rubenerd.com/files/2021/dover-and-dover-phones@1x.jpg 1x, https://rubenerd.com/files/2021/dover-and-dover-phones@2x.jpg 2x" alt="Photo from the music video showing Nelly's flip phone, and Tim's Blackberry, if I had to guess? The one with the double letters on each key." style="width:500px; height:281px;" /></p>

