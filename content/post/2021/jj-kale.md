---
title: "JJ Kale"
date: "2021-05-28T10:56:58+10:00"
abstract: "Kale, Kale, JJ Cale, Kale...!"
thumb: "https://rubenerd.com/files/2021/jjkale@1x.jpg"
year: "2021"
category: Media
tag:
- pointless
location: Sydney
---
I can't remember what I was searching for yesterday, but I couldn't get over this search result. <em>If you got bad news, you want to kick them blues... kale? ♫</em>

<p><img src="https://rubenerd.com/files/2021/jjkale@1x.jpg" srcset="https://rubenerd.com/files/2021/jjkale@1x.jpg 1x, https://rubenerd.com/files/2021/jjkale@2x.jpg 2x" alt="Photo result showing kale, kale, JJ Cale, kale..." style="width:500px; height:130px;" /></p>

