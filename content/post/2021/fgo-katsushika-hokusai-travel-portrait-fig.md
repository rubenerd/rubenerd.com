---
title: "Katsushika Hokusai travel portrait fig"
date: "2021-07-18T10:58:21+10:00"
abstract: "One of the comfiest figs I’ve ever seen, from the FGO game."
thumb: "https://rubenerd.com/files/2021/000575724@1x.jpg"
year: "2021"
category: Anime
tag:
- anime-figs
- comfy
- fate
- fate-grand-order
location: Sydney
---
<p><img src="https://rubenerd.com/files/2021/000575724@1x.jpg" srcset="https://rubenerd.com/files/2021/000575724@1x.jpg 1x, https://rubenerd.com/files/2021/000575724@2x.jpg 2x" alt="Press photos of Katsushika Hokusai’s travel portait fig version showing comfy clothes and atmosphere." style="width:500px; height:400px;" /></p>

Once again I'm being tempted by a dust collector, this time from everyone's favourite Foreigner-class servant from *[Fate/Grand Order](https://fate-go.us/)!* I've been hovering over a pre-order button for the last five minutes, and showing a tremendous amount of self-restraint... if I say so myself.

> From the popular smartphone game "Fate/Grand Order" comes a scale figure of the Foreigner class servant Katsushika Hokusai! Katsushika Hokusai has been recreated based on her Travel Portrait craft essence illustration from the game's 3rd Anniversary. Rather than her traditional Japanese outfit, Katsushika Hokusai is wearing a more casual outfit and carrying around her sketchbooks. This look befitting of a modern artist has been faithfully recreated in figure form. Don't miss the miniature figure of Toto-sama and the stone path themed base too!

She's made by Phat, a company I'm not that familiar with, but then I haven't bought a dust collector for several years now. I like that these companies have also started putting more effort into their press photos as well, rather than using that same backdrop of marble paper. That sandwich board in the right corner is adorable!

[Ina from Hololive](https://www.youtube.com/channel/UCMwGHR0BTZuLsmjY_NT5Pwg) introduced me to the idea of using "comfy" to describe video streams, anime figures, games, and stories. She *definitely* qualifies.

She'll be released in July 2022. Please give me the strength not to press that preorder button; I'd appreciate it.
