---
title: "More than techno: a history of electronic music"
date: "2021-12-14T11:07:05+10:00"
abstract: "A great article in Deutsche Welle about a Düsseldorf music exhibit."
year: "2021"
category: Media
tag:
- electronica
- jazz
- music
- pet-shop-boys
- techno
location: Sydney
---
The German public broadcaster Deutsche Welle ([RSS feed here](http://rss.dw.com/rdf/rss-en-all)) published a great article yesterday on the [history of electronica](https://www.dw.com/en/more-than-techno-a-history-of-electronic-music/a-60103980)\:

> Electronic music is sometimes labeled robotic and one-dimensional, music that can only be enjoyed with the help of alcohol in a dark nightclub. This cliché extends to the idea that the genre originated in the 1980s when synthesizers and drum machines became integral to pop music.
> 
> But electronic music has had a long and diverse influence on the modern musical canon, a topic explored by the exhibition "Electro. From Kraftwerk to Techno."

The article and exhibition discuss the origins of electronic music from the etherophone/Theremin from the late 19th century, all the way through to the Düsseldorf-based Kraftwerk in the 1970s, and modern electronic-dance music (EDM). Give it a read if you have some spare time.

I've long been interested in electronica, both from the novelty of the sound and technical feats that enabled it. Prior to electricity, and later the integrated circuit, these sounds would have *never* been heard before, much less imagined or believed. How often does an artform come along where people are literally getting to play with mediums of expression that would have been impossible to conceive or develop before? That's cool!

It always surprises people to hear a jazz guy like me wax lyrical about electronica, but there are similar qualities that captivate me about both. They both emphasise experimentation and improv. The reason why the latter necessitated an abbreviation but not the former eludes me. Even jazz was seen as unfamiliar, edgy, and unpredictable when it burst onto the scene.

<p><a target="_BLANK" href="https://www.youtube.com/watch?v=ZEIilz9TIlA" title="Play Only the Wind (2018 Remaster)"><img src="https://rubenerd.com/files/2019/yt-ZEIilz9TIlA@1x.jpg" srcset="https://rubenerd.com/files/2019/yt-ZEIilz9TIlA@1x.jpg 1x, https://rubenerd.com/files/2019/yt-ZEIilz9TIlA@2x.jpg 2x" alt="Play Only the Wind (2018 Remaster)" style="width:500px;height:281px;" /></a></p>

That said, there's a lot of bad electronica out there... at least, in my accurate opinion. I can't stand dubstep, and most EDM grates harder than an imported parmesan. Anders Enger Jensen, Kraftwerk, and the legendary Pet Shop Boys (sample above) are easily my favourites though.

I think the jazz electronic fusion is also an avenue that hasn't been explored enough. Electroswing has shown you can blend the two, but I'm more interested in people using synths and electronic instrumentation in jazz as opposed to sampling the latter. Like Ben Sidran did for a few of his 1980s albums. Anyone know of good examples?
