---
title: "The word “best”"
date: "2021-12-11T13:36:10+10:00"
abstract: "It can mean the best possible, or the best of a bad bunch."
year: "2021"
category: Thoughts
tag:
- language
location: Sydney
---
I was having a chat with Clara about freight companies, like all reasonable partners do on their weekends. I said that I thought a specific one was the *best*, to which she responded with all the times they'd delayed or missing shipping dates for her.

Both of us were telling the truth, and our opinions were based on the same facts and circumstances. *Best* was the only word that divided us!

Best is weird. It can be taken as an absolute, such as saying that something is the *best it can possibly be*, as Clara thought. It could merely describe the best a set offered, like I was saying. Taken to another extreme, it could be the *best of a bad bunch*, which flatly contradicts the first use. Except when it doesn't.

This is the best post I could write about it in a few minutes. Or was it?
