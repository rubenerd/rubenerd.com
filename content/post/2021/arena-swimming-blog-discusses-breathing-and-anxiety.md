---
title: "Arena swimming blog discusses breathing, anxiety"
date: "2021-04-01T11:29:20+11:00"
abstract: "“... control our emotional states more effectively, including those that might cause stress or anxiety”"
thumb: "https://rubenerd.com/files/2021/arena-kun@2x.jpg"
year: "2021"
category: Thoughts
tag:
- arena
- anxiety
- personal
location: Sydney
---
I follow the [Arena blog](https://blog.arenaswim.com/en/) among other fitness sites to maintain interest and motivation. What I wasn't expecting was a detailed post on curbing anxiety masquerading as breathing for athletics.

I'm tempted to print this and attach to my desk. Emphasis added:

> Breathing is the simplest and most natural thing we do, but under exertion, it requires coordination and **awareness**.
> 
> Breathing techniques help improve concentration and health in general, since we can use our breathing to learn how to **control our emotional states more effectively, including those that might cause stress or anxiety**.

<p><img src="https://rubenerd.com/files/2021/arena-kun@1x.jpg" srcset="https://rubenerd.com/files/2021/arena-kun@1x.jpg 1x, https://rubenerd.com/files/2021/arena-kun@2x.jpg 2x" alt="Photo of an Arena-kun swim cap." style="width:192px; float:right; margin:0 0 1em 2em;" /></p>

This is worth reading again if you deal with anxiety. Still the best advice I've ever received is acknowledging that slow, deliberate breathing is incompatible with anxiety symptoms. With that, you can break out of the negative feedback loop and assess your circumstance with a clearer head. Which frees it for a cute swim cap. 

This is also an exercise I've also been told about:

> Sit on the edge of its chair with your back straight, your shoulders spread and your shoulder blades pulled downwards, your legs firmly on the floor and not crossed. Place one hand on your chest and the other on your stomach, breathe in through your nose and extend your stomach while keeping your chest still, then breathe out through your mouth, relaxing your stomach gently as if sighing. Using both your hands allows you to feel the movement better and to check you are breathing the right way.

