---
title: "Monotony grows wings on time"
date: "2021-08-29T08:49:36+10:00"
abstract: "The imagery Paul Daley painted about lockdowns was chef’s kiss."
year: "2021"
category: Thoughts
tag:
- covid-19
- health
- nostalgia
- singapore
location: Sydney
---
Paul Daley wrote a [beautiful article for The Guardian](https://www.theguardian.com/commentisfree/2021/aug/28/is-it-bin-night-again-already-lockdown-has-done-something-strange-to-our-sense-of-time) yesterday on the feeling of lost time during Covid and lockdowns. The temptation is there to quote the whole thing, but I'll restrain myself! 

> Something strange and discombobulating has happened to time during this latest lockdown. Like it’s become circular or somehow compressed. Years seem like months like weeks like days like hours like minutes.
> 
> Is it because our days are all the same now? Because the rigid routine of lockdown (wake, exercise, work, cook, read [..] sleep, wake, repeat) minimises the potential for surprise in all but our more vivid than ever inner lives? Which is, after all, the intended consequence of lockdown.

Lockdown days are the longest I've experienced, and the shortest in aggregate. Where have the weeks since June gone?

He then discusses feelings of nostalgia we've all being going down of late:

> Is it any wonder the mind is taking these memorial trips into decades past when, we are all, having pared back so many extraneous “unnecessary” joys, in the words of Pink Floyd, “ticking away the moments that make up a dull day”?

Each post on retrocomputing here has been met with at least a dozen other meandering thoughts about childhood and my teenage years as well. They make me want to go home, eat at a kopitiam, and put on some Phua Chu Kang VCDs. Maybe we will, one day.

