---
title: "Defensive blogging"
date: "2021-07-22T08:33:35+10:00"
abstract: "Here are some things I’ve learned to stave off the worst people."
year: "2021"
category: Internet
tag:
- feedback
- weblog
location: Sydney
---
For all the encouraging words I write here about everyone being able to blog, in our own spaces, and without the permission of large social networks that <del>can</del> will eventually disappear and take our creative works with them, there's a darker side that needs to be acknowledged from time to time.

The web is full of people who suffer from, to use an Australian analogy, tall poppy syndrome. Someone having success or fun with something is seen as an affront, and dumping on the person and the activity it seen as the only reasonable and expected course of action. Almost as bad are those whom Merlin Mann refers to as the exhausting "why am I not a potted fern?" people who question the point of what someone is writing about, usually by acting obtuse and asking tedious questions. They're delightful people! By which I mean I'd delight in knocking them back into the proverbial bog they came from.

*(For those people, a bog is a form of wetland in which someone can get stuck. I'm using it as a metaphor here, which is a figure of speech used for rhetorical effect to make a broader point. Points are what you find on the end of a pencil, and are logical constructs you seem to miss along with your trousers and dignity with alarming regularity. But I digress)!*

<p><img src="https://rubenerd.com/files/2020/clipart-duck@1x.png" srcset="https://rubenerd.com/files/2020/clipart-duck@1x.png 1x, https://rubenerd.com/files/2020/clipart-duck@2x.png 2x" alt="Duck!" style="width:128px; height:146px; float:right; margin:0 0 1em 2em;" /></p>

I've been on the Internet long enough to think that it's getting worse. I think social network echo chambers are feeding a downward spiral in civil discourse, critical thinking, and generally not being a dick. And not the [good kind of spiral](https://rubenerd.com/tag/gurren-lagann/).

Which leads us to blogging. Acknowledging the issue and wanting to improve the state of online discourse is a noble (and I still think achievable) goal, but in the meantime what can you do to stave off these people, at least in part?

Technically speaking, is a phrase with two words. I'd say disable blog comments, and accept feedback on an email address. The number of comments I had dropped after this, but the higher barrier to entry acts as a natural filter. And make it clear to any that still make it through that their posts are liable for publishing along with their name. The only attribute that eclipses troll laziness, short-sightedness, and "being in it for the lulz" is cowardice. I also suspect body odour.

Unfortunately, I have had to modify how I write a bit too. Pointless references and silliness are natural ways to disarm people, but I also review what I write from the perspective of someone who's reading in bad faith. Even if people in a real world conversation would understand what you're saying in context, there are those who will only serve to nitpick what you say. Sometimes that takes an overt "inb4" at the end of a post, other times I make light of the situation or idea myself in advance. I appreciate people who don't take themselves too seriously, and try to do it too.

I wouldn't fault anyone at this stage for wanting to cut off any avenue for feedback at all. But the positive comments and constructive feedback are well worth putting up with dull people.
