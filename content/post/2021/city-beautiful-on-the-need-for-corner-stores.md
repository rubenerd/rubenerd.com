---
title: "City Beautiful on the need for corner stores"
date: "2021-12-21T17:03:25+10:00"
abstract: "Every neighbourhood should have one, but can’t."
thumb: "ttps://rubenerd.com/files/2021/yt-nuHQizveO1c@1x.jpg"
year: "2021"
category: Media
tag:
- economics
- geography
- science
- sociology
- transport
location: Sydney
---
City Beautiful did a great video recently about the need for more corner stores and local shopping. He made the case for accessibility, walkability of suburbia, health benefits, and how such stores can bring communities together. He touched on the zoning challenges such stores face in the US, though I'm sure the same applies to Australia and other suburban countries. 

<p><a href="https://www.youtube.com/watch?v=nuHQizveO1c" title="Play Every neighborhood should have a corner store—but can't"><img src="https://rubenerd.com/files/2021/yt-nuHQizveO1c@1x.jpg" srcset="https://rubenerd.com/files/2021/yt-nuHQizveO1c@1x.jpg 1x, https://rubenerd.com/files/2021/yt-nuHQizveO1c@2x.jpg 2x" alt="Play Every neighborhood should have a corner store—but can't" style="width:500px;height:281px;" /></a></p>

I don't begrudge anyone wanting their own slice of the Australian or American dream with a bungalow or house, but the idea of living out my days among endless tracts of cookie-cutter suburbia accessible only by <span style="text-decoration:line-through">car</span> traffic sounds depressing beyond words. Wait, I used words to describe it. I'd rather live in a small apartment close to coffee shops, grocers, supermarkets, community centres, and a dependable train station.

*But!* I know I'm in the minority here. We should be doing what we can to encourage healthier, more sustainable living, regardless of where people are. Corner stores seem like an obvious addition that'd help everyone.

7-11's aren't exactly comparable, but I used to frequent the one closest to one of our apartments in Singapore, and got to know the people who worked there. I didn't realise until recently how much I missed that; just like how I can't see my favourite baristas as much anymore.

There's probably an online shopping angle to this too, and how we don't directly interact with any human in the supply chain. We're living our lives in bubbles, either in cars between supermarkets, or in cardboard boxes shipped to our doors.
