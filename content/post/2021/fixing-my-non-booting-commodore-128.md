---
title: "Fixing my unbootable Commodore 128"
date: "2021-02-21T09:49:23+11:00"
abstract: "Turns out it was a dirty power switch!"
thumb: "https://rubenerd.com/files/2021/c128-switch@1x.jpg"
year: "2021"
category: Hardware
tag:
- commodore
- commodore-128
- commodore-128-series
- repairs
location: Sydney
---
This is part two in my my [Commodore 128 series](https://rubenerd.com/tag/commodore-128-series/). I originally intended to post about some device history and explain *why* it's my favourite 8-bit computer of all time, but I got my unit booting again last night and wanted to share!

Josh Nunn of [The Geekorium](https://www.the.geekorium.com.au/) sent me this Commodore 128 back in 2018 in working order, though occasionally it would take a few power cycles to turn on. But since our last house move the power LED failed to glow, and it outputted no video signals at all. So I finally sat down last night to try and figure out why.


### Easy tests

The easiest way to tell if a Commodore is working is by plugging in a disk drive, and seeing if the drive seeks when the computer starts. I had my trusty Commodore 1541 I got for my 18th birthday from eBay, but plugging it and booting the C128 showed no activity on the drive's LED. I know the drive and serial cable are good from using them with my Plus/4 and C16.

Another easy check is to see if the C128 is supplying 5V to the second pin from the right of the datasette port. When I bought a [replacement power supply](https://www.c64psu.com/c64psu/48-1390-commodore-c128-128-psu-power-supply-unit.html#/5-color-gray/40-ac_cable-au) for the C128, I also got a [handy passive volt meter](https://www.c64psu.com/c64psu/84-c64c128-voltmeter-5906874492765.html) with pass-through that you can plug into the datasette port. The meter's segment display didn't turn on when the computer booted. Just in case the detector was faulty, I probed with my multimeter directly and got a measly 0.38 V (this number would also come back in other places). I was relieved the board was at least getting power.

*(I forgot to take a photo of the meter when it wasn't showing any power on the second pin. This was taken after fixing the machine).*

<p><img src="https://rubenerd.com/files/2021/c128-meter@1x.jpg" srcset="https://rubenerd.com/files/2021/c128-meter@1x.jpg 1x, https://rubenerd.com/files/2021/c128-meter@2x.jpg 2x" alt="Photo showing the vold meter plugged into the datasette port, with 4.91 on its display." style="width:500px" /></p>


### Probing for power

I watched a bit of a YouTube video where the gentleman was troubleshooting video issues (I think it was the imitable [Jan Beta](https://www.youtube.com/channel/UCftUpOO4h9EgH0eDOZtjzcA)) when he reminded me of the Commodore diagnostic manual! [Archive.org has high quality scans](https://archive.org/details/C128DiagnosticInstructionandTroubleshootingManual3/page/n3/mode/2up) of all the pages, so I loaded them up on my iPad. I set my multimeter to 20 V DC mode, connected the black probe to ground on the board, and started checking.

Section 2 described some basic preliminary checks:

1. **Measure the voltage on pin 25 (+ 5VDC) of the 6581 SID.** I counted from the bottom-right in an anti-clockwise direction. Pin 25 was forth from the bottom on the left. <span style="color:darkred">I only read 3.58 V again, which was wrong</span>.

2. **Measure the signal on pin 28 (+ 12VDC) of the 6581 SID.** I counted this as the bottom-left pin. <span style="color:darkgreen">I read 11.91 V which was fine</span>.

<p><img src="https://rubenerd.com/files/2021/c128-power@1x.jpg" srcset="https://rubenerd.com/files/2021/c128-power@1x.jpg 1x, https://rubenerd.com/files/2021/c128-power@2x.jpg 2x" alt="Photos showing my multimeter reading 3.58 V on pin 25 of the SID, and 11.91 V on pin 28 of the SID." style="width:500px" /></p>

The manual said if any result was incorrect to refer to Section 2.2, **"System Power Supply"**. This could either be a very hairy problem, or something simple, so I crossed my fingers for the latter! I had a defective 5V supply, so I checked Section 2.2.1:

1. **Measure the voltage on the (+ LEG) of Capacitor C107.** This is one of the small caps next to the power plug. <span style="color:darkred">It dropped further to 0.38 V, which was clearly wrong</span>.

2. **Measure the voltage on the (+ LEG) of Capacitor C99.** <span style="color:darkgreen">I read 5.23 V which was fine</span>!

<p><img src="https://rubenerd.com/files/2021/c128-power-2@1x.jpg" srcset="https://rubenerd.com/files/2021/c128-power-2@1x.jpg 1x, https://rubenerd.com/files/2021/c128-power-2@2x.jpg 2x" alt="Photos showing my multimeter reading 0.38 V on capacitor C107, and 5.23 V on capacitor C99" style="width:500px" /></p>

The manual suggested the issue was a **defective Switch S1**. This the rocking switch next to the power plug for turning the C128 on and off. [GGLabs sold replacements](https://www.tindie.com/products/gglabs/replacement-power-switch-dpdt-commodore-64128/), but I was hoping I could fix this one first. I flicked it a few times and it seemed mechanically fine, though I noticed after doing that the voltage on the datasette port and the SID chip had dropped further! This confirmed my suspicions that this switch was dodgy.


### Checking and fixing the power switch

I unplugged the computer from mains and tried to get a closer look. I used the torch from my phone and noticed a large clump of fluff inside the mechanism when the switch was turned off. I used a small can of compressed air and was shocked by the amount of crud that flew out in either direction from a tiny burst, in addition to the aforementioned dust bunny. The second burst dislodged a further mist of dust. I didn't have anything to compare it to before, so I assumed this switch was supposed to be stiff. But it moved with ease now, so it was immediately obvious that it was gummed up before.

*(I suppose this switch is a primary point of ingress into the case, and is basically the only mechanical component of the whole computer).*

My favourite Hi-Fi YouTuber Techmoan clued me into using contact cleaner on crackly volume dials to make them smooth and quiet, so I figured it was also worth a shot. I sprayed a small amount into the switch mechanism, and switched it on and off a dozen times to work it in. It now had a reassuring *click* in addition to taking less physical force. It felt like an entirely new mechanism, just as the drive rails did on my 1541 after cleaning and giving them new lithium grease. *Who'd have thunk it?!*

<p><img src="https://rubenerd.com/files/2021/c128-switch@1x.jpg" srcset="https://rubenerd.com/files/2021/c128-switch@1x.jpg 1x, https://rubenerd.com/files/2021/c128-switch@2x.jpg 2x" alt="Photo showing me applying contact cleaner to the back of the power switch mechanism." style="width:500px" /></p>

The alcohol seemed to evaporate in seconds, but I let the machine dry for an hour, then plugged it back in and turned it on. The sight nearly made me cry; one of my favourite machines was working again! I was getting the right voltages on the datasette port and the SID chip, and the power LED worked! The switch is also infinitely-more satisfying to use now, which is entirely pointless but fun in a tactile way.


### Conclusion

Lesson learned: always troubleshoot basic mechanical things first if you can!

The next step is to source some more thermal paste so I can reattach the shield; the original 36 year-old paste had dried out and wasn't making contact at all. I'm also retr0brighting the keys and thoroughly cleaning the keyboard switch mechanisms which over the years had become stuck under a few keys. But that's for the next post :).

I also noticed that only one of the two LEDs light up on the power status light. I'm fine as long as *one* is working, but I'd like to see if I could replace this at some point too.

<p><img src="https://rubenerd.com/files/2021/c128-working@1x.jpg" srcset="https://rubenerd.com/files/2021/c128-working@1x.jpg 1x, https://rubenerd.com/files/2021/c128-working@2x.jpg 2x" alt="Photos showing the left-most LED power light illuminated, and the Commodore 128 boot screen on our TV." style="width:500px" /></p>
