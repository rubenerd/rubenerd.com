---
title: "My science teacher on lateral thinking"
date: "2021-10-21T08:43:29+10:00"
abstract: "Literal goalpost moving and popsicle sticks."
year: "2021"
category: Thoughts
tag:
- design
- lessons
- science
- thinking
location: Sydney
---
My year 7 science teacher Mr Dall'oste was brilliant. He made every topic approachable, fun, and interesting; as all good teachers do. One of his classes I *vividly* remember turned out to be one of those sneaky life-lesson things.

We'd been told to break out into groups, and create the strongest bridge we could out of a limited set of popsicle sticks. Each bridge would be judged at the end of the lesson by suspending them between two desks, and placing gram weights on their decks. The prize was a small bag of Mars bars, I think.

My group got to work placing a series of popsicle sticks lengthwise, then gluing them together with some X-shaped trusses underneath and on the sides; like a barn door. We connected a few of these sections together with more glue and horizontal sticks. It looked like this from the top:

<pre style="line-height:1em !important">
┌────────────┬────────────┐
├────────────┼────────────┤
├────────────┼────────────┤
├────────────┼────────────┤
├────────────┼────────────┤
└────────────┴────────────┘
</pre>

Most of the kids in the class made a similar design. Surprising nobody, when the teacher placed each bridge deck between two tables and started adding weights, that middle joint acted like a fulcrum and buckled. Ours held up *marginally* better on account of the X-shaped trusses. One other group staggered their sticks so there wasn't a single point of failure, but even it succumbed to splinters. Another group arranged their sticks at a 90-degree angle to ours; it didn't stand a chance.

Finally he got to the last group, who moved the tables close enough together that they could be bridged by a single popsicle stick. Over howls of consternation at this literal shifting of the goal posts, Mr Dall'oste added weight after weight to this shorter bridge. 

Despite being the same design as ours, the lack of a weak, middle joint meant it was able to take the cumulative weight of every other bridge in the class, and then some. I can't remember if he ran out of those gram weights or not, but when he started stacking textbooks and his coffee mug on top, it was clear who the winner was.

Kids have nothing if not an inate sense of fairness, so eventually Mr Dall'oste went back to the front of the class to justify why he'd permitted such a flagrant disregard of the rules! He explained that this one group had demonstrated lateral thinking. Bridges in the real world don't span across an entire river or valley by themselves, they're supported by pylons, or an arch, or tension from cables, or a cantilevered lattice. He argued that by moving the desks closer, that one group had built columns to transfer the weight more effectively, rather than relying on the popsicle sticks themselves which have weak points.

The protests persisted, but like a good lawyer working for an engineering firm he claimed he never mentioned that the desks couldn't be moved. Either I or one of my group mates tried to justify that he said he wanted us to build a bridge out of *popsicle sticks*, and that moving the tables integrated them into the design. He claimed the desks were part of our design too, which I couldn't dispute. He had us beat.

I still think about that today (clearly)! Is what I'm doing the best approach, or have I been permitted leeway (tacitly or otherwise) to adjust the environment? Is there something on the periphery, or in part of the support structure I'm not thinking of? It sounds so obvious, but that train of thought has helped me more in my career than I probably even appreciate. Cheers sir.

