---
title: "Boss Coffee’s comfy promotion"
date: "2021-08-16T08:11:54+10:00"
abstract: "Yes, I would like a comfy chair and a sandwich press with a bunny on it."
thumb: "https://rubenerd.com/files/2021/boss-promotion@1x.jpg"
year: "2021"
category: Thoughts
tag:
- advertising
- comfy
- design
- drinks
- food
- grilled-cheese-sandwiches
- japan
location: Sydney
---
Hololive's Ina has got me using *comfy* to describe all manner of things now. This Boss Coffee and DOD promotion I saw linked to on a Japanese site looks especially so:

<p><a href="https://www.suntory.co.jp/softdrink/boss/campaign/cafebase03/"><img src="https://rubenerd.com/files/2021/boss-promotion@1x.jpg" srcset="https://rubenerd.com/files/2021/boss-promotion@1x.jpg 1x, https://rubenerd.com/files/2021/boss-promotion@2x.jpg 2x" alt="Banner from the Boss Coffee website showing a selection of their drinks next to canvas foldout chairs and a couple of sandwich presses." style="width:445px; height:202px;" /></a></p>

Sitting on one of those canvas chairs, having a sandwich made with a press *that puts a bunny on it*, with a cute plant? Yes, please!

There's something about pine furniture as well that I love. Modern Western interior design seems to place so much emphasis on cold, dark stone surfaces, metal, and glass. Last time Clara and I were in Japan we saw more emphasis on indoor plants and light woods, especially in trendy new coffee shops.

I haven't done a [grilled sandwich post](https://rubenerd.com/tag/grilled-cheese-sandwiches/) in at least five years. I'm as shocked as you are.

