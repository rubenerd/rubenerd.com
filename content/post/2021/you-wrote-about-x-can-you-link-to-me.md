---
title: "“You wrote about X, can you link to me?”"
date: "2021-01-14T20:17:34+11:00"
abstract: "And did you get a chance to review my last email?"
year: "2021"
category: Internet
tag:
- manton-reece
- spam
location: Sydney
---
[Manton Reece microblogged](https://www.manton.org/2020/10/19/ive-fallen-behind.html) this phenomena last October:

> I’ve fallen behind on support email again, and getting more spam isn’t helping. So tired of those “I see you wrote about Topic X, can you link to my blog post too?” emails. (Except for other people’s blog posts and conversations on Micro.blog, not even my own posts.)

This spam is a new scourge, and you know they'll respond with "[did you get a chance to review my last email?](https://rubenerd.com/did-you-have-a-chance-to-review-my-last-email/)".

My favourites are ones that inform you that Netscape Navigator or Limewire that you wrote about in that nostalgia post are... wait for it... retired and no longer maintained! But did you know you can use their tool instead?

