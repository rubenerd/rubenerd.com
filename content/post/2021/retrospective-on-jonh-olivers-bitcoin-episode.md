---
title: "Retrospective on John Oliver’s Bitcoin episode"
date: "2021-06-17T09:14:15+10:00"
abstract: "He was spot on regarding the ethical and business case against it, but I think he still downplayed the concerns."
thumb: "https://rubenerd.com/files/2021/yt-g6iDZspbRMg@1x.jpg"
year: "2021"
category: Media
tag:
- bitcoin
- business
- environment
- ethics
- john-oliver
- video
- youtube
location: Sydney
---
John Oliver did an episode of his Last Week Tonight programme on Bitcoin back in March 2018. I thought it was worth watching again to see how much has changed in the intervening three years.

John's opinion was that Bitcoin may have the technical potential to be faster and more secure than banks (we'll get to those at the end), but that its practical use is tantamount to a pyramid scheme. He cited Bitcoin communities and companies that soaked their investors for millions of dollars with slick advertising, pump-and-dump tactics, and Ballmer-esque enthusiasm.

<p><a href="https://www.youtube.com/watch?v=g6iDZspbRMg" title="Play Cryptocurrencies: Last Week Tonight with John Oliver (HBO)"><img src="https://rubenerd.com/files/2021/yt-g6iDZspbRMg@1x.jpg" srcset="https://rubenerd.com/files/2021/yt-g6iDZspbRMg@1x.jpg 1x, https://rubenerd.com/files/2021/yt-g6iDZspbRMg@2x.jpg 2x" alt="Play Cryptocurrencies: Last Week Tonight with John Oliver (HBO)" style="width:500px;height:281px;" /></a></p>

I'm glad he spent most of his time discussing the business and ethical realities at play here. There's no point having a technically feasible system if its implementations and knock-on effects are unhelpful or harmful (see online tracking as another example). Externalities are a reality in any system, and denying their existence is a sure sign of immaturity.

Since 2018, the environmental impact of *proof of waste* (POW) work has become mainstream knowledge, with comparisons to the output of entire sovereign countries. *Non-fungible trash* (NFT) is now conning even more people, and claiming the scalps of industry professionals I used to admire and respect.

*(POW really stands for "proof of work", and NFTs are "non-fungible tokens", not waste or trash. But I saw those alternative names floating around the Social Medias and thought they were brilliant. It's truth in advertising)!*

Since John filed that report, speculative blockchain humbug is now driving up the prices of silicon, networks, and storage devices in addition to ruining the lives of gamers with graphics cards. These devices are used in everything from personal computers, to medical devices and transport. I can see the appeal to make a quick buck, but redirecting these efforts in aggregate to pointless speculation makes no environmental, ethical, or business sense.

But it gets better! Even if we limit ourselves to the [CIA triad](https://en.wikipedia.org/wiki/Information_security#Key_concepts) yardstick of security, Bitcoin can only guarantee transactional Integrity. The ledger is public knowledge, so it's hardly Confidential in practice. And thanks to laughably slow and energy intensive processing times, Bitcoin transactions can hardly be called Available. Even if we assumed it satisfied all of these, clearing houses and exchanges have fscked off with billions of people's real money, and been the target of attacks themselves. I sympathise with the motivation to replace the banking system with something less brittle and more transparent. This ain't it. 

It's rare that a situation is even sillier than what John and his team presented at the time, and now it's even more bizarre. But that's the reality in 2021.

