---
title: "Gadget-like computers, or computer-like gadgets?"
date: "2021-02-24T12:06:51+11:00"
abstract: "I still don’t understand the comparison between Palm and the iPhone."
year: "2021"
category: Hardware
tag:
- iphone
- palm
- nostalgia
location: Sydney
---
A prominant Apple blogger, with whom I otherwise agree on a lot of things, posted this about Palm:

> Ed Colligan, as the CEO of Palm, should have known that in 2006, the future of phones was gadget-like computers, not the computer-like gadgets the industry (including Palm) had been making until then.

[I tweeted on Monday](https://twitter.com/Rubenerd/status/1363695073716670465) that I've yet to make sense of this. I still haven't! 

Palm devices and iPhones are both computers. They both have memory, operating systems, CPUs, and IO. They're also both gadgets, in that they're multifunctional devices people can hold in their hands. What makes one more like a gadget, and one more like a computer, if they're both? 

John-Mark Gurney [suggested](https://twitter.com/encthenet/status/1363698390970765317?s=20) it might have something to do with application stores, and the turning of computers into appliances. That would be consistent with the idea that an iPhone is gadget-like, though as John points out, Palm devices also had a store.

Palm devices were simpler than desktop computers; they had to be. Their bundled applications had widgets like a traditional computer GUI, but were designed from the start around stylus input (contrast that with Windows CE). The same can be said of the iPhone, only they optimised for fingers with a capacitive display surface and larger controls. Both have far more in common with each other than desktop computers, so I don't see how one is more "computer-like" or "gadget-like" than the other. They're both.

Ed Colligan invited comparisons when he ran my beloved Palm into the ground with strong words and little foresight. But this whole computer/gadget dichotomy reads more like Chopra than anything else.

