---
title: "Annus horribilis"
date: "2021-11-24T09:15:56+10:00"
abstract: "I hope you're doing well. I'll be back soon :)"
year: "2021"
category: Thoughts
tag:
- personal
location: Sydney
---
The Queen spoke of 1992 as being her *annus horribilis* owing to family turmoil that she felt responsible for. I think it's safe to say many people will be seeing 2020 and 2021 as theirs.

Mine was 2007, but this year has sure come close to usurping it. But I'm trying to see the positive. Losing more close family and people I care about has made me appreciate even more what they stood for, and what they contributed to the world. I'm glad I've been cleared of the Big C, and that I can have a firmware update with some scary but hopefully routine surgery. I'm lucky how understanding my colleagues and boss have been of the mental space I'm in right now.

It seems such misfortune is happening to [the best of us](https://antranigv.am/weblog_en/posts/2021-10-04-car-accident/)! All this is to say, I hope you're doing well. I'll be back soon :).

If you need something interesting to read in the meantime, I've [updated my blogroll](https://rubenerd.com/omake/#blogroll) on the Omake page. May I especially recommend [Hales](http://halestrom.net/darksleep/), [Asherah](https://kivikakk.ee/), [Antranig](https://antranigv.am/weblog_en/), [Wouter](https://brainbaking.com/), [James](https://jamesg.blog), [Josh](https://the.geekorium.com.au/), [Georgina](https://hey.georgie.nu/), [Nick](https://www.theredhandfiles.com/), [Om](https://om.co/), and [Doc](https://blogs.harvard.edu/doc/). 
