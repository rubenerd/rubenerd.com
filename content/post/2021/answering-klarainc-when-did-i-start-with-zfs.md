---
title: "Answering @klarainc: Where I started with ZFS"
date: "2021-05-08T09:54:44+10:00"
abstract: "OpenSolaris, then FreeBSD and NetBSD!"
year: "2021"
category: Software
tag:
- bsd
- freebsd
- netbsd
- zfs
location: Sydney
---
Klara Inc have done a great job of late advocating for FreeBSD, including its licencing and unique features. I can speak to the experience of pitching a solution with a FreeBSD component only to have people&mdash;whom you'd otherwise assume are well informed&mdash;not know what it is. Their most recent tweet asked what we started with ZFS on, and what we did with it.

I namedrop ZFS and OpenZFS here *a lot*, but as a primer for those unaware, it's a file system and volume manager designed for the express purpose of **making storage easier to administer**. The two key features for me:

* It offers a **simple terminal interface** (especially compared to the mess of stuff on Linux), and can expand from a single volume, to mirrors, to RAIDZ pools that are functionally similar to RAIDs.

* It offers **data integrity and consistency** features no other system has, meaning it's the only file system I trust for everything from mission-critical data, to precious personal information like family photos. I schedule regular scrubs (full ZFS integrity checks) in cron, and sleep better for it.

I started using ZFS back in the OpenSolaris days, mostly out of curiosity. Sun Microsystems had open-sourced their Solaris OS and tooling, which gave us this kickarse new storage system. I used [SunOS/Solaris at university](https://rubenerd.com/goodbye-to-solaris-probably/ "A goodbye post about Solaris, which mentions more of my experience") in the late 2000s, but OpenSolaris gave me the first experience with how simple ZFS was to use. I installed it on my old ThinkPad X40 with it back in the day, after Fedora had decided it didn't like my integrated graphics anymore. I even briefly [ran it on my MacBook Pro](https://rubenerd.com/opensolaris-mac-partition-order/). You could say the seed was well-planted by then, and thanks to ZFS, it wasn't about to be accidentally deleted.

*Which leads me to a patented Rubenerd.com digression, if you'll indulge me!* I've mentioned it here a few times, but ZFS isn't only useful for large data stores on a remote server. ZFS's integrity features make it *ideal* for laptops, because your data will remain consistent even during power loss or a borked suspend/hibernation. On workstations, ZFS snapshots let you upgrade and try new software without risk. 

FreeBSD's ZFS integration is the best out there, and is now where I use it for most of my systems. My homelab consists of a few FreeBSD servers running ZFS for data storage, as well as to simplify deployments of jails and Xen domu VMs. At work we use it to present storage over InfiniBand to our secondary VMware cluster, for clients who have workloads that can't run on our Linux Xen infrastructure.

And as for NetBSD, I started using ZFS with it starting with their pivotal version 9 release! Benedict and Allan sometimes read my silly posts here on their [BSD Now!](https://www.bsdnow.tv) show which makes me *unreasonably* happy, and I think my post about [trying encrypted ZFS on NetBSD](https://rubenerd.com/encrypted-zfs-on-netbsd-9-for-a-freebsd-guy/) was the first one they discussed.

I still haven't tried running ZFS on Mac or Windows, mostly because I offload whatever I can from those systems. Or DOS! Yes, wouldn't it be great for me to try a new CONFIG.SYS memory setting on my 486, and be able to roll back after the inevitable fireworks set in? Don't answer that.

Give [FreeBSD](https://freebsd.org/) with ZFS a try now, and ping [Klara Inc](https://klarasystems.com/) if you need enterprise-grade support for it. The *aha!* moment I had when I first tried ZFS a decade ago now was something rare and special; if you're at all interested in this stuff I think you'll like it.

