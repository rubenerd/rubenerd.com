---
title: "Some of these kitchen gadgets we do need"
date: "2021-06-18T15:50:32+10:00"
abstract: "Coffee bean grinders are amazing. Don’t believe the naysayers."
year: "2021"
category: Hardware
tag:
- appliances
- coffee
- food
- gadgets
location: Sydney
---
Speaking of [coffee](https://rubenerd.com/james-galligers-coffee-blog/), Tony Naylor wrote a *provocative (!!)* article earlier this month about [ten kitchen gadgets we don't need](https://www.theguardian.com/food/2021/jun/08/10-kitchen-gadgets-you-really-dont-need-from-garlic-presses-to-spiralizers#comment-149798619). I feel it's my responsibility to correct the record here on a few of them. 

To start, *need* is a loaded word. It's normal in places like Singapore to have very little in the way of kitchen utensils and appliances at all, becuase groceries are expensive and eating out is so affordable and quick. Even in Western kitchens, you could argue all you *need* is a fridge and microwave. Or if you can afford to not live out of microwave meals, maybe all you need is a sharp knife, pot, cutting board, and an induction hob.

With that lengthly disclaimer aside, let's take a look at these and whether the author was correct <span style="font-weight:bold; color:#0a4">✓</span> or wrong <span style="font-weight:bold; color:#d04">✗</span>.

* **Ice-cream maker**. <span style="font-weight:bold; color:#0a4">✓</span> Weak agree. I don't think you should eat much of this anyway.

* **Honey dipper**. <span style="font-weight:bold; color:#0a4">✓</span> Agreed, a spoon works fine.

* **Coffee bean grinder**. <span style="font-weight:bold; color:#d04">✗</span> *Hard disagree!* Maybe the UK has a dearth of good coffee beans, but freshly roasted stuff that's just been ground at home smells *incredible* and tastes great. Pre-ground has nothing on it.

* **Bread maker**. <span style="font-weight:bold; color:#d04">✗</span> We don't eat much bread, but I could see how they'd make fresh bread cheaper and more accessible. Sure, we can wash our dishes and clothes by hand, and only wear hand-weaved hats (hats?), but machines offer consistent quality when we're tired or don't have time.

* **Garlic press**. <span style="font-weight:bold; color:#0a4">✓</span> I'll admit to being suckered into these. Tony is right, all they do is make mess. One of those flat, handheld cheese graters works much better.

* **Manual spiraliser**. <span style="font-weight:bold; color:#d04">✗</span> Weak disagree. Eating vegetables is healthier than pasta. Anything you can get to make light work of making "vegetable" pasta should be encouraged.

* **Pasta machine.** <span style="font-weight:bold; color:#d04">✗</span> Same as the bread maker, I could see these being useful if you eat enough of it.

* **Poaching paraphernalia** <span style="font-weight:bold; color:#0a4">✓</span> Hard agree. Poached eggs are boiled eggs with needless faffing.

* **Electric juicer**. <span style="font-weight:bold; color:#0a4">✓</span> Hard agree. If you can, eat fruit instead with their natural fibres and ruffage that help you process the insulin-spiking sugars.

That leaves a final score of 50%. I *suppose* that's a pass.

