---
title: "My experience with an Australian bank broker"
date: "2021-03-26T23:08:29+11:00"
abstract: "This wasn’t as embellished as you might think."
year: "2021"
category: Thoughts
tag:
- conversations
location: Sydney
---
This is not as embellished as you might think.

> **Teller**: Good morning sir.

**Customer**: It sure is! How about that weather, though?

> **Teller**: Oh my yes sir, frightful. But it'll be easing soon.

**Customer**: Splendid, that would be rather delightful.

> **Teller**: Indeed sir. How may I assist you today?

**Customer**: Well you see, I'm in a spot of bother. A bit of a pickle. And I'm not sure from whence I should start.

> **Teller**: Is it a financial matter, sir?

**Customer**: No, something far more fundamental. The name on my account is wrong.

> **Teller**: The name on your account, sir?

**Customer**: Yes, the name. The middle one, to be precise.

> **Teller**: The middle account, sir?

**Customer**: No, the name. My middle name. It's wrong.

> **Teller**: On the account sir?

**Customer**: Correct.

> **Teller**: I see. Was this for a new account?

**Customer**: Indeed. I applied for a brokerage account at your reputable establishment online on one of those newfangled computers. But it was the darndest thing... upon receiving my acceptance letter, it had my name written as "Middle".

> **Teller**: Your middle name, sir?

**Customer**: Yes, my middle name.

> **Teller**: And what had it been set as, sir?

**Customer**: "Middle". It had been set as "Middle".

> **Teller**: I understand sir, your middle name. But what was it set to?

**Customer**: It was set to "Middle". As in, the word. 

> **Teller**: I see sir, so your name was set as Middle. That's strange. Which name was set as Middle?

**Customer**: The middle one.

> **Teller**: I understand sir, it was erroneously recorded as "Middle". But which name? Your surname?

**Customer**: No, no. Listen carefully. My *middle* name was set to "Middle". And my middle name isn't "Middle".

> **Teller**: Your middle name isn't "Middle", sir?

**Customer**: Correct.

> **Teller**: I see sir. So you wish to change your middle name?

**Customer**: I want the middle name on the account to be my correct middle name.

> **Teller**: Changing your middle name requires a Change of Name form sir, with a notorised copy of whatever documentation you have from the state registry.

**Customer**: The state registry?

> **Teller**: Correct, sir. From when you changed your middle name.

**Customer**: No, you don't understand. That's not my middle name. It never was my middle name. The account was created with the wrong name.

> **Teller**: The wrong middle name, sir?

**Customer**: Yes, the wrong middle name. My middle name is "Michael".

> **Teller**: You'd need a Change of Name form to change that sir.

**Customer**: I'm not sure you're understanding me. I didn't *change* my name, I want it *fixed* to what it's supposed to be.

> **Teller**: How did it get set as Middle, sir? Did you enter the wrong name when you registered the account?

**Customer**: I did not, I entered my middle name as "Michael". I wouldn't have written it as "Middle". That's not my name.

> **Teller**: Maybe you were confused or made a mistake, sir?

**Customer**: Normally I'm willing to entertain a mistake on my part, but not in this case. I uploaded all the supporting documentation your web form asked for, including bank statements. They all *clearly* show my middle name as Michael.

> **Teller**: I'm not sure I follow, sir?

**Customer**: What I'm saying is, I wrote out my full name, with "Michael". Your systems *verified* my name was correct with the supporting documentation I provided. Had I typed "Middle" as my middle name, this verification check would have failed, wouldn't it? Because it wouldn't have matched!

> **Teller**: I'm not sure, sir.

**Customer**: So somewhere between me completing your web form, and me getting a confirmation letter, my name was transcribed wrong. Either that, or by some fluke I *did* write my name as "Middle" absentmindedly, and your ID verification system is flawed. Frankly, I'm not sure which to be more scared of.

> **Teller**: Our systems are of the highest standard, sir. I'm sure you just wrote the name wrong.

**Customer**: At this stage I don't care who's at fault. I just need the name fixed. Can you do that for me?

> **Teller**: You'd need a Change of Name form to do this, sir.

**Customer**: But we've been over this! I haven't changed...

> **Teller**: No sir, not to worry, I lodged it electronically while we were talking. And I've already got the result back.

**Customer**: Oh good, finally! What does it say?

> **Teller**: The response says: Request for Name Change Denied. Documentation doesn't match.

