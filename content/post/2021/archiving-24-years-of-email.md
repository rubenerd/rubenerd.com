---
title: "Archiving 24 years of personal email"
date: "2021-10-10T15:19:31+10:00"
abstract: "Why am I still carrying all this around?!"
year: "2021"
category: Internet
tag:
- email
- gtd
- nostalgia
location: Sydney
---
I've carried the same email inbox since I got my own Hotmail account as a kid in 1997. It's been meticulously sorted into folders and carted between various email providers over the years, including Looksmart Mail, Gmail, a self-hosted Dovecot install, a shared webhost, and most recently Fastmail. If only I'd kept those Eudora email archives from our Singapore Pacific Internet email accounts from earlier still, imagine all the nostalgic fun.

But I digest. Wait, digress. Technically, I'm doing both right now.

I've been on a bit of a minimalism and cleaning binge recently, and began to wonder why I'm carting around all this old email. It's only about 10 GiB, but how often am I going back to read school work from 2004, or mailing lists for software that no longer exists? Does this need to all be on an IMAP server somewhere?

It's also increasingly limiting in other ways. My folders, filing system, and filters all start with the same letter in a most satisfying way, but admittedly haven't been useful for how I work now for years.

A small part of me also worries about identity theft. Email has become the front line for all manner of auth systems, thanks to its use as a fallback to reset passwords. But someone getting access to my archive would see all the subjects I did, all my jobs, who my family are, where I've travelled, what I've applied for, and dark secrets like that order for 12-year Glenfiddich because I like it more than the older stuff. You could probably scrape much of this from social media and what I publish like a silly gentleman on my blog here, but email would be much easier to mine.

I belabour all of this, to share that phrase with five words. This afternoon I've been bulk importing all my mail into a clean new Thunderbird profile and exporting them as eml files. I'll throw this onto my OpenZFS data backup pool, in case I ever want to search them. That's the great thing about glorified text files and HTML email, the latter of which I still resent having been introduced, and not just because it made using software like the console Alpine email client untenable. *But I digest.*

I've also decided to change how I sort mail. Instead of sorting based on person or subject, I'm trying out a Getting Things Done approach with folders for Do, Defer, Delegate, Done, and Deleted (aka, the Bin). I cheat a bit with another folder called Info, which is for receipts and other records. It's worked shockingly well for work email, so I'm going to give it a try here too. 📨

*(Mailing lists will still get their own dedicated folders, because mixing them into one place sounds a like a shortcut to going batty... even if they do have the mailing list title in the subject line).*

