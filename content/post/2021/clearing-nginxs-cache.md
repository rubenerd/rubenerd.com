---
title: "Clearing nginx’s cache"
date: "2021-05-23T23:20:41+10:00"
abstract: "As Igor Sysoev mentioned a decade ago."
year: "2021"
category: Software
tag:
- bsd
- debian
- freebsd
- netbsd
- guides 
- web-servers
location: Sydney
---
[Igor Sysoev confirmed](http://nginx.2469901.n2.nabble.com/best-way-to-empty-nginx-cache-td3017271.html) this was all that was necessary, at least at the time:

    find /path/to/cache -type f -exec rm {}\;

I suppose that makes sense!
