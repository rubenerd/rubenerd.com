---
title: "Great bird site quotes this week"
date: "2021-06-03T08:37:06+10:00"
abstract: "The Marshall Project, Josh Simmons, and @\_BADCATBAD"
year: "2021"
category: Thoughts
tag:
- quotes
- twitter
location: Sydney
---
[The Marshall Project](https://twitter.com/MarshallProj/status/1400217134966390791) quoted Jenn Budd, a former senior US Border Patrol agent:

> You can't deter people running for their lives.

[Josh Simmons](https://twitter.com/joshsimmons/status/1399881224307437571) from the Open Source Initiative:

> Communities dominated by people with the privilege of copious free time.

And my friend [@\_BADCATBAD](https://twitter.com/_BADCATBAD/status/1400047043477872642) from uni:

> One day I will get "none of your business" business cards.

