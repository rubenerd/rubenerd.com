---
title: "Ain’t no scheduled posts"
date: "2021-02-17T08:27:56+11:00"
abstract: "This was the first month where I didn't have scheduled posts lined up."
year: "2021"
category: Thoughts
tag:
- personal
- weblog
location: Sydney
---
This was the first month where I didn't have scheduled posts lined up. Sometimes I cheat and set up a dozen or so entries to be released over a period of days to prevent spam.

In the old days people used to joke that they could tell how busy I was based on the frequency and length of posts!
