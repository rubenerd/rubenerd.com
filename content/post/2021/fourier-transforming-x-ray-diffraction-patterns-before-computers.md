---
title: "Fourier transforming X-ray diffraction patterns before computers"
date: "2021-07-27T13:55:08+10:00"
abstract: "This was another fascinating peek into slide rules, pre-computed books of values, and a lot of patience."
year: "2021"
category: Hardware
tag:
- history
- medicine
location: Sydney
---
I came to newbedev's site for some help solving a specific FreeBSD problem, but stayed over lunch reading chemistry Q&A posts. I'm glad someone out there set the [record straight](https://newbedev.com/cleaning-with-vinegar-and-sodium-bicarbonate "Cleaning with vinegar and sodium bicarbonate") on the efficacy of vinegar and bicarb soda as cleaning products, for example.

[Here's their answer](https://newbedev.com/how-were-x-ray-diffraction-patterns-deciphered-before-computers "Chemistry - How were x-ray diffraction patterns deciphered before computers?") for how x-ray diffraction patterns were deciphered before computers:

> Diffraction data was measured on film, with gray-scales to assess intensity of signals. To calculate a Fourier transform, pre-computed tables were used, such as the Beevers-Lipson strips. As Andselisk commented, Fourier transform was used late in the 20s, and initially for problems that were one- or two-dimensional.
>
> Not just in the '20s but up to the '90s at least, the d-spacings were estimated by hand measurements of diffractometer peaks or film lines and applying the Bragg formula.

I had to look up what a [Beevers-Lipson strip](https://en.wikipedia.org/wiki/Beevers%E2%80%93Lipson_strip) was:

> Beevers–Lipson strips were a computational aid for early crystallographers in calculating Fourier transforms to determine the structure of crystals from crystallographic data, enabling the creation of models for complex molecules [..] The approach converted the sizable calculations of multi-dimensional Fourier summations needed in crystallography analysis into sums of more manageable one-dimensional values.

I still can't get over how much civil and commercial engineering was done with slide rules, pencils, carrying 1s, and thick volumes of constants and pre-computed values. My dad was a chemist, maybe one day he'll let me rifle through his prized Merck tomes.

Cryptography is the closest analogue (hah!) I can think of in my line of work, assuming that I went from maintaining and building computer systems to shipping physical envelopes around if computers ceased to exist. RSA is easy to grok, but imagine having to encrypt a packet with AES and encrypting that key with RSA for secure transmission... by hand. I suppose we wouldn't need such bullet-proof ciphers if we weren't threatened by fast computers that could make light work of them.

So much of our world is dependent on maths being handled elsewhere, transparently or otherwise.

