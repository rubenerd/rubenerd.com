---
title: "Beginning Pokémon Brilliant Diamond"
date: "2021-12-28T15:08:05+11:00"
abstract: "I think the only one paralysed from my wit was Sourdough."
thumb: "https://rubenerd.com/files/2021/pokemon-starter-2@1x.jpg"
year: "2021"
category: Media
tag:
- games
- nintendo
- pointless
- pokemon
- switch
location: Sydney
---
<p><img src="https://rubenerd.com/files/2021/pokemon-starter-1@1x.jpg" srcset="https://rubenerd.com/files/2021/pokemon-starter-1@1x.jpg 1x, https://rubenerd.com/files/2021/pokemon-starter-1@2x.jpg 2x" alt="Me: Go! Sourdough!" style="width:500px; height:281px;" /><br /><img src="https://rubenerd.com/files/2021/pokemon-starter-2@1x.jpg" srcset="https://rubenerd.com/files/2021/pokemon-starter-2@1x.jpg 1x, https://rubenerd.com/files/2021/pokemon-starter-2@2x.jpg 2x" alt="Me: I called him Sourdough, because he was my Starter!" style="width:500px; height:281px;" /><br /><img src="https://rubenerd.com/files/2021/pokemon-starter-3@1x.jpg" srcset="https://rubenerd.com/files/2021/pokemon-starter-3@1x.jpg 1x, https://rubenerd.com/files/2021/pokemon-starter-3@2x.jpg 2x" alt="Her: dot dot dot!" style="width:500px; height:281px;" /></p>
