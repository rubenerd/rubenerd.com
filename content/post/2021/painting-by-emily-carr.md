---
title: "Painting by Emily Carr"
date: "2021-03-21T16:31:32+11:00"
abstract: "Odds and Ends from the 1930s"
thumb: "https://rubenerd.com/files/2021/emilycarr-oddsandends@1x.jpg"
year: "2021"
category: Media
tag:
- art
- painting
location: Sydney
---
<p><img src="https://rubenerd.com/files/2021/emilycarr-oddsandends@1x.jpg" srcset="https://rubenerd.com/files/2021/emilycarr-oddsandends@1x.jpg 1x, https://rubenerd.com/files/2021/emilycarr-oddsandends@2x.jpg 2x" alt="" style="width:500px" /></p>

Emily Carr's beautiful *[Odds and Ends](https://en.wikipedia.org/wiki/Emily_Carr#Focus_shift_and_late_life)* made it to Wikipedia's image of the day yesterday. It depicts her concern over European deforesation of British Columbia in the 1930s.

I know nothing about art, but they way she depicted such rich colour, depth, and detail with broad strokes was so unique and powerful.

