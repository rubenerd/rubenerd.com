---
title: "When your customers want your service down"
date: "2021-05-15T10:04:54+10:00"
abstract: "Video conferencing systems going down are met with delight and relief. What does that tell us?"
year: "2021"
category: Internet
tag:
- covid-19
- psychology
- video-conferencing
location: Sydney
---
There's a specific online video conferencing system that has had a rough time with reliability over the last year when compared to other platforms. It might start with *M* and end with *icrosoft Teams*. I consider it the classic Windows experience, only with outages in lieu of blue screens, broken updates, and corrupted FAT disks.

But there's been an interesting phenomena associated with these downtime reports. People clamour on their support Twitter account to ask for the service to *remain down* for longer. For each person asking tongue-in-cheek, there's certainly someone asking seriously, or are delighted and relieved that the service is down.

Think of how many other systems people buy that they *don't want working*. Granted the adage states that those who choose and buy enterprise software are rarely the ones who are subjected to its daily use, but it's a grim assessment of people's feelings!

I wrote last November about [video conferencing fatigue](https://rubenerd.com/video-conference-call-fatigue/). There's a growing body of evidence that such systems take more cognitive load than physical interaction, text-based chat, and phone calls.

Some video conferencing is useful, but it's applied too broadly for people's mental well-being and productivity. Managers aren't entirely to blame here; COVID caught us by surprise, and software companies have billed them as a panacea for remote work, which they absolutely are not.

I think this attitude towards Teams also has an element of *let me do my job*. I'm lucky that I work at a company without micro-managers, but I'm sure there are others who'd rather do their work than have to waste time checking in with a manager to discuss the tasks they're not doing because their energy is being spent on dozens of video calls.

This period of time we're living through now will be analysed for decades. I'll be interested to see where psychology takes us in understanding these things. Video conferencing software brought us closer together when we had to be far apart, but they bubbled up so much more to the surface. I suspect a large percentage of the population will be glad to be rid of it once normalcy&mdash;if and when that happens&mdash;eventuates.

