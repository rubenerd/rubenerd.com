---
title: "Tech people starting their sentences with “So, ...”"
date: "2021-06-06T09:16:20+10:00"
abstract: "We can do better."
year: "2021"
category: Thoughts
tag:
- language
location: Sydney
---
So you're sitting there, catching up on your social media accounts, RSS feeds, and news articles. Maybe you've committed to watching a webinar where the presenter is answering questions, or a YouTube video by an indie presenter discussing a topic for which you have an interest. What do they all have in common, besides communicating in a language you understand, and being disseminated on the Intertubes? *"So, ..."*

So, it turns out, it's everywhere. Every journalist and their dog did articles about this verbal tick a decade ago, but it's identification hasn't put a dent in popular usage. It's usage is *increasing*, at least based on my own anecdotal evidence. I catch myself doing it too.

So, there may be a few reasons for this. Some of the articles quoted The Zuck as popularising it, but whether he was the source or not, I've seen it lace all manner of communications from Silicon Valley. My theory is the industry subconsciously picked up on it, and began using it to *sound more tech*. So, it then seeped into popular culture, much like software versioning by saying a building or a food is a "two point oh" release.

So whatever utility prefacing sentences with "so" once had, it's pervasiveness and cringe factor have lead to it becoming a cliché. So, we can all do better.

