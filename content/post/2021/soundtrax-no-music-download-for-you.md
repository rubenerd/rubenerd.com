---
title: "Soundtrax: No music download for you"
date: "2021-08-07T15:28:45+10:00"
abstract: "An album that came out in 2021 included a card for a non-existent site."
year: "2021"
category: Internet
tag:
- goodbye
- media
location: Sydney
---
A vinyl record I bought this year included a card for a digital download version of the songs. I figured why not give it a try:

> **DIGITAL DOWNLOAD.** Go to www.soundtrax.com. Enter the 8 digit case sensitive code printed below. Click "GET YOUR MUSIC"

I went to Soundtrax:

> **After millions of successful downloads, it's time to say goodbye.** In January 2021, Adobe ceased to support Flash, the platform on which the Soundtrax engine was built and on which it operated since 2008, thus ending our ability to support downloads. We apologize for any inconvenience.

The album came out *after* January 2021. Sometimes I feel like I'm living in a parallel universe; lots of synchronised bits, and no destination.

