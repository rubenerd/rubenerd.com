---
title: "Henry David Thoreau"
date: "2021-07-06T19:46:05+10:00"
abstract: "My life has been the poem I would have writ; But I could not live and utter it."
year: "2021"
category: Thoughts
tag:
- quotes
- microsoft-bookshelf
- henry-david-thoreau
location: Sydney
---
From *Bartlett's Familiar Quotations*, as presented on my Microsoft Bookshelf 1991 CD-ROM on my Pentium 1 tower this evening:

> My life has been the poem I would have writ,   
> But I could not live and utter it.
> 
> <cite>~ A Week on the Concord and Merrimack Rivers [1849]</cite>

And this helped me more than I expected:

> The fate of the country [..] does not depend on what kind of paper you drop into the ballot box once a year, but on what kind of man you drop from your chamber into the street each morning.
> 
> <cite>~ Slavery in Massachusetts [1854]</cite>
