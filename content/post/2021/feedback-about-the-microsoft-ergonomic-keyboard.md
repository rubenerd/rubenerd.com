---
title: "Feedback about the Microsoft Ergonomic Keyboard"
date: "2021-07-21T13:34:14+10:00"
abstract: "Questions from readers about USB-C, comfort, comparing to the 4000, and comparing to other keyboards."
year: "2021"
category: Hardware
tag:
- feedback
- keyboards
- microsoft
- sailor-moon
- sailor-mercury
- pointless-anime-references
location: Sydney
---
My post yesterday about [my new split keyboard](https://rubenerd.com/the-microsoft-ergonomic-keyborad-2019/) generated a few questions already; thanks everyone for reaching out. I've summarised them below:

**Is it USB-C, or does it come with an adaptor?**
: It's USB-A, and unfortunately doesn't come with an adaptor. Granted it's a budget device, and I use it with a dock anyway, but a passive adaptor in the box would be good.<p></p>

**How is it more comfortable?**
: It comes down to the position of my hands, wrists, and arms when using it. The split also forces you to use the correct hand for the keys you're typing, which is a good habit to foster if your touch-typing skills are as weak as mine.<p></p>

**How does it compare to the previous 4000?**
: Aside from being significantly less ugly, they also removed some of the controls I didn't use. The keys also feel slightly crisper and more responsive, though that might be a function of its age. I personally think it's a huge improvement, but reviews online are mixed. Check what features matter to you before committing; the 4000 might still be a better fit.<p></p>

**Your typed content is no wittier than before**
: Quiet, self-doubt!<p></p>

**Have you tried $KEYBOARD instead?**
: Probably not. I did try a Kinesis Freestyle Pro back in the day, but it didn't feel better enough to justify the price delta, and it frustrated me that they were two separate pieces that could move around. I'd be willing to try more in the future; this was mostly just to test if split keyboards were the way for me to go again. Keep sending me recommendations!<p></p>

**Can I steal the Sailor Mercury fig you linked to?**
: No! But I can vouch for her awesomeness. Game prize figs have come a long way. One day I'll get Sailor V or Sailor Venus as well, if I can ever find her.


