---
title: "Kotobukiya’s Bishoujo Remix Miku"
date: "2021-07-06T13:03:03+10:00"
abstract: "Clara and I love seeing these mature versions of everyone’s favourite virtual idol."
thumb: "https://rubenerd.com/files/2021/Bishoujo-Remix-Series-Hatsune-Miku@1x.jpg"
year: "2021"
category: Anime
tag:
- anime-figs
- hatsune-miku
- reviews
location: Sydney
---
Clara's and I still love Miku's 2011 Good Smile Racing rendition above every rendition of her since. The subtle shape Yuichi Murakami gave her eyes convey the same friendly vibes as her millions of other incantations, but with a bit more maturity and class.

Here she is next to her cute 2016 version for comparison, where the difference is *somewhat* obvious!

<p><img src="https://rubenerd.com/files/2021/miku2011-2013-comparison@1x.jpg" srcset="https://rubenerd.com/files/2021/miku2011-2013-comparison@1x.jpg 1x, https://rubenerd.com/files/2021/miku2011-2013-comparison@2x.jpg 2x" alt="Photos of her 2011 rendition (left) and 2016 (right)." style="width:500px; height:400px;" /></p>

Her 2011 racing fig and artwork were a bit of an anomaly, admittedly. Miku is supposed to be one of the cute and energetic Vocaloid singers, with Luka bringing the deeper voice and calmer disposition. But Miku is nothing short of a blank canvas for so many artists and designers, so I'm always interested to see what directions people take her in.

Kotobukiya's recent Bishoujo Remix Series Miku takes it one step further, and shows her perhaps as she looks in her mid-20s rock stage! I love figs that look like action snapshots in time. The detail is unreal, especially around her billowing twintails and cape. Yamashita Shunya's original illustration has been captured so well.

<p><img src="https://rubenerd.com/files/2021/Bishoujo-Remix-Series-Hatsune-Miku@1x.jpg" srcset="https://rubenerd.com/files/2021/Bishoujo-Remix-Series-Hatsune-Miku@1x.jpg 1x, https://rubenerd.com/files/2021/Bishoujo-Remix-Series-Hatsune-Miku@2x.jpg 2x" alt="Photo of Bishoujo Remix Series Miku" style="width:500px" /></p>

What *would* Miku look like in another decade, if she were a mere mortal like us and not living in a futuristic synthesiser? Would she be wearing a beret in her mid-30s jazz funk stage? Maybe I'll ask Clara to think about and draw for us :).

