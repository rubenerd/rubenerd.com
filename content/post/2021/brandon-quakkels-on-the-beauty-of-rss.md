---
title: "Brandon Quakkelaar on the beauty of RSS"
date: "2021-10-24T09:16:35+10:00"
abstract: "Avoid flamewars and doomscrolling, use RSS!"
year: "2021"
category: Internet
tag:
- indieweb
- rss
- syndication
location: Sydney
---
[Brandon Quakkelaar's blog](https://quakkels.com/) ([RSS feed here](https://quakkels.com/index.xml)) was referenced on Lobse.rs yesterday, and I've been getting stuck into his [post about RSS](https://quakkels.com/posts/rss-is-wonderful/)\:

> I am not a big social media guy anymore. Over the last few years I’ve been actively avoiding it. I’m not a fan of each platform’s privacy concerns, and users need to be very cautious to avoid flamewars and infinite doomscrolling. If we aren’t careful, social media’s default state seems to devolve into just destroying trust and goodwill. I prefer society in real life.

*Preach*. I've made my peace with social media again, in part by committing to not taking it too seriously, and by maintaining an exhaustive list of blocked words, phrases, and hashtags. The network effect is strong among my friends and people I care about, though I'm deliberately limiting my exposure.

His antidote? RSS! This is the best summary I've ever read of it:

> With RSS we can curate our own feed of information. We can collect feeds from all the blogs we like, and we can get notified of new posts by subscribing with an RSS Reader. Readers will aggregate posts and list them chronologically for us. When the blog publishes content to their RSS feed, then it will be in our RSS reader without being subject to an invisible ranking algorithm like that which exists in social media. RSS is far more honest in that way.

He also introduces his [RSS Discovery Engine](https://rdengine.herokuapp.com/), which can be used to find links to other related blogs.
