---
title: "Ashe on motivational interviewing"
date: "2021-07-27T09:27:16+10:00"
abstract: "Feedback from my post last month about fostering better habits."
year: "2021"
category: Thoughts
tag:
- feedback
- physchology
- personal
location: Sydney
---
In early June I wrote about [motivational interviewing](https://rubenerd.com/motivation-myths/) as a positive way to affect change. Berating yourself to change a habit usually doesn't work, so why do we think that doing that to others will be any different?

Asherah Connor of [kivikakk.ee](https://kivikakk.ee/) (<a href="https://kivikakk.ee/atom.xml">web feed here</a>) emailed me a while ago (sorry!) with an <a href="https://psyche.co/guides/how-to-get-motivated-to-make-positive-changes-in-your-life">article in Psyche</a> by Angela and Ralph Wood, who help to break it down:

> How can we find more motivation to make positive changes in our lives?
> 
> MI practitioners use their counselling skills, such as open-ended questions and ways to reflect, to evoke what’s called change talk – a conversation about what clients are unhappy about and how they’d like to change. Through an accepting, collaborative and guiding style, this approach seeks to strengthen the person’s commitment to goals they identify for themselves.

This is the best summary I've read about the approach. Now we just need, as Ash says, the *motivation* to do it.

*(And as a pointless aside, I think the stock photo they included in the article is of [Tania Cagnotto](https://en.wikipedia.org/wiki/Tania_Cagnotto), an Italian Olympic diver. That's the [second mention](https://rubenerd.com/harukana-receive-and-olympic-sports/) of sport today. Is this a new record here)?*

