---
title: "What Apple mouses and Braun shavers share"
date: "2021-09-06T07:43:09+10:00"
abstract: "Grr, why can’t I use this shaver while charging?"
year: "2021"
category: Hardware
tag:
- apple
- design
- mouses
location: Sydney
---
Apple's Magic Mouse charging system is, like the design of this blog, the stuff of legend. The lightning connector located on the underside of the device necessitates it being [upside down](https://commons.wikimedia.org/wiki/File:Bad_design_-_Apple_Magic_Mouse_2,_unusable_when_charging_3.jpg) to connect and charge. It looks absolutely ridiculous, but also renders the device inoperable while charging. Their new wireless charging pad looks smarter, though they carries the same functional limitation.

Apple aren't alone with this design decision, unfortunately. Some wireless devices let me operate them with their charging cable attached, such as laptops and kitchen appliances. Others don't. Maybe Apple's purported inspiration from classic Braun industrial design carries over in more ways than one.

Take my Braun Series 7 electric shaver, a device that Twitter had a small meltdown over a few years ago when I admitted to having bought one. It didn't matter that it was more convenient and gentler to my sensitive skin, owning such a device was an affront to manhood. The fact so many people's masculinity were threatened based on my lack thereof was a bit funny. *Facts over feelings, amirite!?* But I digress.

I attempted to use this shaver this morning, only to discover that I'd been lazy enough charging it that it ran out of juice. No biggie I thought, as I connected its charging cable and tried again. Nothing happened. *Wow, the battery must really be dead*, I assumed. I plugged it into another socket just in case, and left it for half an hour. Same result.

At this stage I wondered if the device itself was dead, which would have been a proverbial bummer. Not that I'd ever need to use the device on that part of me anyway. This is strictly for use on the face, as unappealing that side of me is as well. I struggle to grow even the smallest beard (hence the shaving), but at least it's limited to my mug. What were we talking about?

I unplugged the shaver and tilted it to the light to read the label on the back of the device. Pressing the power button again startled me awake as it buzzed to life, with all the shaving vigor I'd come to expect over the last few years. I'm not sure who Vigor is and why I'm shaving them now too, don't they know we're in lockdown?

That lead me to run an experiment. With the shaver still buzzing in my hand, I plugged it back into the wall socket. The device immediately shut off, just as a regular person does after I regale them with another fantastic story about home appliances. I unplugged it again, and it buzzed back to life. I think Evanescence wrote a song about this, not that I even know how to pronounce their name all these years later. Evan-e-scence? *Schweppervescence?*

Please [reach out](https://rubenerd.com/about/#contact) if you're an electrical engineer and know why certain devices with batteries have this as a design requirement, and others don't. I wouldn't expect safety to be an issue for a computer mouse or an electric shaver that couldn't knick you by accident even if tethered to a wall. Perhaps the circuitry doesn't operate unless the battery itself has a minimum amount of charge? But then, if so, why do other cheaper and more expensive devices operate just fine?

Also, I'm aware that a straight razor doesn't have such electrical issues. Neither does the dish washer compared to the sponge and hand towel in our sink, or a book compared to the device you're reading this on. The modern world sure is something!
