---
title: "The M1 Mac: “seems fine”"
date: "2021-05-18T22:23:33+10:00"
abstract: "Real-world feedback from Clara on her new unit."
year: "2021"
category: Hardware
tag:
- apple
- mac
- m1
- universal-binaries
location: Sydney
---
Clara just got an M1-based MacBook Pro at work. Naturally I wanted to know if the software she was installing was running on Rosetta, and how well it performed. "Don't know, seems fine".

Credit where credit's due, that's an achivement.

<p><img src="https://rubenerd.com/files/2021/Mac_Universal_logo.png" alt="The Apple Mac OS X Universal badge" style="float:right; margin:0 0 1em 2em; width:87px; height:118px;" /></p>

I *knew* when PowerPC software was running in Rosetta on my [first-gen MacBook Pro in 2006](https://rubenerd.com/universal-binaries-for-mozilla-software/). It performed better than I expected, having grown up running x86 Windows in Connectix Virtual PC on PowerPC Macs. But my beloved old Macromedia Fireworks did struggle a bit when opening large images.

The world is a different place from the mid-2000s. The difference between the ARM architecture and x86_64 feels like a bigger leap than going from my PowerMac G5 to an Intel MacBook Pro, for example. Maybe the difference was more stark for those coming from PowerBooks with a long-in-the-tooth G4. I wasn't around for the Motorola to PowerPC transition, I wonder how that felt?

App stores and package managers have automated distribution so its not up to customers to look for software cartons sporting a *Universal Binary* badge. Few Apple users beyond professionals and hobbyists back then knew the difference between CPUs and how to select software for them, and I'll bet even fewer do now (at least, as a percentage of their userbase).

I've lamented how "boring" computing have become lately. I hadn't considered that for most people in the real world, that's *exactly* what they want.

