---
title: "The Raspberry Pi A+, and Pi-Hole"
date: "2021-12-15T13:09:11+10:00"
abstract: "Finding a use for one of my spare PIs I bought on impulse a few years ago."
thumb: "https://rubenerd.com/files/2021/photo-wipi-watson@1x.jpg"
year: "2021"
category: Hardware
tag:
- filtering
- privacy
- raspberry-pi
location: Sydney
---
I love the [Raspberry Pi](https://www.raspberrypi.com). I could never get into Arduinos, but these single-board computers have lowered the barrier to entry for so many personal projects. For less than we've spent on some meals, Clara and I have a couple of their Model B+ units with a [familiar FreeBSD console](https://wiki.freebsd.org/arm/Raspberry%20Pi) we can hack on. One of which will be related to some Commodore and DOS stuff I hope to talk about soon!

But I digress. I also bought the smaller, cheaper [Model A+](https://www.raspberrypi.com/products/raspberry-pi-1-model-a-plus/) on impulse when it debuted. It's a third smaller than the B+, with a single USB port instead of four, and no Ethernet. I thought it sat in the sweet spot between the larger B+ boards, while still being more practical than the USB stick-sized Picos. I never used all the USB ports on the B+, and who needs a keyboard once you have SSH configured?

While all that was true, that lone USB port did turn out to be its Achilles’ heel. Using a serial cable for initial configuration was fine, but the B+ was easier and therefore got more use. Buying the appropriate cables also ate into the cost advantage the A+ had. I may have also lost it in a house move (cough).

I even ran it briefly with a powered USB hub salvaged from another project, so I could use a keyboard and Ethernet dongle without one or both dropping constantly. This made the A+ bigger, more expensive, and have more cables than the B+... for a more limited machine! It worked, but it was a bit silly.

<p><img src="https://rubenerd.com/files/2021/photo-wipi-watson@1x.jpg" srcset="https://rubenerd.com/files/2021/photo-wipi-watson@1x.jpg 1x, https://rubenerd.com/files/2021/photo-wipi-watson@2x.jpg 2x" alt="Photo of the Raspberry Pi A+ with a Wi-Pi and Amelia WATSON!" style="width:500px; height:375px;" /></p>

Which leads me to [Pi-Hole](https://pi-hole.net/), something that sounds like a cronut or a similar culinary abomination. The DNS filter has existed for years, and it can run on any Linux machine now. But it started on the RPi, and I wanted an excuse to tinker with the Debian-based Raspberry Pi OS again.

I downloaded the latest image, used **[dd(1)](https://www.freebsd.org/cgi/man.cgi?query=dd)** to copy it across to a micro SD card, booted it off the card, and it just worked. To my surprise, my tiny Wi-Pi USB wireless NIC from one of my B+ machines drew such little power that I could connect it with a passive USB hub, and attach a keyboard. Nice!

Installation of Pi-Hole was as easy as any console-based tool I've ever used. I didn't think I needed the web-based GUI, and wasn't sure how well it'd run with the A+'s limited resources, but I opted to install it anyway.

It's been a month or so now, and the A+ has been sitting on top of my bhyve box with just a USB power lead and the Wi-Pi module, filtering our DNS. It works a treat, and the GUI uses very little memory. I suspect these were the sorts of use cases the A+ was designed for.

Our favourite Hololive time traveller Amelia Watson is there given the Raspberry Pi has no real-time clock. I was tempted to buy one, but NTP seems to work just fine when it boots. Part of the appeal of the A+ is it's just there, in the corner, affordably doing its thing.
