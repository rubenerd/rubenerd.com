---
title: "Minecraft won’t launch on macOS"
date: "2021-03-15T08:43:19+11:00"
abstract: "Run it from the app bundle instead if you have issues."
year: "2021"
category: Software
tag:
- apple
- games
- macos
- minecraft
- troubleshooting
location: Sydney
---
The latest update to Minecraft doesn't launch for me on macOS Catalina, though curiously it does when I run it from the application bundle:

    $ cd /Applications/Minecraft.app/Contents/MacOS/
    $ ./launcher

I haven't checked why yet, but give it a try if you have the same issue.

*Update: This post erroneously had the publish date set as Tuesday the 16th of March. This was still sufficiently far in the future even based on our current timezone to be entirely incorrect. Except for the year, which was correct. Though it still feels like 2020. You know what doesn't? Minecraft, because now I have a way to play it again. Even though I started in 2020. I should quit this addendum while I'm still ahead. At least, ahead in this timezone. Even though the year was wrong. Or, was it?*
