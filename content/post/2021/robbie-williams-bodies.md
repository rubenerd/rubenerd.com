---
title: "Robbie Williams: Bodies"
date: "2021-09-28T17:02:05+10:00"
abstract: "His Body Double Remix wasn’t double AT ALL. Wah lau eh."
thumb: "https://rubenerd.com/files/2021/robbie-williams-bodies@2x.jpg"
year: "2021"
category: Media
tag:
- music
- pointless
location: Sydney
---
Good evening everyone. This post does not qualify as a [Music Monday](https://rubenerd.com/tag/music-monday/) on account of it not being a Monday, and only tangentially being related to Music. Then again, neither was the last post that could best be described as tenously connected to Music. Why am I still capitalising Music?

<p><img src="https://rubenerd.com/files/2021/robbie-williams-bodies@1x.jpg" srcset="https://rubenerd.com/files/2021/robbie-williams-bodies@1x.jpg 1x, https://rubenerd.com/files/2021/robbie-williams-bodies@2x.jpg 2x" alt="Cover from the single." style="width:158px; height:158px; float:right; margin:0 0 1em 2em" /></p>

A decade ago during the Before Times&trade; I was on a family holiday in Europe. It felt like we'd been hearing Robbie Williams’ song *[Bodies](https://en.wikipedia.org/wiki/Bodies_(Robbie_Williams_song))* on the local [Bayern 5](https://en.wikipedia.org/wiki/BR24) radio station for weeks, so on a lark I picked up the single.

The disc included two songs:

1. *Bodies* – 4:05
2. *Bodies (Body Double Remix)* – 6:14

I was importing this ripped CD back into my [offline music collection](https://rubenerd.com/collecting-music-with-your-own-metadata/) when I noticed something horrifying. The *Body Double Remix* is **not** double the length of the original at all. *Wah lau eh*.
