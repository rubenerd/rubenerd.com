---
title: "George Benson, Dance"
date: "2021-10-25T13:58:11+10:00"
abstract: "If his guitar playing had been any smoother, it would have melted onto our floor."
thumb: "https://rubenerd.com/files/2021/yt-XW1aZJulEI4@1x.jpg"
year: "2021"
category: Media
tag:
- jazz
- music
- music-monday
location: Sydney
---
Today's [Music Monday](https://rubenerd.com/tag/music-monday/) is taken right from the LP spinning in our linear tracking, quartz locked, direct drive, track selecting [Technics SL-J300R](https://rubenerd.com/my-new-old-technics-sl-j300r-turtable/). If George's jazz guitar were any smoother, it would have melted all over our floor.

<p><a target="_BLANK" href="https://www.youtube.com/watch?v=XW1aZJulEI4" title="Play George Benson Dance (full version)"><img src="https://rubenerd.com/files/2021/yt-XW1aZJulEI4@1x.jpg" srcset="https://rubenerd.com/files/2021/yt-XW1aZJulEI4@1x.jpg 1x, https://rubenerd.com/files/2021/yt-XW1aZJulEI4@2x.jpg 2x" alt="Play George Benson Dance" style="width:500px;height:281px;" /></a></p>

