---
title: "Favourite game meme feedback"
date: "2021-09-15T20:04:38+10:00"
abstract: "With a link to the template I used."
year: "2021"
category: Software
tag:
- feedback
- games
location: Sydney
---
Mike Carter, [Wouter Groeneveld](https://brainbaking.com/), and Rebecca Hales wanted to know how I created my version of the [favourite game meme](https://rubenerd.com/favourite-game-meme/). Thanks for the kind words!

It was a quick and dirty job in Inkscape, which I exported as a PNG. Each panel is transparent, so I could open it in The Gimp and put each graphic behind it without worrying about getting the dimensions exactly right. You can [download the template](https://rubenerd.com/files/2021/favourites-meme.png) if you want to make one of your own.

I wanted each box readable in the narrower space on my blog, and I don't play all that many games, so I reduced the number of columns. With hindsight I should have used an HTML table so I could link to each picture, but it was a fun experiment.

Wouter also [linked me to the original](https://avoiderdragon.com/8071/favorite-game-meme-but-different/), which I reckon is probably still more interesting.

I don't post about games that often, but maybe I should. I think we all could use more levity.

