---
title: "Morning routines; tech and otherwise"
date: "2021-07-24T08:53:22+10:00"
abstract: "Mmm, coffee."
year: "2021"
category: Thoughts
tag:
- coffee
- covid
- personal
location: Sydney
---
I thrive on consistency, but we live in a world where that's not always available. Life has a sneaky habit of throwing wildcards at us, challenging us in ways in which we weren't prepared, and all those well-intentioned plans just sail away. I think [Enya sang a song](https://www.youtube.com/watch?v=LTrk4X9ACtw) about that.

Having a morning routine is the best way I've found to mentally prepare for the coming day. Even when I'm travelling I like to establish a little base of operations at a hotel, then develop an ad-hoc schedule for myself. If I can establish some structure and control in the morning, it gives me the mental energy and space to take on the rest of the day.

My body gets me up within minutes of 07:00 every day, regardless of whether it's a work day, weekend, or holiday. I've read papers that suggest waking up at the same time, even with insufficient sleep, is important to regulate your circadian rhythm; something I've always read as cicada rhythm. I used to joke that those people either don't have small children, or servers that beep at them at 03:30! I have no say in the matter regardless.

I got into a nasty habit years ago of checking my phone when I first wake up. If you're at all prone to anxiety like I am, this is terrible idea! The temptation is *strong*, but I try to resist it.

The next step is a shower. Always. Showers help me sleep, but somehow they also wake me up. If a shower isn't available, I have to at least wash my face. Travelling in my early 20s with uni friends was fun, but the variable nature of youth hostel showers have meant I'm glad I don't have to go to them anymore!

Pre-Covid, my next step was to leave the house with a small laptop for a brisk walk to a coffee shop. Both of these were just as important. I need fresh air, the sun on my face, movement, and the promise of a black coffee and a cheerful hello from a barista at the end of it! It's not just the taste, it's the atmosphere of a coffee shop that I find so stimulating and wonderful. The sound of the crockery, muffled conversations, the smell of the ground beans. A coffee shop in the morning is just about the best place in the known universe.

During Covid, I've substituted the former for pacing on our balcony, and making coffee using our Aeropress. Sydney winters are mild by global standards, but still nippy enough that people looking at me from the street must think I'm daft in all my layers. I keep a snug blanket on some IKEA patio furniture that I can drape over my lap and legs. I source the beans from local coffee shops here in the hopes that it helps them during this downturn... and because we have some excellent stuff around here!

Now I tackle the IT side of things. I check RSS first, then newsgroups, then monitoring dashboards, then email, then Mastodon, then Twitter if I'm feeling especially masochistic, and then my calendar for the day. It's everyone's civic duty and responsibility to stay informed and educated about what's going on in the world, but I try to avoid reading general news first thing. I might do some blogging or novel writing first thing, or save it for the evening.

I head back inside when its time for work or, on weekends, once Clara is up. Then the rest of the day is an open book. A clean slate. A metaphor!

This all might sound a bit silly, but this routine has helped even more during Covid times. I think we all feel anxiety over this not just for the physical symptoms or the fear that we'd spread a dangerous disease to a loved one or someone in our community; it also represents a loss of control. A routine lets me feel like I get just the smallest amount of that back.

