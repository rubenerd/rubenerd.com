---
title: "Our belated leave"
date: "2021-01-25T11:11:34+11:00"
abstract: "Trying to disconnect for a while up at my dad’s place."
thumb: "https://rubenerd.com/files/2021/dadsplaceday@1x.jpg"
year: "2021"
category: Travel
tag:
- clara
- personal
location: Sydney
---
Trying to disconnect for a while up at my dad's place. It's doing us good :).

<p><img src="https://rubenerd.com/files/2021/dadsplaceday@1x.jpg" srcset="https://rubenerd.com/files/2021/dadsplaceday@1x.jpg 1x, https://rubenerd.com/files/2021/dadsplaceday@2x.jpg 2x" alt="" style="width:500px" /><br /><img src="https://rubenerd.com/files/2021/dadsplacenight@1x.jpg" srcset="https://rubenerd.com/files/2021/dadsplacenight@1x.jpg 1x, https://rubenerd.com/files/2021/dadsplacenight@2x.jpg 2x" alt="" style="width:500px" /></p>

