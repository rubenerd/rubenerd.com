---
title: "Jokes at the expense of retail staff"
date: "2021-03-13T10:14:06+11:00"
abstract: "You don't pay them enough for them to feign amusement!"
year: "2021"
category: Thoughts
tag:
- ethics
- retail
- work
location: Sydney
---
Back in October I wrote about how surprised I was at the cavalier and flippant attitudes people have [towards homeless people](https://rubenerd.com/homeless-people-in-media/). I've felt a similar way for a while about comedians who make jokes at the expense of retail staff.

Here's one example:

> **YouTuber:** Hello, McDonald's? Do you sell *(represses sniggering)* the McHimmeny Jimmeny burger any more?
> 
> **Staffer:** I... could you repeat that please?
> 
> **YouTuber:** The McHimmeny Jimmeny! Come on, it's not hard!
>
> **Staffer:** I'm afraid we don't sir. Goodbye.
>
> **YouTuber:** HAHA! That guy sure didn't seem interested! Wasn't that fun!?

I wish I could say I was making that up. Okay, I was. But it follows the same cringy pattern each time: the staffer sounds confused, bored, or tired, and the comedian thinks they're being quirky and fun. I'm going to let them in on a secret: *you don't pay retail workers enough for them to feign amusement!*

You know they're the same kind of people who shout at trainees for shop policies decided by their managers, or for getting their order wrong out of the hundreds they must do each day. It's a power trip, plain and simple.
