---
title: "No, RSA is not broken"
date: "2021-03-19T09:17:02+11:00"
abstract: "Thank you, Bruce Schneier."
year: "2021"
category: Software
tag:
- bruce-schneier
- ed25519
- rsa
- openssh
- security
- ssh
location: Sydney
---
Remember all the news swirling around Twitter and the blogosphere (yes, I'm bringing the term back) about RSA being broken? [Bruce Schneier](https://www.schneier.com/blog/archives/2021/03/no-rsa-is-not-broken.html):

> I have been seeing this paper by cryptographer Peter Schnorr making the rounds: “Fast Factoring Integers by SVP Algorithms.” It describes a new factoring method, and its abstract ends with the provocative sentence: “This destroys the RSA cryptosystem.”
> 
> It does not. At best, it’s an improvement in factoring — and I’m not sure it’s even that. The paper is a preprint: it hasn’t been peer reviewed. Be careful taking its claims at face value.

I still encourage people to generate and use the unfortunately-named ed25519 keys in lieu of RSA given they're smaller and faster. But I'm more than a little relieved to hear this, and not just because I wrote an implementation of RSA in Perl for a second-year university assignment when everyone else was using Python and Java. `factor_THIS.pl`.

Edgar Gardner had the best response under the story:

> [..] if Schnorr could “destroy RSA”, he would have destroyed one of the RSA Challenge problems to prove it. He did not.

To be clear, is a phrase with three words. It's an *intrinsic good* for research demonstrating weakened cryptography to be published publicly, not least because there's a chance a malicious actor, state-sponsored or otherwise, has done it privately and might already be exploiting it.

