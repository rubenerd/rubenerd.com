---
title: "The XTree Fan Page"
date: "2021-04-13T12:24:53+10:00"
abstract: "I’m so glad people still remember this software, and care enough to document it."
year: "2021"
category: Software
tag:
- dos
- mmx-machine
- nostalgia
- xtreegold
location: Sydney
---
XTreeGold for DOS was one of the first programs I ever used as a kid, and it left a lasting impression. Text-based tools like Norton Commander couldn't touch it at the time, and thirty years later we're still using inferior GUI file managers. The ability to "tag" files across multiple directories and perform an action against them (say, copying) was a stroke of genius, as was its intuitive UI and hotkeys.

<p><img src="https://rubenerd.com/files/uploads/icon.xtreegold.png" alt="The bundled XTreeGold icon for Windows" style="height:64px; width:64px; display:inline; float:right; margin:0 0 1em 2em" />

I've got a longer post about XTree in the works, but for now I was so happy to find [this fansite by Mathias Winkler](https://www.xtreefanpage.org/). The web design is even reminiscent of the software's splash screen and UI.

My favourite page was on [hidden hotkeys](https://www.xtreefanpage.org/lowres/x43hhl.htm). I love finding easter eggs and hidden tricks in old software.

