---
title: "Benefit for all, via @PicardTips"
date: "2021-12-13T09:05:56+10:00"
abstract: "If one side of a conflict preaches purity of bloodline above communal benefit for all, join the other side."
year: "2021"
category: Thoughts
tag:
- ethics
- quotes
location: Sydney
---
This is [among his best](https://twitter.com/PicardTips/status/1463196283452096514) in a while:

> Picard morality tip: If one side of a conflict preaches purity of bloodline above communal benefit for all, join the other side.
