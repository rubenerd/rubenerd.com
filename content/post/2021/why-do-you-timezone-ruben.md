---
title: "Why do you timezone, Ruben?"
date: "2021-08-03T10:21:12+10:00"
abstract: "Answering why I always write in the future, and some blogging nostalgia writing from overseas."
year: "2021"
category: Internet
tag:
- feedback
- timezones
location: Sydney
---
A gentleman by the name of Eric emailed this comment:

> Why do you always write your articles in the future?

I take it he's in the Western Hemisphere. It's not the future where I am, except when I steal Amelia Watson's time-travelling watch. But I take the point; timezones are hard.

My blog here briefly showed both my localtime and UTC under each post. I got rid of the latter because I thought it made things *more* confusing. Maybe I need to go back to calling out my timezone to each post.

While I'm talking about these spherical anomalies of time and space, handing timezones was one of the last nails in the coffin of WordPress for me back in the day. I used to write from multiple places in The Before Times&trade; and when I was studying overseas. To this day, most of my posts from 2005-12 were filed with +08:00 for Singapore and KL, even though half the posts were from Adelaide or Sydney. That might not sound like much, but I also used to write late into the night. That discrepancy of a couple of hours was enough to push hundreds of posts into the wrong day. *The horror!*

It's probably not worth sweating over having the exact time on a blog like this. But as my dad always said, a task worth doing is a task worth doing right. Then he hit his head on a kitchen cabinet.

