---
title: "Comparing PC all-in-ones to the iMac"
date: "2021-07-22T21:47:25+10:00"
abstract: "No surprise, the PC versions are barely cheaper, and are much worse. But grrrr soldered storage."
year: "2021"
category: Hardware
tag:
- apple
- asus
- pc-screen-syndrome
- desktops
- imac
location: Sydney
---
I love that colours are back with the iMacs, though their soldered storage makes them a non-starter for me. Still, I thought I would be an interesting experiement to see if PC makers care at all to compete with Apple in the AiO market segment, or whether they're saddled with *[PC Screen Syndrome](https://rubenerd.com/tag/pc-screen-syndrome/)*.

I checked a few Australian online stores this morning, and the results were as surprising as discovering that I'm a bit awkward in real life:

* [Acer](https://online.acer.com.au/acer/store/desktop/all-in-one-pc), with the only online store that doesn't make me want to hurl web developers into the sun, sell Aspire C27 27-inch machines from $1,400 to $1,900. Both have low-resolution, low-density 1080p displays. 

* [Asus](https://www.asus.com/au/Displays-Desktops/All-in-One-PCs) is the only PC maker making AiO devices with any design sense, and their laptops have HiDPI displays, so I held out hope. They sell M241, V241, V161, and V222 23.8-inch to 27-inch machines for (price withheld). All have low-resolution, low-density 1080p displays. Asus, no! You were supposed to be one of the good ones!

* [Dell](https://www.dell.com/en-au/shop/dell-desktop-computers/sr/desktops/all-in-one?appliedRefinements=31469) make Inspiron 23.8 and 27-inch machines, from $1,700 to $2,300. All have low-resolution, low-density 1080p displays. Not surprised.

* [HP](https://www.hp.com/au-en/desktops-computers.html#filters_explore_collections_section=type-type_aio) make Pavilion and HP-branded 23.8 and 27-inch machines, from $700 to $3,000. All have low-resolution, low-density 1080p displays. Clicking on their 34-inch Envy lines returned no results, but the US store says they're 1440p, which is still low-density and resolution for the size.

* [Lenovo](https://www.lenovo.com/au/en/desktops-and-all-in-ones/c/DESKTOPS#type=TYPE_ATTR3) make V30a, V50a, and ThinkCentre M90a 22 to 24-inch all-in-one machines, from $1,429 to $1,629. All have low-resolution, low-density 1080p displays. Several of the ThinkPad models have *excellent* screens, so this was a surprise.

I'm seeing more IPS panels, which is *fantastic!* Some even have touch screens, something I consider a bit of a gimmick but that Apple doesn't ship with. But these specs and prices simply don't compete with a 24-inch iMac with a 4.5K display for $1,900, and a 27-inch 5K iMac for $2,700. **You could fit four of those AiO computer screens into one iMac**, and the price is barely different!

Apple have sold Retina/HiDPI screen iMacs for *years*, yet PC makers continue to ship displays with a lower resolution than your phone. Yes I sound like a broken record here, but it's only because it continues to flabbergast me! That's a word, right?

You might think the all-in-one desktop segment makes no sense. They don't for my use cases, either. But the fact PC makers are fine ceding technical superiority to Apple in the interest of pushing 1080p panels that were outdated a decade and a half ago, and for the same money, makes as much sense as enlisting me for your basketball team. Sure I have the height, but my hand-eye coordination is off *on account of looking at crappy screens!*

Focus, Ruben.

This leads me to something I've been mulling over for a while now. Why have PC makers lost that spark? Is it margins? Has the mindshare and innovation moved on to phones? Is it Microsoft failing to provide compelling updates for Wintel boxes that would make effective use of the new tech? Are the bean counters in charge of engineering? It seems gaming rigs are the only place where innovation is happening anymore, and have you seen some of those designs? Even teenage Ruben would look at those RGBs and say "that's a bit much, [innit](https://rubenerd.com/robbie-williams-good-doctor/)?"

I still remember a time when PC makers could tout the fact that while Apple computers *looked* better and arguably had a better OS, they had better tech for a cheaper price. All they can claim now in an M1 world is wider compatibility, and even then the industry seems to be moving towards ARM at break arm speed. Get it, instead of break neck speed, because it's called... oh shut up.

I've never exclusively been a Mac user. I've tended to use the Mac as my primary desktop/laptop, but delegate as much as I can to a tower and home servers running FreeBSD, NetBSD, and/or Debian Linux as required. The former gave me the best desktop experience to run work applications, and the latter let me tinker and build to an exact specification and price. I haven't been in the market for a pre-built PC for years, but it's been grim every time I've looked. 

The tech is there, PC makers. *Please* lift your game!
