---
title: "Cosmopolitanism in a cup"
date: "2021-12-24T09:23:58+11:00"
abstract: "So many accents at my local coffee shop this morning!"
year: "2021"
category: Thoughts
tag:
- coffee
- culture
location: Sydney
---
Sitting at this coffee shop on Christmas eve, the barista who served me is Indonesian, and his coworker is from Argentina. The guy at the table next to me is talking on his phone in Cantonese, and the one opposite is speaking Japanese. Another customer just arrived who sounds Filipino, and the one who came in before her sounded suspiciously Liverpudlian. And to top it off, another person just arrived who has an accent just like Clara's, so her parents might be from Hong Kong or the surrounding area too.

This is one of the things I love about Sydney, and what I loved about Singapore and San Francisco. Must be something to do with S.
