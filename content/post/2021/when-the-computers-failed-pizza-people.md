---
title: "When the computers failed pizza people"
date: "2021-09-25T09:20:25+10:00"
abstract: "A failed update, and some angry people waiting for food."
year: "2021"
category: Software
tag:
- food
- irl
location: Sydney
---
Last night was Friday, so Clara and I decided to spluge and get some pizza. It's basically carbs and sugar, so we limit our intake of the stuff when we can. This also means that I'm not as familiar with the ordering process as other things.

We decided to check out our local chain's iOS application while on our evening walk, which was as bad as all the one star reviews indicated. I was surprised that in 2021 a glorified web wrapper was even still allowed on the platform; I thought Apple had clamped down on it. Maybe older software got grandfathered in.

We ordered our pizza, then saw the estimated cooking time as forty-five minutes. This seemed reasonable; Friday night would almost certainly be their busiest time, and we weren't in a hurry. But then the estimated time flashed and changed to ten minutes.

The situation at the pizza place when we arrived could be best described as *tense*. We'd only managed to get there half an hour later, so the overworked staffer told me I only needed to wait a few minutes more. But it was evident that other people had seen the same time issue on their phones, and were demanding pizza that wasn't ready yet.

> Aiyo, your app says the pizza was ready twenty minutes ago, now you're telling me its another twenty minutes!? Strewth!

He said something far more vulgar than aiyo and strewth, but I figured I have an international audience that expects a certain degree of Australian and Singaporean colloquialisms.

The few minutes waiting gave me some time to think about how we got to this situation. Was there an internal API the mobile application was polling every X amount of time, or was it a push notification? Was the local client making a best guess about the time to cook based on the time of day and week, then getting overridden by erroneous data? Could it have been as simple as one of the servers not having time in sync, or was there a race condition? I've been involved in enough complex systems now to know that time is not something you want to mess with.

But while interesting to contemplate, it soon lead me down the path to real-world consequences. This small blip in an application almost certainly made the lives of these people infinitely harder that night. How many times did the people at the counter have to explain, or their manager? How many irate or entitled customers who are used to treating retail staff like rubbish did they have to endure?

Computers, much like the economy, are built for people. Or at least, they're supposed to be. The worst feelings I've had in this occupation are the thoughts that a system I've designed or helped build have worked against people in some failure scenario. I hope those pizza folks made it through the night okay.
