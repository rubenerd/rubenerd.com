---
title: "The tech in fintech"
date: "2021-04-09T08:11:28+10:00"
abstract: "... is a ton of people behind the scenes doing dur dilligence."
year: "2021"
category: Software
tag:
- finance
- fintech
- overheard
location: Sydney
---
I overheard this at a coffee shop this morning and can't get it out of my head:

> The *tech* in *fintech* is a tonne of spreadsheet warriors behind the scenes doing due diligence.

And does blockchain provide value?

> For M&A compliance? No. For dodgy forex trades? Yes!

