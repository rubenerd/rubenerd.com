---
title: "NetBSD 9.2’s new default package DB location"
date: "2021-06-04T09:50:15+10:00"
abstract: "It’s now /usr/pkg/pkgdb, to be consistent with other platforms. Nice!"
year: "2021"
category: Software
tag:
- bsd
- netbsd
- package-managers
- pkgsrc
location: Sydney
---
Speaking of [NetBSD](https://www.netbsd.org/) and [pkgsrc](https://www.pkgsrc.org), version 9.2 was released on the 18th of May. It included this, according to the [release notes](https://www.netbsd.org/releases/formal-9/NetBSD-9.2.html)\:

> [pkg_add(1)](https://man.netbsd.org/NetBSD-9.2/i386/pkg_add.1): moved the default package database location on new installations from /var/db/pkg to /usr/pkg/pkgdb, for consistency with the pkgsrc bootstrap and pkgsrc on other platforms. It can be overridden in [pkg_install.conf(5)](https://man.netbsd.org/NetBSD-9.2/i386/pkg_install.conf.5).

Nice!

