---
title: "Stephen Diehl on NFTs"
date: "2021-10-22T15:30:49+10:00"
abstract: "A thread from earlier this month was the best I’d read about Non-Functional Transactions."
year: "2021"
category: Internet
tag:
- blockchain
- finance
- money
location: Sydney
---
For those lucky enough not to know what Non-Functional Transactions are, [Stephen Diehl explained](https://twitter.com/smdiehl/status/1445795705411579904?s=20) earlier this month the pitfalls of buying a pointer to an artwork:

> NFTs impart no legal ownership, give no rights to the artwork, are non-unique, and provide nothing of intrinsic value except the sign value of owning bragging rights to announce to other crypto bros about a shared collective delusion about database entries.

If this sounds familiar, you've [paid attention to history](https://twitter.com/smdiehl/status/1445795674696597506?s=20
):

> Back in the 90s some entrepreneurs found you could convince the public to buy "rights" to name yet-unnamed stars after their loved ones by selling entries in an unofficial register. [..] Like every market madness since the South Sea Bubble or Tulip Mania, everyone knows that selling million dollars JPEGs is an irrational exercise in greed.

Great thread (though I wish it were a [blog post](https://www.stephendiehl.com/posts.html))!
