---
title: "They foyer of One Canada Square"
date: "2021-12-19T10:40:37+10:00"
abstract: "A hushed antespace of timeless power?"
year: "2021"
category: Thoughts
tag:
- architecture
- pointless
location: Sydney
---
[This description](https://en.wikipedia.org/wiki/One_Canada_Square#Lobby) made me chuckle:

> "Rather than entering a corporate reception, you feel as if you're entering a hushed antespace of timeless power." — Herbert Wright, Johnny Tucker, Journalist (architecture) (2016)

It's name has also always bugged me, given its roof [clearly is a pyramid](https://en.wikipedia.org/wiki/One_Canada_Square#Pyramid_roof). At best, the rest of the tower could be described as a rectangular prism. And it's not even in Canada.

> Rather than departing through an exit, one is left with a sense of a transient awakening that inflames the senses with hashed antipasto. — Ruben Schade, esq. (2021)
