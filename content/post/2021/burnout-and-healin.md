---
title: "Burnout and healin’"
date: "2021-10-09T08:05:16+10:00"
abstract: "My attempt at a word that reads as the opposite of burnout. Hi! Also, talking about books."
year: "2021"
category: Thoughts
tag:
- burnout
- covid-19
- personal
- weblog
- work
location: Sydney
---
That was my attempt at figuring out what the opposite of *burnout* would be. The original word was "icein", but that sounded too much like ricin, and you can be burned by ice too. *You're as cold as ice. [You're willing to sacrifice](https://www.youtube.com/watch?v=mjwV5w0IrcA "Foreigner: Cold as Ice")...*

A well-known YouTuber I've [blogged about before](https://rubenerd.com/tag/hololive/) recently admitted to her paying fans that she'd been feeling fatigue and a lack of creative spark in her streams of late. She's going to continue uploding videos, but was taking some time to wind down, reflect, and get some of her mojo back. As a form of [defensive blogging](https://rubenerd.com/defensive-blogging/) (which people shouldn't have to do), she even said people should stop their monthly donations if they wanted.

I love my job; I must, or I wouldn't still be in it after almost seven years. I like to think I'm good at it, that I bring something unique to the team in terms of skills and attitude. But I absolutely get where she's coming from. What's more, my burnout has been witnessed by a small company, not *millions* of streamers. The pressure to perform at that scale boggles my mind.

I hadn't taken a break from work, but I decided to stop blogging or checking social media for a week. I replaced the urge to read short text snippets with books. Right now I'm reading *[Germany: Memories of a Nation](https://www.kobo.com/au/en/ebook/germany-40)* by Neil MacGregor. The way he tells the story of the modern German state through surviving monuments, artefacts, and experiences is so unique and fascinating. I'm also getting back into the [*Monogatari*](https://www.kobo.com/au/en/ebook/bakemonogatari-part-1) series, which are among the most challenging light novels I've ever read. Having that complex storyline and characters forces you to dedicate you entire mental energy to it, which is exactly what my distracted and anxious mind needs.

*(I say distracted, but I tend to have three books going at once: a non-fiction or Western novel, a Japanese light novel, and a manga. I can tell immediately which I'm more in the mood for when I pick up the Kobo. Maybe because they're presented in such different ways).*

I'm not sure when I'll be back on [Mastodon](https://bsd.network/@Rubenerd) or [The Bird Site](https://twitter.com/Rubenerd) again. When I do go back, it'll likely be with a locked or private account. I want to follow and converse with people I care about, but not have that other pressure. I'm probably going to be ruthless and cull almost everyone I follow, then start adding people back. I haven't decided.

Thanks to Hales, Rebecca, and Jonathan for the well wishes after my [abrupt departure](https://rubenerd.com/taking-a-break/), it was appreciated. ♡
