---
title: "Revisiting webcam covers"
date: "2021-06-27T08:02:49+10:00"
abstract: "They saved me from an over-zealous video conference call application today."
year: "2021"
category: Hardware
tag:
- privacy
- security
- webcams
location: Sydney
---
Most of my critiques about webcams lately have been in the context of [video conference fatigue](https://rubenerd.com/video-conference-call-fatigue/), where the evidence suggets that they're a uniquely-draining experience. I stick with audio if I can, which has been a boon for my concentration, and for others on these calls who don't need to see my flapping head, weird hat selection, and my looks of bemusement when someone suggests they can make their architecture simpler by using k8s.

Back in 2016 the big debate was around [covering laptop cameras](https://rubenerd.com/covering-laptop-cameras), given even the likes of The Zuck use PostIt Notes to cover theirs. Surely the rest of us should do it if someone who has such a blatant, comic-book villain attitude to privacy does, right?

Certain Apple bloggers suggested that if your machine is sufficiently compromised with malware to activate the webcam without your knowledge, you've got bigger problems. Therefore, covers are pointless. The former is true, which is why all effective security is achieved using a single layer. It's like Shrek said, ogres are like <del>onions</del>... bagels.

Back on Earth, Apple themselves were even aware of this, given the original FireWire iSight camera had a brilliant mechanism which would expose the camera and power it on with one twist. It wasn't necessary to protect the lens, the faux aperture blades were behind the protective glass. It was there to prevent inadvertent filming, regardless of whether you have malware or not.

As another example, I granted yet another new video conference call system access to my webcam this morning, which promptly began sharing it to everyone on an audio call! It was my little Targus sliding webcam cover that saved me, by blacking out the video and making it look as though it didn't activate.

As I said back then, covering your webcam is no panacea, and also leaves open the question of what else your computer is recording about your activities. But it's still entirely rational and reasonable to cover your webcam when you're not using it.

