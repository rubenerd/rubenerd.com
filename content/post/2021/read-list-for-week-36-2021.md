---
title: "Links for week 36, 2021"
date: "2021-09-19T11:02:25+10:00"
abstract: "A random list of stuff I found and read last week."
year: "2021"
category: Thoughts
tag:
- extensions
- firefox
- git
- listpost
- news
- privacy
- subversion
location: Sydney
---
A random list of stuff I found and read last week:

* [Am I Unique](https://amiunique.org/fp) uses dozens of metrics to attempt to fingerprint your browser. One side effect using an unusual set of tools (FreeBSD, Mac, etc) was that I was pinged as unique every time.

* This Deutsche Welle documentary on [the Stasi and the Berlin Wall](https://www.youtube.com/watch?v=haxkWC6MgcQ) was chilling, though I couldn't also help see the information vaults at the start as a cautionary tale.

* [OneTab](https://www.one-tab.com) is a brilliant Firefox extension that converts all your open tabs into an HTML file with a list. I'd got into the habit of using the built-in "Bookmark All Tabs" feature, but this makes it even easier to archive.

* This blog has made it onto [The Big List of Personal Websites](http://biglist.terraaeon.com/)! There's some interesting stuff there, not all of which is about tech.

* [bit](https://github.com/chriswalz/bit) is an alternative interface to git. I fully agree with [Ben Tsai](https://www.bentsai.org/posts/2021-02-23-thoughts-today/), git is inscrutable and a poor replacement for [hg](https://www.mercurial-scm.org) and even [Subversion](https://svnvsgit.com/). We in the industry sure make some daft decisions.

And some intersting articles (paywalled):

* The Atlantic: [Joe Biden's New World Order](https://www.theatlantic.com/international/archive/2021/09/us-uk-australia-china/620094/) and [Go for a walk with Arthur C. Brooks](https://www.theatlantic.com/family/archive/2021/09/happiness-walking-pilgrimage/620075/).

* The Economist: [A perfect storm for container shipping](https://www.economist.com/finance-and-economics/a-perfect-storm-for-container-shipping/21804500).

* Nikkei Asia: [Behind every good Japanese inn is an 'okami-san'](https://asia.nikkei.com/Life-Arts/Life/Behind-every-good-Japanese-inn-is-an-okami-san), and the [Race to save Hong Kong's wartime relics](https://asia.nikkei.com/Life-Arts/Life/Race-to-save-Hong-Kong-s-wartime-relics).
