---
title: "Billy Joel and Cyndi Lauper’s Code of Silence"
date: "2021-12-22T18:38:27+11:00"
abstract: "Such a good chorus!"
year: "2021"
category: Media
tag:
- billy-joel
- cyndi-lauper
- music
location: Sydney
---
Such a good chorus.

<blockquote>
<p>And you can’t talk about it;<br />
And isn’t that a kind of madness?<br />
To be living by a code of silence;<br />
When you’ve really got a lot to say?</p>
</blockquote>
