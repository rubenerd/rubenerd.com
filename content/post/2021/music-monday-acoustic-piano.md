---
title: "Music Monday: acoustic piano"
date: "2021-03-09T18:26:31+11:00"
abstract: "The Crash Test Dummies and Emile Pendolfi."
thumb: "https://rubenerd.com/files/2021/yt-9jnIcOdnNoM@1x.jpg"
year: "2021"
category: Media
tag:
- jazz
- music
- music-monday
location: Sydney
---
Today's [Music Monday](https://rubenerd.com/tag/music-monday/) discusses the musical style I'm currently obsessed with again. I like to think [Chick Corea](https://rubenerd.com/chick-corea/) is responsible. Wait, isn't it [Tuesday](https://www.timeanddate.com/time/zone/australia/sydney) already?

The first is from the Crash Test Dummies. For those unfamiliar with this band's fascinatingly esoteric tunes, "[Mmm Mmm Mmm Mmm](https://www.youtube.com/watch?v=eTeg1txDv8w)" was their biggest chart topper. Their 1993 album *God Shuffled His Feet* also included this beautiful hidden ballad at the end, the style of which you wouldn't expect after hearing the other tracks! It's in my top ten acoustic piano tunes of all time.

<p><a href="https://www.youtube.com/watch?v=9jnIcOdnNoM" title="Play Crash Test Dummies - Untitled"><img src="https://rubenerd.com/files/2021/yt-9jnIcOdnNoM@1x.jpg" srcset="https://rubenerd.com/files/2021/yt-9jnIcOdnNoM@1x.jpg 1x, https://rubenerd.com/files/2021/yt-9jnIcOdnNoM@2x.jpg 2x" alt="Play Crash Test Dummies - Untitled" style="width:500px;height:281px;" /></a></p>

And on the Whole Wheat Radio stream yesterday we had Emile Pendolfi's album *Believe*. His YouTube channel lists CDBaby which unfortunately no longer exists. I'll need to figure out where to buy this.

<p><a href="https://www.youtube.com/watch?v=dLQCln-8ixg" title="Play Broken Vow"><img src="https://rubenerd.com/files/2021/yt-dLQCln-8ixg@1x.jpg" srcset="https://rubenerd.com/files/2021/yt-dLQCln-8ixg@1x.jpg 1x, https://rubenerd.com/files/2021/yt-dLQCln-8ixg@2x.jpg 2x" alt="Play Broken Vow" style="width:500px;height:281px;" /></a></p>

