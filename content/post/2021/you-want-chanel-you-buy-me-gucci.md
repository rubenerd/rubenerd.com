---
title: "You want Chanel? You buy me Gucci!"
date: "2021-02-01T09:54:29+11:00"
abstract: "Buying things you don't want, to impress people you don't like, with money you don't have."
year: "2021"
category: Thoughts
tag:
- george-carlin
- memories
- school
location: Sydney
---
I was migrating more of my old nvALT database over to Orgmode, and found this line in a text file about renewing Let's Encrypt wildcard certs:

> You want a chanel bag you get me gucci gucci gucci

Is that how you spell those?

I distinctly remember that afternoon in 2018, sitting at a cafe near our old office in Ultimo. This young group of friends sat down at the table opposite us and spend their time outdoing each other's conspicuous consumption. It was a female version of peacock preening, with a distinct Jakarta accent.

My stance has softened on this over the years. I think it's shallow, but do what makes you happy, you don't need my validation or approval. Right?

But that's still the one small thing: *does* it make them happy? I grew up at a rich international school overseas and I'm fairly sure half the crap people bought to impress people didn't make them happy at all. Or at best it was fleeting.

I also remember sitting at a coffee shop in Singapore one weekend&mdash;I'm sure you'll find that hard to believe&mdash;and someone I vaguely recognised from my high school and apartment building was literally across from me with an expensive bag and a sad look on her face. She caught my eye and came to my table, and I ended up helping her carry stuff home in my $30 Mizuno backpack while she ranted about how shit LV was. Is there a moral to the story? Was there more to it? *No!* I just couldn't help see the similarities.

*(It shows how far removed I am from that world again now, given lv means something [entirely different](https://linux.die.net/man/8/lvm)).*

I love that George Carlin line about buying things you don't want, to impress people you don't like, with money you don't have.
