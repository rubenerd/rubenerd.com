---
title: "Finding which coffee types decrease CLD risk"
date: "2021-06-22T14:29:44+10:00"
abstract: "Study shows similar results for instant, ground, and even decaf!"
year: "2021"
category: Thoughts
tag:
- coffee
- food
- science
location: Sydney
---
A new study published in <a href="https://bmcpublichealth.biomedcentral.com/articles/10.1186/s12889-021-10991-7" title="All coffee types decrease the risk of adverse clinical outcomes in chronic liver disease: a UK Biobank study">BMC Public Health</a> has got me excited. The researchers conducted a study to ascertain whether the type of coffee had an impact on its reduction of chronic liver disease (CLD): 

<blockquote cite="https://bmcpublichealth.biomedcentral.com/articles/10.1186/s12889-021-10991-7"><p>Coffee consumption has been linked with lower rates of CLD, but little is known about the effects of different coffee types, which vary in chemical composition. This study aimed to investigate associations of coffee consumption, including decaffeinated, instant and ground coffee, with chronic liver disease outcomes.</p></blockquote>

The study included just under half a million participants with "with known coffee consumption and electronic linkage to hospital, death and cancer records". The results suggested similar results regardless of whether the coffee was instant, ground, or even decaffeinated! The researches concluded:

<blockquote cite="https://bmcpublichealth.biomedcentral.com/articles/10.1186/s12889-021-10991-7"><p>The finding that all types of coffee are protective against CLD is significant given the increasing incidence of CLD worldwide and the potential of coffee as an intervention to prevent CLD onset or progression.</p>
</blockquote>

I can add "helping my liver" to the list of reasons I drink decaf in the evening, along with being able to sleep afterwards, not feeding my anxiety, and to frustrate people who always reply with "I don't understand what the point of decaf is!"

