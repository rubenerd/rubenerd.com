---
title: "Reading people’s blog archives"
date: "2021-12-30T18:04:42+11:00"
abstract: "The Palm Pre, Mac OS X Snow Leopard, clocks, and banjos."
year: "2021"
category: Internet
tag:
- j-walk-blog
- weblog
location: Sydney
---
Indepdendent bloggers are rare thesedays; those that have an archive are rarer still. Blogs are written in the *present*, so their archives are a fun, if bittersweet time capsule.

I've been [stuck in 2009 with Marco Arment](https://marco.org/2009/06/) this afternoon, reading about the Palm Pre, coffee, and Mac OS X Snow Leopard. Earlier today I was checking out my personal mirror of the J-Walk Blog before he took the site offline. Cornfields, clocks, and banjos.

Just don't ask me to read my own archives. I can barely tolerate myself from one year ago, let alone five or fifteen!

Who else has some great archival material?
