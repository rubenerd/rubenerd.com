---
title: "Harukana Receive, and Olympic sports"
date: "2021-07-27T08:17:43+10:00"
abstract: "Riffing off @MattSansb's rankings, and probably the shortest anime review I've ever done."
thumb: "https://rubenerd.com/files/2021/hr-thumb.jpg"
year: "2021"
category: Anime
tag:
- 2018-anime
- beach-volleyball
- bouldering
- diving
- harukana-receive
- olympics
- sport
location: Sydney
---
This is a bit of a disjointed post, but I'm in a bit of a disjointed mood! We start with Erin Riley's [Bird Site comment](https://twitter.com/erinrileyau/status/1419897685729943554?s=20) this evening that I can't fault:

> I saw someone comment on a friend's thread on FB yesterday that every Olympic event should include one random person selected from the crowd, just for context, and I am 100% here for that.

The Price is Right, but for athletics! Wait, did I just date myself?

That then reminded me of a conversation I was having with Matt of [Digitally Downloaded](http://www.digitallydownloaded.net/) last week, who [ranked Olympic sports](https://twitter.com/MattSainsb/status/1417672978817445889):

> * **Handball**
> * **Kayak**
> * **Rhythmic Gymnastics**
> * **Beach Volleyball**
> * **Bouldering**
>
> **Diving** was in my top 5 but then **Bouldering** debuted this Olympics. Volleyball's awesome too but **Beach Volleyball** is better. 

I'd only make a couple of small tweaks:

* Put **Diving** back in and sub out **Kayak**. I can't help but admire the patience, skill, and guts it would take to jump off a platform that height and navigate those impossible maneuvers. I also respect the fact that making the smallest splash possible is the *antithesis* of bombastic showboatyness. 

* Replace **Handball** with **Table Tennis**, because Singapore. This also brings in **Badminton** as an honourable mention. The mechanics and speed of both seem so utterly implausible, they're the most gripping sports I've ever watched. Why they don't get more publicity in the West confounds me.

I also second Matt's enthusiasm with **Bouldering** becoming an Olympic event. I barely pay attention to sport normally, but I love that the Olympics showcases events that don't involve kicking a ball. Or if they do, it'd likely be an accident, incredibly painful for the male player, and/or worthy of a penalty.

Which leads us to **Beach Volleyball**, and probably the briefest anime review I've ever done here. I watched *Harukana Receive* back in 2019 after reading a synopsis by [Dee over at The Anime Feminist](https://www.animefeminist.com/review-harukana-receive-episode-1/) which gave me an appreciation for the sport I didn't have before.

<p><img src="https://rubenerd.com/files/2021/harukana-receive-opening@1x.jpg" srcset="https://rubenerd.com/files/2021/harukana-receive-opening@1x.jpg 1x, https://rubenerd.com/files/2021/harukana-receive-opening@2x.jpg 2x" alt="Screenshot from the opening of Harukana Receive" style="width:500px; height:281px;" /></p>

The series takes places around the island of Okinawa (I still find it funny that I want to explore the north of Japan, and Clara keeps wanting to island hop in the south! I'll bet the weather is warmer). The colours and landscape backdrops are so bright and optimistic, I ended up using some as desktop backgrounds last winter.

Our protagonist Ōzora Haruka is enamoured with a couple of players she spots on the beach. She befriends and wins back a former player with a troubled past in the sport, and they all train together for a tournament over the course of the series. We learn about the rules and strategy in a surprisingly respectful and engaging way, like all good sports anime. The fan service is mild given the subject matter, though I'll admit I wouldn't have thrown together that quick montage below at a coffee shop (cough).

I won't lie (as opposed to other times?), it gave me *K-On! does Beach Volleyball* vibes, with the same emphasis on friendship and personal growth among a cast of likeable and relatable characters (Haruka gave me *serious* Yui vibes). Episode 8's shrine and coffee shop wanderings were my favourite, not that I'm predictable or anything. That's my second mention of coffee shops in a post nominally about something else. I miss going to coffee shops.

<p><img src="https://rubenerd.com/files/2021/harukana-receive-clip@1x.jpg" srcset="https://rubenerd.com/files/2021/harukana-receive-clip@1x.jpg 1x, https://rubenerd.com/files/2021/harukana-receive-clip@2x.jpg 2x" alt="Screenshots from Harukana Receive, including my favourite scene at the coffee shop in the lower-right." style="width:500px; height:282px;" /></p>

Maybe now I can appreciate the real thing a bit more. Does this also mean I need to watch *Iwa-Kakeru!* (the first search result for "bouldering anime") too? There's really a series for everything. Would the *Free!* boys consider diving at some point?

This wasn't among the *most* disjointed posts I've ever written, but it has to be up there. Before the ball reaches the ground.
