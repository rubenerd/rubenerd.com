---
title: "Introverts and accommodation"
date: "2021-10-12T08:33:50+10:00"
abstract: "Unfortunately, the burden is always on introverts to empathise, understand, and accommodate."
year: "2021"
category: Thoughts
tag:
- introversion
- sociology
location: Sydney
---
Holley Morgan [wrote an article for Introvert Dear](https://introvertdear.com/news/5-tips-for-surviving-your-extroverted-in-laws-as-an-introvert/), explaining the adventures with her new in-laws:

> If you’re an introvert who has extroverted in-laws, it’s probably going to take more understanding on your part than theirs.

That's the crux of the issue for introverts in almost all settings, unfortunately. The burden is always disproportionately on us to empathise, understand, and accommodate the requirements of others. It's pervasive, especially in the West.

That said, is a phrase with two words. She has some useful advice which, again, is broadly useful everywhere. Don't use alcohol as a crutch. And in unavoidable social situations, make it clear that you like and appreciate people. Extroverts take introverts to be cold because we don't communicate.
