---
title: "Applying Georgina’s framework to anxiety"
date: "2021-05-17T09:04:51+10:00"
abstract: "Identifying things, why they're happening, action items, and results."
year: "2021"
category: Thoughts
tag:
- health
- georgina
location: Sydney
---
[Georgina](https://hey.georgie.nu) is the *exact person* I'm talking about when I pontificate about the state of independent writers, blogging, RSS, and wrestling control away from social networks and writing farms that have so successfully convinced most Netizens that one needs their blessing in order to post those aforementioned pontifications. That was a long sentence, especially when placed next to this comparatively shorter one. We need more writers like this.

One of her recent posts was a brutally honest take on the [concept of adulting](https://hey.georgie.nu/adulting/), or being someone who has their shit together. She goes into detail about the emotional states and frustrations that manifest from specific obligations which she routinely struggles to action. She cites "saying yes to too many things", "the apartment has a lot of stuff", and "I need new running shoes" as examples.

I can empathise. I don't suffer from depression, but I've always had acute anxiety. The worst thing isn't not doing something, its the worry from not doing it, which is followed by guilt for not doing it, then doubt about whether it's even possible. This spiral eventually builds up the original task into an insurmountable challenge that triggers crippling physiological symptoms at the mere thought.

Breaking out of this feedback loop is tough, which is why nipping it in the bud before it spirals is so critical. Which is why I took special interest in Georgina's spreadsheet for identifying tasks:

* The thing
* Why is this happening? Why am I feeling this way?
* Action item
* Result

I'd devised my own pseudocode based on triggers, thought mistakes, and diffusions, but it's very much reactionary. This table looks like it could be useful for *proactively* breaking down and tackling specific tasks, which is exactly what I need.

Anyway, I thought I'd share this in case someone out there finds it useful. Read [Georgina's post](https://hey.georgie.nu/adulting/) for all the details.

