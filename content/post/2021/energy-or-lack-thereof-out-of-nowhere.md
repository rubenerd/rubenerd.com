---
title: "Energy, or lack thereof, out of nowhere"
date: "2021-07-22T09:09:22+10:00"
abstract: "It’s so silly and counterproductive how we convince ourselves of certain things."
year: "2021"
category: Thoughts
tag:
- health
- personal
- psychology
location: Sydney
---
This morning was like any other during the various Australian Covid lockdowns, but everyone I've talked with in my family and work have all complained about feeling tired. I'm exhausted too, and the day has barely begun.

I've long been interested in placebo effects and their natural corollaries, such as feedback loops and self-fulfilling prophecies. We convince ourselves of a circumstance or emotion, and we carry on under that assumption whether the evidence is there or not. I've told people I'm tired this morning for some reason, had that reinforced by others, and now I'm struggling even more to stay awake.

I'm not sure if this is related to my [post earlier this week](https://rubenerd.com/what-if-were-supposed-to-feel-it/ "What if we're supposed to feel it?") about trying to flip negative thoughts into positive action, but it does strike me as especially counterproductive that we use this incredible power of self-delusion to convince ourselves of *negative* things.

Part of the problem for me has always been that life coaches and those who say "think positive!" are more than a little irritating. I interpret their exuberance and optimism as insincerity, because nobody could be that happy all the time. Then I see the warmest, funniest people in the world like Robin Williams and... we all see the outcome.

The other thing acting out here is wanting perfect to be the enemy of good. The world doesn't need to be our oyster, nor do we need to convince ourselves that everything is always going to be spectacular and fabulous. Life is a struggle, that's expected. What I *am* going to do is make a concious effort to nip unhelpful thoughts in the bud before they fester and become facts. I'm not tired this morning!

