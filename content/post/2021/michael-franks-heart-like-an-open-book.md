---
title: "Michael Franks, Heart Like an Open Book"
date: "2021-07-26T16:51:29+10:00"
abstract: "Such a great bassline and drums, too."
thumb: "https://rubenerd.com/files/2021/yt-z9gy4VIOhoA@1x.jpg"
year: "2021"
category: Music
tag:
- michael-franks
- music
- music-monday
location: Sydney
---
Today's *[Music Monday](https://rubenerd.com/tag/music-monday/)* takes me back to this site's musical roots. Frankly I'm surprised it took this long for him to reappear. Get it, because his name is Michael... ah shut up.

Michael Franks has been my favourite singer/songwriter since I was barely old enough to understand what half his witty lyrics meant. Online guides pigeonhole him as a latin jazz musician, but his <del>repotoure</del> <del>repotoire</del> (damn it) *repertoire* has also included various forms of jazz funk, fusion, and even a sprinkling of pure 1980s electronica. His music never fails to bring a smile to my face every time he comes up on random.

<p><a href="https://www.youtube.com/watch?v=z9gy4VIOhoA" title="Play Heart Like An Open Book"><img src="https://rubenerd.com/files/2021/yt-z9gy4VIOhoA@1x.jpg" srcset="https://rubenerd.com/files/2021/yt-z9gy4VIOhoA@1x.jpg 1x, https://rubenerd.com/files/2021/yt-z9gy4VIOhoA@2x.jpg 2x" alt="Play Heart Like An Open Book" style="width:500px;height:281px;" /></a></p>

"Heart Like an Open Book" comes from his 1999 album *Barefoot on the Beach*. It was the first album of his that came out when I was old enough to leave the house and rush to the music store to buy it. My music player brought it up *twice* this afternoon, such was its motivation to remind me of its awesomeness.

These [Porta Pro headphones](https://rubenerd.com/the-kos-porta-pro-x-headphones/) recently lead me to rediscover the great bassline and drums on this specific track. Michael always had such excellent backing musicians.

