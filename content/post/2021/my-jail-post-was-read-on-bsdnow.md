---
title: "My jail post was read on BSDNow!"
date: "2021-03-06T10:07:33+11:00"
abstract: "Thanks Benedict and Allan!"
thumb: "https://rubenerd.com/files/2020/bsdnow.jpg"
year: "2021"
category: Software
tag:
- bsd
- bsdnow
- podcasts
location: Sydney
---
I was catching up on podcasts yesterday and realised that Benedict Reuschling and Allan Jude had discussed my [follow-up post about FreeBSD jails](https://rubenerd.com/follow-up-about-freebsd-jail-advantages/) on [ episode 391](https://www.bsdnow.tv/391)! Better still, I made them both laugh, so mission accomplished :).

Allan mentioned that in addition to what I described, ZFS feature flags can be enabled for specific jails. I've been dealing with an issue where extended attributes aren't being passed to Mac guests from a jail running Samba, but on the host they do. This might be an avenue of inquiry to check out.

<p><a title="Tune into episode 391" href="https://www.bsdnow.tv/391"><img src="https://rubenerd.com/files/2020/bsdnow.jpg" alt="The BSDNow logo" style="width:128px; height:128px; float:right; margin:0 0 1em 2em;" /></a></p>

They gentleman also read how I used jails first and foremost for package management, with security being a useful side effect. Allan reminded us that this was the original use case for jails, which I didn't know!

It was a bittersweet moment hearing Allan say we missed out on meeting again in Tokyo for AsiaBSDCon for the second year in a row. Our chats over breakfast and in the hallways at that conference were among the highlights of my life; hopefully as the world slowly starts to open up again we can grab a beer and talk servers again. I still would love to go to Canada and Europe for events there as well!

For those unfamiliar, [BSDNow](https://www.bsdnow.tv/) is a weekly programme where two of the nicest, most knowledgable people in FreeBSD read and discuss the current hot topics in the BSD operation system community. If you're familiar with Linux or commercial UNIX, they also do a great job making the topics approachable and relatable.
