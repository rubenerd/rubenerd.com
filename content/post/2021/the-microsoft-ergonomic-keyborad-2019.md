---
title: "The Microsoft Ergonomic Keyboard 2019"
date: "2021-07-20T22:02:23+10:00"
abstract: "Putting my mechanical boards aside to help my tired wrists."
thumb: "https://rubenerd.com/files/2021/Microsoft_Ergonomic_Keyboard_2019_3c62p@1x.jpg"
year: "2021"
category: Hardware
tag:
- colour
- design
- ergonomics
- keyboards
- health
- microsoft
location: Sydney
---
I've done the rounds with buckling spring IBM Model M clones, Northgate Omnikeys, Cherry MX browns, even Topres. They've all been tactile and fun to use, as was wandering around keyboard stores in Nipponbashi and Akihabara in search of them. Keyboards, like camera lenses and coffee gadgets, are *dangerous* rabbitholes. It's why I'm avoiding watches, despite finding them fascinating too.

Yet here I am typing this post using the latest budget split [Microsoft keyboard](https://www.microsoft.com/en-au/d/microsoft-ergonomic-keyboard/93841ngdwr1h?cid=msft_web_collection&activetab=pivot%3aoverviewtab), and I'm surprisingly happy. It's like discovering you like the taste of Singles after years of real cheese... if unwrapping Singles made my wrists feel better.

<p><img src="https://rubenerd.com/files/2021/Microsoft_Ergonomic_Keyboard_2019_3c62p@1x.jpg" srcset="https://rubenerd.com/files/2021/Microsoft_Ergonomic_Keyboard_2019_3c62p@1x.jpg 1x, https://rubenerd.com/files/2021/Microsoft_Ergonomic_Keyboard_2019_3c62p@2x.jpg 2x" alt="Side profile view of the Microsoft Ergonomic Keyboard" style="width:500px; height:229px; padding:1em 0" /></p>

But let's take a step back for a moment. I haven't seen much peer-reviewed evidence that split keyboards are more ergonomic than their unsplit cousins. Health is something you need to talk to qualified professionals about, especially if you're dealing with pain. All I can say, anecdotally, is that split keyboards orient my hands in a more comfortable position, especially for extended typing sessions.

The indie mechanical keyboard scene has plenty of its own split keyboards, but I saw this latest Microsoft unit for less than AU $100 and couldn't pass it up. It was delivered from JB Hi-Fi the day after I ordered, and coincided with a colleague dropping off my beloved LG 21.5-inch 4K monitor from work. Alongside my [Kensington Orbit](https://www.kensington.com/en-au/p/products/control/trackballs/orbit-trackball-with-scroll-ring/) wired trackball and [Sailor Mercury fig](https://myfigurecollection.net/item/217440 "Bishoujo Senshi Sailor Moon - Sailor Mercury - Girls Memories (Banpresto)") for motivation, it's just about the best home office setup I could ask for.

But back to the keyboard! The 2019 edition of Microsoft's Natural Keyboard, cleverly dubbed the *Microsoft Ergonomic Keyboard*, has the perfect mix of features for me. It's the budget version alongside the swish Microsoft Sculpt and the Surface Ergonomic, but features proper keys rather than the chicklets everyone has (unfortunately) copied from Apple. It's also wired unlike those units, meaning I don't need to worry about keeping a stationary desk object charged for no reason, or transmitting my keystrokes over a broken, insecure wireless channel.

The keyboard is slimmer and more understated than the 4000 it replaced, with a similar non-detachable palmrest for when you're taking a typing break. The Scroll Lock, Pause, and Num Lock keys are located closer to the Page Up/Page Down cluster which has been controversial, but I can't say it's bothered me. It has a row of multimedia function keys, some of which work on macOS and FreeBSD without any configuration.

<p><img src="https://rubenerd.com/files/2021/Microsoft_Ergonomic_Keyboard_2019_ws8km@1x.jpg" srcset="https://rubenerd.com/files/2021/Microsoft_Ergonomic_Keyboard_2019_ws8km@1x.jpg 1x, https://rubenerd.com/files/2021/Microsoft_Ergonomic_Keyboard_2019_ws8km@2x.jpg 2x" alt="Top-down view of the keyboard." style="width:500px; margin:1em 0;" /></p>

You can tell it was made to a cost; the letters are silk-screened onto the keycaps, and the whole thing feels like plastic. But the board feels nicely balanced, and its sheer horizontal size and rubber feet keep it from moving around. The keys themselves feel mushy after using mechanical boards, but not as much as I'd have expected; it feels improved from what they released before. The keycaps don't rattle or move around, and their stacked positioning makes them easier to reach then a standard layout.

The only major sticking point for me is the spacebar. It feels tighter and more responsive having been struck for a few days now, but from the factory it felt mushier than a pancake, and took way too much force to actuate. My hope is it'll continue to improve, because right now it's still borderline frustrating. I'm also one of those weird people who'd prefer a two-tone classic beige colour over just black, but as Jim Kloss famously said during a morning rant on Whole Wheat Radio in 2004:

> That's why I got me a black keyboard... I got a black keyboard right 'ere. *Ain't never get no dirty on it!*

I'm glad Microsoft is still making these tradtional-style split keyboards after all these years, with the same separate function and multimedia keys, and layout that everyone has come to appreciate. I personally know developers, system administrators, factory engineers, and even a legal professional who refuses to use anything else. There's a reason these boards have a following.

I'll write up how one goes about using these boards on FreeBSD and macOS in a future post.
