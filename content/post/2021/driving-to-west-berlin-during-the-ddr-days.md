---
title: "Driving to West Berlin during the DDR days"
date: "2021-09-20T12:32:12+10:00"
abstract: "Someone uploaded the video the British RMP would have you watch before making the trip in your car. Talk about ominous!"
thumb: "https://rubenerd.com/files/2021/bfg-to-berlin@1x.jpg"
year: "2021"
category: Travel
tag:
- berlin
- east-germany
- germany
- music-monday
location: Sydney
---
Today's [Music Monday](https://rubenerd.com/tag/music-monday/) is tenuous at best, but you can't spell tenuous without US. Those of you who know your World War II Allied powers got the joke, but I'll bet it still wasn't funny.

I've been getting back into modern German history again, specifically around the 1980s and the fall of the DDR. It's a fascinating moment in time, one that even with thirty years of hindsight seemed so implausible and unlikely. My dad is German, and my grandmother's side had specific family history tied up with that infernal wall and its surrounding politics, some of which explains my interest in digital privacy and overreach today. One day I hope to learn know more, and to have permission to share it.

But I digress! [Yesterday's DW documentary](https://rubenerd.com/read-list-for-week-36-2021/) I shared about the Stasi and the Berlin Wall lead me down a video rabbit-hole and to a documentary video I never thought I'd see. Malcolm Brooke uploaded the [official British Royal Military Police documentary](https://www.youtube.com/watch?v=QS1xvtLV8Xw) screened to people before allowing them to drive through East Germany to West Berlin.

<p><a title="Watch BFG to Berlin" target="_BLANK" href="https://www.youtube.com/watch?v=QS1xvtLV8Xw"><img src="https://rubenerd.com/files/2021/bfg-to-berlin@1x.jpg" srcset="https://rubenerd.com/files/2021/bfg-to-berlin@1x.jpg 1x, https://rubenerd.com/files/2021/bfg-to-berlin@2x.jpg 2x" alt="Watch BFG to Berlin" style="width:500px; height:400px;" /></a></p>

Everything about this is spectacular, from the 1980s-era graphics and music to the ominous British voices and advice. My favourite line has to be:

> We acknowledge East German traffic regulations, though only accept Soviet authority.

Given how recently it was produced, it further shows just how unexpected the fall of the DDR was to the Allies at the time.

The World War II partition of the German state and its capital are far too complicated to discuss here, but the outcome was an Allied-administered West Berlin enclave within the Soviet-aligned East Germany. One could drive between the two on a pre-approved series of autobahns provided you were willing to submit to Allied and Soviet checkpoints on each side. 

My dad told me it was rote and routine by the 1980s, but the idea of saluting a Soviet officer in East German territory before heading back to my car seems terrifying, despite the video's assurances that there was nothing to worry about!

Anyway, this is a Music Monday post because I've had the low-budget electonica music from this video in my head all day. It sounds so upbeat for the topic it accompanies.
