---
title: "Be kind to retail staff"
date: "2021-05-28T16:30:47+10:00"
abstract: "Michael Sainato reporting on the rise of retail abuse in the US."
year: "2021"
category: Thoughts
tag:
- retail
- work
location: Sydney
---
Michael Sainato reported in The Guardian US on the [growing spate of worker abuse](https://www.theguardian.com/business/2021/may/26/starbuck-employees-intense-work-customer-abuse-understaffing)\:

> Some workers at Starbucks have described understaffing at stores, intense workloads, and customers who have changed their ordering habits and become increasingly aggressive and confrontational during the coronavirus pandemic. [..] Starbucks workers around the US have faced several reported incidents of being verbally abused or physically assaulted by customers over coronavirus safety protocols.

Clara and I witnessed this first hand when we went to New York a few years ago. We went to a branch near Battery Park City on the way to the Statue of Liberty ferry, and the line of rich suits hurling abuse at the staff was toe-curling. Whether they were [power tripping](https://rubenerd.com/jokes-at-the-expense-of-retail-staff/), coked out from their investment banking jobs, overcompensating for small appendiges, and/or were all-around jerks, we didn't know. By the time we were at the head of the queue I joked "wow, busy day, I don't know how you do it" to the woman behind the counter, and the look of relief on her face broke my heart. 

To think retail workers over there are enduring even more of this is obscene. That and the fact they're criminally underpaid for what they do, especially given the toxic environment. Please be kind to people.

