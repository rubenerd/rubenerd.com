---
title: "My Commodore 1571 drive arrived!"
date: "2021-03-16T09:17:45+11:00"
abstract: "Even leaving aside performance improvements, it’s such a beautiful device."
thumb: "https://rubenerd.com/files/2021/commodore-1571@1x.jpg"
year: "2021"
category: Hardware
tag:
- commodore
- commodore-64c
- commodore-128-series
- commodore-plus4
- disk-drives
- shopping
location: Sydney
---
In my ongoing [Commodore 128 series](https://rubenerd.com/tag/commodore-128-series/), today I accepted delivery of a refurbished Commodore 1571 5.25-inch disk drive! I eluded to [buying a rarer drive recently](https://rubenerd.com/necessity-versus-rarity-in-ebay-auctions/), but I'm *so* glad I bought this instead.

Commodore released the 1571 in 1985 to improve transfer speeds over the glacial 1541. It could natively read and write double-density, double-sided disks without needing to flip them, though it required them to be formatted as such in the new drive. It ran quieter and cooler, was physically smaller, and eliminated most of the 1541's head alignment issues owing to a newer mechanism. I'm especially interested in all the new formats it can read including CP/M disks, and DOS with additional software.

<p><img src="https://rubenerd.com/files/2021/commodore-1571@1x.jpg" srcset="https://rubenerd.com/files/2021/commodore-1571@1x.jpg@ 1x, https://rubenerd.com/files/2021/commodore-1571@2x.jpg 2x" alt="Photo showing the new 1571 drive next to my C128 and a small NEC LCD monitor on a table." style="width:500px" /></p>

It's also a *beautiful* device! I'm a huge fan of Commodore's understated, sleek design language from the mid-1980s that carried through to this drive, the Commodore 128, and the 64C. It also doesn't look out of place next to my Plus/4. My Bil *Herd* of machines you could say, if you were a monster.

I use a SD card adaptor for most of my data and software, but half the fun of playing with these machines is to use period-correct hardware. The 1571 seemed like a logical choice after my [1541 was mothballed](https://rubenerd.com/troubleshooting-a-commodore-1541-disk-drive/). It only invokes its Burst Mode when attached to a C128, so is still backwards compatible with my Plus/4 and 16.

I don't normally mention eBay sellers specifically, but I was also blown away at the care with which [Commodore Retro Store](https://www.ebay.com.au/usr/commodore_retro_store) packed the machine for shipping from the UK. Unpacking it felt like a game of pass-the-parcel; I counted no fewer than six layers! They even printed a new drive shipment card to protect the heads.

<p><img src="https://rubenerd.com/files/2021/commodore-1571-parcel@1x.jpg" srcset="https://rubenerd.com/files/2021/commodore-1571-parcel@1x.jpg 1x, https://rubenerd.com/files/2021/commodore-1571-parcel@2x.jpg 2x" alt="Photos showing each layer of unpacking." style="width:500px" /></p>

I'm still waiting on some other display parts for the C128, but I'll do a proper testing and exploration post again soon. But I can confirm it powers up and seeks when the C128 starts :).

