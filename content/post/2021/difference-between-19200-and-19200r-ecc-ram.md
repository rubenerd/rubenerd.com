---
title: "Difference between 19200 and 19200R ECC RAM"
date: "2021-01-15T22:15:00+11:00"
abstract: "-R stood for registered! I should have known that, but Today I Learned!"
year: "2021"
category: Hardware
tag:
- homelab
- ram
location: Sydney
---
This whole time I'd been assuming that both these SKUs of ECC DDR memory were equivalent. [Rod Bland sorted me out at RamCity](https://help.ramcity.com.au/hc/en-us/articles/360001143896-What-are-the-different-Memory-RAM-types-)\:

> DDR4 modules can optionally [b]e "registered" ("buffered"), which improves signal integrity (and hence potentially clock rates and physical slot capacity) by electrically buffering the signals at a cost of an extra clock of increased latency. Those modules are identified by an additional R in their designation, e.g. PC4-19200R. Typically modules with this designation are actually ECC Registered, but the 'E' of 'ECC' is not always shown. Whereas non-registered (a.k.a. unbuffered RAM) may be identified by an additional U in the designation. e.g. PC4-19200U.

It never occurred to me that -R stood for registered/buffered. The Supermicro board in my [new homelab machine](https://rubenerd.com/building-a-new-homelab-server/) only showed unbuffered memory compatibility, so I guess it's just luck I didn't accidentally pick up the wrong type.

[ServeTheHome](https://www.servethehome.com/unbuffered-registered-ecc-memory-difference-ecc-udimms-rdimms/) also has a great article.
