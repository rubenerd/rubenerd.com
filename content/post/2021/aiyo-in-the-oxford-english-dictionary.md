---
title: "Aiyo in the Oxford English Dictionary"
date: "2021-08-04T08:20:32+10:00"
abstract: "The news came out in 2016, but friends only just told me about (aiyo)."
thumb: "https://rubenerd.com/files/2021/sg-thumbnail.png"
year: "2021"
category: Travel
tag:
- language
- singapore
- singlish
location: Sydney
---
[The Sun Daily in Malaysia](https://www.thesundaily.my/archive/1996689-HSARCH399944) reported the news back in 2016, but I only just found out about this from friends: 

> KUALA LUMPUR: The word 'Aiyo' is now an accepted English word. The Oxford English dictionary has included the popular south India slang and the news is trending on social media. The definition given for Aiyo is – to denote distress, regret, or grief; 'Oh no!', 'Oh dear!" in south India and Sri Lanka. Variations accepted includes 'aiyoh' and 'aiyah'. 

Aiyo is an indelible part of Singlish too, though I didn't know it was Tamil slang. Along with *shiok*, it's such a versatile exclamation.

I remember one cab ride with an uncle years ago when we just missed a red light on account of someone cutting us off, and I let out an *aiyo*. We ended up having a fifteen minute chat about the horrible caucasian tourists he'd driven before me, and that I'd reminded him that *some angmohs okay, lah*. He was such a sweet old man, I couldn't imagine what would posess someone to laud over him like that. Tourists... *wah lao eh*.

I think my friends and I used "aiyaaaaaah" the most growing up, because it was easier to draw out. Just like this post!

The [html language attributes](https://www.w3.org/International/questions/qa-html-language-declarations) on all my sites is still set as `en-SG`, in part because they use 24-hour time, and the correct leading zeroes on dates. It's also where I grew up and learned computers, so I think it's a nice tip of the hat. Maybe I need to start reintroducing more of their words, too.

