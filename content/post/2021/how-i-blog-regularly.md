---
title: "How I blog regularly"
date: "2021-04-02T09:43:40+11:00"
abstract: "Based on a question by Wesley Moore on Mastodon"
year: "2021"
category: Internet
tag:
- personal
- weblog
- writing
location: Sydney
---
Wesley Moore [asked this question](https://decentralised.social/objects/79aa6b98-e614-4359-97a7-9947bd6971a9) on Mastodon:

> Re: Feedback from Thomas Jensen
>
> I also subscribed to your blog in the last couple of months (for BSD things I think) and have have been enjoying and been impressed by your post volume too. Perhaps Thomas was curious as I am, if you had any tips for being able to write so regularly?

I'm not sure if I'll be able to impart anything broadly useful beyond just what works for me, but hopefully there's something in this meandering post!

Habit is probably the main reason. I had a journal as a kid, which I'd maintain on a black and white Palm IIIx if you can believe it! My mum instilled the importance of keeping a record of my thoughts, which did end up being very helpful for mentally processing heavy family things. In late 2004 I realised some of what I was writing could easily be public, so I started publishing on my tiny shared web host account in Singapore at the time.

That was a long, tedious way of saying that the **more you do it, the easier it becomes.** Don't worry about it being perfect, and don't listen to all those social media experts. Your ideas are more important than arbitrary rules, and the fact you've already expressed an interest in writing on your own site in 2021 should be treasured. *We need you!*

Which leads into the second point: **don't let people tell you what you can write**. I remember a self-anointed arbiter of blogging asserting in the mid 2000s that you must only ever discuss one primary topic. I internalised that edict for years, which is why I had my silly podcast, anime stuff, and shorter-form snippets in separate places. Then when I realised this was all bunkum, I started writing about whatever.

*(I still get people today who tell me on HN and email that my technical posts can't be taken seriously because of the anime mascot my girlfriend drew for me, or because I sometimes write nonsense posts about grilled cheese. They're entitled to their opinion, just as I am to call them out for unproductive gate-keeping).*

Speaking of food, it's also important to **find a place that's conducive to writing.** For me, that's coffee shops. Almost every post for the last sixteen years has either been drafted or posted from a public cafe somewhere. I can't blog at home, or at work, or at school back in the day. I don't know why this works; one theory is that being surrounded by people with whom I have no social obligation to interact fulfills my human need for contact, *and* my introversion! Or maybe its just the coffee/tea.

If caffeine is a cheat, another is to have a **constant supply of drafts** that your scatterbrained mind never got around to finishing. There are weeks where I'm busy, burned out, or just tired, so I sometimes reach into my evergreen drafts directory, tidy up a post, and publish. Other weeks I may queue up and schedule posts, so I'm not spamming people with a dozen entries at once. This isn't the only approach though; people like John Siracusa only write once or twice a year.

Technical people also need to **avoid getting hung up about infrastructure**. Some of the best blogs I read are hosted on Tumblr, or WordPress.com. I personally think [Ghost](https://ghost.org/docs/install/) is the best self-hosted blog platform, or [Hugo](https://gohugo.io/) if you're into static-site generation. But don't feel like that your words are somehow less meaningful if you don't write your own CMS, or host on a cloud instance or VM you maintain. There's a certain amount of pride or cachet that people think will come from doing it "properly", but it's also nonsense. You own the words on your blog, so you can always export and move as well, as I've done many times.

*(My only cautionary tale here would be to avoid writing aggregation sites like Medium if you can. Your writing is important enough to host on a platform that cares about you, not to treat you like a content cow).*

But **how do I find what to write about?** This one is the hardest to explain, because I feel like my head is constantly full of stuff... sorry mindfulness meditation gurus! I do use RSS heavily to follow other people's writing, and I pay for a few online newspapers. I don't read as many books as I should, or as much manga and light novels as I used to, but I try to make the time for those too. I'm surprised how often a news story or an idea on someone else's post will be a spark to think about something else. If all else fails, hit the Random button on Wikipedia, or ask someone what they'd be interested in reading.

*(As another aside, this is why I don't at all by the argument that blogs should only be about one topic. I can't tell you the number of delightful and fun rabbit-holes I've gone down because someone I followed for one topic deviated into something else that looked interesting)!*

Those were a lot of words for just a few recommendations. But that's how I write; maybe you could summon a level of succinctness that I previously aspired to, before deciding to *own* my verbosity. Yay, words!

