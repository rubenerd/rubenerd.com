---
title: "Australia doesn’t do cappuccinos"
date: "2021-10-29T22:14:12+10:00"
abstract: "That’s a flat white or a latte with chocolate powder on top, damn it!"
year: "2021"
category: Thoughts
tag:
- australia
- coffee
- drinks
- food
- singapore
location: Sydney
---
Speaking of travel, or lack thereof, here's something else I was thinking about that we could do in the Before Times.

Coffee was among the first funnels of culture shock I encountered when moving back to Sydney from Singapore. Australians are globally recognised coffee snobs, to the point where dunking on Starbucks is a part of the citizenship test, and anything less than the best is consigned to the same heap as that nasty, granulated instant. It's lead to a personal struggle between loving single origin brews made by an artisan barista taught in Sydney's Newtown sporting a fedora and listening to out of tune folk music, and the kopi-o brews I used to have with kaya toast at the foot of HDB blocks or hawker centres.

But that wasn't the shock.

I prefer to have black coffee most of the time, having weaned myself off sugar in almost everything, and milk in coffee years ago. But *occasionally* I like to indulge in a bit of a treat, especially on a Friday afternoon like this. For that, I used to turn to a frothy cappuccino.

The problem is, **Australians don't do them**. I'm not sure when this started, but Australian baristas will give you a flat white or a latte with chocolate powder on top and call it a cappuccino. It tastes great, and is probably among the best made coffees you'll have in the world. *It's just not a cappuccino*. It's not even close.

Milk foam is the key to a cappuccino. It's what makes drinking them fun. The light smattering of chocolate powder over the foam is the same reason why the creamy froth of a Guinness is so iconic and tasty. Sure you'd be getting more liquid beer if you have less foam, but where's the fun? *Where's the unintentional moustache, I ask you!?*

Kiwis are as selective and skilled with their coffee across the pond, I wonder if they do the same thing? It'd be a shame if they did, they have the best dairy in the world.

