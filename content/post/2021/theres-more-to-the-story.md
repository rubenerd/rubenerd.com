---
title: "There’s more to the story"
date: "2021-02-05T16:07:52+11:00"
abstract: "... like all stories?"
year: "2021"
category: Media
tag:
- journalism
location: Sydney
---
Channel NewsAsia [quoted an analyst](https://www.channelnewsasia.com/news/commentary/gamestop-what-happen-stock-market-rise-share-price-manipulation-14110314)\:

> Business press promoted a simplified version of the [GameStop short selling] story and labelled it the battle of David versus Goliath. But there is more to the story.

That's *all* journalism: everything is simplified.

See also: [the issue isn't privacy, it's privacy](https://rubenerd.com/the-issue-isnt-privacy-its-privacy/)?

