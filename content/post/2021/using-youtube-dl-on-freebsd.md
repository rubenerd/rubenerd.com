---
title: "Using youtube-dl on FreeBSD"
date: "2021-07-11T10:43:19+10:00"
abstract: "Install py38-xattr if you get a writing metadata error."
year: "2021"
category: Software
tag:
- bsd
- freebsd
- video
location: Sydney
---
[youtube-dl](https://youtube-dl.org/) is a Python utility to download videos from various websites, such as YouTube (surprise!) and Vimeo. I use it to archive channels I like into Clara's and my Plex server. I'm surprised how often videos disappear, especially history and science channels that discuss sensitive topics. It doesn't matter if the host is respectful, or sticks to facts, the *Almighty Algorithm* won't have any of it.

But I digress. I was downloading a certain [British Rail Pacer video](https://rubenerd.com/british-rails-pacer-trains/) yesterday on my fresh FreeBSD 13 laptop install and hit this error:

	[metadata] Writing metadata to file's xattrs   
	ERROR: Couldn't find a tool to set the xattrs. 
	Install either the python 'xattr' module, or the 'xattr' binary.

It was easy enough to fix:

	$ printf "%s\n" "Cheese Steak Jimmy's"

That was clearly the wrong command.

	# pkg install devel/py-xattr

Thanks to timur@ for maintaining the ports :).

