---
title: "The B1M discusses Hudson Yards"
date: "2021-10-25T17:53:34+10:00"
abstract: "Video about social housing, public benefit, and big buildings."
thumb: "https://rubenerd.com/files/2021/yt-vmlKdyXP4nk@1x.jpg"
year: "2021"
category: Media
tag:
- economics
- engineering
- sociology
- the-b1m
- youtube
location: Sydney
---
One of my favourite engineering YouTube channels [recently did an episode](https://www.youtube.com/watch?v=vmlKdyXP4nk) on the Hudson Yards development in New York. Clara and I saw the construction zone when we were there a few years ago; it's *massive.*

<p><a href="https://www.youtube.com/watch?v=vmlKdyXP4nk" title="Play Is Hudson Yards Good For New York?"><img src="https://rubenerd.com/files/2021/yt-vmlKdyXP4nk@1x.jpg" srcset="https://rubenerd.com/files/2021/yt-vmlKdyXP4nk@1x.jpg 1x, https://rubenerd.com/files/2021/yt-vmlKdyXP4nk@2x.jpg 2x" alt="Play Is Hudson Yards Good For New York?" style="width:500px;height:281px;" /></a></p>

Unfortunately, scale is about all it had going for it by the time I saw photos of phase one. The buildings themselves are typical glass boxes with the barest of nods to something interesting, including slanting roofs and the faintest whisper of a curve here and there. The cool Vessel pine cone aside, it felt like a wasted opportunity given the area's prime position, and the opportunity to really make something pop. One day one of my favourite cities in the world will have another Guggeneheim; just not today.

But there's another aspect to the development I didn't realise. Samuel Stein, from the Community Service Society of New York had this to say:

> We should never allow the public benefit to come in "phase two" of a majorly-financed public/private development [lest it be put on hold indefinitely].
>
> If we ever do another project like a Hudson Yards, we should be looking at what the needs of people are in terms of housing, jobs, open space, and not building for the highest-end and assuming that it'll trickle down.

