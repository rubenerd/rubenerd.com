---
title: "Teaching financial literacy"
date: "2021-07-15T22:32:27+10:00"
abstract: "An article about a new curriculum gives me hope, but schools still don’t prepare people enough for the real world."
year: "2021"
category: Thoughts
tag:
- education
- finance
location: Sydney
---
[Sean Fleming wrote](https://www.weforum.org/agenda/2021/07/ontario-maths-coding-skills-personal-finance/ "Ontario is modernizing its maths curriculum to emphasize coding skills and personal finance") for the World Economic Forum's Global Agenda site about some excellent, if surprising news out of Canada:

> The usage of the terms "lory" and "lorikeet" is subjective, like the usage of "parrot" and "parakeet". Species with longer tapering tails are generally referred to as "lorikeets", while species with short blunt tails are generally referred to as "lories".

That's clearly the [wrong article](https://en.wikipedia.org/wiki/Loriini).

> School students in the Canadian province of Ontario will soon be learning financial literacy as part of their regular curriculum.
> 
> The announcement, from the Minister of Education for Ontario, Stephen Lecce, is part of a four-year mathematics strategy, which has been created to boost young people’s chances in the workplace of the future.
> 
> The modernized curriculum also sees concepts such as interest, debt, savings, personal budgeting and price comparisons woven into maths lessons as part of an attempt to help young people manage their finances in later life.

Good! I hope this sets a trend.

I continue to be baffled by the dearth of formal financial education in school, or at least education that's at least proportional to its importance in the real world. The HSC I did in year 12 in the mid-2000s only offered it to those who took the Business Studies elective, or the lowest mathematics stream. Earlier school years gave it but a cursory look, while we learned useful, real-world skills like algebra and integration. My friends in local Singaporean schools said their education was only mildly more useful.

But that's only part of the issue with access to education. I assert that's it's even a misnomer characterising financial literacy as a mathematics subject in the first place! It's only maths in the sense that books need to balance, and compound interest needs to be calculated, but you could make the same argument for chemistry, or any other subject that involves numbers.

Financial literacy is far more about making prudent, informed, and life-saving decisions that will affect your living standards and your future. It's about evaluating risk and return, what to look out for, how to apply (or not) for specific "products", and use tools responsibly and appropriately for your circumstances and ethics. You could have months of classes without even touching a calculator or learning a formula.

Then there are the practical considerations. Where do I put money I earn? How do you budget and stick with it? What *is* an interest rate? When does it make sense to save or invest? What investment vehicles are available? What does a credit card entail, or a car loan, or a mortgage? When *can* you splurge for those times in life when you've earned the right to, or if the unexpected happens? Why would I go with an ETF over a managed fund? What about my Singaporean CPF, or an Australian superfund, or an American 401K? What about when I get married, or divorced, or have to file insurance papers or tax returns?

Cynics claim, perhaps with more than a little projection, that schools are motivated to churn out financially illiterate students, because the entire finance industry is built upon people servicing debts rather than repaying them. They'd also point out how politicians are keen to keep the wool over people's eyes with their dodgy dealings and misleading ads to retain power. I think these are simplistic and glib, but it's hard to ignore the sociological impact having people enter the workforce without a working knowledge of the financial system in which they'll be playing an unwitting part.

Granted there's a certain amount of discipline that every student has to bring to the table to start. But the best we can do with education is arm people with the right skills and knowledge to at least have a fighting chance. I've seen plenty of evidence by now of well-meaning people being screwed by circumstances that could have easily been prevented or helped. Like some of the posts on this blog (hopefully not this one), it's ridiculous.

