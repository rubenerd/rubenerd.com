---
title: "Making your booleans consistent, y/y?"
date: "2021-07-16T10:45:35+10:00"
abstract: "Records should show all TRUE or all FALSE to denote optimal state."
year: "2021"
category: Software
tag:
- development
- logic
location: Sydney
---
I'm sure there's a design methodology or formal name for what I'm about to describe, but I still see the reverse happen far too often in systems I interact with on a daily basis. It doesn't matter how you deploy boolean values in your specification, API, application, or system as long as you apply them *consistently!*

This is important for all the usual reasons consistency is important, but the most critical is that it reduces chance for logical mistakes, both in comparisons within your code, and by humans who have to mentally parse what your system is presenting.

Take these attributes from an API as an example:

* Available
* Is_Broken
* NotOffline

Leaving aside the inconsistent naming scheme emblematic of an old system that's had attributes grafted on with little thought over time, what are the optimal states of these? Or asked another way, when is true good and false bad?

* Available? True is good
* Is_Broken? False is good
* NotOffline? True is good

This means if I looked up the state of this record, I'd be looking for a True, False, and True to confirm optimal operation. In the words of Gwen Stefani from the mid-2000s, *this shit is bananas*. Or is notBananas false?

The example was obvious, but things get trickier when we start to think like a classical <abbr title="object-oriented programming">OOP</abbr> developer with nouns, verbs, and adjectives. The temptation would be to create a spec that looks like this:

* Available
* Broken
* Offline

But that has the same problem. We want something Available, that's not Broken, and that's not Offline. We could slap "Not" in the attribute names for the latter two, which may indeed be necessary in some circumstances. But a better solution would be:

* Available
* Fixed
* Online

Thanks for letting me vent!

