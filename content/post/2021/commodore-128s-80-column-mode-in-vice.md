---
title: "Commodore 128’s 80 column mode in VICE"
date: "2021-02-22T15:13:14+11:00"
abstract: "Type F9 then x."
thumb: "https://rubenerd.com/files/2021/vice-c128-80-columns@1x.jpg"
year: "2021"
category: Software
tag:
- commodore
- commodore-128
- commodore-128-series
location: Sydney
---
For those in a hurry: type **F9**, then **x** when x128 starts to activate its 80-column mode. Does this count as part three in my [Commodore 128 series](https://rubenerd.com/tag/commodore-128-series/)? Let's say so!

<p><img src="https://rubenerd.com/files/2021/vice-c128-80-columns@1x.jpg" srcset="https://rubenerd.com/files/2021/vice-c128-80-columns@1x.jpg 1x, https://rubenerd.com/files/2021/vice-c128-80-columns@2x.jpg 2x" alt="Screenshot showing x128 with the Commodore 128 easter egg in 80-column mode from the emulated VDC." style="width:500px; height:350px;" /></p>

[VICE](https://vice-emu.sourceforge.io/) is an excellent and well-maintained 8-bit Commodore emulator. It's how I learned how to use Commodore computers before taking the plunge into real hardware, and I still use it to write BASIC code and test downloaded software. It can emulate Commodore disk drives, datasettes, Geo-RAM expansion cartridges, and that famous catch-all of *much more!*

The x128 executable starts an emulated Commodore 128, with separate windows for the 40 column VIC-II output, and 80-column for the VDC. I wanted to use the 80-column mode, but couldn't figure out how to activate it given my MacBook Pro doesn't exactly have a 40-80 column toggle switch like my real hardware does! Pressing **F9** then **x** gave me a blinking cursor on the 80-column output.

I intend to write a longer post specifically about its C128 features, and maybe delve a bit into the TED side of the fence too with the Plus/4.
