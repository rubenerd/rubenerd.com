---
title: "The best we can do with passphrases now"
date: "2021-09-16T09:00:40+10:00"
abstract: "Doc Searls summarises our situation on his blog with a quote that’s shorter than my intro."
year: "2021"
category: Internet
tag:
- passphrases
- passwords
- security
location: Sydney
---
[Doc Searls summarised](https://blogs.harvard.edu/doc/2021/09/08/password-hell/) our current security situation so succinctly last Wednesday, this introduction is longer than the quote itself:

> The best we can do with passwords is the best that password managers can do.

I've [written before](https://rubenerd.com/ncscs-password-advice/) that even getting password managers widely adopted is an uphill battle, let alone the fact they don't solve the root problem either. Ultimately we're still anchoring our trust with a word or phrase (we hope) that only *we* know.

There are compelling alternative auth systems today, but we're stuck in a chicken and egg scenario where widespread adoption is hampered by a lack of understanding, which exists because there's no widespread adoption. But I feel like something has to give soon.

