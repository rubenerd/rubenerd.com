---
title: "Comestibles containing holes"
date: "2021-05-21T21:21:54+10:00"
abstract: "Happy Friday evening!"
year: "2021"
category: Thoughts
tag:
- food
- lists
- pointless
location: Sydney
---
Happy Friday evening! This is not an exhaustive list, it's only the first ones that popped into my head like so many corn kernels.

* Bagels
* Doughnuts and cronuts
* Incompletely-filled canolli
* Roll cake
* Calamari, and other squid-based comestibles
* Swiss cheese
* Diced green/spring onions
* Bread, *with inclusions*
* Penne pasta
* Funnel cake
* Kürtőskalács (I cheated and looked up the spelling)
* Onion rings
* Half-baked analogies

These are a sampling of foods that *don't* have holes, for the sake of completeness and comparison:

* Short soup
* Knödel
* Charcuterie boards, whatever they are

