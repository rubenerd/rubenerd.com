---
title: "Introverts and Covid"
date: "2021-02-15T19:35:32+11:00"
abstract: "No, we’re not flourishing either, folks!"
year: "2021"
category: Thoughts
tag:
- covid-19
- introversion
- philosophy
location: Sydney
---
A day after my birthday last year, Andreas Kluth [wrote for Bloomberg](https://www.bloomberg.com/opinion/articles/2020-03-28/coronavirus-for-introverts-quarantine-can-be-a-liberation) about one potential ray of sunshine in *These Covid Times&trade;*. It'd be a tad solipsistic to assume the date of publication had any bearing on its significance, but it's still fun to pointlessly speculate!

> Now what is the essence of quarantine? “Social distancing,” of course. [..] unlike an open-plan office, Zoom can be turned off. 
> 
> Introverts in quarantine are thus less likely than extroverts to feel deflated, isolated or bored, and more likely to be energized, perhaps welcoming the lack of distraction to go deep into, well, whatever. Solitude can make people creative.

Plenty of other journalists have followed a similar theme. Introverts appreciate quiet time, and working from home is quiet, therefore we're hard wired to thrive in this new world. Yay!

Except, it's not true... at least not for most of us.

Isolated introverts still need varying degrees of physical contact, for our emotional and psychological well-being. We're still human beings, and video calls are a poor replacement at best, and [counterproductive at worst](https://rubenerd.com/video-conference-call-fatigue/). It's something we have in common with extroverts, though it manifests in different ways.

Conversely, the journalists who write these posts forget that the rest of us *still live with people!* Just because we're not in an office doesn't mean we're not in close quarters with other human beings. Some of us haven't had more than an hour or so of occasional alone time for almost a year, and it makes us anxious, fatigued, and irritable. Not to mention what parents must feel on top of that, or their kids.

Even in Fortress Australia where we have cases in the single or double digits at any given time, plenty of us are still working cautiously from home. In certain states lockdowns still occur when a flareup happens. Our weird introvert brains don't care if the people we're surrounded with are friends or loved ones, alone time is still as non-existent as the open-plan offices we escaped from. In some ways it's worse, given we never get a break at all, and the lines between where work ends and personal lives resume are blurrier than the RF-modulated output on my 8-bit Commodores.

None of this should come as a surprise, and chances are you're in a similar boat if you've come across my silly blog and have made it this far reading this. But in the rush to talk up what are assumed to be our strengths to cope with these circumstances, our extrovert friends miss the point once more. Maybe it's the fact they're not getting their own dopamine fix from the parties they can't attend. Covid is fun for all of us, isn't it?

