---
title: "That one specific spammer who can’t pad"
date: "2021-01-20T08:59:01+11:00"
abstract: "And is it too much to ask for a different fake name each time?"
year: "2021"
category: Internet
tag:
- spam
location: Sydney
---
There must be a package out there that generates random names for spammers, or they have a list of first and last names they concatenate and blast out. Nah that sounds too much like work, there's probably something on npm, along with a [way to pad](https://preview.npmjs.com/package/string-padding) between the names.

But someone's scripts must not be working, or their source files have been clobbered by single names. Cassandra Smith has appeared almost weekly for the last six months, and she has a penchant for selling customer lists for users of SalesForce, Azure, and Nutanix. I only ever see her when I'm doing a weekly trawl of my spam folder, and she always ends with:

> Best Regards, Cassandra Smith Marketing Campaign |Demand Generation Coordinator

Looks like someone else needs help with string padding.

