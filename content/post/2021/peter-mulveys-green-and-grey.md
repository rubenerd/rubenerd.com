---
title: "Peter Mulvey’s Green and Grey"
date: "2021-10-11T17:03:25+10:00"
abstract: "Needed this today ♡."
thumb: "https://rubenerd.com/files/2021/yt-poAGAaDc-gU@1x.jpg"
year: "2021"
category: Media
tag:
- health
- looking-up
- music
- music-monday
- psychology
location: Sydney
---
Today's [Music Monday](https://rubenerd.com/tag/music-monday/) takes us to [Peter Mulvey](https://www.petermulvey.com/) and his [video channel](https://www.youtube.com/channel/UCm5xFYw0KdxpyuvsvirwhsA). I always have time for this poet and wearer of epic hats.

<p><a target="_BLANK" href="https://www.youtube.com/watch?v=poAGAaDc-gU" title="Play New Song: Green and Grey"><img src="https://rubenerd.com/files/2021/yt-poAGAaDc-gU@1x.jpg" srcset="https://rubenerd.com/files/2021/yt-poAGAaDc-gU@1x.jpg 1x, https://rubenerd.com/files/2021/yt-poAGAaDc-gU@2x.jpg 2x" alt="Play New Song: Green and Grey" style="width:500px;height:281px;" /></a></p>

