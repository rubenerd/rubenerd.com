---
title: "WWR: Singer Songwriter Heaven"
date: "2021-02-27T10:32:52+11:00"
abstract: "Of course they have to be here, heaven needs new music too."
thumb: "https://rubenerd.com/files/2021/yt-nXNLVxo1xZg@1x.jpg"
year: "2021"
category: Media
tag:
- debra-schade
- music
- music-monday
- whole-wheat-radio
location: Sydney
---
I was going to sit on this until the next [Music Monday](https://rubenerd.com/tag/music-monday/), but it couldn't wait.

<p><a href="https://www.youtube.com/watch?v=nXNLVxo1xZg" title="Play Singer Songwriter Heaven"><img src="https://rubenerd.com/files/2021/yt-nXNLVxo1xZg@1x.jpg" srcset="https://rubenerd.com/files/2021/yt-nXNLVxo1xZg@1x.jpg 1x, https://rubenerd.com/files/2021/yt-nXNLVxo1xZg@2x.jpg 2x" alt="Play Singer Songwriter Heaven" style="width:500px;height:281px;" /></a></p>

I heard this on the Whole Wheat Radio stream this morning. It's such a fun, jovial song, delivered with a wink and a smile. But it still choked me up towards the end, imagining my mum at one of those cloud nine cafés with her autoharp and lute. She's in good company with all those others taken during their prime. *Of course they have to be here, heaven needs new music too.*

His music is on [Bandcamp](https://darrylpurpose.bandcamp.com/), along with the [live album](https://darrylpurpose.bandcamp.com/album/live-at-coalesce-2005) he released with Julie Beaver. It's being added to the list.

