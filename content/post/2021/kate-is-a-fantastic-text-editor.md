---
title: "Kate is a fantastic text editor!"
date: "2021-12-20T10:01:27+10:00"
abstract: "Rediscovering the KDE editor."
year: "2021"
category: Software
tag:
- kate
- kde
- text-editors
location: Sydney
---
My recent post about [Kate](https://kate-editor.org/) on the [Bird Site](https://twitter.com/Rubenerd/status/1472387060451274757) garnered more likes than almost anything I've posted in fourteen years, so I thought it was worth exploring here too. This is also my first blog post written in Kate in probably a decade or more. Hi!

Kate is the KDE Advanced Text Editor, and it's one of the many, *many* goodies one gets when using the [Plasma Desktop](https://kde.org/plasma-desktop/). You can install Plasma yourself (like I [do on FreeBSD](https://www.freshports.org/x11/plasma5-plasma-desktop/), thanks maintainers!), or there are Linux distributions such as [KDE Neon](https://neon.kde.org/) and [Kubuntu](https://kubuntu.org/) that ship with it by default for you. Others usually give you the option to replace their default desktop with it too, like [Debian](https://wiki.debian.org/KDE). Like other KDE applications, you can also run it on whatever desktop you want.

My move back to KDE full time it is a topic for a future post, but spending only a few minutes editing files with Kate again was enough for me to remember how *spectacular* it is. It punches *far* above its weight in polish, feature set, and performance, and it floors me that it isn't as widely recognised.

Steve Gibson used to refer to the *Tyranny of the Default* when it came to security settings that nobody ever changes. I think it's also a useful yardstick for software usability. If I grok software immediately after installing it, I take that as a sign that the developers and I are on the same wavelength, and therefore I'll love using it.

I contrast that with my experience with Vim, and my brief foray into Emacs. I've come to appreciate it as a text editor, and I've developed muscle memory for its cryptic but efficient commands over many, many years. But to get it (mostly) into a state where I wanted to employ it as my daily driver required a ton of configuration, and maintaining multiple plugins. I can SSH into a remote box with vanilla with nvi or Vim to edit a file, and would still prefer to do so over something like nano or ee. But I don't especially enjoy doing it.

<p><img src="https://rubenerd.com/files/2021/omake.png" style="width:500px" alt="Screenshot of Kate showing me editing my Omake page." /></p>

I appreciate this *touchy feely* stuff is impossible to quantify, and everyone's mileage will vary, but Kate is intuitive to me. Everything is where I'd expect it to be, and beyond hiding the toolbar and showing the path in the title (both of which are simple checkboxes in the Settings menu), I'm running completely stock.

It also has features that bring it closer to an IDE, like autocomplete, and Git integration that has no business being this good. From the sidebar I can get a summary of my uncommitted changes, or view a file browser, or a list of open documents. The footer exposes the language, insert mode, encoding, and detected file type. It's also trivial to fold blocks of code or markup, enter a split file view, change colour scheme, and activate additional pre-bundled plugins for things like XML validation. It even has a scrolling minimap which I thought was a gimmick when I first tried Sublime Text, but it's surprisingly useful.

It even has snippets, something I've come to [rely on heavily in Vim](https://vimawesome.com/plugin/ultisnips) for generating boilerplate.

And did I mention performance? It starts quickly, and scrolling through massive files is buttery smooth, even on my Japanese Panasonic laptop I bought for size not speed. This might sound like an odd thing to bring up, but in our [dystopian world](https://rubenerd.com/1password-electron-flareup-is-good-news/) of Electron, it's important to remind people that compiled, well-crafted software that respects your system does still exist.

A visualisation I've immediately come to appreciate is the line number tray, which indicates with a green or yellow bar whether a line of text has been changed since last save. This is brilliant for everything I write, but it's especially a lifesaver for huge LaTeX and XML files where a subtle markup alteration can result in massive changes, or YAML where some inadvertently-altered indentation can break it.

And finally, the Terminal Panel. For a quick Perl script or to validate a document, it's *so* handy to be able to pop up a terminal that switches to the directory to where your current file is. I achieved this in the past with Vim and tmux, but I love having it accessible without a second thought. That's basically Kate in a nutshell; it takes all the configuration, tinkering, and fuss out of my environment so I can concentrate on what I want to do. I'm slowly realising how valuable this is.

Much of this isn't new; Kate has been around for many years. But rediscovering it all has been a joy. There are even binaries for [macOS and Windows](https://kate-editor.org/get-it/) now too, so everyone can give it a try.
