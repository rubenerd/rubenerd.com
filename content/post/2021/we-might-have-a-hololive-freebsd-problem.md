---
title: "We might have a Hololive FreeBSD problem"
date: "2021-01-12T11:01:06+11:00"
abstract: "My FreeBSD jails are all named for Hololive characters now, not Star Trek ships or models"
thumb: "https://rubenerd.com/files/2021/ina-hololive-holomyth@1x.jpg"
year: "2021"
category: Software
tag:
- bsd
- freebsd
- jails
- hololive
location: Sydney
---
<p><img src="https://rubenerd.com/files/2021/ina-hololive-holomyth@1x.jpg" srcset="https://rubenerd.com/files/2021/ina-hololive-holomyth@1x.jpg 1x, https://rubenerd.com/files/2021/ina-hololive-holomyth@2x.jpg 2x" alt="Art of the Hololive En girls by Ina." style="width:500px" /></p>

The hostnames on my machines were always Star Trek ships, then they were anime characters. Now *this* has happened on Clara's and my [new homelab server](https://rubenerd.com/building-a-new-homelab-server/), and weirdly I have no problem remembering what each one was for.

    holo$ sudo jls
    
    ==> JID  IP Address  Hostname              Path
    ==>   1  10.8.8.81   gura.holo.lan         /var/jail/gura  
    ==>   2  10.8.8.82   ina.holo.lan          /var/jail/ina
    ==>   3  10.8.8.83   kiara.holo.lan        /var/jail/kiara
    ==>   5  10.8.8.84   cali.holo.lan         /var/jail/cali
    ==>   9  10.8.8.88   ameliaWATSON.holo.lan /var/jail/ameliaWATSON
        
    holo$ ssh gura
    ==> Heh heh hyuh hyuh! A!     
        
    holo$ ssh ina
    ==> Ina Ina Inaaaa~    
        
    holo$ ssh kiara
    ==> Kikkeriki!    
        
    holo$ ssh cali
    ==> ... guh?     
        
    holo$ ssh ame
    ==> Amelia, a-WAT-SON!

Art was by [Ina herself](https://www.youtube.com/watch?t=79&v=8c4_22LjGQo)!

