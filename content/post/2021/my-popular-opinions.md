---
title: "My popular opinions"
date: "2021-10-26T08:04:58+10:00"
abstract: "There are too many unpopular ones. Here’s my attempt to tilt the scales back towards something more positive"
year: "2021"
category: Thoughts
tag:
- food
- popular-opinions
- opinions
- personal
location: Sydney
---
There are altogether too many *unpopular opinion* posts circulating on social media and blogs. This is my attempt to reverse this.

* Freshly-baked bread, toast out of the toaster, and a pan with frying onion and garlic, all have breathtaking aromas. In that you voluntarily take more breaths to smell them.

* Repairability is a good thing.

* It's a nice gesture to let people with prams and wheelchairs into lifts and trains first. Even better if you assist if they look like they're struggling (I qualify that, because I had a colleague in a wheelchair many years ago who said he wanted to be "treated like everyone else, not a unique case").

* Cheesecake is pretty great.

* Few things are as satisfying as removing redundant code.

* Sleep is underappreciated, even among those who admit they need more of it.

* The Beatles had some good music.

* Respecting someone's privacy and security is almost always a good idea.

* Cleaners, garbage collectors, sewer maintainers, and sanitation workers keep us healthy, deserve serious respect, and should be paid much, much more.

* Hello dog. *Who's a good dog? You are! You're a good dog*.

* Under-promise. You can either over-deliver, or use the added slack in the timeline to absorb the things life likes to throw at us.

* It's a shame when you spill coffee on your beige pants, as I did this morning, surprising nobody.

* It's better not to buy the junk than recycle it.

* Text-based server logs, and basic UNIX text-processing tools, are wonders of the modern world and don't get nearly enough praise.

* [Hakos Baelz](https://www.youtube.com/channel/UCgmPnx-EEeOrZSg5Tiw7ZRQ/videos) has *unreasonably* funky opening music on her streams.

* Mismatching socks are more fun, especially if they're bold colours. Related: Japanese idol costumes that go as far as having mismatched sock and stocking *lengths* are the pinnacle of fashion design.

* Art galleries, museums, and art gallery museums are amazing places that Ruben needs to spend more time in.

* A working computer, if rare, is wondrous.
