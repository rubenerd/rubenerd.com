---
title: "Explaining geometry with pizza"
date: "2021-06-06T12:24:06+10:00"
abstract: "An excerpt from an interview with Jordan Ellenberg"
year: "2021"
category: Thoughts
tag:
- books
- food
- mathematics
- science
location: Sydney
---
[Derek Thompson interviewed Jordan Ellenberg](https://www.theatlantic.com/ideas/archive/2021/06/jordan-ellenberg-geometry-explains-world/619051/) about his new book *Shape: The Hidden Geometry of Information, Biology, Strategy, Democracy, and Everything Else.* Alongside a discussion on how many holes a pair of pants has, this was my favourite observation:

> Pizza has a surface with zero curvature, which means a slice resists bending both vertically and horizontally at the same time (unlike the double curvature of, say, a Pringle chip). Your brain intuits this without any assistance from geometric theory. But the theory exists anyway. In 1827, Carl Friedrich Gauss proved the Theorema Egregium—roughly “Awesome Theorem”—which, extremely simplified, says that you can’t change an object’s curvature and keep its geometry intact. An orange peel has positive curvature, and you can’t flatten it without ripping or stretching the peel. Paper has zero curvature, and you can’t fold it into the shape of an orange. A piece of pizza is like a piece of paper: Fold it horizontally, and it will not droop vertically.

And this was the best among my *"wait... huh!"* moments:

> [A]n irrational number is a real number that you can’t write as a simple fraction. The golden ratio turns out to be the most irrational of all the irrational numbers. That is, it’s the hardest to approximate with a fraction. Pi is irrational, but a mathematician discovered that you can get very close to pi with a simple fraction: 355 divided by 113. That’s much harder to do for the golden ratio.

