---
title: "Rubenerd.com linked to on Lobste.rs"
date: "2021-03-16T16:51:51+11:00"
abstract: "Thanks to ethoh for sharing :)"
year: "2021"
category: Internet
tag:
- commodore-128-series
- links
- weblog
location: Sydney
---
Tangentially-related to my ongoing [Commodore 128 series](https://rubenerd.com/tag/commodore-128-series/), I got quite the surprise on Mastodon this morning! My post about [troubleshooting my Commodore 128's 80-column mode](https://rubenerd.com/troubleshooting-my-commodore-128s-80-column-mode-part-1/) was submitted to the [Lobste.rs](https://lobste.rs/s/a6jgu2/troubleshooting_my_commodore_128_s_80) news site.

Further Commodore 128 posts are on hiatus while I wait for international shipping to deliver some more parts. I miss living in Singapore and being able to go to Sim Lim Square and Tower for all manner of assorted components. I even got a 6510 and a Z80 from an IC shop as recently as a few years ago, just because.

Thanks to [ethoh](https://lobste.rs/u/ethoh) for sharing, and [Screenbeard](https://aus.social/@Screenbeard) for inviting me, and hello to all of you who've made it to my esoteric, disjointed corner of the web! I'm a bit weird, but mostly harmless.

