---
title: "My FreeBSD laptop... without a GUI!?"
date: "2021-10-28T08:39:31+10:00"
abstract: "Realising I didn’t need X for my on-call machine turned into a fun experiement."
thumb: "https://rubenerd.com/files/2021/rz6-download-02@2x.png"
year: "2021"
category: Software
tag:
- bsd
- freebsd
- panasonic-lets-note
location: Sydney
---
This feels *very* strange, but I'm writing this post from my on-call FreeBSD laptop, *without X!* I have everything I need here to remotely troubleshoot stuff and write, all without needing a GUI. It's been oddly fun getting back into all this stuff, so I thought I'd share it.

<p><img src="https://rubenerd.com/files/2021/rz6-download-02@1x.png" srcset="https://rubenerd.com/files/2021/rz6-download-02@1x.png 1x, https://rubenerd.com/files/2021/rz6-download-02@2x.png 2x" alt="The Panasonic Let's Note CF-RZ6" style="width:250px; height:219px; float:right; margin:0 0 1em 2em;" /></p>

To take a step back, might lead you to fall off a cliff. I'm working on creating the ultimate "on call" FreeBSD laptop. It needs a few VPN clients, text editors, orchestration tools, QEMU for quick tests, git and rsync for keeping things current, and... that's about it. When I realised that none of these require a graphical environment, I decided to see if I could live entirely within a tty, just like the old times.

My cute Japanese Panasonic Let's Note CF-RZ6 (details on the [FreeBSD Wiki](https://wiki.freebsd.org/Laptops/Panasonic_Lets_Note_CF-RZ6)) that I bought during AsiaBSDCon 2019 in Tokyo is perfect for this:

* It supports full suspend/resume, either with [zzz(8)](https://www.freebsd.org/cgi/man.cgi?query=zzz) or simply closing and opening the lid. I can add a new Wi-Fi network by adding a few lines to [wpa_supplicant.conf(5)](https://www.freebsd.org/cgi/man.cgi?query=wpa_supplicant.conf) and issuing `service netif restart`. Easy!

* Booting with UEFI brings up beautiful, crisp, HiDPI text automatically on its screen. The resolution, and my self-inflicted myopia, are more than adequate for a couple of vertically-tiled terminal windows.

* Its trackpad, like most PCs, is *terrible*, so anything to avoid using it is a plus. The keyboard is tiny, but my fingers are spindly and hit the keys just fine.

I settled on tmux to use as my *de facto* window manager. I moved the notification bar to be at the top of the screen, because I'm a gentleman, and remapped some of the keys. Soon after I was listening to music, committing changes, editing files, and logging into remote servers, all within a comfortable text environment.

It also had some other side effects. I wondered if I could run this stuff in a jail, which I'd always wanted to do with Xorg. You can! Say hello to separate isolated jails for work and personal stuff.

The problem was, it lead me down a rabbit hole of what else I could run on this thing without touching a GUI.

* [links](http://links.twibright.com/) for text web browsing. It's amazing how distraction free the web is once you strip it back. You can also tell immediately which sites are designed with accessibility in mind (or not).

* [Bombadillo](https://bombadillo.colorfield.space/) for the new Gemini protocol, and Gopher. Living in text makes you appreciate the simpler times (I've got something to announce about that too at some point).

* [Musikcube](https://musikcube.com/) for local music files. Its UI is everything that was great about the original iTunes, with a sidebar for artists and simple navigation.

* [nnn](https://github.com/jarun/nnn) is a shockingly fast and flexible console file manager. I used Midnight Commander for nostalgia before, but this is next level.

* [Alpine](http://alpine.x10host.com/) is still the nicest mail client I've used, even after all these years. Getting it to run with all my disparate email providers was... fun? But it works. Maybe that's a topic for a future post.

* QEMU can export a curses interface, so I can run DOS software alongside contemporary and actually useful stuff.

* bsdgames, nbsdgames, ttysolitare for other <del>pointless</del> useful distractions.

Graphical web browsing, and monstrosities like Electron-based chat apps won't be running in here any time soon (at least, not without a proxy). But I can farm that off to my smartphone.

It also has a touch of nostalgia, not just for classic \*nix, but for the MS-DOS machine I had as a child. Only this one is rock solid, fast, has a great battery, and I can carry it anywhere.

I'm sure I'll run into edge cases that require X at some point, but for now I'm enjoying this bizarre little setup.
