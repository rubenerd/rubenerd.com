---
title: "NCSC’s passphrase advice"
date: "2021-08-09T09:43:36+10:00"
abstract: "Password Manager > String of Words > Pseudo-Random Gibberish."
year: "2021"
category: Software
tag:
- keepassxc
- password-managers
- security
location: Sydney
---
[Kate R summarised](https://www.ncsc.gov.uk/blog-post/the-logic-behind-three-random-words) the UK National Cyber Security Centre's longstanding advice for using a string of words over gibberish:

> Whilst not a password panacea, using 'three random words' is still better than enforcing arbitrary complexity requirements. [...] Our minds struggle to remember random character strings, so we use predictable patterns (such as replacing the letter ‘o’ with a zero) to meet the required 'complexity' criteria.

She writes that the uptake of password managers remains "low", so this is the best practical defence. I find it hard to fault this.

I'd also prefer password managers (such as the excellent open source [KeePassXC](https://keepassxc.org/)) be used. But they also come with their own problems in general, namely that you risk exposing your entire existence with a weak master password, or if the binary blob storing your credentials is compromised or stored incorrectly. I think it's fantastic that their use is being encouraged, but let's not treat them as a panacea, either.

*(There's also the bigger question about whether they're a crutch, and that passphrases for auth are an idea who's time has passed. We've kicked the can down the road somewhat by allowing people to log in with their Facebook account et.al., but the chain of trust is still anchored with a passphrase at the other end. Two-factor authentication is being more widely used, but certain implementations aren't perfect either).*

I'd also prefer to see wiser use of the term *passphrase* in lieu of *password* to encourage people to rethink how they use them, as I've done here. My experience has been that subtle rebranding like this *does* affect behavior, like calling myself a super genius. Wait, I forgot the "not a" before that.
