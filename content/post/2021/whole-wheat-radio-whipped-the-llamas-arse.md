---
title: "Whole Wheat Radio whipped the llama’s arse"
date: "2021-01-07T14:57:00+11:00"
abstract: "Shamelessly adding music player reviews to our music wiki back in the day."
thumb: "https://rubenerd.com/files/2021/wwr-2008@1x.jpg"
year: "2021"
category: Internet
tag:
- music
- music-players
- reviews
- whole-wheat-radio
location: Sydney
---
There's so much I miss about the late *Whole Wheat Radio*, but one thing in particular was its vast knowledge base of musicians, albums, genres, regions, interests, and more. It was one of the largest online repositories of music knowledge, and easily the largest collaborative wiki dedicated exclusively to independent artists. Modern wikis with their gaudy designs and wall-to-wall advertising have yet to usurp it in sheer utility, approachability, and depth.

In exchange for helping add metadata, images, and templates to the wiki, the proprietor Jim Kloss graciously afforded me leeway to also use it to share research into the best ways to *listen* to music as well. I'd been an iTunes user since the first generation of iPods on classic Mac OS, but soon realised there was a world of console and GUI players for all sorts of platforms and devices one could use; some of which even resembled my beloved Winamp.

<p><a target="_BLANK" href="https://rubenerd.com/files/2021/Desktop.gnome.wwr.png"><img src="https://rubenerd.com/files/2021/wwr-2008@1x.jpg" srcset="https://rubenerd.com/files/2021/wwr-2008@1x.jpg 1x, https://rubenerd.com/files/2021/wwr-2008@2x.jpg 2x" alt="Screenshot from 2008 showing the Whole Wheat Radio homepage, and the Exaile media player." style="width:500px" /></a></p>

The *Who's Listening* page would show where people were in the world, and which client they were using to stream music from the site. These would be auto-linked using wiki syntax to a page about that client; presumably so you could learn more about how to use it. I soon discovered that I could listen with an obscure or unusual player which would appear with red text, indicating a page for it didn't exist. I'd use that link to create a new page about the player, how to download it, which OSs it supported, how to add Whole Wheat Radio stream feeds to it, and with a few screenshots of it in action.

I shamelessly exploited this loophole to create dozens of articles, and in the process learned about new devices and players. I didn't go as far as to fudge a useragent, but I was amazed sometimes at what a player would present itself as. My old man streamed WWR in his car during trips between Singapore and Kuala Lumpur using an obscure device that returned a UUID of all things. *iRandom!*

<p><a target="_BLANK" href="https://rubenerd.com/files/2021/screenshot-wwr-stevedurr.png"><img src="https://rubenerd.com/files/2021/screenshot-wwr-stevedurr@1x.jpg" srcset="https://rubenerd.com/files/2021/screenshot-wwr-stevedurr@1x.jpg 1x, https://rubenerd.com/files/2021/screenshot-wwr-stevedurr@2x.jpg 2x" alt="Screenshot showing a house concert with Steve Durr in June 2008" style="width:500px" /></a></p>

Eventually Jim decided the site needed more focus, and many of these articles were deprecated in favour of content specifically about artists and their music. It was the right call, given the site had evolved away from its core mission in tangible, unproductive ways. I still have *all* the screenshots in my archives though, I should share more of them one day.
