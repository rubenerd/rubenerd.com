---
title: "Firefox 89’s Proton UI"
date: "2021-06-14T13:28:13+10:00"
abstract: "I empathise with Mozilla’s position, but wish Firefox took a stand for privacy AND accessibility."
thumb: "https://rubenerd.com/files/2021/firefox-proton-ui.png"
year: "2021"
category: Software
tag:
- colour
- design
- firefox
- mozilla
location: Sydney
---
Mozilla Firefox introduced a new theme starting with version 89, dubbed the [Proton UI](https://wiki.mozilla.org/Firefox/Proton). Here's a screenshot running on macOS Big Sur:

<p><img src="https://rubenerd.com/files/2021/firefox-proton-ui.png" alt="Screenshot showing Firefox 89, with barely discernable UI elements and thin, difficult to read icons." style="width:500px; height:230px;" /></p>

It's telling that much of the press coverage has been dedicated to describing [how to turn it off](https://www.zdnet.com/article/firefox-89-with-new-proton-interface-lands-heres-how-to-turn-it-off/ "ZDNet: Firefox 89 with new Proton interface lands, here's how to turn it off"). I love Firefox and have been using it (with brief forays into Mozilla Camino) since the Phoenix days, but it [hasn't had a good UI](https://www.dedoimedo.com/computers/firefox-89.html) for years.

But here's *Resigned Ruben* speaking: I can empathise with Mozilla's position. Apple, Microsoft, and Google have been trending away from accessibility, contrast, and functional design for a while now with macOS, Windows 10, and Chrome. Firefox had to look the same, or it'd look outdated.

A part of me wishes Mozilla would have a Jean Luc Picard moment from *First Contact*, and say "here, no further!" though. A browser making a stand for privacy *and* accessibility would be fantastic. Being similar to other browsers doesn't seem to be improving their market share.

