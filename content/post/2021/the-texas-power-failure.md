---
title: "The Texas power failure"
date: "2021-04-08T13:44:19+10:00"
abstract: "A video by Practical Engineering, and not having a personal frame of reference at all."
thumb: "https://rubenerd.com/files/2021/yt-08mwXICY4JM@1x.jpg"
year: "2021"
category: Media
tag:
- environment
- united-states
- video
- youtube
location: Sydney
---
<p><a href="https://www.youtube.com/watch?v=08mwXICY4JM" title="Play What Really Happened During the Texas Power Grid Outage?"><img src="https://rubenerd.com/files/2021/yt-08mwXICY4JM@1x.jpg" srcset="https://rubenerd.com/files/2021/yt-08mwXICY4JM@1x.jpg 1x, https://rubenerd.com/files/2021/yt-08mwXICY4JM@2x.jpg 2x" alt="Play What Really Happened During the Texas Power Grid Outage?" style="width:500px;height:281px;" /></a></p>

Practical Engineering did the [best video I've seen](https://www.youtube.com/watch?v=08mwXICY4JM) explaining what happened to our Texan friends earlier this year, with their unprecedented snow storm and outages.

One of my [first ever blog posts back in 2004](https://rubenerd.com/boxing-day-palm-pilots/) was about the Southeast Asian tsunami which killed thousands of people, wiped out entire cities, and brought down everything from power to water supplies. I see these disasters and have no frame of reference whatsoever; with all my family troubles growing up I've never not had water, food, or shelter. As I said back then, I can't think of anything else to say.

