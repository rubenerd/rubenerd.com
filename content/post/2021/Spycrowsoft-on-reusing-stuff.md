---
title: "Spycrowsoft on reusing stuff"
date: "2021-10-23T09:04:09+10:00"
abstract: "Reusing and repurposing stuff before throwing it away."
year: "2021"
category: Hardware
tag:
- feedback
- stuff
location: Sydney
---
Companies that attempt to play up their environmental credentials rely *heavily* on telling us we can ship them back hardware to recycle. But they forget the two R's that come before it: reduce and reuse.

Spycrowsoft came at my [being sick of stuff](https://rubenerd.com/being-sick-of-stuff/) post from Thursday from that angle:

> I have a couple of boxes of old stuff, one for each category and one labelled MISC. Each time before I buy something that is not a consumable, I force myself to sift through the respective box to see if there is something in there which can be used to "make due" for whatever I want to buy next.
> 
> Each time I peer into those boxes, some stuff inevitably gets thrown out while other stuff gets recycled or repurposed. The wires inside VGA cables for example, make excellent signalling wires in all kinds of microcontroller and electronics projects if you care to salvage them.

I'd did something similar without realising when I got into Hi-Fi gear and retrocomputing. I have a few of those tiny plastic drawer chests full of caps, wires, connectors, switches, ICs, and such. Much of this was harvested from dead machines, and have come in tremendously handy for DIY jobs and even building my modern sleeper PC.

Extrapolating that idea to stuff in general is an interesting idea. My only fear would be the genes from my parents would assert themselves and mean I'd end up with dozens of tubs of stuff! I guess it comes down to having that discipline Spycrowsoft mentions, and still knowing when to make the call to recycle or throw something away when its clear it couldn't be useful in the short to medium term. Holding onto everything because it *might conceivably* be useful is how hoarding starts.
