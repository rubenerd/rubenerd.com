---
title: "Singapore and Japan dialogue on Covid"
date: "2021-06-01T12:20:56+10:00"
abstract: "Mutual recognition of health certificates."
year: "2021"
category: Thoughts
tag:
- covid-19
- japan
- politics
- singapore
location: Sydney
---
From [Singapore's Ministry of Foreign Affairs](https://www.mfa.gov.sg/Newsroom/Press-Statements-Transcripts-and-Photos/2021/05/20210525-PM-Lee-Telephone-Call-with-Japanese-PM-Suga "Prime Minister Lee Hsien Loong's Telephone Call with Japanese Prime Minister Suga Yoshihide")\:

> The Prime Ministers reaffirmed the excellent relations between Singapore and Japan.  Prime Minister Lee said that Singapore looked forward to deepen economic and public health cooperation with Japan to support the post-COVID-19 recoveries of both countries, including in areas such as the mutual recognition of health certificates.

I wonder if this will end up being the model for Covid border openings? 

*(I've also started referring to it as Covid, instead of COVID or COVID-19. We don't write LASER anymore, and it's become a genericised acronym. It also looks less shouty, which I think we all need).*
