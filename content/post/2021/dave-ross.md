---
title: "To the coolest uncle ever, Dave Ross ♡"
date: "2021-11-12T09:43:00+10:00"
abstract: "Rest in peace, and rock on. 🎵"
thumb: "https://rubenerd.com/files/2021/dave-trumpet.jpg"
year: "2021"
category: Thoughts
tag:
- family
- rest-in-peace
location: Sydney
---
> Classical music is okay, but it doesn't SWING!

While I was on leave and had my phones turned off, the news came down that our beloved uncle Dave passed away. The celebration service was this morning, which I was able to attend virtually despite my own current health issues, which I was tremendously thankful for.

The news hit me hard, as I'm sure it did for everyone who had the joy and honour of knowing them.

Dave and [my late mum Debra](https://rubenerd.com/dedication/) were two notes on the same score. Dave was a performer, composer, and lover of music; and the coolest damned uncle I ever knew. No really, I'd tell friends about his adventures, style, and talents, and they'd scarcely believe it. My mum often regaled us with stories of their time in New Zealand, the music they performed together, and how much she respected and looked up to him. Their close and unwavering relationship is one I'm happy to share with my own sister Elke now.

<p><img src="https://rubenerd.com/files/2021/dave-trumpet.jpg" alt="" style="width:500px; height:284px" /></p>

I think what struck people the most when meeting Dave and Debra were their humility, kindness, and wicked senses of humour. They were deeply intelligent people, well read and explored, and with strong senses of ethics and compassion.

My earliest childhood memories revolve around laugher, some to the point of tears. It's these that I prefer to think I shed again while watching the service today.

Alison, and my cousins Anna, Kate, and Niema all gave beautiful and fun remembrance stories. Dave's life was all about music, but it's clear from their words of appreciation and affection that he was also a wonderful husband and father. My favourite moments were Alison describing their long conversations, Kate's memories of baking pizza together, Niema sharing all the life lessons he brought her, and Anna describing the strong coffee Dave must now be sharing with my mum ♡.

I like to think I got my love of jazz, my voice, and so many of my mannerisms from Dave. There were times where I'd be talking with my mum and she'd smile, saying that for a split second she thought she was taking to him instead. I couldn't ever hope to match his wit, but I could think of no higher praise.

<p><img src="https://rubenerd.com/files/2021/dave-bali@2x.jpg" alt="" style="width:200px; height:282px; float:right; margin:0 0 1em 2em" /></p>

I'll still remember that fateful group family trip to Bali when a haughty, long-winded, nonsensical conversation of silliness culminated in him exclaiming that I was to be an *exploding computer operator!* Or when looking for food, he lowered the passenger window of the car to ask passers by *Salamat Sandwich?* His long beard convinced temple guides that he was a priest, granting him access to areas that regular people like you and I dare not tread.

Dave's talent, gentle demeanour, wisdom, stoicism, unwavering ethical core, and laughter are all qualities I respect, admire, and continue to see as a source of inspiration and joy.

Thank you for being a part of our lives, Dave. And thank you to Alison for giving me the video link for the service. It was an honour to share in these memories. ♡
