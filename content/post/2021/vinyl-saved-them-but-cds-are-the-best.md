---
title: "“Vinyl saved us, but CDs are the best”"
date: "2021-03-23T16:25:15+11:00"
abstract: "Discussing physical media in the context of a record store in Sydney"
year: "2021"
category: Media
tag:
- australia
- music
- news
- vinyl-records
location: Sydney
---
The Guardian Australia's Celina Ribeiro wrote a great article about the [rise of vinyl again in Australia](https://www.theguardian.com/culture/2021/mar/23/its-the-ritual-vinyl-sales-look-set-to-break-australian-records-so-who-is-still-buying-cds "‘It’s the ritual’: vinyl sales look set to break Australian records, so who is still buying CDs?"). She interviewed Peter Thiel who runs a record store in the Sydney suburb of Newtown, known for its off-beat character and culture:

> Thiele stands in the aisle in a short-sleeved button-up shirt in a tiny floral pattern. Grey hair trimmed and neat. He streams. He downloads. He listens to vinyl, and he listens to CDs.
> 
> “People say to me: ‘Sounds better on vinyl’,” he shrugs. “I go, ‘Sounds different.’
> 
> “CD quality is the best you can get, because you’ve got a master.”

Casey Liss of the Accidental Tech Podcast and Analog(ue) likens this to a tea ceremony. Selecting music from a shelf, turning on the amplifier, placing the disc on a turntable or CD tray, and giving the songs your undivided attention imbues the tunes with something you don't get from streaming. Clara and I have got into vinyl in a big way for this reason; the quality is secondary to the experience of playing it. I'll fire up my 500 GiBs of 320 KB/s AACs amassed over twenty years on a checksummed ZFS array when I want the best perceptible fidelity.

Weirdly enough, the same applies to cassettes. I only learned recently that there *were* pre-recorded albums released on type-II chrome tape, which have no business sounding as good as they do when used on a quality deck and the same speakers as our turntable. I also like the fact they've been on a journey themselves before arriving in Clara's and my little Hi-Fi setup.

Peter's second point is on collections: 

> Physical collections – like CDs, records or books – reflect something about their owner, he says. “They tell a story about you. You don’t go, ‘Look at my phone, here’s my music collection.’ It isn’t the same thing. You go to a friend’s place and you look at what albums they have.”

We've got such little space in our small apartment, but I love the fact that we managed to find just enough of it to have our precious few favourite albums on physical media again. I like to think of myself as a declutter, but that only means that what remains must be special.

Celina also talks with two other people in the store:

> The pair are part of an increasingly small cohort of music fans who continue to buy CDs, in a world where most new cars and computers no longer have a way to even play them.

I'll admit, this hit me harder than I expected. In the rush to champion the removal of optical drives from laptops to save space and weight, we didn't ask what we're losing. There's utility, in the form of being able to burn CDs and read software. But what about music?

A CD-ROM drive was my first exposure to optical music. My dad had a small Hi-Fi in his study where I used to use the family computer, and I realised at a young age that I could plug the output from the computer's SoundBlaster card into the amplifier's line input. I was playing CDs through Creative's Multimedia Deck software for years afterwards; it was a ton of fun! I liked that I had "little records" that were shiny, too.

Rational people would be quick to remind us that all these formats are unnecessary. But as I've said before here, what's rational about art? Or put another way, why does art need to be justified?

Which leads to this final comment from another patron of the store:

> Regina Safro still buys CDs in part for the physicality – and in part because it gets more money to the artist than streaming.

