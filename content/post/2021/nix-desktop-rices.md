---
title: "Nix desktop ricing"
date: "2021-08-23T03:16:40+10:00"
abstract: "Cringe name, but there are some great designs."
thumb: "https://rubenerd.com/files/2021/flyingcakes85@1x.jpg"
year: "2021"
category: Software
tag:
- bsd
- debian
- design
- freebsd
- linux
- open-source
- ghost-in-the-shell
- ssss-gridman
- pointless-anime-references
location: Sydney
---
I learned about the concept of desktop ricing a few weeks ago, in which creative people customise their desktops with their own themes, colours, fonts, icons, and layouts. The flexibility and wide range of packages can foster some pretty creative designs, especially after one is used to dealing with the rigid UIs of macOS and Windows.

I assumed rice referred to all the config dotfiles one would need to edit for window managers like i3, but according to [Awesome Open Source](https://awesomeopensource.com/project/ibrahimbutt/direwolf-arch-rice)\:

> "Rice" is a word that is commonly used to refer to making visual improvements and customizations on one's desktop. It was inherited from the practice of customizing cheap Asian import cars to make them appear to be faster than they actually were - which was also known as "ricing".

Even grim stereotypes aside, that connotation doesn't sound all that positive. You'd think \*nix people would be claiming their OS of choice (usually Arch) is awesome, not something cheap that a poser would tart up to look expensive. At best it sounds like a self-own.

Despite the awkward name and some of the surrounding culture, there are some great and creative designs. FlyingCake85 gets [bonus points](https://github.com/flyingcakes85/LoveBites) for using a stylised Rikka from SSSS.Gridman, as well as their Angel Beats audio collection:

<p><img src="https://rubenerd.com/files/2021/flyingcakes85@1x.jpg" srcset="https://rubenerd.com/files/2021/flyingcakes85@1x.jpg 1x, https://rubenerd.com/files/2021/flyingcakes85@2x.jpg 2x" alt="FlyingCake85's salmon-themed light desktop, with Rikka from SSSS.Gridman. Salmon-themed?" style="width:500px; height:281px;" /></p>

And BrakpanMan used the Nord colour scheme to [create this theme](https://www.reddit.com/r/unixporn/comments/p1yzqo/xfce_nord/), who also has excellent anime taste:

<p><img src="https://rubenerd.com/files/2021/BrakpanMan@2x.jpg" srcset="https://rubenerd.com/files/2021/BrakpanMan@1x.jpg 1x, https://rubenerd.com/files/2021/BrakpanMan@2x.jpg 2x" alt="BrakpanMan's Nord-themed desktop, with muted colors and darn blue/black backgrounds." style="width:500px; height:281px;" /></p>

Much of the early days of this blog were dedicated to [customising my FreeBSD desktop](https://rubenerd.com/p1188/) before I started using the stock UI and icons. I'd be half tempted to do it again. [Polybar](https://polybar.github.io/) looks especially intruiging.
