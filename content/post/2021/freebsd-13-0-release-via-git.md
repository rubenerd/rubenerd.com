---
title: "FreeBSD 13.0-RELEASE sources via Git"
date: "2021-04-18T09:26:36+10:00"
abstract: "git clone -b releng/13.0"
thumb: "https://rubenerd.com/files/2020/beastie@2x.png"
year: "2021"
category: Software
tag:
- bsd
- freebsd
- git
location: Sydney
---
Being able to easily recompile the entire OS is one of the *supreme* benefits of FreeBSD (and the other BSDs) over others like Linux. You can trust and verify that binary releases haven't been tampered with, and optimise your system with exactly what you need.

Previously one would have used my beloved Subversion to get system sources similar to this:

    # svnlite checkout https://svn.freebsd.org/base/releng/12.2 /usr/src

Since the switch to Git, you can specify which branch you want which corresponds to a release:

    # cd /usr
    # git clone -b releng/13.0 --depth 1 https://git.freebsd.org/git src

There's no Git client in base that I can tell, but you can install the [net/gitup](https://www.freshports.org/net/gitup/) tool which was written for the express purpose of downloading such sources on FreeBSD. The alternative is the full [git](https://www.freshports.org/devel/git) client, or [git-lite](https://rubenerd.com/git-lite-for-freebsd/) if you don't need as many dependencies.

Now, [back to upgrading](https://docs.freebsd.org/en_US.ISO8859-1/books/handbook/makeworld.html "FreeBSD Handbook: Updating FreeBSD from Source") this Panasonic Let's Note CF-RZ6!

