---
title: "You can search line numbers in Firefox!?"
date: "2021-06-29T23:29:08+10:00"
abstract: "Go to View Source for any page, and use the find field to type a line number. It appears in results as all the regular text does!"
year: "2021"
category: Software
tag:
- firefox
- guides
- mozilla
- today-i-learned
location: Sydney
---
There's someone born every minute who hasn't seen *The Flintstones*, as Merlin Mann says. I've been using Firefox since its original Phoenix incarnation, and I only discovered **today** that you can search line numbers in source.

1. Hit *Command+U* or *Control+U* to view the page source.

2. Hit *Command+F* or *Control+F* to bring up the find bar.

3. Type the number for the line you want to go to.

I had no idea the line numbers are rendered as plain text alongside the HTML source of a document. I was searching for an errant <code>%20</code> buried somewhere, but the first result took me to line 20. I scarcely believed my eyes.

It reminds me of an old Firefox plugin called Vimperator that would bring in Vim keybindings. I could hit a forward slash, and immediately type text to find it on the page. Alas, [their site](http://vimperator.mozdev.org/) no longer resolves.

Now I'm thinking what else I'm missing out on in this complicated piece of software. I'm probably only using the same few features it shipped with from the start.
