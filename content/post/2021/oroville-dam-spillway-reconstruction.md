---
title: "Oroville Dam Spillway reconstruction"
date: "2021-12-29T08:19:50+11:00"
abstract: "Another great video from Practical Engineering that Clara and I watched yesterday."
thumb: "https://rubenerd.com/files/2021/yt-ekUROM87vTA@1x.jpg"
year: "2021"
category: Media
tag:
- engineering
- practical-engineering
- video
- youtube
location: Sydney
---
Speaking of interesting YouTube videos, Clara and I especially enjoyed Practical Engineering's recent discussion of the [Oroville Dam Spillway reconstruction](https://www.youtube.com/watch?v=ekUROM87vTA). I like when people revisit topics they've previously discussed for a status update.

The Oroville Dam in California had a catastrophic failure of its primary spillway in 2017. The concrete structure and the supporting ground collapsed, releasing an uncontrolled water flow and eroding a a deep canyon into the side of the hill.

<p><a href="https://www.youtube.com/watch?v=ekUROM87vTA" title="Play Rebuilding the Oroville Dam Spillways"><img src="https://rubenerd.com/files/2021/yt-ekUROM87vTA@1x.jpg" srcset="https://rubenerd.com/files/2021/yt-ekUROM87vTA@1x.jpg 1x, https://rubenerd.com/files/2021/yt-ekUROM87vTA@2x.jpg 2x" alt="Play Rebuilding the Oroville Dam Spillways" style="width:500px;height:281px;" /></a></p>

Grady did a video in May this year explaining [the causes for the failure](https://www.youtube.com/watch?v=jxNM4DGBRMU). This follow-up video explained the reconstruction process, including the various options the Californian state government considered. As usual, he included plenty of useful diagrams, as well as some great drone footage with credits.

What I found most interesting was the new concrete laying method that replaces much of the water in the mixture with compressive force, similar to how bitumen is laid on road surfaces. <a href="https://www.cement.org/roller-compacted-concrete-(rcc)-old/rcc-faqs">Concrete.org has an interesting FAQ</a> about how it works.

It does leave open the question though about how many other crumbling structures are out there, especially in the West where we've bred a political culture of not wanting to spend anything for the public good... even if it's critical preventative maintenance.
