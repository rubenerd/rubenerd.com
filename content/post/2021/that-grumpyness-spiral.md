---
title: "That grumpyness spiral"
date: "2021-02-08T15:35:15+11:00"
abstract: "Emotions aren’t rational; that’s what makes them emotions."
year: "2021"
category: Thoughts
tag:
- emotion
- personal
location: Sydney
---
I took some time this afternoon once I realised I'd entered a grumpyness spiral. It begins with a couple of niggling problems, then as they pile up they begin to seem worse, then insurmountable. And distractions, don't get me started on context-switching and distractions!

Certain philosophers to self-help gurus place the blame on us for feeling this way. External factors influence our mood, but it's our choice to internalise them and brew them into frustration; or so it's argued. It sounds helpful, before you realise that emotions aren't rational. That's what makes them emotions and not decisions. They're visceral signals in our lizard brains, not a calculated assessment of our circumstances.

They say admitting something is the first step to overcoming it. I'm fully aware I'm grumpy now, but knowing that I could turn that frown upside down and be on my way to overcoming it... that just sounds irritating. I suppose they'd argue that I've talked myself into being grumpy then. They're probably right.

This doesn't even touch on the utility of being grumpy either. Maybe it's helpful external emotion to express, so people offer you a wide berth to let you get things done. If that sounds like more rationalising, it's because it probably is.

Grr, the world today, *amirite?* Don't answer that.

