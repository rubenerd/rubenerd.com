---
title: "Melissa Davey on migraines"
date: "2021-07-08T11:30:03+10:00"
abstract: "An article in The Guardian Health had some data around migraines I wasn't aware of."
year: "2021"
category: Thoughts
tag:
- health
- migraines
- personal
location: Sydney
---
Speaking of [migraines](https://rubenerd.comeven-nintendo-is-shipping-oleds-now/), Melissa Davey wrote a [great article for The Guardian's health section](https://www.theguardian.com/australia-news/2021/jul/01/i-usually-end-up-calling-an-ambulance-why-migraine-pain-is-not-just-a-bad-headache) about this pervasive problem that *isn't just a headache!*

> At first, [my migraines] were similar to [my mum's]. A sudden onset of blinding, stabbing pain on one side of my head, an overwhelming nausea, eventual forceful vomiting, dehydration, intolerance of light, exhaustion and fatigue.

This is spot on. Mine usually end up with vertigo and a dry mouth as well, just for added fun. Unlike a stomach bug where nausea instantly improves how you feel, it does nothing for a migraine.

> I’ve had imaging scans of my head. I still don’t know a clear cause.

Lord almighty, me too! All I'd theorised thus far is that it's genetic; many of my early childhood memories were formed around being told to leave mummy alone in the bedroom because she was having a severe one. But even that link might be tenuous:

> Griffiths says [..] the role of inheritance pattern and the genes involved are less clear-cut. She hopes in the future, the involvement of other genes in these migraines will be better understood, leading to more diagnostic tests and personalised treatment.

What I also didn't realise before reading this was how much more prevalent the affliction is with women, and how little research has been put into it compared to other diseases and disorders. This might go part of the way explaining why:

> Migraine is quality-of-life threatening, not life-threatening, so is doesn’t pull the heart strings for resources and funding in the way the conditions like cancer do,” Prof Anne MacGregor from the Centre for Neuroscience, Surgery and Trauma at the Barts and the London School of Medicine and Dentistry, says.
