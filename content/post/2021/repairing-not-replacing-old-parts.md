---
title: "Repairing, not replacing old parts"
date: "2021-04-09T08:51:29+10:00"
abstract: "I feel a responsibility for this stuff now."
year: "2021"
category: Hardware
tag:
- commodore
- mmx-machine
- repairs
location: Sydney
---
There's an entire philosopical, legal, and environmental discussion surrounding the right to repair, and the societal implications of using devices that have become disposable through consolidation, minaturisation, and the use of hostile parts like proprietary screws and glue. We're trained to think electronic devices have a servicable life within warranty periods. Any problems after this, and you need to throw them away and replace them.

But the calculus changes when it comes to vintage tech. All the parts from the 1980s and 1990s have been built, there's no new supply. Well, there are people using original moulds and emulating old ICs, but you know what I mean.

There's a pinch on both sides, from those who don't value old computers and throw them away, to collectors who *really* want to keep these legacy machines running. I bought most of my Commodore computer hardware in the early 2000s, back when it was old but not collectable yet. I'd easily be paying 10x more for it now, and not just because 8-bit computers are cool again. The supply has shrunk spectacularly since, as these old parts keep failing and people harvest other machines to keep the remaining fleet running.

Which leads me to the brainwave I had last night. I was sitting at my new vintage computer desk tinkering with an old Sound Blaster card, like a gentleman. I got it working again by replacing a corroded jumper! There's a sense of satisfaction and joy that comes from fixing things as opposed to just replacing them.

But now I'm starting to think I have a *responsibility* to. Me fixing that SB card means someone else out there has one more that they might be able to use for a retro build, or to replace something that *is* irreparable.

At the risk of overstating the case, if we want to preserve this hardware for future generations, it'll be people like us doing it. I want to learn more!

