---
title: "The ignominy of relegation"
date: "2021-05-30T10:06:24+10:00"
abstract: "I’m going to start using this."
year: "2021"
category: Media
tag:
- language
- news
location: Sydney
---
Deutsche Welle [reported this about the Bundesliga](https://newsbots.eu/@dw/106320943561104932) yesterday:

> Cologne spared themselves the **ignominy of relegation** after overturning a first-leg deficit in the relegation play-off against Kiel. Facing projected eight-figure losses, retaining their Bundesliga status was imperative.

I'm going to start using that.

