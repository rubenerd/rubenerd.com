---
title: "Saved search issues on Thunderbird for Mac"
date: "2021-10-12T17:21:39+10:00"
abstract: "A known bug prevents popup windows from retaining focus."
thumb: "https://rubenerd.com/files/2021/thunderbird-focus-issue@2x.png"
year: "2021"
category: Software
tag:
- errors
- mozilla
- thunderbird
location: Sydney
---
It's been a while since I created a saved search on Thunderbird, but I'm [adjusting how I do stuff](https://rubenerd.com/archiving-24-years-of-email/) and want to have a few virtual folders.

This works on FreeBSD, but unfortunately not on the Mac. For the past six months the *New Saved Search Folder* window among others loses focus and retreats to the background when you select folders, making the entire UI inaccessible. You can still quit with Command+Q.

<p><img src="https://rubenerd.com/files/2021/thunderbird-focus-issue@2x.png" alt="Screenshot of Thunderbird for Mac showing the focus issue." style="width:500px; height:155px;" /></p>

There's an [open bug report on Bugzilla](https://bugzilla.mozilla.org/show_bug.cgi?id=1699514), but the issue looks thorny enough that it won't be a quick fix. If you have access to a \*nix or Windows machine, you can create your saved searches there then import your profile back. Not ideal, but it works.

Passing on in case this was driving you batty too.

