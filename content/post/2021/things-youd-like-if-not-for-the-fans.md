---
title: "Things you’d like, if not for the fans"
date: "2021-08-13T09:33:12+10:00"
abstract: "I don’t let others dictate my hobbies any more, but I empathise with people who blanch when they see some of those communities."
year: "2021"
category: Anime
tag:
- alcohol
- hobbies
- interests
- manga
- sociology
- sport
location: Sydney
---
Good afternoon! It's a sunny if chilly Sydney day. I'm on leave, sitting out on our balcony and thinking through things. It's been a tough week, but I feel like I've started turning the corner. It's good.

I went briefly on Twitter this morning to answer a DM someone said was urgent, but not before I saw [Katelyn Bowden](https://twitter.com/medus4_cdc) ask:

> What is something you would like, if not for the fans? What or who has the fans that are so awful, they ruin the whole thing?

This is a *such a good question!* I'll bet you could think of a few too.

I avoided sharing most of my hobbies or interests throughout my teens and twenties. I maintained separate blogs under pseudonyms, but they never leaked onto my primary blog here (I remember friends having LiveJournals for this reason, too). Self-censorship wasn't just to avoid judgement and ridicule, but to stave off who I dub the *well actually* crowd who revel in questioning people's aptitude and worthiness.

Something snapped and I stopped giving a shit by my late twenties. I started talking about smartdolls, anime figures, trains, old computers, obscure sports, and even the BSD operating systems again here. I still get trolls, and I'd be lying if I said they still didn't get to me sometimes, but it's been liberating talking honestly about myself here. I wrote more about [hobby judgement](https://rubenerd.com/hobby-judgement-as-an-adult/) back in February.

<p><img src="https://rubenerd.com/files/2021/sailor-moon-dgaf@1x.jpg" srcset="https://rubenerd.com/files/2021/sailor-moon-dgaf@1x.jpg 1x, https://rubenerd.com/files/2021/sailor-moon-dgaf@2x.jpg 2x" alt="Art of Usagi from Sailor Moon in her transformed state with a bemused expression." style="width:500px; height:165px;" /></p>

*(This official art of Usagi from a Sailor Moon R artbook exudes "don't give a fuck" vibes. If only I could get a hold of the original to scan at high resolution and frame going down our hallway)!*

But back to Katelyn's question. I'm at the point now where *generally* don't care about how toxic or not a community is before engaging in an interest or hobby, but I can empathise and relate with people who can't make that leap. These are some examples off the top of my head:

* Anime and manga. Where do you even start? For all the variety in stories, genres, art styles, and audiences, there are some... special fans who'll judge your tastes, make awful comments, and hold toxic views. My favourite will always be the guy I caught trying to get photos up Clara's skirt when she was cosplaying Charlotte from Infinite Stratos at a con. The good news was, a raised eyebrow from me was enough to have him scurrying away back into the darkness.

* Alcohol. My dad is German, so beer was a part of growing up. Going to university and seeing how people acted with it put me off it for years. It's only recently that I've started enjoying it again (there's also being socially ostracised for not enjoying the sensation of being drunk. Again, don't care)!

* Gamers. See *anime* above. But there are tons of great people too; coming back into computer games like Minecraft, Superliminal, Train Simulator, and Firewatch has been so much fun.

* Japan. I had Japanese friends in Singapore who were wary of Caucasian fans of their country, saying they only had a stereotypical, superficial, and cringy understanding. I can see that.

* Open-source software. I didn't issue pull requests or want to contribute for years for fear of judgement, and after seeing how some of those mailing list threads go. I'm still a bit like that.

* Sport, especially ones involving ball kicking, perhaps in more ways than one. I had no interest to start with, but I've also seen how some of those fans act.

* The US. There's this unflattering global caricature of Americans, and there are Europeans and those in the Commonwealth who'll chalk you up as a sycophant for showing an interest in their country. Doesn't stop me having many dear friends over there, an appreciation for the good things they've introduced into the world (Robin Williams, the IC, NY-style pizza), and an urge to return to travel at the earliest opportunity. Next up, Portland OR and Boston!

I suppose there will always be those who see someone enjoying themselves, and want to take them down a peg, or gatekeep them away on account of not being a "real" fan. I suppose it balances out when seen through a zero-sum game worldview. It's not though; we can lift everyone's sprits. 🍻

