---
title: "Truth in advertising hits Twitter"
date: "2021-02-28T09:32:23+11:00"
abstract: "Has anyone else noticed this unintentional messaging on their login page?"
thumb: "https://rubenerd.com/files/2021/twitter-torn@1x.png"
year: "2021"
category: Internet
tag:
- marketing
- twitter
location: Sydney
---
I rarely go to the landing page for Twitter, but if you're not logged in you get this background image. It's quite literally tattered remains of text; like a band poster that's been torn off the wall of a city street for being illegally placed.

<p><img src="https://rubenerd.com/files/2021/twitter-torn@1x.png" srcset="https://rubenerd.com/files/2021/twitter-torn@1x.png 1x, https://rubenerd.com/files/2021/twitter-torn@2x.png 2x" alt="Picture from Twitter's login page showing inverted text torn up among splatterings of colour." style="width:500px" /></p>

The effect is interesting, but did anyone on Twitter's marketing team stop to think about what it represents? Is this what they want people to equate tweets to?

I might start my own line of step ladders, with a motif of an upstanding gentleman no longer being able to.

By the way, are we following each other yet on [Mastodon](https://bsd.network/@Rubenerd)?
