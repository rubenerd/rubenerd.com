---
title: "Mental health at the Olympics"
date: "2021-07-29T08:33:37+10:00"
abstract: "My hope is we're finally starting to turn a corner on identifying and being honest about mental health."
year: "2021"
category: Thoughts
tag:
- gymnastics
- health
- mental-health
- psychology
- sport
location: Sydney
---
[Sean Eagle filed this](https://www.theguardian.com/sport/2021/jul/27/simone-biles-withdraws-tokyo-2020-olympics-gymnastics-all-around-final) in the Guardian yesterday:

> The American gymnast Simone Biles, the biggest star at the Tokyo Olympics and the greatest athlete in the sport’s history, last night walked away from the women’s team competition after admitting she had “freaked out in a high stress situation”.
>
> [..] Biles said that the influence of Japanese tennis star Naomi Osaka, who pulled out of the French Open citing mental health concerns, had really helped her speak freely. Osaka, who was seen as the Japanese face of the Games and lit the Olympic cauldron, was knocked out in the third round of the women’s singles at the Olympics on Tuesday and admitted that pressure was a factor.

Simone Biles is amazing. If you've haven't seen one of her routines, she imbues them with such individualism and personality in addition to being technically excellent. I could only hope to have both qualities here, and I'm not exactly performing in front of billions of people.

*(As an aside, check out [Katelyn Ohashi's 2019](https://rubenerd.com/katelyn-ohashis-10-point-routine/) routine for another excellent example. Gymnastics is awesome because the best examples demonstrate skill *and* artistic flair. A Russian gentleman I knew in Singapore was able to do this too, and the mix of respect and jealously I felt was palpable)!*

I hope this doesn't sound crass or opportunistic, but these reports actually give me a glimmer of optimism. I'm *glad* these are being reported for what they are, and that people like Simone can be honest about her thoughts and position. I get the distinct impression that other factors would have been blamed even a decade or so ago by an Olympic team or country.

My hope is we're finally starting to turn a corner on identifying and being honest about mental health, and that being open about it will encourage others to come forward. Then, eventually, we'll be able to drop the word "mental" from the phrase, because it's a health issue regardless of the body appendage in which it manifests.
