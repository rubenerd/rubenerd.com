---
title: "Chancellor of Earth: Advertising"
date: "2021-11-02T20:12:47+10:00"
abstract: "An act against meaningless slogans with broken grammar!"
year: "2021"
category: Thoughts
tag:
- advertising
- chancellor-of-earth
location: Sydney
---
The [Chancellor of Earth](https://rubenerd.com/tag/chancellor-of-earth/) Ruben Schade made his way up the flower-lined stairs to the stage overlooking the Planetary Parliament chamber. The crowd of politicians, journalists, and commoners in the viewing gallery hushed their conversations and took their seats.

He adjusted his multicoloured tie and glasses, smiled, and looked into the transparent teleprompter.

> Good morning, afternoon, or evening, depending on your locale. Pause for applause.

He shook his head. He wasn't supposed to read that bit.

> This month’s legislative agenda will put to rest once and for all two issues concerning the advertising of goods and services. These will come into effect within one Earth week, permitting affected parties time to pull down all relevant billboards, privacy-invading website banners, business cards, video advertisements, novelty pens, and so on.

He didn't read the *pause* this time, but forgot to implement it nonetheless.

> The first concerns grammatically-incorrect, vague, or meaningless slogans. If you'd flunk a language test reading the slogan, you will be fined the net worth of your company, and all your novelty pens, for publishing it.

A hand shot up from the front row of seated executives, though he was quick to retrieve it with his other one.

> I'm not sure when these press conferences became a Q&A, but yes, you there with enough hair gel to launch a ship down a slipway.

> Thank you Chancellor, I... hey. What's the harm if my phone company wants to use the phrase *Today Now* or *Possibilities Are Yes*. It speaks to the dynamism, synergistic disruption, and unlimited potential for the...

The executive disintegrated into a puff of pink smoke, his bowtie and blazer dropping to the chair upon which he once sat.

> Seems like someone could have *stood* to be more *grounded*. Huh? **HUH!?**

The sound of crickets punctured the silence in the chamber, a bizarre occurance given the time of day and the fact it was indoors.

> Well then. The second of these concerns an immediate termination of the use of euphemistic or vague words to describe products we all know. For example, it is not *bathroom tissue*, it is *toilet paper*. We all use it, it is just as essential, there is no shame calling it what it is.

Another hand shot up, this time from someone wearing one of the aforementioned rolls on his head.

> No, I think that's all we have time for today. Thank you.

The Chancellor stepped down from the podium to the sound of applause.
