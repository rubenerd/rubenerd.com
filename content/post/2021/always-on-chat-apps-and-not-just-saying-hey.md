---
title: "Always-on chat apps, and not just saying “hey”"
date: "2021-09-28T08:45:27+10:00"
abstract: "Aka, don’t do it!"
year: "2021"
category: Thoughts
tag:
- psychology
- work
location: Sydney
---
[Jennifer Liu laid bare](https://www.cnbc.com/2020/06/17/communication-and-etiquette-tips-for-the-virtual-workplace.html) the real issue behind modern chat apps, and it's not just their [offloading](https://rubenerd.com/1password-electron-flareup-is-good-news/ "The 1Password Electron debate is good news for the industry") of technical debt:

> Communicating via Slack and other instant messaging platforms can be a blessing and a curse. For one, you and your teammates can share information more quickly. On the other hand, it can also encourage an “always on” culture where you’re expected to be available at all times.

My dad said the same thing about mobile phones in the 1990s. I still remember when I got my first one in late primary school, and he said "you'll be happy one day when you can turn it off". I didn't know at the time what he meant. It's amazing what a decade or two will do.

She also raised the issue of open-ended conversations, and those terse ones we're all used to:

> That means including a casual salutation followed by a specific request or message. Sending a lone “hey” without any context could send the recipient’s anxiety soaring, and it also doesn’t allow them to gauge what kind of conversation they’re entering when they respond.
> 
> Similarly, to close the communication loop and not leave anyone hanging, a quick “thanks” or “I’ll get back to you by X” can move the conversation along and give it a natural close.

It reminds me of [this Blogger site](https://www.nohello.com/) from back in the day, and [this other site](https://nohello.net/) with some examples.

I did report briefly to someone who made it a habit of messaging me at all hours of the night with "hey" and no further context. Given I was in an IT engineering job, my mind immediately catastrophised. Almost always, it was innocuous.

Working remote has forced all of us to learn these new frameworks on our feet, and in a compressed timeframe. Let's at least try to not entrench bad ideas or attitudes.

