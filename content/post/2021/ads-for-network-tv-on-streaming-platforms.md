---
title: "Full circle: TV ads on streaming platforms"
date: "2021-06-25T13:46:38+10:00"
abstract: "They remind us of why we moved off TV in the first place."
year: "2021"
category: Media
tag:
- advertising
- movies
- tv
location: Sydney
---
The future doesn't look great for free-to-air television stations, or TV as it's referred to by people. Thesedays cool hipster 10x engineers would probably refer to it as *t8n*.

I remember reading an article a few years ago saying the median age of American TV viewers is increasing by one year annually; meaning there aren't any new people tuning in. I'm not sure if we can extrapolate that further to countries like Australia and Singapore, but it wouldn't surprise me. It takes a lot not to surprise me. Wait, that doesn't make sense.

People are watching as many (if not more) shows and independent programming as before, but they're on streaming services, social networks, or independent publishing platforms. It was BitTorrent and Gnutella prior to this, but we all pretend those didn't happen. Uncovered old copies of TV Week stuffed into wall cavities as cheap insulation will as likely be met with a *huh?* than any form of nostalgia.

This has had several interesting side effects. News channels have begun publishing video clips on sites like YouTube, reaching new audiences with their unsubstantiated bollocks. TV stations have had to work hard to pander to their remaining viewership, lest their great-grandchildren tell them they can get far better stuff elsewhere. Is it showing through yet that I don't think free-to-air t8n is any good?

*(Was it **ever** good? Or was it just better than crushing boredom in a time before the Internet where we could surf mindlessly instead)?*

The other that Clara and I noticed was a sharp increase in ads on sites like YouTube... for network TV. One of these stations in Australia have successfully transplanted their show promos from their moribund channels to the web, with the same faux gritty voice and intense music.

> This is something. You. CAN'T. MISS.

It had the opposite effect, by reminding us of everything we *don't* like about TV. I suppose I should be happy that they're keeping the disgust alive and fresh, in case I'm ever tempted to plug in an aerial or subscribe to cable again.

