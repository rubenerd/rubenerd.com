---
title: "Hololive’s bossa nova jazz mix"
date: "2021-08-09T10:04:22+10:00"
abstract: "Just when we thought they couldn’t get more fabulous."
thumb: "https://rubenerd.com/files/2021/yt-VPBqpyub4Kc@1x.jpg"
year: "2021"
category: Media
tag:
- bossa-nova
- jazz
- hololive
- music
- music-monday
location: Sydney
---
Today's [Music Monday](https://rubenerd.com/tag/music-monday/) reminds us that just when we think Hololive couldn't get more spectacular and fabulous, they do something like this.

<p><a href="https://www.youtube.com/watch?v=VPBqpyub4Kc" title="Play 【作業用BGM】hololive music studio - bossa nova & jazz mix【 #hololivemusicstudio 】"><img src="https://rubenerd.com/files/2021/yt-VPBqpyub4Kc@1x.jpg" srcset="https://rubenerd.com/files/2021/yt-VPBqpyub4Kc@1x.jpg 1x, https://rubenerd.com/files/2021/yt-VPBqpyub4Kc@2x.jpg 2x" alt="Play 【作業用BGM】hololive music studio - bossa nova & jazz mix【 #hololivemusicstudio 】" style="width:500px;height:281px;" /></a></p>

