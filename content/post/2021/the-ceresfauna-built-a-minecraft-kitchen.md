---
title: "The @ceresfauna built a Minecraft kitchen"
date: "2021-09-18T09:48:34+10:00"
abstract: "One of the nicest ways we’ve ever ended the week."
thumb: "One of the nicest ways "
year: "2021"
category: Media
tag:
- hololive
- ceres-fauna
location: Sydney
---
Clara and I were a headachey, mentally exhausted mess by Friday night, and [Fauna's stream](https://www.youtube.com/watch?v=c31slp86BVA) was one of the nicest ways we've ever ended the week. [Mumei](https://www.youtube.com/channel/UC3n5uGu18FoCy23ggWWp8tA) even joined in to say hello.

<p><a title="MINECRAFT building my kirin house 🌿 #holoCouncil" href="https://youtu.be/c31slp86BVA?t=8378"><img src="https://rubenerd.com/files/2021/yt-c31slp86BVA@1x.jpg" srcset="https://rubenerd.com/files/2021/yt-c31slp86BVA@1x.jpg 1x, https://rubenerd.com/files/2021/yt-c31slp86BVA@2x.jpg 2x" alt="MINECRAFT building my kirin house 🌿 #holoCouncil" style="width:500px; height:281px;" /></a></p>

