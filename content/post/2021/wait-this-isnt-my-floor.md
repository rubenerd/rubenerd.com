---
title: "Stepping into an alternative dimensional floor"
date: "2021-06-07T20:34:29+10:00"
abstract: "I made it all the way to the apartment before I realised."
year: "2021"
category: Thoughts
tag:
- adventures
- pointless
location: Sydney
---
This evening started out like any other, with the setting of the sun and a natural decrease in atmospheric temperature that accompanies that subsequent lack of sunlight. I had made my way home on the train, stepped aboard the lift in our building, and busied myself typing out a quick email to a client.

The lift arrived, the doors whirred open, and I stepped out into the lobby for my floor. I was so engrossed in the technicalities of this message that I let my muscle memory and peripheral vision guide me around the corner and down the warmly-lit corridor to our apartment. I may or may not do this regularly, when I should be paying attention to my surroundings. But that's a mindfulness discussion for another time.

Then something started nagging at me. My surroundings didn't feel quite right. It began with the colour temperature of the lighting being slightly off, and even some cooking smells that were unfamiliar. The carpet looked a little more worn. It was unsettling, though I still hadn't figured out why.

It felt like I'd stepped into an alternative dimension, where everything was *almost* exactly the same as I expected, from the floor layout and positions of the apartment doors, to the wall light sconces and the emergency exit signs. It felt *weird*.

As you've no doubt grokked from the heading of this post, I'd made my way onto the wrong floor... and it was only until I stood at the door of what should have been Clara's and my apartment with the wrong colour and number that I realised. Suddenly what I'd taken for granted moments before now felt foreign and unusual, and my presence felt unwelcome and hostile.

I was puzzled all the way back up to my floor. You need a swipe card to unlock your floor in the lift, and I was the only person in it before to choose a destination. I couldn't have chosen the other floor accidentally; it would have denied me access.

Someone could have pressed the button on that lower floor to go up, then walked away without catching it when it arrived. But how often does that happen? Almost everyone only goes *down* from their floor to the lobby, and from the lobby back to their floor. To go up in the building *from* your floor, you'd need a swipe card not only for where you are, but the destination floor as well. The chances of that seem improbable under normal circumstances. Maybe the person knew someone and was borrowing their card, or were moving within the same tower.

Maybe someone caught the lift earlier and chosen the lower floor, but then got out early themselves, leaving me to leave on that floor while I wasn't paying attention. But I was *fairly* sure the button panel was otherwise empty when my card activated my floor and I pressed the button.

Who knows, perhaps the security system lapsed for a moment and granted me access to a floor for which I had no authorisation. The button panel itself could have glitched and registered the wrong button, or the lift simply took me to the wrong floor. I've been in lifts where the doors inadvertently open halfway below the floor they're supposed to be arriving at, and one in Malaysia that would register the floor *next* to the button I chose, but not my target floor. Microcontrollers aren't perfect, and are working in the physical world where operating conditions can't always be guaranteed.

The only other explanation is a RIP in the space-time continuum, or maybe an OSPF. I've been routed in the wrong direction before.

