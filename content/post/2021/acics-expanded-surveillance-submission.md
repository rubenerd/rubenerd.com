---
title: "ACIC’s expanded surveillance submission"
date: "2021-05-08T10:33:25+10:00"
abstract: "Will be used “exclusively” for bad actors, you say?"
year: "2021"
category: Internet
tag:
- australia
- privacy
- security
location: Sydney
---
The Australian Criminal Intelligence Commission has [submitted a proposed amendment](https://www.aph.gov.au/DocumentStore.ashx?id=0cfd0e34-ae76-42e4-9438-d8218c70b760&subId=706935) for review on online criminal threats. They base the expanded powers on this premise:

> The encryption and anonymisation that underpins the Dark Web and encrypted communications has challenged existing powers and allowed serious and organised crime (SOC) groups and individuals to more effectively conceal their criminal activity. In particular, the networks established on the Dark Web and via encrypted communications have provided criminals with platforms to easily and more confidently communicate anonymously about, and obfuscate, their serious criminal activities.

There's significant conflation in the submission between the Dark Web and Encrypted Communications, as if to suggest the latter implies the former. For law enforcement, encrypted communications may constitute the Dark Web given their inability to decode data, but they're two *very* different things. It's dishonest.

The whole report is worth reading, especially the expanded sections on the new types of warrants proposed. But this paragraph on page two summarises my concerns; emphasis and numbers added:

> Encryption and anonymising technologies have a valuable role in protecting the privacy and data of Australians. As such, the ACIC notes new powers cannot be **(1) exclusively** focused on subverting encryption and anonymising technologies. Instead new powers must provide the ability for agencies, like the ACIC and AFP, to **(2) access material at unencrypted points**, or to utilise their capabilities to **(3) see through the obfuscation** that nefarious use of these technologies provide.

"Exclusively" so perfectly encapsulates my concerns for this entire submission. I'm not sure how encrypted communications could be practically subverted without knowledge and access to decrypt all traffic, and ours fears of scope creep and misuse have been proven so many times I'd be *very* surprised if this didn't become routine, if what they're proposing became possible. If "exclusively" was included to assuage concerns that such powers would be used responsibly, with restraint, and in a manner that respects the law, it absolutely didn't. As an American colleague used to say, "y'all haven't earned that benefit of the doubt".

Despite former Australian Prime Minister Malcolm Turnbull's embarrassing assertion that the laws of Australia trump those of mathematics, points 2 and 3 are not possible with encrypted communications without a man-in-the-middle, or subversive tools like deep packet inspection. How these would be implemented at all, let alone at scale, and what legal framework would need to be in place to compel it, are all monstrous question marks that the report does nothing to address.

I *do* empathise with law enforcement's needs to track illegal activity; something of an unpopular opinion from what I can see. But speaking as an IT professional, this submission is a lot of worrying hand-waving, assertions without evidence, and vague euphemisms. These claims demand precision, accuracy, and honesty to be taken seriously.

