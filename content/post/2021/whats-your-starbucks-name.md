---
title: "What’s your Starbucks name?"
date: "2021-08-31T16:58:04+10:00"
abstract: "Robbin, Ribbon, Rupert."
year: "2021"
category: Thoughts
tag:
- coffee
- news
- nostalgia
- singapore
- starbucks
location: Sydney
---
[@dorothyho](https://twitter.com/dorothyho/status/1432581453506875393) on *The Bird Site* reminded me of [What's My Starbucks Name](https://www.whatsmystarbucksname.com/), a fabulous site that invites you to tell a barista your name and see what gets written on your cup.

I've written many times here my frustration at how people treat retail staff, especially those in high-traffic stores like coffee shops. It's inevitable that underappreciated and overworked people will make mistakes with names from time to time. It's especially true in melting pots like Singapore where it's normal to be speaking in a *lingua franca* that's not either of the speakers' first languages, so you're interpreting accents as well as muffled voices in a busy and loud setting.

I bring it up though because of that meme that circulated in 2016 that Starbucks baristas spell your name wrong *on purpose*. The source video (which I won't link to) made some dubious claims that spread across all the typical mainstream media outlets without much fact checking. Maybe because the story was so believable.

Speaking from my own experience, "Ruben" isn't a rare name, but it's sufficiently unusual that I've had everything from "Ribbon", "Rupert", "Robert", and "Renee". I abbreviated it to Ben, which became Bin and Pen. Curiously, Kopitiams, and indie coffee houses almost never spelled it wrong... or did away with names entirely and used numbers. The data scientist in me appreciates the resulting reduction in collisions.

