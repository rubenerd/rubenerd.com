---
title: "A 740-page Tennant Schrobmachine"
date: "2021-04-14T10:05:12+10:00"
abstract: "7,400 posts celebrated with another industrial cleaning device."
thumb: "https://rubenerd.com/files/2021/tennant-7400-cleaner@1x.jpg"
year: "2021"
category: Thoughts
tag:
- pointless-milestone
location: Sydney
---
My silly blog here has reached seven hundred and forty pages, which sounds much longer when you write the numbers out in English in lieu of digits. At ten posts per page, that means we have... wait, let me check.

    $ bc 740 * 10
    ==> File 740*10 is unavailable

I *always* make that mistake!

I used to mark such milestones with a Tennant floor cleaning device with a similar model number or feature, like [this silly post from 2009](https://rubenerd.com/post-id-5700/ "The Tennant 5700 industrial scrubber"). Here's the Tennant 7400, which has the same model number as this blog has posts:

<p><img src="https://rubenerd.com/files/2021/tennant-7400-cleaner@1x.jpg" srcset="https://rubenerd.com/files/2021/tennant-7400-cleaner@1x.jpg 1x, https://rubenerd.com/files/2021/tennant-7400-cleaner@2x.jpg 2x" alt="Photo of a small, ride-on floor sweeping machine." style="width:380px" /></p>

This [Dutch Industrial cleaning site](https://industrialcleaning.nl/product/schrobmachine-tennant-7400/) mentions:

> Tennant 7400 Schrobmachine Verhuur machine: De Tennant 7400 opzit-schrobzuigmachine is een eenvoudig te bedienen machine dit mede dankzij de gemakkelijke besturing is deze machine zeer geschikt voor magazijn, parkeergarages en bouwopleveringen.

I tried running this through babelfish.altavista.com, but the site no longer exists. 
