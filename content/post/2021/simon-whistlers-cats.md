---
title: "Simon Whistler’s cat"
date: "2021-09-20T15:49:18+10:00"
abstract: "Big brain."
thumb: "https://rubenerd.com/files/2021/whisler-cat-02@1x.jpg"
year: "2021"
category: Media
tag:
- business-blaze
- simon-whistler
location: Sydney
---
From one of his [recent design critique videos](https://www.youtube.com/watch?v=AMJu3qVB0Yg)\:

<p><img src="https://rubenerd.com/files/2021/whisler-cat-01@1x.jpg" srcset="https://rubenerd.com/files/2021/whisler-cat-01@1x.jpg 1x, https://rubenerd.com/files/2021/whisler-cat-01@2x.jpg 2x" alt="I had two cats. One was Sweep, one was called Tabby. She was called Tabby, because she was a tabby cat." style="width:500px; height:281px;" /><br /><img src="https://rubenerd.com/files/2021/whisler-cat-02@1x.jpg" srcset="https://rubenerd.com/files/2021/whisler-cat-02@1x.jpg 1x, https://rubenerd.com/files/2021/whisler-cat-02@2x.jpg 2x" alt="*points at head*" style="width:500px; height:281px;" /><br /><img src="https://rubenerd.com/files/2021/whisler-cat-03@1x.jpg" srcset="https://rubenerd.com/files/2021/whisler-cat-03@1x.jpg 1x, https://rubenerd.com/files/2021/whisler-cat-03@2x.jpg 2x" alt="Big Brain." style="width:500px; height:281px;" /></p>

