---
title: "Sitting in that comfy chair"
date: "2021-12-04T19:39:50+10:00"
abstract: "Hi everyone!"
year: "2021"
category: Thoughts
tag:
- health
- personal
location: Sydney
---
Hi everyone! I posted on Mastodon and The Bird Site that I'd been discharged from hospital. The surgery in that *tender area* (hah!) was more complicated than they expected, which means the recovery will be much longer. But it wasn't a tumour, and I have my beautiful (in every sense of the world) girlfriend Clara nursing me back to health, so I'm counting my lucky stars.

<p><img src="https://rubenerd.com/files/2021/hospital-coffee-yum@1x.jpg" srcset="https://rubenerd.com/files/2021/hospital-coffee-yum@1x.jpg 1x, https://rubenerd.com/files/2021/hospital-coffee-yum@2x.jpg 2x" alt="The best tasting coffee ever!" style="width:500px; height:333px;" /></p>

I've been unable to move from this chair for two days now, and *boy* does it make things seem distant. At least during Covid lockdowns I could walk to the balcony and get fresh air. Instead I've been staring at a book, or a computer screen, or the wall. The sum of all the world's knowledge is a few clicks away, and I've never been so bored in my life. It's ridiculous how the mind works.

I'm slowly getting back into the mental swing of things, even if the most adventurous part of my day is figuring out how to stand to perform certain duties without bucking over and screaming. The neighbour in the apartment next door banged on the wall once; maybe they thought I had a horror movie up too loud. They may have been right.

How have you all been? I know for the most part this is a one-way communication platform, but in some strange way I do feel like this is more of a time-delayed conversation. Writing this meandering nonsense has already lifted my spirits immeasurably :).
