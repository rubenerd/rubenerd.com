---
title: "Omake refresh, and the state of blogrolls"
date: "2021-09-10T16:52:13+10:00"
abstract: "Most blogs on the list are gone or stale. Have a personal blog I should add?"
year: "2021"
category: Internet
tag:
- blogging
- nostalgia
location: Sydney
---
Mmm, *rolls*. Focus, Ruben.

A few eagle-eyed readers have noticed that my [omake page](https://rubenerd.com/omake/) has changed. I've merged all my disparate lists into one collapsible outline that you can explore by clicking through each section. I'm not at all sure I'll keep the structure the same here, but it's a start. Outlines are a fun way to collect and organise information, especially links and short snippets of text. *OPML and RDF pending!*

Which leads us to blogrolls. These used to be a fixture of the web, back when everyone had a blog. You would share links to your friends and the sites you read, sometimes in your blog sidebar, or on a separate page. I loved clicking through to all these different places in the mid 2000s, and seeing where I ended up.

As Josh from The Geekorium noticed when I posted about it on Mastodon, [my fifteen-year old blogroll](https://rubenerd.com/omake/#subs) was in a bit of a sorry state. Over half the sites didn't resolve, and those that did hadn't been updated in years. It perhaps shouldn't have been surprising, but it took me back a bit. I couldn't decide whether to keep them or not, so I decided to split the difference and keep the ones that still exist, in the hopes that maybe one day they'll come back. I did the same thing in my blog reader.

But now I'm in the market for other personal blogs to read! [Let me know](https://rubenerd.com/about/#contact) if you'd like to swap links :).

