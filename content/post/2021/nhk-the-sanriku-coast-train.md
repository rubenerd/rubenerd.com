---
title: "NHK: The Sanriku coast train"
date: "2021-03-03T22:28:11+11:00"
abstract: ""
thumb: "https://rubenerd.com/files/2021/yt-@1x.jpg"
year: "2021"
category: Travel
tag:
- documentaries
- japan
- nhk
- trains
- video
- youtube
location: Sydney
---
This video of the [Sanriku Tetsudou](http://www.sanrikutetsudou.com/) made Clara's and my evening. What a beautifully produced documentary for such a jaw dropping part of the world. JJ was also a respectful and fun host :).

<p><a href="https://www.youtube.com/watch?v=6XDyX76OgLk" title="Play The Sanriku Coast's Unbeatable Spirit - Train Cruise"><img src="https://rubenerd.com/files/2021/yt-@1x.jpg" srcset="https://rubenerd.com/files/2021/yt-@1x.jpg 1x, https://rubenerd.com/files/2021/yt-@2x.jpg 2x" alt="Play The Sanriku Coast's Unbeatable Spirit - Train Cruise" style="width:500px;height:281px;" /></a></p>

**Update:** Of course, being a Japanese railway, they have a store. It's not that I want [this keyring](https://sanrikutetsudou.shop-pro.jp/?pid=155101549), as much as I *need* it!
