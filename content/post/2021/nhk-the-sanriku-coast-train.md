---
title: "NHK: The Sanriku coast train"
date: "2021-03-03T22:28:11+11:00"
abstract: "A beautifully produced documentary for such a jaw-dropping part of the world."
thumb: "https://rubenerd.com/files/2021/yt-@1x.jpg"
year: "2021"
category: Travel
tag:
- documentaries
- japan
- nhk
- trains
- video
- youtube
location: Sydney
---
This video of the [Sanriku Tetsudou](http://www.sanrikutetsudou.com/) made Clara's and my evening. What a beautifully produced documentary for such a jaw dropping part of the world. JJ was also a respectful and fun host :).

**Update:** Of course, being a Japanese railway, they have a store. It's not that I want [this keyring](https://sanrikutetsudou.shop-pro.jp/?pid=155101549), as much as I *need* it!

**Update 2:** [The video](https://www.youtube.com/watch?v=6XDyX76OgLk) has been taken down, nuts.
