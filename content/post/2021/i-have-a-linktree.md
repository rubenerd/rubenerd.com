---
title: "I have a Linktree"
date: "2021-04-27T14:57:43+10:00"
abstract: "linktr.ee/rubenerd"
year: "2021"
category: Internet
tag:
- links
- social-media
location: Sydney
---
I don't know how I found this link site. Maybe it's because I'm a sucker for conifers 🌲. Many people have a Linktree profile, but this one is mine:

> **[https://linktr.ee/rubenerd](https://linktr.ee/rubenerd)**

I'm missing a ton of stuff, but you get a pretty good idea of my interests from it. It's strange that some services like Apple Podcast feeds appear as single icons at the bottom, instead of a coloured bar with text.

This was entirely pointless, considering my [About](https://rubenerd.com/about/) page and [Omake](https://rubenerd.com/omake/) section has all the links to all my stuff. But sometimes we don't need to justify everything we do.

