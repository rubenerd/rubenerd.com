---
title: "Decluttering Fandom.com with uBlock Origin"
date: "2021-08-15T09:40:30+10:00"
abstract: "Their new site design is the modern web writ large, but it’s easy enough to hide all the new junk."
year: "2021"
category: Internet
tag:
- design
- guides
location: Sydney
---
Speaking of [decluttering](https://rubenerd.com/covid-lockdown-decluttering/)! I frequent Fandom sites all the time for [Vim](https://vim.fandom.com/wiki/Vim_Tips_Wiki), [Minecraft](https://minecraft.fandom.com/wiki/Minecraft_Wiki), [Fate/Grand Order](https://fategrandorder.fandom.com/wiki/Fate/Grand_Order_Wikia), [Star Trek](https://memory-alpha.fandom.com/wiki/Portal:Main), and more. It uses the same Wiki syntax and engine as sites like Wikipedia, which makes editing predictable and easy.

Unfortunately, the network's new web design includes tons of unrelated material from other wikis, often times using more bandwidth and space than the actual page content. It also has persistent ("sticky") navbars which are a pet peeve not only for their distracting in/out slide animations, but for all the space they waste for their duplicated material. It's not as egregous as much of the modern web, but it seems like they're aiming for it.

The good news is that it's all easy to block if you match the appropriate classes. Click the [uBlock Origin](https://ublockorigin.com/) icon in the toolbar and choose the gear icon. Under the **My Filters** tab, add the following:

	! https://*.fandom.com
	fandom.com###WikiaBar
	fandom.com##.mcf-wrapper
	fandom.com##.wds-global-footer__main
	fandom.com##.message
	fandom.com##.global-navigation__top
	fandom.com##.is-visible.fandom-sticky-header
	fandom.com##.wds-global-footer
	fandom.com##.page-side-tools__wrapper

Note the triple pound next to WikiaBar; that is a HTML ID not a class. 

These work as of August 2021 across any Fandom wiki. If not, it should be easy to right-click any element on the page and choose **Block Element**.

uBlock Origin is fantastic software. I'd consider it nothing short of essential to use the modern web without losing your marbles. Thanks Raymond Hill and everyone who helps maintain it.

