---
title: "Music Monday: Violet, Ninomae Ina’nis"
date: "2021-09-13T10:32:07+10:00"
abstract: "Aaaaah! ♡"
thumb: "https://rubenerd.com/files/2021/violet@1x.jpg"
year: "2021"
category: Media
tag:
- hololive
- music
- music-monday
- ninomae-inanis
location: Sydney
---
Given the [one-year anniversary](https://rubenerd.com/one-year-of-hololive-en/ "One year of Hololive-EN!"), I'd be remiss if I didn't share Ina’s first original song. It's so beautiful! 💜 I'm impressed with her vocals, and the light electronic instrumentation is *right* up my alley.

<p><a href="https://www.youtube.com/watch?v=8ZdLXELdF9Q" title="Play 『VIOLET』 - Ninomae Ina'nis"><img src="https://rubenerd.com/files/2021/violet@1x.jpg" srcset="https://rubenerd.com/files/2021/violet@1x.jpg 1x, https://rubenerd.com/files/2021/violet@2x.jpg 2x" alt="Play 『VIOLET』 - Ninomae Ina'nis" style="width:500px;height:281px;" /></a></p>

