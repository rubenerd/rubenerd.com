---
title: "Rebrand Philadelphia’s transit system to The Metro"
date: "2021-09-10T08:10:04+10:00"
abstract: "Sounds like a great idea!"
year: "2021"
category: Travel
tag:
- accessibility
- maps
- philadelphia
- trains
location: Sydney
---
[The Philadelphia Inquirer reported](https://www.inquirer.com/transportation/septa-signs-directions-rail-transit-el-metro-20210907.html) on the Pennsylvanian state transit authority's plan to rebrand Philly's transit system *The Metro*:

> [..] in the transit agency’s wayfinding master plan, released Tuesday, to make rail transit easier to use in the Philadelphia region. The idea: Unify under one brand a system often thought of line by line, route by route because it’s been labeled that way for a century.

The idea is to use a standardised system of colours and icons:

> The agency plans a redo of the system’s maps and signs with the aim of making wayfinding images easier to see and understand quickly. Lines will be denoted by capital letters and color badges instead of pictographs of rail vehicles over colored backgrounds. Planners propose keeping the hues historically associated with them, such as orange for the Broad Street Line, blue for the Market-Frankford Line (the El), and green for trolley routes.

Philadelphia is a beautiful city, and its transit system was one of the reasons Clara and I enjoyed wandering around there. I'll admit I liked the realistic pictograms of each of the lines, but standardised signage with unique letters and numbers would make more sense.

Most systems in Asia rely on colour codes and names, but I like the idea of also using simple, block letters for those with colour blindness or other vision impairments. Singapore does number the lines as well, but they're used to denote the direction of travel which I've personally found a bit confusing.

SEPTA also introduced their stored-value [Key system](https://www.septakey.org/) in 2016 which makes life easier (you'll need a US VPN to access). We both still have leftover tokens we used when Jim was showing us around :).

