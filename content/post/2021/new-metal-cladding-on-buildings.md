---
title: "New metal cladding on buildings"
date: "2021-10-27T08:34:04+10:00"
abstract: "Watching their BANG year long BANG progress replacing BANG panels, presumably BANG for fire safety?"
thumb: ""
year: "2021"
category: Thoughts
tag:
- architecture
- design
- safety
- sydney
location: Sydney
---
Across the street from our Sydney apartment complex is *The Concourse*, a multi-function public building with concert halls, Willoughby council's largest library, a reflection pool, restaurants, and a green space. The sides of the building consist of walls to prevent weather getting inside, and a form of roof to achieve similar weather-proofing. These are punctured with a series of doors, windows, and skylights to permit ingress of light and human beings. I'm told it also has a floor, as much for convention.

Get it? Because it's a convention hall that... shaddup.

The external walls are clad with hundreds of metal panels of various sizes, some of which wrap more than 270 degrees around curved sections that jut out from the main structure. I can appreciate architects trying something a bit different, or at least giving us something that isn't a box.

For the better part of a year, workers have been hammering, sizing, grinding, and replacing each and every one of these panels. Their *agonisingly* slow (and loud!) progress hints at just how complicated the removal and installation of these panels must be.

<p><img src="https://rubenerd.com/files/2021/chatswood-cladding@1x.jpg" srcset="https://rubenerd.com/files/2021/chatswood-cladding@1x.jpg 1x, https://rubenerd.com/files/2021/chatswood-cladding@2x.jpg 2x" alt="Photo of The Concourse showing a few of the open areas where metal cladding was once affixed." style="width:500px; height:333px;" /></p>

I've been wondering why they went to the trouble. My selfish hope was that they were replacing glossy panels with matte ones to minimise reflections into our apartment building. Not being blinded by a narrow beam of focused sunlight when I inadvertently pass by a window would have been most welcome for my headaches and eyes. Alas, the new panels are just as shiny; perhaps even moreso. This could hint at it being cosmetic.

Those plastic sheets might in fact be insulation they're installing for saving energy. It could be the rails used to attach the panels to the walls were rusting or bent, which could have been a safety hazard if a panel were to detach itself. They could have replaced those panels, attached them more securely, and got some energy savings for free. Well, not *free*, but insulation would surely pay for itself quickly.

Or finally, the elephant in the room attempting to perform the Safety Dance might have realised those panels were fire hazards. We've seen a steady stream of news reports of buildings across the world that have used flammable plastics in their cladding because of shortsighted engineering. These ridiculous panels have cost lives. Not having a flaming building spewing plastic smoke at our apartment would be rather lovely, to say nothing for the building's occupants.

Maybe it's a combination of the above. All I know is I can't wait for BANG them to BANG be finished BANG sometime BANG soon!
