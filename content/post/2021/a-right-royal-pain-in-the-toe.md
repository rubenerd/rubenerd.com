---
title: "A royal pain in the toe"
date: "2021-01-13T16:42:50+11:00"
abstract: "Don’t stub your toe! Or any other body part, if you can avoid it."
year: "2021"
category: Thoughts
tag:
- health
- personal
location: Sydney
---
There's no metaphor here, no commentary about the state of the word, no analogies or comparisons. This really is just about my big toe, and the ensuing pain that ensued! As opposed to ensuing pain that didn't ensue? I already read ensue as *ensuite*, which should be spelled as *on suite*. Or *on sweet*.

I slammed my aforementioned left big toe into the side of a table leg during our current house move. I yelped and commented on my lack of spacial and situational awareness, then proceeded to put weight on it to finish this aspect of packing. The little guy swelled up and turned red, then a shade of purple.

*Turns out* I'd impacted the nail into the side of the toe. Gruseome details aside, the resulting infection was painful and unpleasent; as if the former didn't already indicate the latter. It reminded me of my [busted ankle](https://rubenerd.com/i-broke-ground-on-sunday-with-my-ankle/) at a smaller scale; like the skin around the toe was too small and tight for the stuff inside. Wait, I said I was leaving the details aside. 

Some stinging Dettol foot baths and awkward propping up of my leg later, and he's on the mend. Which is good, I'm rather attached to him. He comes with me on walks, and is a source of great stability in my life. Don't we all crave that?

It shoes just how one small, seemingly inconsequential action can have such a dramatic impact on your life. *Shoes* in that sentence was a typo for *shows*, but it's too delightful to fix.

