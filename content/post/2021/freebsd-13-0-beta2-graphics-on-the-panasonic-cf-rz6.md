---
title: "FreeBSD 13.0-BETA2 graphics on the Panasonic CF-RZ6"
date: "2021-02-13T22:23:48+11:00"
abstract: "pkg install drm-fbsd13.0-kmod && sysrc kld_list=i915kms!"
year: "2021"
category: Software
tag:
- bsd
- freebsd
location: Sydney
---
I haven't been as excited for a FreeBSD release for a while. We get OpenZFS 2 in base, and it's more polished out of the box for desktop use as well with better wireless support and prettier terminal graphics. Little touches of polish like that make a huge difference.

I installed it on my [tiny Panasonic CF-RZ6](https://twitter.com/Rubenerd/status/1360536106651947010) I got during AsiaBSDCon 2019&mdash;remember travel?&mdash;and set to grab the latest packages:

    # mkdir -p /usr/local/etc/pkg/repos/
    # cp /etc/pkg/FreeBSD.conf /usr/local/etc/pkg/repos/
    # sed -i '' -e 's/quarterly/latest/' /usr/local/etc/pkg/repos/FreeBSD.conf
    # pkg bootstrap
    # pkg update

Then did a search for the latest graphics for its integrated Intel GPU:

    # pkg search drm-fbsd
    ==> drm-fbsd13.0-kmod-5.4.92.g20210202

And installed, as per the package's instructions:

    # pkg install fbsd-13
    # sysrc kld_list="i915kms"

Rebooted, and `startx(1)` for xorg worked!

