---
title: "Prussian blue"
date: "2021-01-03T22:21:00+11:00"
abstract: "Today I learned where the name came from, and the fact it's also a medicine. That's really cool."
year: "2021"
category: Thoughts
tag:
- chemistry
- colour
- design
- etymology
---
I've decided to spend time this year learning more about the etymology and history of phrases and names I assume or take for granted. I'm most interested in technology, engineering, and science, but I love when we get an overlap between those and art!

<p><img src="https://rubenerd.com/files/2021/great-wave@1x.jpg" srcset="https://rubenerd.com/files/2021/great-wave@1x.jpg 1x, https://rubenerd.com/files/2021/great-wave@2x.jpg 2x" alt="The Great Wave off Kanagawa" style="width:500px" /></p>

I overheard the phrase *Prussian blue* in a documentary and decided to check it out. [Wikipedia](https://en.wikipedia.org/wiki/Prussian_blue) described the dye's discovery:

> Prussian blue was probably synthesised for the first time by the paint maker Diesbach in Berlin around 1706. [..] The pigment is believed to have been accidentally created when Diesbach used potash tainted with blood to create some red cochineal dye. The original dye required potash, ferric sulphate, and dried cochineal. Instead, the blood, potash, and iron sulphate reacted to create a compound known as iron ferrocyanide, which, unlike the desired red pigment, has a very distinct blue hue.

And the name?

> It was named Preußisch blau and Berlinisch Blau in 1709 by its first trader.

*The Great Wave off Kanagawa* is probably the most famous early artwork that made use of Prussian blue, though as the name eludes, it was sourced from Europe. It's been used to the point of being a clich&eacute;, but it's still one of my favourite artworks.

It's also on the World Health Organisation's List of Essential Medicines for its use as a heavy metal poisoning antidote.
