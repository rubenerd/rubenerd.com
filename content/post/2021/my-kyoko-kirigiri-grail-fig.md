---
title: "My Kyoko Kirigiri grail fig"
date: "2021-10-30T08:08:07+10:00"
abstract: "I don’t need more dust collectors, damn it!"
thumb: "https://rubenerd.com/files/2021/2180717@1x.jpg"
year: "2021"
category: Anime
tag:
- anime-figs
- danganronpa
location: Sydney
---
Collectors of all stripes online talk about their "grail", or a specific item they covet but have yet to find, buy, or receive. One can hear the mixed feelings of jubilation and envy when *others* are able to procure them, especially if they've proven to be expensive or elusive.

I stopped amassing anime figures in the interest of collecting ETFs and tiny apartment breathing space instead. Here comes the proverbial posterial prognostication: *but...* I do have one more, and she makes absolutely no sense. But that's fine, because today [I don't give a...](https://rubenerd.com/tim-king-gives-us-permission-to-not-give-a-fuck/)! Introducing the [B-style Kyoko Kirigiri](https://myfigurecollection.net/item/740245):

<p><img src="https://rubenerd.com/files/2021/2180717@1x.jpg" srcset="https://rubenerd.com/files/2021/2180717@1x.jpg 1x, https://rubenerd.com/files/2021/2180717@2x.jpg 2x" alt="Photo of the Kyoko Kirigiri fig by FREEing showing her purple eyes, hair plait, gloves, and tie." style="width:275px; height:414px;" /></p>

I *love* the art style of *Danganronpa*, even though I've never played it. It only narrowly lost out to *Persona 5* in the art and character design category of my [Favourite Game Meme grid](https://rubenerd.com/favourite-game-meme/), and *Portal 2* for games I want to play but haven't tried.

The character design and art style are so unmistakeable and unique in this series, and they've matched it well here. Her purple hair plait, tie, and subtle expression are *so cool*, and Clara likes heir hair ribbon. All of FREEing's figs are clearly made for, shall we say, *fan service*, but I think this is surprisingly understated and true to the character's original appearance, rather than the rushed, glorified caricatures the company tends to put out on a regular basis.

*(As examples, [Zero Two](https://myfigurecollection.net/item/740258) from the science-fiction Darling in the Franxx, and [Sakurajima Mai](https://myfigurecollection.net/item/676272) from Seishun Buta Yarō bare little resemblance to their characters at all; quite a feat given the outfit is a key plot device in the latter. These may be controversial views, but again, I refer you to the first link of this post)!*

Phat Company also make [this incredible rendition](https://myfigurecollection.net/item/144354), but it's too busy for my tastes, *and* I don't think her face or expression come close to being as nice or accurate. Go figure. Get it, because anime people call them figures and... *shaddup*.

<p><img src="https://rubenerd.com/files/2021/2180711@1x.jpg" srcset="https://rubenerd.com/files/2021/2180711@1x.jpg 1x, https://rubenerd.com/files/2021/2180711@2x.jpg 2x" alt="Side view of the aforementioned dust collector." style="width:400px; height:325px;" /></p>

The only thing I'm better at than writing pointless blog posts is coming up with rules for myself. I assured my wallet that I'd never seek out and buy a fig for a series I either haven't watched their anime, read their manga or light novel, or played their game. Unfortunately, that just means I've got another game to play. When and how is another question, especially given all the new BSD software releases of late that I have to experiment with.

Given how expensive she is on the second-hand market, and FREEing tending not to issue re-releases unless they've done a modification of some sort, this may already be solved for me. Time to buy another parcel of ETFs, I guess. Knowing my luck, these figs will end up being better investments!

