---
title: "pkgin’s manpage"
date: "2021-06-13T13:03:21+10:00"
abstract: "BUGS: We’re hunting them."
thumb: "https://rubenerd.com/files/2019/screenie2008-maihime-netbsd@1x.jpg"
year: "2021"
category: Software
tag:
- bsd
- freebsd
location: Sydney
---
I can get behind this:

> **BUGS**    
> We're hunting them.

Wait, you mean you're not using [pkgin](https://pkgin.net/) or [pkgsrc](http://www.pkgsrc.org/) for your package management needs? Or worse, you are, but you haven't contributed to the [NetBSD Foundation](https://www.netbsd.org/foundation/)? There's still time to correct this :).
