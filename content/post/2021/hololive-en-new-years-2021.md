---
title: "Hololive EN New Years, 2021"
date: "2021-01-02T09:17:00+11:00"
abstract: "Watching these streams made our day yesterday"
year: "2021"
category: Media
tag:
- hololive
- youtube
---
A dozen cases are being reported in New South Wales again over the last few weeks, so Clara and I being safe and not going anywhere if we can help it. We were probably going to spend the first day of 2021 yesterday watching Hololive characters anyway given the timezone difference was fortuitous, but shaddup.

Ame did *nine hours*, which we ran for most of the day. Everyone's favourite shark Gura chimed in halfway through with her own stream too. They motivated us for the first time in a while to think what our resolutions and goals for the year were.

<p><img src="https://rubenerd.com/files/2021/hololive-new-years-ame@1x.jpg" srcset="https://rubenerd.com/files/2021/hololive-new-years-ame@1x.jpg 1x, https://rubenerd.com/files/2021/hololive-new-years-ame@2x.jpg 2x" alt="Ame's stream" style="width:500px; height:333px;" /><br /><img src="https://rubenerd.com/files/2021/hololive-new-years-gura@1x.jpg" srcset="https://rubenerd.com/files/2021/hololive-new-years-gura@1x.jpg 1x, https://rubenerd.com/files/2021/hololive-new-years-gura@2x.jpg 2x" alt="Gura's stream" style="width:500px; height:333px;" /><br /><img src="https://rubenerd.com/files/2021/hololive-new-years-ina@1x.jpg" srcset="https://rubenerd.com/files/2021/hololive-new-years-ina@1x.jpg 1x, https://rubenerd.com/files/2021/hololive-new-years-ina@2x.jpg 2x" alt="Ina's stream" style="width:500px; height:333px;" /></p>

But our favourite was Ina, who [went into Minecraft](https://www.youtube.com/watch?v=Bi0Y1pv-tg4) and set off some little fireworks behind her torii gate. She had a few misfires, but it was beautiful!

Happy New Year to you and your folks.
