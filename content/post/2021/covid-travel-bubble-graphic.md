---
title: "Our Covid travel bubble"
date: "2021-09-15T22:25:24+10:00"
abstract: "The world is so... small."
thumb: "https://rubenerd.com/files/2021/covid-bubble@1x.png"
year: "2021"
category: Travel
tag:
- covid-19
location: Sydney
---
The Guardian's Datablog has an [interactive map](https://www.theguardian.com/australia-news/datablog/ng-interactive/2021/sep/15/5km-10km-from-home-radius-map-how-far-your-travel-limit-bubble-around-me-sydney-nsw-melbourne-victoria-vic-5-10-km-kilometre-calculator-tool-overlap-hotspot-lga-location) to show where we're allowed to travel within our Sydney Covid lockdown bubble.

<p><img src="https://rubenerd.com/files/2021/covid-bubble@1x.png" srcset="https://rubenerd.com/files/2021/covid-bubble@1x.png 1x, https://rubenerd.com/files/2021/covid-bubble@2x.png 2x" alt="Screenshot showing Chatswood and surrounding suburbs." style="width:500px; height:580px;" /></p>

Zooming in, I didn't realise we could go as far east as Northbridge within our LGA. Clara and I have ventured no further than Artarmon for months.
