---
title: "RSS feeds with pictures of the day"
date: "2021-12-16T15:45:41+10:00"
abstract: "Anyone else have some good ones?"
year: "2021"
category: Internet
tag:
- art
- rss
- space
location: Sydney
---
I've been trying to subscribe to more RSS feeds that publish *pictures of the day*. So far I've found:

* [Japan Today](https://japantoday.com/category/picture-of-the-day/feed)
* [The Guardian](https://www.theguardian.com/news/series/ten-best-photographs-of-the-day/rss)
* [Konachan image board landscape art](https://konachan.net/post/atom?tags=landscape)
* [NASA Astronomy](https://apod.nasa.gov/apod.rss)
* [NASA's Earth Observatory](https://earthobservatory.nasa.gov/feeds/image-of-the-day.rss)
* [Wikimedia Commons](https://commons.wikimedia.org/w/api.php?action=featuredfeed&feed=potd&feedformat=rss&language=en)

Anyone else have some good ones? I'd love to have more art to brighten up my mornings.
