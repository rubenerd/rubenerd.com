---
title: "Alicia Wanless on tackling disinformation"
date: "2021-05-26T09:00:51+10:00"
abstract: "Deplatforming and closed social network data."
year: "2021"
category: Media
tag:
- lies
- social-media
location: Sydney
---
Anastasia Kapetas [published an article last Tuesday](https://www.aspistrategist.org.au/the-case-for-a-disinformation-cern/) for the Australian Strategic Policy Institute, quoting Alicia Wanless from the Partnership for Countering Influence Operations at the Carnegie Endowment for International Peace. That was a lot of words.

> [T]here are yawning knowledge gaps on the effects of disinformation countermeasures. For example, said Wanless, there are very few credible studies on the effects of de-platforming disinformation spreaders. Does it help in limiting disinformation? Or do the perpetrators just move underground to more niche platforms, where followers can be further radicalised and exhorted to violence? 

It's at the bizarre stage now where disgraced or discredited people see their standing *elevate* as a result of being called out. They wear it like a badge of honour to have attracted the critique of doctors or scientists, and their followers are programmed to distrust those voices of authority. It's almost like a weaponised Streisand Effect, with less [duck sauce](https://www.youtube.com/watch?v=wWhtcU4-xAM "Music video of Barbra Streisand by Duck Sauce") and more evil.

The very architecture of current social networks also plays right into this:

> The other problem for research is that private companies hold most of the relevant data and are unwilling to share it widely. The platforms regard their data as valuable proprietary information and to date have only been willing to share small amounts with handpicked research institutions on particular cases. 

I agree with Alicia that social networks themselves must help fund academic research to tackle this problem. As businesses they have a financial incentive to stoke this dissent that is [counter to the needs](https://en.wikipedia.org/wiki/Market_failure "Wikipedia article on market failures") of civil society.

