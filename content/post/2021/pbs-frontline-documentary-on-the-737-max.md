---
title: "PBS Frontline documentary on the 737-MAX"
date: "2021-11-02T18:13:20+10:00"
abstract: "One of the better ones I’ve seen recently. They should have kept making the 757."
thumb: "https://rubenerd.com/files/2021/yt-wXMO0bhPhCw@1x.jpg"
year: "2021"
category: Media
tag:
- aviation
- boeing-737
- boeing-757
location: Sydney
---
<p><a href="https://www.youtube.com/watch?v=wXMO0bhPhCw" title="Play Boeing's Fatal Flaw (full documentary) | FRONTLINE"><img src="https://rubenerd.com/files/2021/yt-wXMO0bhPhCw@1x.jpg" srcset="https://rubenerd.com/files/2021/yt-wXMO0bhPhCw@1x.jpg 1x, https://rubenerd.com/files/2021/yt-wXMO0bhPhCw@2x.jpg 2x" alt="Play Boeing's Fatal Flaw (full documentary) | FRONTLINE" style="width:500px;height:281px;" /></a></p>

This is one of the better retrospectives on the 737-MAX, and the MCAS system that caused those two fatal crashes. I appreciated that they started with the human impact, before talking about the engineering, business decisions, and FAA oversight that lead to it. They also provided a lot of context that other rapid-fire videos tend to lack. The interview with Dennis Tajer at 37 minutes made me choke up too. "And I swore [at the transcript]. He got it right. The kid got it right".

The 737 was a clever modification to the 727 trijet (itself derived from the venerable 707) when it first came out, but it's been overdue for a proper replacement for decades. Hindsight has 20-20 vision, but I still [think it was a mistake to retire the 757](https://rubenerd.com/a-true-757-replacement/) to extend the jet beyond what it was designed for. The ungainly engine nacelle modifications were always the obvious external indiciation of this, though MCAS may very well be the one that had the most impact.

It's weird to think that in 2021, the closest airframe in capacity and range to the 757, one of my favorite aircraft designs of all time, is the A321neo.

