---
title: "Dell 27-inch 4K panels as Retina displays"
date: "2021-10-23T08:31:59+10:00"
abstract: "5K would be perfect, but 4K still looks crisp from my Mac and FreeBSD/KDE machine."
thumb: "https://rubenerd.com/files/2021/20_dell_s2722qc_i_s2722dc_27_calowe_monitory_z_matrycami_ips_o_rozdzielczosci_4k_oraz_wqhd_i_wsparciem_amd_freesync_0_b@1x.jpg"
year: "2021"
category: Hardware
tag:
- dell
- displays
- hidpi
- monitors
- retina
- reviews
location: Sydney
---
There are plenty of great reviews for Dell's various 4K panels, but today I want to answer if they work as *Retina* displays. Retina is Apple's name for 2× HiDPI, where pixels are cleanly doubled for added font clarity, and photo-quality images. The summary is: you can!

Apple introduced Retina MacBook Pros a decade ago, but external displays with high pixel densities and resolution have proven elusive. Apple partnered with LG on their Ultrafine series, but they're expensive and had quality control issues (I would know, unfortunately). Apple's reference monitor isn't aimed at consumers, and alas you can't use an iMac as an external display anymore either.

Fortunately, 4K panels have started becoming more common, and are reasonably priced. But there's a catch: they stretch this resolution over 27-inch panels, so they can't match the pixel density of Apple's kit. But people were saying they were great displays, so I snapped a refurbished Dell S2722QC (gesundheit) unit on special to see.

<p><img src="https://rubenerd.com/files/2021/20_dell_s2722qc_i_s2722dc_27_calowe_monitory_z_matrycami_ips_o_rozdzielczosci_4k_oraz_wqhd_i_wsparciem_amd_freesync_0_b@1x.jpg" srcset="https://rubenerd.com/files/2021/20_dell_s2722qc_i_s2722dc_27_calowe_monitory_z_matrycami_ips_o_rozdzielczosci_4k_oraz_wqhd_i_wsparciem_amd_freesync_0_b@1x.jpg 1x, https://rubenerd.com/files/2021/20_dell_s2722qc_i_s2722dc_27_calowe_monitory_z_matrycami_ips_o_rozdzielczosci_4k_oraz_wqhd_i_wsparciem_amd_freesync_0_b@2x.jpg 2x" alt="" style="width:500px; height:316px;" /></p>

My work MacBook Pro detected it as a native 60 Hz Retina display, and 2× HiDPI worked great from KDE on my FreeBSD desktop. The former was connected over USB-C, and the latter with a DisplayPort to USB-C cable. *Hurdle one cleared!*

There's no question it has a lower pixel density than what I'm used to, which manifests as less physical workspace and larger UI elements. It's Retina 1080p stretched over a 1440p size. And yet my impression is one of a lower-resolution Retina panel, not a standard monitor (if that makes sense). Fonts still look crisp, pictures have that gorgeous printed photo look, and the matte finish makes it *better* for glare and headaches than my MacBook Pro.

5K at 27-inch would be perfect, but I'm still enjoying this. If you've been holding off on upgrading your machine from a glorified Hercules or EGA display, and you have a GPU that can output 4K at 60 Hz, now might be a good time to try :).
