---
title: "Being simple"
date: "2021-06-27T11:48:37+10:00"
abstract: "It’s so simple to be happy, but so difficult to be simple."
year: "2021"
category: Thoughts
tag:
- happiness
- quotes
- philosophy
location: Sydney
---
I've seen this quote attributed to Gururaj Ananda Yogi in a few places:

> It’s so simple to be happy, but so difficult to be simple.
