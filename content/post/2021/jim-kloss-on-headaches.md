---
title: "Jim Kloss on headaches"
date: "2021-01-09T10:12:00+11:00"
abstract: "A post series in Nature is one of the best I've read on the subject."
year: "2021"
category: Thoughts
tag:
- health
location: Sydney
---
Speaking of Jim Kloss of *Whole Wheat Radio*, he noted my penchant for discussing migraines on Twitter last year, and shared [this article series on Nature](https://www.nature.com/collections/edcifebegf):

> Most people would describe a headache as a dull pain, a feeling of pressure, perhaps accompanied by a stiff neck — an inconvenience, but not something requiring medical assistance. However, the more than one billion people who experience migraines and cluster headaches have a very different understanding. For them, a headache is a much crueller condition. Attacks strike repeatedly, each one capable of delivering unfathomable pain that lasts anything from minutes to days. These primary headache disorders are no mere annoyance — they are debilitating conditions which the medical community must do more to alleviate.

There's some great content in these posts, including the most lucid and accurate [description of visual auras](https://www.nature.com/articles/d41586-020-02863-8) I've ever read. I know I'm trouble when I start having kalidoscipic vision along my periphery. That sounds like a band name.

I got migraines at least once a week as a kid, which mercifully became a once or twice a year occurance. But I've had a marked increase again since early 2020. Obvious external anxiety aside, if anything I'm healthier now, so I'm not sure what it is.
