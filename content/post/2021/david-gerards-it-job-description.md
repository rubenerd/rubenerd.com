---
title: "David Gerard’s IT job description"
date: "2021-05-19T08:41:49+10:00"
abstract: "I touch computers occasionally."
year: "2021"
category: Media
tag:
- bitcoin
location: Sydney
---
[Stilgherrian's interview](https://stilgherrian.com/edict/00131/) with [David Gerard](https://davidgerard.co.uk/) on the absurdity, environmental catastrophe, and scams surrounding Bitcoin was rather excellent. I especially liked his job description which sounds suspiciously familiar:

> I have a day job as a senior sysadmin, where my life is about half meetings and public relations... but I touch computers occasionally.


