---
title: "Unix Text Processing"
date: "2021-09-01T08:59:28+10:00"
abstract: "You owe it to yourself to download a copy of this seminal tome."
thumb: "https://rubenerd.com/files/2021/unixtextprocess_s.png"
year: "2021"
category: Software
tag:
- books
- bsd
- debian
- freebsd
- linux
- mwl
- netbsd
- sysadmin
- text-editing
location: Sydney
---
[Jan-Piet Mens](https://jpmens.net/) reminded me of *Unix Text Processing*, by Dale Dougherty and Tim O'Reilly of wood-cut animal fame. I've had the PDF of this book in my home directory for almost as long as I can remember, alongside the [Camel Book](https://www.programmingperl.org/) and many of [Michael W Lucas](https://mwl.io/)'s *Mastery* tomes.

I forgot that it's available under a Creative Commons licence to [download](https://www.oreilly.com/openbook/utp/)! You owe it to yourself to get a copy. The introducion to Unix alone is worth it.
