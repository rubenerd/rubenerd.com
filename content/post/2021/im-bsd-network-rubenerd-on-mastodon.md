---
title: "Follow me on Mastodon"
date: "2021-02-25T09:06:16+11:00"
abstract: "bsd.network/@rubenerd"
year: "2021"
category: Internet
tag:
- mastodon
- twitter
location: Sydney
---
I'd love to follow you! Here's my link: <a style="font-weight:bold" rel="me" href="https://bsd.network/@rubenerd">bsd.network/@rubenerd</a>

Mastodon is a federated microblogging platform, like Twitter but you can host it yourself. You can see a river of posts from the platform you're on, but you can also ping people across networks. Many of my friends at the moment are coming from [aus.social](https://aus.social/), but [mastodon.social](https://mastodon.social/) is also available if you want to give it a try.

The imitable [@phessler](https://bsd.network/@phessler) invited me to try his [BSD OS-adjacent](https://bsd.network/) Mastodon instance in 2018, but I'm only just getting around to using it properly thanks to the encouragement of [@screenbeard](https://aus.social/@screenbeard).

I was among the first users of Twitter, but I've had a love/hate relationship with the platform for the last few years, as evidenced by my need to [take breaks](https://twitter.com/taking-a-twitter-break/) from it. I wish I could say my interest in Mastodon was based on technical or philosophical reasons, but I think I need a break from the kinds of stuff that get posted there.

I used [Jaiku](https://rubenerd.com/p1167/), [Pownce](https://rubenerd.com/p3540/), [Soup.io](https://rubenerd.com/p3598/), [tent.is](http://5by5.tv/hypercritical/88), and [App.net](https://web.archive.org/web/20170304074305/https://alpha.app.net/Rubens), so I'm not holding my breath about the longevity of this platform either. But I'd be gloriously happy to be proven wrong!
