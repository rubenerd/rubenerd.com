---
title: "Inline links to videos with play buttons"
date: "2021-06-15T08:56:44+10:00"
abstract: "Via a question from Wouter Groeneveld on Mastodon."
thumb: "https://rubenerd.com/files/2021/yt-zCE26J0cYWA@2x.jpg"
year: "2021"
category: Internet
tag:
- privacy
- scripts
- youtube
location: Sydney
---
You know the adage that the cobbler's son walks barefoot? It describes the phenomena where the care and attention we accord our professional lives don't always translate into our personal ones. Nowhere is this more obvious than the scripts I use to create this site, which are a mishmash of gaffer tape, outdated knowledge, and *temporary* fixes!

Wouter Groeneveld of the excellent [Brain Baking](https://brainbaking.com/) site ([RSS feeds here](https://brainbaking.com/subscribe/)) asked me [on Mastodon](https://chat.brainbaking.com/objects/27c24d67-e141-415e-a4ca-a71a722773f1) how I generate thumbnails with play buttons for external videos, instead of using iframes. I'm sure it'd be trivial to add this to my [Hugo CMS](https://gohugo.io/) install, but I farm this off to a [shell script called video.sh](https://codeberg.org/rubenerd/rubenerd.com/src/branch/trunk/scripts/video.sh) that sits in my blog repo. It uses youtube-dl to download the video thumbnail, overlays a transparent PNG play button with ImageMagick, and spits out HTML with the correct `srcset` attribute for grainy and Retina/HiDPI displays.

*(At some point I want to replace this with a more robust Perl script using [PerlMagick](https://pkgsrc.se/graphics/p5-PerlMagick), [WWW:YouTube::Download](https://metacpan.org/pod/WWW::YouTube::Download), and [Text::Template](https://rubenerd.com/text-template-as-a-perl-jinja-alternative/), so I can handle different image sizes and other niceties, but this has worked surprisingly well for the last few years).*

Here's a recent [music example about Esther Golton](https://rubenerd.com/esther-golton-all-the-room-i-need/)\:

<p><a href="https://www.youtube.com/watch?v=yWT5IJ7Cusc" title="Play All The Room I Need"><img src="https://rubenerd.com/files/2021/yt-yWT5IJ7Cusc@1x.jpg" srcset="https://rubenerd.com/files/2021/yt-yWT5IJ7Cusc@1x.jpg 1x, https://rubenerd.com/files/2021/yt-yWT5IJ7Cusc@2x.jpg 2x" alt="Play All The Room I Need" style="width:500px;height:281px;" /></a></p>

My motivation for doing this was to remove external dependencies, tracking, and JavaScript from this site. It has the side effect of making my site faster and simpler, but the real reason was that I didn't think it was ethical to force external sites onto people they didn't opt-in to.
Or you might only use certain sites at arms length, such as via a VPN or proxy to protect your privacy.

A video thumbnail is static, hosted locally here, and linked to an external site that you can open yourself in a new tab. It doesn't have the instant gratification of inline playback, but then I think videos are best viewed on the target site at larger size anyway.

