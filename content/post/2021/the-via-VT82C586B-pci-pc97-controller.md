---
title: "The VIA VT82C586B PCI, PC97 controller"
date: "2021-03-19T12:57:17+11:00"
abstract: "Rediscovering the chipset in my Pentium 1 tower. It has an AT plug AND USB!?"
year: "2021"
category: Hardware
tag:
- mmx-machine
location: Sydney
---
With all this Commodore talk of late, you'd think I have no interest in any other vintage tech. I have namedropped my Pentium 1 tower a few times though, and as a Centronics parallel project (ah, so good), I'm restoring that machine as well to run an assortment of 16-bit OSs. She was my first computer, and restoring her to full working glory has also been a fun and rewarding journey.

The machine's 1997 Rhino 12+ board is fascinating. It was released right on the tail-end of the AT standard, with most peripherals requiring ISA or PCI cards. It has a physical toggle switch for power and an AT keyboard port in lieu of PS/2, but it also has USB which is bizarre. I can't imagine there are many boards with this specific combination, like finding an electric car dashboard that can deliver WAP via IR to a PDA.

For one evening exercise, I wanted to know if the Rhino 12+ board supported a few different things, and funnily enough the Data Sheet Archive had a copy of the technical guide for the [VIA VT82C586B PCI controller](https://www.datasheetarchive.com/pdf/download.php?id=9359a1633087999c2aa46cc3ebf89263eae589&type=M&term=VT82C586B). The pinout diagram is fascinating, and eye-opening after looking at Commodore 6510-compatible ICs from a decade earlier:

<p><img src="https://rubenerd.com/files/2021/via-pci-controller@1x.png" srcset="https://rubenerd.com/files/2021/via-pci-controller@1x.png 1x, https://rubenerd.com/files/2021/via-pci-controller@2x.png 2x" alt="" style="width:394px; height:459px;" /></p>

I also learned some surprising things:

* The USB 1.0 headers on the board also supports HCI v1.1. I'm interested in learning the difference between the two versions; I know back in the day we didn't really care which one a peripheral was.

* It does support ACPI *in addition* to APM, which makes it the perfect consolidated nostalgia machine. APM is required for proper power management in Windows 3.x, and can be invoked with POWER.EXE in your AUTOEXEC. But ACPI means a newer BSD could also run on it.

* It has distributed DMA, for supporting ISA DMA over the PCI bus! I'm not sure if that was a common on earler boards, or whether there were seperate controllers for ISA and PCI before inevitable consolidation.

My Commodore projects are taking priority right now, but I hope to write more about this beautiful old DOS/Windows 95/NT/BeOS/Red Hat/FreeBSD computer at some point. I'm coming to 8-bit computers *after the fact*; this machine was my first one :).

