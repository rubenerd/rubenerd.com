---
title: "Redirect your @FSF donations elsewhere"
date: "2021-03-25T09:31:25+11:00"
abstract: "RMS’s behaviour and views cannot be tolerated."
year: "2021"
category: Software
tag:
- bsd
- gnu
location: Sydney
---
My concerns with the Free Software Foundation, their licences, and how they conduct their advocacy, are well documented here. But Richard Stallman's announcement of his re-admission into the FSF casts *serious* doubt on the judgement of the board and the viability of the project.

You can [sign this open letter](https://rms-open-letter.github.io/) which also provides more details. We all need to send a sharp, immediate, and thorough rebuke of this decision.

For those of you who have donated to the FSF in good faith, I'd implore you to redirect to one of these organisations instead:

* [Open Source Initiative](https://opensource.org/civicrm/contribute/transact?reset=1&id=2)
* [Open Source Technology Improvement Fund](https://ostif.org/donate-to-ostif/)
* [Software in the Public Interest](https://www.spi-inc.org/donations/)

I'd also check out projects like the [BSD Fund](https://bsdfund.org/sponsor/) for those interested in donating directly to projects that benefit everyone in the open source community. I've also started contributing to projects directly; most have donate, Open Collective or Patreon buttons. Take my favourite password manager [KeePassXC](https://keepassxc.org/donate/), for example.

Michael Dexter has made the point that a community is only as good as the behaviour it tolerates, and therefore tacitly accepts. Let's take this opportunity to be the change we want to see. 👍

