---
title: "Brevity is often important"
date: "2021-10-11T07:18:47+10:00"
abstract: "But those who advocate for it are themselves rarely brief."
year: "2021"
category: Media
tag:
- writing
location: Sydney
---
An astute observation readers *always* make about my writing here is that it's accurate, witty, and with a degree of brevity that would make the most time-poor worker's head spin with possibilities about all the disparate activities such compact prose frees them to do. When those aforementioned readers ask how I impart such wit, brilliance, and sophistication in so few words, I answer with a response containing impactful references to the Commodore 128, Hololive, and that time I accidentally fell down a flight of stairs because my crush was coming the other way.

I belabour this succinct, terse point of short, clarifying brevity to mention, impart, and advocate for the position that everyone's writing is unique. We have different audiences, mediums, motives, artistic direction, style, and voice. Being prescriptive for specific use cases is fine, but I treat any claim of a universal truth for writing with skepticism. *Here be dragons*, as my favourite high school English teacher Ms Gravina used to say in response to such advice.

Which leads us to this article on <a rel="nofollow" href="https://have-a-word.com/why-brevity-is-important/">why brevity is important</a>, and how it's bugged me every time I see it recommended to aspiring writers on Twitter. I fear people are taking its message as rote, or are being discouraged.

The writer also doesn't heed his own advice. He identifies "waffle" (mmm, waffles) in his introduction by striking out words, but then runs afoul (mmm, chicken waffles) of [Skitt's Law](https://en.wikipedia.org/wiki/Muphry's_law):

> Regardless of industry, audience or approach, <span style="color:purple;text-decoration:line-through">all</span> successful content <span style="color:purple;text-decoration:line-through">exercises extreme brevity</span> <span style="color:green; font-weight:bold">is brief</span>.
>
> <span style="color:green; font-weight:bold">[Will]</span> <span style="color:purple;text-decoration:line-through">Do you really think</span> someone <span style="color:purple;text-decoration:line-through">is going to sit and</span> read a<span style="color:green; font-weight:bold">[n]</span> <span style="color:purple;text-decoration:line-through">2000 word</span> essay <span style="color:purple;text-decoration:line-through">on a topic</span> that could be <span style="color:purple;text-decoration:line-through">summed up in</span> a paragraph? <span style="color:purple;text-decoration:line-through">Of course they’re not</span> <span style="color:green; font-weight:bold">[No]</span>. <span style="color:purple;text-decoration:line-through">They’re going to go</span> <span style="color:green; font-weight:bold">[They'll visit]</span> <span style="color:purple;text-decoration:line-through">to</span> the website or publication that <span style="color:purple;text-decoration:line-through">has</span> covered the story in <span style="color:purple;text-decoration:line-through">as</span> <span style="color:green; font-weight:bold">the</span> few<span style="color:green; font-weight:bold">[est]</span> words <span style="color:purple;text-decoration:line-through">as possible</span>.

Is that true? I'll take a detailed explanation with diagrams over a brief, terse summary when learning a new concept.

> People <span style="color:purple;text-decoration:line-through">are busy. They</span> don’t want to waste time <span style="color:purple;text-decoration:line-through">reading your fluff</span>. That fluff might make your article sound nice <span style="color:green; font-weight:bold">[thank you!]</span>, but it distracts from the main point of your article and bores your readers.

Is that true either? Brevity is the soul of wit, depending on audience and purpose. But not always.

> Impactful copy <span style="color:purple;text-decoration:line-through">always</span> gets to the point. Its author <span style="color:purple;text-decoration:line-through">has ignored the desire to waffle, they’ve trimmed every last piece of fat ensuring</span> <span style="color:green; font-weight:bold">[has ensured]</span> the only words left are those <span style="color:purple;text-decoration:line-through">that have</span> <span style="color:green; font-weight:bold">[with]</span> meaning.

"Impactful" also bugs me; it borders on [synergistic meta-linguistic paradigm shifts](http://www.inquiriesjournal.com/articles/1538/a-general-theory-of-buzzwords-synergistic-meta-linguistic-paradigm-shifts "A General Theory of Buzzwords: Synergistic Meta-Linguistic Paradigm Shifts"). The large, empty website banner and redundant header image also took two spacebar hits to dismiss, but that's more to do with the [wasteful nature](https://rubenerd.com/scrolling-to-get-to-content/) of modern web design. He also employs filler words like "really", and repeats himself with a "featured download" within the text, having told us not to.

Clarity is important. I aim to be succinct in my technical writing, documentation, diagrams, compliance forms, etc. There's joy in realising prose can be *removed* when explaining a concept, just as one can remove refactored or redundant code.

But such advice isn't universal, and can be harmful. Clarity is beautiful, but so is a bit of fun. Or should I say, I endeavour to engage in levity in order to inject some amusement and personality into the prose for which I derive such joy typing. I've read so many whitepapers, and been to so many webinars and online industry events in the last few months, and the ones I remember weren't the briefest.

I'd hate for people to come away from articles like that thinking they need to remove personality to reduce their word count by 5%. That's what I worry about when experts pitch blanket advice. Maybe that's due to my impactful morning encounter with a cyclist.

