---
title: "Why did initials on MOS chips disappear?"
date: "2021-03-12T17:08:33+11:00"
abstract: "A fun story from a paper I found about the history and design of the MOS CSG chips."
thumb: "https://rubenerd.com/files/2021/initials-mos@1x.jpg"
year: "2021"
category: Hardware
tag:
- commodore
- history
location: Sydney
---
Commodore/MOS chips from the early 1980s often contained easter eggs in the form of the designer's initials. Thomas 'skoe' Giesel's [C64 PLA Dissected](http://skoe.de/docs/c64-dissected/pla/c64_pla_dissected_a4ds.pdf) paper included this example on page 29:

<p><img src="https://rubenerd.com/files/2021/initials-mos@1x.jpg" srcset="https://rubenerd.com/files/2021/initials-mos@1x.jpg 1x, https://rubenerd.com/files/2021/initials-mos@2x.jpg 2x" alt="" style="width:322px" /></p>

But why did they disappear? Thomas included this fun story on page 45:

> People who worked on reverse engineering CSG chips noticed that older chips usually have the initials of their engineers written on it. Later revisions do not have these nice signatures anymore. A certain incident was the reason.
> 
> In about 1984 or 1985 several chips were redrawn for the HMOS2 process to reduce cost. Bret Raymis worked on a shrink of the SID chip from 6 to 5 micron. He recalls, “I almost got fired for putting my initials on the chip with it appearing in 3 spots across the die with ‘Dave [DiOrio's] Bar and Grill’. The SID had three identical large blocks with a hole in the middle.” Dave DiOrio confirms this story, “It actually got fab’ed and the mask shop went berserk.”
>
> Bret’s intention was to make a joke, “But the president of Commodore did not think it was funny. Fortunately Mike [Angelina] took the heat and I did not get fired.” The chip in question was the 8580 R1 most likely. Unfortunately only later revisions have been seen up to now, which do not carry this Easter egg anymore. 
> 
> James Redfield concludes, “And from that day on we were no longer permitted to include our initials on the chips.”

