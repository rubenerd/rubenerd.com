---
title: "Tom Gauld’s science headline generator"
date: "2021-06-07T20:08:26+10:00"
abstract: "Avocados are improving my pancreas, by Elon Musk is clearly altering all our sourdough."
year: "2021"
category: Media
tag:
- journalism
- science
location: Sydney
---
Tom Gauld is a rather excellent cartoonist for New Scientist. His most [recent work](https://twitter.com/tomgauld/status/1401842595312160770) lets us write an intriguing science headline, and with bonus points for invoking *[Betteridge's Law of Headlines!](https://rubenerd.com/considered-harmful-is-considered-harmful/).*

<p><img src="https://rubenerd.com/files/2021/E3RYPlkXMAUku5r@1x.jpg" srcset="https://rubenerd.com/files/2021/E3RYPlkXMAUku5r@1x.jpg 1x, https://rubenerd.com/files/2021/E3RYPlkXMAUku5r@2x.jpg 2x" alt="Let’s Write an Intruiging Science Headline! Could (Avocados, Solar Wind, Tik-Tok, Helium, Elon Musk, or Snails) be (Improving, Harming, or Altering) your (Memory, Dog, Pancreas, Sleep, Sourdough, Pension)" style="width:500px; height:324px;" /></p>

Avocados are improving my pancreas, by Elon Musk is clearly altering all our sourdough. Damned Tik-Tok.

