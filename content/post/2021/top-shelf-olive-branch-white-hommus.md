---
title: "Top Shelf: The Olive Branch White Hommus"
date: "2021-10-18T18:20:45+10:00"
abstract: "It’s THAT good, I wrote about it on a nominally-tech blog."
thumb: "https://rubenerd.com/files/2021/TOBWhiteHommuswithlemon@1x.jpg"
year: "2021"
category: Thoughts
tag:
- food
- reviews
- top-shelf
location: Sydney
---
As the late, great humourist and poet Ogden Nash once observed: a cow is of the most bovine ilk; one end is moo, and the other is milk. The dip discussed today has neither, but as I'm sure at least one high school English teacher has said, it's a biting transposition of post-consumerism, irresistibly juxtaposed with the shifting zeitgeist of edible comestibles. As opposed to the inedible ones.

On this first installment of [Top Shelf](https://rubenerd.com/tag/top-shelf/), we dive into a creamy vat of hommus (hummus?), and what is easily the best one I've had the pleasure of consuming.

Clara picked me up a small pot of [Olive Branch White Hommus with lemon and olive oil](https://fruitezy.com.au/products/the-olive-branch-original-white-hommus-with-lemon-olive-oil-200g) from our local fruito, and I haven't been able to stop raving about it to people. It's everything I endeavour to be as a person: light, creamy, zesty, and fresh in a way I didn't think hommus could be. Wait a minute. 

<p><img src="https://rubenerd.com/files/2021/TOBWhiteHommuswithlemon@1x.jpg" srcset="https://rubenerd.com/files/2021/TOBWhiteHommuswithlemon@1x.jpg 1x, https://rubenerd.com/files/2021/TOBWhiteHommuswithlemon@2x.jpg 2x" alt="A small pot of Olive Branch White Hommus with lemon and olive oil." style="width:484px; height:342" /></p>

I fear my genuine love of this food may appear disingenuous on account of the haughty opening paragraph, and this bizarre mental state in which I find myself inextricably locked as I type this. All I can say, Australians and Kiwis deserve to buy some of this. Buy three or four, we need to support companies who make stuff this good.

Hommus detractors who lament the stodgy consistency and bland taste of regular, mass-produced muck would be well served with this excellent culinary creation. Please give it a try.

