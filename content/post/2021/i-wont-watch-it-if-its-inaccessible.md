---
title: "I won’t watch it if it’s inaccessible"
date: "2021-02-28T12:24:59+11:00"
abstract: "A follow-up post to my pontifications about cord cutters."
year: "2021"
category: Media
tag:
- shows
- streaming-services
location: Sydney
---
I reworked the title for this post at *least* a few times, but I ended up sticking with this seemingly obvious one because it captures the absurdity of this issue so well.

Earlier in the month I wrote about how "cord cutters" [weren't saving any money](https://rubenerd.com/cord-cutters-and-the-age-of-streaming/) with all these new streaming services. Whereas one could watch Netflix before (for example), now every media company is launching their own *me too* service. Even former computer companies... oh Apple, how far you've fallen. This dilutes the utility of any one platform each time, as more and more shows are splintered off into their own paid services that fewer people are likely to subscribe to in aggregate.

Media companies either don't realise this is happening, or they think that a tiny and ever-shrinking piece of pie is better than bending to the whims of Netflix, or not having pie at all. Both are silly.

In that post I theorised that piracy would be the natural outcome of this. If you don't make your content accessible, people will resort to the methods they used to access it before streaming existed. You know the adage about fences keeping honest people honest? This is not how you make honest people.

But it's perhaps even sillier than that. I realised recently I don't even consider shows I might otherwise be interested in if they're on an unsupported or different platform. Media companies are still used to living in the days of limited public airwaves and cable TV channels. We're living with an embarrassment of riches now; we've got plenty of other entertaining things to enjoy if you lock your show behind another streaming platform.

*(Original programming is about the only direct counterpoint I can see to this. It might be better overall for the industry, if your metric is the quantity of shows being produced. But it's the same outcome: Apple releasing shows on their platform don't even cross my radar because I don't subscribe, and I ignore recommendations as soon as I realise I can't watch it. But I suppose for companies like Apple, or Amazon, the content is there to have you buy hardware from them).*
