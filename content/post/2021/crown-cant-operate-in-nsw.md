---
title: "Crown can’t operate in NSW"
date: "2021-02-17T08:29:48+11:00"
abstract: "So why are they operating in Victoria?"
year: "2021"
category: Thoughts
tag:
- censorship
- ethics
- great-firewall-of-australia
- news
location: Sydney
---
Alongside the word *punters* (which sounds like a broken strawberry container), there are few things I dislike more than Crown in Australia. Their Melbourne casino is the largest in the country and, as you would expect, it's been embroiled in some *dodgy things*, to use the legal term.

A few years ago the company built a $2.2 billion skyscraper in the Barangaroo precinct in Sydney that isn't ugly as much as it's pedestrian. And then this happened, as reported by [Josh Bavas for the ABC](https://www.abc.net.au/news/2021-02-16/ilga-tells-crown-resorts-it-is-unsuitable-to-open-sydney-casino/13158434)\: 

> Crown Resorts has officially been informed by the NSW gaming regulator it is no longer suitable to hold the licence for its new Sydney casino. [..] In an ASX statement released this morning, Crown said it had been informed of the decision and that it had also been found to have breached a clause of the state's gaming regulations.

They're appealing the state's independent body, but it still made my week! I'm tired of specific industries expecting that they can steamroll through what they want to do, so even an inconvenience like this is a big win. It also raises the question about why Victoria allowed them to operate the largest casino in Melbourne, and continues to do so.

Why is this news on a nominally-technical blog? Aside from my own sense of joy, the CEO of Crown is Helen Coonan. If that name rings a bell, she was the former conservative MP and Australian telecommunications minister who pushed for mandatory Internet filtering. Eventually she oversaw the release of the multi-million dollar NetAlert system for ISPs, which a 16-year old cracked within thirty minutes. I love that that's her legacy!

