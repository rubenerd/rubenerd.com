---
title: "Mori Calliope’s new loading screen is dope!"
date: "2021-06-13T20:11:27+10:00"
abstract: "Get ready for the stream, Dead Beats!"
thumb: "https://rubenerd.com/files/2021/mori-op-2@1x.jpg"
year: "2021"
category: Media
tag:
- gawr-gura
- hololive
- mori-calliope
location: Sydney
---
From her [latest collab with Gawr Gura](https://www.youtube.com/watch?v=sbo8zS8otzQ)\:

<p><img src="https://rubenerd.com/files/2021/mori-op-1@1x.jpg" srcset="https://rubenerd.com/files/2021/mori-op-1@1x.jpg 1x, https://rubenerd.com/files/2021/mori-op-1@2x.jpg 2x" alt="Screenshot showing Cali judging us with her scythe." style="width:500px; height:280px;" /><br /><img src="https://rubenerd.com/files/2021/mori-op-2@1x.jpg" srcset="https://rubenerd.com/files/2021/mori-op-2@1x.jpg 1x, https://rubenerd.com/files/2021/mori-op-2@2x.jpg 2x" alt="Cali with her drip shades and a smile. I'm still a bit scared." style="width:500px; height:280px;" /></p>
