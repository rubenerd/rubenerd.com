---
title: "The Raspberry Pi 400 as a couch game machine"
date: "2021-12-18T19:21:04+10:00"
abstract: "Was I the last person to learn of the existence of this machine!?"
thumb: "https://rubenerd.com/files/2021/rpi-400@1x.jpg"
year: "2021"
category: Hardware
tag:
- bochs
- dosbox
- qemu
- nostalgia
- raspberry-pi
- simh
- vice
location: Sydney
---
Speaking of the [Raspberry Pi](https://rubenerd.com/trying-pi-hole-on-the-raspberry-pi-a-plus/), I've been in the market for an integrated keyboard case for one. I've rediscovered the joys of couch computing, and love the idea of having a small machine I can emulate various vintage games on the TV (mostly DOS), and take with me to other places.

Using a Raspberry Pi for this has a few advantages:

* It easily has the compute power for the old stuff I love.

* I can emulate a few different architectures and software platforms from the one device, rather than needing a bunch of separate boxes. Space in our tiny apartment is at a premium at the best of times.

* Running ARM FreeBSD or Debian/Raspberry Pi OS gives me access to a huge library of stuff, including DOSBox, QEMU, VICE, SimH, and Bochs.

* The Raspberry Pi's HDMI port and software scaling negates the need for an expensive analogue video upscaler.

* I keep my precious vintage computer kit firmly on my desk where they're not getting rattled around. They also have *so much* stuff plugged into them, it'd be a pain to carry them around.

* I can carry my entire software library on one or more cards. Using the CD-ROM and Iomega drives on my Pentium 1 tower, and the 1571 disk drive on the C128, C16, and Plus/4 is fun, but not that portable!

There are a few options, some of which involve some [manual soldering](https://technabob.com/blog/2015/03/22/raspberry-pi-inside-mechanical-keyboard/). But then I discovered what [everyone else had](https://www.raspberrypi.com/products/raspberry-pi-400/) for a year now:

<p><img src="https://rubenerd.com/files/2021/rpi-400@1x.jpg" srcset="https://rubenerd.com/files/2021/rpi-400@1x.jpg 1x, https://rubenerd.com/files/2021/rpi-400@2x.jpg 2x" alt="Photo of the Raspberry Pi 400" style="width:500px; height:232px;" /></p>

This looks *so cool!* It's a remodelled Raspberry Pi 4 in the company's official little keyboard, with ports and GPIO pins running along the back. It reminds me of a 1980s home computer, both in form factor and spirit.

**I can't stop grinning at this beautiful little thing!**

I was intending to write a grand long post about how the Vilros Keyboard Trackpad Hub looked disappointing, and that I wished more people did something better. I might just bite the bullet and get one of these... or add it to the Xmas wishlist!

