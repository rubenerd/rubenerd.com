---
title: "The 1Password Electron debate is good news for the industry"
date: "2021-08-17T17:32:03+10:00"
abstract: "My hope is we’re seeing a new market signal for quality software being established."
year: "2021"
category: Software
tag:
- accessibility
- electron
- passphrases
- security
location: Sydney
---
The furore around 1Password's move to Electron has exposed rifts in the software development community, and challenged assumptions about what compromises and tradeoffs are acceptable when shipping commercial software. AgileBits is disproportionately copping the brunt of this right now, but I see the ensuing discussion as good (and long overdue) for the health and viability of the industry.

1Password is but one of many applications that have moved to the Electron browser wrapper in recent years. The framework's march into chat applications might have been tolerated and shrugged off as inevitable, but having it take over an indispensable, polished, and respected utility has struck a nerve. As Jean-Luc Picard said of the Borg's incursions in *Star Trek First Contact*, users have reacted with "this far, no further!"

*(There are also legitimate questions about the removal of local 1Password storage, and the company's wholesale move to a subscription model. There are indications they're listening about the former, and I can empathise with their need to do the latter. All topics for another time).*

Electron is not a framework deployed to improve software for users. Nobody proudly boasts about its use, as evidenced by AgileBits’ insistence of a Rust backend in response to feedback. At best it's seen as an engineering hack to deliver a tolerable desktop application across multiple platforms using web technologies and shared code, while saving time and money.

In a perverse case of misaligned incentives, these savings are externalised and borne by users, who's machines run slower and with greater resource utilisation, to say nothing of the impact to accessibility, interface responsiveness, and OS integration. You don't get something for nothing, and in this case Electron's very real technical debt lives rent free on customer machines. People have a right to feel resentful about this, especially when a application was previously a respectful citizen. Not for 1Password specifically, but I needed to spec my latest laptop with twice as much memory to run all the Electron I need for work, and another had to retire early. That costs real money.

*(I'd include security in the above list of potential shortcomings, but I take AgileBits’ word that they've done work to harden the framework).*

The prevailing industry attitude thus far was that users wouldn't notice, or would cop the hit. My optimistic hope is we're starting to see some real pushback, and that a market signal for quality software is being established. "Not built on Electron" or "native application" are now legitimate and recognised marquee features many people wouldn't have known about before, and are now being used to compare software. That's *awesome*.

