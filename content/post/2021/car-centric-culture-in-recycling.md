---
title: "Car-centric culture in recycling"
date: "2021-10-24T08:33:02+10:00"
abstract: "Here are rules for your car, as you’re clearly coming by car. Car car car. Zebra crossing!"
year: "2021"
category: Thoughts
tag:
- cars
- recycling
location: Sydney
---
Our local council has an [e-waste recycling centre](https://www.willoughby.nsw.gov.au/Residents/Waste-and-recycling/Reduce-reuse-recycle/E-waste), but their COVIDSAFE plan makes it clear they haven't considered people who don't arrive by smokebox:

> All CRC customers are to wear a mask, sign in using the NSW Government’s QR code and remain in their vehicles until directed to enter the drop-off area by a staff member. [..] Only enter the CRC when the boom gate is lifted for your car by a staff member. Please drive in slowly as cars may be queued in the driveway. [..] All CRC customers are required to self-unload their items without any assistance from staff. Customers should bring someone along to assist if required.

Most of these rules pre-date Covid, but have been rearranged and clarified in the context of social distancing and medical safety.

It's not entirely unreasonable to expect *most* people to rock up in four-wheeled body crushers. Old CRTs, bags of disued cables, and broken lightbulbs aren't exactly the sorts of things you want to be carrying long distances on foot, or lugging onto public transport.

But that's not to say everyone will need to drop off stuff in that way. Maybe they live nearby. Maybe they don't have a carbon-sink burner, and are walking there for the exercise with a small trolley, like certain weird bloggers. I've been let into the facility before on foot, and it was perfectly safe. Why not have a blurb about that as well?

*(Update: No longer. And worse, they couldn't have been ruder if they tried. Go to The Good Guys store in Chatswood instead; they take your e-waste with a smile, and even help with unpacking)*

Sydney is slightly better than most Australian cities when it comes to public transport investment, but culturally and politically we still have a long way to go. Maybe I'm still a bit miffed that a motorist plowed into me on a zebra crossing yesterday. Don't they know zebras are highly-strung with skinny legs?

