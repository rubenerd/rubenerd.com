---
title: "Anti-vaxxers"
date: "2021-09-14T08:21:15+10:00"
abstract: "Vaccines are a privilege, and a civic responsibility."
year: "2021"
category: Thoughts
tag:
- covid-19
- health
- psychology
location: Sydney
---
Anti-vaxxer sentiment has to be among the most counterproductive, wasteful, and pointless follies humans have ever advocated. It's hard not to look at the current pandemic with despair, knowing that we're dragging it out so a cohort can cut off their nose to spite their face. And for what?

There's an element of gullibility, as with all conspiracy theories. Anti-vaxxer sources would crumble if held to the same standard as the institutions, researches, and doctors they seek to discredit. Charlatans exploit this gap by peddling their bullshit; for a price, of course. It's also transparently easy to flip around based on the same dubious claims, such as *anti-vaxxers are part of a left-wing environmentalist conspiracy to thin the world's population to stave off climate change.* The truth, as tends to be the case, is far more mundane.

But the biggest issue isn't facts or evidence. Human emotion rarely follows either, especially when we're dealing with a pandemic of global significance, and when politics gets its grubby hands involved. Detractors feel as though taking a vaccine is mandatory, invasive, and therefore robs them of agency and liberty.

You know what, I can empathise with this more than the bullshit merchants above. Having a government inject me with something I don't understand, against my will, is scary. The "not understand part" can be addressed with education, and the latter is a lesson in civics.

Vaccines are a privilege. It wasn't that long ago when we had to accept death for these sorts of diseases. It's humbling to live in a time period where we have the medical and scientific capability to develop effective protections against these ailments. There are people without access to vaccines today that are screaming for them to protect themselves and their loved ones, who can't understand why loud people in the West would shun them.

If you want to live in a society, you don't shirk your responsibility when it's under threat. It is your moral, ethical, and (dare I say) patriotic duty to be informed, and to reduce the spread of this disease. Even if you're only doing it for selfish reasons to reduce your chances of a hospital visit, you stand a better chance if those around you are healthy as well. If you think the disease was somehow developed in a lab, or state sponsored, arming yourself with a vaccine will not let them get away with it!

Vaccines are no panacea; just as turning your lights off during The Blitz didn't stop all the bombers, or seat belts don't always save you from crashes. But it's a quick, simple thing you can do, then immediately forget about and live your life. I felt *awesome* after getting my second shot, knowing how lucky I was that I could.

Please spread the word if you have a platform of your own, no matter how small. The only vaccine against bad ideas are good ones.

