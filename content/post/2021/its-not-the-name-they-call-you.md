---
title: "It’s not the name they call you"
date: "2021-04-21T22:44:47+10:00"
abstract: "… but the name you answer to."
year: "2021"
category: Thoughts
tag:
- ethics
- philosophy
- quotes
location: Sydney
---
[Brandon quoted Madison Cowan](https://brandonsjournal.com/2021/03/26/free-write-week-12-mar-19-mar-25/) in his journal:

> It’s not the name they call you that’s important, but the name you answer to.

