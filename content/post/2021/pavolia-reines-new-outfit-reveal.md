---
title: "Pavolia Reine’s new outfit reveal"
date: "2021-08-20T08:30:26+10:00"
abstract: "Batik, a cute bob, AND glasses!?"
thumb: "https://rubenerd.com/files/2021/reine-outfit@1x.jpg"
year: "2021"
category: Media
tag:
- hololive
- pavolia-reine
- youtube
- video
location: Sydney
---
It's hard to keep up with all the Hololive news over the last week. Or the last month; we're still not current with [IRyS](https://www.youtube.com/channel/UC8rcEBzJSleTkf_-agPM20g) despite her debuting for Hololive English *a month ago*. Where would we be during Covid lockdown and home isolation without these entertaining people (home obviously, it was a metaphor. Wait, who am I talking to)?

Clara and I finally caught up with our [favourite Hololive Indonesia](https://www.youtube.com/channel/UChgTyjG-pdNvxxhdsXfHQ5Q) character's [outfit reveal](https://www.youtube.com/watch?v=WleEDM3r1hw) yesterday. Batik, a cute bob, *and* glasses!? *Bagus!*

Reine now not only has the best Minecraft <abbr title="background music" style="border:0; text-decoration:none;">BGM</abbr>, but the best shawl.

<p><img src="https://rubenerd.com/files/2021/reine-outfit@1x.jpg" srcset="https://rubenerd.com/files/2021/reine-outfit@1x.jpg 1x, https://rubenerd.com/files/2021/reine-outfit@2x.jpg 2x" alt="Screenshot from her reveal stream" style="width:500px; height:286px;" /></p>

