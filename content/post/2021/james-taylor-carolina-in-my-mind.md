---
title: "James Taylor, Carolina in My Mind"
date: "2021-10-18T07:48:35+10:00"
abstract: "Making me nostalgic for a place to which I haven’t been."
thumb: "https://rubenerd.com/files/2021/yt-tRIhSrWFeWM@1x.jpg"
year: "2021"
category: Media
tag:
- james-taylor
- music
- music-monday
location: Sydney
---
Today's [Music Monday](https://rubenerd.com/tag/music-monday/) still tugs at my heartstrings and makes me nostalgic for a place to which I've never been.

*(My sister's fiancé worked for a few months in Raleigh and loved it. Maybe I need to add it to the list).*

<p><a href="https://www.youtube.com/watch?v=tRIhSrWFeWM" title="Play Carolina in My Mind"><img src="https://rubenerd.com/files/2021/yt-tRIhSrWFeWM@1x.jpg" srcset="https://rubenerd.com/files/2021/yt-tRIhSrWFeWM@1x.jpg 1x, https://rubenerd.com/files/2021/yt-tRIhSrWFeWM@2x.jpg 2x" alt="Play Carolina in My Mind" style="width:500px;height:281px;" /></a></p>

