---
title: "The @watsonameliaEN and @pavoliareine"
date: "2021-04-08T21:06:00+10:00"
abstract: "My two favourite VTubers!"
thumb: "https://rubenerd.com/files/2021/ame-reine@1x.jpg"
year: "2021"
category: Anime
tag:
- amelia-watson
- hololive
- pavolia-reine
location: Sydney
---
<p><img src="https://rubenerd.com/files/2021/ame-reine@1x.jpg" srcset="https://rubenerd.com/files/2021/ame-reine@1x.jpg 1x, https://rubenerd.com/files/2021/ame-reine@2x.jpg 2x" alt="Picture from Ame's Twitter account showing her and Reine" style="width:500px" /></p>

[This tweet](https://twitter.com/watsonameliaEN/status/1380041621761187841) made me so happy; it's my two favourite VTubers!

You definitely should subscribe to [Ame](https://www.youtube.com/channel/UCyl1z3jo3XHR1riLFKG5UAg) and [Reine](https://www.youtube.com/channel/UChgTyjG-pdNvxxhdsXfHQ5Q), assuming you want to go down the Hololive rabbithole. Which you definitely should. They even got Clara and I into Minecraft.
