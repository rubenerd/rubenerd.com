---
title: "KTMB USB keys, and what do we call them?"
date: "2021-07-31T09:12:20+10:00"
abstract: "This would match my Singapore Downtown Line MRT key. Damn it."
thumb: "https://rubenerd.com/files/2021/3f765c279c5496130c5c6432be7de380@1x.jpg"
year: "2021"
category: Travel
tag:
- malaysia
- singapore
- usb
location: Sydney
---
Speaking of [train journeys and nostalgia](https://rubenerd.com/train-journey-bucket-list-via-jim-kloss/) for Peninsula Malaysia's railway operator, you can get [official KTMB USB keys](https://shopee.com.my/KTM-Berhad-Blue-Tiger-Pendrive-32GB-i.257872790.7155021529). Here's one handsome example of their *Blue Tiger* unit that plies from Singapore to Kuala Lumpur, along with a rather handsome metal case:

<p><img src="https://rubenerd.com/files/2021/3f765c279c5496130c5c6432be7de380@1x.jpg" srcset="https://rubenerd.com/files/2021/3f765c279c5496130c5c6432be7de380@1x.jpg 1x, https://rubenerd.com/files/2021/3f765c279c5496130c5c6432be7de380@2x.jpg 2x" alt="Thumbnail showing the aforementioned USB key and case" style="width:336px;height:305px;" /></p>

Clara and I got official Singapore Downtown Line MRT keys in 2019 from the Land Transport Authority gift shop, and have more clear files and lanyards from various Japanese railway authorities than we know what to do with. This seems like a natural progression.

This raises a question though: what are these things called? Have we ever decided, or standardised on a name? I erroneously called my first 256 MiB [sic] unit a *memory stick* in the early 2000s before realising that was a Sony memory card format. I call them *USB keys* out of habit, though that sounds more like a hardware auth token than anything else. KTMB calls them *pendrives* which I think sounds classy. My dad calls them *thumb drives*. *USB drive* sounds like it could refer to any portable hard drive or SSD.

I asked a colleague, and he calls them "malware vectors". Such a buzzkill.

