---
title: "Newer CF card slower in my Pentium tower"
date: "2021-04-10T17:04:20+10:00"
abstract: "Could there be something about UDMA7 that this old machine can’t handle?"
thumb: "https://rubenerd.com/files/2021/photos-cf-cards@1x.jpg"
year: "2021"
category: Hardware
tag:
- compact-flash
- memory-cards
- mmx-tower
- troubleshooting
location: Sydney
---
I used CompactFlash cards in my Pentium 1 tower in lieu of spinning hard drives. CF cards implement ATA/IDE, so all you need is a passive adaptor. I had two SanDisk Ultra 16 GiB cards from an old digital camera that worked a treat.

CF cards don't posess that nostalgic clattering sound of mechanical hard drives, and chipset throughput limitations likey mean they *generally* don't perform much better either. But they're *much* easier to source, don't draw anywhere near as much power, take up less space in cluttered cases, and in a pinch can be unplugged and easily connected to a card reader on another machine to transfer files.

<p><img src="https://rubenerd.com/files/2021/photos-cf-cards@1x.jpg" srcset="https://rubenerd.com/files/2021/photos-cf-cards@1x.jpg 1x, https://rubenerd.com/files/2021/photos-cf-cards@2x.jpg 2x" alt="Photo showing a 32 GiB Extreme CF card next to a 16 GiB Ultra CF card, sitting on a MS-DOS 6.2 box" style="width:500px; height:333px;" /></p>

They've worked so well for me so far, that I decided to upgrade one of the 16 GiB cards to a brand new 32 GiB SanDisk Extreme. It claimed to have a  140 MB/s sustained write speed compared to the 16 GiB card's 30 MB/s. Again, not a big deal considering it'd still bottleneck on the VIA chipset. At the very least I expected a slight performance improvement along with more space.

The exact opposite happened! This card has been *glacially* slow. PartitionMagic took more than 45 minutes to move a 24 GiB partition. Installation of Windows NT 4.0 Workstation took longer than the time Clara and I went out for dinner, and 95 didn't fare much better. The installers for BeOS 5 and NetBSD 8.1 never finished loading before I gave up, and OS/2 4.5 Warp didn't want to boot at all.

So what went wrong? Either I got a dodgy card, which I can't discount because I don't have a similar card to test. Or there's a protocol mismatch. Maybe UDMA7 isn't as backwards compatible as UDMA1 on the 4 GiB card, and the VIA chipset invoked a fallback. I'm not well versed in the IDE protocol standards, but is such a thing possible?

I didn't think to run any benchmarks, because I was too busy putting the old card back in so I could boot and read Encarta 95 again :). My subjective view of the card's performance might not be as stark as what raw numbers suggest; I'll leave the card out for now in case I want to do a more quantitative comparison at some point.

