---
title: "The iPad mini 6"
date: "2021-11-11T09:59:41+10:00"
abstract: "Tempted to get one as a passive media device."
year: "2021"
category: Hardware
tag:
- apple
- books
- ipad
- reading
location: Sydney
---
I've written choice words about the direction of Apple's iOS platform of late, especially on mobile phones. Their last few generation of iPhones are unusable for those of us with OLED sensitivity, and the platform's UI and design feel like they're slipping backwards. At least, for the way I use them.

But then, as so often happens, all I needed was a bit of empathy.

Some recent health adventures have meant sitting or lying down for extended periods where using a laptop is awkward, so I've been shamelessly using my work iPad Air for reading newspapers, manga, and books. Steve talked about tablets being more "personal" and "intimate" devices... and I think I'm finally starting to understand why.

My only concern has been the size. If you're feeling weak or tired, it's gets too heavy too fast. It also has *work stuff* on it, which I'm trying to take leave from.

I'd love to get a larger form-factor Kobo for dedicated reading, but now I'm considering an iPad Mini. A few recent books have large photos and maps which look much better on a colour screen, and I've been reading *so many* more newspapers and RSS feeds having it on a tablet. It's like the book form factor we've used for hundreds of years has something going for it.

iPads will only be media consumption or presentation devices for me; even with external keyboards they have far too many design compromises and limitations for how I work (not to mention the closed App Store, etc). There's something about having a smaller one just for reading that could be fun. And uncharacteristically for modern Apple, it doesn't have a notch, or an OLED screen!

Maybe I could use all the money I've saved living in a country with universal healthcare on something silly like this to cheer me up. I check the Medicare app and my jaw hits the floor how much these adventures would have cost otherwise. It feels like freedom.

*(My ideal future setup would be a FreeBSD laptop with a HiDPI display, and an iPad to offload stuff I can't run on the former, like chat or conferencing apps. A smaller one would weigh less in my bag next to it, too. But that's for a future post).*

