---
title: "A rough week for Apple users"
date: "2021-08-13T13:28:54+10:00"
abstract: "Image scanning precedent, and a beloved password manager ditches its users for Electron. But the antitrust legislation looks promising."
year: "2021"
category: Software
tag:
- apple
- electron
- hardware
- keepassxc
- phones
- privacy
- security
location: Sydney
---
It's felt a bit like a one-two punch this month, hasn't it? Among the Apple, iOS, and macOS-related stories, these stood out:

* Apple's [latest privacy stance](https://appleprivacyletter.com/) is deeply worrying, regardless of whether they were coerced by an opaque government process or not to have iPhones scan and report images. It has scope creep written all over it, and sufficient precedent to worry about people's safety in repressive regimes.

* 1Password has regressed into an Electron application, and mandates remote storage. Being a native application with standalone vaults were the two reasons engineers recommended the tool. Electron externalises resource constraints to the client, which only compounds as you run more of them. It's electronic kudzu, and represents a disregard for end users.

If you're affected by the latter, I can't recommend [KeePassXC](https://keepassxc.org/) highly enough. It's open source, and cross-platform done right using Qt/C++, so it's fast and uses few system resources. It has native plugins for Firefox, Vivaldi, and Chromium, and its UI is straight forward. They also accept [Patreon donations](https://keepassxc.org/donate/), which I'm more than happy to chip in each month.

