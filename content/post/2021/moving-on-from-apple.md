---
title: "Moving on from Apple"
date: "2021-03-09T08:53:15+11:00"
abstract: "A ThinkPad with FreeBSD 13 sounds pretty good."
year: "2021"
category: Hardware
tag:
- apple
- mac
- macos
- thinkpads
location: Sydney
---
Regular readers among you wouldn't be surprised to know that I use Apple kit. I've had Macs since my gorgeous blueberry iMac DV in primary school, but it was Mac OS X that made me love the platform. Here was a well-designed UNIX workstation with Wi-Fi and graphics drivers that *just worked*. Trawl through my archive here and you'll see my excitement and joy at upgrading and installing my favourite desktop OS.

Truthfully though, I've been wary of the direction the company's devices have been going for a while. Despite finally firing on all cylinders again with their desktop and phone CPUs outperforming everything in their class, the company's other priorities haven't aligned with mine for a few years now. These are the two latest examples:

* macOS's Big Sur UI represents all the shallow excess the company is stereotyped with. It's something I'd expect from the Windows Aero era of Microsoft.

* The latest iPhones are unusable for those of us with OLED sensitivity.

For a company so lauded for accessibility and usability, it blows me away that these are the reasons. My prevailing attitude that "I use them because everything else is worse" is starting not to hold, either. People are catching up, and filling in the gaps that for years Apple have either pretended don't exist, or haven't shown an interest in. Which is their right of course, but it's not for me.

I'm probably better placed than most long-term Mac users. I've always had side PCs and servers running either Linux or FreeBSD. I've been deliberate with my choices to avoid Mac-specific software wherever I can. I need Microsoft Office for work, and I do like a few specific Mac tools like Alfred, but I'm sure those could be worked around.

Which leads us to pull factors. I'm not sure if you've checked out the betas of FreeBSD 13, but it's *slick*. The team have done an incredible job with polish and driver support with this release. Dare I say it, but I also want to spend more money on vintage tech as well, which I'm *really* getting back into.

So where to from here? Right now, nowhere. I have a 16-inch MacBook Pro that works, and I'll keep it on Catalina as long as I can get updates for it. My iPhone 8 still runs fine with a bright LCD, TouchID, and no notch. The most affordable thing to do right now is stay put, especially during *These Uncertain Times.*

But I'm keeping my eyes open. A refurb or second-hand 15-inch HiDPI ThinkPad would be great; maybe a T-series over the Carbons because I miss having a numeric keypad. Android is more of a jump considering I dislike the company that makes it, and there are so many mediocre handsets with poor track records of security updates; but maybe I can find a decent LCD one that still runs the software I need. Have I talked about how much I miss Palm and webOS this week?

If all of this sounds silly and self-absorbed, it's because it is. But it *feels* like a big decision for someone who spends their lives tinkering and working with this stuff.

