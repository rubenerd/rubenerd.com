---
title: "Farmer Gura"
date: "2021-01-08T17:36:00+11:00"
abstract: "... had a farm! I can't tell you how much joy these Hololive EN characters bring to our lives."
category: Media
tag:
- hololive-en
- video
- youtube
---
I can't tell you how much [joy](https://www.youtube.com/channel/UCMwGHR0BTZuLsmjY_NT5Pwg "Hololive En, Ina Ina Ina...!") [these](https://www.youtube.com/channel/UCyl1z3jo3XHR1riLFKG5UAg "Hololive En, Amelia WATSON!") [characters](https://www.youtube.com/channel/UCoSrY_IQQVpmIRZ9Xf-y93g "Hololive EN, Gawr Gura") bring to our lives. Except, I just did!

<p><a href="https://www.youtube.com/watch?v=o6lnSnqn3Uc" title="Gawr Gura cooks meatloaf"><img src="https://rubenerd.com/files/2021/farmer-gura-0@1x.jpg" srcset="https://rubenerd.com/files/2021/farmer-gura-0@1x.jpg 1x, https://rubenerd.com/files/2021/farmer-gura-0@2x.jpg 2x" alt="Farmer Gura had a farm~" style="width:500px" /><br /><img src="https://rubenerd.com/files/2021/farmer-gura-1@1x.jpg" srcset="https://rubenerd.com/files/2021/farmer-gura-1@1x.jpg 1x, https://rubenerd.com/files/2021/farmer-gura-1@2x.jpg 2x" alt="Wait, is that the right song? What's the farm song?" style="width:500px" /></a></p>
