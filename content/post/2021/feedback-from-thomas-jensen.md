---
title: "Feedback from Thomas Jensen"
date: "2021-03-30T14:56:38+11:00"
abstract: "Writing, CSP, and image delivery."
year: "2021"
category: Internet
tag:
- comments
- feedback
location: Sydney
---
Thomas Jensen emailed me a kind note, which was a pleasent change from all the anti-BSD trolls that have come out of the woodwork of late. He also asked some questions which I thought would be interesting to other people as well.

> I recently discovered your blog, and was fascinated by the amount of posts you have managed to write. I tend to obsess over making all posts perfect, which is really counter productive.

Thanks! I know all too well the paralysis of perfectionism. I'd wanted to talk about my 8-bit Commodore computers for years but worried that I wasn't qualified, and that I wouldn't have all the right details. In the end writing is something I do for fun, and I'd rather have some ideas out there than none.

> I noticed that the search feature on the archive page doesn't work, 
> seems to be blocked by CSP.

Fixed... I think! Thanks for the heads up. Turns out I had the CSP header referring to `*.duckduckgo.com`, which turned out not to be a catch-all for the root domain as well.

> I was also wondering how you deal with images, I noticed they are not part of the git repository.

I don't think I've ever talked about this before. I know I'm supposed to be using OrionVM's Object Store or Amazon S3, or a CDN, or `$kitchen_sink`, but I just use nginx to load balance assets across a few VMs in a basic round robin configuration. Ideally these would be sitting in different regions, and use something like GeoIP to match people to the nearest server. But this works for my purposes, and I don't like overcomplicating things.

Thomas also has a [great site](https://cavelab.dev/wiki/Main_Page) you should check out. He's also [migrating to a blog](https://blog.cavelab.dev/), which you can [subscribe to](https://blog.cavelab.dev/index.xml).

