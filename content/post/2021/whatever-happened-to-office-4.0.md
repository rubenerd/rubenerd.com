---
title: "Whatever happened to Office 4.0?"
date: "2021-07-18T10:06:22+10:00"
abstract: "Answering a question from my 6-year old self!"
thumb: "https://rubenerd.com/files/2021/office-40@1x.png"
year: "2021"
category: Software
tag:
- 1990s
- disks
- office-suites
- microsoft
- microsoft-office
- nostalgia
- retrocomputing
location: Sydney
---
I have strange interests and hangups, as those of you who've read this blog for any amount of time could attest. Recently I've been getting back into classic DOS and Windows 95 computers again while I wait for a few Commodore 128 parts. Installing Microsoft Office 4.3 Professional lead me to wonder... what happened to Office 4.0? Did it even exist? Was it a Windows NT 3.1 affair and had no prior releases in 4.x? Did they bump all the version numbers to match?

The family computer from my childhood got whatever volume-licenced kit my dad was able to get from work. For years that was Office 4.3 on a stack of 3.5-inch floppy disks, with the distributor's name *LASER* emblazoned on each sticker. It was my job after prep (kindergarten in Victoria... that state is also weird) to load each disk, one by one, into the family computer to install it each time. I still hear the seeking drive heads from those two dozen disks in my dreams.

<p><img src="https://rubenerd.com/files/2021/office-40@1x.png" srcset="https://rubenerd.com/files/2021/office-40@1x.png 1x, https://rubenerd.com/files/2021/office-40@2x.png 2x" alt="Office 4.0 and Windows for Workgroups installed in a QEMU VM" style="width:500px; height:250px;" /></p>

I knew about Office 4.2 Standard from reading PC Magazines my dad would bring home. This version was released without the Access database, though both had Microsoft's Mail client. If I recall correctly, this was the first time Microsoft split out "Standard" and "Professional" SKUs to target different markets, something their marketing and sales departments would crank up to 11 by the time Vista and their three million versions were released.

This lockdown weekend I decided to go back to the source and do some reading to figure out if Office 4.0 existed. The 9th of November 1993 issue of PC Magazine indeed [had an advertisement for it](https://books.google.com.au/books?id=NGX-Fo9YqzcC&pg=PA193)! It also included this line:

> If you act now, you'll also automatically get Microsoft Excel 5.0 and PowerPoint 4.0 upgrades as soon as they're released.

This raised an eyebrow. If those versions are coming *after* the release of Office 4.0, were they really shipped with the same versions of Excel and PowerPoint as Office 3.0? Did they really just bundle a new version of Word and sell the entire suite again?

<p><img src="https://rubenerd.com/files/2021/office-40-ad@1x.jpg" srcset="https://rubenerd.com/files/2021/office-40-ad@1x.jpg 1x, https://rubenerd.com/files/2021/office-40-ad@2x.jpg 2x" alt="Advertisement showing Office 4.0 Standard" style="width:500px; height:269px;" /></p>

PC Magazine's Robin Raskin [wrote this in her Pipeline column](https://books.google.com.au/books?id=NGX-Fo9YqzcC&lpg=PA193) in November 1993:

> Microsoft beats IBM by offering not one, but two coupons. The first copies of the $750 [!! –ed] Microsoft Office will include Word for Windows 6.0, Excel 4.0, PowerPoint 3.0, and Microsoft Mail. There will also be two coupons on the box: one for Excel 5.0, which will be ready momentarily, and one for PowerPoint 4.0, which will ship early next year.

This has a whiff of antitrust, doesn't it? If your competitors at Lotus or WordPerfect are beating you in features and performance, say you've got a new version coming out too, and in the meantime lock them in!

I wonder what the cause of the delay was? Did they put all their eggs into Word for Windows 6, or were Excel and PowerPoint simply not developed in time? Compatibility, OLE 2.0, and consistent interfaces were the hallmark features of the release, so maybe the Excel and PowerPoint developers ran out of time to implement them.

Now it makes sense why there was an Office 4.2 and 4.3. These versions came with Excel 5.0 and PowerPoint 4.0 that the original version of Office 4.0 was *supposed* to ship with. This leaves the question of what happened to Office 4.1, though scant online information suggests this *was* skipped.

I found a copy of 4.0 from... sources, and quickly installed it in my QEMU DOS VM. Sure enough, there were the Office 3.0 versions of PowerPoint and Excel, right alongside the newer version of Word. So weird.
