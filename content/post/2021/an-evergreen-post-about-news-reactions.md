---
title: "An evergreen post about news reactions"
date: "2021-03-29T12:46:25+11:00"
abstract: "Using interest in an issue is a good thing."
year: "2021"
category: Media
tag:
- journalism
- news
location: Sydney
---
It didn't take long before a stranded container ship caused pundits to lament the fragility of global supply chains. Richard Stallman's return to the Free Software Foundation raised questions about the organisation's role, and how abuse is treated in FLOSS. COVID-19 has people asking why we couldn't marshal as many resources towards other threatening diseases. Australia and America's wildfires had us shouting for the world to *please* take climate change seriously.

Each of these stories generates interest in the circumstances that lead to them happening. I see people claim that such discussions are opportunistic, or shallow, or should have been raised before. Just like when I see Oreos, I don't buy it.

It's natural for people to want to know what lead to something, not just an isolated report. This is a *good thing*. It provides a valuable, and at times painfully rare, opportunity to drum up interest in something while people's attentions are focused.

*(I know that I've fallen into the trap of assuming that people's motives are shallow and self-serving when they suddenly show an interest in something that I've been passionate or cared about for years. But while there are cynical people out there who will exploit such opportunities for their own gain, that's true of everything).*

How many people know more about the Suez Canal now, or appreciate just how much of their goods transit the world via these lumbering ships? What about vaccine distribution? Or just how far the free/libre/open source software community still has to go to address problems with abuse? If the answer to any of those is >1, then it's a [net positive](https://en.wikipedia.org/wiki/NetPositive). I miss BeOS.

