---
title: "Finally buying a Compaq spaceship"
date: "2021-07-21T15:31:22+10:00"
abstract: "Fulfilling another childhood nostalgia trip. Yes it’s pointless, but that’s not the point."
thumb: "https://rubenerd.com/files/2021/presario-spaceship@1x.jpg"
year: "2021"
category: Hardware
tag:
- compaq
- compaq-spaceship
location: Sydney
---
My [Compaq Presario spaceship post](https://rubenerd.com/compaq-presario-5510-spaceship/) from 2013 rates among the most visited in the history of this blog. Many of you seemed to enjoy me wandering down nostalgia lane, figuring out which Compaq tower all my friends had when I was a kid in the late 1990s, and that I secretly wanted. Due in no small part to what my childhood self thought was a passing resemblence to a spaceship.

I wrote at the time:

> Still, it stuck with me in my head all this time, and now I must locate one and purchase it, if only to satify that little kid in me. If you couldn’t have stuff as a kid, you can get it when you’re older, right? And I could replace the internals with a modern board and use it as a file server. This is called justifying frivolousness.

<p><img src="https://rubenerd.com/files/2021/presario-spaceship@1x.jpg" srcset="https://rubenerd.com/files/2021/presario-spaceship@1x.jpg 1x, https://rubenerd.com/files/2021/presario-spaceship@2x.jpg 2x" alt="" style="width:180px; height:240px; float:right; margin:0 0 1em 2em;" /></p>

*Here's the thing.* Don't judge me, but I may or may not have just purchased one from the US, almost a decade later. If I did, it was in excellent physical condition, though it's missing many of its internal components. That's fine though, I intend to use it as a case for something else.

Compaq computers were notorious for their proprietary connectors and case layouts, so I'm certain to run into problems shoehorning an old 80486 DX AT board, or a modern MiniITX into it. But we also have 3D printers now, and I've been learning how to sketch up standoffs. The bigger question will be if the back of the case needs cutting; I hope not.

As always, I expect this to be a work in progress. But I'll post about it here :).
