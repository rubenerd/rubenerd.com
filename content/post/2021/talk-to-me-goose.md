---
title: "Talk to me… goose?"
date: "2021-01-28T08:49:11+11:00"
abstract: "But they hear me and my Maverick™ ways."
year: "2021"
category: Internet
tag:
- marketing
- spam
location: Sydney
---
I've developed a tolerance for marketing speak, but this email I got during leave was entirely new to me:

> Talk to me Goose… about your `$AWARD` nomonation

Goose?

> We hear you Maverick. You asked, we listened. We extended the nomination deadline for `$AWARD` until February 8. Submit your nomination now and recognize the rising IT leaders driving digital innovation in your organization.

I'm flattered they recognised my Maverick&trade; rugged individuality that breaks through barriers and disrupts synergistic paradigms to leverage digital transformation. How that makes me a goose puzzles me though.

Anyone know what that's about? *(inb4 Untitled Geese)*.
