---
title: "Chick Corea ♡"
date: "2021-02-13T10:11:46+11:00"
abstract: "One of the all-time greats. His was my first concert, with Gary Burton in 2007. Rest in peace."
thumb: "https://rubenerd.com/files/2021/yt-NGJ2AaIVGl8@1x.jpg"
year: "2021"
category: Media
tag:
- jazz
- music
- rip
location: Sydney
---
Who was your first concert? Mine was Chick Corea and Gary Burton when we lived briefly in Malaysia in 2007. They were heading down to a jazz festival in Singapore, but stopped by Kuala Lumpur to do a one night show at the Dewan Filharmonik at the base of the twin towers. My dad and I almost missed them; I think we were driving back from the supermarket and heard an ad on the radio.

It's still my favourite performance I've ever been to; only Bebel Gilberto and Sonny Rollins have came close. The audience was predominantly a mix of caucasian and Japanese expats, but they clearly had a local Malaysian fanbase too. I heard a few Singaporeans during intermission talking about how they'd driven up just to see them.

They did a mix of their extensive repertoires with such flair, energy, and even a little cheekiness. I'd memorised so many of their recordings, so it was a thrill to hear their reinterpretation perfuming together with Gary's vibes and Chick's unique piano style, in acoustic form this time.

I had the ticket stub stuck to the lid of my laptop for years, still in disbelief that I heard them perform. I ended up taking it off and putting in a folder of my treasured memories when the ink started to fade. It's still there.

<p><a href="https://www.youtube.com/watch?v=NGJ2AaIVGl8" title="Play Return To Forever: Chick Corea, Stanley Clarke, Al Di Meola, Lenny White - 43 Jazzaldia Festival"><img src="https://rubenerd.com/files/2021/yt-NGJ2AaIVGl8@1x.jpg" srcset="https://rubenerd.com/files/2021/yt-NGJ2AaIVGl8@1x.jpg 1x, https://rubenerd.com/files/2021/yt-NGJ2AaIVGl8@2x.jpg 2x" alt="Play Return To Forever: Chick Corea, Stanley Clarke, Al Di Meola, Lenny White - 43 Jazzaldia Festival" style="width:500px;height:281px;" /></a></p>

I grew up with Chick's music, and his circle of musical inspirations and associate acts. My dad especially got me into jazz fusion, so our *Return to Forever* records and CDs were in regular rotation. They were in all our family mixtapes we took on trips.

Few musical acts have had as much of an impact on my life as Chick. His music was one of the few bright lights in my teens, and got me through difficult times. Heck, [I even wrote](https://rubenerd.com/summer-night-live-chick-coreas-akoustic-band/) some of his Wikipedia articles!

[Henry Rasussen wrote the best eulogy](https://www.abc.net.au/jazz/features/specials/remembering-chick-corea/13148190) I've read. Chick was one of the greats. I'm humbled that I got to see him.

