---
title: "Saying goodbye to my Commodore 64"
date: "2021-03-21T17:16:58+11:00"
abstract: "Investigating some strange aftermarket addons before sending this onto Josh from The Geekorium"
thumb: "https://rubenerd.com/files/2021/goodbye-commodore64-front@1x.jpg"
year: "2021"
category: Thoughts
tag:
- commodore
- commodore-64
- josh-nunn
location: Sydney
---
This is a bittersweet post, but it has a good ending. Today I bid farewell to a piece of Commodore kit I had since I was seventeen. She's off to live with Josh Nunn of [The Geekorium](https://www.the.geekorium.com.au/) and [Mastodon](https://aus.social/@Screenbeard) to help him troubleshoot another C64 he's been trying to restore for a while.

I first got into 1980s tech back when I was in high school in the 2000s. It's wild to think that the time between those decades has elapsed *again* now that we're in the 2020s. I was lucky that I had parents who always encouraged and supported my esoteric hobbies, so one year for my birthday they gave me an small eBay budget to spend on vintage tech. Commodore stuff is trendy and expensive again now, but it was going for bargain basement prices in 2004.

<p><img src="https://rubenerd.com/files/2021/goodbye-commodore64-front@1x.jpg" srcset="https://rubenerd.com/files/2021/goodbye-commodore64-front@1x.jpg 1x, https://rubenerd.com/files/2021/goodbye-commodore64-front@2x.jpg 2x" alt="Photo showing the front of my old Commodore C64 alongside my C16 and Plus/4." style="width:500px; height:333px;" /></p>

My Commodore 16 and Plus/4 came from someone in the United States, but I also had my sights on a combo from a gentleman in Australia. He was selling a Commodore datasette (tape drive), a 1541 disk drive, and his breadbin Commodore 64. He was selling the lot even cheaper because the C64 had been heavily modified, and he didn't know how to test it. I suspect it was an estate sale.  

The first thing I noticed was how many additional switches and ports had been added to the machine. This machine was *used* by someone serious, which I appreciate. On the top was a red button with a plastic shroud to prevent it being accidentally pressed, and next to it a red metal flip switch. The right-hand side included an aftermarket fuse caddy, and on the left was what looked like an RCA connector with a (+) sign.

<p><img src="https://rubenerd.com/files/2021/goodbye-commodore64-addons@1x.jpg" srcset="https://rubenerd.com/files/2021/goodbye-commodore64-addons@1x.jpg 1x, https://rubenerd.com/files/2021/goodbye-commodore64-addons@2x.jpg 2x" alt="Closeup photos showing the red button, toggle switch, RCA jack, and an external fuse." style="width:500px" /></p>

I figured out what most of these things did over time, but I was too nervous to poke around inside. Even if I did, I wouldn't know what to look for. Now that I have electrical experience troubleshooting and repairing my Commodore 128, I finally had the guts to open up this machine and see what was done.

The breadbin Commodore 64, like my beloved C16 you can see next to it in the first photo, pivots opens with three screws along the front of the case, and three plastic hinges at the rear. The C128 is my favourite 8-bit machine, but there's no question the C64 is easier to work on!

After unclipping and moving the RF foil/cardboard shield aside, I immediately saw how the fuse was wired up. Two thin leads were run from the internal fuse clips, which ran to a fuse caddy that protrudes from the side of the case. The chap who used this machine presumably lived in an area prone to power surges, and got tired of opening the case up to replace the fuse! It *works*, but that exposed wiring makes me antsy; Josh should probably insulate it with tape if he wants to keep it.

<p><img src="https://rubenerd.com/files/2021/goodbye-commodore64-fuse@1x.jpg" srcset="https://rubenerd.com/files/2021/goodbye-commodore64-fuse@1x.jpg 1x, https://rubenerd.com/files/2021/goodbye-commodore64-fuse@2x.jpg 2x" alt="Photo showing the wiring of the external fuse to the internal fuse clips." style="width:500px" /></p>

The switches on top are more interesting, pictured below. The left button is wired with white cables to pins on the user port. It's a reset switch if I had to guess, to save you detaching and re-attaching carts like a VICMODEM that are causing problems. The right toggle switch is connected to an aftermarket ROM attached above the factory MOS ROM. The circular, white label says **1/4/93**, which I'd guess was the install date. It's been a while since I've had a working C64 power supply, but I remember this toggled JiffyDOS. I honestly never really used it; in part because it disabled the datasette. The switch on the case let you toggle it on and off.

And as for that RCA jack on the left with the (+) sticker, it's connected with a sinister-looking orange and black cable to pin two on the datasette port, which I now know from troubleshooting the C128 connects to the 5V rail. This was likely added to power an external peripheral or cart; thesedays you'd use that for something like an SD2IEC to read SD or MMC cards. I'm not sure what people in the early to mid-1990s were using.

<p><img src="https://rubenerd.com/files/2021/goodbye-commodore64-cables@1x.jpg" srcset="https://rubenerd.com/files/2021/goodbye-commodore64-cables@1x.jpg 1x, https://rubenerd.com/files/2021/goodbye-commodore64-cables@2x.jpg 2x" alt="Final photo showing the JiffyDOS IC on top of the standard Commodore ROM, and wires going to the RCA jack." style="width:500px; height:333px;" /></p>

This has been fascinating, but it's time to bid her farewell. I have good reason to; Clara and I have limited apartment space, and I want her to be able to have space for her sewing machine and cosplay stuff. The Commodore 128 Josh sent me has a near-perfect C64 mode, so it makes sense to use that machine for both. It also feels right in a karmic sense to send this C64 to him in return :).

In the future I *might* try and source a 64C, the gorgeous later revision of the 64 that would match my [C128 and 1571](https://rubenerd.com/my-commodore-1571-drive-arrived/). But for now, I think this is the best possible outcome for this machine, and to thank Josh for his generosity. [Check out his blog](https://www.the.geekorium.com.au/) if you want to read his adventures with Commodore hardware too.

    LOAD"GOODBYE",8
