---
title: "Humans don’t have ECC memory"
date: "2021-09-09T08:09:35+10:00"
abstract: "What's my ATM PIN again?! With comment from Mike on Mastodon."
year: "2021"
category: Thoughts
tag:
- memory
- psychology
location: Sydney
---
We've all had that experience where we forget our ATM PIN, or another access code, despite having used it for years. It's unsettling, frustrating, and can be embarrassing if the lapse happens in public.

The same thing happened to me earlier in the week with my password manager. I sat there for a solid ten minutes unable to remember how to unlock the damned thing, before deciding that going for a walk was a more productive use of my time. I wanted to help someone solve an issue they were having, but I had to get back to them later.

I've had lapses where I forget my signature (though I so rarely use that now I understand it a bit more), how to spell my middle name, and the code to access a storage locker which has resulted in alarms sounding. Years ago I even forgot which finger I'd used for biometrics at a data centre, and a security guard had to come rescue me and file a report. Fun!

[Mike from Mastodon](https://social.chinwag.org/@mike/106895157003671302) and [Bremen Saki](https://bremensaki.com/blog/) hits the nail on the head, emphasis added:

> At some point last year I temporarily lost the ability to tie my shoelaces for most of a day after I **accidentally thought too hard about it**. Had to forget that I'd forgotten before I could do it again.

*That's the key!* So much of this is committed to muscle memory, I feel like as soon as we have to think about it too much, it evaporates.

Back when I was a bad person and used the same password everywhere, I had that string of gibberish committed to my fingers. I didn't realise it until I got a new keyboard with a slightly different layout (the backtick is always a culprit) where that muscle memory didn't produce the right password. As soon as I had to start thinking what the password was, it was gone.

As Mike said, our brains are *weird*.

