---
title: "Many of my software icons are green"
date: "2021-01-17T10:35:13+11:00"
abstract: "This was a fun, and ENTIRELY pointless exercise."
thumb: "https://rubenerd.com/files/2021/green-icons.png"
year: "2021"
category: Software
tag:
- libreoffice
- macos
- minecraft
- scummvm
- vim
location: Sydney
---
*[Speaking of colours!](https://rubenerd.com/random-article-the-circus-building/ "Random article: The Circus Building")* Most OSs tend to favour blue in their icons and interfaces, presumably because it's the world's favourite colour, and it's perceived as calming. But I recently had occasion to look through my Applications folder and noticed green slowly encroaching on blue's space:

<p><img src="https://rubenerd.com/files/2021/green-icons.png" alt="Icons for FaceTime, KeePassXC, LibreOffice Calc, Line, MacVim, Excel, Minecraft, and ScummVM" style="width:494px;" /></p>

It reminds me of those silly Twitter colour wars in 2007, back when that platform was fun. And speaking of keeping things fun: the inclusion of an application here says nothing about whether I want to use it or not, so please don't email me. It won't stop the regular stream of rude (and therefore counterproductive) messages I get now and immediately delete, but the rest of you can at least admire my optimism!

I also love that there's still sufficient differences in their shapes and design that one can pick them out at a glance. The trend in interfaces towards uniform squares is *such* a bad idea for accessibility; it's one of many, many reasons I'm holding off on Big Sur.

Colours are fun! I miss having chats about them with my mum.

