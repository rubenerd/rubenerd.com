---
title: "Cord cutters, and the age of streaming"
date: "2021-02-18T08:01:52+11:00"
abstract: "How many of these services does any one person need?"
year: "2021"
category: Anime
tag:
- streaming
location: Sydney
---
Cable TV never had the ubiquity in Australia like it did in Singapore or the United States. But I'm familiar enough with the concept of "cord cutters", or people cancelling their services in an act of defiance against the machine.

I've heard a few cited reasons. Customers were frustrated with the steep cost, especially given you still had to sit through commercials like free-to-air. The business model didn't permit *à la carting* specific channels, so you were stuck scrolling through junk you didn't want. People were worried during the Mr Orange era that a portion of their subscription compensated news channels that disseminated <del>misinformation</del> lies. And then there's the poor customer service that's emotionally scarred enough people that it's a trope itself.

Torrents were used as an early replacement&mdash;and are yet to be beaten for technical efficiency&mdash;but streaming services are now seen as the savior. There was a brief period of time when media companies and consumers seemed to be on the same page, which I'm sure the former took as a challenge.

Today there are dozens of steaming platforms, with more launching all the time with flashy ads and eyerolls from the public. These dilute the individual value and utility of any one platform, and drives people back to questionably-legal distribution. What's the motivation for customers to subscribe to yet another service for a single show?

Dave Winer [recently discovered](http://scripting.com/2021/02/16.html#a174955) that going back to cable saved him money compared to the multiple streaming services he had before. I'm not surprised.

Clara and I recently cancelled all but one of our services, because outside anime we only watch independent producers now. I say this instead of "YouTubers" because that just happens to be where they are at the moment. Whatever we paid for the other services now get funneled into Patreon, which I suppose is the *à la carte* option.

