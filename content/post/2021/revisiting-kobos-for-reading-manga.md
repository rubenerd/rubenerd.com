---
title: "Revisiting Kobos for reading manga"
date: "2021-10-10T08:12:59+10:00"
abstract: "The new Sage has the same large screen (and price) as the Forma, but can now take notes."
thumb: "https://rubenerd.com/files/2021/kobo-sage@1x.jpg"
year: "2021"
category: Anime
tag:
- ebooks
- kobo
- manga
location: Sydney
---
A year ago I discussed the prospect of using a [Kobo Forma as a manga reader](https://rubenerd.com/the-kobo-forma-as-a-manga-reader/). I stole some of Clara's physical manga and realised the Forma was 90% the size, as opposed to my old Kindle Paperwhite and most other ebook readers that are too small to see the art and text clearly.

I've since got an iPad for work, and have been trying it out for reading books. It works, but I long for an eink display, especially in the evenings when my eyes and other body parts are tired.

Kobo are now taking preorders for their [Sage](https://au.kobobooks.com/collections/ereaders/products/kobo-sage) ebook reader. It's slightly heavier than the Forma, but has the same sized screen and is about the same price. The biggest external difference seems to be its support for pens for notetaking, and a new type of cover.

<p><img src="https://rubenerd.com/files/2021/kobo-sage@1x.jpg" srcset="https://rubenerd.com/files/2021/kobo-sage@1x.jpg 1x, https://rubenerd.com/files/2021/kobo-sage@2x.jpg 2x" alt="Press image of the Sage." style="width:420px" /></p>

What I'm most interested in is its 32 GiB of internal storage. The Forma had 8 GiB, which was already more than what my Kindle had. But that extra capacity would let me put an entire manga series on there to read through, as opposed to dowloading a few at a time which becomes tedious.

The price is on the steep side, but I want to read more with fewer distractions, and this seems to be the best device for my interests. I'll set the money aside and see what reviewers say once the first pre-orders are out.

