---
title: "Pop Up Parade’s Quintessential Quintuplets"
date: "2021-02-20T23:30:43+11:00"
abstract: "They’ve only released two thus far, but they’re adorable!"
thumb: "https://rubenerd.com/files/2021/popupparade-qq@1x.jpg"
year: "2021"
category: Anime
tag:
- figures
- quintessential-quintuplets
location: Sydney
---
I'm always bemused by how much of a networking conversation I end up having with people using almost nothing but abbreviations; people without industry experience would think we're daft or speaking in tongues. I just realised non-weebs would see the post title here likely think the same thing.

*Good Smile Company* have done such a great job making anime figure collecting a more approachable hobby with their *Pop Up Parade* line, both in cost and in the smaller scale of their sculpts, while still maintaining decent quality. Their figs are much closer to premium figure manufacturers than so-called game prize companies like Sega, which is impressive considering even the latter have hugely improved over the last few years.

<p><img src="https://rubenerd.com/files/2021/popupparade-qq@1x.jpg" srcset="https://rubenerd.com/files/2021/popupparade-qq@1x.jpg 1x, https://rubenerd.com/files/2021/popupparade-qq@2x.jpg 2x" alt="" style="width:500px" /></p>

I've only seen Nakano Itsuki (left) and Nakano Yotsuba (right) be announced so far from the *Quintessential Quintuplets* franchise, but they're adorable! The sculptors and painters captured the personalities of the characters so well with their expressions and poses. Itsuki's blatant lack of glasses is my only minor quibble.

This is problematic for us, considering I've amassed even more Commodore hardware to write my [C128 series](https://rubenerd.com/tag/commodore-128-series/) and therefore have even less apartment space. Maybe Itsuki could stand on my 1541 *shifty eyes*.
