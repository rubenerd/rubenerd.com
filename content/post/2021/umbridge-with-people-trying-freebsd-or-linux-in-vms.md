---
title: "Umbrage at trying FreeBSD and Linux in VMs"
date: "2021-05-10T10:14:43+10:00"
abstract: "Uh oh, is that a VirtualBox agent running!? A discussion of counterproductive and technically incorrect takes."
year: "2021"
category: Software
tag:
- bsd
- community
- debian
- freebsd
- gatekeeping
- illumos
- linux
- netbsd
- social-networks
location: Sydney
---
Picture this: you're browsing through a forum, mailing list, or social network, and someone posts that they're trying a BSD, illumos, Linux, or another operating system they're not familiar with. The community rallies around this convert, and offers words of encouragement and technical assistance.

All goes well, until an eagle-eyed reader chances upon something that causes them, to use a phrase popular in Singapore at the moment, *umbrage*. Buried in dmesg output, or a screenshot, is evidence that the OS the person installed is running in... hold your hats... *a virtual machine.* And boy do they let the poster know their disdain.

Gatekeeping of this nature is as rife as it is baffling. Not only is it counterproductive to mock people for testing a new OS in a VM, but the entire premise of this elitism is technically flawed. If you're going to be pedantic about something, at least take the time to be correct. *Think of the other pendants!*

I always read *pedant* as *peanut*. Maybe that's why so many people have allergies to them.

Open source software communities, especially those not associated with large corporate sponsors, don't have the luxury of big advertising budgets, training programmes, and social media campaigns. Someone coming to you, of their own volition, to learn about your OS is fantastic. Nurturing someone today could mean a contributor or advocate tomorrow. Driving them away because they had the audacity to share their experience in a VM is neither useful or nice.

Virtual machines are perfect for trying new OSs, precisely for their composability, ability to rollback learner mistakes without damage to the host system, and being able to leverage existing hardware. There's a reason VMs are popular in academia and education: they let you start with a clean slate and focus on picking up the fundamentals. I don't begrudge anyone for wanting to avoid the potential pitfalls of dual booting, especially if it's your primary or only machine. VMs are fun, convenient, and inexpensive.

But even leaving all that aside, the worst kept secret in IT today is that the **bulk of Unix-like OSs run in virtual machines!** They're in containers, cloud instances, VPSs, or even in on-premise hypervisors. A bhyve VM on FreeBSD, or a VMware VM on Workstation, or any number of other VMs, can even be migrated and booted elsewhere. Knowing how an OS will perform within a virtualised environment, from networking to memory use, is also valuable skill. How *does* your target database handle not having direct IO, for example? What other considerations, from performance to security, must you factor in?

Professionals use VMs for everything from building, testing, and production. People like me run the hypervisors to make it work!

Which leads us to the real reason why these delightful people try and take down newbies starting with a VM. They're seen as easy takedowns in the zero sum game world they see. You can't be a serious computator if you're running it in a VM on your laptop, right? Those people likely won't ever be swayed by facts and logic, so instead I'd propose that *serious* computators don't discriminate over such an arbitrary metric. They're bigger than that.

