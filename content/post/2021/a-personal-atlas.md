---
title: "A personal atlas"
date: "2021-04-25T09:57:59+10:00"
abstract: "He had one, to himself!"
year: "2021"
category: Software
tag:
- cd-roms
- geography
- mmx-machine
location: Sydney
---
I overheard this at a coffee shop this morning:

> Alex had an atlas, *to himself!*

Sounds great to me. I still have the frayed, tattered atlas my parents saved from the year I was born, complete with the crumbling Soviet Union and East Germany. It wasn't much longer before they were gone.

I might load up my DK World Reference Atlas multimedia CD-ROM this evening on my P1 machine and do some exploring.
 
