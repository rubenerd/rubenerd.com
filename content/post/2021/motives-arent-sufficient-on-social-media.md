---
title: "Motives aren’t sufficient on social media"
date: "2021-03-30T09:23:43+11:00"
abstract: "Even the most well-intentioned advice can backfire."
year: "2021"
category: Thoughts
tag:
- being-a-human-being
- social-media
location: Sydney
---
I'm trying to get better at calling out bad behaviour when I see it online. Over the weekend it was from an Australian Twitterer who was being harassed for pushing back on the idea that turmeric could cure her chronic illness.

I've been in the *exact* situation. Years ago I had someone suggest vegetable juice would be just as effective as the chemotherapy my mum was undergoing at the time, without the side effects and expense. I may have, not so subtly, told the person where they could shove the bottle.

In the case of the turmeric incident, they argued that they were acting in good faith, and the resulting replies were mean spirited. I soon questioned that, given the patronising tone of their messages and use of clichés like "triggered". But even if it was entirely innocuous, how did they think such unsolicited and irrational medical advice would be received by someone in pain?

Good faith, motives, and intentions don't trump sensitivity or situational awareness; what we would call *reading the room*. I'd go as far as to say you can't propose something in good faith if you *haven't* taken the time to understand the recipient's circumstances. This really should be *Being a Functional Human Being 101*.

We soon realised they were just a bully, though. After mocking people for blocking them&mdash;widely considered by trolls to be an admission of defeat for some reason&mdash;they soon deleted their tweets and... blocked us. Like all bullies, they can dish it but cower when called out.

But there's still a lesson here, and it's something I've slipped up on too. For those who *do* have the best interests of others at heart, sometimes you just need to let people vent. They're stressed, tired, and dealing with bad circumstances. Even the most well-intentioned advice can be received poorly, especially in limited text forums like Twitter where we don't have other social cues or signals. The best responses I got during my mum's illness were "Mate, that's so shit" not "Have you considered...".

