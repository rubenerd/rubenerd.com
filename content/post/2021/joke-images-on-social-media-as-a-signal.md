---
title: "Joke images on social media as a signal"
date: "2021-10-27T09:55:21+10:00"
abstract: "A map of the Mediterranean superimposed over the US generated… comments about climate change hoaxes."
year: "2021"
category: Internet
tag:
- climate-change
- environment
- social-media
- sociology
- united-states
location: Sydney
---
A map of the United States [flooded by rising sea levels](https://twitter.com/mrj880/status/1453017745369534469) went viral again recently, with the usual people replying that climate change is a hoax. The fact the caption was a joke, and that the [floodplain](https://www.kottke.org/17/10/the-mediterranean-sea-of-america) was the Mediterranean Sea was, unsurprisingly, lost on them. You'd think the massive, Italian boot would have kicked them into thought, even if they didn't recognise any other part of it.

Funny story, I tripped on a boot and hit my head on a banister once, and even *I* could make out what it was.

It's easy to see how someone predisposed to believing in conspiracy theories would also lack basic knowledge in other areas, such as geography. It's also as much a failure of society not preparing people for our complicated world as it is a lack of curiosity or interest on their part. Critical thinking isn't a skill you can learn by rote, despite the best efforts of certain educational systems!

But more broadly, it demonstrates how knee-jerk social media can be, and how we're *all* susceptible to it if we're not careful. All it takes is for a story or graphic to push our buttons or conform to our own views, and we'll respond without thinking as critically as we should. I've fallen for it, and I'll bet you have too at some point.

I'm starting to think this is another way social media is corrosive, at least in its current form. It doesn't elevate thinking, it conforms to our baser instincts. It's the inevitable outcome when platforms measure their success based on "customer engagement", a phrase that I thought referred to marriage.

