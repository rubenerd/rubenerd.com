---
title: "The joy of music collections"
date: "2021-01-11T17:11:08+11:00"
abstract: "Nuts to streaming services!"
thumb: "https://rubenerd.com/files/2021/a1890734739_16.jpg"
year: "2021"
category: Media
tag:
- esther-golton
- music
- music-monday
location: Sydney
---
I've mentioned on previous [Music Mondays](https://rubenerd.com/tag/music-monday) that I've gone back to maintaining a music collection, in lieu of using streaming services. It's been such a rewarding exercise, and the tea ceremony of maintaining and using a proper Hi-Fi system has been a lot of fun! Streaming services made music disposable, but doing this made it tangible and real again.

Clara and I have all the music we want between LPs, cassettes, CDs, Minidiscs, and a large file server. We jump to sites like YouTube if we want to preview music, then we buy it from Bandcamp, ZDigital, or Apple iTunes... while dodging the latter's incessant ads for their own streaming platform.

Drive space is cheap, and you get so many benefits:

* Artists you care about make *orders of magnitude* more when you buy their album compared to streaming. This is especially critical during Covid times where concerts are difficult.

* Nobody can revoke your music if you buy it DRM-free. You licenced the right to listen to it by getting it on physical media or as a file, and that's the end of the transaction.

* You have the final say on metadata. A specific streaming service had so many mistakes in album text, or had missing translations, or the wrong cover art. Maintaining a collection lets your type 1 personality shine!

* Potentially higher quality. Streaming services are pretty good now, and I don't have equipment good enough to tell the difference between most high-bitrate AAC files and ALAC/FLAC. I also play pre-recorded, type 1 cassettes and LPs! But well-sampled albums like Esther Golton's *[Aurora Borealis](https://esthergolton.bandcamp.com/album/aurora-borealis-conversations-with-alaskas-northern-lights)* absolutely *sparkle* with that extra headroom.

I'll be doing some more reviews of music software and tools for organising in the coming months.

