---
title: "Tim King gives us permission to not give a fuck"
date: "2021-09-16T13:12:52+10:00"
abstract: "Stop. Right now."
year: "2021"
category: Thoughts
tag:
- mental-health
- psychology
location: Sydney
---
I'm [passing this on](https://hellotimking.medium.com/stop-giving-so-many-fucks-f2a7b6cd13) for any of you who need it. Please read it.

> Today I’m here to give you permission to stop giving a fuck about every little thing. Unless of course your life goal is to have a heart attack at forty-five, you need to stop.
> 
> Seriously, stop. Right now.
> 
> I’ll wait.
> 
> …
> 
> …
> 
> …
> 
> …
> 
> Have you done it? Stopped giving a fuck?
> 
> I don’t believe you, but that’s ok. The psychology of this isn’t that I believe you, it’s that you believe you.

