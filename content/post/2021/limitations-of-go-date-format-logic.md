---
title: "Limitations of Go date format logic"
date: "2021-10-26T08:17:14+10:00"
abstract: "Who thought 02-01-2006 was a good idea!?"
year: "2021"
category: Software
tag:
- internationalisation
- golang
- programming
location: Sydney
---
Go's date formats are a special beast, as everyone else already realised a decade ago. I'm not a Go developer, but I do use a few [devops](https://www.hashicorp.com/) and [blogging](https://gohugo.io) tools written in it, and as such I work regularly with its unconventional date representation.

Unlike other languages, Go works against a reference date which you refactor to get the format you're after:

    Mon Jan 2 15:04:05 -0700 MST 2006

I can see the appeal of this rather than an alphabet soup like `strftime`, even though I committed most of that to memory by now. It's more <abbr title="what you see is what you get">WYSIWYG</abbr>, and easier to visualise in something like a template.

I always wanted to know why this *specific* date, for reasons I'll get to in a moment. It lead me to this [old Hacker News thread](https://news.ycombinator.com/item?id=8944578) from 2015, and a quote from the documentation that shows this ascending mnemonic:

    Mon Jan 2 15:04:05 -0700 MST 2006
    0   1   2  3  4  5              6

But there are some assumptions and <abbr title="internationalisation">i17n</abbr> drawbacks here. Non-American readers would have immediately spotted a problem: we generally don't put months first. Most of us would write long dates like this:

    Mon 2 Jan 15:04:05 -0700 MST 2006
    0   2 1    3  4  5              6

It's not a big deal, the mnemonic holds even though it doesn't have the elegant ascending numbers anymore. But it's a bit weird.

The far bigger problem comes when you want to validate short dates. Americans use `MM-DD-YYYY`, whereas the rest of us use `DD-MM-YYYY`. At a glance, will this render as the first of February, or the second of January?

    02-01-2021

Using a day (02) and a month (01) that can be easily confused is, to use the Shakespearean term, a *huge pain in the asp*. I can't tell you how many times I've looked at a template and had to do a double-take on the date, or troubleshooting why the months on a page are all wrong.

Conventional wisdom in documentation is to always use a day greater than 12 to remove global ambiguity. The order of this date looks wrong to me, but at least it's mutually intelligible:

    01-27-2021

We'd all be using `YYYY-MM-DD` in an ideal world, but alas we're all stuck in meatspace. I'll continue to work around this, but it's disappointing that such a high-profile tool would make this difficult to satisfy a mnemonic that doesn't even hold that well outside the US.

