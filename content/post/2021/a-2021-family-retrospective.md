---
title: "A 2021 retrospective"
date: "2021-12-23T09:32:33+11:00"
abstract: "Some positives made up for an otherwise rubbish year!"
thumb: "https://rubenerd.com/files/2021/xmas2021@1x.jpg"
year: "2021"
category: Thoughts
tag:
- family
- personal
location: Sydney
---
Today's the annual anniversary that I lost my mum Debra to the big C. I still think about her every day, and "what would she do or think?" continues to be a valuable moral lens to for everything I do. I put on an old Bread album she used to listen to, and the [lyrics to the second song](https://www.youtube.com/watch?v=_21cZe2eO_I "Everything I Own") did it:

<blockquote>
<p>You sheltered me from harm<br />
Kept me warm, kept me warm<br />
You gave my life to me<br />
Set me free, set me free<br />
The finest years I ever knew<br />
Were all the years I had with you</p>
<p>And I would give anything I own<br />
Give up my life, my heart, my home<br />
I would give everything I own<br />
Just to have you back again</p>
</blockquote>

This year has hit harder than most. Maybe it's the two years of Covid fatigue we're all feeling, or the fact we just lost her brother and my dear uncle a few weeks ago as well. I'm also still wallowing in post-operation pain and self-absorbed blues! My ongoing anxiety issues, which I was supposed to be taking leave for, has also deteriorated further with all this extra stuff.

<p><img src="https://rubenerd.com/files/2021/xmas2021@1x.jpg" srcset="https://rubenerd.com/files/2021/xmas2021@1x.jpg 1x, https://rubenerd.com/files/2021/xmas2021@2x.jpg 2x" alt="Our tree this year :)." style="width:500px; height:333px;" /></p>

But it's also important to *[do the needful](https://www.theguardian.com/commentisfree/2016/jan/04/indian-english-phrases-indianisms-english-americanisms-vocabulary)*, which in this case is to remind ourselves of the good.

My dad's health continues to improve after his heart surgery and wrist injuries. My little sister got married to someone who I'm honoured and happy to be introducing into the family. I don't know how we would have got through any of this stuff with my dad's new partner being a responsible, caring voice. The relationship I have with Clara is the best thing that's ever happened to me, whether it be our Friday night Minecraft sessions, choosing boba after dinner, sharing in Japanese culture, or reading on the couch while the lights on our tiny Christmas tree sparkle. I've never had a massive circle of friends, but the ones I do have are special, too. Even work is doing well, and I enjoy spending time with my colleagues, even if virtually.

Dare I say as well, this year has also proved to be a turning point for this blog. The outpouring of fun comments and regular feedback has bowled me over, despite my dips into neurotic posts like this! That was almost mistyped as *erotic*, and would have taken this in a whole other direction. You all have your choice of writers, so I appreciate you giving me a bit of your time every day to let me ramble.

The world continues to face uncertainty, and I'm sure we're all feeling a bit on edge. But I'll wager dimes to doughnuts we'll get through this. I only have a handful of American dimes, and I'd prefer a bagel, but this sentence has nineteen words.

I hope you're going well. If there's anything I've learned from the last two years, its that we need to take care of ourselves, too. ♡

