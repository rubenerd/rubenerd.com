---
title: "Privacy and trust as a luxury good"
date: "2021-10-19T14:01:47+10:00"
abstract: "Karl Bode’s latest article for Techdirt was brilliant."
year: "2021"
category: Hardware
tag:
- phones
- privacy
location: Sydney
---
Speaking of [ethics in tech](https://rubenerd.com/tech-firms-adjudicating-whats-appropriate/), Karl Bode's latest [latest Techdirt article](https://www.techdirt.com/articles/20211012/07235747734/many-digital-divide-solutions-make-privacy-trust-luxury-option.shtml) was absolutely brilliant. I hadn't considered this:

>  We've noted a few times how privacy is slowly but surely becoming a luxury good. Take low-cost cellular phones, for example. They may now be available for dirt cheap, but the devices are among the very first to treat consumer privacy and security as effectively unworthy of consideration at that price point. So at the same time we're patting ourselves on the back for "bridging the digital divide," we're creating a new paradigm whereby privacy and security are something placed out of reach for those who can't afford it. 
