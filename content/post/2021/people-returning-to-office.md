---
title: "People returning to offices"
date: "2021-08-26T16:19:53+10:00"
abstract: "Bosses who worry about flexibility will lose their staff."
year: "2021"
category: Thoughts
tag:
- news
- work
location: Sydney
---
I saw [this news fly by](https://www.wsj.com/articles/remote-work-may-now-last-for-two-years-worrying-some-bosses-11629624605
) from the Wall Street Journal:

> With offices closed for nearly two years, will anyone want to come back?
Companies have repeatedly postponed their return-to-office dates, and some bosses fear it will get harder to draw workers back to their desks.
>
> The Delta variant changed companies’ return-to-office timelines. It’s also making bosses worry that the longer people stay at home, the more challenging it will be to bring them back.

Bosses who worry about flexibility will lose their staff.

The cat is out of the bag; a large part of the population has worked remote just as effectively before; some even moreso. That was a lot of semicolons. Two of them.
