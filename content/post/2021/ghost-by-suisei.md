---
title: "GHOST / 星街すいせい by Suisei"
date: "2021-05-10T15:43:41+10:00"
abstract: "My favourite Japanese Hololive character’s latest song!"
thumb: "https://rubenerd.com/files/2021/yt-IKKar5SS29E@1x.jpg"
year: "2021"
category: Media
tag:
- hololive
- music
- music-monday
- suisei
location: Sydney
---
[Suisei](https://www.youtube.com/channel/UC5CwaMl1eIgY8h02uZw7u8A) is my favourite Japanese Hololive character, and this song she released a month ago has been looping in my head ever since. May it find it into your ears and mind this [Music Monday](https://rubenerd.com/tag/music-monday/) too :).

<p><a href="https://www.youtube.com/watch?v=IKKar5SS29E" title="Play GHOST / 星街すいせい"><img src="https://rubenerd.com/files/2021/yt-IKKar5SS29E@1x.jpg" srcset="https://rubenerd.com/files/2021/yt-IKKar5SS29E@1x.jpg 1x, https://rubenerd.com/files/2021/yt-IKKar5SS29E@2x.jpg 2x" alt="Play GHOST / 星街すいせい" style="width:500px;height:281px;" /></a></p>

