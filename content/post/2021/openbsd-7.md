---
title: "OpenBSD 7.0"
date: "2021-10-18T14:09:35+10:00"
abstract: "Congratulations to the team! Your OS gives me hope for the industry."
thumb: "https://rubenerd.com/files/2021/StarryPointers@1x.jpg"
year: "2021"
category: Software
tag:
- bsd
- openbsd
location: Sydney
---
Congratulations to the OpenBSD developers for the [51st release](https://www.openbsd.org/70.html) of their operating system! Download and verify [their bootable images here](https://www.openbsd.org/faq/faq4.html#Download) and give it a spin. I'm going to this weekend, and not just because Natasha Allegri drew an omage to one of my favourite paintings for it :).

<p><img src="https://rubenerd.com/files/2021/StarryPointers@1x.jpg" srcset="https://rubenerd.com/files/2021/StarryPointers@1x.jpg 1x, https://rubenerd.com/files/2021/StarryPointers@2x.jpg 2x" alt="Starry Pointers by Natasha Allegri. Brilliant!" style="width:500px" /></p>

OpenBSD continues to provide a viable, secure, performant, and efficient alternative to some other open source OSs you may have heard of. Even if you don't run it directly, we all benefit from their tools and their pursuit of engineering excellence. Their efforts give me hope for the industry.
