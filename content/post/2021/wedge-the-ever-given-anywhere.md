---
title: "Wedge the Ever Given anywhere"
date: "2021-03-30T09:00:58+11:00"
abstract: "I put it in Singapore and Sydney just because!"
thumb: "https://rubenerd.com/files/2021/evergiven-singapore.jpg"
year: "2021"
category: Internet
tag:
- memes
location: Sydney
---
Speaking of evergreens, Garrett Dash Nelson made an *excellent* website where you can [wedge the Ever Given container ship somewhere else](https://evergiven-everywhere.glitch.me/)! Here she is stuck in Singapore's Marina Bay:

<p><img src="https://rubenerd.com/files/2021/evergiven-singapore.jpg" alt="View of the Ever Given container ship overlayed on a sattelite photo of Marina Bay in Singapore." style="width:500px; height:333px;" /></p>

And what she'd look like blocking off the ferry terminal across from the Opera House in Sydney:

<p><img src="https://rubenerd.com/files/2021/evergiven-sydney.jpg" alt="" style="width:500px; height:333px;" /></p>

