---
title: "Merging power buttons on appliances"
date: "2021-01-05T13:58:00+11:00"
abstract: "Make it as simple as possible, but not simpler."
year: "2021"
category: Hardware
tag:
- appliances
- design
- ergonomics
---
Have you noticed how there's a drive to reduce the number of buttons on things? It's presumably to reduce cost, and make interfaces more minimal, which we perceive as simpler. [But it's not](https://quoteinvestigator.com/2011/05/13/einstein-simple/).

One extreme is offloading functions to a partner app on a phone. We're beyond smart bulbs now; you can get Wi-Fi coffee machines, shoe shiners, even routers... thank you, I'll be here all week. Most, if not all, of the app's functions could easily be added to the aforementioned appliances with buttons, dials, switches, levers, or any number of mechanical interfaces with which we're all familiar and have had exhaustive usability and accessibility testing. The apps to control these appliances are often terribly designed, poorly maintained, difficult to use, and needlessy tie an appliance's lifespan to a control interface that can be revoked at the whim of the app developer, or your phone being replaced. I've often thought claims of "planned obscelecence" were dubious at best, considering science and technology are consatntly evolving; but this coupling is an example I'm willing to concede.

Another is the merging of functions into a smaller set of controls. Some of these are mechanically elegant, like merging a fan speed dial with power. Not spinning at all is a valid opposite state to full blast, and also happens to be the off position. Others make less sense, or are far less intuitive. A kettle we have at work takes five short button presses and a long press to go from cold to hot water, some of which are on unmarked buttons. Choosing a preset and activating the kettle could be done with two presses, assuming each preset had a button rather than scrolling through a list each time.

Merging mechanical functions also doesn't cleanly translate into buttons. Our Kambrook tower fan has a button to cycle through power modes, the last of which is off. Whereas a dial is easy to turn and click to off, this fan requires three button presses, with a pause each time, to turn it off each morning. I've considered pulling it from the power socket, or using a timer, or adding a switch along the power cord, but it shouldn't have to come to that! 

Physical controls come with their own limitations. They're prone to breaking because they're mechanical devices. They can't ever hope to represent the breadth and depth of features a touch screen with near-infinte customisation and an API can achieve. But how many controls does my kettle really need?

I suppose I'd have a leg to stand on, but my 1980s Pioneer amplifier shamelessly has a slider instead of a dial, presumably to look more modern. A dial would make way more sense, but would look far less retro awesome. Hey, at least it doesn't have a proprietary streaming service added that <del>can</del> will be disabled before the device itself stops working.
