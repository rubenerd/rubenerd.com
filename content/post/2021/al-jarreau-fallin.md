---
title: "Al Jarreau: Fallin’"
date: "2021-06-28T11:01:42+10:00"
abstract: "Today’s Music Monday is a gem I only recently discovered."
thumb: "https://rubenerd.com/files/2012/yt-ZgbrdFca6-g@1x.jpg"
year: "2021"
category: Media
tag:
- al-jarreau
- jazz
- music
- music-monday
location: Sydney
---
Today's [Music Monday](https://rubenerd.com/tag/music-monday/) is a gem I only recently discovered. The imagery and lyrics are so beautiful.

> Misty water colors;   
> Raining trees and flowers. ♫

I still kick myself that I missed the opportunity to see him perform with George Benson in Singapore so I could <em>study for an exam</em>. The mark was, as they say, *totally* not worth it!

<p><a href="https://www.youtube.com/watch?v=Z3ceGJMX2Uo" title="Play Fallin'"><img src="https://rubenerd.com/files/2012/yt-Z3ceGJMX2Uo@1x.jpg" srcset="https://rubenerd.com/files/2012/yt-Z3ceGJMX2Uo@1x.jpg 1x, https://rubenerd.com/files/2012/yt-Z3ceGJMX2Uo@2x.jpg 2x" alt="Play Fallin'" style="width:500px;height:281px;" /></a></p>

