---
title: "How do you use your computer?"
date: "2021-04-11T22:19:06+10:00"
abstract: "If you live in the real world, you’d be telling Microsoft all of the above?"
year: "2021"
category: Software
tag:
- games
- microsoft
location: Sydney
---
You know when you're filling out a survey, and it asks you to pigeonhole either something about yourself, or how you use something? I'm always struck by the limited range of choices, and the assumption of atomicity.

Albacore on Twitter uncovered a new settings screen in a Windows 10 build that will let you [report how you use your computer](https://twitter.com/thebookisclosed/status/1372238675338231810). You can choose from:

* Gaming
* Family
* Creativity
* Schoolwork
* Entertainment
* Business

Granted you can choose more than one, but I'm still wondering how much useful data they'd get from this. Some have dedicated *work* or *study* machines, but barring stringent corporate networks I'd be surprised if people aren't using their computers for *all* of this. The same would apply for phones, and especially during remote COVID work.

*Schoolwork* is also an interesting one, and not just because I didn't realise that even existed as a single word. I wonder why they didn't go with *education*?

It also raises the question about why they'd be interested. For feeding into Telemetry to target you with more ads? Optimisations?

No, I suspect the real reason is so when Microsoft completes the migration of Windows from the Windows NT kernel to Linux, they'll release flavours for each use case. This is merely market research to justify this to management. Yes, that's it.

