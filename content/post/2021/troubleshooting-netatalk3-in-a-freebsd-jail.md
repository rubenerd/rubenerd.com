---
title: "Troubleshooting netatalk3 in a FreeBSD jail"
date: "2021-08-08T14:49:23+10:00"
abstract: "Use “afp listen”, because it can’t detect the virtual jail interfaces."
thumb: "https://rubenerd.com/files/2020/beastie@2x.png"
year: "2021"
category: Software
tag:
- bsd
- file-sharing
- freebsd
- netatalk
- netbsd
- troubleshooting
location: Sydney
---
[Netatalk3](http://netatalk.sourceforge.net/) is a file server for exporting storage to Macs. Samba has long been considered its replacement, but to this day Netatalk still handles file labels and other Mac-specific metadata more reliably and with greater performance. One day I'll properly try replicating this in Samba.

I installed it in a new FreeBSD jail:

	# pkg install net/netatalk3

Then configured it largely the same as I [did on FreeBSD in 2014](a-basic-freebsd-nas-with-netatalk3), and on [NetBSD last year](https://rubenerd.com/netatalk3-mac-file-sharing-on-netbsd/). Only this time, all the Macs in the house refused to talk to it.

I tail'd `/var/log/daemon.log` in the jail and was inundated with **[afpd(8)](https://www.freebsd.org/cgi/man.cgi?query=afpd)** spam:

	netatalk[34758]: Restarting 'afpd' (restarts: 7)
	afpd[42393]: dsi_tcp_init(*): getaddrinfo: Name does not resolve
	afpd[42393]: No suitable network config for TCP socket
	afpd[42393]: no suitable network address found, use "afp listen" or "afp interfaces"
	afpd[42393]: main: no servers configured

I followed the error's advice and added the IP address of the jail to the `[Global]` section of my `/usr/local/etc/afp.conf` file:

	afp listen = <IP Address>

It worked, and I was able to log in, as shown in the logs:

	afpd[88524]: Netatalk AFP/TCP listening on <IP Address>:548

I don't recall ever having to add a specific interface or IP address to an `afp.conf` file before on a FreeBSD or NetBSD host. My hunch is it has something to do with the jail environment, and `dsi_tcp_init` not being able to autodetect or initialise the jail's virtual network interface. Please [correct me](https://rubenerd.com/about/#contact) if you have more details!

