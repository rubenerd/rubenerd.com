---
title: "State and mess in our computers"
date: "2021-06-03T08:56:40+10:00"
abstract: "Discipline, supporting infrastructure, and making it easier to do the right thing."
year: "2021"
category: Software
tag:
- browsers
- mess
- psychology
- gtd
location: Sydney
---
My post about [FreeBSD on my tiny Panasonic laptop](https://rubenerd.com/freebsd-13-on-the-panasonic-cf-rz6/) reminded me of a pitfall of suspend/resume. It's great for power consumption on desktops, and saves time when you're carrying a laptop around, but it encourages you to accumulate more stuff, such as:

* open browser tabs
* open terminal tabs
* unsaved text editor buffers

I've talked before about seeing colleagues and friends open their laptops to *hundreds* of open things, and being amazed that their brains don't collapse form cognitive overload, to say nothing about the RAM and swap space on their machines! My machines are lightly-loaded by comparison, but there's room for improvement.

It plays into an overall issue with *state*. The dream would be that I could immediately pick up where I left off if my laptop were to be stolen or broken. These get in the way:

* I try and keep meticulous file system trees for documents, but I still end up with random stuff in Downloads and Desktop folders.

* Git repos for work projects are always checked out and committed, but I can go for days without doing that for my blog, for example.

* I can stand up servers with Ansible and scripts down to the exact packages I need. Software on my desktop seems to be an ever-moving target.

These are *somewhat* mitigated by having automated backups, but that only encourages a pile of junk somewhere else as well. Information you can't find or use is just data.

It's getting a bit silly, so I'm thinking through what I can do:

* **Discipline!** I thought about applying David Allen's *Getting Things Done* task system to other things, like open files and tabs:

	* **Do:** It's open for a reason. Action it.
	* **Defer:** Save the file, bookmark on Pinboard, make a #TODO.
	* **Delegate:** To a script to process without me thinking.
	* **Delete:** Just close the damn thing!

* **Supporting infrastructure.** The wisest advice I ever got for backups is that it *has to be automatic*, otherwise you won't do it. Maybe I need to have a Perl script to watch specific folders for new files and organise it for me. Like Hazel, but cross-platform.

* **Make it easier to do the right thing**. This overlaps with above. Using pkgsrc instead of multiple other package managers has helped. Having fewer folders and a less complicated tree structure for projects makes it easier to regularly use them. Tree Style Tabs for Firefox enables hoarding even more open tabs, so going back to a limited horizontal list means I *have* to keep the list short.

I'll report how successful (or not!) I've been with this.

