---
title: "Perceptions of remote work"
date: "2021-04-14T10:11:18+10:00"
abstract: "Sense of belonging, the human connection, and feedback from HR managers."
year: "2021"
category: Thoughts
tag:
- covid-19
- remote-work
location: Sydney
---
I was about to pipe some unsolicited marketing spam to the bin, but a shockingly-insightful tidbit caught my eye:

> In the hybrid workplace, technology can be a double-edged sword, enabling a sense of belonging and human connection while also creating overwhelm and fatigue. Getting the balance right is critical.

That's... true. They summarised what I've written for paragraphs here over the last year into two sentences.

> Independent research revealed that while 78% of HR managers believe they have done a good job of keeping in touch with home workers about their Health & Safety, just 28% of remote workers agreed.

I'd be interested to see what that *independent research* was, but it definitely wouldn't surprise me based on anecdotal evidence among people I know.

