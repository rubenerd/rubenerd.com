---
title: "Afghanistan in 2021"
date: "2021-08-17T08:12:04+10:00"
abstract: "It sucks. Can’t think of much else to say."
year: "2021"
category: Thoughts
tag:
- politics
location: Sydney
---
This is a half-baked post, but I'm at a loss how to articulate my thoughts. Nothing I've said feels worthy or respectful enough.

This has been a rough month for my family and I, but then I see the news of the Taliban retaking Kabul yesterday and my heart sinks further. We have so completely failed the people of this country, with seemingly even the best minds unable to come up with a way out.

Even if we assume the best of intentions, staying there wasn't a good idea. Leaving wasn't a good idea. Returning there wouldn't be either. Unfortunately, the Universe doesn't owe us a solution. 

Nobody on this planet should be left behind. All of our great global challenges today, from education, disease, nutrition, and climate change, will require input and funding from everyone. We have the resources, we need the willpower, creative thinking, and maybe something other than sending people in with bullets. We can't afford not to. 
