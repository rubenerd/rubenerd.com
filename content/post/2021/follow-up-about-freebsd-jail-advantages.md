---
title: "Follow-up about FreeBSD jail advantages"
date: "2021-01-16T08:24:10+11:00"
abstract: "Discussing security, ZFS integration, and package management."
thumb: "https://rubenerd.com/files/uploads/icon.freebsd-logo.png"
year: "2021"
category: Software
tag:
- bsd
- freebsd
- jails
location: Sydney
---
The first Ukrainian reader to email me&mdash;Pryvit!&mdash;asked me to clarify what I meant here in my [introductory post about FreeBSD jails](https://rubenerd.com/starting-with-freebsd-jails/)\:

> Much has been written about the potential security benefits of isolating processes, but I shamelessly use them foremost to keep my ports clean. My Plex jail has everything for video encoding, the Minecraft jail is the only one with a JDK runtime. Conflicts aren’t an issue, they’re simple to update without unintentionally breaking something else, and it keeps individual attack surfaces small.

I'll admit I ran a lot of justifications together into a single paragraph because I wanted to get to configuring the jails themselves. They're also, by and large, not specific to FreeBSD's flavour of containerisation, though I still think it's easily the most elegant implementation. Sometimes the simplest solution *really is* the best one.

But let me address something first. People were surprised at how cautious I was qualifying security as a "potential" benefit. There's a stubborn industry perception that containers negate or reduce the need for other standard security practices; that somehow wrapping a service in a container intrinsically renders them immune to security problems. The Docker and Kubernetes crowds weren't the first to push this, but they're the highest-profile. Inappropriate application of these technologies have created entire new classes of security issues, and it hasn't helped that the tools themselves have also introduced critical bugs.

But back to FreeBSD! Jails&mdash;and process isolating tools in general&mdash;are great for security if configured with the same maturity and care as a new host, such as user management, permissions, configuring services to only listen on specific ports, system updates, and so on. A jailed environment is a great additional layer of security, but it absolutely does not absolve a system administrator's responsibilities elsewhere. If I sound like I'm being overly cautious or critical, it's only from reading these glowing posts for many years and being concerned at the direction the industry is taking!

With that in mind, jails let you do some cool stuff. They let you expose only the portions of the host's file system the service is supposed to see, which can even include hiding specific binaries or system tools. They hide their processes from other jails, but you can also hide them from the host itself. Jails let you create specific users, groups, and permissions unique to that environment. And you can build the base system and packages specific for what a service needs, while removing unnecessary components that might introduce bugs or security problems. More on that in a moment.

FreeBSD jails run atop ZFS add a whole suite of extra tools that make building and maintaining jails easier, *without having to learn an entire extra set of over-engineered tools or complicated configuration!* You can snapshot and roll back a jail's running state before a large upgrade or change. You can create a base jail and clone off it to create new ones. You can even backup and ship them elsewhere.

Which leads us to my comment about "keeping ports clean". I still subscribe to Einstein's Paraphrased Law of Package Management: install the fewest packages you need for a system, and no fewer (this is why I don't consider tools like bash cross-platform, despite the hand-wringing that generated from a specific person on Twitter before my current break from the platform). Jails let you install only the packages you need for a specific service. Poudriere itself even uses jails to build packages for you for this reason! It helps with potential package conflicts, and makes managing packages easier.
