---
title: "Happy International Webloggers Day 2021"
date: "2021-06-15T19:35:01+10:00"
abstract: "This month, I’m thanking Antranig Vartanian!"
thumb: "https://rubenerd.com/files/2020/rss-buttons-crop.png"
year: "2021"
category: Internet
tag:
- antranig-vartanian
- blogging
- weblog
location: Sydney
---
No, it's not official in any formal capacity. Yes, it's also called Blogging Day, Blog Day, International Day of Bloggers, and various other things. I almost forgot all of them, but [Antranig Vartanian](https://antranigv.am/) reminded me on Mastodon with kind words that will will not go unchallenged!

Back in 2002, Dave Winer defined the `blogChannelModule` namespace, which can still be seen declared on a few RSS feeds:

	<rss version="2.0" 
	  xmlns:blogChannel="http://backend.userland.com/blogChannelModule">

The URL no longer resolves, like much of RSS's early infrastructure. But fortunately [Feedforall](https://www.feedforall.com/blogchannel.htm) documented some of the elements. My favourite is **blink**:

	<blogChannel:blink>
	The url of a blog the blog publisher is promoting

Antranig's [English RSS feed](https://antranigv.am/weblog_en/index.xml) will now be what [my own RSS feed](https://rubenerd.com/feed/) **blinks** at for the rest of June. This might become a regular, recurring feature here where I showcase someone I admire and respect.

It's my hope that someone, somewhere, has implemented a UI for this feature in their reader or aggragator, and see this link pop up. A side project I'm working on right now sure does! But discussion of that is for another time.

