---
title: "We should rename Australian things"
date: "2021-11-01T15:50:45+10:00"
abstract: "While wandering along the Price Henry Cliff Walk..."
thumb: "https://rubenerd.com/files/2021/prince-henry-cliff-walk@1x.jpg"
year: "2021"
category: Travel
tag:
- australia
- blue-mountains
- new-south-wales
- south-australia
location: Sydney
---
I'm on personal leave for a couple of weeks, and find myself in the [Blue Mountains](https://www.nationalparks.nsw.gov.au/visit-a-park/parks/blue-mountains-national-park) once more. It's such a beautiful part of the world, and feels so far from inner Sydney despite only being a few hours away by train. It's become a sanctuary for me during Covid times.

Wandering along one of the walking trails near Echo Point and the Three Sisters (an unintentional band name if ever I heard one), I was struck by this sign foregrounding one of the most jaw-dropping vistas in the world:

<p><img src="https://rubenerd.com/files/2021/prince-henry-cliff-walk@1x.jpg" srcset="https://rubenerd.com/files/2021/prince-henry-cliff-walk@1x.jpg 1x, https://rubenerd.com/files/2021/prince-henry-cliff-walk@2x.jpg 2x" alt="Sign saying The Prince Henry Cliff Walk." style="width:500px; height:333px;" /></p>

Is that... is that the best we can do? A World Heritage site with rock formations dating back further than any other continent on Earth, and it's called... the Prince Henry Cliff Walk?

Nowhere will beat Jersey City, New Jersey for me; a city named for a state that took its name from an island. But with all the New things in the United States, and British things in Canada, I think Australia has them beat with our uninspired names. The bulk of our state and territories merely describe where they are, what they are, or have ties to the British monarchy:

* Australian Capital Territory
* Northern Territory
* New South Wales
* Queensland
* South Australia (which isn't even accurate)
* Victoria
* Western Australia

That leaves us with Jervis Bay, Norfolk Island, and Tasmania? I guess they're *better*, but still a far cry from what we could have. And don't get me started on our [state flags](https://en.wikipedia.org/wiki/List_of_Australian_flags#States_and_territories).

What strikes me as ridiculous, aside from the inanity of it all, is that we have so many amazing Native Australian languages here, all rich with unique words and pre-existing place names we could be using. Instead we're stuck, much like our constitution and head of state, with glorified hand-me-downs that are ill-fitting and smell of inbred mothballs. I'm not sure how a mothball can be inbred, but have you *seen* some European royalty?

I'd say we can do better, but clearly we can't at present. Let's change this!

