---
title: "A FreeBSD Puppet refresher by Romain Tartière"
date: "2021-07-28T08:38:13+10:00"
abstract: "Thanks to BSDCan for continuing to make these resources available."
thumb: "https://rubenerd.com/files/2020/beastie@2x.png"
year: "2021"
category: Software
tag:
- bsd
- freebsd
- automation
- puppet
location: Sydney
---
I mostly live in Ansible land for automation, but I was looking for a quick refresher on Puppet. I didn't have to look far; FreeBSD's Romain Tartière [did a talk at BSDCan 2018](https://www.bsdcan.org/2018/schedule/events/930.en.html), and his [slides](https://www.bsdcan.org/2018/schedule/attachments/462_slides.pdf) are still available. This made my morning!

Thanks to BSDCan for continuing to make these resources available.

