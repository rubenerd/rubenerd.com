---
title: "Rubenerd’s Law of Food"
date: "2021-06-23T08:58:00+10:00"
abstract: "Any comment about substitute or mock food will result in equal and opposite obtuseness."
year: "2021"
category: Thoughts
tag:
- food
- rubenerds-laws
location: Sydney
---
My post yesterday about the [health benefits of decaf coffee](coffee-decreasing-the-risk-of-cld/) reminded me of this new law I've been mulling.

Back in 2017 I proposed [Rubenerd's Law of Headlines](https://rubenerd.com/considered-harmful-is-considered-harmful/), a play on Betteridge's which states that the answer to a headline ending with a question mark is *no*. I proposed that if an article is titled with “Technology foo considered harmful”, there’s an unharmful, legitimate use for it.

I'm now shamelessly extending Newton's Third Law of Motion, and Cunningham's Law which states that the best way to get to the correct answer on the Internet isn't to ask a question, but to post the wrong answer.

**Any comment about substitute or mock food will result in equal and opposite obtuseness.**

Examples include "I don't understand the point of decaf coffee, non-alcoholic beer, fake meat", etc. What they're *really* saying is "I can't relate to anyone's circumstances beyond my own". Or they can, but feign ignorance for Internet points.

