---
title: "If you have two loaves of bread, buy a CD-ROM"
date: "2021-05-06T22:55:04+10:00"
abstract: "Or sell one and buy a hyacinth."
year: "2021"
category: Thoughts
tag:
- quotes
- microsoft-bookshelf
- shopping
location: Sydney
---
For someone who spends so much time tinkering with vintage computer hardware, it surprises people that I don't do much gaming on them. My [Pentium MMX](https://rubenerd.com/tag/mmx-machine/) machine I built as kid is mostly used as a multimedia CD-ROM catalogue thesedays. Unlike the World Wide Web that ultimately supplanted them, these discs are fascinating static snapshots in time, as well as being fun to read and explore.

Microsoft Bookshelf 1991 always has some gems. Here's an unattributed early Persian saying as quoted in *Bartlett's Familiar Quotations*:

> If you have two loaves of bread, sell one and buy a hyacinth.

🌼

