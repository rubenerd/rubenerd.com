---
title: "Quote from @Aral on debugging"
date: "2021-03-16T08:00:17+11:00"
abstract: "Debugging is the subtle art of reconciling what you think you asked a computer to do with what you actually asked the computer to do."
year: "2021"
category: Internet
tag:
- aral
- software
- troubleshooting
location: Sydney
---
Darshak Parikh quoted Aral Balkan of [Small Tech](https://small-tech.org) on [Mastodon](https://im-in.space/@dubiousdisc/105892425964493946):

> Debugging is the subtle art of reconciling what you think you asked a computer to do with what you actually asked the computer to do.

This also applies to hardware. It's no use routing packets to an unplugged switch, or asking a Z80 to compute something with a dead short. I may have done both in the last day.

