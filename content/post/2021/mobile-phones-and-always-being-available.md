---
title: "Mobile phones, and always being available"
date: "2021-08-08T09:54:13+10:00"
abstract: "At least Introverts are a resourceful bunch."
year: "2021"
category: Thoughts
tag:
- business-blaze
- mobile-phones
- simon-whistler
- sociology
location: Sydney
---
One of the more [recent Business Blazes](https://www.youtube.com/watch?v=c42mAGOfGJ4) dropped this truth bomb at the start:

> I sometimes feel the invention of the mobile phone was a bad move for society, as we now seem to think everyone in our contacts list should really be readily accessible around the clock. And we get irritated if they don't respond to our messages in a timely fashion.

I'm sure mobile phones have been a net positive for the world, at least on balance. There are people for whom their smartphone is their computer, their primary business tool, and their only way to keep in contact with people. The alternative for so many, especially in the developing world, would be... nothing.

I watched a great documentary a few years ago about an entrepreneur in Indonesia who'd collect everyone's mobile phone in his town, then for a small fee ride them on his motorbike for a few hours to the nearest phone tower. All the cached messages would be sent and received, and he'd deliver them back. It put a lot of my whinging into stark contrast.

Now, having said all that, even the most optimistic technologist would (or should!) readily admit that mobile phones have been a mixed blessing. Populations in the developed world treat them as disposable accessories to be refreshed annually with ever-diminishing returns and functional justification, generating millions of tons of e-waste laced with rare earths and other non-recyclable materials. 

Sociologically there's still the open question about mobile addiction, online bulling, extortionate mobile applications store practices, and what Cory Wong and Cody Fry so eloquently put in their song [Golden](https://www.youtube.com/watch?v=zqw6y61p328)\:

> Looking all around at the people I see;  
> Everybody's living the dream.   
> Starring in the show that we film on our phones;  
> Taking pictures for pretend magazines.

You could write entire books just on each of these points.

But back to Simon's point in Business Blaze, via his illustrious writer Danny *(AM I RIGHT, PETER!?)*. Mobile phones didn't just make it easier for us to reach out to other people, it made it easier for *others to always access us*. I don't think we've fully come to terms with the implications.

I'm part of the last generation (yay, millennials!) ever in human history that can remember a time *before* ubiquitous access was normalised. I had to arrange meetups with friends in advance as a kid, with set times and locations. I got a phone by the time I was 13, but it was unusual enough that MSN Messenger, ICQ, and email were still the only reliable ways to reach out to someone. I'm sure some of the Gen-X and Baby Boomers among you would even consider *those* luxurious. Early Seinfeld episodes literally used inaccessibility as a plot device and joke punchline... they would make no sense now.

Which comes back to the point about expectations. You're required to have one of these infernal devices on you at all times to participate in the modern world, and our society has codified a set of norms and expected behaviour around them. I don't think many of them are healthy.

Phone callers demand you drop everything you're doing and focus on them, which is *incredibly* invasive for a device that's on your person 24/7. Some of those calls may be required in emergency situations, but it's seeped out into every interaction. We're trained to let the phone keep ringing, because immediately rejecting the call is considered rude and a snub. But really, who are we kidding? And the only acceptable excuse for not answering a call, beyond bathroom activities or maybe even being asleep? *I was on another call*. At least with a landline you could claim you were elsewhere when a caller got your entertaining voicemail message.

Text messages, private DMs, and public social media mentions don't carry the same level of urgency, but the expectations remain. We've associated how much we value someone on how studious they are with their replies on these silly, artificial slabs. This disproportionately affects introverts for whom excessive social interactions are a drain and source of anxiety, but I'm sure its not healthy for anyone. Our social circles have become ever wider, but are they any deeper?

If there's any hope here, it's that introverts are a resourceful bunch, and will continue to find new and innovative ways to obfuscate and pretend communications are interrupted for reasons beyond their control... something we can all benefit from. *I'm sorry, my latest vaccine didn't have enough 5G antigens!*

