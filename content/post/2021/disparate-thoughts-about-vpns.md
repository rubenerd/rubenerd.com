---
title: "Disparate thoughts about VPNs"
date: "2021-10-17T09:34:43+10:00"
abstract: "A listpost about privacy, geoblocks, and other adventures."
year: "2021"
category: Internet
tag:
- geoblocks
- privacy
- security
location: Sydney
---
VPNs have been in the news again, which is a nice segue into a few things I've been thinking about them of late. Here's another [listpost](https://rubenerd.com/tag/listpost/)!

* Web companies informing us that they've detected a login from a new location (often due to use of a VPN) sounds like a great idea, but isn't it also a great spearfishing vector? *Hey, we detected you logged in on a site, better click (this fake URL) to verify.* If you'd man-in-the-middle'd someone's network, this would also be the best way to trigger someone to log into a site again so you could steal their credentials.

* Does every network technology have to be an abbreviation?

* I'm encouraged by the growing number of people questioning the simplistic advice that using a VPN solves all privacy and security concerns. People still think Private Mode in web browsers are private though, and on the flip side I'm worried that it'll discourage people from using VPNs at all. We *really* need to get better about educating people outside our infocomm bubble.

* VPN is only one letter away from being an acronym with one of the letters changed. Three letters changed, is a phrase with the same number of words.

* IPsec has done as much to hold back secured comms than it's protected. Emails disputing this will be redirected over an unsecured channel to a null modem cable connected to a SIP gateway serving a fax machine with a broken PDF export tool and toner cartridge.

* I'm surprised how often VPNs are openly advertised as ways to circumvent geoblocks. We all do it, but isn't it legally dubious and a terms of service violation? I'm also surprised more streaming platforms don't block the IP ranges of known VPN providers. Perhaps it's too much of a moving target, or the streaming platforms would rather keep you as a customer and look the other way than enforce arbitrary rules by their content providers.
