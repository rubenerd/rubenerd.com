---
title: "Comparing and contrasting"
date: "2021-06-02T12:35:46+10:00"
abstract: "Effective communication is about being understood."
year: "2021"
category: Thoughts
tag:
- communication
- english
- nostalgia
- school
- technical-writing
location: Sydney
---
Speaking of [school memories](https://rubenerd.com/speed-reading-advertisements/), I was listening to a podcast recently in which the host mentioned *comparing and contrasting* two things. Out of the blue, I was sitting in a mid-2000s high school science class looking over exam results, and having a conversation that's stuck with me since.

One of the questions asked us to *compare and contrast* renewable and fossil fuels (I think). I understood this to mean writing about **similarities** and **differences**, so I got full marks. But this was an international school in Singapore, and we had a large cohort of <a href="https://en.wiktionary.org/wiki/ESL" title="Wiktionary: English as a second language">ESL</a> students who didn't understand what was being asked. Some listed the benefits of both, others only talked about differences. At best, they got half marks.

Enough of the class got the question wrong that the teacher took time out to explain what *compare and contrast* meant, and what he was looking for. One of my more outspoken friends piped up that he didn't realise that it was an English class, and that the exam was testing phrasing and not technical understanding. I agreed.

The teacher smiled and said "life's not meant to be fair".

I learned three things that day. One, that he was right about fairness. Two, that I'd be spending more of my time after school helping friends study than I expected. This worked out well, because in exchange we'd all go get hawker food for dinner and they'd shout :).

But three, I sound like a broken record here: *effective communication is about being understood.* It's why I endeavour to be accurate *and* understandable when I do technical writing; it isn't just important for accessibility, or when talking with people learning English, it's professional courtesy.
