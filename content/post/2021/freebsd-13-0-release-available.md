---
title: "FreeBSD 13.0-RELEASE available!"
date: "2021-04-15T09:09:42+10:00"
abstract: "Grab it while it’s hot!"
thumb: "https://rubenerd.com/files/2020/beastie@2x.png"
year: "2021"
category: Software
tag:
- bsd
- freebsd
location: Sydney
---
<img src="https://rubenerd.com/files/2020/beastie@1x.png" srcset="https://rubenerd.com/files/2020/beastie@1x.png 1x, https://rubenerd.com/files/2020/beastie@2x.png 2x" alt="The BSD Daemon" style="width:192px; float:right; margin:0 0 1em 2em;" />

As I [said in February](https://rubenerd.com/freebsd-13-0-beta2-graphics-on-the-panasonic-cf-rz6/), I haven't been this stoked for a FreeBSD release in a while. The [announcement page](https://www.freebsd.org/releases/13.0R/announce/), [release notes](https://www.freebsd.org/releases/13.0R/relnotes/), and [errata](https://www.freebsd.org/releases/13.0R/errata/) list all the details. Thanks to everyone on the release engineering team.

You can download from the official mirrors, or John-Mark Gurney has also made [magnet links for torrents](https://wiki.freebsd.org/Torrents).

I'll be working at getting [OrionVM's](https://orionvm.com/) FreeBSD 13 template finalised in the next couple of weeks.

