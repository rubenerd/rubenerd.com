---
title: "Audioboom found and archived"
date: "2021-06-04T12:15:59+10:00"
abstract: "Remember that audio sharing site?"
year: "2021"
category: Media
tag:
- archiving
- audio
location: Sydney
---
I completely forgot about Audioboom. It was a popular audio uploading and sharing service back when Twitter was a site that only asked "what are you doing?" I used it from 2009 to 2012 according to my [profile](https://audioboom.com/rubenerd).

I wrote a [quick script](https://codeberg.org/rubenerd/lunchbox/src/branch/trunk/download-audioboom.sh) to download all my posts, and uploaded to [Archive.org](https://archive.org/details/Rubenerds_Audiobooms). You never know when these services will disappear, even if the value of what you put there was questionable! I might listen to them all again and shove into a Rubenerd Show at some point, there might be something silly/fun/nostalgic among the pointlessness.

Maybe I should upload something else to say where my current stuff is. That might be fun.

