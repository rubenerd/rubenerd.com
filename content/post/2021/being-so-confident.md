---
title: "Being so confident"
date: "2021-04-20T07:59:06+10:00"
abstract: "Windows 94 was better than 95, and Singapore is in China!"
year: "2021"
category: Thoughts
tag:
- kuala-lumpur
- malaysia
- people
- relationships
- singapore
location: Sydney
---
After my family migrated to Singapore in the 1990s, my dad's firm gave him an allowance to spend on trips back to Australia to visit relatives and friends he left behind by being transferred. It was literally called *hardship pay*; something we thought was funny given we liked living there. I love Australia, but the hope is there that one day I'll make it back with my own green card and eventually PR.

But I digress! We'd come back to Sydney each year and do the rounds. I can't remember how I was related to this one gentleman who came to our big family dinners, but he'd spout what could best be described as *confident nonsense*. He'd hear a topic, riff off it, then go into the weeds with concocted fantasy. My parents would later tell me he was infamous for it, but we'd smile and nod out of social obligation. Nobody likes flareups, especially with family involved.

I remember a few specific examples. Upon hearing that we lived in Singapore, he talked about all donations he made to help famine victims, and all the good work these charities were doing helping Singaporeans and other cities in China. Singapore wasn't undergoing a famine, nor was it in China. It's not even a "Chinese city", but that Western misconception is for another post.

Another example had him explaining electronics. My cousin James and I were the only ones in the family with the reputation of being "computer people". This didn't stop this gentleman loudly complaining that Windows 95 was no better than Windows 94 (a non-existent OS), and that his new mobile phone charged from radio waves faster than all ours thanks to an ingenious antenna modification (which is not how that works).

Australians and Kiwis take the piss like its a national sport, which at times gets us into trouble internationally. We have the driest sense of humour, and will outrightly say something factually ridiculous with a straight face. It's probably one of the biggest things that differentiates us from Canadians whom we otherwise have a lot in common, at least based on my experience.

The other charitable interpretation was that he was making broader points with hyperbole. Maybe he thought Windows 95 was overhyped, or that income inequality in Singapore was too high, or that Singapore's Indian and Malay populations were disadvantaged compared to the Chinese. These involved stretches so vast, I'm still recovering from tendon damage in my thighs.

No, this gentleman was someone else. He wanted to dominate the discussion and have people regard him as well-informed and intelligent. He sure achieved the former, though the latter meant I now only remember a caricature. I wonder if he's taken credit for an antimicrobial COVID tablet, or a revolutionary triangular tyre... *it has one fewer bump than square ones*. I don't think he ever knew we lived in Kuala Lumpur for a couple of years either, though I'm sure he'd have stories about South America to relay about that. Those Malaysians sure love their Chimichurri, but what about all the native French speakers?

We're told by self-help gurus that confidence is a positive attribute. It renders you more attractive, gets you further in business, inspires people, and reinforces your self-worth. But I can't tell you how relieved I am to have a discussion with someone in real life who *isn't*; or at least, not to the same extent. Confidence is only worth something with substance.

