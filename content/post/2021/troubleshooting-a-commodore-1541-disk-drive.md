---
title: "Troubleshooting a Commodore 1541 disk drive"
date: "2021-03-01T09:43:58+11:00"
abstract: "I fixed the sticky read/write head, but the stepper motor might have issues."
thumb: "https://rubenerd.com/files/2021/commodore-1541-drive@1x.jpg"
year: "2021"
category: Hardware
tag:
- commodore
- commodore-128
- commodore-128-series
- disk-drives
location: Sydney
---
Today in my ongoing [Commodore 128 series](https://rubenerd.com/tag/commodore-128-series/), I'm talking about my adventures trying&mdash;and unfortunately failing&mdash;to restore an original 1541 disk drive over the weekend. Hopefully I'll have a follow-up at some point.

The Commodore 1541 was a 5.25-inch floppy disk drive released to compliment the C64. Commodore had sold drives for its PET and VIC-20 computers, but this was arguably the first to achieve a significant install base. They were faster and easier than data cassettes, and eventually became the preferred way to distribute software and store files in most markets.

<p><img src="https://rubenerd.com/files/2021/commodore-1541-drive@1x.jpg" srcset="https://rubenerd.com/files/2021/commodore-1541-drive@1x.jpg 1x, https://rubenerd.com/files/2021/commodore-1541-drive@2x.jpg 2x" alt="" style="width:500px" /></p>

*(Ignore the partially disassembled Commodore 128; I'm cleaning and retr0brighting the keyboard!)*

Among its quirks, the last digit "1" on the front badge looks more like a pipe character! I assume this was done to maintain kerning, but it's always fun hearing the audible gasp from even veteran Commodore users when they realise they hadn't noticed this before :). It's also single-sided, but could use both sides of a disk if flipped and a notch punched into the opposing side.

The unit contained an entire computer with similar circuitry and ICs to the C64 itself. This freed the main computer from needing drive controllers, which kept costs down and meant (in theory) any type of drive could be added in the future. Unfortunately this meant the drives were physically larger and more expensive, and thanks to a bug in the original VIC implementation, were significantly slower than drives in the Apple II and others. Still better than tape, though!

I got my 1541 from eBay years ago for an abandoned Commodore project, so I decided I'd give it a try with my Commodore 128. I inserted a disk, plugged the drive into the cereal (heh) port of the Commodore 128, and powered both devices on. The 1541's green LED powered on, and the drive span for two seconds as expected.

Then I tried loading the contents of the disk:

    READY  
    LOAD"$",8

    SEARCHING FOR $
    ?FILE NOT FOUND ERROR

The sounds emanating from the drive were *horrifying*. It made the head-knocking sound for what sounded like an age, then flashed the red LED on and off continuously. Ejecting the disk made no difference.

I opened up the drive and marvelled at the complex logic board and mechanics. A couple of traces had been bypassed with patches on both sides; I'm not sure if this was done at the factory or someone else tried to fix this drive one before.

<p><img src="https://rubenerd.com/files/2021/commodore-1541-board@1x.jpg" srcset="https://rubenerd.com/files/2021/commodore-1541-board@1x.jpg 1x, https://rubenerd.com/files/2021/commodore-1541-board@2x.jpg 2x" alt="" style="width:500px" /></p>

I lifted the logic board off, and gently cleaned the drive head with a q-tip soaked in isopropyl alcohol. The drive head is under the top black plastic mechanism in the below photo:

<p><img src="https://rubenerd.com/files/2021/commodore-1541-mechanism@1x.jpg" srcset="https://rubenerd.com/files/2021/commodore-1541-mechanism@1x.jpg 1x, https://rubenerd.com/files/2021/commodore-1541-mechanism@2x.jpg 2x" alt="" style="width:500px" /></p>

The small metal circle to the top right is part of the pulley mechanism that transfers motion from the stepper motor to the drive head; [retro64 has a great diagram](http://retro64.altervista.org/blog/commodore-1541-disk-drive-maintenance-part-2-head-alignment/). I noticed that when I started the drive again, it would lurch slightly as though it was stuck, and the drive head only darted up and down the rails by a millimetre in either direction.

I added some lithium grease to the drive head rails, and a little to the metal wheels. All the parts move freely now and don't make nasty sounds, but the drive head still refuses to move.

Various forums and mailing lists suggest it could be a drive alignment issue, so I have a 1541 cart in transit just in case to test. Ray Carlsen's [excellent 1541 drive guide](http://www.zimmers.net/anonftp/pub/cbm/documents/repair/troubleshooting-1541.txt) also describes similar symptoms and suggests I check some of the ICs:

> UE4 LM311 COMPARITOR (READ): Drive powers up and resets normally. Spindle motor runs, stepper moves slightly, but "FILE NOT FOUND" error, and red LED flashes.
Check also UF3 and UF4.

I have a Commodore 1571 in transit at the moment, which supports burst mode on the 128, and drops to a 1541-compatible drive for my Plus/4 and C16. That said, if anyone has any suggestions about anything else I should try, please [ping me](https://rubenerd.com/about/#contact)! 

