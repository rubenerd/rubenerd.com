---
title: "When image positioning attacks"
date: "2021-05-09T11:08:39+10:00"
abstract: "I love when the universe randomly lines up like this."
thumb: "https://rubenerd.com/files/2021/fgo-wiki@2x.png"
year: "2021"
category: Anime
tag:
- fate
- fate-grand-order
location: Sydney
---
<p><img src="https://rubenerd.com/files/2021/fgo-wiki@2x.png" alt="Screenshot showing a thumbnail of Saber's upper body, with a thumbnail of Gil's lower body underneath." style="width:320px; height:154px;" /></p>

I saw these thumbnails on a Fate/Grand Order wiki last week. I adore when the universe randomly lines up like this!

