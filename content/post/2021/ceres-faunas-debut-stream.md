---
title: "Ceres Fauna’s debut stream #faunline"
date: "2021-08-23T08:20:23+10:00"
abstract: "We’re watching the latest Hololive English debuts one by one. Today is the keeper of nature… who got us GOOD."
thumb: "https://rubenerd.com/files/2021/yt-iICu7NcUK4o@1x.jpg"
year: "2021"
category: Anime
tag:
- hololive
- hololive-english
- video
- youtube
location: Sydney
---
Clara and I are *slowly* catching up with latest generation of [Hololive English](https://www.youtube.com/channel/UCotXwY6s8pWmuWd_snKYjhg) debuts, which we're watching entirely out of order. I say both of us, though of course *she* has seen them all already. Last night was for the HoloCouncil's keeper of nature, Ceres Fauna.

<p><a href="https://www.youtube.com/watch?v=iICu7NcUK4o" title="Play 【DEBUT STREAM】Mama Nature 🌿 #hololiveEnglish #holoCouncil"><img src="https://rubenerd.com/files/2021/yt-iICu7NcUK4o@1x.jpg" srcset="https://rubenerd.com/files/2021/yt-iICu7NcUK4o@1x.jpg 1x, https://rubenerd.com/files/2021/yt-iICu7NcUK4o@2x.jpg 2x" alt="Play 【DEBUT STREAM】Mama Nature 🌿 #hololiveEnglish #holoCouncil" style="width:500px;height:281px;" /></a></p>

I never thought I'd have to put a **spoiler warning** on a debut stream, but wow! She suckered us in good. The buildup, the subtle hints, then that mic drop with the lights and... why am I worried about spoling people, any of you interested in Hololive almost certainly know what the takeaway was by now. She owns us now... but she's Mother Nature, so that comes as no surprise.

I appreciated that she listens to bossa nova&mdash;the other good genre of music&mdash;and is interested in playing *Spore!* I never thought I'd see the day. And her omage to [Ina](https://rubenerd.com/im-in-inas-tentacult/) with that *WAH*... chef's kiss.
