---
title: "Hong Kong MTR station colours"
date: "2021-04-04T19:02:54+10:00"
abstract: "Being reminded of Tsim Sha Tsui from a cafe in Sydney."
thumb: "https://rubenerd.com/files/2021/photo-mine-tst-hk@1x.jpg"
year: "2021"
category: Travel
tag:
- colour
- design
- food
- hong-kong
- public-transport
- sydney
location: Sydney
---
There is *so much* I love and miss about Hong Kong. Clara and I were reminded of another reason when we got HK-style milk tea and coffee from Tea Square in the [Mandarin Centre](https://www.mandarincentre.com.au/) in Sydney. Take a look behind the cash register and you can see the colours from some of the MTR's famous stations.

<p><img src="https://rubenerd.com/files/2021/hk-mtr-chatswood@1x.jpg" srcset="https://rubenerd.com/files/2021/hk-mtr-chatswood@1x.jpg 1x, https://rubenerd.com/files/2021/hk-mtr-chatswood@2x.jpg 2x" alt="Photo behind the counter of the aforementioned cafe with a 3x3 grid pattern of MTR station signs." style="width:500px; height:333px;" /></p>

Hong Kong's MTR is spectacular; it even beats Singapore's MRT. If you give people reliable, fast, clean, efficient, and affordable mass transit, *people will use it*. I love the architecture of each station, the iconography, the maps, the traditional Chinese characters mixed with sans-serif Latin, everything. I could spend a week just exploring the system.

I went digging through the photos I took in 2017 to see if I had any of these stations. Here's a bit of that [Tsim Sha Tsui](https://en.wikipedia.org/wiki/Tsim_Sha_Tsui_station) yellow from the middle-left on the above grid!

<p><img src="https://rubenerd.com/files/2021/photo-mine-tst-hk@1x.jpg" srcset="https://rubenerd.com/files/2021/photo-mine-tst-hk@1x.jpg 1x, https://rubenerd.com/files/2021/photo-mine-tst-hk@2x.jpg 2x" alt="View down the station platform at Tsim Sha Tsui with the platform screen doors running down the left, a station master's office, and the aforementioned yellow column." style="width:500px; height:333px;" /></p>

It might be hard to tell from the photos, but that colour comes from a tile pattern. I thought it was such an elegant and modern take on the traditional station tiles you would have seen in London or New York, with all the benefits of instant station recognition as you whiz by. It also let the designers built more intricate patterns, like the speckled white and grey in Yau Ma Tei, and the bands of colour in Choi Hung. *Beautiful!*

Some of the earlier stations on the Singapore MRT had colours, but these were deprecated for greys and whites starting with the Northeast Line. Sydney's underground stations also had colours, but were gradually replaced with a similar pallete after the last upgrades a few years ago.

