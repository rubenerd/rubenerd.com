---
title: "Taking small victories"
date: "2021-10-28T08:08:03+10:00"
abstract: "Giving myself small tasks for personal stuff each day has helped to reframe bigger problems as attainable and lower effort."
year: "2021"
category: Thoughts
tag:
- cleaning
- personal
- psychology
location: Sydney
---
I'm certain nothing here is new, or a revelation, or isn't a mainstay talking point among self-help gurus and those life hacking sites of yore. Here comes the proverbial posterial prognostication: *but...* implementing this myself by accident recently helped a lot.

I've started thinking about home and personal problems at a smaller scale, and giving myself one to tackle every day. It reduces the mental burden associated with thinking about something, which means I'm more likely to start it. Because it's smaller, I'm also more likely to *finish* it, which feels like an accomplishment and gives me motivation to do more.

It also builds up. I've let myself take a day to declutter a box, or to recycle dead electronics, or sort through mugs, or rewire the graphic equaliser on our Hi-Fi, or fix that weird bug with `grep(1)` that turned out to be an errant line in my `~/.kshrc`. Some days I even just decide to change the clocks everywhere for daylight savings. These have made measurable improvements to our lives.

It feels overwhelming when I add "clean up apartment" to a `#TODO` list. But thanks to rethinking about the problem this way, I've done far more than I would have. This is why I don't even say it takes "longer", because the alternative was not doing it.

