---
title: "No more JavaScript frameworks"
date: "2021-03-30T14:31:53+11:00"
abstract: "A manifesto by Joe Gregorio."
thumb: "https://rubenerd.com/files/2021/yt-GMWAHzXQnNM@1x.jpg"
year: "2021"
category: Internet
tag:
- html
- javascript
- webdev
location: Sydney
---
So much of what [Joe Gregorio discussed in 2014](https://www.youtube.com/watch?v=GMWAHzXQnNM) still applies. Check out [his manifesto post too](https://bitworking.org/news/2014/05/zero_framework_manifesto/).

<p><a href="https://www.youtube.com/watch?v=GMWAHzXQnNM" title="Play Stop Writing JavaScript Frameworks - Joe Gregorio - OSCON 2015"><img src="https://rubenerd.com/files/2021/yt-GMWAHzXQnNM@1x.jpg" srcset="https://rubenerd.com/files/2021/yt-GMWAHzXQnNM@1x.jpg 1x, https://rubenerd.com/files/2021/yt-GMWAHzXQnNM@2x.jpg 2x" alt="Play Stop Writing JavaScript Frameworks - Joe Gregorio - OSCON 2015" style="width:500px;height:281px;" /></a></p>

I still think JavaScript was one of the worst things to happen to the Internet, but I also realise I live in the Real World. Vanilla JS and web standards would be far preferable to the cornucopia of incompatible, resource expensive, and brittle frameworks around today.

