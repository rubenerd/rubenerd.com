---
title: "Survey on the perceptions of migraines"
date: "2021-04-05T10:34:23+10:00"
abstract: "If you live in Australia, you can help."
year: "2021"
category: Thoughts
tag:
- australia
- health
location: Sydney
---
Migraine Australia are [conducting a short survey](https://www.surveymonkey.com/r/7TRVXHW):

> The purpose of this survey is to find out what people think and understand about migraine, and in particular, compare the perceptions of those who do not experience migraine with the perceptions of people who do live with migraine. 
> 
> **Any Australian can participate in this survey**. Your name and email are not required to participate. 
>
> It is our intention to use the information gathered in this survey to help design awareness and information materials. The results will be published on our website. We may also use some of the results in our advocacy with Government and other organisations.

Despite living with migraines since childhood, I had to truthfully answer at least half the questions with "not sure". That says something!

Please [take the survey](https://www.surveymonkey.com/r/7TRVXHW) if you can.

