---
title: "The @elkeee schooling me on obliviousness"
date: "2021-08-12T09:27:34+10:00"
abstract: "No Ruben, couture and culture aren’t the same thing."
year: "2021"
category: Thoughts
tag:
- family
- language
- personal
location: Sydney
---
I was wandering around the Sydney CBD with Clara and my sister Elke a few years ago, in the Before Times. I joked that a sign on a high-fashion store was misspelled, and how ironic it was given the expense they must have incurred gilding it. It was probably fine though, given their steep profit margins. Haha, look at me, I'm so savvy!

Elke laughed and said "No Ruben, Couture is not Culture".

I'd managed to live for three decades not knowing that word existed. And my instinct upon seeing it wasn't to assume I lacked knowledge, but that someone had messed up.

At the risk of overanalysing it (when have I ever done *that?!*), I think about that experience to this day. To invoke an oft-overused word, it's humbling to think how many other *Coutures* are out there. It's not that I was aware of it but didn't know anything about it, it never existed in my head in the first place. I like to think I'm open-minded and rational, but the truth is I still dismiss stuff more than I should.

It all balances out though; how many people in that Couture store know about the Commodore 128's various video modes, or can recite every station on the North South and East West MRT lines in Singapore, in the correct order? Huh? *HUH!?* Don't answer that.

One of them is Dover. [Dover and Dover again](https://rubenerd.com/dover-and-dover-again/).
