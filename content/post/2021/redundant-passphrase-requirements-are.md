---
title: "Redundant passphrase requirements are"
date: "2021-09-03T08:08:01+10:00"
abstract: "Not to mention client-side validation is usually broken too."
year: "2021"
category: Internet
tag:
- passphrases
- security
location: Sydney
---
I follow the same process whenever I have to sign up for a website. I unlock my KeePassXC keychain, create a new item, paste the URL for the new site, then use the Password Generator to spit out 128 characters of gibberish. That then gets pasted into the target site. No muss, no fuss.

*(As an aside, please check out KeePassXC. It's cross platform, open source, written in snappy Qt, and has plugins for Firefox, Vivaldi, and the death star. I send money to them each month, it's that good).*

This process continues to expose websites and their redundant passphrase requirements. Here was a warning I got from a site having pasted my latest string of alphanumeric soup:

> Password must contain: Uppercase, Lowercase, Number, Symbol (#$@!%\*?&) and be between 8 - 20 characters long

We'll tackle the broader issue here in a moment, but has anyone else noticed the validation checks for these are almost always flawed? This passphrase was rejected, and I still can't see why:

	Ji5$356fS#@1uYqQD!Va

It took me thirteen (!) attempts to generate a passphrase that worked. Those superstitious among you would expect no less from such a number, but for me it was just a frustrating exercise in painful frustration. Not that I'd ever use redundant phrasing.

But even if one day every frontend web developers figured out how to do client-side validation, that still doesn't address the fact **these rules hurt security**. Short keysmashes manage to be more difficult to remember than passphrases, and are less secure. [NIST has advised](https://www.infoworld.com/article/3194705/nist-to-security-admins-youve-made-passwords-too-hard.html) against mandating these requirements for years.

Length requirements are also pointless if the passphrase is being stored as a salted hash, something that should be considered a minimum technical requirement in addition to sounding tasty. My German genes lead me to being a potato rosti guy, but hash browns are also excellent.

I've heard from sysadmins working in finance that shorter passwords were mandated based on the assumption that longer ones are harder for customers to remember. That might have been true prior to those additional symbolic requirements above, but I'd challenge that assumption now! Another told me that shorter passwords are also easier to be read out over the phone... which is multiple levels of problematic.

I appreciate the real issue here is that passphrases are an antiquated and creaky auth method, and that it needs to be replaced. But not letting perfect be the enemy of good, we could start by removing these silly limitations.
