---
title: "Feedback on my “not sure if UNIX won” post"
date: "2021-09-07T07:35:24+10:00"
abstract: "Time for a beer?"
thumb: "https://rubenerd.com/files/2021/yt-aBE9EQ7gXKI@1x.jpg"
year: "2021"
category: Software
tag:
- bsd
- feedback
- freebsd
- netbsd
- linux
- unix
location: Sydney
---
I wrote a post back in May saying I [wasn't sure that UNIX won](https://rubenerd.com/im-not-sure-that-unix-won/), as so many media outlets were claiming. I said I was on the fence, but that I saw Linux continue to depart from UNIX's legacy in meaningful ways. It's since been picked up and circulated on the usual news aggregator sites and social media, most of which have generated relevant, tactful comments that swayed my opinion and... *nah, got you!*

Nobody that I could see challenged the post's premise that UNIX didn't win (which for certain Linux and BSD folks was seen as a bad thing for the ongoing project of cross-platform compatibility and good system design, or fabulous by others who claimed it freed their systems from perceived UNIX baggage).

Great, end of the post then, time for a beer! Wait, what do you mean it's Tuesday morning?

<p><a href="https://www.youtube.com/watch?v=aBE9EQ7gXKI" title="Play Spanish Flea"><img src="https://rubenerd.com/files/2021/yt-aBE9EQ7gXKI@1x.jpg" srcset="https://rubenerd.com/files/2021/yt-aBE9EQ7gXKI@1x.jpg 1x, https://rubenerd.com/files/2021/yt-aBE9EQ7gXKI@2x.jpg 2x" alt="Play Spanish Flea" style="width:500px;height:281px;" /></a></p>


