---
title: "tulloch4801’s video on the 3801 relaunch"
date: "2021-03-18T09:00:59+11:00"
abstract: "I’m still giddy from seeing her relaunched!"
thumb: "https://rubenerd.com/files/2021/yt-5IOU8VkvRXQ@1x.jpg"
year: "2021"
category: Travel
tag:
- 3801-locomotive
- history
- trains
location: Sydney
---
What a great compilation! I'm still [feeling the excitement](https://rubenerd.com/seeing-the-3801-steam-locomotive/) from fulfilling that childhood dream last weekend. What a stunning locomotive, and how great it is knowing she's fully operational and looking amazing again.

<p><a href="https://www.youtube.com/watch?v=5IOU8VkvRXQ" title="Play 3801 relaunch"><img src="https://rubenerd.com/files/2021/yt-5IOU8VkvRXQ@1x.jpg" srcset="https://rubenerd.com/files/2021/yt-5IOU8VkvRXQ@1x.jpg 1x, https://rubenerd.com/files/2021/yt-5IOU8VkvRXQ@2x.jpg 2x" alt="Play 3801 relaunch" style="width:500px;height:281px;" /></a></p>

As an aside, [Hales](http://halestrom.net/darksleep/) emailed to say he now understood why he kept hearing whistle sounds in his neck of the woods!

