---
title: "The @davewiner does a bank experiment"
date: "2021-06-13T14:05:59+10:00"
abstract: "IT is still the Wild West by comparison."
year: "2021"
category: Thoughts
tag:
- australia
- dave-winer
- finance
location: Sydney
---
[Dave posted this last Thursday](http://scripting.com/2021/06/10.html#a140520)\:

> Experiment: call your bank. Tell them you have a security issue. See how long it takes to freeze the account.
> 
> I've found that credit card companies are very tuned into security. If you call them up with a security issue, within a minute they have put a freeze on the account, and send you a new card. I generally get the new card the next day. And by &quot;a minute&quot; I mean a minute after the phone rings, not after navingating voicemail hell, getting upsold and hung up on, and being reminded your call is important to us, and did you know that you can get all the information you need on the web? Please hold and an operator will be with you soon. We apologize for the inconvenience.

Not to mention the follow-up email where they say *how likely are you to recommend this to your friends and family, basd on this arbitrary and unqualified numeric metric?* Card securtiy is an interesting case, because its a financial instrument directly tied to your credit score and legislation. IT is still the Wild West by comparison.

My experience hasn't always matched his, unfortunately. I used to be with Bankwest in Australia, back when I thought I'd be living the Australia-Singapore commute for longer and figured I'd eventually want to move to Perth to make that easier. I had my card skimmed, and Bankwest refused to dishonour the charges until I'd filed a police report. Amex, by comparison, did it almost immediately.

