---
title: "What sort of learner are you?"
date: "2021-07-11T08:53:36+10:00"
abstract: "Turns out, the distinction is entirely arbitrary."
thumb: "https://rubenerd.com/files/2021/yt-rhgwIhB58PA@1x.jpg"
year: "2021"
category: Thoughts
tag:
- learning
- studies
- video
- youtube
location: Sydney
---
Clara and I watched the [latest episode of Veritasium](https://www.youtube.com/watch?v=rhgwIhB58PA "The Biggest Myth in Education") last night which discussed *VARK*. It sounds more like a C preprocessor or a secret government weapon than a learning framework, but it breaks people down into four types:

* Visual learners
* Audible learners
* Reading/writing learners
* Kinesthetic learners, or learning by doing

I drew a lot of diagrams in school and uni, and I've always loved maps. I also typed so many notes that people even started asking for them back in the day! Writing is also my favourite activity in the world, especially first thing in the morning sitting outside with coffee in hand, and reading is among my favourite ways to pass time.

But there was no doubt in my mind, or any other body part, that I was *predominately* an audible learner above the others. I used to record classes and lectures, surrepticiously or otherwise, and play them back on my iPod while going on evening walks, or cleaning, or doing other menial tasks. It made studying more accessible, but over time I came to realise that I was absorbing more of the information *hearing* someone explain something over seeing a diagram or reading a textbook. This was *especially* true for subjects like history and geography.

Audible learning didn't always work. Highly-symbolic subjects like maths, chemistry, and computer science had an important visual component, but even among those I was able to grok concepts and reasons behind certain theorums and data structures by hearing someone discuss them. It also meant that come class time, I was able to ask better questions.

<p><a href="https://www.youtube.com/watch?v=rhgwIhB58PA" title="Play The Biggest Myth In Education"><img src="https://rubenerd.com/files/2021/yt-rhgwIhB58PA@1x.jpg" srcset="https://rubenerd.com/files/2021/yt-rhgwIhB58PA@1x.jpg 1x, https://rubenerd.com/files/2021/yt-rhgwIhB58PA@2x.jpg 2x" alt="Play The Biggest Myth In Education" style="width:500px;height:281px;" /></a></p>

But back to the Veritasium video. *Turns out* this identification and classification of people into different learning styles has little to no scientific basis whatsoever! People performend similarly in tests whether they were placed in classes matching their professed learning styles or not. The best approach to learning, as far as the scientific literature demonstrates, is to have multiple types of information presented. Written *and* visual, for example.

I can only go by my own lived experience and emprical evidence here. Granted I haven't ever conducted a test where I only studied by reading, then again with audio, for example. But I *can* say that I performed better in subjects where I heard a component rather than relying on just reading for subjects. But as I write this, I realise now that the audio was in addition, so it really was multiple types of information helping.

Clearly I had to hear someone say it before it sank in *#badumtish*.

