---
title: "Missing things in our home"
date: "2021-08-10T16:13:34+10:00"
abstract: "Power bricks, SATA terminators, a red sock."
year: "2021"
category: Hardware
tag:
- mmx-machine
- pointless
- sata
location: Sydney
---
You'd think that with all this spare time stuck in Covid lockdown, our apartment would be a model of organisation, cleanliness, and organisation. I reiterated that first and third point to emphasise their *you'd think-ness*. That sounds like Loch Ness. Wait, no it doesn't. Ruben, focus.

If anything I've lost *more* stuff in the last two months than any point since our last move a year ago. As an aside, I'm so glad I'm in lockdown with with Clara in a one-bedroom apartment instead of a studio, even if it does represent more floor space and cupboards for hiding and losing stuff.

Anyway, I'd appreciate any assistance if you've found these things in our apartment over the last month:

* Power supply brick for my HP 620LX handheld computer so that I can confirm it still doesn't work.

* 2.5-inch external hard drive backups, and their micro-USB cables, or mini-USB, or whatever the smaller, skinnier ones are called.

* Stack of spare Aeropress filters I bought just before last lockdown. We haven't reached critical yet with the existing supply, but I'd sleep better at night knowing. Which is ironic given caffeine messes with my sleep after 15:00.

* IDE to SATA adaptor, so I can connect my 1995 Creative Hex-Speed CD-ROM to my FreeBSD home server for no good reason whatsoever.

* Internal SCSI ribbon-cable terminator block. The self-terminator on my internal Jaz drive doesn't seem to work, and I knew I had a cool DEC/Digital-branded one somewhere that I bought on eBay for a lark a decade ago.

* Single red sock. I can't even blame the dryer for disappearing this; we only use it to dry sheets.

It's [Missing Salsa 2006](https://rubenerd.com/someone-stole-my-salsa/) all over again. That was fifteen years ago, yikes.
