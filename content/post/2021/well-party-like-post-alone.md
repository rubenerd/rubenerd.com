---
title: "We’ll party like post alone!"
date: "2021-04-22T15:41:57+10:00"
abstract: "A response to my 3am thoughts earlier this week."
thumb: "https://rubenerd.com/files/2021/LS012477@1x.png"
year: "2021"
category: Media
tag:
- music
- pointless
location: Sydney
---
*This post is dedicated to [Asherah](https://kivikakk.ee/), who sent the first comment in response to the post I mention below.*

I wrote what some would consider a [contentious article](https://rubenerd.com/the-free-software-foundation/) about the Free Software Foundation and the effect of the GPL on open-source software, alongside discussions about [Palm nostalgia](https://rubenerd.com/palm-pilot-nostalgia-on-its-way/), pop psychology views on [confidence](https://rubenerd.com/being-so-confident/), and a couple of posts on [FreeBSD 13](https://freebsd-13-0-release-available/). And yet, *this* comment I made at [three in the morning](https://rubenerd.com/03-00-thoughts/) last Wednesday generated the most comments!

> I counted Aloe Blacc saying he needs a dollar twenty times in his 2010 song [I Need a Dollar](https://www.youtube.com/watch?v=Pqj119D9kDg). That means he needs $20.

For someone who just spent time [talking about whimsy](https://rubenerd.com/coloured-imacs-and-whimsy/), this warms my heart. And gives me an excuse to pivot to the weird lyric in the title of this post. For those who don't want to scroll back up, it goes:

> We'll party like post alone! ♫

There's a specific formula most pop music follows now, where they have a verse, a weak chorus, and then a looping techno hook which carries the song. It's impossible not to hear this pattern everywhere once you identify it. As someone who otherwise loves electronic music, it grates to hear this instrumentation relegated to such a pedestrian role. *I'm not old!*

<p><img src="https://rubenerd.com/files/2021/LS012477@1x.png" srcset="https://rubenerd.com/files/2021/LS012477@1x.png 1x, https://rubenerd.com/files/2021/LS012477@2x.png 2x" alt="" style="width:128px; height:128px; float:right; display:inline; margin:0 0 1em 2em;" /></p>

I've heard that one specific song continuously in coffee shops, and loudly from the headphones of inconsiderate commuters. It's otherwise a forgettable tune, but I've been fascinated with what "post alone" means, and why someone would want to party like it.

These are the only meanings I could think of:

1. Having the confidence to spin around a lamp post and sing in the rain, because you're alone with nobody else to judge you.

2. A person received a delivery of something specific in the post, which they'd use alone on themselves for a party. Cough.

3. The satisfaction and joy philatelists feel putting their parcel in a mailbox. That still sounds like the second point.

Urban Dictionary didn't shine much more light. They define "post" as describing things that are "dated, unhip" that were once "in vogue and in style". Maybe it means enjoying a hobby that would otherwise consign you to being socially ostracised? Sounds more like point 2 again.

I finally heard it at a coffee shop again today and managed to snag it on Shazam. The title of the song is *[Post Malone](https://www.youtube.com/watch?v=3_l4vYlJTY0)* by Dutch DJ Sam Feldt, sung by RANI. I still didn't know what a post malone was; is it different from a pre-malone? Is it what you use on your malone when recursion doesn't work? Is it a monad? A lambda? Something else that sounds like a sheep? Is it a functional form of posting?

[Wikipedia](https://en.wikipedia.org/wiki/Post_Malone_(song)) sorted me out:

> The song was referred to as an "ode" to American rapper Post Malone and features the lines "Tonight, we go all night long / We party like Post Malone!".

Being in my thirties doesn't make me old, I'm just *Post Young*.
