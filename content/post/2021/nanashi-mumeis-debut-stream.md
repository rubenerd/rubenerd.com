---
title: "Nanashi Mumei’s debut stream #watchMEI"
date: "2021-08-25T11:06:30+10:00"
abstract: "Surprising nobody, she’s probably already my other favourite Hololive-EN character."
thumb: "https://rubenerd.com/files/2021/yt-M2gwJ-7s0Zo@1x.jpg"
year: "2021"
category: Anime
tag:
- hololive
- nanashi-mumei
- video
- youtube
location: Sydney
---
Clara and I are *slowly* catching up with latest generation of [Hololive English](https://www.youtube.com/channel/UCotXwY6s8pWmuWd_snKYjhg) debuts, which we’re watching entirely out of order. I say both of us, though of course she has seen them all already. Last night was for the HoloCouncil’s Guardian of Civilisation, Nanashi Mumei.

<p><a href="https://www.youtube.com/watch?v=M2gwJ-7s0Zo" title="Play 【DEBUT STREAM】Oh? OH! #holoCouncil #hololiveEnglish"><img src="https://rubenerd.com/files/2021/yt-M2gwJ-7s0Zo@1x.jpg" srcset="https://rubenerd.com/files/2021/yt-M2gwJ-7s0Zo@1x.jpg 1x, https://rubenerd.com/files/2021/yt-M2gwJ-7s0Zo@2x.jpg 2x" alt="Play 【DEBUT STREAM】Oh? OH! #holoCouncil #hololiveEnglish" style="width:500px;height:281px;" /></a></p>

Let me be clear, I do not take the responsibility of imparting this reaction lightly. 
**FGSDFGJWERFFVS FVASDJ GFKGRT REHS ADFAFHNNGGG!** Her self-deprecating humour, tone of voice, and mannerisms pushed all our buttons and then some. Not that I'm predictable about my favourite characters or anything.

Her voice actor was clearly nervous at the start, but by the end of the stream she'd hit her stride and managed to impart some of those subtle digs against her Hololive senpais. The setup and her inquisitive voice made it that much funnier. I'd give more examples if either of us had a better memory (where am I, have I mentioned memory yet)?

I haven't watched the rest of the other debuts yet, but I can already tell Mumei has our favourite character design as well. My site mascot Rubi appreciated the callout and respect for mismatched stockings, but I'd say her cape is my favourite feature. *It has hieroglyphics!*

At the risk of reading too much into her lore here, I appreciated a more optimistic view of humanity as well, especially at a time like this. I maintain that the timing of Hololive during Covid was almost too good. They're all been such an uplifting and positive force in a world needing both right now, and I can already tell Mumei will be a great addition.

I'm not at all a Hololive completionist on account of not playing many games, so I'm also keen to see some of the history streams she's planning. Minecraft aside, most of my favourite streams have been ones where the characters discuss ideas, learn a new skill, and make music. Plenty of game streams are entertaining and fun, but there's still so much untapped potential in this format.

And finally, I could also see her mascot Friend chilling with Ina's Tako. I think the world would succumb to a comfiness vortex if they did a collaboration stream in the future. But Inaff of that.
