---
title: "Going to Shadu"
date: "2021-01-31T11:24:15+11:00"
abstract: "I just heard it on the rebooted Whole Wheat Radio stream, from Melbourne apparently!"
year: "2021"
category: Media
tag:
- esther-golton
- music
- whole-wheat-radio
location: Sydney
---
I heard Esther Golton's "Going to Shadu" play this morning on the [Whole Wheat Radio stream](http://wholewheatradio.duckdns.org:8000/WWR) that Jim put up again recently. Wait, you're not listening to it? Even if it says you're in Melbourne for some reason!?

It's one of my all-time favourite songs, not just because it's so beautiful, but because of how it made me feel upon first hearing it. 2007 to 2009 was a rough period for my family, and it was just the right song and album at just the right time. Hearing it again in our new apartment with all the hi-fi gear having scarcely been set up an hour before... gave me goosebumps.

You <del>can</del> should buy [Esther's albums on Bandcamp](https://esthergolton.bandcamp.com), including her album *[Unfinished Houses](https://esthergolton.bandcamp.com/album/unfinished-houses)* where this track came from. You shouldn't use Spotify, but if you do, you can [preview the track](https://open.spotify.com/track/3C32yRBszJoRnt5GF6z03R) there too.

