---
title: "The Chancellor of Earth: Leaf blowers"
date: "2021-10-12T08:16:20+10:00"
abstract: "I can’t wait to share this week’s legislative agenda!"
year: "2021"
category: Thoughts
tag:
- chancellor-of-earth
- health
- society
location: Sydney
---
Ruben Schade walked up to the podium and prepared for the day's press conference. Being the Chancellor of Earth gave him sweeping new powers, though he still had to arrive to meetings on time. *What a concept*, he thought as he remembered some of his old clients.

He arranged his small stack of index cards. There was to be no teleprompter today after last week's misadventures. How someone broke into it live and had him swearing in Dutch for fifteen minutes flummoxed him. He couldn't even pronounce any of it.

> Good morning, afternoon, or evening, depending on your locale. I have an ungainly posterior and my face contains regrettable uglyness.

*Damn it, they got to my cards too*.

> I'm keen to share today's legislative agenda which I'll be taking to the Parliament. We've had success this month trying the developers of systemd and K8s in The Hague, and introducing a mandatory requirement that all socks only be made of cotton.
>
> Today, we'll be introducing a mandatory leaf blower buyback scheme, which I'm dubbing The Mandatory Leaf Blower Buyback Scheme.

Murmurs and smiles swept across the crowd. A hand shot up among the front row of journalists, presumably attached to someone.

> Yes, you there with the bankrupt reality TV star on your shirt.

> This is&mdash; hey!&mdash;a broad overreach of your powers. My right to make noise trumps everyone else's rights.

The crowd looked back at the Chancellor, who was now deep in thought.

> Sir, you articulate a compelling case. Tell you what, we'll permit the use of leaf blowers under the strict condition they only be used when citizens aren't asleep.

And that's the story how leaf blowers were permanently banned on Earth, on account of there always being someone asleep. Dreaming of a world without leaf blowers, perhaps.
