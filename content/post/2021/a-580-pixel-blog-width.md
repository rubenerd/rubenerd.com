---
title: "A 580 pixel blog width"
date: "2021-04-20T14:20:32+10:00"
abstract: "It’s a fascinating, cough, story!"
year: "2021"
category: Internet
tag:
- design
- weblog
location: Sydney
---
Josh Nunn asked me on the [better social network](https://aus.social/@screenbeard/106078797694688201) about why the CSS in my blog theme is 580 pixels wide. It's a fascinating story, which I posit is true because I said so despite clear and present evidence to the contrary.

*(Contrary is only one letter away from Country. No, wait, I count twenty-three letters between them in that sentence. Is this what people subscribe for? Don't answer that).*

I ran the [first version](https://rubenerd.com/omake/engine-room/#historical) of my blog on shared web hosting from 2004 to 2005, and so needed to limit the disk space and bandwidth use. I settled on 500 pixels for image widths, and wrote my first themes to accommodate this. Later when I moved to WordPress I styled the Sandbox theme in the same way, and then on from Jekyll and Hugo.

Over time I increased the font size of posts for legibility, which meant there were too few words per line. The solution was to expand the width of posts so the same amount of text fit as before. 500 pixels at 0.8em was roughly the same as 580 pixels at 1.0em, give or take. But I've stuck with 500 pixel-wide images ever since, for reasons of shameless nostalgia and the fact all my image-processing scripts assume it.

It's probably one of the biggest technical things I regret about this site. Scaling Retina/HiDPI graphics would have been far easier if I started with 640 pixels instead of 500, even if the surrounding text would have needed more padding. I was doing HSC Extension English in high school at the time though, so was used to padding words. But I suppose 500 still made more sense when I was counting every cent in hosting costs, and I didn't work at a [cloud infrastructure](https://www.orionvm.com/) provider who let me run it for free (cough)!

