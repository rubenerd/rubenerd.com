---
title: "Not just the price limiting EV adoption"
date: "2021-01-28T10:23:28+11:00"
abstract: "Renters, and those in apartment buildings have no choice."
year: "2021"
category: Hardware
tag:
- environment
- journalism
location: Sydney
---
The inflated Australian housing market continues to affect the prospects of people who didn't make it onto the property ladder, or those entering the workforce after the boom started. Covid has only widened this stratification between the haves and have nots; Clara and I are lucky that we can afford a deposit, but we blanch at the absurdity of the mortgage debt it represents. I suspect it's the same in cities like Vancouver, San Francisco, and Hong Kong where housing loans are also dozens of times higher than average incomes.

Redirecting so much disposable income into an unproductive asset hampers wellbeing and economic growth, but also has less-publicised effects we're only beginning to realise. Take electric vehicle sales: financial and environmental writers are keen to wax lyrical about their future while praising government incentives like credits and subsidies, but they only go so far and are fundamentally one-sided.

Renters can rarely buy EVs, and we're the fastest growing cohort in the housing market. Landlords without an altruistic or ethical streak have little motivation to pay for installing the charging infrastructure required; I don't think we're at the level of market penetration where an EV charger can be used to fetch a rental premium. Renters are understandably reluctant to foot the steep bill if we can't take it with us, or have to pay all over again with each move.

This is doubly the case in apartment buildings where, again, more people are living than before. Renters don't have access to strata the way landlords do, and can't advocate for charging installations. Even if we could, and even if apartment buildings could have their electrical systems beefed up to support all these new cars, we'd be right back at the issue of who pays for it.

I only see a few options. I've had one wonderful landlord who was happy to split the bill on a home improvement that I argued improved the value of their investment while improving our quality of life. Most expect you to pay for it all, or wish you'd stop pinging them so they can treat you like glorified house sitters. Governments could get involved, though the Australian electorate seems adverse to spending money on infrastructure that would benefit them. The only other option would be raising rental standards to require houses to include charging infrastructure, but the same lack of forward-thinking political will applies there too.

Meanwhile, concerns about range, and the actions of an eccentric billionaire, seem to be all the press care to cover. Renters can't buy the EVs they want if they can't get them juice.

*(I still maintain the ultimate solution to urban transport is slavish, unbridled investments in rapid transit and commuter rail. But a zippy EV for those times where you need wheels would be rather nice... like going to IKEA to get furniture to fit your latest rental).*
