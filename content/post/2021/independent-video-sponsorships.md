---
title: "Independent video sponsorships"
date: "2021-10-29T17:12:47+10:00"
abstract: "I think dismissing them out of hand is simplistic, but I see where skeptics come from."
year: "2021"
category: Media
tag:
- advertising
- ethics
- youtube
location: Sydney
---
*(Hi, it's Ruben, from the future. I started writing this with a clear view of my conclusions, but thinking out loud lead me in a direction I didn't expect. Normally I wouldn't post such half-baked thoughts, but I'm risking it. Time will soon tell if this was a good idea).*

I've fallen head over heels for documentary-style YouTube channels over the last year, as I'm sure you've noticed based on what I link to here. Learning about geography, science, and engineering is *fun*, and especially so when it's from an independent producer who's doing it for the love of the subject.

These range from channels with thousands of subscribers, like [David Frankal](https://www.youtube.com/c/DavidFrankal/videos) in the UK, to millions like [Techmoan](https://www.youtube.com/c/Techmoan) and [Real Engineering](https://www.youtube.com/c/RealEngineering). I keep a list of my current obsessions over on my [Omake page](https://rubenerd.com/omake/) for those interested.

Authenticity is a big reason why I love them. Unlike big budget productions that so often bring in someone to read a script, these creators present themselves as a friend; someone who talks with you in your loungeroom about a topic they've researched. Even the most polished productions still try hard to come across as friendly and approachable.

You're smart enough to know where this is going. I know that when Techmoan reviews a Panasonic boom box, he's being genuine with his comments. He doesn't claim to be impartial; he has strong opinions, and doesn't compare devices against hundreds of other products in a detailed spreadsheet. But he's also honest about the capabilities, limitations, and prices of what he discusses.

This is why it hits hard when that trust feels like its violated. I admit to being taken aback by Veritasium's self-driving car video which, despite his admission of sponsorship, still felt one-sided and lacking in important information and context. Specific claims were also poorly cited and, as much as it pains me to say, dubious.

Tom Nicholas has a compelling series of videos explaining how company PR budgets are influencing independent creators, [one of which](https://www.youtube.com/watch?v=CM0aohBfUTc "Veritasium: A Story of YouTube Propaganda") I linked to last week. He also talked about [another prominent YouTuber](https://www.youtube.com/watch?v=Dum0bqWfiGw "Johnny Harris: A Story of YouTube Propaganda") prompting the world view of a specific international organisation, under the guise as a documentary. I'm glad these have sparked discussion.

But here's where I'm struggling. Critics of sponsored content point to the fact that advertisers may (being the operative word) have editorial control over the material, and that it naturally follows that we can’t trust them. Having healthy skepticism is good and even necessary, but I think disqualifying someone's points entirely on such a basis is simplistic.

My issue with the Veritasium video wasn't that it was sponsored, but that it was framed as a scientific discussion on the merits of self-driving cars. I think the distinction is important; had it been listed as a "tour of company X's self-driving cars", the entire tone of the video changes. I still think his selective use of statistics was misleading and less forgivable, but even then I think we'd be more disposed to looking at the claims critically if we know it's an advertisement.

Well-cited, independently-verifiable data can still be skewed to fit any agenda or perspective (lies, damn lies, and statistics), but that's true of *any* media, commercial or not. What I'm more concerned about is independent creators claiming objectivity, or being misleading about the genesis of a video.

And the great irony is, I doubt being truthful would have much of an impact on their viewership. Techmoan getting a paid tour of a Sony factory might be really cool! Veritasium pitching his video as a behind-the-scenes look at self-driving car tech would have been just as interesting.

I guess I tend to be skeptical when I see people claiming issues are black and white, whether such advice is well-intentioned or not. I do look more critically at sponsored content, but I'm not going to dismiss it entirely.

*(I get around this issue by having day jobs and other sources of income, and not being paid by companies to write this blog. It's a luxury)!*
