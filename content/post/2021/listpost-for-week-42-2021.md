---
title: "Listpost for week 42, 2021"
date: "2021-10-24T08:05:20+10:00"
abstract: "Bananas, sky-blue aircraft liveries, educational YouTube, and 42."
year: "2021"
category: Thoughts
tag:
- listpost
location: Sydney
---
It's Sunday, which means it's time for another [listpost](https://rubenerd.com/tag/listpost/)!

* SimpleFlying reported on the [Italian carrier ITA's new livery](https://simpleflying.com/ita-new-livery/). Painting your aircraft sky blue doesn't *seem* like the best idea for visibility or safety, but maybe it's to save money by slipping out of airports undetected without paying fees.

* [Banana Craze](https://bananacraze.uniandes.edu.co/language/en/) is a virtual exhibition showcasing how the humble banana has shaped identity, ecosystems, and violence in South America. I saw this listed on Metafilter and got stuck into it for the better part of an afternoon.

* I've mentioned the [Veritasium](https://rubenerd.com/tag/veritasium/) educational science channel here before, but I'll admit his recent video about self-driving cars was underwhelming. Tom Nicholas has a well-researched, if drawn-out [rebuttal](https://www.youtube.com/watch?v=CM0aohBfUTc). 

* Om Malik's [recent trip](https://om.co/2021/10/17/sitting-in-place/) was "an excellent opportunity to step away from the daily torrent of media inanities". I'm going to do that with a cup of tea and some manga this afternoon, too.

* This is [week 42](https://en.wikipedia.org/wiki/Phrases_from_The_Hitchhiker%27s_Guide_to_the_Galaxy#The_Answer_to_the_Ultimate_Question_of_Life,_the_Universe,_and_Everything_is_42) of 2021. Heh.

* The <a rel="nofollow" href="https://www.aspistrategist.org.au/sea-mines-the-asymmetrical-weapon-australia-must-have/">Australian Strategic Policy Institute</a> suggests we should use sea mines, those asymetrical weapons that sank the hospital ship *Britannic* during World War I. Alternatively, we could *not* use them!

This listpost is brought to you by Listpost, the postal service for lists. Find out more by clicking the link in the description. Listposters can get 1% off their first order by using the code *Listpost21* at checkout. Don't forget to leave a comment, like, subscribe, click the bell, floof the dog, and tell all your friends about Listpost. Listpost: *It's the postal service for lists!*
