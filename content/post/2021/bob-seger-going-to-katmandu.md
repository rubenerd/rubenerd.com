---
title: "Bob Seger going to Katmandu [sic]!"
date: "2021-09-27T15:07:53+10:00"
abstract: "“You want some people... well they got the most!”"
thumb: "https://rubenerd.com/files/2021/yt-XG-wuWNIyzI@1x.jpg"
year: "2021"
category: Media
tag:
- music
- music-monday
location: Sydney
---
Years ago I was listening to the radio, like a gentleman, and the soft-spoken Singaporean DJ Brian Richmond came on between songs to announce that "here's a song by a guy... who's never been to Kathmandu". That deadpan delivery still cracks me up.

<p><a href="https://www.youtube.com/watch?v=XG-wuWNIyzI" title="Play Bob Seger - Katmandu"><img src="https://rubenerd.com/files/2021/yt-XG-wuWNIyzI@1x.jpg" srcset="https://rubenerd.com/files/2021/yt-XG-wuWNIyzI@1x.jpg 1x, https://rubenerd.com/files/2021/yt-XG-wuWNIyzI@2x.jpg 2x" alt="Play Bob Seger - Katmandu" style="width:500px;height:281px;" /></a></p>

Speaking of cracking up, Bob Seger must surely have delivered one of the greatest versus in musical history:

> I ain't got nothing 'gainst the East Coast.   
> You want some people... well they got the most!

He could be talking about the US, Australia, the PRC... a lot of places, now that I think about it. I did like wandering around [East Coast Park](https://commons.wikimedia.org/wiki/File:1_east_coast_park_panorama_2016.jpg) in Singapore, though the population isn't concentrated there.

*(Update: Looks like my video embed script didn't like the aspect ratio of that song. I'm going to leave it; I think the weird skew and positioning is fitting).*
