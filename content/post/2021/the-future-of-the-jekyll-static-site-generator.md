---
title: "The future of the Jekyll static-site generator"
date: "2021-09-16T08:24:20+10:00"
abstract: "The project is still receiving updates, but one of the former core developers claims it’s on permanent hiatus."
year: "2021"
category: Internet
tag:
- gohugo
- jekyll
- static-site-generators
location: Sydney
---
My blog here has been rendered with the [Hugo](https://gohugo.io/) static site generator since at least 2016. Having all my blog posts stored as plain text files, wrapped with a simple enough theme, and generated on a server makes so many things easier. Hugo cuts through my almost 8,000 blog post archive like butter, rendering it in fewer than 20 seconds. My web server is the most basic thing imaginable, because all it has to do is deliver HTML.

But my first experience with static site generation&mdash;as opposed to server-site software like WordPress or a homebrew Rails app&mdash;was Jekyll. Its upending of how sites were created spawned a cottage industry of static-site generators, and a renewed interest in simplicity over what software like WordPress had become.

Jekyll got a boost when it was used to power GitHub Pages, GitHub's static page hosting service. Suddenly anyone could upload text to a public repo, and have a rendered site on the other side.

I loved Jekyll. I [made the switch in 2013](https://rubenerd.com/tag/jekyll), and it forever changed how I write. The Liquid template system was a joy to use, and having all my posts in plain text meant I could use basic \*nix shell tools to edit, search, organise, and process posts. I'm good with SQL, but sometimes raw text is just easier.

But why the nostalgia trip today? Unbeknownst to me, media outlets are reporting that the writing has been on the wall for Jekyll for a while. [Tim Anderson reported for the Register](https://www.theregister.com/2021/09/14/future_of_jekyll_project_engine/) that one of the former core developers Frank Taillandier (rest in peace) described the project as being "in frozen mode and permanent hiatus". GitHub Pages is also stuck at version 3, despite the 4.x branch being available for years.

The [official repo](https://github.com/jekyll/jekyll) is still being updated though, and active discussions are ongoing, so I wouldn't be quick to [rdiscount](https://dafoster.net/projects/rdiscount/) it yet. *Get it? Because that Markdown renderer that Jekyll used to use was called... oh shaddup*. Much of the Register article was lifted from a blog post about the maintainer of a new Jekyll fork, so I take it with a grain of salt.

Dare I say, there's also something to be said for mature software that does its job not needing such regular updates. I know people who continue to run and maintain Jekyll sites successfully for large clients today. I'm willing to reserve judgement for now.
