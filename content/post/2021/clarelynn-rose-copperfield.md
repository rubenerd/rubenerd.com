---
title: "Clarelynn Rose, Copperfield"
date: "2021-08-02T08:21:05+10:00"
abstract: "Today’s Music Monday comes from the Whole Wheat Radio stream, and is among the most beautiful I’ve ever heard."
thumb: "https://rubenerd.com/files/2021/yt-QAwm541Z5J8@1x.jpg"
year: "2021"
category: Media
tag:
- music
- music-monday
- whole-wheat-radio
location: Sydney
---
This morning's [Music Monday](https://rubenerd.com/tag/music-monday/) came from the Whole Wheat Radio stream, and was just what I needed having gazed onto my work and personal calendar for today. This is one of the most beautiful songs I've ever heard.

<p><a href="https://www.youtube.com/watch?v=QAwm541Z5J8" title="Play Copperfield"><img src="https://rubenerd.com/files/2021/yt-QAwm541Z5J8@1x.jpg" srcset="https://rubenerd.com/files/2021/yt-QAwm541Z5J8@1x.jpg 1x, https://rubenerd.com/files/2021/yt-QAwm541Z5J8@2x.jpg 2x" alt="Play Copperfield" style="width:500px;height:281px;" /></a></p>

Her [official store](http://www.heartwoodmusic.com/store/) links to CDBaby which was unfortunately taken offline a few years ago, but the page lists a few other online store options. I just put in a request on her [CD page](http://www.heartwoodmusic.com/store/cds.php) for how much her discography would cost to ship to Australia :).

