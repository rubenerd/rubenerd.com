---
title: "Laptop, tablet, smartphone, or phone?"
date: "2021-06-16T09:01:51+10:00"
abstract: "I chose laptop without a second thought."
year: "2021"
category: Hardware
tag:
- android
- apple
- ios
- laptops
- phones
location: Sydney
---
[Serge Matveenko asked on Mastodon](https://fosstodon.org/@lig/106410816498540932) which of the above you could live with exclusively for a year, if given the choice. I chose laptop *without a second thought*, and I still stand by it after a day of rumination. Or sit by it, depending on the table I'm using.

I tolerate my relationship with smartphones. Leaving aside my privacy qualms with Android, and usability issues with iOS, I don't like what they represent now. They used to be fun portable computers, now they're just another invasive but necessary distraction with seemingly-arbitrary limits that make their use impractical for things they otherwise could excel at. They certainly have the processing power now to do what the likes of the OQO attempted, but their hobbled IO and lack of UI maturity keeps them from achieving it.

I work on laptops, but I also write on them, play games, tinker with them, and I'm interested in desktop/server OSs to an extent that I'm not with mobile OSs. That might just be my age showing, though. Laptops might be cheaper and lower spec'd than certain smartphones now, but they're infinitely more functional and fun. I can also fire up a SIP tunnel, or a GoToMeeting bridge to take calls. There's also the novelty and fun that I'm literally carrying around a full computer environment around with me. My childhood CRT desktop built to a cost couldn't have ever been plonked down at a coffee shop!

Tablets are an interesting case. My work iPad is indispensable for iRL meetings, because I can take notes in front of people without looking rude. Laptop displays have the ergonomic advantage of being pointed *at* us, but meeting attendees can't tell whether you're paying attention or reading something else. Tablets are also nice to catch up on long-form content like the Nikkei Asia Review and The Atlantic, for example. But all of this could be replaced with a laptop and physical paper. And I still find their mobile OSs as frustrating and limiting to use for anything more substantial than note taking or reading. Again though, that might be my age showing. 

As for regular or non-smart phones, I think I'd prefer a pager. It gives you a number to call back if you're available, but it's asynchronous nature would feel far less intrusive. I *suppose* that's what voicemail is... kinda. I never had one, but I could tell my dad preferred it to the phone he ended up getting in the 1990s.

