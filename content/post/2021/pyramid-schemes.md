---
title: "Pyramid schemes"
date: "2021-05-04T11:25:02+10:00"
abstract: "I was so proud of this person for sticking up to this snake oil salesperson."
year: "2021"
category: Thoughts
tag:
- business
- scams
location: Sydney
---
We all know about those modern pyramid schemes that recruit people to sell health shakes and supplements. It really came home for me when I saw a friend from my uni days standing in a railway underpass trying to spruik these to anyone who'd listen. It broke my heart thinking that he'd been suckered into buying all this stock, and that's what his life was.

But it's a double-edged sword. I've been sitting at this coffee shop for half an hour overhearing a young man being pitched this alternative medicine scheme. The sales guy has all his talking points down, from bulk discounts, the purported health benefits without actually saying anything concrete, and how he has people lining up to buy from him.

Then out of the blue, the young guy calmly destroyed him with two sentences. I didn't write it down fast enough, but was something to the effect that he was "onto him" and that he should get a real job instead of being a "bullshit artist". He stood up and walked away, leaving the pyramid schemer with the coffee bill. *Brilliant!*

For each of these slick sales guys is a person who's life has been destroyed. Maybe the best approach isn't to debunk them, but to waste their time so they can't spend it on impressionable or uninformed people. Sign me up to that!

Cryptocurrencies, get-rich-quick schemes, multi-level marketing... the only pyramid scheme I want to see is one in which Clara and I get to go to Egypt one day.

