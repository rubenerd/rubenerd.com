---
title: "Christopher Eccleston on machoism"
date: "2021-12-27T09:39:35+11:00"
abstract: "I like this guy even more now."
year: "2021"
category: Media
tag:
- doctor-who
- quotes
- psychology
location: Sydney
---
This was a [great post in The Guardian](https://www.theguardian.com/lifeandstyle/2021/dec/25/christopher-eccleston-this-much-i-know)\:

> My physical appearance, accent and class might suggest machismo, but I’m anything but macho. I’m very much the son of my mother. I’m often cast in tough, gritty roles, but I’m nothing of the sort. We’re all forced to put that armour on, but I move through life gently.
>
> Caring for my kids has transformed me. I wish my dad had shown his vulnerability more – I’ve never wanted to appear tough to my two. I cried when I was with them last weekend, their love makes me emotional. My daughter often says she feels like she has a girl for a dad. I take that as the greatest compliment.

Everyone has "their Doctor", and Christopher Eccleston is mine. He had a lot riding on his shoulders when they restarted *Doctor Who*, and he delivered a superb performance. He had the perfect mix of offbeat, somewhat aloof, but compassionate. His success with the character paved the way for the heavy hitters of the modern era, for which I don't think he garners enough credit.

Now I like him even more. I need to check out some of his other stuff.
