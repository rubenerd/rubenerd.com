---
title: "Jim Kloss on art"
date: "2021-02-03T08:56:12+11:00"
abstract: "Art and crossovers never need to make sense."
thumb: "https://rubenerd.com/files/2021/Room-in-new-york-edward-hopper-1932@1x.jpg"
year: "2021"
category: Media
tag:
- art
- jim-kloss
- whole-wheat-radio
location: Sydney
---
> Art and crossovers never need to make sense.
