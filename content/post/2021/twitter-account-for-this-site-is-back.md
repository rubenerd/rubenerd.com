---
title: "Twitter account for this site is back"
date: "2021-05-27T09:02:56+10:00"
abstract: "Thanks to everyone for reporting."
year: "2021"
category: Internet
tag:
- social-media
- twitter
- weblog
location: Sydney
---
The [Twitter account for this site](https://twitter.com/Rubenerd_Blog) was locked. I logged in and got this cheerful message:

> <span style="font-weight:bold">We’ve temporarily limited some of your account features.</span>
>
> **What happened?**
> In order to make sure Twitter is as safe as possible, sometimes &mdash; like now &mdash; you may be asked to confirm you're not a robot. Easy, right? Just complete the following to get back to the Tweets.
>
> **What next?**
> For full access, please: [p]ass a Google ReCAPTCHA challenge.

There are a couple of funny things here. I'm sure you picked up that em dashes *aren't supposed to be padded with spaces!* This is an affront to typographic style, convention, and taste. What's next, people using three full stops instead of an ellipsis? Rarely do I ask here for people to think of the children, but I've even slipped into passive voice here given how overwhelmed I am at this dashed misapplication. This cannot&mdash;and must not&mdash;stand. Hey look, em dashes used correctly!

But also, this Twitter profile is fed from an RSS feed, so it *isn't human*. Having a human manually tweet each time I write a post would negate the point. I know, they want to know a human owns it and it wasn't just randomly generated. Still, the fact it's been around since 2008 and had a human post to it a bunch of times should have demonstrated that.

Thanks to everyone who emailed and tweeted me about this, it should all be good now.

I also need to figure out how to get <abbr title="If This Then That">IFTTT</abbr> working with [Mastodon](https://bsd.network/@Rubenerd). I set up a pipeline but I don't think it's ever worked.

