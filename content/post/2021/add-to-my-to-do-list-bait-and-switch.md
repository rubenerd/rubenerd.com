---
title: "Add to my to do list: bait and switch"
date: "2021-05-25T18:06:07+10:00"
abstract: "As part of the change, we’re introducing X, and crippling Y!"
year: "2021"
category: Internet
tag:
- services
- uh-oh
location: Sydney
---
It's time for another installment of *[Uh Oh!](https://rubenerd.com/tag/uh-oh/)*, the series where I highlight attempts by companies to obfuscate negative changes to their services. It's like *Yu-Gi-Oh!*, only you lose half your cards each time you draw a holo.

Today is a to-do list service I briefly used a few years ago, with this email:

> 🚩 Changes to your **$SERVICE** plan

In the words of Aussie comedian Carl Barron, *that dun look good.*

> We hope you’ve been getting the most out of **$SERVICE**, whether you’re using it to organize your work tasks, plan a wedding, or keep track of your grocery list (or all three!). Starting today, you’ll be able to do even more on **$SERVICE's** Free plan. We’re unlocking a ton of previously-Premium features [..]

Uh oh.

> As part of the change, any brand new free account will now be limited to 5 active projects.

And there it is! I was expecting something like syncing being paid-only; this is far worse. People who would use any of those extra features would be the exact ones who'd have more than five projects. I'm sure they know this.

It's the same playbook every time, isn't it? Cripple a core part of a service once you've locked in sufficient users, then cop the people churning with profits. My thoughts about that misleading business model aside, why do marketing teams think complement sandwiches are an effective announcement strategy, rather than being honest and upfront? It's so blatant and transparent.

Be wary of such free tools, you'll always end up paying for them in other ways. I prefer paying for services that are honest from the start.

