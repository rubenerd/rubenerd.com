---
title: "Answering @buzzyrobin about burnout"
date: "2021-11-08T09:28:37+10:00"
abstract: "I’m in the middle of leave too. Here are my scatterbrain thoughts."
year: "2021"
category: Travel
tag:
- personal
- work
location: Sydney
---
Robin posted on The Bird Site:

> I've read a lot of "I was burned out, I took some leave, I've kind of recovered" retrospectives, but comparatively few written by people in the middle of it and living with that uncertainty.

I get that. As Robin explains, it floors me that people in financially perilous circumstances (aka, most of the world) *can't* take leave, or the disabled among us who live with so much of this frustration on a daily basis. Respect doesn't cover it. But I digress.

I'm in the middle of such leave right now too, so here's my middle-of-the-road report. I've rewritten this at least a dozen times; I wouldn't have expected that articulating such thoughts would be so difficult.

Everyone's burnout manifests in different ways. Mine exacerbated longstanding anxiety, which when mixed with some deaths in the family, commitment overload, messed up sleep, and some unexpected "elective" surgery on the horizon, I snapped like a Polaroid Picture. Wait, that's not the lyric.

<p><img src="https://rubenerd.com/files/2021/blue-mountains-trail-2021@1x.jpg" srcset="https://rubenerd.com/files/2021/blue-mountains-trail-2021@1x.jpg 1x, https://rubenerd.com/files/2021/blue-mountains-trail-2021@2x.jpg 2x" alt="Photo of one of the walking trails around Echo Point, near the Three Sisters in the Blue Mountains." style="width:500px; height:333px;" /></p>

Robin's "kind of recovered" summary matches my mood thus far too. Not panicking (as often) has been wonderful. My resting heart rate is lower, and that foreboding fight or flight alliteration is starting to feel manageable again.

IT people are trained to think in inputs and outputs, so it flummoxes me that my mental black box isn’t responding as well to this break as before. The Blue Mountains are a beautiful reprieve, but it hasn't done it for me the way travel in Southeast Asia, or Japan, or the US travel did prior to Covid.

I'm sure this is a perverse form of mental recursion, but I can't shake this unsettling feeling that my current trip "isn't working", or not to the extent previous ones have. Maybe it’ll take two weeks this time, or four? Who knows. Maybe things will click in a couple of weeks, or a month, and I'll look back on this and laugh. I mean, the absurdity of it all makes me crack a smile now, which I'm sure PuTTY would fix. Eh, not one of my better ones.

Robin is among the most compassionate, professional people I've ever met. Not at all to equate what they're going though with my own struggles (as Merlin Mann of 43 Folders and <abbr title="Back to Work">B2W</abbr> fame says, we all have our own personal forms of psychosis), but I <span style="text-decoration:line-through">hope</span> know they'll pull through this too. ♡

