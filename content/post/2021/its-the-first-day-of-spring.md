---
title: "It’s the first day of spring"
date: "2021-09-01T09:13:32+10:00"
abstract: "🍀"
year: "2021"
category: Travel
tag:
- weather
location: Sydney
---
It doesn't feel any warmer on our lockdown balcony, but I'm hotly (hah!) anticipating some milder weather and the pretty foilage. Foliage? There's a Simpsons reference there somewhere. 🍀

I've been back in Australia for almost a decade now, but seasons are still a novelty. In Singapore it was either more damp than normal, or a bit drier than normal. It sure made my wardrobe lighter.

