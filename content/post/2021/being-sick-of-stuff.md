---
title: "Being sick of stuff"
date: "2021-10-21T08:54:21+10:00"
abstract: "Adrian Chiles on “a good 70% is useless stuff”."
year: "2021"
category: Thoughts
tag:
- decluttering
- stuff
location: Sydney
---
I haven't written about decluttering *stuff* for a while. [Adrian Chiles nails it](https://www.theguardian.com/environment/2021/oct/20/we-need-to-stop-buying-stuff-and-i-know-just-the-people-to-persuade-us)\:

> I’m so sick of stuff. Some of it is stuff I really need or that is at least genuinely nice to have, but a good 70% is useless stuff. Clothes I’ll never wear, books I’ll never read, kitchen utensils I’ll never utilise. Items big and small that presumably felt essential the day I bought them but turned out to be quite the opposite. I suppose that as I get older the 70% figure will grow and grow until the morning of the day I shuffle off this mortal coil.

I've got to the point now where every new thing I get must displace something else, in addition to being useful or sparking joy (with apologies to Marie Kondo cynics). That's helped, but it's not making a dent. I need to *rid* our space of stuff again.

Anyone need some VGA cables? I have eight.

