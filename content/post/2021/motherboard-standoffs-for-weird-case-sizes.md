---
title: "Motherboard standoffs for weird case sizes"
date: "2021-07-24T09:35:54+10:00"
abstract: "Thanks Alex Banks from Bit-Tech."
thumb: "https://rubenerd.com/files/2021/5d7f4274-1870-4fc7-9f43-e3b95353f996@1x.jpg"
year: "2021"
category: Hardware
tag:
- compaq-spaceship
- motherboards
location: Sydney
---
Some research on how to fit a MiniITX board into my <del>new</del> old Compaq tower I [blogged about last week](https://rubenerd.com/finally-buying-a-compaq-spaceship/ "Finally buying a Compaq spaceship") lead me to [this page on Bit-Tech](https://bit-tech.net/guides/modding/case-mod/how-to-make-a-motherboard-tray-from-scratch/1/) by Alex Banks:

> One of the points that definitely crosses the mind of folks wishing to jump into the territory of making a case from scratch, is definitely "How do I make everything line up?"

Alex recommends using a tray from an existing case where you can. If those aren't available, he made a series of templates you can print out at 1:1 scale to create your own trays, or even just use to drill motherboard mounts and standoffs in an existing tray.

*Très bien!* Ah, so good (lah). 🇫🇷🇸🇬

<p><img src="https://rubenerd.com/files/2021/5d7f4274-1870-4fc7-9f43-e3b95353f996@1x.jpg" srcset="https://rubenerd.com/files/2021/5d7f4274-1870-4fc7-9f43-e3b95353f996@1x.jpg 1x, https://rubenerd.com/files/2021/5d7f4274-1870-4fc7-9f43-e3b95353f996@2x.jpg 2x" alt="Templates of ATX, Micro-ATX, and Mini-ITX trays by Alex Banks" style="width:500px; height:267px;" /></p>
