---
title: "Martin Fowler on the impact of software"
date: "2021-10-11T14:30:11+10:00"
abstract: "We carry a duty and responsibility."
year: "2021"
category: Software
tag:
- ethics
- privacy
location: Sydney
---
Martin opened his [most recent blog post](https://martinfowler.com/articles/2021-responsible-tech-playbook.html "Responsible Tech Playbook") with this:

> Those of us developing software don’t need to be told what a big impact it’s had on humanity this century. I’ve long maintained that this places a serious responsibility on our profession. Whether asked to or not, we have a duty to ensure our systems don’t degrade our society.

See: Bitcoin.
