---
title: "Japanese Friendship Garden in San Jose"
date: "2021-04-27T17:24:44+10:00"
abstract: "I didn’t know this place existed!"
thumb: "https://rubenerd.com/files/2021/lower-pond-japanese-friendship-garden@1x.jpg"
year: "2021"
category: Travel
tag:
- california
- san-francisco
- san-jose
- united-states
location: Sydney
---
I only went briefly to San Jose a couple of times when I was [working in San Francisco](https://rubenerd.com/180-montgomery-street-san-francisco/), but I would have made the trip here for sure if I knew. It look beautiful and, most importantly, it's prime desktop background material.

<p><img src="https://rubenerd.com/files/2021/lower-pond-japanese-friendship-garden@1x.jpg" srcset="https://rubenerd.com/files/2021/lower-pond-japanese-friendship-garden@1x.jpg 1x, https://rubenerd.com/files/2021/lower-pond-japanese-friendship-garden@2x.jpg 2x" alt="Photo of the Lower Pond in the Japanese Friendship Garden" style="width:500px" /></p>

Thanks to Maya Visvanathan for [sharing this on Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Lower_pond_at_Japanese_Friendship_Garden_in_San_Jose.JPG).
