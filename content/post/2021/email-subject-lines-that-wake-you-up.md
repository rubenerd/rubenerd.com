---
title: "Email signatures that wake you up"
date: "2021-03-27T09:48:36+11:00"
abstract: "“Technical debt so bad I quit my job”"
year: "2021"
category: Internet
tag:
- email
location: Sydney
---
I got an email pop up on my phone:

> Technical debt so bad I quit my job

Seeing that subject line made me jump out of my skin. Surely our company doesn't have technical debt that bad that one of my colleagues quit! Who was it? Which department? Did they give notice? Am I suddenly going to get work on my birthday!?

It was a hypothetical from the GoRails website.

