---
title: "Something incoherent for the week’s end"
date: "2021-09-03T16:16:16+10:00"
abstract: "The Church of Sub/Ungenius"
year: "2021"
category: Thoughts
tag:
- language
- pointless
location: Sydney
---
There are times at the end of some days where, synapses firing, I think back on the week that was and am overcome with the desire and motivation to smash out a dozen blog posts from all manner of topics. Did you know topics is an anagram for cipots, which [isn't a word](https://1word.ws/cipots)?

Other days, such as this one, are different. It's the late afternoon and I'm staring down Outlook with six draft emails and a rapidly approaching Friday evening. I think back to all that's happened in the last week, and struggle to think of anything specific to mention that hasn't already been said. My drafts folder and #TODO file overflows with topics, and I can't bring myself to discuss anything in particular.

The word particular itself is peculiar, just like the world peculiar. Particular sounds like particulate, which could go in the direction of particles or articulate. Articulate particles sounds like a backhanded compliment for a literary genius of below-average stature, which wouldn't affect a literary ungenius of above-average stature such as myself. Is ungenius a word? I feel asking that already disqualifies me from being a genius.

Ungenius. **Harrison Ford**. *Get me off this genius.*

Thank you.
