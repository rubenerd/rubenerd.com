---
title: "What if we’re supposed to feel it?"
date: "2021-07-20T23:30:23+10:00"
abstract: "A thought over mouthwash gargling."
year: "2021"
category: Thoughts
tag:
- philosophy
- personal
location: Sydney
---
Here's a thought. What if that self-doubt we're feeling at the moment is what we're *supposed* to be thinking? What if that negative emotion is the impetus or spark to do something different, or to tackle the causes for that negativity?

This is among the most half-baked ideas I've ever posted here. It came to me while brushing my teeth before bed, but I wanted to jot it down before I forgot. On a [new keyboard](https://rubenerd.com/the-microsoft-ergonomic-keyborad-2019/ "The Microsoft Ergonomic Keyboard 2019"), no less.

Does it count as jotting if I typed it? Yes.

