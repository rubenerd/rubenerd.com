---
title: "Even Nintendo is shipping OLEDs now"
date: "2021-07-07T09:33:01+10:00"
abstract: "Sucks to be among the people who can’t use them."
year: "2021"
category: Thoughts
tag:
- accessibility
- displays
- games
- oleds
- phones
location: Sydney
---
The latest Nintendo Switch uses an OLED screen. Most of tech press and social media are tripping over themselves with excitement at this development, saying it'll improve battery life, colour reproduction, and weight. They're right.

Here comes the proverbial posterial prognostication: *BUT*... the lack of reporting on the accessibility concerns of these panels continues to astonish me! Can you imagine if a new battery technology caused electrical shocks to certain people, and it was just ignored?

These horrible panels of death cause headaches, migraines, and eye fatigue to a significant amount of the population (I've seen estimates of 5–10%, though I've never read that figure formally quantified). Even a conservative estimate places that at millions of people. If I wanted a strobing screen shooting knifes into my eyes, I'd... 

... wait, I can't think of a single situation where I'd ever want that. Unless it was to win a lifetime coffee subscription, and there was a trophy at the end full of codeine and jars of Tiger Balm to salve the inevitable head throbbing from looking at such a screen. Actually, I'd rather pay for coffee.

We can only hope Nintendo will continue shipping the previous LCD version, just as Apple still sells their low-end iPhone SE with a usable display. But it's one more data point in a wider industry trend away from caring about accessibility. I'll keep calling it out whenever I see it... as long as it's not published on an OLED, and I'll have to get someone to read it to me.

