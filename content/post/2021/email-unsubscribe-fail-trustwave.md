---
title: "Email unsubscribe fail: Trustwave"
date: "2021-02-26T10:23:57+11:00"
abstract: "Needing confirmation to unsubscribe, needing to type your email address, and being told it will take two days to process."
year: "2021"
category: Internet
tag:
- email
- spam
- unsubscribe
location: Sydney
---
We haven't done an email unsubscribe adventure for a while! It's been a bit of a feature here since I first got that wall of text from [CafePress in 2006](https://rubenerd.com/spam-unsubscribe-gives-cafepresss-life-story/), and I even used to have a [blog annexe](https://rubenerd.com/tag/from-unsub-me-already/) dedicated to it. There are still so many bad practices around extrication of emails from marketing lists, many of which you may or may not have opted into in the first place.

Here was the footer from a Trustwave marketing email:

> **Change Your Preferences** or **Unsubscribe**

This is how it should be done. There's a clear, unambiguous unsubscribe link that takes you directly to a page to process your request. It doesn't require you to log in, or use a euphemism for *Unsubscribe* to evade email filters.

But then it falls apart. The resulting page says this:

> Ware sorry to see you go. Are you sure you want to unsubscribe from all Trustwave marketing emails? You will no longer receive notifications about security topics and services specifically selected for you. Go back and update your email preferences to select the types of communications you get from us.
> 
> If you decide to unsubscribe, we'd appreciate your feedback.
> 
> Email confirmation:   
> Email Address: [textbox]

The *only* acceptable outcome from clicking an unsubscribe link is to be unsubscribed. These are all redundant:

* Needing confirmation to unsubscribe
* Needing to type your email address
* Being told it will take two days to process

Therefore, this is a fail.

