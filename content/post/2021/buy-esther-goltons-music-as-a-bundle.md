---
title: "Buy Esther Golton’s music as a bundle!"
date: "2021-04-19T20:39:31+10:00"
abstract: "Please, you owe it to yourself ♡."
thumb: "https://rubenerd.com/files/2021/esther-discography.png"
year: "2021"
category: Media
tag:
- esther-golton
- music
- whole-wheat-radio
location: Sydney
---
It's [Music Monday](https://rubenerd.com/tag/music-monday/) time! Each and every Monday without fail, except when I fail, I discuss and share music here that makes me especially happy.

Today's is especially important. Esther Golton has **[bundled her entire discography on Bandcamp for US $17.53](https://esthergolton.bandcamp.com/album/stay-warm#buyFullDiscography)**, or you can choose your price. I cannot overstate how much you owe it to yourself right now to stop what you're doing and buy this. No really, this page will still be here when you get back.

<p><a href="https://esthergolton.bandcamp.com/album/stay-warm#buyFullDiscography"><img src="https://rubenerd.com/files/2021/esther-discography.png" alt="View Esther's discography in Bandcamp" style="width:426px; height:292px;" /></a></p>

Each of her albums has its own character:

* *[Unfinished Houses](https://esthergolton.bandcamp.com/album/unfinished-houses)* is fun and just a little cheeky, and *Going to Shadu* is still one of my all-time favourite songs.

* *[Stay Warm](https://esthergolton.bandcamp.com/album/stay-warm)* is her most recent mix of beautiful instrumentation and vocals. It gets a lot of play in our office now.

* Meditating to the the jaw-dropping, lucid sounds of *[Aurora Borealis](https://esthergolton.bandcamp.com/album/aurora-borealis-conversations-with-alaskas-northern-lights)* has become a ritual for dealing with anxiety, and is my go-to for listening in the evening with Clara.

Please consider [throwing some money Esther's way](https://esthergolton.bandcamp.com/album/stay-warm-unreleased?from=fanpub_bfd#buyFullDiscography), and enjoy. ♡

