---
title: "Women’s mental exhaustion in IT"
date: "2021-03-17T08:59:33+11:00"
abstract: "With quotes from Hey Georgie and an interview with Tim Berners-Lee."
year: "2021"
category: Software
tag:
- ethics
- work
location: Sydney
---
Georgina of *[Hey Georgie](https://hey.georgie.nu/)* wrote one of [the more poignant posts](https://hey.georgie.nu/iwd2021/) about International Women's Day I've ever read:

> In 2019, I wrote about how there is nothing surprising about a woman in tech, and how it’s mentally exhausting being the minority. Two years later, unfortunately, I still feel the same. It’s exhausting being a woman. I’m tired of it. Many women work hard to fight for their cause and to be treated equally to men. Having conversations to fight for equality is tiring. We do the research, we point out the facts, we use our voices. We deserve more.

We won a contract with a wife and husband team once, on the basis that I spent more time talking to her instead. She was the engineer, but she said every other firm directed technical questions at her husband who kept their books and didn't know Linux from a brand of detergent. This one example illustrated to me how entrenched and self-defeating this is. Those other companies *literally* left money on the table by not according a potential customer with the attention she deserved.

We're beyond the point where people in positions of influence can feign ignorance. Treating women as lesser than men in any position, let alone in a professional setting, is an overt and deliberate decision, and deserves being called out. As Georgina says, they deserve more.

Caucasian men like me also need to be better advocates. I had a difficult childhood, and I worked hard to get where I am, but that only makes me appreciate *how much more difficult* it must be for people starting with even less. Everyone deserves the opportunity to shine, and it starts with an acknowledgment that our gender is immaterial to technical competency.

But even then I don't think I'm properly acknowledging it, especially in our industry. [Tim Berners-Lee put it well](https://www.theguardian.com/lifeandstyle/2021/mar/15/tim-berners-lee-we-need-social-networks-where-bad-things-happen-less) for an interview he did with John Harris on The Guardian:

> The letter pointedly refers to “the toxic internet”. Can he explain what that means to him?
> 
> "[..] If you’re a woman, you can have a fine time until the one moment you get picked on. And it can be psychologically very, very extreme. The toxic internet is something that young people have less ability to manage. Women will be more susceptible. LGBTQ people, typically, are much more likely to be picked as a target. The toxic internet isn’t something I experience at all. You may not.”
