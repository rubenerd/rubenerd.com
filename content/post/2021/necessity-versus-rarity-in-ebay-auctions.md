---
title: "Necessity versus rarity in online auctions"
date: "2021-02-27T09:53:51+11:00"
abstract: "Just because it’s rare, doesn’t mean I should get it. Right? R-right?"
year: "2021"
category: Hardware
tag:
- budgeting
- commodore
- shopping
location: Sydney
---
I've had saved eBay search for *seven years* for a specific piece of vintage computer gear. It'd almost become a joke at that stage, yet I kept it around on the remote chance that someone, someday, would be wanting to part with it and willing to sell it to me.

eBay alerts me almost every day with emails, and they're usually either garbage, or the price isn't right. So I delete them from my inbox and move on. It's become a morning routine alongside reading RSS feeds.

So imagine my surprise when I saw an email for this specific item I've been waiting for since 2014! I almost deleted it on a reflex before realising the gravity of the subject line. I felt like one of those policeman on *Forensic Files* who got a hit in CODIS after running a latent print throughout his career. What were the chances that this automated system, churning away for all those years, would finally return something!?

Turns out, we can calculate that. Assuming roughly seven years, that's one day out of 2,555. That's about a 0.04% chance! That's flawed in so many ways, but it does quantitatively communicate just how flabbergasted I was at finding this thing.

But then reality set in. It commanded a high price, as one would expect for a rare item. But then infinitely worse, it was still a price I could just afford.

I wouldn't in my wildest dreams spend that much money on any other device like that, or even half that, or a quarter. My discretionary [budget envelope](https://en.wikipedia.org/wiki/Envelope_system) for February had already been spent on other Commodore computer gear. I don't put things on credit that I haven't budgeted for and have the cash to cover (I only use them for points), so I'd imagine the buyers remorse I'd feel for tapping into savings to buy this thing would be swift and harsh.

There's no rational, logical reason to get this thing. But then the mental bargaining begins: will I have to wait another seven years before I see this again? Will I be kicking myself that I let it go? Won't Murphy's Law ensure that a cheaper version will appear as soon as I buy this one? Or... who cares, these have been difficult months, and it's my birthday soon, I deserve it! Right? R-right?

Envelope budgeting is the strongest method I've found to resisting temptation; another is to think specifically of opportunity costs. If I didn't get this thing, I could have all these other experiences and extra vintage computer stuff. Because if I *did* get this expensive, silly thing, I'd be living in self-imposed austerity for the next month. And I'll bet I'd come to resent it.

It's all self-indulgent nonsense. But then, that's what hobbies are, right?
