---
title: "Wordpress.com’s password advice"
date: "2021-10-22T08:19:44+10:00"
abstract: "Needs updating."
year: "2021"
category: Internet
tag:
- passwords
- security
location: Sydney
---
Wordpress.com remains one of the better blog platforms out there for people looking to get out of relying on social media to spread their ideas. The UI is clunky by today's standards, but they score highly for me for their easy import and export tools, and their advocacy of open web standards. I've said before, but [Ma.tt](https://ma.tt/) gives me a hopeful counterpoint to The Zuck for my generation.

Unfortunately, this doesn't extend to their password advice. Signing up a new account for a friend last night shows this messages:

> Great passwords use upper and lowercase characters, numbers, and symbols like !"£$%&.

This advice is incomplete at best, and increasingly runs contrary to accepted best practices and current advice as published by security agencies. Short, difficult to remember passwords with gibberish are less secure than longer passphrases consisting of a string of simple words. Even a password haystack, in which you pad a simpler password with repitition of one or more characters, is more secure on account of it being harder to brute force.

Back to Wordpress.com, pasting a pseudo-random string of gibberish from [KeePassXC](https://keepassxc.org/) (an excellent, open source passphrase manager I encourage everyone to try and [donate to](https://keepassxc.org/donate/)!) also returned this error:

> Passwords may not contain the character "\\".

If this is being salted and hashed, the character here shouldn't matter. Mmm, salted hash browns. Perhaps they're avoiding character escaping, but that shouldn't be necessary. I did a quick check of some other passwords I have stored, and other sites are able to accept gibberish with backslashes.

The best thing the industry could do is replace passphrases with something more robust. The next best thing is password managers that automate password entry. After that, it's encouraging people to use longer, easier to remember passwords.

Wordpress.com runs so many sites, they're in a powerful position to advocate for better, more practical security for everyone. As one of the more ethical citizens of the modern web, I hope they consider it.
