---
title: "Instapaper links for January 2013"
date: "2013-01-30"
year: 2013
category: Annexe
tag:
- from-instapaper
- listpost
location: Sydney
---
Links I saved onto [Instapaper](https://rubenerd.com/tag/from-instapaper/) this month:

* [Microsoft's aching Windows 8 hangover](http://m.infoworld.com/d/the-industry-standard/microsofts-aching-windows-8-hangover-209878?source=twitter)<br />Infoworld

* [The tip of the spear](http://www.lamag.com/features/2012/12/18/the-tip-of-the-spear)<br />Los Angeles Magazine

* [Oldest Swiss bank to close after guilty plea](http://www.dw.de/oldest-swiss-bank-to-close-after-guilty-plea/a-16497384?maca=en-TWITTER-EN-2004-xml-mrss)<br />Deutsche Welle

* [Fiscal cliff fatigue? You're not alone](http://m.guardian.co.uk/commentisfree/2013/jan/02/fiscal-cliff-fatigue-oliver-burkeman)<br />The Guardian

* [No, I Will Not Go To The Beach With You](http://thoughtcatalog.com/2012/no-i-will-not-go-to-the-beach-with-you/)<br />Thought Catalog

* [Cameron 'entitled' to use threat of veto to win back powers from Europe](http://m.guardian.co.uk/politics/2013/jan/06/david-cameron-entitled-threat-europe)<br />The Guardian

* [Blaming Rape Victims in Cambodia](http://globalvoicesonline.org/2013/01/06/cambodia-blaming-the-rape-victims/)<br />Global Voices

* [How a gift economy powers education in rural Nepal](http://www.shareable.net/blog/how-a-gift-economy-powers-education-in-rural-nepal)<br />Shareable

* [Developer divide: 19 generations of computer programmers](http://m.infoworld.com/d/application-development/developer-divide-19-generations-of-computer-programmers-209971)<br />Infoworld

* [Confusing Value And Price, Choir Demands £3000 Per Download](http://www.techdirt.com/blog/casestudies/articles/20121228/12224621513/confusing-value-price-choir-demands-3000-per-download.shtml)<br />Techdirt

* [Japan Airlines' Jet Suffers Fire in Boston](http://online.wsj.com/article/SB10001424127887323482504578227621155767836.html)<br />Wall Street Journal

* [Alitalia investors consider possible sale: sources](http://mobile.reuters.com/article/idUSBRE9060ET20130107?irpc=932)<br />Reuters

* [Republican staffer fired for copyright memo talks to Ars](http://arstechnica.com/tech-policy/2013/01/republican-staffer-fired-for-copyright-memo-talks-to-ars/)<br />Ars Technica

* [Oxfam's new Africa campaign reveals a misguided messiah complex](http://m.guardian.co.uk/world/2013/jan/07/oxfam-campaign-africa-aid)<br />The Guardian

* [Why Russia Won’t Play Ball on Iran](http://thediplomat.com/2012/06/23/why-russia-wont-play-ball-on-iran/)<br />The Diplomant

* [John McAfee's Last Stand](http://www.wired.com/threatlevel/2012/12/ff-john-mcafees-last-stand/all/)<br />Wired

* [Schneier on Security: The Politics and Philosophy of National Security](http://www.schneier.com/blog/archives/2013/01/the_politics_of_2.html)<br />Bruce Schneier

* [PSO wannabe linked to arms dealer](http://www.theage.com.au/national/pso-wannabe-linked-to-arms-dealer-20130110-2cj3t.html)<br />The Age

* [Herbalife: from high-energy clubs to dashed dreams](http://www.nbcnews.com/business/herbalife-high-energy-clubs-dashed-dreams-1B7899747?ocid=twitter)<br />Business on NBCNews.com

* [Five federal policies on guns you never heard of](https://www.propublica.org/article/five-federal-policies-on-guns-you-never-heard-of)<br />Propublica

* [H.265 Is Coming, And Bringing With It Truly High-Def Video That Won’t Kill The Network](http://techcrunch.com/2013/01/10/rovi-divx-ces-2013/)<br />Techcrunch

* [Stem Cell Showdown: Celltex vs. the FDA](http://mobile.businessweek.com/articles/2013-01-03/stem-cell-showdown-celltex-vs-dot-the-fda)<br />Businessweek

* [The Cold Hard Facts of Freezing to Death](http://www.outsideonline.com/outdoor-adventure/As-Freezing-Persons-Recollect-the-Snow--First-Chill--Then-Stupor--Then-the-Letting-Go.html?page=all)<br />Outside Online

* [Top tip if you're going out in Beijing: don't breathe](http://www.guardian.co.uk/world/2013/jan/13/beijing-breathe-pollution?CMP=twt_gu)<br />The Guardian

* [Let the facts speak for themselves in 2013](http://www.abc.net.au/unleashed/4463186.html)<br />Australian Broadcasting Corporation

* [The Shady Inside Deals That Are Protecting Goldman Sachs at Your Expense](http://www.alternet.org/economy/shady-inside-deals-are-protecting-goldman-sachs-your-expense)<br />Alternet

* [Starbucks Vietnam Debut Challenged by Light Coffee Image](http://www.bloomberg.com/news/2013-01-13/starbucks-vietnam-debut-challenged-by-light-coffee-image.html)<br />Bloomberg

* [India, China military officials hold Annual Defence Dialogue](http://sp.m.timesofindia.com//articleshow/18016305.cms)<br />The Times of India

* [On the uselessness of nuclear weapons](http://blogs.scientificamerican.com/the-curious-wavefunction/2013/01/13/on-the-uselessness-of-nuclear-weapons/)<br />The Curious Wavefunction, Scientific American Blog Network

* [How to Fix India's Male-Superiority Complex](http://www.theatlantic.com/international/archive/2013/01/how-to-fix-indias-male-superiority-complex/267093/)<br />The Atlantic

* [The saga of David Cameron's Europe speech exposes his strategic failings](http://www.guardian.co.uk/commentisfree/2013/jan/15/david-cameron-europe-speech-strategy?CMP=twt_gu)<br />The Guardian

* [Notebook on Cities and Culture S3E4: Ashukurafuto with Brian Ashcraft](http://blog.colinmarshall.org/?p=1263)<br />Colin Marshall

* [Positive Procrastination, Not an Oxymoron](http://undrip.com/d/i5ZTa/positive-procrastination-not-an-oxymoron/?r=yf7PE&sn_id=4)<br />Undrip

* [White paper chronicles rise of ramen worldwide](http://www.japantimes.co.jp/text/nn20130116a7.html)<br />The Japan Times Online

* [Singapore watchdog calls for F&N auction](http://www.france24.com/en/20130116-singapore-watchdog-calls-fn-auction)<br />FRANCE 24

* [NASA Satellite Image Shows Beijing Drowning in a Lake of Smog](http://www.theatlanticcities.com/neighborhoods/2013/01/nasa-satellite-image-shows-beijing-drowning-lake-smog/4397/)<br />The Atlantic Cities

* [The problem with hybrids -- it's the tablet](http://www.zdnet.com/the-problem-with-hybrids-its-the-tablet-7000009828/)<br />ZDNet

* [How Australia's post-massacre gun control has worked, and saved lives](http://www.theage.com.au/opinion/politics/guns-policy-saving-lives-20130114-2cpny.html)<br />The Age

* [Brooklyn's Affordability Crisis Is No Accident](http://www.theatlanticcities.com/technology/2013/01/brooklyns-affordabilty-crisis-no-accident/4401/)<br />The Atlantic Cities

* [NATO Patriot missiles arrive in Turkey to counter Syria risks](http://www.reuters.com/article/2013/01/21/us-syria-crisis-nato-idUSBRE90K0K820130121)<br />Reuters

* [In Historic Inauguration Address, Obama Takes the Fight to the Right](http://www.alternet.org/news-amp-politics/historic-inauguration-address-obama-takes-fight-right)<br />Alternet

* ['Curious' Cuban net cable has activated, researchers say](http://www.bbc.co.uk/news/technology-21120786)<br />BBC News

* [The wrong goodbye of Barnes and Noble](http://www.mhpbooks.com/the-slow-death-of-barnes-and-noble/)<br />MobyLives

* [This man helped save six children, is now getting harassed for it](http://www.salon.com/2013/01/15/this_man_helped_save_six_children_is_now_getting_harassed_for_it/)<br />Salon.com

* [Polls close in Israeli elections](http://www.abc.net.au/news/2013-01-23/polls-close-in-israeli-elections/4479654)<br />Australian Broadcasting Corporation

* [David Cameron To Propose EU Referendum Tomorrow](http://www.zerohedge.com/news/2013-01-22/david-cameron-propose-eu-referendum-tomorrow)<br />Zero Hedge

* [China Unveils Ambitious Clean Energy Plan For 2013](http://www.the9billion.com/2013/01/21/china-unveils-ambitious-clean-energy-plan-2013/)<br />The 9 Billion

* [Harry Reid Might Change the Rules of the Senate. Here's What You Need to Know](http://www.motherjones.com/politics/2013/01/change-senate-rules-filibuster-reform-explained)<br />Mother Jones

* [US Airways' profit soars amid strong demand](http://www.usatoday.com/story/todayinthesky/2013/01/23/us-airways-quarterly-profit/1857955/?dlvrit=110940)<br />USA Today

* [The Conventional Wisdom About Government Health Care Spending Is Wrong](http://www.theatlantic.com/business/archive/2013/01/the-conventional-wisdom-about-government-health-care-spending-is-wrong/267378/)<br />The Atlantic

* [Is buying a home cheaper than renting?](http://www.brw.com.au/p/is_buying_home_cheaper_than_renting_JkToBx7LnrP7lazjLmmTVJ)<br />BRW

* [How to uncover closed-door restaurants when travelling](https://www.lonelyplanet.com/europe/travel-tips-and-articles/77622?affil=twit)<br />Lonely Planet

* [3 things I wish I knew when we got married](http://www.relevantmagazine.com/life/relationships/3-things-i-wish-i-knew-we-got-married)<br />Relevant Magazine

* [Catalan parliament paves way for referendum on independence](http://www.guardian.co.uk/world/2013/jan/23/catalonian-parliament-referendum-independence-spain)<br />The Guardian

* [Asia's Other Island Spat...Between Japan and Russia](http://thediplomat.com/2013/01/24/history-aside-a-russian-japan-rapprochement/)<br />The Diplomant

* [China's Water Pollution Crisis](http://thediplomat.com/2013/01/22/forget-air-pollution-chinas-has-a-water-problem/)<br />The Diplomat

* [Singapore retailers splurging on renovation & re-branding](http://www.channelnewsasia.com/stories/singaporebusinessnews/view/1249994/1/.html)<br />Channel NewsAsia

* [Republicans push to change electoral college in swing states](http://www.salon.com/2013/01/25/republicans_push_to_change_electoral_college_in_swing_states/)<br />Salon.com

* [Are You Comfortable with 3-Year Prison Terms for Saggy Pants?](http://www.theatlantic.com/national/archive/2013/01/are-you-comfortable-with-3-year-prison-terms-for-saggy-pants/272517/)<br />The Atlantic

* [Beijing watching Tokyo for moves to improve ties, Tang tells Yamaguchi](http://www.japantimes.co.jp/news/2013/01/25/national/beijing-watching-tokyo-for-moves-to-improve-ties-tang-tells-yamaguchi/)<br />The Japan Times

* [Apple lists Quanta Computer in Fremont California as Final Assembler for Macs](http://9to5mac.com/2013/01/25/apple-lists-quanta-computer-in-freemont-california-as-final-assembler-for-macs/)<br />9to5Mac

* [Election loss forces Singapore into a delicate balancing act](http://www.reuters.com/article/2013/01/27/us-singapore-politics-idUSBRE90Q05X20130127)<br />Reuters

* [Literary protest: Local hero](http://www.economist.com/blogs/analects/2013/01/literary-protest?fsrc=scn%2Ftw%2Fte%2Fbl%2Flocalhero)<br />The Economist

* [It is now a crime to unlock your smartphone](http://www.theatlantic.com/business/archive/2013/01/the-most-ridiculous-law-of-2013-so-far-it-is-now-a-crime-to-unlock-your-smartphone/272552/)<br />The Atlantic

* [France suspends acne drug used as contraceptive](http://www.channelnewsasia.com/stories/health/view/1251207/1/.html)<br />Channel NewsAsia

* [Complexity and Security](http://www.schneier.com/blog/archives/2013/01/complexity_and.html)<br />Schneier on Security

* [Game to destroy CCTV cameras: vandalism or valid protest?](http://www.guardian.co.uk/theguardian/shortcuts/2013/jan/25/game-destroy-cctv-cameras-berlin)<br />The Guardian

* [Winners for Apps My State in Victoria Show the Limits of Application Contests](http://blogs.gartner.com/andrea_dimaio/2010/06/22/winners-for-apps-my-state-in-victoria-show-the-limits-of-application-contests/)<br />Gartner Blogs

* [App.net moves beyond its ad-free Twitter alternative, adding 10 GB of storage to share](http://www.theverge.com/2013/1/28/3924352/app-net-adds-dropbox-twitter-with-10gb-storage-data-API)<br />The Verge

* [Join](https://join.app.net/)<br />App.net

* [Michael Dell Seeking Majority Control Of Dell Inc., Contributing As Much As $1 Billion Of His Own Personal Funds](http://t.co/mtKJ7vTH)<br />TechCrunch

* [A call for civil discussion](http://skepticink.com/justinvacula/2013/01/30/a-call-for-civil-discussion/)<br />Skeptic Ink

* [Abe First Leader Since Koizumi to See Support Rise After 1 Month](http://www.bloomberg.com/news/2013-01-28/abe-first-leader-since-koizumi-to-see-support-rise-after-1-month.html)<br />Bloomberg

* [Do We Really Want to Live Without the Post Office? - Esquire](http://www.esquire.com/print-this/post-office-business-trouble-0213?page=all)<br />Esquire

