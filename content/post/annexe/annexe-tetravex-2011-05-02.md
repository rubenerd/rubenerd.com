---
title: "Tetravex for 2011-05-02"
date: "2011-05-02T00:00:00+09:30"
abstract: "Back when I used to record daily puzzles."
year: "2011"
category: Annexe
tag:
- from-my-puzzles-blog
- tetravex
---
<p style="font-style:italic;">This originally appeared on the Annexe, back when I recorded daily puzzles.</p>

<figure><p><img src="https://rubenerd.com/files/museum/tetravex-2011-05-02.png" alt="Sudoku puzzle for 2011-05-02" style="width:126px; height:126px; image-rendering:pixelated" /></p></figure>
