---
title: "Sudoku for 2009-09-20"
date: "2009-09-20T00:00:00+09:30"
abstract: "Back when I used to record daily puzzles."
year: "2009"
category: Annexe
tag:
- from-my-puzzles-blog
- sudoku
---
<p style="font-style:italic;">This originally appeared on the Annexe, back when I recorded daily puzzles.</p>

<figure><p><img src="https://rubenerd.com/files/museum/sudoku-2009-09-20.png" alt="Sudoku puzzle for 2009-09-20" style="width:206px; height:206px; image-rendering:pixelated;" /></p></figure>
