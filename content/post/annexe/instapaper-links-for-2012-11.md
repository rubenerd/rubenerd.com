---
title: "Instapaper links for November 2012"
date: "2012-11-30"
year: 2012
category: Annexe
tag:
- from-instapaper
- listpost
location: Sydney
---
Links I saved onto [Instapaper](https://rubenerd.com/tag/from-instapaper/) this month:

* [The world before Wikipedia - an honestly horrible vision](http://marshallk.posterous.com/the-world-before-wikipedia-an-honestly-horrib)<br />Marshall's Curated FabulousNess

* [UK's Blair says EU exit would be monumental error](http://mobile.reuters.com/article/idUSBRE8AR0OD20121128?irpc=932)<br />Reuters

* [What I Learned from a Night Editing Wikipedia](http://marshallk.com/what-i-learned-from-a-night-editing-wikipedia)<br />Marshall Kirkpatrick's Blog

* [Killer blueberries: Inside the reality of paranoia](http://blogs.scientificamerican.com/literally-psyched/2012/11/20/killer-blueberries-inside-the-reality-of-paranoia/)<br />Scientific American

* [Israel Destroys the Wrong House in Gaza](http://www.newyorker.com/online/blogs/closeread/2012/11/the-wrong-house-in-gaza.html?mobify=0)<br />The New Yorker

* [10 Reasons the GOP Is Really Messed Up, According to Republicans](http://www.alternet.org/news-amp-politics/10-reasons-gop-really-messed-according-republicans)<br />Alternet

* [Why Is Belarus the Only Country Where Opera Is the Most Popular Browser?](http://m.theatlantic.com/technology/archive/12/11/why-is-belarus-the-only-country-where-opera-is-the-most-popular-browser/265406/)<br />The Atlantic

* [Micro-Apartments So Nice You'll Wish Your Place Was This Small](http://m.theatlanticcities.com/housing/2012/11/micro-apartments-so-nice-youll-wish-your-place-was-small/3932/)<br />The Atlantic Cities

* [Annals of Archibabble](http://blogs.crikey.com.au/theurbanist/2012/11/19/annals-of-archibabble/)<br />Crikey

* [Wikipedia failing to recruit any new admins](http://www.theregister.co.uk/2012/07/18/wiki_admins_dwindle/)<br />The Register

* [3 Charts That Show How Wikipedia Is Running Out of Admins](http://m.theatlantic.com/technology/archive/2012/07/3-charts-that-show-how-wikipedia-is-running-out-of-admins/259829/)<br />The Atlantic

* [Learn a language in three months](http://m.guardian.co.uk/education/2012/nov/09/learn-language-in-three-months)<br />The Guardian

* [FreeBSD takes another step toward GPL escape](http://m.techrepublic.com/blog/australia/freebsd-takes-another-step-toward-gpl-escape/1534)<br />TechRepublic

* [Legal Lesson Learned: Copywriter Pays $4,000 for $10 Photo](http://blog.webcopyplus.com/2011/02/14/legal-lesson-learned-copywriter-pays-4000-for-10-photo/)<br />Webcopyplus Web Copywriter Blog

* [The Secret History of Guns](http://m.theatlantic.com/magazine/print/2011/09/the-secret-history-of-guns/308608/)<br />The Atlantic

* [Jonah Lehrer Wasn’t the Only Journalist Shaping His Conclusions](http://nymag.com/news/features/jonah-lehrer-2012-11/)<br />New York Magazine

* [Monopoly Is Theft](http://harpers.org/blog/2012/10/monopoly-is-theft/?single=1)<br />Harper's Magazine

* [iPad mini review](http://mobile.theverge.com/2012/10/30/3576178/apple-ipad-mini-review)<br />The Verge

* [Microsoft's meaningless RT license could sink Surface, Tablets](http://m.infoworld.com/t/tablets/microsofts-meaningless-rt-license-could-sink-surface-205931)<br />InfoWorld

* [The gender fault-line in Japan](http://www.eastasiaforum.org/2012/11/03/the-gender-fault-line-in-japan/)<br />East Asia Forum

* [Our American endorsement: Which one?](http://www.economist.com/news/leaders/21565623-america-could-do-better-barack-obama-sadly-mitt-romney-does-not-fit-bill-which-one?fsrc=scn%2Fgp%2Fwl%2Fpe%2Fwhichone)<br />The Economist

