---
title: "Instapaper links for February 2013"
date: "2013-02-28"
year: 2013
category: Annexe
tag:
- from-instapaper
- listpost
location: Sydney
---
Links I saved onto [Instapaper](https://rubenerd.com/tag/from-instapaper/) this month:

* [The vast differences between the NBN and the Coalition's alternative](http://www.abc.net.au/technology/articles/2013/02/21/3695094.htm)<br />Australian Broadcasting Corporation

* [Congressman demands answers from Google about Play Store and Wallet privacy concerns](http://mobile.theverge.com/2013/2/21/4013966/congressman-google-play-store-wallet-privacy-concerns)<br />The Verge

* [Japan PM Abe heads for Obama talks](http://www.bbc.co.uk/news/world-asia-21528834)<br />BBC World

* [Oracle updates NetBeans for HTML5](http://www.infoworld.com/d/application-development/oracle-updates-netbeans-html5-213212)<br />Infoworld

* [Bankruptcy filing may be bad option for Detroit](http://feeds.reuters.com/~r/reuters/businessNews/~3/CqiglojlXXM/us-usa-detroit-bankruptcy-idUSBRE91K0HT20130221)<br />Reuters

* [Treacherous Waters - By Ross A. Klein](http://www.foreignpolicy.com/articles/2012/04/05/treacherous_water)<br />Foreign Policy

* [International law allows cruise workers to be paid $2 an hour -- and they may go nearly a year without a day off](http://www.salon.com/2013/02/15/cruise_from_hell_dont_pity_carnivals_passengers/)<br />Salon.com

* [Australia Proves Republicans Wrong With a Growing Economy and a High Minimum Wage](http://www.politicususa.com/australias-minimum-wage-16-hour-economy-growing.html)<br />PoliticusUSA

* [The Internet’s Weakest Links](http://www.bbc.com/future/story/20130214-the-internets-weakest-links)<br />BBC Future

* [Stupid, Stupid xBox!!](http://ilikecode.wordpress.com/2013/02/12/stupid-stupid-xbox/)<br />iLike.code
)

* [1,000 paper cranes + message of #goodbyenukes delivered to President Laura Chinchilla of Costa Rica.](https://www.facebook.com/ippnw/posts/339654979473284)<br />IPPNW

* [Xbox co-founder: The last five years have been painful to watch](http://www.guardian.co.uk/p/3dnf6/tf?CMP=SOCxx2I2)<br />The Guardian

* [What is ADN File Manager?](http://blog.app.net/2013/02/13/what-is-adn-file-manager/)<br />App.net
)

* [The NRA vs. America](http://m.rollingstone.com/?redirurl=/politics/news/the-nra-vs-america-20130131&seenSplash=1)<br />Rolling Stone Mobile

* [Patrick Radden Keefe: A Mass Shooter’s Tragic Past](http://m.newyorker.com/reporting/2013/02/11/130211fa_fact_keefe?currentPage=all)<br />New Yorker

* [Believe the Hype about the 'World's Most Expensive City'](http://www.theatlanticcities.com/jobs-and-economy/2013/02/dont-believe-hype-about-worlds-most-expensive-city/4643/)<br />The Atlantic Cities

* [How Capital Cities Distort Reality](http://www.theatlanticcities.com/politics/2013/02/how-capital-cities-distort-reality/4513/)<br />The Atlantic Cities

* [Friedman: India vs China vs Egypt](http://www.nytimes.com/2013/02/06/opinion/friedman-india-vs-china-vs-egypt.html)<br />New York Times

* [John Lindsay, Ed Koch and the end of liberalism ](http://www.salon.com/2013/02/04/john_lindsay_ed_koch_and_the_end_of_liberalism/)<br />Salon.com

* [Disgruntled employee spends three years destroying work computers with Cillit Bang](http://www.telegraph.co.uk/news/uknews/crime/9845363/Disgruntled-employee-spends-three-years-destroying-work-computers-with-Cillit-Bang.html)<br />The Telegraph

* [Why we stopped using Drupal for our platform](http://blog.varunarora.com/2013/why-we-stopped-using-drupal-for-our-platform/)<br />Varunarora

* [The ALP needs to make it impossible for voters not to notice that “Tony Abbott is trying to pull a fast one”](http://anonymouslefty.wordpress.com/2013/02/02/how-to-win-the-alp-needs-to-make-it-impossible-for-voters-not-to-notice-that-tony-abbott-is-trying-to-pull-a-fast-one/)<br />Anonymous Lefty

* [Meet the NRA's Top 10 Enemies](http://www.alternet.org/tea-party-and-right/meet-nras-top-10-enemies)<br />AlterNet

* [Austerity doesn't work: Cuts in government spending are strangling the recovery](http://www.salon.com/2013/02/01/austerity_economics_is_strangling_the_u_s_economy_partner/)<br />Salon.com

* [China's Smaller Cities Are Home to Growing Middle Class - Michael J. Silverstein](http://blogs.hbr.org/cs/2013/01/chinas_smaller_cities_are_home.html)<br />Harvard Business Review

* [Dana Milbank: Tea party is losing a few of its revelers](http://www.washingtonpost.com/opinions/dana-milbank-tea-party-is-losing-a-few-of-its-revelers/2013/01/29/42d290ce-6a60-11e2-95b3-272d604a10a3_story.html)<br />The Washington Post

