---
title: "Instapaper links for October 2012"
date: "2012-10-30"
year: 2012
category: Annexe
tag:
- from-instapaper
- listpost
location: Sydney
---
Links I saved onto [Instapaper](https://rubenerd.com/tag/from-instapaper/) this month:

* [How Japan Has Virtually Eliminated Shooting Deaths](http://m.theatlantic.com/international/archive/2012/07/a-land-without-guns-how-japan-has-virtually-eliminated-shooting-deaths/260189/)<br />Atlantic Mobile

* [Inside the U.S. Military’s Chemical-Weapons Tests](http://m.newyorker.com/reporting/2012/12/17/121217fa_fact_khatchadourian)<br />The New Yorker

* [PM Lee expresses 'great sadness' over Palmer's resignation](https://sg.news.yahoo.com/pm-lee-accepts-palmer-s-resignation-072509288.html)<br />Yahoo! News Singapore

* [What Happens Now That the War on Drugs Has Failed?](http://nymag.com/news/features/war-on-drugs-2012-12/)<br />New York Magazine

* [The big (and increasing) difference between data and discovery](http://www.planetary.org/blogs/emily-lakdawalla/2012/12031316-curiosity-kerfuffle.html)<br />The Planetary Society

