---
title: "Tetravex for 2009-09-24"
date: "2009-09-24T00:00:00+09:30"
abstract: "Back when I used to record daily puzzles."
year: "2009"
category: Annexe
tag:
- from-my-puzzles-blog
- tetravex
---
<p style="font-style:italic;">This originally appeared on the Annexe, back when I recorded daily puzzles.</p>

<figure><p><img src="https://rubenerd.com/files/museum/tetravex-2009-09-24.png" alt="Sudoku puzzle for 2009-09-24" style="width:320px; height:169px; image-rendering:pixelated" /></p></figure>
