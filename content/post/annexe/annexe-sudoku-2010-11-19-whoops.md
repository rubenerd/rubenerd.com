---
title: "Sudoku for 2010-11-19, whoops"
date: "2010-11-19T00:00:00+09:30"
abstract: "Back when I used to record daily puzzles."
year: "2010"
category: Annexe
tag:
- from-my-puzzles-blog
- sudoku
- whoops
---
<p style="font-style:italic;">This originally appeared on the Annexe, back when I recorded daily puzzles.</p>

<figure><p><img src="https://rubenerd.com/files/museum/sudoku-2010-11-19-whoops.png" alt="Sudoku puzzle for 2010-11-19" style="width:194px; height:194px; image-rendering:pixelated;" /></p></figure>
