---
title: "Tetravex for 2010-12-13"
date: "2010-12-13T00:00:00+09:30"
abstract: "Back when I used to record daily puzzles."
year: "2010"
category: Annexe
tag:
- from-my-puzzles-blog
- tetravex
---
<p style="font-style:italic;">This originally appeared on the Annexe, back when I recorded daily puzzles.</p>

<figure><p><img src="https://rubenerd.com/files/museum/tetravex-2010-12-13.png" alt="Sudoku puzzle for 2010-12-13" style="width:322px; height:242px; image-rendering:pixelated" /></p></figure>
