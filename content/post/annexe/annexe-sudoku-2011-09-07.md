---
title: "Sudoku for 2011-09-07"
date: "2011-09-07T00:00:00+09:30"
abstract: "Back when I used to record daily puzzles."
year: "2011"
category: Annexe
tag:
- from-my-puzzles-blog
- sudoku
---
<p style="font-style:italic;">This originally appeared on the Annexe, back when I recorded daily puzzles.</p>

<figure><p><img src="https://rubenerd.com/files/museum/sudoku-2011-09-07.png" alt="Sudoku puzzle for 2011-09-07" style="width:342px; height:342px; image-rendering:pixelated;" /></p></figure>
