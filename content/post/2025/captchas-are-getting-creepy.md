---
title: "CAPTCHAs are getting creepy"
date: "2025-01-22T19:12:53+11:00"
abstract: "CAPTCHA showing a disembodied hand and what looks like the head of a ram, maybe?"
year: "2025"
category: Internet
tag:
- accessibility
- captchas
location: Sydney
---
[Speaking of CAPTCHAs](https://rubenerd.com/anticipating-how-captchas-want-me-to-solve-them/ "Anticipating how CAPTCHAs want me to solve them"), what is&hellip; *this!?*

<figure><p><img src="https://rubenerd.com/files/2025/evil-captcha@1x.jpg" alt="CAPTCHA showing a disembodied hand and what looks like the head of a ram with horns." srcset="https://rubenerd.com/files/2025/evil-captcha@2x.jpg 2x" style="width:431px; height:334px;" /></p></figure>

Can we all stop this please? It's getting a *bit* silly.
