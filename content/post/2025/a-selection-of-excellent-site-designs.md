---
title: "A (blue) selection of excellent site designs"
date: "2025-02-09T08:37:11+11:00"
abstract: "Perfect motherfucking websites!"
year: "2025"
category: Internet
tag:
- design
- satire
location: Sydney
---
I've been meaning to write about their excellence for a few years, but I've decided to collate them here for easy reference:

* [Motherfucking Website](https://motherfuckingwebsite.com/). "Seriously, what the fuck else do you want?"

* [Better Motherfucking Website](http://bettermotherfuckingwebsite.com/). "Seriously, it takes minimal fucking effort to improve this shit."

* [Perfect Motherfucking Website](https://perfectmotherfuckingwebsite.com/). "Seriously, some minimal fucking things are needed to make this shit perfect."

There are a few more in that vein&mdash;vain?&mdash;but those are the best ones. The one with the grey text and background is *actually* bad.

Speaking of which!

* [Make Frontend Shit Again](https://makefrontendshitagain.party/). Playful shit, not modern shit! Make useless stuff; make the web fun again!

* [Ruben's Retro Corner](http://retro.rubenerd.com/). Literally lifted from my 1997 "theme". It was more fun to build than anything I've done in years. Fuck!
