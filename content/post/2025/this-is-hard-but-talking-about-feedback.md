---
title: "This is hard, but talking about feedback"
date: "2025-02-14T17:18:30+11:00"
abstract: "I wish I could deal with this, but I can’t, so I'm pausing feedback for now."
year: "2025"
category: Thoughts
tag:
- personal
- weblog
location: Sydney
---
I know I'm a bit silly here on occasion, even when discussing technical topics. But I want to be serious here for a moment, if you'll indulge me.

Writing this blog almost every day for twenty years has been one of the great joys of my life. It's not as impressive as the creative outlets of, say, an artist or a musician, but it feels natural and fun to do. I love researching topics, sharing fun things I've found, walking you through how I've fixed a problem, and on occasion expressing my concerns at the state of the industry or the wider world. Sometimes I even remember to do a spell cheque.

Blogging is a great way to process my thoughts, and to remember things. The fact some people think those thoughts are useful or interesting to *them* too is astonishing and, as cliché as it sounds, humbling beyond belief.

I get amazing comments from people almost every day now, some of whom I even work up the guts to respond to, and within a timely fashion! You're all interesting, intelligent, kind, and above all *patient*, and I'm forever thankful. No really, I mean it.

🌲 🌲 🌲

But then the 5% come along, and they make me want to pack up the entire thing and quit. It's generally when a post of mine has made it to Hacker News, Lobsters, or Reddit, but they can strike at any time. It's not sufficient to disagree with me, or to clarify a point, or to correct a fact. They have to make it personal. Same goes for social media.

Strong people would take this in their stride. Water off a duck's back, stiff upper lip, ignore the trolls, and so on. I aspire to be one of those people. But I'm not; at least, not right now.

I apologise for speaking in euphemisms, but my circumstances over the last few years have been tenuous. I have a support network that's helping me, but extricating myself from these internal and external pressures is proving slow, painful, complicated, hard, and terrifying. You'll understand why I don't want to go into detail, but I suspect many of you have an inkling (and maybe even face a soup of similar issues yourselves).

The universe doesn't owe us good solutions or easy decisions, but I'm forced to attempt them now.

I wish I could say the 95% of you who are wonderful counteract the bile, like a calming antacid to a troubled stomach. But the truth is, those 5% poison the well I drink from, and it's making a difficult situation much worse. For now I've removed the contact section on my About page, and locked my Mastodon account while I figure out what I need to do.

Thanks, as always, for reading. I'll sort this out.
