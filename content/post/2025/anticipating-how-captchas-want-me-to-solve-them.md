---
title: "Anticipating how CAPTCHAs want me to solve them"
date: "2025-01-15T10:46:34+11:00"
abstract: "They don’t want the correct answer, they want an answer they expect a human to provide."
year: "2025"
category: Internet
tag:
- accessibility
location: Sydney
---
A CAPTCHA asked me to select every square with a motorbike this morning. There was just one small problem: the pictured motorised conveyance was a scooter, *not* a motorbike. I've had this happen before where it asks me to select a traffic light, when only the *pole* for a traffic light is visible. Or a bus, where only the front of a van is visible. 

This sent me down a rabbit-hole of thought, from which I'm only starting to extricate myself. I had two options:

* Do I click the button saying no motorbike is visible because... no motorbike is visible? Because a scooter is *not* a motorbike?

* Or do I try to anticipate what the CAPTCHA *expects* me to do instead? Do I try to think like a robot trying to think like a human, instead of thinking like a human?

I went with the latter, and was granted access to the site. CAPTCHAs don't want the correct answer, they want an answer they expect a human to provide.

Perhaps this is a new way CAPTCHAs are trying to beat AI solving tools that make them functionally redundant. CAPTCHAs expect most people to misidentify a scooter as a motorbike, whereas an AI trained on billions of stolen motorbike photos wouldn't make that mistake (well, until it hallucinates a motorbike with wheels on the helmet of the driver. That'd be pretty funny).

What a time to be alive!
