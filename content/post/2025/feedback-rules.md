---
title: "Feedback rules"
date: "2025-02-16T08:52:56+11:00"
abstract: "I wish I didn’t need this, but hopefully it removes ambiguity."
year: "2025"
category: Internet
tag:
- feedback
- trolls
- weblog
location: Sydney
---
Hi! This will be the third and final instalment in my “trolls get to Ruben” series, fun as it has been to live it since last Thursday.

Sadly, it's become clear some ground rules are required for people who want to send me messages. These will do nothing to deter the dregs, but I'm going to codify and link to this anyway so there's no ambiguity. 

1. You can have constructive criticism, disagree with an opinion, or have an alternative technical solution to a problem. **You cannot be a dick, douchebag, or other metaphor**. I reserve the right to define what those mean.

2. I accept requests! **I do not accept demands or threats**, regardless of how right you think you are. I reserve the right to go public with any such messages.

3. I'm happy to accommodate the neurodivergent; I'm one myself! If you're unsure how well your message will be received, or are writing under duress, **let me know**. I may waive some of these rules, and will respect you for it.

4. If you **can't stand by your words publicly, don't send them.** I removed the offending person's name from my post yesterday. I want to be crystal clear: I will not extend this courtesy again.

5. I will **block you** for breaching any of these rules, either via email or social media. If we've had past communication, I will request you not ping me again. You will not seek contact again, including but not limited to hunting me or my partner down on other platforms. If your behaviour looks like stalking, it's because it is.

6. **Do not contact me** if any rule here is ambiguous to you. We'll both be better off.

I'd rather be hanging out with my regulars around the world at a fun coffee shop, pub, or bakery than wasting my time dealing with bad people. Don't be one, and you'll be fine.


### Addendum: some examples

Here's a **bad** message:

> Your quote from my Mastodon profile STOLE FROM ME. Remove my FUCKING words or or I will go public with your name! Wait no, don't do that to ME!

And another **bad** one:

> I read you on Hacker News. Are you an idiot? That feature doesn't work like that. Nobody uses FreeBSD. You're pathetic.

Here's a **good** message:

> Hi Ruben. I noticed you referenced my blog, or included a link to my work. Can you please remove this.

And another **good** one:

> Hi Ruben, you said that Linux doesn't support this BSD feature. It does, you can use `$TOOL` to implement it.

A test I use is if a jury would look favourably to your post being read in court. If not, best to not send it.
