---
title: "Unobtainium PC building in early 2025"
date: "2025-02-19T15:59:30+11:00"
abstract: "The hobby of PC building is being pushed out of the reach of even more people."
year: "2025"
category: Hardware
tag:
- building-pcs
- graphics-card
location: Sydney
---
This is a bit of a *thinking out loud* post that normally I'd push to drafts, but Wouter has convinced me that it should go out into the world anyway :).

I don't know how else to say it, but it's a weird time to be building PCs. After a brief reprieve when blockchain guff fizzled out, graphics cards have become unobtainium again in either availability or price. We all hold out hope for AMD and/or Intel to deliver the goods, and at a price that will make them reasonable against the entrenched market leader, but we also know how this has worked out the last couple of times.

I'm sure I'm not the first to observe this, but the price of graphics cards seem *way* out of balance again. Increasingly it feels like components like motherboards, CPUs, flux capacitors, memory, and cooling systems are merely there as vessels to deliver a graphics card to a game or workload. We're even seeing this in the server space, thanks to the proliferation of slop machines. Though that's not to say those other components are inexpensive either; far from it!

My philosophy in life was always to buy a generation older than what's on offer, partly because it's been tested, but also because you could get them more easily and at a much lower price. It sounds rational, but even availability of older components isn't guaranteed anymore. Our local Australian electronics distributors are routinely out of stock of past-generation graphics cards, and when they *are* in stock, they're barely more affordable than current cards. And thanks to the sorts of 24/7 workloads these cards may otherwise have been subjected to, I'd question buying a second-hand one from anywhere unless you knew the seller.

There was a time where you could easily recommend a gaming PC over a console, because it offered more flexibility at a lower price. But I don't fault people now seeing *entry* level cards scraping that $1000 price range, and thinking they'll spent their money and leisure time elsewhere. I'm lucky in that the games Clara and I like to play together are relatively pedestrian. For a AAA title in 2025? *Forget about it,* as my beloved New York friends would say in an accent I can't muster, but it doesn't stop me trying.

Anyway! It's a bit sad. My knowledge of computers as a kid came down *entirely* to my having built, tinkered with, upgraded, and programmed them after school, and on weekends. Gaming is now about the only reason home users would build a machine now, and there are people being put off the whole thing thanks to the prices and availability issues. The cost of living crunch in much of the world also isn't helping matters.

I see the argument that phones and tablets are turning young would-be engineers into mindless drones who can barely operate a keyboard or file system. This might be a controversial take, but I think pushing PC building into the unreasonably priced territory we're seeing now is just as responsible, if such a trend exists. How are you supposed to grok hardware, when you've never had the ability to play with it?
