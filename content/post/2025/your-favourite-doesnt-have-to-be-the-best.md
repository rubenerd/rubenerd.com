---
title: "Your favourite doesn’t have to be the best"
date: "2025-01-31T17:00:22+11:00"
abstract: ""
year: "2025"
category: Thoughts
tag:
-
location: Sydney
---
I was mulling some feedback I received about my [OM System OM-1 Mark II](https://rubenerd.com/the-om-system-om-1-mark-ii/) post, in which the mental brain genius claimed I was "retarded" for wanting something when a "full frame camera could be bought" for the "same cost". They then gave me a link to an American retailer selling a full-frame Lumix camera body that was more expensive than the OM-1 Mark II, and not available in my locale. Whoops.

Embarrasing lapses in mental judgement aside, this gentleman's post did give me something more profoud to think about. Your *favourite* thing need not be the *best thing*. The *best thing* is also far more nebulous, subjective, and difficult to analyse than I think people realise.

I've known this about media for years. My *favourite* movies are *Mr Vampire* and *The Blues Brothers*. Are they the best examples of Cantonese and American cinema, respectively? I mean, it's more likely that than the other way round. But I'd say not. These movies are charming, fun, and make me smile, but the acting in *The Blues Brothers* is objectively terrible, and *Mr Vampire* doesn't have Andy Lau.

It's the same with music. My favourite songs are&mdash;for the most part&mdash;by Michael Franks. I personally think he's the most wonderful singer/songwriter of all time, but is he the *best?*

Ah, I hear some of you cry, you can't make a judgement on what art is best, because it's subjective. Taste, as a translated character from *Mr Vampire* might have said, differs from person to person. Get it... taste? Because they... shut up. Technical devices are a different matter, because they have specifications. Okay then, let's interrogate that.

<figure><p><img src="https://rubenerd.com/files/2025/om-1-mark-ii@1x.jpg" alt="Press photo of the OM-1 Mark II" srcset="https://rubenerd.com/files/2025/om-1-mark-ii@2x.jpg 2x" style="width:500px; height:358px;" /></p></figure>


### Cameras and other tech

In my post about the OM System OM-1 Mark II (try saying that fast), I mentioned that the Micro Four Thirds sensor size was smaller than full frame, or even APS-C found in other mirrorless and SLR cameras. This might lead those for whom comprehension is a challenge to conclude that I'm silly for wanting one, let alone buying one. There's something *better*, you see.

This is where circumstances, preferences, and constraints come into conflict with what a technical person might traditionally think of as the *best* thing. The *best* camera would surely be the one with the most megapixels and biggest sensor right? Everything else is ancillary, given the primary purpose of a camera is to take photos.

Well, not really. As I mentioned in the post, I enjoyed the ergonomics of OM System (ne. Olympus) cameras, which meant they got more use than technically better cameras I own. The Micro Four Thirds format is also great for travel thanks to its lighter and smaller glass for equivalent performance at that crop factor. The best, for my circumstances and I, happens to be this camera, irrespective of whether you can buy a smaller one with a larger sensor for the same price.

I see well-intentioned people make this mistake in online reviews constantly. Everyone likes a star rating, or a `$FOO / 10`, because it's neat and easy to compare. But devices come with their own baggage, target different people, and have their own quirks and limitations.

For example, a noise-normalised test for CPU coolers divided by price might sound like an objective test of value, but have they considered which cases will fit them? What their maximum cooling potential is, if or when that's needed? Do they match what other components are in someone's build? Are they easy to install, clean, repair, or replace components? How easy are they to configure? What warranties do they come with? Can then operate at zero speed?

You have a set of criteria against which you measure what you're deciding to buy. They include the tangible and testable, and the touchy feely stuff. It therefore follows that what might end up being your favourite thing isn't the *best* thing by some ordained metric everyone thinks is the most important. That's fine!
