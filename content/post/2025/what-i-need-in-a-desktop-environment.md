---
title: "What I need in a desktop environment"
date: "2025-01-04T09:24:47+11:00"
abstract: "A history of my adventures building my own nix desktops, and a discussion of my requirements now."
year: "2025"
category: Software
tag:
- bsd
- desktop-environments
- freebsd
- linux
- netbsd
location: Sydney
---
When this blog started [twenty years](https://rubenerd.com/this-site-turns-20-today/ "This site turns twenty today") ago&mdash;sorry, I'm still processing that number&mdash;many of my early posts concerned setting up my ideal FreeBSD, NetBSD, and/or Linux desktop environment. I cobbled together what I needed and settled mostly on ROXFiler, urxvt, and Fluxbox, with a few other experiments. It was fun, and I learned more from tweaking my environments and building my own software than four years of undergraduate studies. 

But over the years, is a phrase with four words. I ended up going back to Xfce and KDE, just because they include everything I needed without messing with config files and scripts. I suppose its inevitable when you transition from being a student to working full time, and you need a functional system to do work.

The first major departure I made from this came in 2021, when I started using my on-call Japanese Panasonic laptop [without a desktop environment](https://rubenerd.com/my-freebsd-laptop-with-just-tmux/ "My FreeBSD laptop... without a GUI!?") at all, and only using tmux. I realised that I was living most of my life in the terminal, so why not go the whole way? A year later and I was [still using it that way](https://rubenerd.com/a-year-of-using-a-freebsd-laptop-without-a-gui/ "A year of using a FreeBSD laptop without a GUI"). I've since had to stop using this machine for unrelated reasons, but I did enjoy my time using it like that. When push comes to shove and you just need to *fix* something with an SSH session, or you want to do some light writing, it's great not having other distractions.

On my desktop though, that's another story. Though I realised recently I only have a few core requirements:

* **A robust file manager** with thumbnail support. I no longer use a dedicated photo organising tool, instead preferring to use a file manager to traverse folders instead. Why add another abstraction?

* **A quick launcher** that can be invoked from the keyboard. I built the muscle memory for this with Alfred and Spotlight on Mac OS X.

* **A polished interface**, or at least one that isn't ugly or hacked together. I look at these machines every day of my life; I don't think it's unreasonable to want a pleasant experience.

* **A panel** that can be easily configured. My benchmark for this is how easy it is to set 24 hour time. Scripting is fine.

* **Retina/HiDPI support**. I either run at 1× and 2× because fractional scaling sucks. If the UI scales cleanly, and I don't see blown out raster images everywhere, that's fine.

And these are my nice to haves, but not essential:

* **Included desktop essentials** for things like calendars, taking screenshots, and reading PDFs. At the risk of invoking a tautology, you don't realise you need these things until you need them.

* **Theming out of the box for GTK and Qt**. Related to the polished UI point above, it sucks when you launch a program in a competing GUI toolkit it looks like crap. I'd prefer to not deal with dotfiles and config for this sort of thing.

Based on these requirements, KDE and Xfce still fit the bill for me depending on the machine in question. But I still think it was useful to actually put down what my requirements really are.
