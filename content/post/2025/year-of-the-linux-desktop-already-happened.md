---
title: "Year of the Linux desktop"
date: "2025-01-31T06:36:06+11:00"
abstract: "My mum wasn’t a computer person, and it worked great for her!"
year: "2025"
category: Hardware
tag:
- linux
- xfce
location: Sydney
---
Last year I was the Main Character&trade; on Linux Mastodon for suggesting that [some people need to run Windows](https://rubenerd.com/people-who-need-to-run-windows/), irrespective of whether they *want* to. Therefore, a blanket retort such as "just run Linux" isn't just a waste of time, it misreads the room. That's the lack of self-awareness people point to when they ironically use the phrase "year of the Linux desktop".

But it *is* true for certain people, and technical proficiency isn't among the primary reasons why.

My mum Debra wasn't a computer person. Her first jobs involved processing film and selling cameras, so she had technical and troubleshooting experience with complicated kit. But while she encouraged me to pursue computers when I showed an aptitude and interest, she was content watching from the sidelines. As a calligrapher later in life, I think she also saw printed text from computers as impersonal.

This changed in the early 2000s, when a friend of hers in Singapore told her she could read newspapers from Australia online. I set her up with my old iMac DV at her desk, but as she became more frail she wanted to read them in bed. This was before devices like the iPad, so I went out and bought her a basic Acer laptop, and replaced whatever slow, garbage version of Windows was on it with Xubuntu.

To say she took to the machine like a duck to water would be an understatement. Newspapers were the gateway, but soon she was reading from all over the place. Within a year, she'd even started a small eBay business, and had a del.icio.us bookmark account with thousands of entries. It made me so happy walking into the bedroom and seeing her not lying there with a finished book looking distant and sad, but sitting up with her reading glasses furiously typing something. And this was all thanks to a Linux desktop, something so many are willing to dismiss as too complicated for *average* people to use.

Debra was a unique case in many ways:

* She had no prior Windows experience, and with that, no preconceived notions of how a computer was "supposed" to work. To her, the Xfce desktop on Ubuntu was normal.

* She didn't have to run any specialised software. She lived her life through Firefox, OpenOffice, XMPP, and PySolFC. Again, that's all she knew, and it worked *extremely* well for her.

* It printed to our EPSON kit just fine, and synced flawlessly with her Palm organisers, the only other technical devices she loved using. Anything more exotic might have been difficult.

* She had someone living with her who could install and troubleshoot Linux on the desktop.

But here's the thing. I spent more time troubleshooting and fixing my dad's and sister's Windows machines than I ever did fixing Debra's laptop. It handled updates just fine. It was stable, reliable, and fast even on that low-end hardware. I think anyone who says they'd set up a family member with Windows instead of Linux to reduce support overhead may be kidding themselves.

I'll grant detractors that it's a niche, though I suspect not as narrow a one as people think. Thesedays, I'd grab a second-hand ThinkPad for a hundred bucks, and throw Linux onto it for someone without a second thought.
