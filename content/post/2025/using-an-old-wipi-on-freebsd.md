---
title: "Using an element14 Wi-Pi [sic] on FreeBSD"
date: "2025-02-09T10:28:27+11:00"
thumb: "https://rubenerd.com/files/2018/gpd-libretto@1x.jpg"
year: "2025"
category: Hardware
tag:
- bsd
- freebsd
- guides
- networking
location: Sydney
---
With all my discussion last year about an [ultraportable computer](https://rubenerd.com/an-oqo-or-hand-386-successor-for-commuting/) I can use on the train, I remembered that I [bought a GPD Pocket](https://rubenerd.com/the-gpd-pocket/) in Akihabara back in 2018 for testing BSD hypervisors at conventions. It's hinged, which isn't ideal for commuting when standing, but I thought the form factor could be a good test case.

<figure><p><img src="https://rubenerd.com/files/2018/gpd-libretto@1x.jpg" srcset="https://rubenerd.com/files/2018/gpd-libretto@1x.jpg 1x, https://rubenerd.com/files/2018/gpd-libretto@2x.jpg 2x" alt="My Libretto 70CT and GPD Pocket" style="width:500px; height:375px;" /></p></figure>

Alas, the machine has 802.11ac, meaning a no-go on FreeBSD currently (though that may change soon). I rummaged through my drawers of random adaptors, and found an old element14 "Wi-Pi" dongle, ostensibly designed for a Raspberry Pi. According to [this teardown](https://goughlui.com/2015/07/19/teardown-element14-wi-pi-usb-wireless-802-11n-150mbits-2-4ghz-dongle/ "Teardown: element14 Wi-Pi USB Wireless 802.11n 150Mbit/s 2.4Ghz Dongle") by Gough Lui, the Wi-Pi has a Ralink RT5370N IC.

A quick search, and I found it's supported by `run(4)`, which is awesome. According to the <a href="https://man.freebsd.org/cgi/man.cgi?query=run">manpage</a>, you can add the following to `loader.conf`:

    if_run_load="YES"
	runfw_load="YES"

Then add to `rc.conf`:

    # sysrc wlans_run0="wlan0"
    # sysrc ifconfig_wlan0="WPA DHCP"

A reboot later, and it works!

Now you can configure your wireless networks with `wpa_supplicant.conf`. The [FreeBSD Handbook](https://docs.freebsd.org/en/books/handbook/network/#network-wireless) has more details.

I'll still probably get a less obtrusive dongle for commuting, but this worked in a pinch. Not bad for a device that's sat in a drawer for close to a decade!
