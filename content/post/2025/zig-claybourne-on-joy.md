---
title: "Zig Claybourne on joy"
date: "2025-02-24T08:07:46+11:00"
abstract: "“It's good to still have joy in your life.”"
year: "2025"
category: Thoughts
tag:
- health
location: Sydney
---
This was the [most important thing I've read](https://wandering.shop/@zzclaybourne/114027625722914777) in a long time:

> It's good to still have joy in your life. 
> 
> It's crucial. 
> 
> It's cool. 
> 
> Swing that sword while you bake a pie.
