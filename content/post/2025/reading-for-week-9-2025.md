---
title: "Reading for week 9 2025"
date: "2025-02-27T09:46:30+11:00"
abstract: "Kickstarters, work culture, the Xeon 6300, Fry’s Electronics, and hot sauce!"
year: "2025"
category: Thoughts
tag:
- cpus
- food
- work
location: Sydney
---
* Nerdy Pleasures has a post where he explores the legal ramifications of [failed Kickstarters](https://nerdlypleasures.blogspot.com/2025/02/the-failed-kickstarter-legal.html). It's specific to the US legal system, but still interesting. I didn't know 9-10% of Kickstarter campaigns fail, and only 13% of these backers receive refunds.

* [Futurity discusses a study](https://www.futurity.org/money-stress-burnout-job-satisfaction-3270552/) in the Journal of Workplace Behavioral Health that money stress may lead to reduced job satisfaction. It sounds like one of those "they needed a study to tell us water is wet?", but it has some interesting insights.

* Speaking of work, [Wouter at Brian Baking debunks](https://brainbaking.com/post/2025/02/no-rules-are-implicit-rules/) the nonsense contained in *No Rules Rules: Netflix and the Culture of Reinvention.* As he summarises so eloquently, "no rules are implicit rules". I forgot how much we're shielded from this sort of toxic work culture in Australia and the EU.

* Phoronix reviews the [new budget Xeon 6300 series CPUs](https://www.phoronix.com/news/Intel-Xeon-6300-Series). As Michael Larabel writes, they're not so much new CPUs as rebrands to match others in the Xeon 6 series. I run AMD (and Apple Silicon) for desktops and laptops, but I still use an E3-1240 v6 for our FreeBSD NAS/bhyve box. Still on the fence what our upgrade path will be here.

* Dave Farquhar writes about the [downfall of Fry's Electronics](https://dfarq.homeip.net/what-happened-to-frys-electronics/) in the US. I was lucky to visit their San Jose store when I worked in San Francisco briefly in the late 2010s. Even for someone who grew up with Sim Lim Square and Funan Centre in Singapore, Fry's was *massive*. I wasn't aware of its issues, or even that they'd folded.

* Over on MetaFilter, Lemkin shared a post at Notes From the Road about [global hot sauces](https://www.notesfromtheroad.com/desertmexico/hot-sauces-of-the-world.html). They had my two favourites: the legendary *Tabasco*, and even *Indofood Sambal Pedas Hot Sauce*. I haven't gone as far as to carry small bottles of them around with me as some of my American friends do, but I've been tempted.
