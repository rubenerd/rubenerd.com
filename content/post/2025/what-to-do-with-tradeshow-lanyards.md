---
title: "What to do with trade show lanyards?"
date: "2025-01-08T07:45:44+11:00"
abstract: "The rational thing would be to dispose of them, along with memories of lacklustre coffee. Maybe?"
year: "2025"
category: Thoughts
tag:
- coffee
- events
- hololive
- josuiji-shinri
- ouro-kronii
- work
location: Sydney
---
Every time I go to an industry gathering, user group, trade show, conference, weeb event, or other such gathering, I'm issued one of these lanyard things with either a name tag or card attached. Sometimes it's to identify me among the crowd, other times it's to verify that I&mdash;or the company!&mdash;paid for me to attend their free lunch with an event attached (cough).

This was one for a data centre conference I went to last year. Feel free to scan the expired QR code if that's your thing.

<figure><p><img src="https://rubenerd.com/files/2025/wmedia-kronii@1x.jpg" alt="" srcset="https://rubenerd.com/files/2025/wmedia-kronii@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

To clarify, Hololive's [Kronii](https://www.youtube.com/channel/UCmbs8T6MWqUHP1tIQvSgKrg) didn't come with to that event. Nor did that excellent cup of AeroPress coffee I just made in a [Shinri](https://www.youtube.com/@josuijishinri) mug, my oshi from Holostars. It's a tough life being me, not least because I have to attend events without the aforementioned people and beverages. Both Shinri and Kronii would have been infinitely more entertaining than the MC at many of these events, and I'm sure the coffee is better too. MC? More like *medicore coffee*, amirite!?

Some of these lanyards are for forgettable events that I've since forgotten, as I implied with the phrase *forgettable events*. Others, like AsiaBSDCon, are prized treasures that I want to frame because they quite literally changed the direction of my life, and they're where I met so many people I respect and appreciate from projects like NetBSD, FreeBSD, and even illumos.

Because I'm a *massive* nerd, I keep *all* of them. But aside from framing those ones I treasure... what do I do with the rest? Currently I have a hook in our wardrobe where I hang them, but the weight of years of these things is reaching criticality. Do I start a new hook? Do I put them somewhere else? Or do I do the rational thing and... dispose of them?

I imagine someone more creative could think of something more creative to do with them. Aka, someone with a dang *thesaurus*.
