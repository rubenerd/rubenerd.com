---
title: "My Wedgewood Egypt mug"
date: "2025-01-13T10:00:04+11:00"
abstract: "The latest in my mug post series."
year: "2025"
category: Hardware
tag:
- coffee
- mugs
location: Sydney
---
We haven't done a [mug post](https://rubenerd.com/tag/mugs/) in a while. This is my Wedgewood *Stamps from Afar: Egypt* mug, made in England no less:

<figure><p><img src="https://rubenerd.com/files/2025/wedgewood-egypt@1x.jpg" alt="My Wedgewood Egypt mug, showing a woodcut-style print on a postage stamp." srcset="https://rubenerd.com/files/2025/wedgewood-egypt@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

In the [words of 10cc](https://www.youtube.com/watch?v=fUNTk5xsxk4 "Dreadlock Holiday"), *is was a present from my mother*. She and I had always intended to do all the tourist stuff around Egypt, alongside going to the Metropolitan Museum of Art in New York. We shared in so much of this stuff when I was a kid, and they're among my most treasured memories.

As far as the mug goes, it's easily the most expensive piece of crockery I own. That, coupled with its sentimental value, limits the amount I use it. But it's lovely when I do. It's very fine, in the sense that the walls and handle are quite thin. But it's exceptionally well made.

I find these wider mugs are great for larger pourovers like the Clever Coffee Dripper, because it lets it cool down into the tasty range a bit faster.
