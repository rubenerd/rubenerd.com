---
title: "I wish partner graphics cards got the memo"
date: "2025-01-25T08:52:07+11:00"
abstract: "Who would have thought most graphics cards look terrible?!"
year: "2025"
category: Hardware
tag:
- design
- graphics-cards
location: Sydney
---
A well-known graphics card company announced their new kit at CES this year. I'm not the target market for this silicon; I plan to buy AMD next round for their open-source driver support, and don't have a burning need to draw 575 watts from a PSU. But I want to call your attention to one aspect:

<figure><p><img src="https://rubenerd.com/files/2025/rtx5090@1x.jpg" alt="Press photo of a 5090, showing its minimal aesthetic." srcset="https://rubenerd.com/files/2025/rtx5090@2x.jpg 2x" style="width:500px;" /></p></figure>

Okay, this isn't as good a photo of it as I thought it'd be. It's taken from their website. But note the design. It's not great, but it's fine. At least some care and attention was spent on it.

Meanwhile, this is what every partner graphics cards look like this generation, because of course they do:

<figure><p><img src="https://rubenerd.com/files/2025/rtx5090-tuf@1x.jpg" alt="A something something 4090, I think it’s even called a TUF. No, no Tough, a TUF. It's hideous." srcset="https://rubenerd.com/files/2025/rtx5090-tuf@2x.jpg 2x" style="width:500px;" /></p></figure>

I would have thought the existence of "reference" cards would have encouraged these third parties to get a clue in the design department, but it doesn't look like it.

Design is a decision. You can release something that looks like crap, or something that doesn't. In the case of partner boards, that's a lot of ugly, redundant styling. PC cases continue to become more refined, but graphics cards are stuck in 2004 Lanboy territory. I guess when you're paring it to a motherboard called *GAPING WOUND* or *COLLATERAL DAMAGE* with *PTSD... DR MEMORY* you'd expect styling cues from ammo crates and boy racer cars.

I haven't checked any of the AMD offerings yet, but fingers crossed there's one that doesn't feel embarrassing to buy. Not that I have the money to anyway, so *touché*.
