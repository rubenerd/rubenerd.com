---
title: "I accidently made abstract art"
date: "2025-01-28T18:02:43+11:00"
abstract: "A borked upload resulted in a broken image I think looks quite fabulous."
thumb: "https://rubenerd.com/files/2025/image-glitch@2x.jpg"
year: "2025"
category: Media
tag:
- art
- colour
- errors
location: Sydney
---
I wrote a post at the start of the year about [tradeshow lanyards](https://rubenerd.com/what-to-do-with-tradeshow-lanyards/), and how I had no idea what I should do with them. I'd been hanging them in my cupboard the whole time, and I was running out of space.

This was the image that accompanied that post, featuring the lanyard, a fresh cup of coffee in a Shinri mug, and Kronii who was Clara's and my latest acrylic stand.

<figure><p><img src="https://rubenerd.com/files/2025/wmedia-kronii@1x.jpg" alt="Photo of the aforementioned characters and lanyard" srcset="https://rubenerd.com/files/2025/wmedia-kronii@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

There was just one problem: the HiDPI version of the image on the server was corrupt. Some browsers rendered it fine, but a few of you emailed me the following instead:

<figure><p><img src="https://rubenerd.com/files/2025/image-glitch@1x.jpg" alt="A glitched version of the image." srcset="https://rubenerd.com/files/2025/image-glitch@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

Firstly, can we appreciate the fact that the three primary subjects in the image were so *artfully* presented in this? And the warm colour in the lower two-thirds adds a *whole* other dynamic. I'm shocked at how oddly amazing this looks.

I ran the source through ImageMagick, and sure enough:

    magick: Premature end of JPEG file `wmedia-kronii@2x.jpg'   
        @ warning/jpeg.c/JPEGWarningHandler/411.
       
    magick: Corrupt JPEG data: premature end of data segment   
        `wmedia-kronii@2x.jpg' @ warning/jpeg.c/JPEGWarningHandler/411.

I suspect the image didn't finish uploading, or I closed the laptop lid in a hurry before going somewhere else. Fun either way!

I've uploaded a corrected version, and used ImageMagick to export a "fixed" version of the corrupted image, which I've preserved here for posterity. As opposed to posterior. Prognostication.
