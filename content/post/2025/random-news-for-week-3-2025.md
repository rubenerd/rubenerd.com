---
title: "Random news for week 3, 2025"
date: "2025-01-16T09:18:37+11:00"
abstract: "Casinos losing money, clothing choices, billions cant buy you a spine, and LXQt lands Wayland support."
year: "2025"
category: Thoughts
tag:
- finance
- lxqt
- news
- wayland
- women
location: Sydney
---
This is a collection of random, unrelated things I read this week.

I fell into the trap last year of wanting to comment on individual stories, but they never left my drafts because I already exceeded my self-imposed daily post limit. This format will let me post stuff that interests me, and avoids the drafts folder trap where they lose timeliness.


### A casino losing money

From [ABC News Australia](https://www.abc.net.au/news/2025-01-14/star-casino-collapse-explainer-debt-premiers/104810582)\:

> Now one of Australia's biggest casino operators is facing the prospect of losing it all, with a top analyst predicting Star Entertainment Group has a 50 per cent chance of going into administration.

It's impressive to see a *casino* of all businesses face the prospect of losing money. They have the ultimate business model: hoover money out of the vulnerable, and give them nothing in return. How do you `fsck(8)` this up?

> Star is now battling legal action, with the corporate watchdog ASIC alleging Star's board and directors had "failed to give sufficient focus to the risk of money laundering and criminal associations".

Quelle surprise!


### "It's womens’ fault, damn it!"

I had to *wade*&mdash;**AAAAAAH**&mdash;into this story about a [Sydney council policing dress](https://www.theguardian.com/australia-news/2025/jan/15/greater-sydney-council-bans-revealing-swimwear-sparking-debate-about-double-standards) because this section from Lauren Rosewarne, an associate professor from the University of Melbourne, was absolute fire:

> “The undercurrent of these stories is that somehow women are doing something with their bodies to distract men in ways that make men feel as though they’re being tempted, and it’s up to women to sort themselves out … Somehow, the responsibility is on women not to stir desires in men, because then men might act badly and be punished, so we have to put the responsibility of morality on to women’s shoulders.”

It's the same nonsense I hear female friends being subjected to when they cosplay at events. I know right, the worst! Expecting women to wear/not wear specific things because it'll "trigger" men infantilises everyone. It denies women freedom and basic respect, and reduces men to witless clods without agency or self control.

> “So long as [practicality] and safety are considered it shouldn’t be any one else’s business what I’m comfortable swimming in,” one person commented.

This. Also, **wade**. I was proud of that. I shouldn't be.


### Sneeze

Gesundheit.


### Billions can't buy you a spine

[Reuters](https://www.reuters.com/world/us/apple-google-ceos-join-tech-leaders-trump-inauguration-media-reports-say-2025-01-15/)\:

> Apple's Tim Cook and Google CEO Sundar Pichai are among the Big Tech leaders planning to attend [U.S. President-elect's] inauguration on Monday, according to media reports on Wednesday.

This has done wonders for my self esteem and mental health, and it should for you too! We stand taller than the richest people on the planet.

*I don't care too much for money. Money can't buy me spine. Can't buy me spiiiiine! Spiiiiine! Can't buy me spiiiiine! ♫* Hey, I worked a Beatles reference into a post. Maybe I need to do Wings next. *You'd think that people would have had enough of silly... posts? ♫*.


### LXQt has Wayland support

This was [announced last November](https://lxqt-project.org/release/2024/11/05/release-lxqt-2-1-0/), but I'm just getting aroud to reading the release notes after it made it to [openSUSE news](https://news.opensuse.org/2025/01/13/LXQt-Wayland-support-is-now-here/).

> Through its new component `lxqt-wayland-session`, LXQt 2.1.0 supports 7 Wayland sessions (with Labwc, KWin, Wayfire, Hyprland, Sway, River and Niri), has two Wayland back-ends in `lxqt-panel` (one for `kwin_wayland` and the other general), and will add more later. All LXQt components that are not limited to X11 — i.e., most components — work fine on Wayland. The sessions are available in the new section `Wayland Settings` inside LXQt Session Settings. At least one supported Wayland compositor should be installed in addition to `lxqt-wayland-session` for it to be used.

It wouldn't surprise you to know I have... mixed feelings about Wayland. But it's where the industry is moving, so news like this is encouraging.

I haven't tried LXQt in a long time. I know Allan Jude of FreeBSD and OpenZFS fame runs it, and it could be a nice Qt-flavoured stepping stone from KDE. I'll add it to my neverending list of things to try.

