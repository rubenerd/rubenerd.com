---
title: "When people judge you for wearing a mask"
date: "2025-01-26T07:11:45+1000"
abstract: "Cough really loudly! That makes them leave you the fuck alone, maybe."
year: "2025"
category: Thoughts
tag:
- covid
- health
location: Sydney
---
I'd hoped that COVID-19 would have normalised people in the West wearing masks when they're sick but can't isolate. We'd been doing it in Asia for years, even before the original SARS virus.

> If you get SARS, you go to Tan Tock Seng, because they know SARS like I know Ah Beng. ♫

That public health commercial sung by none other than Mr Phua Chu Kang Private Limited in Singapore still rattles around in my head to this day. Wow I miss the little red dot sometimes. *Wah lau eh.*

**Anyway.** Most people ignore me when I wear a mask, because they're reasonable or don't give a schmidt. This is a good thing. But occasionally you'll get cooked individual who sees it as their mission to call you out. This is my primary strategy to deal with them:

* Nod and give them a thumbs up!

I may have done these on occasion as well, though I wouldn't recommend any of them under any circumstances:

* Stare at them blankly without blinking. This really creeps people out.

* Do what Clara calls my "manic evil laugh". This scares the shit out of them.

* Cough *really* loudly, and watch them suddenly back off.

* Say "I'm foiling facial recognition and the deep state" or "I don't do what the government tells me".


