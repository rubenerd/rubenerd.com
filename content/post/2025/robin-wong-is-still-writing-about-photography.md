---
title: "Robin Wong is still writing about photography"
date: "2025-01-27T09:31:16+11:00"
abstract: "One of my favourite street photographers is still at it. I’m unreasonably happy."
year: "2025"
category: Thoughts
tag:
- blogging
- malaysia
- photography
location: Sydney
---
The vast majority of bloggers who started around the time I did are no longer doing it. Blogging and owning your own space was massive in 2004, but the so-called *blogosphere* is but a shell of its former self now. The IndieWeb movement has revitalised this a bit which is encouraging, but you're still unlikely to bump into people in the street who have a blog.

I belabour all this, because it's always incredible finding someone I used to read is not only still doing it, but has made a name for himself.

[Robin Wong](https://robinwong.blogspot.com) was one of *the* preeminent digital photography experts when my family and I lived briefly in Malaysia, and he's still writing on his blog today. I just finished reading his review of the tiny [Panasonic GM1 with a 14mm f/2.5](https://robinwong.blogspot.com/2025/01/smallest-micro-four-thirds-28mm-setup.html). My heart ached with nostalgia and joy seeing a glimpse into my adopted home again, right down to the hanging bags of teh tarik. Robin has an incredible eye for detail, especially for those whimsical details we may otherwise walk past every day.

<figure><p><img src="https://rubenerd.com/files/2025/robinwong@1x.jpg" alt="Photo of Robin with his trademark smile from his About page" srcset="https://rubenerd.com/files/2025/robinwong@2x.jpg 2x" style="width:500px;" /></p></figure>

[Check him out](https://robinwong.blogspot.com) if you're interetsed in photography, especially street photography in Malaysia. He also has an [RSS feed](https://robinwong.blogspot.com/feeds/posts/default?alt=rss).
