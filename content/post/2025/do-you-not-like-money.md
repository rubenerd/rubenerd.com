---
title: "“Do you not like money?”"
date: "2025-02-26T13:12:39+11:00"
abstract: "No, sorry Mr Financial Company, I do not!"
year: "2025"
category: Thoughts
tag:
- finances
location: Sydney
---
I received an email from an Australian financial company with the blog title as the subject, and it's been rattling around in my head for days.

> Do you not like money?
>
> Ruben... are you there? Look at me, Ruben. Do you... do you not like money?

I know both the answers I'm *supposed* to give, and the answer they're *expecting* me to provide. They want me to say I like money, so they can launch into their pitch about how their low fees and whatnot will save me from parting with it. Specifically, parting with something I like. Which is money.

The implication is clear. Only someone strange wouldn't like money. Do you not like it? What's wrong with you? Who wouldn't like money? Do you... do you not like money?

But here's the thing! It's not a novel concept, and this financial company may be frustrated by what they see as an obtuse response. But no, thanks, I don't "like" money, just as I don't "love" money. In fact, I tend to be wary or suspicious of those who claim to.

I tolerate money. Money is&mdash;in our current system&mdash;a necessary evil required for my survival. It's odd, and mildly dystopian, that I may live or die by the size of a number hosted in a computer database somewhere. I budget, I reconcile, and I work hard to ensure this number is always going up, so I can survive when I'm no longer able to add to it. I donate too, but only insofar as it doesn't impact my ability to save. Money is selfish. It's unfeeling. It drives bad people to do worse things. And so on.

I'll admit I do enjoy *accounting*, but that's more to do with deriving satisfaction from numbers balancing and being correct. The money itself? Meh.

Sure, more money will make my life easier. But that's not me liking money, it's liking having food, and shelter, and being able to delegate tasks I may otherwise have to do myself. It's the hobbies, and travel, and coffee. It's being able to have a nice lunch with Clara, or my sister, or a friend I haven't seen in years. It's seeing the person struggling and being able to offer help. *Those* are what I like.

Is that a pedantic difference? Maybe, I don't know. But I still find the idea of "liking" money to be... unsettling.
