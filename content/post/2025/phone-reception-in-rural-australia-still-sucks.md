---
title: "Fixing phone reception in rural Australia"
date: "2025-03-05T08:15:57+11:00"
abstract: "Phone reception in rural Australia sucks, but at least some people are thinking about solutions."
year: "2025"
category: Internet
tag:
- australia
- broadband
- mobile-phones
- safety
location: Sydney
---
*An earlier version of this post was titled "Phone reception in rural Australia still sucks", hence the permalink. Maybe I should have kept it!*

There's a trend among members of my family where they move up the Australian coast when they retire. My grandad bought land near Forster so he could fulfill his dream of building a telescope and watching the night sky. My dad has a place an hour away from Newcastle and paid less than half than Clara and I did for our modest Sydney apartment. It's right on the beach, and perhaps fits the stereotype of how a lot of Brits imagine people living in Australia. I could go on.

They all have something else in common though, and it's something they share with everyone from remote Aboriginal communities to farmers. Phone reception has traditionally *sucked*. It sucked when 3G was rolling out, it sucked when 3G was being taken down, and it sucked at every other time. You'd drive down the highway and see the bars for the three major telcos drop from five, to four, to three, to two, to stabilising on one for a bit, and then nothing.

The reasons for this are complicated, but there are a couple of perhaps unsurprising themes.

Australia is a similar size to the contiguous United States, yet we have ten million fewer people than Canada and California. That population is also highly skewed to urban areas, meaning those population density claims of "three people per square kilometre" aren't even that meaningful. Rolling out ubiquitous mobile coverage to this area would always be a challenge; a former colleague who worked in comms said putting towers out there felt like you were on the moon.

The lack of investment in critical comms is another issue. The National Broadband Network was envisioned to partly alleviate this, with profitable urban installations subsidising rural areas. This was then politicised and *Malcolm Turnbull'd* into the mess we're all spending extra tax dollars now fixing. Businesses claimed they were willing and ready to invest in rolling out equivalent infrastructure prior to the NBN, but did they? In the local vernacular, <span class="lang" style="font-style:italic;" lang="en-au">yeah nah</span>. Once again, I have fibre broadband in Sydney, but someone in the sticks? Good luck.

[Mark Gregory wrote a report](https://www.theguardian.com/australia-news/2025/mar/03/we-need-to-improve-mobile-connectivity-in-the-bush-we-can-start-by-collecting-more-data "We need to improve mobile connectivity in the bush. We can start by collecting more data") yesterday about his experiences with rural mobile reception. His tests showed speeds "90% lower than urban areas", and that such results "will not be news to people in the bush: the digital divide in rural areas is now wider than ever before".

He explored ways to improve this:

> Collecting mobile performance data is vital if rural telecommunications coverage and performance are to be improved. Some of this work is being done, with the launch of a three-year national audit of mobile coverage in May 2024, which aimed to better identify mobile coverage black spots across Australia.
>
> Recently, I proposed to government that it fund the ongoing research and development of low-cost, solar-powered Internet of Things (IoT) devices to enable mobile performance data to be collected right across the nation.
>
> There is also an urgent need for the Australian Competition and Consumer Commission and the Telecommunications Industry Ombudsman to be given new powers to set and enforce minimum performance standards. 

Again, speaking to friends who've worked in comms in Australia, this all sounds sensible. We just need the political will to do it.
