---
title: "Zen and the art of PCIe ×1 placement"
date: "2025-01-01T15:36:08+11:00"
abstract: "Most boards place their additional slots below the graphics card, where they impede airflow."
year: "2025"
category: Hardware
tag:
- design
- motherboards
location: Sydney
---
My penultimate post for 2024 considered my future with Mini-ITX, and how I might be looking to [expand to MicroATX again](https://rubenerd.com/bidding-farewell-to-mini-itx-maybe/ "Bidding farewell to Mini-ITX, maybe") for future builds. I've exclusively run small form factor for my personal desktops since the early 2010s, but expansion and cooling limitations have led me to reconsider this time. *Whoo-oo-ah-oo, [this time](https://piped.video/watch?v=mvFes64TCuQ "Wet Wet Wet: This Time") ♫*.

It's been fun researching MicroATX boards again. A lot has changed since I last looked at them, in particular the placement of PCIe slots seems to be entirely arbitrary, and in ways that occasionally even make sense.

This is roughly the layout I remember at the base of MicroATX boards, if you'll forgive the terrible and not-to-scale ASCII:

<pre style="line-height:1em !important">
║ ┌───────────────────────────────┐      ║ 	
║ │ PCIe ×16                      │      ║ 	
║ └───────────────────────────────┘      ║	
║ ┌────────────────┐                     ║ 	
║ │ PCIe ×8        │                     ║ 	
║ └────────────────┘                     ║
║ ┌───────────────────────────────┐      ║ 	
║ │ PCIe ×16                      │      ║ 	
║ └───────────────────────────────┘      ║ 
║ ┌─────────┐                            ║ 	
║ │ PCIe ×1 │                            ║ 	
║ └─────────┘                            ║	
╚════════════════════════════════════════╝ 	
</pre>

This was a classic SLI layout. You have two PCIe ×16 slots, spaced sufficiently apart for two tall cards. If you have an older card that only takes up the physical space of one slot, you have other slots at your disposal for additional expansion cards. Sometimes you'd have a legacy 32-bit PCI slot in there somewhere too (my [Supermicro workstation board](https://www.supermicro.com/en/products/motherboard/X11SAE-M "Supermicro X11SAE-M") uses its lone PCI slot for an old SCSI card for use with Iomega drives, because I'm weird like that).

Today, the majority of consumer desktop cards will have precisely one card attached to the PCIe bus: a chonky graphics card. It's not uncommon therefore to see boards with the following layout at the base instead:

<pre style="line-height:1em !important">
║ ┌───────────────────────────────┐      ║ 	
║ │ PCIe ×16                      │      ║ 	
║ └───────────────────────────────┘      ║	
║                                        ║ 	
║                                        ║ 	
║                                        ║
║                                        ║ 	
║                                        ║ 	
║                                        ║ 
║ ┌─────────┐                            ║ 	
║ │ PCIe ×1 │                            ║ 	
║ └─────────┘                            ║	
╚════════════════════════════════════════╝ 	
</pre>

It's easy to see why. Modern graphics cards are glorified computers unto themselves, with massive radiators and cooling fan assemblies that can take 3, sometimes 4 slots of physical space. This motherboard layout grants sufficient space for the card, and even lets it encroach onto the ×1 slot if it needs the space (or more specifically, its cooling assembly).

This does present an issue if you *do* want to use that ×1 slot, say for a capture card, discrete sound card, or an additional NIC (otherwise, you may as well go with Mini-ITX). Any card placed in that ×1 slot will impede airflow from the bottom of the case to the graphics card. If the graphics card is 3 slots deep, an extra ×1 card may also completely block one of its fans. It's bananas to me how most motherboards are configured in this way.

The good news is, there *are* motherboards that place the ×1 slot in a more practical position *above* the graphics card:

<pre style="line-height:1em !important">
║ ┌─────────┐                            ║ 	
║ │ PCIe ×1 │                            ║ 	
║ └─────────┘                            ║	
║ ┌───────────────────────────────┐      ║ 	
║ │ PCIe ×16                      │      ║
║ └───────────────────────────────┘      ║	
║                                        ║ 	
║                                        ║ 	
║                                        ║
║                                        ║ 	
║                                        ║ 	
║                                        ║ 
╚════════════════════════════════════════╝ 	
</pre>

In this configuration, the ×1 card won't impede the airflow of the graphics card from the bottom. Even cards with "flow through" coolers typically place these at the far end, where most ×1 cards won't reach or cover. The layout means you're limited to 3 slot GPUs (or whatever slack space your case has below the board), but it might be worth it if you actually intend to use that other slot.

So far I've found the [MSI B650M GAMING WIFI](https://www.msi.com/Motherboard/B650M-GAMING-WIFI/Overview) for the Ryzen AM5 platform positions its PCIe slots in this configuration, though I'm sure others have this layout too. It'd be great for my use case.
