---
title: "Getting in 10,000 Steps"
date: "2025-01-25T09:23:02+11:00"
abstract: "Yes it’s arbitrary, but the point was to be more active and make it a habit."
year: "2025"
category: Thoughts
tag:
- exercise
- health
location: Sydney
---
As of next week, Clara and I would have taken 10,000 steps a day, every day, for a year. Yay! Barring extenuating circumstances like severe illness or late night emergency server fixes, we've pulled ourselves off the couch and computer chair, and had brisk walks when the *Walk of Life* didn't afford it to us during the day.

This is the pedometer on my phone as of this morning:

<figure><p><img src="https://rubenerd.com/files/2025/10000-steps.png" alt="Screenshot from iOS showing 11,089 average steps" style="width:500px; height:255px;" /></p></figure>

We started doing this when I realised a big part of why I suspected we loved travelling so much is because we're so *active* when we're in a new place. Our current record is still 32,000+ daily steps in New York, and we regularly exceeded 20,000+ every day we wandered in Japan. It's trivial to get those step counts in places with great public transport and lots of attractions within walking distance.

To be clear, is a phrase with three words. There is no medical or scientific basis for an arbitrary number like 10,000. The key for us was to experiment with moving *more*, and making it a *habit*. To that end, is another phrase with three words. We've also lost weight, though for me that had *way* more to do with diet.

I bring this up because I was talking with some people late last week who said the same thing meat eaters say to vegetarians: "Oh, I could never do that!" as they showed me their iPhones and their 300 steps. Then I saw them get into their cars and drive away.

*(I haven't formed enough thoughts around this for a full post, but I'm starting to think car culture also contributes to poor health beyond breathing in noxious fumes and brake dust as we walk down the street. If you bundle yourself into a car outside your home to drive to work, then sit at a desk, then drive yourself back, that's not much activity).*

Anyway, it's a step&mdash;**AAAAAAAAAAAAAH**&mdash;in the right direction, but we still need to get more exercise in. I really need to hit the pool again sometime soon for upper-body strength, and do some more hiking. We have some incredible nature reserve pathways literally outside our new apartment that I have no excuse not to be exploring.
