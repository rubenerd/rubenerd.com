---
title: "The Ricoh GR III is an incredible camera"
date: "2025-02-22T10:00:31+11:00"
abstract: "Reviewing the technical specs, and why it makes a better camera for Clara."
thumb: "https://rubenerd.com/files/2025/hankyu-train-flying-by@2x.jpg"
year: "2025"
category: Hardware
tag:
- fun
- photography
location: Sydney
---
I've waxed lyrical about my love of [OM System](https://rubenerd.com/tag/om-system/) (ne. Olympus) cameras here of late, saying they were some of the best electronic devices I'd ever owned, how much I loved their mechanical dials, and how wonderful the [Micro Four Thirds](https://www.four-thirds.org/en/) system is for travel. I've even used them in discussions about how [technical specs](https://rubenerd.com/your-favourite-doesnt-have-to-be-the-best/) may not tell the whole story when deciding what to buy and use.

The camera I was thinking about when making these comments, even if I didn't always mention it by name, was my Ricoh GR III. But a recent day trip with Clara has made me realise I've been a bit unfair to it, and I want to set the record straight.

<figure><p><img src="https://rubenerd.com/files/2025/griii@1x.jpg" alt="Image of the GR III." srcset="https://rubenerd.com/files/2025/griii@2x.jpg 2x" style="width:420px;" /></p></figure>

The GR III is incredible, as mentioned in the title. It packs an SLR-sized APS-C sensor, and the sharpest prime lens I've ever used, into a body barely bigger than a pack of playing cards! The best camera is the one you have on you, and this one could easily live in a jacket pocket without feeling bulky. You can't help but wonder *how did they do this!?*

The colours out of the camera look realistic, with a certain warmth and depth to the shots that reminded me of my old Nikon D60. While I expected a bit more from its low-light performance, it still takes great shots without too much noise even handheld in low light.

My favourite feature is its unique snap-to-focus system, which lets you retain a fixed focal point even if the action around the frame is rapidly changing. I had **so much fun** with this in Kansai back in 2023!

<figure><p><a href="https://rubenerd.com/files/2025/hankyu-train-flying-by.jpg"><img src="https://rubenerd.com/files/2025/hankyu-train-flying-by@1x.jpg" alt="A Hankyu train flying by in Japan." srcset="https://rubenerd.com/files/2025/hankyu-train-flying-by@2x.jpg 2x" style="width:500px; height:333px;" /></a></p></figure>

This is more subjective, but I also found the menu system and on-screen status information to be clear and easy to grok, certainly better than Clara's Sony and FujiFilm kit. Adjusting common settings like ISO is easy by clicking the respective part of the rear control wheel, and scrolling to your desired setting.

I don't know how else to describe it, but the GR III is a *friendly* camera. Family and friends who've seen me use it always want to give it a try, and get amazing shots having had no experience with it whatsoever. Clara tried it again recently, and this was literally the first photo she got with it. She said it was seeing detail in the leaf she couldn't see with her naked eye, and immediately lead to her downloading the Ricoh app on her iPhone 15 to stop using that camera instead!

<figure><p><a href="https://rubenerd.com/files/2025/kronii-flower.jpg"><img src="https://rubenerd.com/files/2025/kronii-flower@1x.jpg" alt="Close-up of a little cluster of flowers, surrounded by green leaves" srcset="https://rubenerd.com/files/2025/kronii-flower@2x.jpg 2x" style="width:500px; height:333px;" /></a></p></figure>

The GR III took photos that matched, and often beat, what I could achieve with my Olympus OM-D EM-10 Mk II&hellip; *and in far fewer letters!* I used sharp, fast primes on my Olympus kit as well, but there's no question that massive sensor paired with that stunning Ricoh prime gets you great shots.

But I suppose this is where more of that subjectivity comes in. While I couldn't fault its technical specs or picture quality, the GR III wasn't as fun as my Olympus kit. Maybe it was the result of growing up using point and shoot film cameras, bridge cameras, and budget SLRs, but I missed the electronic viewfinder more than I expected. I loved being able to hold a camera to my eye, compose a shot, adjust a range of mechanical dials by feel alone to set the exact parameters I needed, and take a photo. Using the Ricoh is more akin to a smartphone camera app, which for some people might be far more intuitive and fun than messing around with dials.

As with everything, tradeoffs are made to achieve a certain design. The lack of an electronic viewfinder, the necessity of a touchscreen interface, and a dearth of mechanical dials, are a large part of what makes the GR III such a compact camera. It'd be like complaining an ultralight laptop doesn't have four drive bays, or that it's hard to get into the bucket seat of a sports car. You can respect a design while also acknowledging it's not for you.

I was *so happy* that Clara had as much fun with the GR III as she did, that I decided to give it to her. We've been in a bit of a rut lately, and seeing her jumping over to a flower with that twinkle in her eye as she grabs it from her pocket to take a photo... that's the shit you can't capture in a technical spec sheet or performance review.

As mentioned before, I'm saving for a ridiculous OM-3 to be my next camera, because I know that's where my joy lies. But if you get a chance to try a Ricoh GR III, or the more recent IIIx in a store, I'd encourage you to check it out... and not just because it has the same model number as my first PalmPilot. If you're wired like Clara instead of me, you'll love it.
