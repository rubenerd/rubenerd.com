---
title: "Ey, fuhgeddaboudit"
date: "2025-02-20T09:20:49+11:00"
abstract: "Does this train go to Seacawwwwcuss?"
year: "2025"
category: Thoughts
tag:
- language
- new-york
location: Sydney
---
Yesterday I made [this throwaway comment](https://rubenerd.com/unobtainium-pc-building-in-2025/)\:

> *Forget about it*, as my beloved New York friends would say in an accent I can’t muster, but it doesn’t stop me trying.

That public troll earlier this week must be infuriated that once again I'm quoting someone, only this time it's... *myself!* On my own blog, no less! The absolute *hide* of some people. Hyde? Is Hyde Park where you go not to be seen? Wait, Hyde Park is London. Also, every Australian city for some reason.

Along with *whaddyawant?* and *ey, does this train go to Seacawwwwcuss?*, my favourite New Yorkism has to be *fuhgeddaboudit*. Clara and I took our first international holiday [together to New York](https://rubenerd.com/live-from-new-york-city/) back in the day, and it remains one of my favourite trips ever. The accents were, unashamedly, a big part of the appeal.

Still, I didn't know it [had an article](https://en.wiktionary.org/wiki/fuhgeddaboudit)!

> Pronunciation spelling of forget about it, representing New York City and New Jersey English.

Thanks to [Michał Sapka](https://crys.site/) for linking this to me. It made my morning.
