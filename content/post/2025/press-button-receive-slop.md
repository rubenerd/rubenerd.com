---
title: "Press button, receive slop"
date: "2025-01-24T08:45:12+11:00"
abstract: "How do you compartmentalise your contempt for one form of expression, while practicing your own?"
year: "2025"
category: Ethics
tag:
- creativity
- genai
- slop
- spam
location: Sydney
---
I love writing. I love writing blog posts, technical documentation, short stories, and everything in between. Forming ideas and putting them down in a text editor is just about the greatest feeling in the world. It's the same rush I get when I've finished a diagram, or built a system that meets a spec. I can point to *the thing* and say *hey, I made that!*

Sometimes I forget that not everyone thinks that way, in this case about writing. And I only have to think about how I approach drawing and music to relate. These are such foreign concepts of expression to me that I couldn't begin to understand the process. I watch Clara draw, or my brother-in-law compose a song out of thin air, and it's magic to me. I'm in awe of how they're able to take abstract concepts and inspiration, and make something unique. Then they say the same thing about writing, something that feels so basic and obvious to me. Maybe with practice I could improve, but it's not where my passion lies.

Fast forward to today, and I've seen an increase in technical bloggers using genAI tools to create slop to accompany their words. Like me, they lack the creativity and technical experience to draw. They don't want to or can't pay an artist, or spend the time sourcing the perfect permissively-licenced image, so they use a tool trained on the works of millions of people without their <abbr title="permission, attribution, compensation, knowledge">PACK</abbr>. They may have even paid for access to the tool, which is a whole other ethical quagmire I've already covered at length here.

Anyway, I have questions! The slop itself is milquetoast, derivative, a bit creepy in that "uncanny valley" way, and riddled with hilarious errors. My favourites aren't people with seven fingers, they're computer keyboards with bad geometry, and nonsensical text on everything from fake signs to screenshots. Do people not realise how comical these images are? How they actually *detract* from their words, not add to them? Do they... do they not... *look* at them before posting? I suppose when you don't value something, a quick and cheap simulacrum is acceptable.

There's a profound irony to this I find fascinating. The writer understands the value of the creative process, *because they're writing*. If they didn't, they'd farm the words off to a planet-boiling LLM. And yet, they lack the introspection to realise they're doing precisely that with their slop images. Creativity is disposable, *except for theirs?* It's so deeply weird, like taking the time to cook provisions, then throwing them into a known-leaky boat. For six months. [Can you imagine doing that](https://www.youtube.com/watch?v=cSGpLto1yxU "Split Enz - Six Months in a Leaky Boat")!?

*Adjusts tie*. Ah, Kiwis. I need to get back there one day. It's like being in the US, and flying to Canada! Only with more sheep.

Such a position also makes me ask: do people really not like making things *that much?* Do they really see the creative process as something rote and mundane to be scripted and automated, just like their fridge light coming on automatically to illuminate their soylent green and antidepressants? Press button, receive bacon?

I'm sure some people are just lazy, or incurious, or in a managerial position where their only KPI is *make line go up*. Those people I understand, sad as their positions are. There's also massive financial incentives to flood the web with genAI slop for a quick buck, just as there was playing SEO games before that.

But to the writers specifically who are dumping garbage in their vegetable crisper, I'd *implore* you to reconsider. You don't need them! Eschewing (gesundheit) an image entirely is preferable. Heck, even a stick figure or a bunch of shapes thrown together in LibreOffice Draw would be more useful and charming than anything these slop machines are puking onto your site and stinking up the joint. You&mdash;and by extension, your words&mdash;deserve more.
