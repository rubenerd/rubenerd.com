---
title: "Baltic states disconnect from Russian grid"
date: "2025-02-11T10:52:14+11:00"
abstract: "Those arcs were incredible."
thumb: "https://rubenerd.com/files/2025/noelreports-arcs.jpg"
year: "2025"
category: Thoughts
tag:
- baltic
- estona
- europe
- engineering
- latvia
- lithuania
location: Sydney
---
[Public Broadcasting of Latvia](https://eng.lsm.lv/article/economy/economy/08.02.2025-saturday-a-success-for-baltics-newly-independent-electricity-grids.a586281/), on what finally took place over the weekend:

> The electricity systems of Estonia, Latvia, and Lithuania continue to operate independently and problem-free following their de-coupling from the electricity network that formerly linked them to Russia and Belarus.
>
> On Sunday morning voltage deviation tests will be carried out, after which synchronization with Continental Europe is due to take place around 14:00 on Sunday.

Congratulations to all the engineers who made that a success. I can't imagine just how much work this would have been to plan and execute.

This [video clip shared by NoelReports](https://mstdn.social/@noelreports/113972984053584494) on Mastodon shows the grid being physically disconnected in Estonia. The arcs were incredible:

<figure><p><img src="https://rubenerd.com/files/2025/noelreports-arcs.jpg" style="width:320px;" /></p></figure>


