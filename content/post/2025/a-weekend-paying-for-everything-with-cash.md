---
title: "A weekend paying for everything with cash"
date: "2025-03-10T08:21:24+11:00"
abstract: "It was a fun experiment, but I’m glad I don’t need it daily."
year: "2025"
category: Thoughts
tag:
- finances
location: Sydney
---
I pay for everything using cards, as I suspect most Australians my age and younger do. It was less a deliberate decision, and more something that "just happened". Thing is, I don't like when something "just happens", and would rather reflect on such circumstances and make a concious choice than let the "invisible hand" of the "inverted commas" decide for me. So I made an effort to pay for things with cash last weekend just to see. This included lunch, groceries, topping up my Opal public transport card, and a couple of coffees.

I had no idea how *ingrained* the act of using a card was. I would reach for one of two cards I use to pay for things&mdash;depending on what the store accepts&mdash;before taking a metaphorical step back and remembering that I was supposed to be paying with flexible pieces of plastic and rigid metal tokens instead. I re-sheathed the cards, opened the bifold of the wallet instead, and began the process of paying physically.

I had to recall just how one *hands over* notes and coins, and how to hold out my hand to accept a complicated assortment of cash as change from someone. It reminded me of accepting my first change from a cashier as a kid when my mum showed me how it was done. That was a sweet memory. Fortunately it did come back to me almost immediately, like tying shoelaces on formalware after I insisted I'd never use shoes with laces again.

But oh boy, can it take time. Counting out coins, checking if you have exact change, and flicking through your wallet trying to find the right notes, it can feel like an age. I didn't have an issue with the mathematics necessarily, but I felt this time pressure to get it right, lest the queue of people behind me become impatient by my dawdling, doting dispensation of decimalised denominations. I dropped coins at least once, which resulted in a mad scramble across the floor between myself and the checkout staff as we tried to gather them up before they rolled onto the street, drain, or something.

I also remembered just how important receipts were. People still ask for receipts on a regular basis in Australia, but as with my habitual card use, I'd become used to saying "no need, thank you". Why would I need a receipt, I have a transaction record on my... oh wait, crap. Twice I had to go back and ask for one having dismissed them, though fortunately I was the last person they'd served on both occasions, and were able to press a button to deliver me a small sheet of thermally-printed paper with a bunch of numbers on it.

But it wasn't all awkward. There were times where something would add up to $4.80, where I said "keep the change, have a nice day" to a barista. That was fun. Other times I'd swap coins with Clara so we both had a more reasonable amount of the stuff, and with a more even spread that would be useful to both of us. It reminded us of our trip to New York in the late 2010s, remembering what a "quarter" was, and whether we each had enough. $0.25, what a weird amount of money. Anyway, there was an odd sense of camaraderie from doing this, like we were engaged in a shared struggle.

I was then reintroduced to the idea of "breaking" large notes. The green $100 I had in my wallet would be looked at with scorn or dismay at a coffee shop, but a topup machine or those infernal self-service checkouts wouldn't care. Suddenly I had a need to buy something small and affordable from one of the latter places so I could receive reasonable denomination notes like $10 and $20. Of course it gave me a $50 which was only marginally better, but it was a start.

I eluded to this above, but this process also forced me to confront something I thankfully forgot about with cards: jingly jangly pants full of metal tokens. It was a bit of a novelty again seeing the Queen in her younger days from the 1960s, and thinking it had been in circulation ever since. How many people must have used this coin? That soon turned to disgust though, as I pondered how clean these worn coins and notes were. One poor $5 note with Her Majesty on it looked decidedly worse for wear, and with stains I'd rather forget.

By Sunday evening I was sitting at our shared budget spreadsheet adding up these cash transactions in the ledger, lest I forget anything. The remaining cash I'd spent didn't quite match the receipts. Maybe it was from coins we'd swapped, or change I'd let them keep, or maybe it was another 20c that had somehow rolled off into the setting sun as I scrambled to pick up its brethren from an unfortunate counter-top tumble of clumsiness. I then had to add some numbers to *The Barrier* category for the first time in years, named for the phenomena in that Australian children's book *Finders Keepers* for stuff that disappears never to be seen again.

I will say, there was a social aspect to using cash again that was a bit fun, even if awkward at times. I also did feel a *bit* of that sense that I was spending "real" money when parting with physical notes as opposed to digital numbers on a screen, though not as much as those financial self-help gurus said I should. There was also this sense that I was *sticking it to the man*, so to speak. I'm off the grid, so nobody would know that I like... coffee! Or that I go to grocery stores to buy... toilet paper. I'm sure the security cameras tracking me and my spending were just there for show anyway, irrespective of my payment method.

But this morning I went to pay with a card. It was instant. And sure, I probably paid a fee for doing so, but that was likely a wash given the money I was leaking in other ways with cash. And when I got home, I had a transaction in my online banking I could reconcile on my desktop at my leisure. *WOW* I forgot how useful that was.

Cash is one of those things I feel like I'm *supposed* to prefer. I can see where it would have appeal, and why still having it available as an option is absolutely necessary for privacy and financial accessibility. I'm also still going to keep some cash in the wallet at all times anyway, lest our electronic overlords strike and make card payments impossible. But in the meantime, cards do it for me.
