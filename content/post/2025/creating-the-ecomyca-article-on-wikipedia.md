---
title: "Creating the ecomyca article on Wikipedia"
date: "2025-01-31T07:16:03+11:00"
abstract: "ecomyca (えこまいか) is a rechargeable contactless smart card issued by the Toyama Chihō Railway."
year: "2025"
category: Internet
tag:
- japan
- trains
- wikipedia
location: Sydney
---
Last week I created an article for the [ecomyca card](https://en.wikipedia.org/wiki/Ecomyca)\:

> ecomyca (えこまいか) is a rechargeable contactless smart card issued by the Toyama Chihō Railway in Japan since 2010. The name is a portmanteau of "ecology" and "my card", and is a play on the Toyama dialect "ikomaika" (行こまいか) which translates to "let's go".

This was a lot of fun to research, and definitely pushed my limited Kanji and Hiragana! Anyone who's fluent in Japanese, or has spent time in Toyama Prefecture, feel free to add to it, or ping me if you have more info. I only built the absolute basics.

I've noticed Wikipedia has massive coverage for the big Japanese cities, railways, and cultural franchises, but very little elsewhere. I suppose it's not surprising; Wikipedia's jazz catalogue is in a similar shape.

The last Japanese train articles I made were for the colourful Shimabara Railway in Nagasaki, such as [Shimabarakō Station](https://en.wikipedia.org/wiki/Shimabarak%C5%8D_Station). These were "red links" on the English Wikipedia for decades, probably because tourists don't visit them to the same extent they do Tōkyō, for example.

I'm also definitely making it a mission to visit more places outside the big cities in Japan. I've loved every trip we've been able to make over there, but our time in Odawara definitely ranks among my favourites.
