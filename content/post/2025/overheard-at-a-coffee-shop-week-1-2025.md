---
title: "Overheard at a coffee shop, week 1 2025"
date: "2025-01-08T14:12:37+11:00"
abstract: "Another instalment in the overheard chatter series."
year: "2025"
category: Thoughts
tag:
- overheard
location: Sydney
---
Where we're coming from, we don't need no context! Welcome to the next in our series of overheard coffee shop snippets, I'm your host Yves Drop.

* Nah I didn't get a break, I work for a living.

* Do you drink cappuccinos? No? Wait, what?

* Everyone is a dictator. You are. I am. We vote for dictators. Even the ones that say they're [sic] dictators are dictators. They're the ones you have to look out for.

* How does that even work? I should be able to just press a button, *amirite?*

* Can I get that to go please? No sorry not takeaway, I just meant if I could. No no it's fine, I'll have here. But *can* I? Oh good.

* **Person A:** And it all comes under cover of everything. **Person 2:** Wait, everything? **Person A:** Oh yes, yes, mmm... *everything*.
