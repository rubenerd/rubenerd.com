---
title: "Our new sandstone friend"
date: "2025-02-06T17:55:45+11:00"
abstract: "We found him on the side of the road. He’s delightful."
year: "2025"
category: Thoughts
tag:
- geology
- history
location: Sydney
---
Clara and I were walking down the street last week&mdash;as one tends to do when requiring bi-pedalled locomotion to a specific location without encountering a powered vehicle of some sort&mdash;when we noticed a large truck depositing a small shower of rocks and other debris on the side of the road after what could best be described as an ill-advised, last-minute turn into a side-street. Some of those words did not require a hyphen, or likely any of them, but I haven't used them in a while and I thought it fitting.

Today's piece of rock did not come from that truck. In fact, that story I just told you was completely pointless. *Or was it?*

Later that same day, is a phrase with four words. Clara and I were walking on a different road within the same suburb, when we encountered a small pile of rubble. Much of it consisted of small, sharp pieces of rock that looked like they came from a quarry, or a truck driving away from such a facility. It *could* have been the aforementioned truck driven by someone reckless and irresponsible, or it could have been another truck. The world consists of many types of truck, some of which are bound to work at the same facility performing similar jobs.

We thought the pile odd and kept walking, but I was struck not by a rock (thankfully), but by the *appearance* of one. It was a small piece of sandstone, about the size of one of those American scones you can buy in Japanese branches of Starbucks, or two British Imperial scones without the jam and cream.

I can't explain why, it made absolutely no sense. But I had to take him home. I brushed off some dirt, named him *Colonel Sanders*, and here he is:

<figure><p><img src="https://rubenerd.com/files/2025/sanders@1x.jpg" alt="Our small new sandstone friend in afternoon sun, sat atop a cork board." srcset="https://rubenerd.com/files/2025/sanders@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

Sydney famously sits on several layers of this stuff, which is why so many classical buildings are made of it. It's a *fascinating* rock, because it doesn't feel so much like stone as it does impossibly-compact sand. Which I suppose it is. Traces of the stuff crumbled off in my hands as I carried him home, but he's otherwise in solid shape.

I'll admit, I had him sat on the table this afternoon while I worked, and I couldn't help but look at specific grains twinkling in the mid-afternoon sun. We have a piece of history, millions of years old, on our coffee table. I'm sure each distinct layer tells its own story, like rings on a tree.

My parents always had a *Bowl of Things* in the centre of their dining table, in which they gathered random things over the course of their married life. Clara and I recently made a similar purchase of a cabbage-shaped vessel from a big box store, which is utterly delightful. We try not to let too much *stuff* into our lives for a whole host of reasons, but I think Colonel Sanders will take pride of place. What a great find!
