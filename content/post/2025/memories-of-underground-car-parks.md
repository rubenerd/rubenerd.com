---
title: "Memories of underground car parks"
date: "2025-02-22T07:45:52+11:00"
abstract: "I never wanted to experience this again!"
year: "2025"
category: Thoughts
tag:
- cars
- public-transport
location: Sydney
---
I've been on record here espousing what may charitably be described as a *dislike* of motorised conveyances of an automobile nature. The more rational and level-headed among urbanists may be more specific in decrying car *dependency* over the cars themselves, but I just don't like them. Cars take up space, they foul the air with their emissions and brake dust, they kill and maim millions, and seemingly turn rational people interacting with pedestrians into unhinged monsters frustrated that they had to stop at a level crossing to let a plebeian dash to the other side of the road in the pouring rain from their glass bubbles.

*(That's not to say I don't have preferences purely from an art or industrial design perspective. I love learning about ocean liners and steam turbines, while also acknowledging that it's better we don't have coal-burning ships everywhere today. I also oddly find racing games to be my favourite outside simulators and city builders, which makes even less sense. But I digress).*

Clara and I were walking back from a restaurant one evening when we started talking about memories from our childhood, and of things we used to do we don't have to now. I had this specific memory of being stuck on an exit ramp of a car park, and how I vowed I'd never want to subject myself to them voluntarily!

Cars in Singapore, more than most places, are optional. Leaving aside the eye-watering price for the vehicles themselves, the registration, and the requirement to bid on a limited number of *Certificates of Entitlement*, you don't need them there. The public transport system takes you everywhere, and for those times where you do need a ride, taxis are affordable, clean, and ubiquitous.

Okay, I just admitted to using taxis, *which are cars*. Maybe I should have said *private* cars. Even though some taxis are private cars. Stop making me look bad; you get the point.

That said, is a phrase with two words. My parents, perhaps more specifically my dad, still liked having a car. Sometimes we'd take road trips up to KL in Malaysia (I would have preferred the KTMB train, but hey, getting out of the city is always a good thing), but usually the car was used for utterly pedestrian (**HAH!**) things like family trips to the shops. I didn't really question it at the time, though when I was old enough I was taking the MRT everywhere I wanted to go instead.

Singapore has a population larger than each Baltic state in a land area smaller than an Australian farm, so large surface parking lots were mostly confined to the far-reaches of the island like Jurong East or Woodlands. This means underground parking in most shopping centres and office buildings, which means underground ramps.

The most epic&mdash;in a matter of speaking&mdash;was the corkscrew ramps one needed to negotiate to enter the car park of Ngee Ann City, that stately granite building on Orchard Road where the Kinokuniya and so much good food was. It took *forever* to drive down there because there would always be a queue, not to mention all the wasted time and smoke driving in circles to find a free space to park in that massive catacomb of columns and cars. Ooh, I'm proud of that one.

The trip *up* though was even worse. The toll gates meant cars could only go through single file, so sometimes those queues would back up further than someone after a late-night bender eating... let's nip that metaphor in the bud. Later technology like wireless tags and licence plate recognition removed one bottleneck, but the ramp still deposited you on a side street with traffic lights, so it still sucked. And yes, I absolutely *detested* crossing in front of that as a pedestrian, much as I can't stand being anywhere near those gaping exit holes of an Australian Westfield car park.

As I mentioned, it was during one of those slow, smelly trips up the exit ramps where I decided I never would subject myself to that again. By the time I was a teenager, I would offer to take the train and meet them there, blaming it on a bout of car or motion sickness, or maybe a headache. The truth was, getting the MRT from our place to Orchard, then walking up to the shop or restaurant was an *infinitely* nicer experience than being a passenger stuck in traffic, then in one of those awful on-ramps.

And yes before anyone asks, I **always** beat them there, a fact of which I was unreasonably proud. Did I power walk *briskly* from the station to the shop or restaurant to make sure I got there first? I mean, yes, sure. But in the indelible words of Frank Sinatra, *you couldn't take that away from me!* Which I knew, because I had time to browse the HMV while waiting for the car to arrive.

To each their own, and maybe you might prefer to have a large house somewhere that requires a car to get a bag of milk or a cup of coffee. I'm just glad that Clara and I were lucky enough to buy a nice (albeit smaller) place near a station and a bunch of shops, so we can get what we need on foot, or by rail-based conveyance. Because the idea of circling a car park looking for a place to deposit a fume-belching couch and two armchairs on wheels sounds awful, and I never&mdash;never!&mdash;want to do that again.

Also studying for high school exams. That sucked.
