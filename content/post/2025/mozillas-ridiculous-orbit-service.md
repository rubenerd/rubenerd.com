---
title: "Mozilla’s Orbit service"
date: "2025-01-04T11:36:44+11:00"
abstract: "Another service claiming an LLM can summarise something, which it can’t."
year: "2025"
category: Software
tag:
- genai
- slop
- spam
location: Sydney
---
Mozilla's fall from grace has been so sad to see, especially for someone who used to respect them and their ideals. [Now it's just embarrassing](httpss://web.archive.org/web/20250101150720/https://orbitbymozilla.com/), like witnessing your uncle stumbling drunk into a family dinner and immediately hitting on the wait staff a third his age:

> AI you can trust. Easily summarize emails, docs, articles, and videos across the web — without sacrificing your privacy

As a reminder, **genAI tools don't summarise,** they shorten. If you don't understand the difference, you've probably been spending too much time talking with ELIZA, or autocomplete on your phone, or that stochastic parrot your neighbour bought who keeps flying in and leaving feathers everywhere because you left your window open for fresh air and haven't got around to installing one of those flyscreens yet. *Parrot screens?*

This smacks of the same desperation all those companies felt to implement cryptocurrency or blockchain tech into their systems, *after* it had all been debunked. I thought [FOMO](https://en.wikipedia.org/wiki/Fear_of_missing_out "Fear of missing out, on Wikipedia") only affected individuals, but evidently not.

If there's a silver lining here, is a phrase with six words. Parrots!
