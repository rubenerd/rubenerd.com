---
title: "The AMD Radeon RX 9070’s power efficiency"
date: "2025-03-07T17:00:25+11:00"
abstract: "It looks great! I’m stoked for a good 220W card."
year: "2025"
category: Hardware
tag:
- amd
- graphics-cards
location: Sydney
---
When AMD announced their 9000-series Ryzen CPUs late last year, the tech press largely reacted with bemusement and boredom. This was a shame, because [as I wrote last August](https://rubenerd.com/amds-new-65-watt-cpus/)\:

> They've managed to bring the TDP back down to what my two-generation old 5700X has, and with a significant performance uplift. They use less power and run cooler than 7000 series under equivalent workloads, which is welcome news for Mini-ITX builds, and those who want quieter setups.

I don't have a CPU upgrade on the horizon, mostly because it would involve a new motherboard and DDR5 memory, and I have other priorities right now. But when I do, these CPU's are *very* compelling to me in a way the hot and thirsty 7000-series chips weren't. Hotter silicon means more active cooling, which means potentially larger cases, more fans, and more noise. It's also heat I'm pumping into our study that needs to be pulled out with more air conditioning, which in Australian summer isn't cool.

Get it? *Isn't cool?* **AAAAAAAAAAA!** Quiet you. Wait, quiet, because fans! *Inhales...* ***AAACOUGHWHEEZE.***

&hellip;

I bring this up because AMD just did the equivalent of a 9000-series Ryzen introduction this year with their latest **RX 9070** graphics card. Announced alongside their higher-end **RX 9070 XT**, they're designed to broadly compete with NVIDIA's 5070 line of cards, hence the change in naming scheme.

Here's the announcement slide AMD presented earlier this year. See if you can spot what made me keen, based on my rambling about efficiency above:

<figure><p><img src="https://rubenerd.com/files/2025/rx9070@1x.jpg" alt="" srcset="https://rubenerd.com/files/2025/rx9070@2x.jpg 2x" style="width:500px;" /></p></figure>

Third from the bottom there you can see that the RX 9070 has a reported total board power of 220W. This is impressive considering how close it is to features with the 9070 XT; it even shares the same glorous 16 GB of video memory! It also places it below even the 5070's 250W, and the 5070 Ti's 300W. These are impressive numbers if the touted performance was there.

Here's where the reports differ somewhat. For example:

* [Hardware Unboxed 📺](https://www.youtube.com/watch?v=gWIIA-a9Q9A) concluded the 9070 wasn't as "good value" than the 9070 XT, despite their own tests showing the 9070 drew less power per frame. I chalk this up to us having different priorities.

* [GamersNexus 📺](https://www.youtube.com/watch?v=LhsvrhedA9E) were more bullish, claiming the 9070 demonstrated power efficiency up there with the best NVIDIA cards in most of their tests. I agree with Steve, this is a vast improvement over the previous-generation 7900 XTX, and a real achievement.

* [TechPowerUp](https://www.techpowerup.com/review/asus-radeon-rx-9070-tuf-oc/44.html) concluded their exhaustive article that their partner card was "whisper quiet" even under full load, which speaks to the power draw and effectiveness of the cooler and fans. This is music to my ears! 

* [JaysTwoCents 📺](https://www.youtube.com/watch?v=AHpIGcABFbc) showed that not only is the power efficiency impressive, but the resulting output and power draw are also surprisingly more consistent than the equivilent NVIDIA silicon. I'm very sensitive to visual jitter and stutter, so this is also excellent news.

* Another channel I used to love spend a significant amount of time talking about AI slop generation, but their conclusion mirrored GamersNexus.

This bodes well! If and when I have the budget to replace my ZOTAC Twin Edge RTX 3070 with its dreadful cooling solution, I might be looking out for an RX 9070. That is, assuming availability and pricing being reasonable. I can't wait to kick these NVIDIA binary drivers to the kerb on my Linux gaming partition once and for all.
