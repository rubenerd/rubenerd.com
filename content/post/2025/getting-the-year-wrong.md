---
title: "OM-1 Mark II follow-up, and getting years wrong"
date: "2025-01-26T08:35:43+11:00"
abstract: "The camera came out in 2024, not 2025. Whoops."
year: "2025"
category: Hardware
tag:
- cameras
- olympus
- om-system
location: Sydney
---
I said this in my recent post about the [gorgeous OM System OM-1 Mark II](https://rubenerd.com/the-om-system-om-1-mark-ii/) Micro Four Thirds camera that I'd love to get:

> Unfortunately, OM Systems heard me, and are trying to convince me to spend our travel savings not on food or airfares, but instead on their highest-end M43 camera: the OM-1. The second revision came out this month, the OM-1 Mark II.

I was surprised at the availability of a new camera body that had only been out mere *weeks*&mdash;maybe even *days*&mdash;according to all the review sites I was reading. Then I read this recent article in [DPReview about a new camera](https://www.dpreview.com/news/4954706636/om-system-teases-release-of-new-camera-february-2025 "OM System is building suspense for a new camera coming on February 6th"):

> OM System is working to build excitement for a launch happening on February 6. The company released a short teaser video, offering brief glimpses of an unreleased OM System camera. This video follows up on a post from the OM System CEO at the beginning of the month announcing that the company is working on a new camera and lenses.

I thought this was a bold move, given they only just released an update to their aforementioned flagship.

By now, I'm sure you can see where this is going:

> OM System OM-1 Mark II initial review: AI AF improvements to Stacked CMOS flagship. Published Jan 30, 2024

Yup, I read the release date as being January this year. **It's 2025.**

That said though, while I'm spruiking DPReview, they did share news of [OM System's lens plans](https://www.dpreview.com/news/6698523374/om-system-new-camera-lenses-shortly-ceo), which *was* published this year.
