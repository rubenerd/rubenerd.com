---
title: "What to consider before consuming"
date: "2025-01-05T10:12:32+11:00"
abstract: "Various rules and advice I’ve received before forking over money for something."
year: "2025"
category: Hardware
tag:
- clutter
- money
- psychology
location: Sydney
---
Once again, I've written a title that somehow manages to conjure something completely different to what I intended. Or is the imagery of an alien eating a human a thinly-veiled critique of our economic system? I'll let you decide.

Where was I going with this?

I have a complicated relationship with *stuff*. Stuff, and more specifically, *things*. Reading, tinkering, building, fixing, playing with, and displaying *things* make me happy, yet conversely I find clutter to be anxiety inducing in a way few other things are. It's akin to being told by my doctor in 2022 that I need *drastically* more vitamin D, and that I'm allergic to several kinds of grass. How are these contradictory positions tenable!?

I've learned to cope with this in a few different ways.

* **The 24 hour rule.** My granddad Jim Ross taught me this when I was a kid. If I see something in a store or online, sit on it for at least a day before committing to buying it. This gives you time to research your options, and short-circuits the urge for instant gratification that retail stores are designed to exploit.

* **The budget envelope rule.** Is it in the budget for this month? If not, would I be willing to give up our planned holiday or our Friday night movie for it? Can I save for it? Would I want to? This helps to contextualise the true cost of something. A mortgage is the only debt I want/need.

* **The placement rule.** Is there somewhere for this new thing to go? *Everything gotta be somewhere*. If not, what's my plan here, for it to go on a stack on the floor? That sucks.

* **The swapping rule.** If I'm contemplating buying something, what would I be happy to swap it with at home? This prevents me accumulating more stuff than what I already have. I also find comparisons a useful way to measure how much I want something, just like how it's easier to taste the difference between two things by having them both right there.

* **The lifecycle rule**. I know that it's harder to part with something I brought into my life, than to walk away from something I haven't. I'll need to sell it, figure out who to donate it to, recycle it somehow, or throw it away. That sounds like a lot of work.

* **The aspirational rule.** Am I buying this thing because I think I'll use it, or do I know deep down it'll accumulate dust? I'll be honest, this is the toughest one to think about.

My mum always said I liked making rules for myself. Good thing she was wrong! I called it the judgement rule. Take *that*, Debra! By the way, I miss you.

...

I've seen others advocate for aids like paying for things with cash, so you can feel the tangible impact of your decisions. I prefer paying with cards because we budget and reconcile to the cent each week, and statements are a handy record to do this. Others say to always shop with other people, so they can hold you accountable. I'd say in the case of Clara and I, that's been a... mixed bag! Do I want this widget? Yeah you're right, probably not. What about this Pompompurin plushie? Well of course we have to!

This leads to another useful framework I only just saw via Mastodon yesterday, [by the excellent Annie Mueller](https://anniemueller.com/posts/anticipation-experience-satisfaction)\:

> **Anticipation, experience, satisfaction**

> Start comparing how much you want something (or think you want it) with how you feel while you’re actually having it.
> 
> Compare your anticipation with your experience.
> 
> Start comparing how much you want something with how you feel after you’ve had it.
> 
> Compare your anticipation with your satisfaction.
> 
> Wanting something is not the same as enjoying it.

I've already deployed this today for a piece of retrotech. I know that for certain classes of these things, the race is more fun than winning. Finding something rare and unusual feels like an accomplishment, but then I get it and don't use it. Worse than that, I then feel *guilty*. That's not helpful.

Anyway, food for thought to start the year. I should make a rule for that.
