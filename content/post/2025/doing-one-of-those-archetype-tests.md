---
title: "Doing one of those “archetype” tests"
date: "2025-02-26T09:10:24+11:00"
abstract: "I’m a problem solving artisan helper! Maybe."
year: "2025"
category: Thoughts
tag:
- pseudoscience
location: Sydney
---
I hold archetype tests in the same esteem as I do those Myers-Briggs personality quizzes, and horoscopes more broadly. But it was still a bit of fun.

This latest one my colleague clued me into was *massive*, but it returned these as my dominant archetypes:

<figure><p><img src="https://rubenerd.com/files/2025/archetype@1x.jpg" alt="You are most like: Problem Solver, Artisan, Helper" srcset="https://rubenerd.com/files/2025/archetype@2x.jpg 2x" style="width:500px; height:315px;" /></p></figure>

This was the description for a **Problem Solver**:

> Problem Solvers are motivated to support and help others in an industrious and professional manner. They tend to be supportive, conscientious, responsible and proactive.

An **Artisan**:

> Artisans are driven to use their creativity to bring life to beautiful and well-crafted ideas. They tend to be imaginative, detail-oriented and attuned to their own and others' emotions.

And a **Helper**:

> Helpers are driven by compassion and care for others, and support of their emotional needs. They tend to be empathetic, nurturing, generous and agreeable.

Again, I'm not sure how accurate any of these are. But they're basically describing a tech blogger, aren't they? Maybe?
