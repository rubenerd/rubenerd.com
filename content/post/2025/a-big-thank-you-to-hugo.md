---
title: "A big thank you to Hugo"
date: "2025-01-07T10:51:00+11:00"
abstract: "Hugo is the fast static site generator with a great community that has powered my site for a decade."
year: "2025"
category: Internet
tag:
- weblog
location: Sydney
---
One thing I didn't do in my [twenty-year anniversary post](https://rubenerd.com/this-site-turns-20-today/ "This site turns 20 today") was thank the specific projects and software that make this blog possible. I apologise for this massive oversight!

The [Hugo static site generator](https://gohugo.io/) is the first one I want to call out. I've bounced around almost a dozen CMSs here since 2004, but I've [been on Hugo since 2016](https://rubenerd.com/hugo-generating-content-in-memory/ "Hugo generating content in memory").

I first moved to Hugo from Jekyll when the latter was taking upwards of half an hour to render the site. **Hugo takes less than twenty seconds** to generate almost [ten thousand posts](https://rubenerd.com/year/ "Yearly archive"), hundreds of [podcasts](https://rubenerd.com/show/), archives for thousands of [tags](https://rubenerd.com/tag/ "Tag archive"), and some [truly ghastly theme code](https://codeberg.org/rubenerd/rubenerd.com "My repo for the site on Codeberg") that I've hacked together over many years.

Could this be the biggest Hugo install? Maybe, maybe not! Either way, the scale makes it a good test case, and I can report Hugo has zero issues at this scale.

Hugo also has a great developer community. I've rarely had to ask a question, because Bjørn Erik Pedersen, Joe Mooring, and so many others have been quick to answer someone with my exact problem on the [forums](https://discourse.gohugo.io/). They're worth their weight in gold, and I can't thank them enough.

I've contemplated going back to true server-side software for some of the niceties that provides. I've also delegated some of my pages to my silly Omake XML format, such as the [Omake](https://rubenerd.com/omake.xml) and [Coffee](http://ruben.coffee/) pages. But for writing? I fire up Vim or Kate, write a post, generate the site, and everything just works. That, coupled with all my posts being in version control, has simplified a *lot* and let me concentrate on writing instead of faffing about with another proverbial LAMP stack.

Thank you!
