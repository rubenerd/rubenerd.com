---
draft: true
title: "How many Mac rumours MacRumours publishes"
date: "2025-01-28T18:11:09+10:00"
abstract: ""
thumb: ""
year: "2025"
category: Thoughts
tag:
- 
location: Sydney
---
Every now and then I like looking at MacRumors to see how they're living up to their site title.

According to the articles listed in the home page, and 

Out of eighteen stories, twelve are about iOS and iPads, and a couple are about Apple Pay and general Internet news, with nay a mention of Macs at all. The Guides section on the sidebar fares is a bit better, with a single Mac story among nine linked articles.

I suppose its to be expected. Even Apple's website only makes passing mention of Macs twice, and only in the context of financing.

Insert obligatory comment about missing [Apple Computer](https://www.economist.com/business/2007/01/11/drop-the-computer). I suppose it's still a better outcome for Mac users than Sun Microsystems.

