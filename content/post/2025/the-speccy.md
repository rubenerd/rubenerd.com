---
title: "The Speccy"
date: "2025-03-08T08:49:38+11:00"
abstract: "I’m finally giving some attention to this important machine."
thumb: "https://rubenerd.com/files/2025/zx128_bill_bertram@2x.jpg"
year: "2025"
category: Hardware
tag:
- retrocomputing
- zx-spectrum
location: Sydney
---
I have a terrible confession to make. Okay, I have many, but let's take them one step at a time.

I grew up on DOS and Windows 3.x machines, so all my 1980s home computer adventures have been retroactive. Retroactively *awesome*. But while I have way too much knowledge of the 486, developed an obsession with Commodore 8-bit kit, and later bought the same Apple //e Platinum my first school had... I know comparatively little about Sinclair and their cute little machines. It was *so much fun* reading the [Wikipedia post](https://en.wikipedia.org/wiki/ZX_Spectrum "ZX Spectrum article on Wikipedia") over coffee this morning.

For example, I didn't even realise that Amstrad had bought Sinclair and continued releasing Spectrums in a new case that closer resembled the Amstrad CPC. I didn't realise many of the BASIC commands were issued using single keys, which is how that initial rubberised keyboard wasn't *quite* as bad to use. I didn't know the ZX Spectrum 128K had an external heatsink "toast rack" on the side! And perhaps most surprising to me, I didn't know the ZX Spectrum +3 could run CP/M, my current retrocomputing obsession!

<figure><p><img src="https://rubenerd.com/files/2025/zx128_bill_bertram@1x.jpg" alt="Photo of a ZX Spectrum 128K by Bill Bertram." srcset="https://rubenerd.com/files/2025/zx128_bill_bertram@2x.jpg 2x" style="width:500px;" /></p><figcaption>Photo of the gorgeous ZX Spectrum 128K by <a href="https://commons.wikimedia.org/wiki/File:ZX_Spectrum128K.jpg">Bill Bertram</a>.</figure>

In today's context, you can run ZX Spectrum emulators, there are new kits you can buy to build and repair your own, there are modern drive interfaces for SD cards, and there's a thriving demo scene as there is on Commodore. It stands to reason, it was one of the best selling computers of all time. For all the frustration and missed opportunities of modern tech, we really are living with an embarrassment of riches when it comes to what's available today for these old machines, which makes sitting here reading about this stuff so much fun.

With a pile of unfinished retrocomputer projects growing larger by the day, it's clear to me I need to add another one. I've been learning some basic Z80 assembler in my spare time which I might write about when I'm less embarrassed to admit how pedestrian my knowledge is, and it'd be so damned cool to run some stuff on a Speccy alongside the Commodore 128 and the Apple //e with an Applicard or SoftCard.

If any of you have recommendations for books, blogs, video channels, or even software for the ZX Spectrum, let me know! I've got some long service leave coming up, and I'd love to spend some time exploring these machines.
