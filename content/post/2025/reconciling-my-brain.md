---
title: "Reconciling… my brain!"
date: "2025-02-19T16:25:10+11:00"
abstract: "I reconciled my accounts with dates set to 2024, because of course I did."
year: "2025"
category: Thoughts
tag:
- finance
- personal
- pointless
location: Sydney
---
I was reconciling my accounts over lunch&mdash;like a gentleman&mdash;when I went to my budget view and none of the resulting numbers made sense. All these accounts, transactions, and budget envelope amounts that had been zero'd out suddenly had wildly incorrect numbers going back to last year.

I scoured all the transactions I'd added since the beginning of the year. Everything looked correct! Why were some envelopes suddenly stuffed with extra money, and others were in the red, over the period of twelve months!? I'd literally entered these before, and they'd never been a problem before.

Oddly, it all started going haywire in February last year too, which is such a coincidence because it's February again now, and...

Oh. **OH!** *Ohhhhhhhhhhh*.

Not to get all Malcolm Gladwell on you, but *turns out* I'd set the date on a bunch of transactions to `2024-02-18`, instead of `2025-02-18`. Because *of course* I had. I updated the dates to reference the current year, and not some other year that had passed more than a month prior. Suddenly all the envelopes and accounts made sense again.

**Who would have thought?!**

&hellip;

I thought January was the month where you'd get numbers wrong on dates. The fact it's still occuring in February is a bit silly. Much like this post, really.

This has been the latest instalment of *Ruben did something silly, and now feels compelled to tell you about it too, in the off chance you're staring at a database with the same problems, but you're probably not, and if you are, your exact circumstances probably don't match mine*. It's a long title; I'm workshopping it. Which is a silly turn of phrase, because it has no axle, and *workshopping* is a needlessly-long word.

