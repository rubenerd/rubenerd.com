---
title: "The limits of advice, and having your own thoughts"
date: "2025-01-15T08:51:48+11:00"
abstract: "Nobody knows your circumstances better than you."
year: "2025"
category: Hardware
tag:
- cameras
- coffee
- shopping
location: Sydney
---
If you’re a massive nerd like me, you probably buy something new the same way you dip your toes into a new hobby: with **obsessive research!** Armed with a list of requirements and a target price, we read the reviews, scour the product pages, watch the videos, and maybe even build a comparison matrix (… or maybe that’s just me). Then, and only then, do we feel confident enough to walk into a store or click through to the purchase page on a site, because we have all the quantitative and qualitative data tabulated, formulated, and discombobulated every which way from here to Sunday.

This is where advice comes in. Advice is important when we're starting something new, because we have no experience to lean on ourselves. Specifications are great, and make you all rational, smart, and rationally smart. But what if you don’t know how aperture and shutter speed work together? Or what a conical burr is? Or a sector size? Is smaller better? Specifications are meaningless without context, something that's worth a post in its own right.

Eventually you start synthesising your own opinions based on these reviews and advice. Five reviewers and a backup provider said drive manufacturer `$FOO` make the best ones, and three coffee YouTube channels all said the retention on this coffee grinder makes it unworkable, so those are my opinions going forward too. *It's obvious, d'uh!*

I see this in a lot of things, but specifically with tech, music equipment, cameras, and coffee kit. A consensus will be reached within these respective communities, often due to trusted people in those communities. This can be powerful, but it's a double-edged sword.

First, you get people who have Opinions&trade; for something they don't own, have never used, or never done themselves. You see this often with creative people who veer into technical topics, as though the people leaving negative comments or feedback have the foggiest idea how to spray primer evenly, build a server rack in a confined space, or mix resin for a surface the size of a table. Streamers call these people "backseat gamers", which I like. The description I mean, not the action.

The advice itself can also be counterproductive. Recently AMD released a new line of CPUs that offered more power efficiency for the same workloads. I won't wade into the details, but suffice to say the consensus among reviewers fell between a shrug, and thinking they were a disappointing waste of silicon. This despite the fact they'd be perfect for someone upgrading from a CPU from before they became massive space heaters, or those who value quiet cases with less active cooling. You might not know this as a novice however, and you end up buying hotter silicon that's noisier for no practical reason other than the advice said so.

I've had this same experience with camera gear. We're told you should prioritise a camera with a larger sensor, a kit lens telephoto for general use, and a sharp prime lens. I tried a micro-four thirds camera with a (relatively) slow pancake lens, and it was so much fun and so damned *convenient* that I used it more than any SLR I had before. I then listened to advice, and got a follow-up to that camera years later that met all the specifications and guidelines, and it was so frustrating I barely used it.

This is where the reality of advice and research sets in. It's all a bit of a gamble. You hope that whomever is offering you tips on what to buy has the same requirements, taste, and budget you do. But it's unavoidable that sometimes you'll get something that doesn't hit the mark, even in spite of all the people telling you it's the best. What that *did* buy you though was experience, which is something you didn't have before.

This is all a classic *Rubenerd* roundabout way of saying that you're allowed to have your own views on things. I see *so many people* enjoying something online, only to be told that it's no good, a waste of money, or that they need to justify their purchase for some reason to a faceless nobody online who wants to [sealion you](https://en.wikipedia.org/wiki/Sealioning) into being sad, because your setup isn't what they consider ideal, or the best, or whatever.

Nobody knows your circumstances better than you! And yes, that includes subjective opinion as well.
