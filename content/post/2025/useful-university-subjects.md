---
title: "“Useful” university subjects"
date: "2025-02-10T09:28:10+11:00"
abstract: "A retelling of an old debate from Twitter, paraphrased in the style of Clarke and Dawe."
year: "2025"
category: Ethics
tag:
- clarke-and-dawe
- studies
location: Sydney
---
This is a retelling of an old debate I got into on Twitter back in the day, paraphrased in the [style of a Clarke and Dawe skit](https://www.youtube.com/watch?v=ELaBzj7cn14 "The Energy Market, Explained"). I miss their banter dearly.

**Brian:** And your name is?

> **Fund E:** Fund E.

**Brian:** Last name?

> **Fund E:** Mentalist.

**Brian:** Oh Fund E. Mentalist. That's clever.

> **Fund E:** Yes, yes it is.

**Brian:** Does that mean you can read peoples minds, Fund E?

> **Fund E:** Only as it pertains to financial matters, Brian.

**Brian:** Fair enough. It also says here you're a "free speech absolutist", what does that mean?

> **Fund E:** It means, Brian, that I consider free speech sacrosanct. It's a fundamental right that cannot be infringed by government or business, no matter how inconvenient the facts or thoughts being espoused.

**Brian:** Fascinating. Okay Fund E, well your topic this evening is Contemporary University Subjects. Your time starts now, best of luck.

> **Fund E:** Thank you.

**Brian:** What are the humanities, Fund E?

> **Fund E:** The humanities are the study of humans, Brian.

**Brian:** Correct. What subjects would let you study that?

> **Fund E:** The useless ones Brian.

**Brian:** Can you be more specific?

> **Fund E:** The ones that teach you [sexist rambling].

**Brian:** And how do they do that, Fund E?

> **Fund E:** Well universities make you believe silly things, Brian. They're a hotbed of brainwashing. They brainwash you.

**Brian:** Into taking useless subjects?

> **Fund E:** No Brian, they twist your views. It's unavoidable, because it's so intrinsic to the institution. They're lefty hotbeds of extremist thought. Identity, economics, politics, you name it.

**Brian:** If universities are "hotbeds of brainwashing" Fund E, should you be avoiding physicians and doctors next time you need surgery?

> **Fund E:** Well no Brian, they studied something valuable at university. That's my point.

**Brian:** Correct. If some subjects are useless, and others are valuable, who should be adjudicating on which is which?

> **Fund E:** Well obviously the universities refuse to Brian.

**Brian:** They make their money either way, don't they?

> **Fund E:** Exactly, Brian. Therefore I believe its incumbent upon governments to step in and tell universities they can't teach useless subjects. I don't want my tax dollars funding brainwashing at public universities. Businesses should also refuse to offer student loans.

**Brian:** You want government and business dictating what people can study?

> **Fund E:** That's right Brian. Someone has to.

**Brian:** Isn't that censorship, Fund E? I thought you were a "free speech absolutist"?

> **Fund E:** Well yes Brian, but&hellip;

**Brian:** But... what?

> **Fund E:** Sorry Brian, I was trying to come up with a witty rejoinder, but I'm drawing a blank.

**Brian:** Flunked your arts degree too?

> **Fund E:** I'm afraid so, Brian.

**Brian:** Well Fund E, that's the end of the round. You win this pride pin that says "you're loved"!

> **Fund E:** Oh that's a cracking design, thank you. Will it work on my adult kids you reckon?

**Brian:** How do you mean, Fund E?

> **Fund E:** They don't speak to me anymore, Brian. I'm lonely.
