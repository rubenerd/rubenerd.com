---
title: "Getting some scones in beautiful weather"
date: "2025-02-17T08:38:21+11:00"
abstract: "The humble British scone is amazing, and you should eat them."
thumb: "https://rubenerd.com/files/2025/scones@2x.jpg"
year: "2025"
category: Thoughts
tag:
- australia
- food
- scotland
- sydney
- uk
location: Sydney
---
Australia is like Canada in the sense we have cultural ties to Britain, but we've also been influenced by the US. One link with the [green and pleasant land](https://en.wikipedia.org/wiki/And_did_those_feet_in_ancient_time#%22Green_and_pleasant_land%22) is the humble *scone*, something we relished in partaking in during some very un-British weather in Sydney yesterday.

<figure><p><img src="https://rubenerd.com/files/2025/harbour-bridge-weather@1x.jpg" alt="The Harbour Bridge in nice weather." srcset="https://rubenerd.com/files/2025/harbour-bridge-weather@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

To be clear, I'm not referring to the *American scone* here, which is delicious in its own right, or the *Japanese/American scone* one can find in cute coffee shops in Ōsaka. They're worth a post of their own at some point, because they really do hit different.

British food cops a lot of flack internationally, which I don't think is (entirely) justified. Welsh cakes and rarebit, shortbread, porridge, the stunning Scottish cranachan, the Sunday roast, beer-batter fish and chips with vinegar, the baked bean, Yorkshire puddings, crumpets, the Plougman's sandwich, Christmas cake and steamed pudding, the ceremony of tea and dainty little biscuits, even chicken tikka masala if we're being generous. Scottish and English beer is wonderful, as is Scotch wiskey and Welsh gin. It's all *glorious*. Australians have their own twists on all of these, perhaps most famously with Vegemite, pasties, and pies.

I'll stop there at the risk of digressing any further than we already have.

British-style scones are everywhere in Australia, from fancy restaurants and tea houses to those takeaway "lobby cafés" in office buildings. Clara's and my favourite is the [Tea Cosy](https://www.therocks.com/eat-drink/the-tea-cosy) in central Sydney. They serve them with your choice of dozens of jams and spreads, and cream which must be placed *after* the spread (I know there's controversy about the order, so I wanted to set the record straight).

<figure><p><img src="https://rubenerd.com/files/2025/scones@1x.jpg" alt="A full table of scones and tastyness." srcset="https://rubenerd.com/files/2025/scones@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

It was amazing, as always. They also had a rose-water scone for the occasion which I felt like could go either way, but was surprisingly good. And as mentioned, the weather was amazing. I think it was the best Sunday we've had in a while.

I'm not quite sure where I was going with this post. Scones. Eat them!
