---
title: "Voting with our wallets"
date: "2025-01-17T08:17:25+11:00"
abstract: "A rigged ballot that’s always won by the people with the thickest wallet."
year: "2025"
category: Thoughts
tag:
- finance
location: Sydney
---
[Cory Doctorow](https://pluralistic.net/2025/01/14/contesting-popularity/ "Pluralistic: Billionaire-proofing the internet")\:

> A rigged ballot that's always won by the people with the thickest wallet.
