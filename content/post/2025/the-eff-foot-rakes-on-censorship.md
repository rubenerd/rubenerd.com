---
title: "The EFF is not a serious organisation"
date: "2025-01-10T12:11:08+11:00"
abstract: "“We applaud Meta’s efforts to try to fix its over-censorship problem”."
year: "2025"
category: Internet
tag:
- rights
location: Sydney
---
The EFF [saw Mozilla](https://rubenerd.com/mozillas-ridiculous-orbit-service/ "Mozilla’s ridiculous Orbit service"), and said "hold my beer" [on Mastodon](https://mastodon.social/@eff/113789440544631242)\:

> We applaud Meta’s efforts to try to fix its over-censorship problem [&hellip;]

Imagine seeing what's going on, and thinking that's an appropriate opening for a post. In the local vernacular: *you what mate?*

They've since issued what could (charitably) be described as a [partial retraction](https://www.eff.org/deeplinks/2025/01/metas-new-content-policy-will-harm-vulnerable-users-if-it-really-valued-free). But we've now seen, again, who's behind the curtain.

The EFF are surplus to requirements.
