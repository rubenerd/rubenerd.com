---
title: "My (first ever) good vitamin D reading!"
date: "2025-02-07T19:31:13+11:00"
abstract: "I’m chalking it up to our 10,000 step a day challenge."
year: "2025"
category: Thoughts
tag:
- health
- personal
location: Sydney
---
This sort of thing is normally a bit personal, but I had a blood test last week, and it's the first time *in my life* that I have "normal" vitamin D! Not excellent, but "acceptable".

<figure><p><img src="https://rubenerd.com/files/2025/aint-much-vitamind@1x.jpg" alt="It ain’t much, but it's acceptable vitamin d." srcset="https://rubenerd.com/files/2025/aint-much-vitamind@2x.jpg 2x" style="width:486px;" /></p></figure>

I'm sure it's my lifestyle and occupation, but I've lived my entire life with vitamin D either cronically low, or to the point where it's not above the margin of error. Even when I lived in Singapore and Malaysia, barely a few degrees above the equator with year-long sunshine, I somehow managed to avoid getting and/or synthesising the stuff. Doctors said it was quite the... "achievement".

Anyway, I'm chalking this up to another downstream benefit from Clara's and my [10,000 steps a day](https://rubenerd.com/getting-in-10000-steps/) challenge for the last twelve months. It does help our mood tremendously, it's allowed us to maintain the weight we lost, and apparently it also helps with getting enough vitamin D. Getting vitamin D, by going outside!? Who knew.

Check your vitamin D! Get some sunshine, or if you can't, make sure you're getting the approprite supplements. It's important.

Also my [regards and love to David Brandt's family](https://www.npr.org/2023/05/30/1178378575/no-till-farming-legend-reddit-hero-honest-work). The gentleman behind that meme was a wonderful person who, among other achievements, advocated for sustainable farming, and even hosted the French agricultural minister in 2015. For today, the sunflower in my blog bio is an ode to him.
