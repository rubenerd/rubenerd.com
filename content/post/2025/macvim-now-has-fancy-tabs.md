---
title: "MacVim now has fancy tabs"
date: "2025-03-05T09:01:24+11:00"
abstract: "Version r181 updates tabs with a new, clearer design."
year: "2025"
category: Software
tag:
- design
- macos
- vim
location: Sydney
---
I like them! They give me 1990s Lotus SmartSuite vibes somehow.

<figure><p><img src="https://rubenerd.com/files/2025/macvim-tabs.png" alt="Screenshot showing slanted tabs." style="width:500px; height:455px;" /></p></figure>
