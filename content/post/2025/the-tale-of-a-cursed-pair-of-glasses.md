---
title: "The tale of a cursed pair of glasses"
date: "2025-03-03T13:29:38+11:00"
abstract: "When your new glasses break twice, that’s amore?"
year: "2025"
category: Hardware
tag:
- glasses
- health
location: Sydney
---
Last December I received a new pair of glasses, a form of speculation equipment used to bend light entering my eyes such that the object of my attention is presented to my brain with sharper focus. While I tend not to need these devices when positioned at my desk, my self-imposed myopia does require their use when up and about, and for reading text further than a few metres away. Given how bad most drivers are, I also use these to give myself the best possible vision when crossing to the other side, lest I cross to the other side.

*(I was in my thirties before I realised the "chicken crossing the road" joke was so sinister. I grew up assuming it was just an example of getting a silly answer in response to asking a silly question. But no, a chicken crossing a road will get to... [the other side](https://en.wiktionary.org/wiki/other_side)).*

After inspecting the dozens of available frames, I redeemed what I considered some fairly innocuous glasses, albeit slightly rounder than the regular black rectangles I've worn for the last decade. *It might be fun for a change!* I thought to myself. Little did I know, is a phrase with four words.

A few weeks into using these devices, I was at a data centre helping the hardware team out for a bit of a change of pace, when I heard an odd creaking sound emanating close to me. Then without warning, half my field of vision dropped sharply in resolution, as though I was suddenly looking at a PC laptop shipping with a garbage 1080p panel in 2025.

*Turns out*, the left lens in these glasses had popped out. It happened suddenly enough that I *heard* the frame creaking before it split open, releasing the lens from its confines. I didn't recall seeing a crack prior to this catastrophic loss of structural integrity occurring; it sounded and felt as though it happened spontaneously. Fortunately the lens wasn't damaged on its trip to the floor, though its utility to me as an optical refraction device was hampered by it no longer being in front of my eye.

I took the glasses back to Owndays where I purchased the lens and glasses, and they sorted it out within half an hour. As the glasses were less than a year old, the breakage was covered under their standard warranty, which was a relief. They took my existing lenses, fitted them to new frames, and I left being able to see the world again. Wonderful!

&hellip;

You can probably guess what happened next. After another few weeks of wearing these glasses to see the world with delightful sharpness, they had accumulated sufficient smudges to warrant cleaning. I used the provided cloth to lightly rub the lenses, just as I had with dozens of other glasses over two decades.

*Creak~*

Uh oh!

This time, Clara was in the room with me and heard the resulting break, and the *ping* sound as the lens jumped out and fell to the floor. The frame had split in about the same place as before, in the same way, and with the same unsettling sound. It was as though someone dressed up as a ghost had stepped on a floor board in a haunted house and caused it to groan under their weight. Something something Scooby Doo.

Once again, I was back to my previous pair of glasses, which had seen&mdash;**AAAAAAH**&mdash;significant and unexpected use again when their replacements decided to become monocles, without even consulting me first. More than a bit rude if you ask me. Which glasses can't do, I suppose.

I went back to Owndays again, expecting to have to plead my case. You know when you know you're in the right, but you still roleplay how it'll go in your head?

> **Me:** Hello, yes, remember me?
>
> **Them:** Oh of course, you were the weird guy with the broken glasses we replaced free of charge under warranty! How can we help you this time? Looking to get some prescription sunglasses?
>
> **Me:** Well, so, here's the thing&hellip; it happened again.
>
> **Them:** What happened again?
>
> **Me:** The glasses. They, uh, they broke again.
>
> **Them:** Ah, I see. Once is understandable, but twice? What have you been doing to them to cause them to break twice?
>
> **Me:** I mean, I don't know. Wear them? I was cleaning them and&hellip;
>
> **Them:** Well you see, that's the problem. You should never clean glasses. That voids your warranty, don't you know? And besides, we only offer new pairs once. After that, you have to pay.
>
> **Me:** But I've been buying glasses from you for more than ten years. It's the first time I've ever had glasses break. I didn't drop them or manhandle them, I treated them the same way I've treated every other pair of glasses from your company without problems.
>
> **Them:** User error! You'll have to pay for the new frames.
>
> **Me:** Something something Australian Consumer Protection laws.
>
> **Them:** Hmm, okay. For you, just this once, we'll make an exception. But I'll need you to grovel some more.

Except, this didn't happen! I know, right!?

I took the glasses back, and the person on staff that day was just as confused as I was. Inspecting the lenses with her *functional* glasses, she theorised that one of them had been ground slightly larger than the other, which may have been too tight for the frames. She made me a new set of lenses, fitted them to a new pair of frames, tested them, and gave them to me that afternoon for free.

I am now the proud owner of another pair of glasses! Fingers crossed I make it past a month this time.
