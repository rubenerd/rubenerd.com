---
title: "Goodbye to Skype"
date: "2025-03-06T08:23:23+11:00"
abstract: "The Internet telephony software was universally loved, but I suppose it was on the way out for a while."
year: "2025"
category: Software
tag:
- goodbye
- phones
- skype
location: Sydney
---
By now I'm sure you've read the news that Microsoft is ceasing development of Skype, the ubiquitous Internet telephony software. If this *is* news to you, ugh, I'm sorry! Or not, depending on whether you depend on it for your communications.

I can't express how massive Skype was, especially back in the day. For millions of people like me who were living, studying, and/or working in a different country to their families for years, it was a lifeline back home that was not only affordable, but *feasible*. I have warm, fuzzy feelings about Skype to this day because it let me talk with my mum, in shockingly high quality, over five thousand kilometres away.

*Begone*, you awful international phone calling cards that take 900 digits to use, last five seconds, and had&hellip; latency worse&hellip; sorry no, you go first&hellip; no, it's okay&hellip; all good, I was just going to say&hellip; than a Tesla Autopilot. Damn it, the call dropped again.

Now the *Well Ackchyually* crowd have been quick to point out that modern Microsoft communications software rely on components that, in part, can be traced back to Skype. Therefore, Skype isn't dead! That's true, insofar as it's true that Android is Linux, or that a mouldy sandwich might still be edible in parts. But we're talking about *Teams* here, and anyone claiming therefore that Teams must be as universally loved, admired, and respected as Skype may have eaten one too many mouldy sandwiches.

Admittedly, Skype has been on the way out for a number of years, so the news doesn't come as a shock. Skype was among the first examples I could remember of software that used to be brilliant before it was passed around between companies and morphed into something designed for someone *other* than the users who made it a success. Even the underlying peer-to-peer protocol that made it so technically interesting didn't survive. You'd think it'd be a cautionary tale of how big business squanders good will, but plenty have since followed in their trailblazing footsteps. 

I will fondly remember what Skype did for me, as I'm sure many others will. To the original team, if you're still out there and are somehow reading this post, thank you. I can't tell you how much your software meant to me. Well, I guess I can, because I just did. ♡
