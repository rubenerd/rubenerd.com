---
title: "The One Wi-Fi Network"
date: "2025-02-17T14:44:49+11:00"
abstract: "“Frodo! Don’t wear the ring!”"
year: "2025"
category: Internet
tag:
- lord-of-the-rings
- wi-fi
location: Sydney
---
Saw this at a coffee shop yesterday:

<figure><p><img src="https://rubenerd.com/files/2025/wifi-frodo@2x.png" alt="Frodo! Don't wear the ring!" style="width:346px;" /></p></figure>
