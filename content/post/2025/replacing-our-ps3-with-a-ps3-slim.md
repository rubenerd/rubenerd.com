---
title: "Part three: Replacing our PS3 with a PS3 Slim"
date: "2025-01-11T19:53:55+11:00"
abstract: "It fits! And works great! And nostalgia!"
thumb: "https://rubenerd.com/files/2025/our-ps3-slim@2x.jpg"
year: "2025"
category: Hardware
tag:
- games
- playstation-3
location: Sydney
---
We're into our third and final post in my [PlayStation 3 shaggy-dog story](https://rubenerd.com/tag/playstation-3/). In [part one](https://rubenerd.com/consoles-and-the-playstation-3/ "Consoles and the PlayStation 3") I talked about consoles in general, and in [part 2](https://rubenerd.com/our-recent-history-with-the-ps3/ "Part two: Our recent history with the PS3") I explained why we ended up with a PS3 years after it was old hat.

Last we spoke, our 2012 Super Slim had given up the proverbial ghost, assuming you were playing a haunted game of some sort. Putting the device's hard drive in my workbench PC didn't even yield a detected device, let alone a file system. When I pulled it apart, the smell of burned plastic wasn't encouraging. I suspect the magic smoke was let out.

<figure><p><img src="https://rubenerd.com/files/2025/ps3-slim@1x.jpg" alt="Image of the PS3 Slim, by Evan-Amos on Wikimedia Commons" srcset="https://rubenerd.com/files/2025/ps3-slim@2x.jpg 2x" style="width:500px" /></p></figure>

By this point though, the PlayStation 4 had subsumed most of its light gaming duties, and the Apple TV was now our media centre device. As I said in the last post though, the Apple TV's ergonomics were a far cry from even what the PS3 had. And while I'd managed to get some of the racing games from the PS3 on Steam for my PC, you can't sit on the couch with friends and play that.

Wait hold on, I didn't intend for this post to go in that direction. But that's a key point to stress about consoles and TV-based devices in general that I didn't appreciate until shockingly recently. **Consoles are social devices**, if you want them to be. LAN parties are/were a thing, as is online play. But for an introvert who has a social budget rather a floor, gathering around a console and either watching or playing a game together is a uniquely relaxing way to be social. It's the same with card games, which is a topic for another time.

Anyway, we were setting up our [George Credenza](https://rubenerd.com/remembering-your-hi-fi-and-tv-inputs/ "Remembering our hi-fi and TV inputs") at our new home, when we realised there was a physical and metaphorical gap on one of the shelves. Between the Switch and the PS4 was a space that looked *tailored* for a PS3, as though someone at IKEA had said "these people need to get a PS3, clearly". Neither of our names are Clearly either, though Clara's name gets closer than mine. Wait, does it? Let me check... *R*... *U*... no, I was right. That's a relief.

We agreed we needed a working PS3 in our lives again&mdash;clearly&mdash;so we ventured out to see where we could get one second-hand in time for ourselves for Christmas. Being 2024 at this point though, the PS3 was almost eighteen years old, and the Super Slim revision was at least twelve. Would we even find them?

We went to our local CEX, and found they were flush with a dozen PS3 Slim models. This was the first revision of the PS3, which was first released in 2009. It also had a revised motherboard and internal components that reduced heat and power draw, though it retained the original PS3's front-loading CD mechanism. This would make it perfect for the space in the George Credenza, where the top-mounted drive in the Super Slim would be feasible, but awkward.

We picked out the one in best cosmetic and working condition, paid less than we spent on groceries that day, and connected her up. Here she is!

<figure><p><img src="https://rubenerd.com/files/2025/our-ps3-slim@1x.jpg" alt="Photo of our PS3 Slim in the George Credenza" srcset="https://rubenerd.com/files/2025/our-ps3-slim@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

Wow, that's not a great photo. It also demonstrates our temporary lack of cable management. But hey, PS3! Also, check out how we used one of those soft IKEA KALLAX inserts as a box for controllers and associated components, so we can charge them while still having them put away. You can't spell *genius* without *me!* Wait, damn it.

While it does have minimal surface wear and looks great overall (or in pants, I suppose?) it still does get a bit warm. One weekend when I'm less busy I think I'll still do a thorough clean, and maybe scrape off the fifteen-year old thermal paste and replace it on the cell CPU and RSX chips. Yay, PowerPC! I've also swapped out the SSD, which hasn't had much of an impact on performance, but makes me feel a bit better after the Super Slim's drive fizzled out.

The PS4 will probably continue to be the central device in our lounge now, but I love the fact that we have access to all those older games again. And I think we may end up playing DVDs and Blu-rays on the PS3 again, and not just because we were able to restore the old *Hyperdimension Neptunia* theme on it for early 2010s weebism.

Uh oh, I'm sensing a part four already to talk about the specific games and memories, and *another* for the SSD replacement process. That breaks the whole "three parts for a PS3" justification I had before. My bad.

*(PS3 Slim photo by [Evan-Amos on Wikimedia Commons](https://commons.wikimedia.aorg/wiki/File:Sony-PlayStation-3-2001A-wController-L.jpg). Photo of the PS3 in our George Credenza by me)!*
