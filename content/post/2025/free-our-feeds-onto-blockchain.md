---
title: "Free Our Feeds… onto blockchain"
date: "2025-03-03T09:33:45+11:00"
abstract: "That fishy Free Our Feeds site? Yeah, it was nonsense!"
year: "2025"
category: Internet
tag:
- social-networking
- spam
location: Sydney
---
Rememeber that [Free Our Feeds](https://rubenerd.com/free-our-feeds/) campaign to make Blusky and its protocols more open? How it seemed like something was a bit&hellip; off about it, despite all the regular news outlets obsessing over the idea?

From a [project](https://en.wikipedia.org/wiki/Ascute "Wikipedia article on ascute") that has since partnered with them:

> Ascute is a genus of calcareous sponges. It contains two species, both found in Australia.

That's clearly the wrong article. [Let's try again](https://archive.is/rJiwx "Blockchain guff site, archived"):

> This partnership marks a milestone in decentralized social networking, ensuring that access to the “firehose” of AT protocol data is available to a broad ecosystem of developers and users, independent of any single controller or corporation.

Huh! How will they do that? Say it with me now:

> By integrating with [&hellip;]—a public, permissionless blockchain developed by [&hellip;]

**AAAAA** there it is! I *knew* it was going to be blockchain, or genAI, or slop images of tulips on a blockchain. Their site is a *Greatest Hits* of all the baseless guff from the blockchain hype years, stuff that would be ineffectual even if all of it were technically possible.

*(An earlier version of this post had blockchain written as "blockchin", which gave me instant flashbacks to that board game "Guess Who". I might need to buy that again. Does your person have a block chin)?*

This sort of thing still amuses me after all these years. But I did hold out a sliver of hope that maybe&mdash;just maybe&mdash;this fishy site was something legitimate or worthwhile after all. Sponges. *Ascute* sponges.
