---
title: "Solving the Moving Sofa Problem"
date: "2025-02-06T18:22:05+11:00"
abstract: "This made me grin AND think. Which is the best kind of thinking."
year: "2025"
category: Thoughts
tag:
- mathematics
- science
location: Sydney
---
We've all encountered that moment, faced with a corner or a set of stairs, and wondering *how in the hell* we're going to fit our couch, fridge, or 42RU server rack around it.

[Jac Murtagh over at the Scientific American](https://www.scientificamerican.com/article/mathematicians-solve-infamous-moving-sofa-problem/) has some great news! The maximum size of an object that can fit around a corner is now knowable:

> What’s the largest couch that can turn a corner? After 58 years, we finally know. The problem sat unsolved for nearly 60 years until November, when Jineon Baek, a postdoc at Yonsei University in Seoul, posted a paper online claiming to resolve it. Baek’s proof has yet to undergo thorough peer review, but initial passes from mathematicians who know Baek and the moving sofa problem seem optimistic.

I love **every single part** of this article, including the helpful animated graphics. This was my favourite paragraph:

> Solving the moving sofa problem requires that you not only optimize the size of a shape, but also the path that shape traverses. The setup permits two types of motion: sliding and rotating. The square couch only slid, whereas the semicircle slid, then rotated around the bend, and then slid again on the other side. But objects can slide and rotate at the same time. Mathematician Dan Romik of the University of California, Davis, has noted that a solution to the problem should optimize both types of motion simultaneously.

Give it a read if you need a break from doom scrolling. It made me grin *and* think, which is the best kind of thinking.
