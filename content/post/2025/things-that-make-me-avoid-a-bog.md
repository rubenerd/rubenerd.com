---
title: "Things that make me avoid a blog"
date: "2025-01-18T14:49:57+11:00"
abstract: "Slop, autoplaying videos, popups, techbro vibes, and Substack, basically!"
year: "2025"
category: Internet
tag:
- design
- respect
location: Sydney
---
*This post was originally typo'd with "bog" instead of "blog". As that [Oblique Strategy](https://www.enoshop.co.uk/product/oblique-strategies) says, honour thy error as a hidden intention.*

I've written before about the [things I look for in a blog](https://rubenerd.com/what-i-look-for-in-blogs/ "What I look for in blogs"). This time I thought I'd flip this on its head&hellip; an exercise that used to be much easier when I was into gymnastics.

This is what will make me bounce instantly:

* GenAI slop images and text, for philosophical reasons and matters of taste.

* Popups or autoplaying videos. I'm sure your newsletter is great, but I'll leave the instant you pester me about it.

* Edgelord or techbro vibes, especially those who punch down, blame the powerless, uses specific terms unironically as pejoratives, or otherwise lack self-awareness. In the words of Merlin Mann, I don't put trash in my vegetable crisper.

* If you try and guilt-trip me for using an [FBI recommended security tool](https://www.tomsguide.com/news/the-fbi-now-recommends-using-an-ad-blocker-heres-why) with a popup from the likes of Admiral.

* Any post on the first page that advocates for blockchain, scientific or medical denialism, or Oreos. One of those was fake, though I'll still judge you secretly for it.

* If it's hosted on Substack, or similar. I get they're trying to make it easier for writers to get a following and earn income, but I find their policies and editorial positions... dubious.

These won't make me bounce individually, but satisfy enough of them and I might:

* If the blog contains insufficient Internets. Blogs should have at least one Internets. I don't make the rules.

* If you have one of those so-called "EU Cookie Notice" popups. The issue here is that you're admitting to collecting data you shouldn't, not that you're being forced to admit it. Some people enable these in their CMS without thinking about the ethical and legal concerns.

* If you need JavaScript to load text on your page, and/or if tools like NoScript or UBlock Origin breaks your site in fundamental ways. Progressive enhancement, graceful degradation, I don't mind. An RSS feed with full text posts somewhat negates this.

The good news is, the *vast* majority of personal blogs today don't do these. This isn't a coincidence; it follows that people who love maintaining their own sites also love the Web.
