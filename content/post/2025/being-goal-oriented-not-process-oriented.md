---
title: "Being goal oriented, not process oriented"
date: "2025-02-05T08:17:51+11:00"
abstract: "“You can always build more, but people won't use the thing if it sucks.”"
year: "2025"
category: Software
tag:
- infocomm
- transit
- urbanism
location: Sydney
---
Yesterday I wrote how people generally don't care how something works, they only [want a solution](https://rubenerd.com/people-want-solutions/
) to their IT problems. Jason of NotJustBikes was joined by Reece from RM Transit to [discuss a similar idea](https://www.youtube.com/watch?v=jgc_c2E4nFk). Edited for brevity:

> **Jason:** In the Netherlands, the planning seems to be a lot more high level and strategic. "This is the overarching goal that we have", and here are the pieces that make that possible. It's not common to say "we're going to build a bike project for this many lanes per year". Instead, it's "we want them to make it safer for kids to go to school on their own". So then it becomes "how do we do that?" It's not just bike lanes, it's different crossings. Those will come in the "make it safer for kids to get to school" project, not a "transit" project.
>
> **Reece:** It's process oriented, not goal oriented. I think [the reverse] is a much better metric to measure success. You can always build more, but people won't use the thing if it sucks.

I'm tempted to print that last line on the back of a business card.
