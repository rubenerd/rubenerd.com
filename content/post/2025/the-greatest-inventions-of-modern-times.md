---
title: "Refrigeration: my greatest invention of modern times"
date: "2025-01-03T10:26:17+11:00"
abstract: "Responding to my friend Michał Sapka who asked about washing machines."
year: "2025"
category: Hardware
tag:
- air-conditioning
- life
location: Sydney
---
I read that title back and realised it implies that refrigeration was **my** invention. This may surprise some of you, but I cannot take credit for inventing refrigeration. Well, I *could*, but that'd leave you... what's a pun related to air conditoning? Frosty?

...

[Michał Sapka](https://crys.site/) ran a poll asking what the [greatest invention](https://mastodon.bsd.cafe/@mms/113759992380746659) of modern times was. Out of *washing machine* and *dishwasher* I chose the former, though it does amuse me how silly English is with these terms. Why not a *clotheswasher* and a *dishwasher*? Surely a *washing machine* would be able to handle both? I do like having a dish washer, but not washing clothes by hand is an absolute wonder.

It wasn't an item on his list, but I'm staking my claim on refrigeration being the greatest invention in modern times. Transfer heat from one place, and put it somewhere else. Throughout history we've known how to heat instantly on demand, but cool instantly on demand? That was either imperfect, slow, or difficult, unless you had favourable geography.

Refrigeration gave us the residential and industral fridge, which has likely saved millions of lives from spoiled food. It's allowed people to eat more healthy fresh and frozen produce, even if they don't live in an area where growth of that food is feasible. Ice boxes helped here, but fridges made food preservation ubiquitous and easy. Fridges are also invaluable in medical settings for transplants, preserving samples, and delivering medicine.

Refrigeration also gives us the modern reverse-cycle air conditioner. These units make living in certain parts of the world comfortable even in summer, and use far less power than conventional resistive electric heaters in winter. Clara and I each had a few non-negotiables when buying an apartment, and air conditioning was one of mine. [Sydney has a mild climate](https://en.wikipedia.org/wiki/Climate_of_Sydney "Climate of Sydney article on Wikipedia") most of the year, though December-February I'd say an air conditioner is mandatory.

More recently, refrigeration has also lead to the proliferation of closed-cycle clothes dryers that use the dehumidifying and heating properties of reverse-cycle air conditioning to dry fabric. They take longer than regular clothes dryers, and I'm mostly partial to air drying on a rack, but for bulky items like sheets they're *wonderful*. They also don't turn your laundry into a humid mess.

The nomenclature of air conditioning is also interesting to me. Most text ads refer to it as *air conditioning*, though in Singapore I mostly heard it referred to as *aircon*. In Australia, you're more likely to hear and see *A/C*. In the US, I hear people refer to *heat pumps* for reverse-cycle systems. It's intersting, because we've all come to accept *fridge* as the universal truncation of *refrigerator*.

One thing I won't be abbreviating is my thank you to Willis Carrier :).
