---
title: "It’s not a bicycle"
date: "2025-01-23T08:47:30+11:00"
abstract: "A morning squeak was a garage door instead. Illusion shattered!"
year: "2025"
category: Thoughts
tag:
- mornings
location: Sydney
---
You may have read my early morning routine here by now: I get up, shower, make coffee, sit on the balcony, and check my RSS feeds, email, and calendar for the day. Feeling the outdoor air, seeing the trees, and hearing the sounds of nature mentally prepare me in a way nothing else does. Clara was gracious enough to let us spend more time looking for an apartment that specifically had a view of the nature reserve, and with enough semi-enclosed space to put some patio furniture.

*But I digress*. Something that punctures the morning air every day is a faint squeak, as though a bicycle hasn't been lubricated in a while. But when I was resting against the handrail of the balcony with coffee this morning&mdash;like a gentleman&mdash;I saw a rolling garage door open and close. And wouldn't you know it, *it made that exact sound*.

Thing was, I had this vivid image of what that bicycle looked like. I had it in my head that someone was leaving their house, stowing their backpack in the basket on the front, mounting the bike for their commute, kicking off, and squeaking a bit as they rode down the lane outside the balcony. 

Having that illusion shattered affected me more this morning than I would have expected. *It's not a bicycle!* It's like someone I got to know over the last month has left. Except, they were never there to start. Huh.
