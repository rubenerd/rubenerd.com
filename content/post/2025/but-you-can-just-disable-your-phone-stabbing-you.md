---
title: "But you can just disable your phone stabbing you!"
date: "2025-02-08T08:49:35+11:00"
abstract: "To disable it, I had to go into that locked filing cabinet behind the “Beware of the Leopard” sign again."
year: "2025"
category: Software
tag:
- accessibility
- design
- privacy
location: Sydney
---
I've shared this example on Mastodon before, but imagine our phones came with a new stabby feature that stabs you whenever you miss a notification. Most people would look at that and say "that's dreadful"! But you'll always get some deliberately-obtuse sealion retorting with "well duh, you can disable it, I don't see the problem".

Yes, it's a ridiculous example inflated for dramatic effect. But this happens *constantly*. I really feel for non-technical people who have to navigate this increasingly hostile digital world, let alone being subjected to comments from people who pretend not to understand our reservations about a feature.

It reminds me of a [certain galactic hitchhiker](https://www.goodreads.com/quotes/40705-but-the-plans-were-on-display-on-display-i-eventually):

> "But the plans were on display…”
> 
> “On display? I eventually had to go down to the cellar to find them.”
>
> “That’s the display department.”
>
> “With a flashlight.”
>
> “Ah, well, the lights had probably gone.”
>
> “So had the stairs.”
>
> “But look, you found the notice, didn’t you?”
>
> “Yes,” said Arthur, “yes I did. It was on display in the bottom of a locked filing cabinet stuck in a disused lavatory with a sign on the door saying ‘Beware of the Leopard.” 

The [latest BSD Now!](https://www.bsdnow.tv/597) also reminded me of the [Do-Not-Stab HTML header](https://www.5snb.club/posts/2023/do-not-stab/) as well, which satirises online tracking and surveillance. The assumption that people wish to enter an adversarial relationship unless they opt-out really has to be nipped in the bud.

