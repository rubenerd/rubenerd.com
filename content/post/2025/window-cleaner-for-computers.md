---
title: "Window cleaner for computers"
date: "2025-03-01T11:31:52+11:00"
abstract: "Turns out Windex works great."
year: "2025"
category: Hardware
tag:
- cleaning
location: Sydney
---
*I've been a bit under the weather the last few days, so I'm posting from the drafts folder from late last year. I don't remember why I didn't publish this. Maybe I was waiting to take a photo of a cleaned laptop, but that would just look like a normal laptop, so that doesn't make sense. I should probably stop writing this paragraph before it becomes longer than the post itself.*

There was a range of PC utilities in the 1990s in a similar vein to The Norton Utilities, but they had names like *Cleaner* and *Scrubber*. I can't remember specifics; I might to go trawling through my old CD-ROMs to find examples. Fortunately for this specific post, this trip down memory lane was *entirely* irrelevant.

For years I've used IPA to clean monitors, keyboards, trackpads, and laptop surfaces. But a friend of mine clued me into the possibility that IPA isn't great for rubber and plastics, and that regular old window cleaner is milder and does the trick just as well.

I'm not sure how true the former is, but the latter *definitely* is. I used some Windex (sorry Aldi, your generic brand cleaner is only middling for once) on my work MacBook Air case and display, and it worked *shockingly* well. The display wasn't a surprise, given this sort of thing is designed to clean windows and glass. But the case and keyboard? Huh! I applied it, wiped it off, and it looks good as new.

I pass this on for your consideration. If you need to lightly clean some computers, give window cleaner a try instead of something harsher. Something something I don't even run Windows.
