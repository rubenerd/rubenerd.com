---
title: "The elusive desoldering station"
date: "2025-02-23T07:11:10+11:00"
abstract: "Tossing up between a clone or a Hakko, but neither seem to be available in Australia."
year: "2025"
category: Hardware
tag:
- electronics
- homelab
location: Sydney
---
I've reached that point in my amateur electronics experience where I want to buy a desoldering gun, or a desoldering station. Solder wick, a sufficiently hot iron, and a solder sucker is serviceable for small jobs, but when faced with the prospect of desoldering 64 pins for an *entire bank* of Commodore 128 memory, I think we both deserve better.

Plenty of electronics enthusiasts in blogs and video use these tools, so I made the mistake of thinking they must be ubiquitous. This doesn't seem to be the case, at least in Australia. Our local electronics stores don't stock them, and the shop keepers give you strange looks when you ask. A cursory search of their web portals for "desoldering" only returns... soldering irons.

The solution therefore appears to be the same it usually is now: buy online. But again, where I thought it'd be easy to just search for an Australian distributor, it doesn't look to be the case. I'll likely have to import one, which then raises the issue that most aren't sold for 230 V, if they're even available for shipping for less than the price of the unit itself!

<figure><p><img src="https://rubenerd.com/files/2025/duratool@1x.jpg" alt="The Duratool D000672" srcset="https://rubenerd.com/files/2025/duratool@2x.jpg 2x" style="width:480px;" /></p></figure>

There seems to be three options:

* A clone of an early Hakko desktop station, like the **Duratool D00672/ZD-915** or the **Pro's Kit SS331/ZF-8915**. They reportedly clog easily, and the plastic construction of the latter raises eyebrows, but they can be bought for less than AU $250 shipped. Maybe it's a case of *you get what you pay for*.

* An official portable **Hakko FR-301** desoldering gun, which includes the pump in the handle. It looks a bit unwieldy to me, but I've read and watch people swear by them. Hakko will undoubtedly have better build quality, if I can find a 230 V model for less than a small fortune.

* A **Hakko FR-410** or equivalent desktop desoldering station, which looks amazing but is well beyond my price range. I include it here purely for aspirational purposes.

[Jan Beta improved his ZD-915](https://www.youtube.com/watch?v=26VEOadIn1M) with a better DC-DC power converter, fan, and fan grille, and there's a decent modding community for it. For my budget and limited use case I might consider it.

If anyone in Australia or New Zealand has any recommendations for local distributors, let me know.
