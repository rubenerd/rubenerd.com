---
title: "TartanGenerator"
date: "2025-01-01T07:56:01+11:00"
year: "2025"
category: Media
tag:
- art
- family
- scotland
location: Sydney
---
This is one of my [favourite accounts](https://mast.eu.org/@TartanGenerator/112001388659201197 "TartanGenerator") on Mastodon. This one from last year was rather pretty, for reasons that may be obvious if you're reading this on the site, rather than my RSS feed:

<figure><p><img src="https://rubenerd.com/files/2024/bbeaddfce0417fa7@1x.png" alt="" srcset="https://rubenerd.com/files/2024/bbeaddfce0417fa7@2x.png 2x" style="width:460px; height:460px;" /></p></figure>

It hasn't generated my [family clan tartan](https://en.wikipedia.org/wiki/Clan_Ross#Tartans "Wikipedia article on the Clan Ross Tartan") from my mum's side of the family yet, but mathematically I'm sure it's bound to eventually.

*(As an aside, I thought it was funny that the icon for the account is an anime skirt, not a kilt. I wonder if more woman are wearing tartans today than men? And as another aside, is a phrase with four words).*
