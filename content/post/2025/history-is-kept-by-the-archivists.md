---
title: "History is kept by the archivists"
date: "2025-01-22T19:36:26+11:00"
abstract: "I saw this in response to a comment that history is written by the victors."
year: "2025"
category: Thoughts
tag:
- archiving
- facts
location: Sydney
---
I saw the title of this post fly by as a comment on social media, I think in response to someone saying *history is written by the victors.* As a voracious&mdash;almost certainly excessive&mdash;archivist myself, yay!

At least, I think yay? Perhaps the quote is suggesting that archivists are merely pawns perpetuating what the victor wrote. Hmm.

I couldn’t find the original source of that quote, though [Jenny Sinclair wrote this](https://www.smh.com.au/culture/books/archives-are-only-what-the-victors-have-left-behind-20200618-p553z0.html "Archives are only what the victors have left behind") for the Sydney Morning Herald in 2020:

> Winston Churchill said history is written by the victors. I’m more interested in Shakespeare’s take: "Truth will out." In the world of the historical novel, the archives are only what the victors have left behind. There’s still time, these stories suggest, to write a different story.
