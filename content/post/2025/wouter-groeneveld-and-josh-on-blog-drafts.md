---
title: "Wouter Groeneveld and Josh on blog drafts"
date: "2025-02-02T08:09:48+11:00"
abstract: "Wouter says I believe in drafts, which I think gives me too much credit."
year: "2025"
category: Internet
tag:
- weblog
- writing
location: Sydney
---
Wouter recently did one of those [blog question posts](https://brainbaking.com/post/2025/01/fine-ill-answer-your-blog-questions/) that are so popular thesedays. While he claimed not to find them especially interesting to read, I found a lot of nuggets in his that made it worth my time.

Something that resonated with me was his perspective on database-driven CMSs and static-site generators:

> If you want to write often, you want to reduce friction, and for me, seeing the files and just having them on your drive is more important.

I hadn't thought of it that way before. I do miss the ability to post from anywhere, not just where I have the repo for this blog checked out. Think phones on a train, for example (much better than snakes on a plane). But the experience of firing up a text editor to write locally and push is certainly a better experience for me, and I do like seeing posts sorted by year on a hard drive. Or SSD, or whatever.

He also mentioned this about publishing immediately versus letting a post simmer, emphasis added:

> Austin Kleon would say: Push Your Work! It’ll be online within the hour. I usually re-read it in my news feed reader and correct stupid mistakes afterwards. **Unlike Ruben, I don’t believe in drafts**. I try to keep a steady pace and just write one every four days or so.

This may be a reference to my recent post about [my drafts folder](https://rubenerd.com/the-current-state-of-my-drafts-folder/), though I've mentioned it other times here over the years. I write a lot of drafts, some of which even get published. Occasionally.

I think Wouter gives me more credit than I deserve here by saying I *believe* in drafts. If anything, it's as though drafts believe in me. I don't start a post intending for it to become a draft, it just happens. I'll start writing something based on a title and a loose idea, and it either goes somewhere or it doesn't. *Why* it doesn't could be down to many factors:

* My thoughts weren't as cogent, cohesive, or fleshed out as I thought they were, which only becomes apparent when I attempt to articulate them in a way that makes sense. As Wouter would say, a half-baked idea is even worse than half-baked bread.

* My opinion on something changes, either from doing more research, or from the natural process of writing uncovering a flawed assumption. The world has enough bad takes and misinformation, and I don't want to contribute to it!

* I'm not in the correct mood or mindset. Technical writing can be done irrespective, but I like my posts to have a bit of personality. I wear my emotions on my sleeve, and I can tell when I'm not "feeling" it.

* I hit a massive roadblock of imposter syndrome, or I fear that something I'm writing is bad, and therefore I should also feel bad. Most of the time I can overcome this by remembering that it's literally how I feel about *everything* I write. But as per above, sometimes the mental space isn't there.

* I've finished writing something, but it's missing some specific detail, image, or other material that I don't have readily available at the time. This doesn't happen as often as the other reasons above.

My drafts folder therefore becomes a place where half-baked ideas go. Sometimes I've revive these days, weeks, years, or decades later, either with the knowledge or confidence I was missing before. But I don't deliberately start a post expecting it to be a draft.

This leads me to a question [Josh from The Geekorium](https://the.geekorium.au) asked me recently about *where* my drafts folder is. He's right, I have it in a separate repo from my blog posts. Maybe that's a topic for another time, but in short I have a repo with my KeePassXC store, drafts, my standard library of shell and Perl scripts, browser backups, and so on. It's not that I'm embarrassed by anything in my drafts folder, but I'd rather only have finished thoughts out in the world.

My high school English teacher Ms Gravina&mdash;who was amazing&mdash;once remarked she thought it was "impressive" that my school folder was so massive. I admitted that my brain worked in this weird way where I had all these loose and incomplete thoughts, which I'd write on whatever I had available at the time, and would stuff into the folder. I remember her smiling and saying I should keep at it, because it was a sign that I cared. To this day I'm not entirely sure what she meant by that, but it's why the drafts folder is actually my `./gravina` folder :).

So to correct the record, I don't necessarily believe in drafts either. It's where stuff goes that I would rather see published, if it weren't lacking in one or more ways. Because I'm with Wouter here: get your ideas out there into the world. 
