---
title: "Export PDFs, don’t print them"
date: "2025-01-01T16:21:59+11:00"
abstract: "Exporting retains the metadata, structure, and accessibility features of the document"
year: "2025"
category: Software
tag:
- accessibility
- documents
location: Sydney
---
I've been doing something wrong for a while, and thought it worth bringing to everyone's attention.

When you're preparing a document for PDF to share, remember to use your application's Export feature, and *not* Print to PDF. Exporting retains the metadata, structure, and accessibility features that Print to PDF strips out, which hampers screen readers among other tools.

I can't remember if I first saw this suggestion in my RSS reader or Mastodon, but I thought it was too important not to pass on.

