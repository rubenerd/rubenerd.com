---
title: "Spraining a finger"
date: "2025-01-02T18:17:13+11:00"
abstract: "Celebrating a random injury not being worse with some bubble tea."
thumb: "https://rubenerd.com/files/2025/happy-lemon-friend@2x.jpg"
year: "2025"
category: Thoughts
tag:
- health
- personal
location: Sydney
---
2025 has been going *exceptionally* well. Except for the part where I somehow sprained my left index finger without knowing it, until this morning where it was so swolen and painful I couldn't move it. *How does this happen!?*

I went to the GP who referred me to get an X-ray and ultrasound. There was no major damage which was great news, but the ultrasound showed swelling around the muscles. I've been given the usual instructions to keep it elevated, cool, and have it bandaged with my middle finger to stop it moving, along with some anti-inflammatories.

Because I live in what one libertarian American reader said is a "sosialist [sic] hellscape", the visit and all the tests were covered under Medicare. I know right, the absolute worst (for the benefit of that reader, that's referred to as *sarcasm*. But this sentence isn't sarcasm. Yes).

<figure><p><img src="https://rubenerd.com/files/2025/happy-lemon-friend@1x.jpg" alt="" srcset="https://rubenerd.com/files/2025/happy-lemon-friend@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

Anyway! Clara and I celebrated it not being as serious as we thought with some Happy Lemon, who gave us a rugged-up little friend as a bonus :).
