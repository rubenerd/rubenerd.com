---
title: "Bad Apple!! But it’s a fluid simulation"
date: "2025-01-11T09:17:24+11:00"
abstract: "One day I’ll stop being taken aback by how far this meme has gone."
thumb: "https://rubenerd.com/files/2025/yt-2Ni13dnAbSA@2x.jpg"
year: "2025"
category: Anime
tag:
- bad-apple
- memes
- science
location: Sydney
---
A colleague shared [this Sebastian Lague video](https://www.youtube.com/watch?v=2Ni13dnAbSA) yesterday. One day I’ll stop being taken aback by how far this meme has gone, and the lengths people will go to recreate it. Amazing :).

<figure><p><a target="_BLANK" href="https://www.youtube.com/watch?v=2Ni13dnAbSA" title="Play Bad Apple but it's a Fluid Simulation"><img src="https://rubenerd.com/files/2025/yt-2Ni13dnAbSA@1x.jpg" srcset="https://rubenerd.com/files/2025/yt-2Ni13dnAbSA@1x.jpg 1x, https://rubenerd.com/files/2025/yt-2Ni13dnAbSA@2x.jpg 2x" alt="Play Bad Apple but it's a Fluid Simulation" style="width:500px;height:281px;" /></a></p></figure>
