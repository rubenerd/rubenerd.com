---
title: "Goodbye to our Kindles"
date: "2025-02-27T14:37:07+11:00"
abstract: "Disabling download and transfer was the last proverbial straw."
thumb: "https://rubenerd.com/files/2025/kindle-recommendations@1x.jpg"
year: "2025"
category: Media
tag:
- books
- drm
- kindle
- reading
location: Sydney
---
Clara and I [bought Kindles years ago](https://rubenerd.com/my-kindle/) to use as affordable ebook readers. I used the [Calibre](https://calibre-ebook.com/) software to convert my epub books to the mobi files that the Kindle required, and I was off to the races. If by "races" I mean sitting on a train seat or couch and reading a book.

Over time though, we did buy a few books from the Kindle store. Well, "buy", we'll come back to that. We immediately saw the appeal of being able to have books delivered automatically to our devices. You could say this drew us in. And the proof was in the pudding; we read more than we did before. It was fun!

But as anyone familar with DRM and locked stores will attest, there's usually no such thing as "ownership" on these platforms. Amazon recently disabled their [download and transfer](https://arstechnica.com/gadgets/2025/02/psa-amazon-kills-download-transfer-via-usb-option-for-kindles-this-week/) feature, as Ars Technica reported:

> Amazon [closed] a small loophole that allowed purchasers of Kindle books to download those files to a computer and transfer them via USB. Originally intended to extend e-book access to owners of very old Kindles without Wi-Fi connectivity, the feature has also made it easier for people to download and store copies of the e-books they've bought, reducing the risk that Amazon might make changes to their text or remove them from the Kindle store entirely.

I've talked about this before in the context of streaming platforms. Only being able to download and/or stream services from one place, and play/read them on one device, means they control your access indefinitely. It's wild to me this is accepted practice; imagine a record label or a book publisher coming to your house to take back something you bought. Sure, we own the physical medium in which the words, pictures, and audio are contained, but by that token you could argue we own the Kindle that contains those words too.

Anyway, *nuts to that* as they say! Clara and I hadn't used our Amazon accounts for a while now, but we've closed them for good. This does sting for authors we love who've signed exclusivity deals, and we do emphatise with their position, but we can't continue supporting the platform.

I did get a screenshot of my account recommendations page though, which was a snapshot into my tastes a decade ago! Lisp, geography, and Suzimiya Haruhi... this checks out.

<figure><p><img src="https://rubenerd.com/files/2025/kindle-recommendations@1x.jpg" alt="A wall of book recommendations." srcset="https://rubenerd.com/files/2025/kindle-recommendations@2x.jpg 2x" style="width:500px; height:315px;" /></p></figure>

I do push back somewhat on the fatalists who say this is the inevitable outcome of such platforms. It's only inevitable because they're allowed to get away with it. Just as there was nothing inevitable about blowing past climate targets to power genAI slop in lieu of tech that's improving lives, there's still room for a company to do the right thing here. *Surprise us!*
