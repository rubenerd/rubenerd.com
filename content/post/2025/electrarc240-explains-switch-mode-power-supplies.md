---
title: "Electrarc240 explains switch mode power supplies"
date: "2025-02-20T09:39:10+11:00"
abstract: "Teachers that can explain abstract concepts using relatable analogies are worth their weight in linear power supplies."
year: "2025"
category: Hardware
tag:
- electronics
- video
location: Sydney
---
This was such a fun video! Teachers that can explain abstract or difficult concepts using relatable analogies are worth their weight in linear power supplies.

<figure><p><a target="_BLANK" href="https://www.youtube.com/watch?v=3FyXqNcqvRM" title="Play Every Component of a Switch Mode Power Supply Explained"><img src="https://rubenerd.com/files/2025/yt-3FyXqNcqvRM@1x.jpg" srcset="https://rubenerd.com/files/2025/yt-3FyXqNcqvRM@1x.jpg 1x, https://rubenerd.com/files/2025/yt-3FyXqNcqvRM@2x.jpg 2x" alt="Play Every Component of a Switch Mode Power Supply Explained" style="width:500px;height:281px;" /></a></p></figure>

