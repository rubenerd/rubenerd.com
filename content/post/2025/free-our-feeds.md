---
title: "“Free Our Feeds”"
date: "2025-01-16T11:00:58+11:00"
abstract: "This new crowdfunding site is deeply weird. And no mention of ActivityPub?"
year: "2025"
category: Internet
tag:
- activitypub
- mastodon
- social-media
location: Sydney
---
Speaking of social media and feeds, a [new site launched](https://web.archive.org/web/20250114001705/https://freeourfeeds.com/ "Free Our Feeds, via the Internet Archive") that's honestly one of the more surreal things I've ever seen:

> Bluesky is an opportunity to shake up the status quo. They have built scaffolding for a new kind of social web. One where we all have more say, choice and control. But it will take independent funding and governance to turn Bluesky’s underlying tech—the AT Protocol—into something more powerful than a single app. We want to create an entire ecosystem of interconnected apps and different companies that have people’s interests at heart. Free Our Feeds will build a new, independent foundation to help make that happen.

Bluesky can do things that Mastodon can't. I've been told their moderation tools are far superior for example, though that wouldn't be hard. But still, the whole site feels like something a person knocked up with a genAI tool one afternoon. It has that... uncanny valley vibe that's hard to describe.

I have so many questions, but these are the top two:

* Why is there no mention of ActivityPub, or Mastodon, at all? You know, the protocol that isn't tied to one app? At best, this reads like not-invented-here syndrome. At worst, it's obfuscation.

* Why can't Bluesky use some of their millions from investor funding to do this? Wasn't that the whole point of "decentralisation", and the creation of the Bluesky Public Benefit Corp? Or put another way, what were people investing into Bluesky for, if not for *exactly* this?

Okay, those were more than two questions. But [I agree with Tante](https://tante.cc/2025/01/13/but-does-it-free-our-feeds/), there's something *deeply* fishy about this whole thing. And not the good kind of fishy that you could post a photo of using [Pixelfed](https://pixelfed.org/), or any other number of ActivityPub-compliant applications.

It does make me wonder if I'm going about donations here wrong though. Maybe I need to launch a donation site called *Free Our Rubenerds*, in which I talk about how Rubenerd.com needs to have independent funding to create something truly powerful. A nuanced, disruptive paradigm synergy, one could say.
