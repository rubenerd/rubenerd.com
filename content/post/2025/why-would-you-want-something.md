---
title: "Why would you want something?"
date: "2025-01-14T08:58:21+11:00"
abstract: "“I wanted an SACD player, because I wanted an SACD player.”"
year: "2025"
category: Thoughts
tag:
- feedback
location: Sydney
---
I get a lot of comments asking this. As always, [Techmoan](https://www.youtube.com/watch?v=YRySrXk9uRA "The tale of a faulty SACD player and the equally faulty repair attempt") has the answer.

<figure><p><img src="https://rubenerd.com/files/2025/techmoan-sacd-1@1x.jpg" alt="" srcset="https://rubenerd.com/files/2025/techmoan-sacd-1@2x.jpg 2x" style="width:500px;" /><br /><img src="https://rubenerd.com/files/2025/techmoan-sacd-2@1x.jpg" alt="" srcset="https://rubenerd.com/files/2025/techmoan-sacd-2@2x.jpg 2x" style="width:500px;" /></p></figure>



 
