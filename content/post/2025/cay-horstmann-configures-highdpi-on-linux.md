---
title: "Cay Horstmann configures HiDPI on Linux"
date: "2025-01-08T10:22:28+11:00"
abstract: "“The fonts were amazingly crisp.”"
year: "2025"
category: Hardware
tag:
- bsd
- accessibility
- ergonomics
- guides
- linux
- pc-screen-syndrome
location: Sydney
---
Almost exactly two years ago&mdash;huh!&mdash;Cay Horstman [configured his Linux machine for 2× HiDPI](https://horstmann.com/unblog/2023-01-07/index.html), including some tweaks I was unaware of. He also summarised why you'd want to do this:

> At first I didn't care because my external monitor didn't change, and just set the resolution to 1920x1200. But when I traveled over Christmas, I got curious. Would it be nicer at full resolution?
>
> Yes, it was. The fonts were amazingly crisp.

*This!* But that won't stop people saying they either don't need 2× HiDPI, can't tell the difference, or think fractional scaling is fine. I guess some people like spinters; it makes them feel alive.

He also mentions `swisswatch(1)`, which I had long forgotten about. It's in [pkgsrc](https://pkgsrc.se/time/swisswatch)!
