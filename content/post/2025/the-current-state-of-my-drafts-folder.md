---
title: "The current state of my drafts folder"
date: "2025-01-18T10:10:34+11:00"
abstract: "Don’t worry about having lots of drafts! But also, silcence that inner critic. They’re a jerk."
year: "2025"
category: Internet
tag:
- drafts
- weblog
location: Sydney
---
Surely it can't be that bad:

    $ find ./drafts/ -type f | wc -l
    ==> 11987

*Womp womp!*

Admittedlly, that folder is *also* now [twenty years old](https://rubenerd.com/this-site-turns-20-today/ "This site turns 20 today"). Most of those drafts are also just a title or a topic sentence. Some are full-blown posts, with images, diagrams, and regret, while others are simply not timely anymore, are perhaps NSFW/blue in some way, or I decided against posting them. Oh well.

I bring this up, becuase a few people on Masto and email have admitted they often struggle to get their thoughts out of drafts, implying that people like me have their acts together in such regards. **Not at all!**

It may not look like it, but I still flinch slightly whenever I run my upload script on a batch of posts for the day. Did they say everything I meant to? How many spelling, punctuation, and grammar mistakes did I include this time? Will people get angry at them? Did I provide sufficient context or detail? Am I going to be mentioned in a newspaper, or linked to in an aggregator again, with the resulting schmidtstorm of feedback from people who don't know me, have never read me before, yet feel a compulsion to provide what could *charitably* be described as "feedback"?

Silencing your inner critic&mdash;who's a jerk&mdash;is hard. But we also want to read what you have to say.
