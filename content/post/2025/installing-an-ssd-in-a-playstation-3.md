---
title: "Part four: Installing an SSD in our PlayStation 3 Slim"
date: "2025-01-13T15:10:46+11:00"
abstract: "The PS3 is ripe for an SSD conversion, and with the system software from the Sony site, is easy(ish) to do."
thumb: "https://rubenerd.com/files/2025/ps3-load@2x.jpg"
year: "2025"
category: Hardware
tag:
- games
- playstation-3
- storage
location: Sydney
---
*This is the next in my increasingly inaccurate "three part" blog post series on the [PlayStation 3](https://rubenerd.com/tag/playstation-3/), Clara's and my favourite HD console.*

It was embarrassingly recently when I realised the PlayStation 3 (and 4) had hard drives. My brain was firmly in late 2010s mode, and assumed they had flash storage when their capacities were reported. *Derp!* For someone who worries about data integrity and longevity, I wanted to see how easy it was to swap these out with SSDs. Turns out, it was easier than I expected.

[IFixit has a great guide on swapping the physical drive](https://www.ifixit.com/Guide/PlayStation+3+Slim+Hard+Drive+Replacement/3223), so this is more of a post with some "gotchas" and other learnings.

<figure><p><img src="https://rubenerd.com/files/2025/ps3-slim@1x.jpg" alt="Image of the PS3 Slim, by Evan-Amos on Wikimedia Commons" srcset="https://rubenerd.com/files/2025/ps3-slim@2x.jpg 2x" style="width:500px" /></p></figure>


### Why you may want to swap to an SSD

The PS3 is ripe for an SSD conversion. While the SATA1 bus won't offer much of a performance uplift, SSDs run cooler and quieter. I've also read reports of quicker load times for heavier games with lots of random accesses. This shouldn't come as a surprise.

My motivation though was around preservation. Those original spinning drives are getting old, and I want a backup for <del>if</del>/when the associated stores for DLCs and such go offline. Knowing how to remove the drives would let me do a full disk image for long-term archiving <del>if</del>/when they too kick the bucket.

We picked up a couple of **2.5-inch Crucial BX 500 GB SSDs** for AU $43 each, one for the PS3 Slim, and another for our PS4 (the subject for a future post). These are low-end drives by today's standards, but are perfect for this use case.

Don't worry about what capacity hard drive the PS3 Slim originally shipped with; ours only had a 120 GB drive, and it accepted the 500 GB SSD without issues :). Though anything bigger than this is probably overkill.


### Backing up data if you need to

We [bought our PS3 Slim second-hand](https://rubenerd.com/replacing-our-ps3-with-a-ps3-slim/), which we wanted to wipe and restore to factory defaults. If you want to save your data, I'd suggest attaching the PS3's hard drive drive to a computer and using a tool like `dd(8)` to clone it to the SSD.

The alternative is to use the PS3's built-in [Backup Utility](https://manuals.playstation.net/document/en/ps3/current/settings/backuputility.html) under **Settings** and **System Settings** on the home screen. Connect a FAT32-formatted USB key to the device, and the tool will back up your game and save data to it. The caveat here though is that certain DRM'd material won't be transferred.


### Removing the drive

Again, IFixit has a [proper guide](https://www.ifixit.com/Guide/PlayStation+3+Slim+Hard+Drive+Replacement/3223). But in a nutshell:

1. Turn off and unplug the PS3 Slim. This shouldn't need spelling out, though I blatantly almost forgot this myself.

2. Flip the device upside down, and gently turn the little screw cover towards the front of the device. This will expose one blue screw.

3. Remove the exposed screw.

4. Slide the drive cover (with all the compatibility logos) off sideways.

5. Use the thin exposed metal handles to pull out the drive caddy.

6. Install the SSD into this caddy, and repeat the process in reverse.


### Preparing the system software

The PS3 is designed to update its system software from a USB key if required. For a new SSD like this, you can go to the Sony website and [download the system software](https://www.playstation.com/en-us/support/hardware/ps3/system-software/). Do a search for "PS3 system software update" if that link stops working in the future.

The site provides detailed instructions, but in a nutshell:

1. Format a new USB 2.0 key with FAT32 on your desktop. Don't do what I did and assume it wouldn't choose ExFAT by default (cough).

2. Create a new folder on the key called `PS3`. Within this, create a folder called `UPDATE`.

3. Go to the [PS3 System Software](https://www.playstation.com/en-us/support/hardware/ps3/system-software/) site, navigate to the **How to update PS3 System Software** subheading, click **Update using a computer**, and click **Download PS3 Update**.

4. Move this `PS3UPDAT.PUP` file into the `UPDATE` folder.

5. Attach this key to your PS3 Slim.

As an aside, you can use the same key for updating the PS4 as well, which is a topic for a future post! I labelled mine `PlayStation desu`, because I'm embarrassing.


### Installing system software

If all goes well, your PS3 Slim will boot onto a screen that warns of the following:

> The system software cannot be run correctly. Press the PS button to try to restart the system.
>    
> If the system cannot be restarted, the system partition of thge system storage must be reformatted and you must reinstall the system software.
>   
> Connect storage media that contains update data of version 4.91 or later, and the press the START and SELECT buttons at the same time.
>    
> For information on how to obtain update data, refer to the SIE Web site for your region.

This is expected, because you're dealing with a blank SSD without the PlayStation 3 system software. Press **START** and **SELECT**.

If your USB key was not supported&mdash;as a bunch of mine weren't&mdash;you'll get the following error:

> No applicable update data was found.
>    
> Connect storage media that contains update data of version 4.91 or later, and the press the START and SELECT buttons at the same time.

If this happens, try again specifically with a USB 2.0 key not a 3.0, and make sure it's formatted with FAT32, and *not* ExFAT. If even those don't work, just keep swapping keys from different manufacturers. I found none of my Verbatim keys worked, but as always, my [Toshiba ones did](https://rubenerd.com/my-favourite-usb-key-ever-the-toshiba-pa5003/ "My favourite USB key ever: The Toshiba PA5003A"). Is there anything these keys can't do!?

When the update is detected on the key correctly, you'll get this message:

> The system partition of the system software will be formatted. During the format operation, all data on the system storage may be deleted.
>    
> To format, press and hold the START and SELECT buttons at the same time for at least 5 seconds.
>
> If you format, you cannot go back to the previous version of the system storage.

Press **START** and **SELECT** until the next screen loads.


### After formatting

The rest of the steps are self-explanatory. You'll be advised the disk is being formatted, after which it'll restart and prompt you to accept the User Agreement. It'll then install the system software, restart again, and you'll be presented with that brand new PlayStation 3 controller connection screen.

<figure><p><img src="https://rubenerd.com/files/2025/ps3-load@1x.jpg" alt="The initial PlayStation 3 screen, asking you to connect your controller." srcset="https://rubenerd.com/files/2025/ps3-load@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

Yay! Now log in as normal, and use the **Backup Utility** described above again to restore game data if you need to.

We've been using our PS3 Slim with an SSD for a couple of days now. We haven't noticed a massive difference, but I feel more comfortable using this than the creaky hard drive. And when we've downloaded and installed our stuff, I'll be doing a disk image on the computer to back it up.
