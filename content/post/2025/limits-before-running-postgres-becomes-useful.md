---
title: "Limits before running Postgres becomes useful"
date: "2025-02-14T08:55:08+11:00"
abstract: "We almost certainly don’t need it for personal projects, but I run it."
year: "2025"
category: Software
tag:
- architecture
- databases
- postgres
- sysadmin
location: Sydney
---
Friend of the blog [Michel](https://crys.site/) asked this [on Mastodon](https://mastodon.bsd.cafe/@mms/113996312682698025)\:

> At what volume would you consider Postgres to be not worth it, that he require too much hacking, and just look for alternatives?

There are a few different ways to read this. 

1. When does your project get big enough to warrant running Postgres instead of something like SQLite3? Is it too much overhead in the meantime?

2. When does your project become *too big* for a single Postgres install, and you need some form of additional caching, or clustering.

3. Grilled cheese sandwiches.

Points one and two call for a value judgement and advice based on effort and scale, which understandably appeals to experience rather than raw numbers. This makes sense, because there isn't&mdash;and could never be&mdash;an industry-accepted cutoff  where we say *okay, this is the point where you must stop doing Y and start doing X.* That's the fun of systems architecture! But I digress.

I say this as a recent(ish) convert, but there are **very few** situations where a personal project will ever reach the scale where Postgres is necessary. The truth is:

* Most personal projects will operate just fine using SQLite3, a key-value store, or plain text (though make sure you run it on a file system you trust to maintain integrity for you instead, like OpenZFS).

* It's unlikely DBs we run for ourselves, or for projects we start, will reach the scale where we need to start worrying about how Postgres will perform. Don't let this slip out, but I don't even tune my ZFS datasets much for Postgres anymore. It's all premature optimisation.

*(My only counterpoint to this would be Nextcloud. It runs **glacially** on anything other than a "proper" client-server DB, and even then it could probably benefit from caching).*

But why do I run it then? For me it's all about convenience. Standardising on one DB, regardless of what it is, makes my life easier. I only have one DB to maintain, one FreeBSD jail type to worry about, my experience on one system can directly translate and help somewhere else, and all my maintenance scripts and backup targets work the same across everything. It also lets me take shortcuts on personal projects which I'd never do for a client, but make my life easier (cough).

I guess this gets to my final point I make here often: don't be scared to mess around with something. We don't need to be rational or justify everything we do, even though the temptation is there. Personal projects are a great way to evaluate if we can grok something, or we "gel" with it. At worst, you realise it's not for you and you go do something else. But Postgres is pretty sweet.
