---
title: "A question about aviation ESG plans"
date: "2025-02-16T10:03:19+10:00"
abstract: "If you have to ask yourself these sorts of questions, you’ve stalled at takeoff"
year: "2025"
category: Ethics
tag:
- aviation
- business
location: Sydney
---
I read a few blogs and sites for transport insiders. I'm clearly not the target audience, but I still find them really interesting.

An aviation site I frequent doesn't have ads *per se*, but they run these unusual surveys in an `iframe`. I guess they're designed to lure you in with a question, then direct you to another site where they can sell you something.

Today's one was oddly specific. Notice anything missing in the options?

> <span style="font-weight:bold;">What is the primary reason why a company should set up an environmental, social and governance (ESG) performance plan?</span>
> 
> • Pressure from customers   
> • Pressure from investors   
> • Legislation and pressure from government   
> • Desire to improve financial performance   
> • Pressure from workers

What about... because you should? It's like reading a list of reasons not to poke people in the eye, and they're all about how expensive eye pokers have become, or how your investors think there are only a finite number of eyes to poke!

My eye just twitched. Welp.

The framing (**AAAAAH** get it, frames, glasses?!) here is a useful insight into how management at these businesses view their responsibilities and obligations to the societies that made them possible. To them, ethics are just another external pressure or imposition, to be resisted as long as viable.

I can't help but draw parallels here between ethics and quality. You can't make a business ethical with platitudes and a survey, much as you can't “do quality” by adding another check at the end of a production line, or add tastiness by sugarcoating something disgusting at the end. They have to be intrinsic to the business and how it operates for it to exist. An ESG, therefore, should be a gap analysis, or a set of refinements, or codifying existing behaviour.

Put another way, is a phrase with three words. If you think such a plan is a complicated burden or imposition, you've stalled at takeoff. Get it, because it was an aviation website and... hey, shaddup.

*Damn it Jeeves, when did eye pokers become so much trouble!?*
