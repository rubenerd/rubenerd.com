---
title: "Remember international calls?"
date: "2025-01-15T11:01:30+11:00"
abstract: "A rude person failing to make an international call reminded me that they used to be a thing!"
year: "2025"
category: Hardware
tag:
- phones
location: Sydney
---
Someone decided to make a loud call on a packed train with her phone company yesterday. I’ll spare you writing it in capital letters:

> “I’ve been struggling to call a number in Canada, I’ve even putting in the prefixes right but it’s simply not working. Please escalate me to someone who will assist me to resolve this&hellip;”

As the train filled up, she became increasingly frustrated with the people around her making noise, the irony seemingly lost on her. But then, self-absorbed people tend not to score highly in the self-awareness department. That was a lot-of hyphens.

But that aside, remember when STD and international calls were a thing!? I can still remember needing `001` for SingTel and `008` for StarHub to call internationally from Singapore. Calls to Malaysia meanwhile, despite technically being a foreign country after 1965, continued to be routed and charged as though they were interstate within the same country [until the scheme was half-retired in 2017](https://rubenerd.com/no-more-singapore-malaysian-domestic-calls/).

In Australia, Telstra’s international code was `0011`. Maybe it still is. Either way, you really didn’t want to use these too often though, on account of the massive cost and… very… frustrating… l-l-latency. Australia is a long way away from most people and things. I've never called New Zealand, but I wonder if there was a special code to access that like there was from Singapore to Malaysia?

It's all moot thesedays anyway now, at least for me. I press the phone button in LINE, Signal, or Skype, and be done with it. The quality of the call between here and my colleagues in San Francisco is better than the phone calls I take from delivery drivers who’ve arrived downstairs.

I know how and why, but it’s still a bit mind bending.
