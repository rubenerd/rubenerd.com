---
title: "Neverending beeps in the night"
date: "2025-01-07T08:31:32+11:00"
abstract: "How a broken (and LOUD) fire alarm made me appreciate everything that works around me, most of the time."
year: "2025"
category: Hardware
tag:
- engineering
- life
location: Sydney
---
I should be more thankful that the systems surrounding me work as well as they do. There is so much that happens behind the scenes that make our lives possible and pleasant, from the power grid and water systems, to telephony wiring and mobile reception. I take it for granted that I plug stuff in, or connect to something, and it just works. Good tech is transparent, as an old professor of mine was fond of saying.

But it's all a balancing act, as any systems engineer will tell you. Behind the scenes is a complicated, tangled web of interconnected systems, some of which are likely in a degraded state right now without you knowing. As long as the [Swiss cheese holes don't line up](https://en.wikipedia.org/wiki/Swiss_cheese_model "Wikipedia article on the Swiss cheese model"), we're good. Until the rare day when they do, and the illusion of perfect functionality is shattered.

Last night was one such occurrence. We'd sat down to eat dinner with ungodly amounts of white pepper&mdash;my current obsession&mdash;when we heard an ear-piercing `BEEP` emanating from the hallway outside the apartment. It beeped in quick succession, first with a short `BEEP` lasting a second, then a longer `BEEEEP` lasting two, then back to the shorter `BEEP`. We figured it'd stop in a moment, so we kept eating.

But in the paraphrased words of famed electrical engineers Smashmouth, the beeps were coming and they wouldn't stop coming. And they were **LOUD.** It was loud enough that if you'd said the source of the beeps was under our dining table, I'd have believed you.

After the `BEEEEPs` had managed to drill sufficiently into my skull that I began to question my sanity, I ventured outside to see what the deal was. I found my neighbour on his mobile phone with the building manager trying to troubleshoot what the issue was. The `BEEEEPs` were somehow even louder by his apartment, for which I felt pangs of empathy. These were expressed with ringing in my ears. 

For a time I'd assumed it was the lift. The call buttons for the shiny replacement lift in our building make a horrible, shrill beep sound when the doors open, which was a similar frequency to the beeping we were hearing. I theorised it might have been the over capacity warning had got stuck, based on prior experience in an industrial building where this had happened. But we got into the lift, and the beeping became muffled as the doors closed. There went that theory.

Eventually our neighbour and I tracked down the source of the noise: one apartment at the end of the hallway. We took a punt and theorised it was their smoke alarm. The building manager had us put the phone close to the door, and immediately confirmed our suspicions. Though he said the duration and frequency of the beeps was “screwed up, suggesting a malfunction.

None of us had keys to the unit&mdash;obviously&mdash;so the building manager offered to call the owner and request they come home to silence their alarm. By this point I think we were tempted to smash down the door with our bare hands to turn off `BEEEEEP` this infernal alarm (cough).

We went for a long evening walk to top up our 10,000 steps a day, but were dismayed to hear the `BEEP` `BEEEEP` `BEEP` `BEEEEP` still sounding when we made it back. Either the people who owned that unit were taking their time getting back, or worse, they were away or on holidays. Would we `BEEP` be hearing `BEEEEP` all this `BEEP` noise non-stop `BEEEEP` for days?!

As we were prepping the bedroom with additional fans in an attempt to drown out the sound with some white noise, the `BEEEEP` sounds stopped as suddenly as they started. We checked our watches: 23:03. The relief washed over us like relief that washed over us. Excellent use of metaphor there Ruben.

It struck me though as we were fixing the room up for bedtime: the situation we found ourselves in was normal. This is what it always is. But having that peace violated by a piece of errant technology for more than four hours suddenly made this quiet room feel spectacular.

I like that stuff is working again (touch wood) and that we have our (relative) quiet back. I'm still *slightly* concerned that it's the following morning and I *still* have ringing in my ears, but I assume that will fade until the next time it `BEEP` happens.
