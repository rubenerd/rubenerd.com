---
title: "Downloading WatchGuard evaluation feature keys"
date: "2025-01-09T09:54:24+11:00"
abstract: "The Feature Key link in the trial activation step no longer exists. You need to go to Manage Products now."
year: "2025"
category: Internet
tag:
- guides
- networking
location: Sydney
---
The way Feature Keys are downloaded for WatchGuard FireboxV evaluations has changed, at least since the last time I had to do this for a client. On the off-chance this helps someone:

1. Log into the **WatchGuard Partner Portal**, and under the **Products** menu, click **Virtual Appliance Evaluation**.

2. Next to the **WatchGuard Product** you want to request an evaluation key for, click **Request Evaluation**.

3. You'll receive an email with the evaluation **Serial Number**. Note this is *not* the Feature Key you need to activate the FireboxV.

4. Click the **Activate** link in the email. Enter the **Serial Number** into the box, click **Next**, give the device a unique name, then click **Activate**.

The Feature Key link here no longer exists. To get the Feature Key, you need to download it in a separate process, which is slightly annoying:

1. Go back to the **WatchGuard Partner Portal**, and click the **Support Center** button in the top-right.

2. From the **My WatchGuard** menu, click **Manage Products**.

3. Under the **Network Security** heading, click **View Products**.

4. On the table heading, click **Support Expiration** twice to sort the table in descending order of expiration. This will float the most recent to the top.

5. Click the three vertical dots next to your new device, and click **Get Feature Key**.

