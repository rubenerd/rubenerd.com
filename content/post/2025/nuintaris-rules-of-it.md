---
title: "nuintari’s Rules of IT"
date: "2025-01-21T08:31:19+11:00"
abstract: "[Removed]"
year: "2025"
category: Software
tag:
- networking
- programming
- quotes
location: Sydney
---
This was a two sentence quote from Mastodon, which this person didn't like. I have removed it by request. I've also codified some [feedback rules](https://rubenerd.com/feedback-rules/) to prevent this happening in the future.

So you don't go empty handed however, I encourage you to read about the [Heisenbug](https://en.wikipedia.org/wiki/Heisenbug), which is a variant of the idea I originally shared here. It deserves own post someday!
