---
title: "Lightbulbs, and two types of people in this world"
date: "2025-01-17T20:21:25+11:00"
abstract: "Be the person who doesn’t ping you if they already see you doing the thing."
year: "2025"
category: Thoughts
tag:
- philosophy
location: Sydney
---
You know that quip about there being ten types of people in this world: *those who understand binary, and those who are sick of this joke?* Well I have some additional classifiers.

For example, there are people who like crunchy peanut butter, and there are those who like *extra* crunchy. There are early risers and late risers. There are people who care, and people who don't. There are those who understand UNIX, and those condemned to reinvent it, poorly. Something something containers.

But lately I've found one other, oddly specific way that's been illuminating. Say you're approaching a light switch to turn off a light, surprising though it may seem. Someone **observing** you doing this will react in one of two ways:

1. Not say anything.

2. Ask us to turn off the light.

The people in group (1) I'd consider... normal. We're walking towards the light switch to turn it off. The actions are in progress, and the outcome is already known. There's no point telling someone to do something they're already doing.

Or is there? Because the people in group (2) think there is. They know the switch is about to be turned off, they can *see* this action is being performed, and yet they still want to issue the instruction. It's fascinating.

I can think of reasons for this, some of which may overlap:

* They want to take the credit for turning off the light. They realised it was a good idea, and/or are miffed they didn't think of it first.

* They want to feel as though they contributed to a positive outcome. As Barney said on The Simpsons: "Wait, you didn't do anything", to which Leonard replied: "Didn't I?"

* It's a weird power play, flex, or performance to be *seen* bossing around or directing the actions of others. It's a zero-sum game, power is nothing without perception of power, and so on.

* They're... not pleasant people.

Now this is a contrived example, and I'm clearly speaking in subtext here. But I've known a shocking amount of people, in my personal life and professionally, who behave like group (2). It's one of the signals I use to inform whether I like a person, or will want to engage with them voluntarily.
