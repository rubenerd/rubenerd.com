---
title: "Music Monday, Let It Be"
date: "2025-02-17T09:48:56+11:00"
abstract: "Speaking words of wisdom."
thumb: "https://rubenerd.com/files/2025/yt-QDYfEBY9NM4@2x.jpg"
year: "2025"
category: Media
tag:
- music
- music-monday
- the-beatles
- paul-mccartney
location: Sydney
---
Today's *[Music Monday](https://rubenerd.com/tag/music-monday/)* is another Paul McCartney classic.

My favourite Beatles song was always *Here Comes the Sun* by the late great George Harrison, but this always came close. That piano line is probably my favourite of any pop song, and the guitar in the coda... amazing.

I bring this up, because I visited my mum in that island café in my dreams last night, and she offered the same advice. She knew Clara and I have been more than a little obsessed with Paul lately, so it stands to reason. Although we were sitting down.

<figure><p><a target="_BLANK" href="https://www.youtube.com/watch?v=QDYfEBY9NM4" title="Play Let It Be (Remastered 2009)"><img src="https://rubenerd.com/files/2025/yt-QDYfEBY9NM4@1x.jpg" srcset="https://rubenerd.com/files/2025/yt-QDYfEBY9NM4@1x.jpg 1x, https://rubenerd.com/files/2025/yt-QDYfEBY9NM4@2x.jpg 2x" alt="Play Let It Be (Remastered 2009)" style="width:500px;height:281px;" /></a></p></figure>

