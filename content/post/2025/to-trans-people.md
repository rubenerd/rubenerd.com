---
title: "To trans people 🏳️‍⚧️"
date: "2025-01-23T11:45:58+11:00"
abstract: "I felt it important putting this out there given what’s happening."
year: "2025"
category: Thoughts
tag:
- love
- trans-rights-are-human-rights
location: Sydney
---
You are loved, respected, and appreciated here. I wish I could reach through this blog post and give you a hug. Or a fistbump, or whatever your preference.

You also have medical science on your side. You already knew that, but the fragile *facts over feelings* crowd need to be reminded.

I thought that was worth putting out in the world, given what's happening.

If you disagree, consider how much you need the Internet and your phone. You'd be shocked how much of it is held up by my queer and trans friends. No seriously, I'd almost consider it a key qualification for comms and engineering at this stage.

**Update:** I was asked by a few of you if this extends to the nonbinary as well. Of course :).
