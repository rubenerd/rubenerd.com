---
title: "Daniel Gerlach’s programming quote collection"
date: "2025-01-28T18:40:41+10:00"
abstract: "If it doesn’t work, it doesn’t matter how fast it doesn’t work."
year: "2025"
category: Software
tag:
- programming
- quotes
location: Sydney
---
Daniel collated a [list of programming quotes](https://gerlacdt.github.io/blog/posts/programming-quotes/), though they could easily apply to so many endeavours. I hadn't heard of these:

> Weeks of coding can save you hours of planning. ~ Unknown
> 
> Good judgement is the result of experience … Experience is the result of bad judgement. ~ Fred Brooks
> 
> When one teaches, two learn. ~ Robert Heinlein
> 
> The craft of programming begins with empathy, not formatting or languages or tools or algorithms or data structures. ~ Kent Beck
> 
> A good programmer looks both ways before crossing a one-way street. ~ Anonymous
> 
> If it doesn’t work, it doesn’t matter how fast it doesn’t work. ~ Mich Ravera

And my favourite, which I *had* read before:

> Don’t comment bad code—rewrite it. ~ Brian W. Kernighan

