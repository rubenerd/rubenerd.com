---
title: "We defeated the Ender Dragon!"
date: "2025-02-10T08:34:40+11:00"
abstract: "And it only crashed my Linux game machine once!"
year: "2025"
category: Software
tag:
- games
- silly
- minecraft
location: Sydney
---
*With special thanks to the imitable [Minecraft Wiki](https://minecraft.wiki/) for their advice, and all the reference material linked to below. Make sure you go to them over the inferior Fandom version.*

Clara and I have been [playing Minecraft for four years now](https://rubenerd.com/tag/minecraft), all on the same map and server. It managed the Herculean feat of surpassing the *SimCity* franchise, *Train Simulator*, and *Commander Keen* to become my favourite game of all time. It's the perfect mix of open-world adventure and endless creative sandbox that I'd so badly wished those LEGO games were when I was a kid. You can [view the full archive](https://rubenerd.com/tag/minecraft/) here.

We've been playing it of an evening, mostly to relax and unwind after long days at work. Being a peaceful-mode server though did limited what we could gather and where we could go. [The End](https://minecraft.wiki/w/The_End) was always out of reach as it requires [Blaze Rods](https://minecraft.wiki/w/Blaze_Rod) from hostile mobs in [The Nether](https://minecraft.wiki/w/The_Nether), which don't spawn in peaceful.

Something tweaked this year, and we decided to go for it. I installed the MyWorlds plugin which lets you host multiple worlds on the same server (we'll come back to that). We generated a new hostile "resource" server&mdash;as so many streamers do&mdash;and connected it to our primary peaceful world with a portal. We named it *[Midsomer](https://en.wikipedia.org/wiki/Midsomer_Murders)* after the deadliest county in England, and proceeded to built it into a fortress.

We found a [Nether Fortress](https://minecraft.wiki/w/Nether_Fortress) with surprising ease, and two [Blaze](https://minecraft.wiki/w/Blaze) spawners. I didn't get any screenshots from the main event (so to speak), but in our classic silly nostalgic style our first Blaze Rod, [Blaze Powder](https://minecraft.wiki/w/Blaze_Powder), and the armour we used to defeat them soon made their way to our museum on the auspicious eighth floor:

<figure><p><img src="https://rubenerd.com/files/2025/museum-blaze@1x.jpg" alt="Wall of our museum showing our first Blaze Rods and Powder" srcset="https://rubenerd.com/files/2025/museum-blaze@2x.jpg 2x" style="width:500px;" /></p></figure>

We had accumulated enough [Ender Pearls](https://minecraft.wiki/w/Ender_Pearl) from random loot boxes over the years, so we managed to mix them with the Blaze powder to make [Eyes of Ender](https://minecraft.wiki/w/Eye_of_Ender), and activate the [End Portal](https://minecraft.wiki/w/End_portal) closest to our main base. We also took the opportunity to refurbish the dingy room into something nicer with glow berries and our *tricolour* (ooh lah lah):

<figure><p><img src="https://rubenerd.com/files/2025/end-portal@1x.jpg" alt="The completed End Portal." srcset="https://rubenerd.com/files/2025/end-portal@2x.jpg 2x" style="width:500px;" /></p></figure>

We were feeling epic up to this point. While we far prefer the exploring and creative parts of the game, there's definitely a sense of accomplishment having come this far. It was also like re-discovering the game again in a way we were completely unfamiliar with before.

We jumped through the End Portal together, armed with dirt, arrows, and the [Ender Dragon Trap](https://www.youtube.com/watch?v=dxGmt8_Vbgg) from ianxofour on recommendation from [CallMeKevin](https://www.youtube.com/c/callmekevin). We quickly built the requisite dirt platform, set the trap, and took down the [Ender Dragon](https://minecraft.wiki/w/Ender_Dragon) together with a barrage of arrows. This worked spectacularly well.

Yay! We defeated the Ender Dragon! **We "won" Minecraft**, and it only took four years on the same server!

<figure><p><img src="https://rubenerd.com/files/2025/ender-dragon@1x.jpg" alt="The Ender Dragon going boom!" srcset="https://rubenerd.com/files/2025/ender-dragon@2x.jpg 2x" style="width:500px;" /></p></figure> 

We sailed down to the [Exit Portal](https://minecraft.wiki/w/Exit_portal) and jumped through, and the game credits began scrolling for the first time. I'd avoided reading any of it on streams, so this was all new.


### Here comes the proverbial posterior prognostication

*But...* while we were high-fiving and generally being embarrassing for people in their thirties beating a computer game, the universe had other plans. Those of who who've had the misfortune of playing games on Linux with Nvidia silicon know it all too well. Barely 30 seconds into the credits, my desktop ran out of leather jackets for the penguins and crashed, requiring a full power cycle.

Clara made it through the Exit Portal back to our world, but when I re-joined and attempted to go though, nothing happened. It was the most disappointing wading pool ever. Meanwhile, Clara tried to enter from the End Portal in our primary world, but received an error message saying there wasn't anything "to connect it to". Frack.

I'll save the detailed troubleshooting steps for another post, but in the interim we disabled MyWorlds, updated PaperMC, pledged to my machine that we'd be replacing the graphics card with AMD as soon as the funds were available, and the portals began working again. I'm not sure whether it was our aging combination of plugins, but going back to something as vanilla as we could resolved the issue. We then swapped back to using [Multiverse](https://www.spigotmc.org/resources/multiverse-core.390/) again, and this also worked fine.

Anyway, we beat the Ender Dragon! Boom! Now we can explore the end, bring back some of those tasty [Wensleydale Cheese](https://minecraft.wiki/w/End_Stone) blocks, [purple stone](https://minecraft.wiki/w/Purpur_Block), [Shulker Boxes](https://minecraft.wiki/w/Shulker_Box) for our endless construction projects, and [Elytras](https://minecraft.wiki/w/Elytra) to fly.

What a blast!

<figure><p><img src="https://rubenerd.com/files/2025/exit-portal@1x.jpg" alt="Our Exit Portal" srcset="https://rubenerd.com/files/2025/exit-portal@2x.jpg 2x" style="width:500px;" /></p></figure>

