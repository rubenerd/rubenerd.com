---
title: "Doing maths on Hugo post params"
date: "2025-03-06T07:31:22+11:00"
abstract: "Cast it as a number first, because params are always strings."
year: "2025"
category: Software
tag:
- guides
- hugo
location: Sydney
---
Go's templating engine in Hugo has `mul`, or `math.Mul` function, for performing basic multiplication:

    {{ $number = .Params.number }}
    {{ mul $number 2 }}

I did this on some frontmatter for a post I was testing, but got an error:

    error calling mul: Can't apply the operator to the values

Not the most helpful error. I assumed post params stored without quotes would be treated as numbers, but they're still strings. Yay, YAML!

So cast as a number, as [agh and ju52 on the forums](https://discourse.gohugo.io/t/can-you-use-math-functions-on-page-and-site-variables/14442) suggest:

    {{ $decimal := (float .Params.number) }}

This also works with `int`:

    {{ $number := (int .Params.number) }}

