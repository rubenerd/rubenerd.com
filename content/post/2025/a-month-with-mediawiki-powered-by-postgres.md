---
title: "A month with MediaWiki powered by Postgres"
date: "2025-02-08T09:44:51+11:00"
abstract: "It works great, so I’m moving to it."
year: "2025"
category: Software
tag:
- databases
- mediawiki
- posgres
- retrospectives
location: Sydney
---
I wrote about [running MediaWiki with PostgreSQL](https://rubenerd.com/mediawiki-with-postgres/) instead of MariaDB (et.al) back in January:

> We run MediaWiki stock without extra plugins, but I’m still impressed how there’s been no appreciable difference in functionality or performance. I can now also use my little library of Postgres scripts for backups, and only have one DB on our jailed FreeBSD environment.

I'd actually been running it for a few weeks before that point, so it's now been about a month. And it's great. I can use all my Postgres maintenance scripts, and remove another set of dependencies from what I have to maintain! Happy days.

    $ psql mediawiki
    mediawiki=# BEGIN;
    mediawiki*# SELECT * FROM mediawiki.actor ORDER BY actor_name;
      
     actor_id | actor_user |    actor_name     
    ----------+------------+-------------------
            5 |          5 | BlueBearCat
            4 |          4 | JimKloss
            3 |          3 | Kiri
            2 |          2 | MediaWiki default
            1 |          1 | Rubenerd

I'm pretty conservative with how I run software, as I said in that original post. But I think in this case I think I'm ready to call it. At the scale I run MediaWiki for people, and with the standard plugins, PostgreSQL is a great DB. I've been in the process of migrating and merging a few different legacy and recent MediaWikis into this one, and so far everything has worked well.

With thanks to the MediaWiki volunteers maintaining Postgres support, the `pgsql@` team at FreeBSD team for maintaining the port, and the Postgres developers. You've all made my life immeasurably better :).
