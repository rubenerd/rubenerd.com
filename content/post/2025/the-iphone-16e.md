---
title: "Goodbye to the iPhone SE"
date: "2025-02-20T13:40:25+11:00"
abstract: "It was an interesting phone, but the iPhone 16e is the end of line."
thumb: "https://rubenerd.com/files/2025/iphone-se-tangara@2x.jpg"
year: "2025"
category: Hardware
tag:
- apple
- history
- iphone
- iphone-se
- phones
location: Sydney
---
The Fruit Company launched the iPhone SE in 2016, a handset with the same form factor as the iPhone 5. It offered people a more affordable, smaller alternative to the iPhone 6 and 6s, with the same access to software updates, and the company's ecosystem of hardware, accessories, and the App Store.

It was an interesting move from the company at the time. They'd sold prior versions of iPhones alongside the new hotness for a while, downgrading the former in price to attract more buyers. But the SE was the first iPhone designed *from the outset* to not be a flagship (technically the 5c wasn't either, but this was released at the same time as the 5s, and could be thought of as part of the same family. The SE was its own product line).

Two more iPhone SEs would come out in 2020 and 2022 respectively, both based on the external design of the iPhone 8, and designed around the same ethos. This was to be an entry-level smartphone to get one's foot in the proverbial Apple door, and was marketed and priced accordingly.

Save for its unusual position in Apple's device lineup, the SE itself wasn't an interesting phone. But that was the point. Being cutting edge, exciting, and conspicuous was the job of the flagship iPhones, just as Samsung, Google, and others have their expensive and midrange tiers. You could certainly buy a cheaper Android handset, but if you wanted an affordable iPhone&mdash;relatively speaking&mdash;this was it.

<figure><p><img src="https://rubenerd.com/files/2025/iphone-se-tangara@1x.jpg" alt="The iPhone SE third gen, with a Tangara for scale." srcset="https://rubenerd.com/files/2025/iphone-se-tangara@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

For someone who just wanted a phone that that worked, and with regular security and software updates, the SE was perfectly serviceable. One might say [it was boring](https://rubenerd.com/boring-tech-is-mature-not-old/ "Boring tech is mature, not old"), and mean it positively! I can say from experience that being an SE owner for years was&hellip; oddly liberating. There was no push to jump on the annual upgrade treadmill, because the phones themselves were so rarely updated. It was the Mac Mini of iPhones, in that an update may come sometimes with improved features, but the company wasn't fussed about ignoring it for stretches at a time.

A lot was made about how the SE was Apple's attempt at targeting "emerging markets" or "The Global South", but I was surprised at just how many SEs I saw and continue to see in the business world. They're a popular choice for corporate fleets, and seem to have found a niche as a second or work phone. They can all be managed remotely, they're reliable, have great battery life, and are far less expensive than the laptop-priced phones the company otherwise releases.

The new iPhone 16e is not an SE, despite what the pundits and Apple themselves may claim. Where the iPhone SE was a budget(ish), entry-level device, the 16e is not. It's the iPod Touch to the iPod Classic, if the Touch was twice the price.

As far as I'm concerned, the analysis basically stops there. The SE is no longer on offer from Apple. If you're after a (more) budget iPhone, your best bet is the second-hand market. They probably won't support AI either, which is a bonus!
