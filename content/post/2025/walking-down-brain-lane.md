---
title: "Walking down Brain Lane"
date: "2025-01-12T08:21:08+11:00"
abstract: "Inaugurating my series for people who’ve demonstrated exceptional use of their brains"
thumb: "https://rubenerd.com/files/2025/gbl@2x.jpg"
year: "2025"
category: Thoughts
tag:
- walking-down-brain-lane
location: Sydney
---
Clara and I were walking back from having lunch on Sydney's lovely North Shore, when we crossed the path of what might be the best-named street of all time:

<figure><p><img src="https://rubenerd.com/files/2025/gbl@1x.jpg" alt="George Brain Lane" srcset="https://rubenerd.com/files/2025/gbl@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

[George Brain](https://en.wikipedia.org/wiki/George_Brain) was a book keeper, accountant, and New South Wales state politician who advocated for free milk distribution to school children during the Great Depression, and was an advocate for higher education access and free public libraries among other noble pursuits. He also, reportedly, had a dry sense of humour not unlike my own. I wish I could have met him.

In light of his credentials, I've decided to walk down his namesake lane whenever I need to highlight a person or organisation who has demonstrated *exceptional* use of their brains. [Two](https://rubenerd.com/mozillas-ridiculous-orbit-service/ "Mozilla's ridiculous Orbit service") [organisations](https://rubenerd.com/the-eff-foot-rakes-on-censorship/ "The EFF is not a serious organisation") didn't  make the cut in time, but I'm sure there will be plenty more whey they came from.

This might be fun! Normally I don't want to share news that frustrates me, because it's somehow even less fun to write than it is to read. I think this format will add much-needed levity to these occasions, like a fine starter to a sourdough of hubris. Brains. 🧠
