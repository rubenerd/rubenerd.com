---
title: "My Atari 1040STᴱ!"
date: "2025-01-30T10:44:57+11:00"
thumb: "https://rubenerd.com/files/2024/1040ste@1x.jpg"
year: "2024"
category: Hardware
tag:
- atari
- atari-ste
- retrocomputers
location: Sydney
---
Remember when I [waxed lyrical about the Atari ST](https://rubenerd.com/the-atari-st-is-my-favourite-16-bit-machine/ "The Atari ST is my favourite 16-bit machine") last year, describing it as my favourite 16-bit computer series? Even leaving aside their unique technical capabilities and stunning industrial design, I said the ST machines had "so much home computer history soldered into their DNA that I can’t help read and write about them".

I wear my emotions on my sleeve&mdash;it's why I don't play poker&mdash;and I suspect you all read right through my excitement (cough).

I can now admit that after *years* of saved searches, I'd found a local music producer in Sydney who was selling their 1040STᴱ by the time that post went live. Once I confirmed she was fully functional and wasn't missing any components (save for a floppy drive replaced with a GOTEK), I managed to get her for a *shockingly* reasonable price. ST machines aren't that common in Australia, and I paid less than half than what I'd seen for an STfm, or even a *broken* 520 ST on eBay.

But we're getting ahead of ourselves. Here she is:

<figure><p><img src="https://rubenerd.com/files/2024/1040ste@1x.jpg" alt="A slightly yellowed Atari 1040STE on a wooden floor." srcset="https://rubenerd.com/files/2024/1040ste@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

As you can see from the photo, her keys are slightly yellowed and need a clean, and her grey case has taken on that "coffee milk colour" like my [Commodore 64 "Aldi" case did](https://rubenerd.com/what-colour-is-the-commodore-64/ "What colour is the Commodore 64?"). I might want to retrobright the keys slightly, but I oddly... like this warmer grey colour?

This is the 50 Hz UK model, with a manufacturing date of 1989. I'm not sure if the ST line was ever localised specifically for Australia; I suspect we were too small a market. The *E* stands for *Enhanced*, referring to the machine's expanded 4096 available colours, SIMM slots for up to  4 MiB of memory, digital 8-bit PCM audio support, and a "BLiTTER" graphics chip for accelerated hardware scrolling. More details are on my [Retro Corner](http://retro.rubenerd.com/1040ste.htm).

I can't test her properly yet, because I'm still waiting for the appropriate switching VGA connector to output her colour and monochrome graphics. But I connected her to mains power and both her and the GOTEK lit up, which is a good sign. I hope that somewhere, somehow, both Gary Kildall and Jack Tramiel are getting a kick out of it!

[People on Mastodon](https://bsd.network/@rubenerd/113491467412800893) have been *extremely* helpful with tips for modern hardware, upgrades, demos to run, and games to play. If you have any other advice for an ST fan who admittedly has never used one beyond the excellent [Hatari emulator](https://hatari.tuxfamily.org/), feel free to reach out.

That's all for this post for now. I can't wait to start what I'm hoping will be an ongoing series on the 1040STᴱ. I grew up obsessed with 8-bit home computers, and we only ever had 32-bit machines at home, so the 16-bit era completely passed me by. Time to make up for lost time.
