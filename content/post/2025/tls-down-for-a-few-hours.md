---
title: "TLS down for a few hours"
date: "2025-02-07T13:04:54+11:00"
abstract: "Changed DNS for a subdomain resulted in the whole cert breaking. Welp!"
year: "2025"
category: Internet
tag:
- hosting
- security
location: Sydney
---
Many of you pinged me this morning saying my site certificate had expired. Thanks for letting me know.

This time certbot didn't autorenew because I'd removed a DNS entry from a side project, and I had it as one of the subdomains for the renewal. It couldn't issue a cert for that subdomain, so the entire domain renewal failed. I believe the technical term for this is *welp*.

I'm not sure what best practices are here. Let's Encrypt lets you issue a cert for multiple subdomains. Maybe it's worth using Let's Encrypt to issue discrete certificates for subdomains, so a failure to renew one doesn't break everything else.

Maybe it's also time to *buy* certificates again? I dunno, this site is already costing a decent amount now. Is RapidSSL still a going concern?

