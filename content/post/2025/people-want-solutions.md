---
title: "People want solutions"
date: "2025-02-04T13:55:18+11:00"
abstract: "People don’t care how something works."
year: "2025"
category: Software
tag:
- business
- design
location: Sydney
---
My dad used to quip that chemists have solutions. That's probably from where I derive this awful sense of humour. *But I digress*.

It's been five years now since I moved into my current role, doing something I never in a million years would have expected. I talk to people, about their tech. I sit with sales teams, and I'm "the tech guy" for architecture, system design, and advice. I then write these notes, requirements, and constraints into handover documents, which I pass to clients whom I'm sure are happy to receive them and proceed to never read.

*Huzzah!*

I've learned a lot from doing this, but there's one lesson above all that has stood out, and it's something I think a lot of sysadmins, developers, and project managers forget. **People&mdash;by and large&mdash;don't care how something works.** People care about three things:

* Solving a business problem, or fulfilling a requirement

* Within a certain timeframe

* Within a certain budget

People hire solution architects, or ask for advise more broadly, because they don't know, don't care, or don't have the time to be thinking about it themselves. That's literally why we exist.

I suppose it's no different from a hair dresser, or a doctor, or an airline pilot. I don't care what kit a hair dresser uses (after all, the cloud and hair dressers are "just" someone else's computer or scissors), I just want a certain output, at the agreed upon price, and in a reasonable amount of time.
