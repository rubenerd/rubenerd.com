---
title: "Helicopters are loud"
date: "2025-02-03T08:53:03+11:00"
abstract: "Who knew!? Besides everybody."
year: "2025"
category: Thoughts
tag:
- transport
location: Sydney
---
I'm out on the balcony upstairs this morning having coffee. I know, shocking. Just as I opened Thunderbird to consult the morning calendar, a helicopter came into our view. It's well away from us, at least a kilometre I'd say, but it has a direct line-of-sight owing to there being nothing but trees between us.

Helicopters are *loud*. I mean, really, *really* loud. Who knew!? Besides everyone. 🚁
