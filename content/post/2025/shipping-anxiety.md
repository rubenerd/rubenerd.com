---
title: "Shipping anxiety"
date: "2025-01-25T07:14:41+11:00"
abstract: "Our package wasn’t delivered, it’s entered... a state!"
year: "2025"
category: Thoughts
tag:
- shopping
- transit
location: Sydney
---
Damn it, *another* ambiguous title. Do I mean that shipping is causing anxiety? Or that my spicy visual novel has a character nicknamed Anxiety whom people want to ship with the protagonist? Or am I bundling my anxiety in a metaphorical sense into packages and shipping it elsewhere. Oh boy, if only it were that easy!

As I've disturbingly started to say with more frequency here of late: where was I going with this?

Logistics is one of those industries that goes well until it doesn't. Whether we receive our packages on time, or have it [wedged in the Suez canal](https://en.wikipedia.org/wiki/2021_Suez_Canal_obstruction "Wikipedia: 2021 Suez Canal obstruction"), there isn't a middle ground, or canal. It's in one of two states, with a bunch of sub-states:

1. **Our package was delivered**

2. **Our package has not been delivered, because it...**

    * hasn't been collected

    * hasn't been shipped

    * hasn't been dispatched

    * is in transit

    * has been lost

    * has been stolen

    * has been eaten/crushed by a large animal or small vechile

The problem is, even with all the tracking in the world, state 2 is an almost complete black box. It's Schrödinger's Package (heavens), operating in a cardboard superstate, seemingly floating somewhere in the world, yet functionally may as well not exist. It's only when our consignment is received into our welcoming arms, letter boxes, parcel lockers, or postal depos because they claimed you weren't home when you were with one of those infernal "we tried to deliver" cards, that we can say&mdash;with confidence&mdash;we received our goods, and they exist.

Whew, that was a long sentence. Though not half as long as the time I've been waiting for a retrocomputer part, as I [mentioned on The Mastodons](https://bsd.network/@rubenerd/113779934420772781). How's that for a *delivered* segue? Don't answer that.

Because the other problem with logistics&mdash;okay, there are many problems&mdash;is compensation. People who ship packages may opt into paying for insurance, or the shipping company may offer some up to a certain financial value. The seller may also offer to ship you a replacement for your lost consignment, with an extra bag of fresh coffee beans on the side for your trouble.

That's great and all, but as with all insurance, and much to the consternation of the most ardent of capitalists, *money can't buy everything.* Financial compensation may help your wallet recover, and a replacement component might get your coffee machine working again. But if that item was a one-off piece, or something unusual, unique, or irreplaceable, you're Evergiven up the Suez canal without a rudder.

This is what I mean when I refer to shipping anxiety. For something you couldn't feasibly replace, how long do you hold out for the parcel to be found? A week? A month? This is compounded by the fact that on two occasions I've received parcels sometimes *years* after they were sent, often with a sheepish message written from Australia Post saying my parcel destined for the largest city in the country was incorrectly sorted and sent to Sydney *Street* in Riga instead. MegaLag did a [series of videos about this](https://www.youtube.com/watch?v=tLqjlTKiR9o "AirTags Expose Dodgy Postal Industry"), though he's probably most famous now for being "the Honey guy". Don't get me started.

I'm not sure why I first thought of Latvia, and I doubt they have a Sydney Street. But my point stands&hellip; wherever it is.

It's also why I don't envy people who ship physical goods online. Even my side business selling occasional wares on eBay has generated enough problems that it makes me not want to do anything outside the digital realm ever again. Though I suppose at that stage I'm then dealing with delivery problems of a different nature.

Logistics. It causes me anxiety.
