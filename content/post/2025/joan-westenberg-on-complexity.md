---
title: "Joan Westenberg on complexity being a trap"
date: "2025-02-17T08:26:26+11:00"
abstract: "“Life can be simple. We choose to make it complex”."
year: "2025"
category: Thoughts
tag:
- design
- life
location: Sydney
---
Her [entire post](https://www.joanwestenberg.com/complexity-is-a-trap/) is worth a read, as always (<a href="https://www.joanwestenberg.com/rss/">RSS feed here</a>). But these lines in particular are spot on:

> Mastery isn’t adding layers. It’s peeling them away until only the essential remains.
> 
> Life can be simple. We choose to make it complex. The art is knowing what to keep.
