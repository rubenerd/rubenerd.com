---
title: "Tiny KVMs for one-off colocation"
date: "2025-01-11T09:02:18+11:00"
abstract: "Listing the PiKVM, NanoKVM, Mini-KVM, JetKVM, and pets (cough)."
year: "2025"
category: Hardware
tag:
- data-centres
- homelab
- kvm
location: Sydney
---
It's been fun seeing how many options there are for one-off KVMs thesedays. These devices let you get out of band console access to servers (or desktops) without paying the proprietary OOB tax. I can also see them useful for remote controlling my homelab boxes without having to plug in a monitor for the boot process.

These seem to be the most common/talked about in the last year. Let me know if you know of others!

* [PiKVM](https://pikvm.org/): A hat that turns your Raspberry Pi into an IP KVM. You can assemble your own, or they offer versions with metal cases.

* [Sipeed NanoKVM](https://github.com/sipeed/NanoKVM): A RISC-V based IP KVM in an tiny package. The Full version comes with an enclosure and eInk display.

* [Openterface Mini-KVM](https://www.crowdsupply.com/techxartisan/openterface-mini-kvm): A cute USB KVM that even has audio capture. Definitely the most capable of any option I've seen for local control of a headless box.

* [JetKVM](https://jetkvm.com/): The slickest I’ve seen; people with industrial design experience made this. The screen is a nice touch.

* [A pet](https://en.wikipedia.org/wiki/Dog "Wikipedia article on dogs"): I’ve been told that some pets can be trained to walk over to a keyboard in another room and step on it with sufficient accuracy to issue commands. I have yet to try this.


