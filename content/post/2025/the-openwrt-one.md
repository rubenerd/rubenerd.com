---
title: "The OpenWrt One, and NameTLAs"
date: "2025-01-18T14:17:20+11:00"
abstract: "OpenWrt has their own hardware now."
year: "2025"
category: Hardware
tag:
- networking
location: Sydney
---
The news of this slipped me by last year, but I just saw [OpenWrt has their own hardware now](https://openwrt.org/toh/openwrt/one), based on the BananaPi:

> OpenWrt One is based on the MediaTek Filogic 820 SoC and has WiFi 6, dual-band, 3×3/2×2, 1x 2.5Gbit WAN, 1x 1Gbit LAN, 1GB DDR4 RAM, 256 MiB NAND, 16 MiB NOR (for recovery), M.2 SSD, USB-C Serial console and USB 2.0. 

You can buy the bare board, or a BananaPi-style industrial blue case.

<figure><p><img src="https://rubenerd.com/files/2025/openwrt_one_top@1x.jpg" alt="Top-down view of the OpenWrt One, showing its blue PCB." srcset="https://rubenerd.com/files/2025/openwrt_one_top@2x.jpg 2x" style="width:500px;" /></p></figure>

As mentioned in the tech specs, it even has an M.2 slot, and MikroTik MikroBUS. At least one of these places it ahead of the best Raspberry Pi, as does its external antenna support.

I've been after a small box I can configure and pass onto friends and family who aren't technical, and are still running the garbage, insecure routers their service providers gave them in 2012. This might be it? I might to get one to try.

As an aside, I still mistype OpenWrt as OpenWRT. I spend too much time running FreeBSD, NetBSD, and OpenZFS, evidently. Hey, I could start calling this blog RubenERD!

That reminds me, I'd also love to see if I could get a small box running FreeBSD or NetBSD for a wireless access point and router. I know `ipf(8)` and FreeBSD's flavour of `pf(8)` more than anything else, and just enough FreeBSD `ipfw(8)` to be dangerous.
