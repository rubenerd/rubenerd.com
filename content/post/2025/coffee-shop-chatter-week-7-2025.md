---
title: "Coffee shop chatter, week 7 2025"
date: "2025-02-16T09:58:32+11:00"
abstract: "“Metallic gold beige! But I could do without the wheels.”"
year: "2025"
category: Thoughts
tag:
- coffee-shop-chatter
location: Sydney
---
Some fun random things I overheard this morning:

* These phones don't make sense anymore. I'm barely 30, and I don't understand what half this crap does.

* It's like, metallic goldy! But also beige. Beigy! Metallic gold beige! But I could do without the wheels.

* **(A)**: I missed the train, but I caught the next one. **(2)**: What did you catch it with? **(A)**: Mate, you're awful.

* Can you make one, but larger than large? Does that make sense?
