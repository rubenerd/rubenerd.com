---
title: "Right to repair farm equipment"
date: "2025-02-01T10:00:23+11:00"
abstract: "A report in The Guardian this week about equipment in Australia being increasingly locked down"
year: "2025"
category: Hardware
tag:
- drm
- news
location: Sydney
---
You know how so many things, from modern laptops and phones are sealed boxes where swapping a battery can void a warranty? It wouldn't surprise you to know this is happening at a larger scale as well, from cars to factory equipment.

[Mandy McKeesick wrote a great article](https://www.theguardian.com/australia-news/2025/jan/26/australia-farmers-right-to-repair-laws-equipment "https://www.theguardian.com/australia-news/2025/jan/26/australia-farmers-right-to-repair-laws-equipment") in The Guardian about the pressure modern farmers are feeling not to repair their own machinery:

> As farms have moved into the digital age, machinery has become more sophisticated. Onboard computers are standard and the embedded software brings a wealth of productivity in the form of guidance systems, allowing pinpoint accuracy for spraying or sowing crops. [However, &hellip;] “once there is an operating system, you can’t just swap a part in and out because the system won’t accept it unless you have the diagnostic software,” she says.

> “With new machinery there is pressure on us not to do our own maintenance – even an oil change, and that’s not brain surgery,” Honner says. “I feel we’ve been dumbed-down.”

The article goes on to discuss that farmers are being discouraged from making modifications to this equipment, which then *own*. Likewise, authorised repair shops in places like Australia can be hundreds of kilometres away, which means even simple fixes can result in a piece of equipment being unusable for days or weeks. All so tractor manufacturers can sell software subscriptions.

There have been some positive steps in the US taken to address this at the state level, in no small part thanks to the tireless work of people like Louis Rossman. The ability to repair your equipment shouldn't be something for which you must be granted authorisation. It's yours, despite manufacturers wanting to convince us otherwise.

*(There's an ewaste angle here too, but that warrants its own post).*
