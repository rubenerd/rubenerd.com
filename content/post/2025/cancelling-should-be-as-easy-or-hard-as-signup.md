---
title: "Cancelling should be as easy (or hard) as registration"
date: "2025-02-04T15:58:24+11:00"
abstract: "I judge the process of cancelling a service against how easy it was to register in the first place."
year: "2025"
category: Internet
tag:
- business
location: Sydney
---
I judge the process of cancelling a service against how easy it was to register with the company in the first place. It's amazing how many companies fail when judged against this! Okay, it's not amazing at all, it's *entirely* routine. Which is frustrating.

If you can register online, you must be able to cancel online. If you can register without a call, you must be able to cancel without a call. If you can register without having a stilted and frustrating "conversation" with a chatbot where you're [trying to predict](https://rubenerd.com/anticipating-how-captchas-want-me-to-solve-them/ "Anticipating how CAPTCHAs want me to solve them") what answers they want, you must be able to cancel without one.

Some businesses I've talked with blanch at this idea, claiming its important that they confirm people **really do** want to cancel something. Great! Then the registration process should be equally rigorous, because you'd want to confirm people **really do** want to register. Nobody would want to think your business is acting hypocritically, after all.

We all get it; registrations attract money, and cancellations attract cost. Congratulations, *that's how business works.* Get out of the kitchen if you can't stand the heat, and all that.

This rant has been brought to you by *Rubenerd Insurance*. Register in one click, cancel in ten... after our chatbot. <span lang="jp">です</span>.
