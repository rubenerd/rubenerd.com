---
title: "The Tennant S960 Battery Ride-on Sweeper"
date: "2025-01-20T10:11:17+11:00"
abstract: "To celebrate 960 pages, like old times."
year: "2025"
category: Hardware
tag:
- pointless-milestone
location: Sydney
---
This blog just reached 960 pages of posts. At ten posts per page, that's even more than 960 posts. Neat.

I used to do this thing here where I'd look at the Tennant industrial cleaning site whenever I reached a pointless numeric milestone [like this](https://rubenerd.com/post-id-5700/). To celebrate like old times, I checked to see if Tennant had something for me. [Of course they did](https://www.tennantco.com/en_ap/1/machines/sweepers/product.s960.battery-ride-on-sweeper.stn8000801e011.html)!

> Experience a productive and efficient clean in one pass with a wide sweep path and dual side brushes on the S960 battery ride-on sweeper. Easy to use for all operators, the S960 is ideal for cleaning a variety of indoor and outdoor environments.

Here's a photo of the unit, complete with its seat and brushes. There's something about the tail light design I find exceptionally charming:

<figure><p><img src="https://rubenerd.com/files/2025/tennant-s960@1x.jpg" alt="Press photos of the S960." srcset="https://rubenerd.com/files/2025/tennant-s960@2x.jpg 2x" style="width:500px; height:400px;" /></p></figure>

