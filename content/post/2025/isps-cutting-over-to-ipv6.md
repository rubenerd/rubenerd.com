---
title: "Residential ISPs cutting over to IPv6"
date: "2025-01-27T06:45:25+11:00"
abstract: "I’m sick of the industry running fast and breaking things, irrespective of how many problems it will solve for you."
year: "2025"
category: Internet
tag:
- networking
location: Sydney
---
A family friend of mine woke up late last year to find nothing in their house was able to connect to the Internet. They were able to use their phones if they turned off Wi-Fi and fell back onto mobile networks, but anything going through their routers failed. This person doesn’t work in the industry, but I’d call them above average in technical and troubleshooting ability.

They did everything they're recommended to do in these circumstances, including doing ping tests to various sites, rebooting their router, checking the "WAN" port on their router was a solid green, and staring at the FTTN box down the street while cursing Malcolm Turnbull. Eventually they gave me a call, and we walked through what the issues might be.

I asked them to log into their router, which was still the usual `192.168.0.1` or similar. They had to use their password manager to fill in the credentials they'd changed from the default, which was encouraging. When they got to the landing page, they let out a "huh", and said "the IP address looks wrong". I asked if they had a static IP or were used to seeing a specific number, but instead they said "no, the address is much longer and has colons in it".

Unbeknownst to them, their ISP had cut them over to IPv6 overnight, and it had somehow borked their entire home network. *Turns out*, they were using their ISP's router more as a modem, and a downstream device was their router. They took that extra router out of the mix, plugged the machines directly into the ISP's router, and most of their hardware responded. The last step was to use DHCP on the last remaining devices they'd hardcoded with static IPv4 addresses years prior and forgot about.

After a day of faff, they were back online and happy. Well, as happy as someone would be after a day of faff.

🌲 🌲 🌲

This was a silly circumstance that shouldn't have happened.

Their ISP cutting them over to IPv6 without telling them was poor form, irrespective of how important or necessary it was. Fundamentally changing how a service is delivered wouldn't fly for any other utility, and it floors me that IT gets a free pass, again.

I love sweating the small stuff when it comes to standards and protocols, but it cannot be overstated just how little people in the real world **(A)** know or **(2)** care about it. IPv4 verses IPv6, RSS versus Atom, systemd versus OpenRC... they just want something that works in exchange for the money they've paid. Violate that, and it causes pain and frustration.

Maybe you might think they *should* know about these differences, just as every chef thinks people should know how to cook restaurant-grade food at home, every cobbler thinks everyone should know how to make shoes, and every mechanic thinks that repairing a car is simple. We can't expect people to be experts at everything. That's why people buy residential plans from an ISP, and don't become experts at BGP.

IPv6 might be the best example of [second-system effect](https://en.wikipedia.org/wiki/Second-system_effect) I've ever seen in my years working in this industry, but the alternatives (aka, carrier-grade NAT) are much worse for everyone. We've functionally run out of addresses, routing has long become painful, and it would be great to solve a bunch of these problems. We won't ever get rid of IPv4, but our tech stacks have supported IPv6 for years. We need to cut people over.

But doing so in a way that `fsck(8)s` with people? That's where I draw the line. I'm frankly sick of the industry running fast and breaking things, and leaving laypeople in their wake.

To be clear, I do empathise with the position of ISPs here. You will never be able to accommodate every end device with a transition. But that's where good comms comes in. Maybe you advise people that the move could have unintended side effects, and that not every device may work in its current configuration. Heck, you could also take the opportunity to talk about the benefits of running IPv6, or offer to send a tech out to fix thorny issues that result. Plan it, schedule it, and help people after the fact. This stuff isn't hard.

In the paraphrased words of Infosec Elvis Presley, a little less action and a little more conversation, please.
