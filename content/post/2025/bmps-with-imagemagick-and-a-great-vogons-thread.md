---
title: "BMPs with ImageMagick, and a great Vogons thread"
date: "2025-03-07T06:12:26+10:00"
year: "2025"
category: Software
tag:
- forums
- graphics
- imagemagick
- retrocomputing
location: Sydney
---
Zerker asked on Vogons [how to create BMP images with ImageMagick](https://www.vogons.org/viewtopic.php?t=47082) that would be compatible with old Windows. This is a summary of the thread, with words in **bold** directly related to the question, and *italics* for semi-related.

1. **OP asks question**
2. Another tool
3. Spelling
4. Spelling
5. Spelling, another tool
6. Trivia
7. Nostalgia
8. Nostalgia
9. *Encoding*
10. *Encoding*
11. **OP answered his own question**

Vogons have some incredible resources, and have helped me out more times than I can count. Here comes the proverbial posterior prognostication: *but...* sometimes you get threads like that. Heck, a bunch of my own posts are probably like that (though thesedays I do try and include a tl;dr).

Hats off to Zerker for taking the time to answer his own question; it's one of the top search results for that specific problem. If you're interested in the syntax:

    $ magick image.jpg BMP3:image.bmp
