---
title: "The ISO 639-1 language code for Japanese is ja"
date: "2025-03-09T18:01:58+11:00"
abstract: "ばか!"
year: "2025"
category: Internet
tag:
- errors
- html
- languages
- xml
location: Sydney
---
I've got a longer post about mixed-language support in HTML and XML documents pending, but in the meantime I realised I've been defining Japanese wrong for probably *years*.

<figure><p><img src="https://rubenerd.com/files/2025/arashiyama-sign@1x.jpg" alt="A beautiful part of Arashiyama in Kyoto, taken in 2023." srcset="https://rubenerd.com/files/2025/arashiyama-sign@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

This was the output from the [W3C Validator](https://validator.w3.org/) on a test document I uploaded:

> Bad value `jp` for attribute lang on element `span`: The language subtag `jp` is not a valid ISO language part of a language tag.

Yup, turns out the correct attribute is `ja`. Though in my defence, is a phrase with four words. <span lang="ja">ばか!</span>

I probably have this littered over hundreds of posts and files going back twenty years. Me thinks I'll need to say hello to my good friend `sed(1)` to batch fix these.

*Update: I asked Clara if she'd been getting this right, and she said "oh yeah, it's always been `ja`". Because of course she knew.*
