---
title: "Australia, Singapore, and the US"
date: "2025-03-04T15:32:37+11:00"
abstract: "Thinking out loud about where things are headed. Feel free to skip."
year: "2025"
category: Thoughts
tag:
- australia
- politics
- singapore
location: Sydney
---
I try not to talk too much about politics here&mdash;at least directly&mdash;because I get the impression many of you read my sillyness for a bit of a break. I completely understand if you don't need this in your life right now, so feel free to skip this one.

&hellip;

Australia and Singapore couldn't be further apart in many respects, yet are tied together by several threads of history. Both were British colonial outposts during the living memory of some of their citizens. Both inherited English as their administrative language, have parliamentary systems of government, are members of the Commonwealth of Nations, and maintain high commissioners in lieu of ambassadors in each others' countries. Both fought in World War II on the side of the Allies (in Singapore's case, the war came to them). There's a wide diaspora of Singaporeans in Australia, and lots of Australians live in Singapore (including me at one point!). Our defence ministers and prime ministers convene annually for a security conference. Australia and Singapore share military training facilities. And so on.

Both also pivoted from the UK to the US in the mid-20th century, though this manifested in different ways.

Singapore has been a member of the *Non-Aligned Movement* since 1970, but American warships regularly dock in the Port of Singapore and carry out shared drills. Former prime minister Lee Kuan Yew, widely seen as the father of modern Singapore, claimed the US was a stabilising force in the region, and welcome its influence. Singapore is a common base for many American multinational companies, a trend that has only accelerated with the movements in Hong Kong.

Australia is even more overtly part of the US sphere of influence. It is a member of the *Five Eyes* security arrangement, the *ANZUS* security treaty alongside New Zealand, and most recently *AUKUS* alongside the United Kingdom. Australia hosts American military bases, and jointly operates the Pine Gap signals intelligence station. Australia has sent troops into American-lead boondoggles that even Canada didn't, such as into Iraq and Vietnam. We were the receiving station for the first television pictures from Apollo 11!

&hellip;

My Canadian friends have this great line about feeling as though they're sleeping next to an elephant. We have a bit of a distance, but we still jump when the US sneezes. This doesn't even touch on the country's soft power. As Sting and Shaggy sang in their unlikely *44/876* album, we wear American jeans, and so on.

I've been very open here about my love of the US. I had the privilege and joy of working from there for a short period, and count dozens of Americans as my friends, including many of you reading this right now. NYC is my favourite city outside Asia, and I'd love nothing more than to hop on an Amtrak in Chicago and wander the country. America invented jazz&mdash;the world's best musical artform&mdash;its melting pot has resulted in some incredible food and culture, and I'm typing on a device theoriesed, designed, and invented by some bright American sparks, many of whom immigrated lured by the promise of the USA.

Like Europe, we came to assume that while there were frustrations at times, our association with the US was beneficial. That it might go through rough patches, but we'll always come back together again.

That calculus has now changed.

&hellip;

The news in the last week or two out of the North Atlantic is chilling. The current US administration hasn't shaken up established norms as much as taken a torch to them. With a meeting that could best be described as an ambush, a series of incoherent policy decisions, and utter rubbish parroted by someone willfully ignorant or duped, Europeans have been left wondering if anything from bilateral trade to NATO means anything. And nowhere is this more acutely felt than Ukraine, and in the Middle East.

A friend on Mastodon asked me recently what the perception is over here. Opinions wax and wane largely based on who the president is at any given time. But this time is different. The Singaporean government has been open with their frustrations for the first time I can remember, and Australia's prime minister has been dancing around the issue. But like our friends up north, we're all starting to ask some fundamental questions. Foundations we thought were rock solid now seem dubious. If a large country in our region were to attack either of us, or another place close to us, could we depend on the US to assist? Do the mutual treaties we have mean anything? The answer feels increasingly like a no.

What does that mean then? What do we do? Who do we call?

I don't know what the answer is. I don't think anyone does. And if or when the current US administration is consigned to the "anals of history" (to quote a former conservative Australian MP), could that trust be rebuilt again this time?

I'm sorry, I feel like I should end this with a pithy or witty remark that neatly ties together all these thoughts. But nothing is coming up.

I guess if there's one positive thing out of all of this, is a phrase with twelve words.
