---
title: "The 1988 Kenwood KX-47C was a great budget deck"
date: "2025-02-19T11:34:17+11:00"
abstract: "It’s simple and does everything we want. It’s wonderful."
year: "2025"
category: Hardware
tag:
- 1980s
- kenwood
- music
- hifi
- reviews
location: Sydney
---
I'll admit, I had three ulterior motives when [posting that quote](https://rubenerd.com/joan-westenberg-on-complexity/) from Joan Westenberg yesterday about complexity. **(1)** I wanted to [promote her blog](https://www.joanwestenberg.com/) which I find massively valuable. **(B)** I wanted to prove to a troll that quoting people is perfectly reasonable. And **(三)**, I wanted to have something to reference when referring to complexity. Because I'm working towards reducing it in my life where I can.

Case in point, Clara and I bought this beautiful 1988 Kenwood KX-47C tape deck from eBay last year for less than a grocery bill, and it's not complex... *at all!* You might even say it's a minimum-viable deck. And we absolutely **love it**.

<figure><p><img src="https://rubenerd.com/files/2025/kenwood-deck@1x.jpg" alt="Photo of the deck among some of our other components." srcset="https://rubenerd.com/files/2025/kenwood-deck@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

As an aside, it's **so difficult** to take photos of hi-fi gear with lots of glossy panels. It's distracting, hard to focus, gah! Maybe we need a slightly larger lightbox for electronics. But I digress.


### What it doesn't have

It might seem odd to write a review (of sorts) by talking about what something *doesn't* have, but let's run down the list:

* There's no auto reverse. You flip the tape manually when it reaches the end, *like a Polariod picture*. Wait, I'm mixing my tapes and metaphors again.

* It doesn't have tape detection tabs which interface with the top of the cassette. You have to select whether you're using ferric, chrome, or metal tapes with buttons.

* There's no display. There's a strip of LEDs to show the Left and Right channel levels, and a mechanical tape counter you must reset yourself each time you start a new tape. That's it.

* There's no quartz lock, so you have to adjust it yourself with a tone tape, and maintain it over time.

* There are no soft-touch keys. The controls engage mechanically with the deck, albeit with some of the most satisfying clicks and tactile feel of any hi-fi component I've ever used. The texturing of the buttons felt almost [*Olympus*-esque](https://rubenerd.com/the-om-system-om-3-is-the-camera-of-my-dreams/ "The OM SYSTEM OM-3 is the camera of my dreams").

* It's also only a two-head mechanism, so making mix tapes is a bit more work because you can't live preview what's being recorded.


### What it does have

That was a&hellip; long list. Before you start questioning if this deck can do anything *at all*, rest assured it has these going for it:

* Excellent wow and flutter that are imperceptable to me (and *significantly* better than modern stuff based on those infamous Tanashin mechanisms, but that's hardly anything to brag about).

* Dolby B and C noise reduction (though no HX-Pro for recording). Chrome mix tapes recorded with Dolby C sound clear with no perceptible hiss between tracks at all. I've yet to break out my precious few metal tapes, but I might do some tests with those too at some point.

* This is more subjective, but pre-recorded tapes sound *slightly* warmer on this deck than others I've used in the last few years, which I love. Less... metallic? I'm not an audiophile, so it's hard to explain.

* Metal construction. Even though the chassis is mostly empty, it still feels solid and premium, like good Japanese gear does. 

* A tastefully arranged front panel that's simple, elegant, and timeless. I *love* the last years of the Shōwa period of electronic design, before everything gave way to bulbous styling and redundant flourishes of the mid-to-late 1990s. I respect people who love silver dials and analogue VU meters from the 1970s, but to me this is peak hi-fi design.

* Easy to access belts, motor speed adjustments, and balance trimpots for servicing; something you'll definitely appricate in older kit.

<figure><p><img src="https://rubenerd.com/files/2025/kenwood-kx-47c-docs@1x.jpg" alt="Photo from the original documentation." srcset="https://rubenerd.com/files/2025/kenwood-kx-47c-docs@2x.jpg 2x" style="width:500px;" /></p></figure>

*Photo from the original documentation, [courtesy of Hi-Fi Engine](https://www.hifiengine.com/manual_library/kenwood/kx-47c.shtml).*

### Conclusions

All this is to say, is a phrase with five words. Do the pros outweigh the cons here? Surely this deck is lacking key features that would make it useful and fun to use? Why get such a stripped-down deck, when you can ones with more features for not that much more on the second-hand market?

The features this deck lacks does result in a more "hands on" experience using it compared to those space-age Aiwa machines of the early 1980s, or something like the cockpit switch panel of a Namaichi. And don't get me wrong, I'd still *love* to have one of those at some point, or at least experience one first-hand in lieu of selling a kidney.

But in the least-surprising conclusion of all time, it's that simplicity we love. Extra features, *especially* ones on old hi-fi kit, are extra things to fail. We have a shelf of old decks that have dead displays, broken logic I don't yet posess the ability to diagnose properly, or other problems. The same goes for many pieces of kit, now that I think about it. The KX-47C is easy to service, it plays tapes without fuss, and that's it.

It also demonstrates something I think we've lost in modern consumer technolgy: something can be a budget device without being cheap. That might be worth its own post.
