---
title: "A Give Way sign, but shorter"
date: "2025-02-25T16:44:51+11:00"
abstract: "I've seen smaller Give Way signs used for cyclists, but one mounted to such a tiny pole is a new one!"
thumb: "https://rubenerd.com/files/2025/short-give-way@2x.jpg"
year: "2025"
category: Travel
tag:
- cycling
- sydney
location: Sydney
---
I've seen smaller *Give Way* signs used for cyclists, but this is the first time I've seen a regular-sized sign mounted to such a tiny pole! This was on a side street in [Wahroonga](https://en.wikipedia.org/wiki/Wahroonga) in northern Sydney:

<figure><p><img src="https://rubenerd.com/files/2025/short-give-way@1x.jpg" alt="A VERY short give way sign" srcset="https://rubenerd.com/files/2025/short-give-way@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

I wonder why it's like that?
