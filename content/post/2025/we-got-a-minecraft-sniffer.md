---
title: "We hatched a Minecraft sniffer!"
date: "2025-01-26T09:12:58+11:00"
abstract: "Only took us a year to brush up one of the eggs for these massive sniffy creatures."
thumb: "https://rubenerd.com/files/2025/snuffles@2x.jpg"
year: "2025"
category: Software
tag:
- games
- minecraft
location: Sydney
---
Clara's and my Minecraft world had its fourth birthday last October, but we still haven't found a single mangrove biome, nor had we ever hatched a [sniffer](https://minecraft.wiki/w/Sniffer). This changed last night!

<figure><p><img src="https://rubenerd.com/files/2025/sniffer.gif" alt="A Minecraft sniffer sniffing" style="width:240px; height:240px;" /></p></figure>

Sniffers are friendly, lumbering critters that were first announced in 2022, and introduced in version 1.20 of the game. They make an adorable sniffing motion with their noses&mdash;hence the name&mdash;and have six legs for extra legging. They can also dig up torchflower seeds and pitcher plant pods.

Sniffers are the perfect example of how Minecraft starts as such as a simple LEGO-like game, but can get as complicated as you want it to be. To get a sniffer, one has to:

1. **Create a brush.** Obtain a feather, either from a chicken mob, or find one in a loot box somewhere. Chop down a tree or bamboo shoot to obtain sticks. Create a pick to mine cobblestone to make a furnace. Dig sufficiently far in a more recent biome to obtain copper ore, which you smelt in your aforementioned furnace. Create a crafting table, and use the feather, stick, and copper to craft a brush.

2. **Look for ocean ruins.** Craft a boat, and head into warm ocean biomes looking for underwater ruins. Ones generated on older versions won't spawn the correct blocks.

3. **Brush until you get a sniffer egg**. Make note of suspicious sand or gravel that has a subtly different texture to regular sand or gravel. Use your brush on these blocks until you brush one that gives you a Sniffer egg. *Clara and I were stuck on this step for more than a year.*

4. **Hatch the egg!** Place the sniffer egg on the ground, ideally a moss block to reduce the hatching time. Wait for several cracks, and you have a sniffer!

Here's our lovely new sniffer *Snuffles-kun* near the barn where he was hatched. He's adorable&mdash;yet massive!&mdash;and we hope to find him some friends soon.

<figure><p><img src="https://rubenerd.com/files/2025/snuffles@1x.jpg" alt="Our new sniffer Snuffles" srcset="https://rubenerd.com/files/2025/snuffles@2x.jpg 2x" style="width:500px;" /></p></figure>

