---
title: "People may want to join you, if you treat them well"
date: "2025-02-06T15:12:17+11:00"
abstract: "A rambling political post that’s been on my mind for a while."
year: "2025"
category: Thoughts
tag:
- politics
location: Sydney
---
I apologise in advance both for the topic, and my disjointed thoughts here. But it's something that's been knawing at me for years, and especially again in light of recent events.

My impression from reading a lot on history, and witnessing it first hand, is that push factors play into motivations and identity as much as pull factors.

Take Ukraine. My Ukranian friends have blood relations and deep East Slavic history with their Russian and Belarusian families across their shared border. But the unprovoked and unjustified rain of death brought on by a little man in the Kremlin ensures that any kind of cooperation between them is all but impossible, and will likely remain so for at least my lifetime. Nice one Putin, you fuckwit.

My Scottish friends and relatives have often talked about Brexit being as much a referendum on the concept and identity of Britishness as it was a decision to leave or remain within Europe. When your southerly neighbour keeps electing Tory politicians who regard Scotland as little more than a distant oil field, it's understandable why they'd see themselves more as European than British.

A quirk of the Australian constitution is that it grants New Zealand the option to join Australia as a state. In some cases, it hasn't seemed far off. My parents used to speak of a time where you could cross the Tasman without a passport, live indefinitely in each place, and even receive social security. Had this sense of cooperation and camaraderie continued, it could have even been feasible that a larger state (whatever it would end up being called) would exist. The current political climate doesn't make that likely.

In Malaysia, Sabah and Sarawak were admitted to the Federation at the behest of the British, though there's still an ongoing sense of resentment that the Peninsula (where Kuala Lumpur is) ignores them, or focuses their money attention away from them.

🌻

Am I making a point here? I thought I was, but I'm not sure.

I suppose the galaxy brain thought here is that borders and governments are arbitrary constructs, and all of this is an academic exercise. But still, imagine if we looked out for each other, rather than being adversarial. Who knows, **people may want to join with other people if they're treated well.**

