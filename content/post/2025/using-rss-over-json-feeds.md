---
title: "Using RSS over JSON Feeds?"
date: "2025-01-14T08:37:59+11:00"
abstract: "I’d say they're functionally equivalent, though I think the reasons JSON Feed has seen such limited adoption is telling."
year: "2025"
category: Internet
tag:
- json-feed
- rss
location: Sydney
---
[Robb Knight asks](https://rknight.me/notes/202501131109/)\:

> Ignoring podcasts, which has to be RSS, is there an argument for using RSS/atom over JSON feeds?

You can probably implement either. Most modern aggregators support both. As long as you publish it in such a way that aggregators can find them, you're golden. Or platinum; I tend to find gold stuff gaudy.

But there's something else implicit in the question. The framing suggests that [JSON Feeds](https://www.jsonfeed.org/) should be the default (I don't think Robb would argue with this), and therefore one would need a sufficient reason to substitute RSS or Atom. What we find instead is that RSS rules the roost, and JSON Feed hasn't seen wide adoption since it was introduced in 2017. Eight years is an eternity in Internet time. So what gives?

I don't know for sure, but I have patented *Rubenerd Theories!*

* JSON Feed suffers the [XKCD Standards Effect](https://xkcd.com/927/). Every blog has RSS support, even those that implement JSON Feed. I’ve yet to encounter the reverse. If JSON feed were offering anything more than an alternative serialisation format, maybe it’d be more compelling. But RSS does the job, and adding another standard didn't offer much.

* JSON Feed was mostly a redundant format. [As I wrote in 2017](https://rubenerd.com/rss-json-feed-and-json-in-rss/), if the issue with RSS was XML, you could directly serialise RSS with JSON. JSON Feed was entirely new, which requires you to reimplement everything to do... what you can already do with an existing tool. Meh.

* JSON Feed solved a problem for developers, not users. People who write material that end up in feeds, and the people who read those feeds, couldn't give a toss what the format is written in. Those of us in the industry forget this at our peril, every single time.

* "RSS" isn't a great name, but it's catchier than "JSON Feed". Plain "JSON" encompasses more than a periodical syndication format. JSON Feed sounds like a committee name. Microsoft JSON Feed for Foundation Syndication Services. Okay I'm taking the piss here... a bit.

To be clear though, there’s room in this space for innovation. I suppose some of the Fediverse is doing just that. RSS won’t be around forever, but I suspect its broad replacement will be doing more than just changing the payload syntax. It’ll be entirely new mechanism for distributed sharing.

In the meantime, you can grab the [RSS 2.0 spec](https://www.rssboard.org/rss-specification), or the [W3C Atom spec](https://validator.w3.org/feed/docs/atom.html) for syntax and examples, and have something that can interoperate. We even have namespaces for when we need to extend the spec (maybe JSON can be similarly extended, I'm not sure).

The fact RSS and Atom have stood the test of time and not succumb to [Second-Systems Syndrome](https://en.wikipedia.org/wiki/Second-system_effect) is a feature not a bug, and one for which I continue to be grateful. Their resiliency gives me hope that the Open Web is still feasible, even in spite of all the attempts to quash it.
