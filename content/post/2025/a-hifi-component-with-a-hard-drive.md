---
title: "Playing digital audio files on a Hi-Fi?"
date: "2025-01-24T09:10:32+11:00"
abstract: "Clara’s and my ideal Hi-Fi addition would be a device with local storage to play digital files."
year: "2025"
category: Hardware
tag:
- hi-fi
- music
location: Sydney
---
Clara and I have been building up our dream Hi-Fi system over the last five or so years, mostly to play physical formats like vinyl, CDs, audio cassettes, and MiniDiscs. But we both grew up in the 1990s and 2000s, meaning we have extensive collections of offline music in MP3, AAC, and to a lesser extent FLAC and ALAC. We'd love to play them on our Hi-Fi system too. But how?

Our solution thus far has been to burn audio files onto CD-RWs. Our Sony DVP-NC625 CD/DVD changer supports MP3 data CDs (though curiously not data DVDs). We group albums by genre and mood, burn them to disc, set it to random, and it works a treat. The downside is selecting individual albums is tedious, because the screen only shows track and folder *numbers*, not names. It also means burning and reburning CDs.

The rational thing to do would be to get a Bluetooth receiver, or use Apple AirPlay from our laptops to the Apple TV. *But what part of the above description sounded rational?* Screw that, we want to have fun! This means getting a dedicated Hi-Fi box of some description that we can walk up to, select an album, and play it. Think of a cassette deck, but with an internal hard drive or SSD.

Matt from *Techmoan* [reviewed the Sony HAP S1](https://www.youtube.com/watch?v=yNwB50bN0Do) music Hard Drive system back 2014 and found it decent:

<figure><p><a target="_BLANK" href="https://www.youtube.com/watch?v=yNwB50bN0Do" title="Play Sony HAP S1 REVIEW - Using A Hi-Res Audio player to resurrect my ripped CDs (Part 2)"><img src="https://rubenerd.com/files/2025/yt-yNwB50bN0Do@1x.jpg" srcset="https://rubenerd.com/files/2025/yt-yNwB50bN0Do@1x.jpg 1x, https://rubenerd.com/files/2025/yt-yNwB50bN0Do@2x.jpg 2x" alt="Play Sony HAP S1 REVIEW - Using A Hi-Res Audio player to resurrect my ripped CDs (Part 2)" style="width:500px;height:281px;" /></a></p></figure>

More recently, *Mend it Mark* began a repair of a Sony GigaJuke, which included a CD-ROM and IDE hard drive for ripping and playing audio CDs:

<figure><p><a target="_BLANK" href="https://www.youtube.com/watch?v=c4PvnH36EpU" title="Play The GIGA JUKE is dead."><img src="https://rubenerd.com/files/2025/yt-c4PvnH36EpU@1x.jpg" srcset="https://rubenerd.com/files/2025/yt-c4PvnH36EpU@1x.jpg 1x, https://rubenerd.com/files/2025/yt-c4PvnH36EpU@2x.jpg 2x" alt="Play The GIGA JUKE is dead." style="width:500px;height:281px;" /></a></p></figure>

I guess we're after something similar to this that can play a local store of music, that looks like a piece of Hi-Fi kit, and is decent quality. We'd rather pay extra for something older than some new but cheap tat (wow, Mat and Mark are rubbing off on me). But I'm not even sure where to start looking. Is it a digital jukebox? A "HiRes Player?" I suppose another alternative is to get one of those computer cases that vaguely looks like Hi-Fi gear, and wire in a small screen.

Stay *tuned* while we do some exploring.
