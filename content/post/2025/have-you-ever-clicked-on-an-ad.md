---
title: "Have you ever clicked on an ad?"
date: "2025-03-02T09:02:29+11:00"
abstract: "I did, back in the day."
year: "2025"
category: Internet
tag:
- advertising
- security
- shopping
location: Sydney
---
[@float13 asked on Mastodon](https://hackers.town/@float13/114085116443303926), and I instinctively reached for the No option. But then I thought about it, and realised I had.

I clicked banner ads as a kid *constantly*. This was back in the day when ads were simple animated GIFs that a webdev would put on their page. Heck, I was so silly, I even collected the things!

<figure><p><img src="https://rubenerd.com/files/2025/thinkpad.gif" alt="An ad for a Thinkpad" style="width:468px; height:60px; image-rendering: pixelated;" /></p></figure>

*(That image might not load if you have an ad-blocker. It's an old <a href="https://rubenerd.com/files/2025/thinkpad.gif">IBM Pentium II ThinkPad</a>, which I wanted so badly as a kid).*

I'm not sure what it said about my childhood or my attitudes at the time, but I remember clicking them out of aspiration. I couln't afford a fancy new Pentium 3 desktop, or an iMac, or the latest PalmPilot, but that didn't stop me from clicking the banner to learn more and add its specifications to my massive childhood spreadsheet of features!

Modern adtech is hostile and compromises security, so I don't click on them even if they manage to slip through my [FBI-recommended plugins](https://web.archive.org/web/20250219132811/https://www.ic3.gov/PSA/2022/PSA221221).
