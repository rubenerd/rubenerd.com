---
title: "Useful dreams"
date: "2025-01-29T20:51:00+11:00"
abstract: "A study in Denmark found a majority of people have them. They're great!"
year: "2025"
category: Thoughts
tag:
- health
- psychology
location: Sydney
---
*This was a draft post from 2021. Not sure why I didn't post it.*

Douglas Heingartner shared the findings of a Danish research paper in [Psych News Daily](https://www.psychnewsdaily.com/study-62-of-people-report-having-useful-dreams-and-9-even-use-dreams-to-make-important-life-decisions/) which suggested a majority of respondents have useful dreams:

> The results showed that 62% of the participants said they’d had dreams that were of help, or of good use. About 56% said they’d had dreams that influenced their opinions about others, and 54% had had dreams that changed their behavior. [..]
>
> Among the 62% of respondents who reported having had helpful dreams, the most frequently-cited area of help was in assisting with creative tasks. This was followed close behind by emotional problem-solving, with “providing personal insight” coming in third. [..]
> 
> The findings of this study represent “huge potential for a major part of the general population,” the authors write, “in terms of using their nightly dreams for solving waking life problems to a much greater extent.”

I'd score myself among those respondents too, but not even for creative reasons. I used to solve so many programming problems in my sleep when I was a full-time developer; not necessarily in a dream state, but I'd wake up with that euphoric *Eureka!* feeling all the time. I also frequent an island cafe with my late mum when I need life advice and guidance.

I'd love to learn more about the subconscious; I'll bet there's a whole world there waiting to be explored and understood further.
