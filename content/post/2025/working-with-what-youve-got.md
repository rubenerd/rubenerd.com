---
title: "Reverse sunk cost, and working with what you have"
date: "2025-01-14T07:52:54+11:00"
abstract: "Not wanting to invest in things because you’ll replace them just makes life frustrating in the interim!"
year: "2025"
category: Thoughts
tag:
- philosophy
location: Sydney
---
Every morning, Clara and I walk out onto the balcony, and we see one of the XPTs fly past. This is a mid-speed interstate train operated by the NSW state government that [Clara and I took to Melbourne last March](https://rubenerd.com/taking-the-xpt-from-sydney-to-melbourne/ "Taking the XPT from Sydney to Melbourne"). It's fun waving at some of the people, and having them wave back! This photo from last year shows me in one of my awkward nerd poses living the dream with an XPT at Sydney Central.

<figure><p><img src="https://rubenerd.com/files/2024/xpt-front-nerd@1x.jpg" alt="Photo of an awkward person standing alongside an XPT power car in Sydney Central" srcset="https://rubenerd.com/files/2024/xpt-front-nerd@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

Alas, much like my choice of fashion, it's looking... a bit tired. The carriages are great, but some of the poor locomotives look like they need a lick of paint and a wash; the smoky exhaust and noise hints that they need a tuneup or a bit of an engine overhaul too. When the department was renamed to NSW TrainLink, the XPT fleet didn't get the new colour scheme or decals either, instead the old CountryLink logos were painted over without a replacement.

And I suspect I know why. The fleet that was [supposed to replace the XPTs](https://aboutregional.com.au/huge-demand-for-rail-but-new-trains-face-long-delay/413653/ "About Regional: Huge demand for rail but new trains face long delay") has been delayed multiple times, including but not limited to challenges around Covid. This has pushed the XPT into operating longer than expected.

I dub this a classic *reverse sunk cost* fallacy. The government doesn't want to be seen investing into something they're going to retire soon (aka, the sunk cost fallacy), so they only give it what's necessary to continue operating. But for how long?

I've since realised I do this myself. I have multiple pieces of kit, from computers and cameras, to file systems and FreeBSD jails, that I know are on the verge of being replaced. I'm reticent to sink more money&mdash;or more importantly, time&mdash;into them, because they're a sunk cost. I know that I'll need to replace this stuff sooner rather than later, but without a firm timeline or budget in place, that could be in a few weeks, or next year. Who knows? My stuff is like those XPT trains.

But like all deferred maintenance, in practical terms this just makes using that kit frustrating in the interim! My use of them in the *present* shouldn't be impacted by a hypothetical future where they're replaced. If there's an issue that needs fixing, I should fix them.

There's an old truism in IT that there's nothing more permanent than a "temporary fix"! I'd tack on a corollary: nobody ever wants to improve temporary fixes because they're seen as temporary, in spite of that truism. So it is with my gear.

I'm sure there comes a point on the graph of life where the time and money I'd need to spend fixing something really *does* cost more than what is valid for something you expect to replace. I'm not sure whether that's the cost of the New Thing, or something in between.

I guess the galaxy brain thought here is that *everything* is temporary, it's just a matter of time. In which case, shouldn't I be doing what I can to make the most of what I currently have, even if it's getting replaced at some point?
