---
title: "Fauna’s farewell stream #faunline"
date: "2025-01-04T10:21:51+11:00"
abstract: "💚"
year: "2025"
category: Anime
tag:
- ceres-fauna
- goodbye
- hololove
location: Sydney
---
It still seems like yesterday we were [watching her debut](https://rubenerd.com/ceres-faunas-debut-stream/ "Ceres Fauna’s debut stream #faunline"), then we were [saying goodbye](https://rubenerd.com/goodbye-to-ceres-fauna/). I've loved everyone in Hololive, but Fauna was up there with Ina, Watson, Reine, and Liz. I still can't believe I won't be able to catch a comfy stream again before work.

Thanks so much Fauna, and all the best. *[Let Me Stay Here](https://rubenerd.com/ceres-fauna-let-me-stay-here/)* may be on repeat for a while. 💚

<figure><p><img src="https://rubenerd.com/files/2025/goodbye-fauna-stream@1x.jpg" alt="Fauna’s goodbye stream" srcset="https://rubenerd.com/files/2025/goodbye-fauna-stream@2x.jpg 2x" style="width:500px;" /></p></figure>


