---
title: "Titles referring to “my stuff”, not just “stuff”"
date: "2025-01-10T15:21:55+11:00"
abstract: "Reframing titles to say I solved a problem for us, not necessarily for everyone."
year: "2025"
category: Software
tag:
- weblog
location: Sydney
---
This is a bit more of what my American friends would call *Inside Baseball*... which I assume would just more baseball. It's baseball all the way down! Wait, that's Terry Pratchet.

Jeff Geerling did a [retrospective](https://www.youtube.com/watch?v=oRtyC0mi5fs) last year where he talked&mdash;among other things&mdash;about clickbait titles. He said his video about [replacing the AppleTV with a Raspberry Pi](https://www.youtube.com/watch?v=3hFas54xFtg) was better received when he changed it to say he replaced **his** AppleTV with a Raspberry Pi. 

It's a subtle reframing, but it disarms a lot of what I call *edge-case feedback*. For example, I'll post about running OpenZFS to do something, only to be told that someone lives in the Arctic circle, and I failed to mention the drives need to be microwaved first or the frozen platters won't spin. Saying that **I'm** running OpenZFS to do something avoids this (and might also encourage me to work up the guts to contribute documentation to official projects instead, where it's more urgently needed).

I might give this reframing a try. Digital reframing is much easier; last time I tried reframing a canvas I just ended up tearing it. Wait, those were my scales.
