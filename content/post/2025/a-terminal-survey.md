---
title: "A terminal survey"
date: "2025-02-14T07:34:06+11:00"
abstract: "I did one of those surveys that ask how you use the terminal. Maybe there’s something interesting."
year: "2025"
category: Software
tag:
- bsd
- freebsd
- illumos
- linux
- netbsd
location: Sydney
---
I did one of those terminal surveys! As in, a survey about using terminals, not a survey that's terminal. Some of the answers are below. I didn't include duplicate questions, or ones where the answer is just a "yes".

**How long have you been using the terminal?**

: 21+ years (... yikes)

**Which shells do you use?**

: ksh, sh, csh (in that order).

**Do you use your system's default shell?**

: No, I changed it (except when running as root/toor).

**What OS do you use a Unix terminal on?**

: FreeBSD, Linux, macOS, NetBSD, illumos.

**What Terminal emulators do you use?**

: Terminal.app, Konsole, rxvt, Xfce Terminal.

**Do you use a terminal-based editor?**

: vim/neovim/vi, (also mg, and have been known to use mc).

**Do you customise your terminal's colour scheme?**

: Yes, I use a theme (admittedly I used to do much more).

**If your terminal get messed up, what do you do?**

: Run `stty sane` (though I have this aliased to `cls`, because DOS habits die hard).

**What terminal settings do you customise?**

: `PATH`, environment variables, alias, the prompt (I like having two lines), custom functions.

**Do you use job control?**

: No. (I used to, but now I only really use tmux and tabs).

**Do you manage your files using the terminal, or a GUI file manager?**

: Mostly GUI (I also use GUI file managers as media organisers. The terminal is useful for bulk processing, syncing directories, and remote uploads).

**Which of these environment variables have you set intentionally?**

: `PATH`, `MANPATH`, `EDITOR`, `VISUAL`, `LANG`, `LC_ALL`, `PAGER` (and `CLICOLOR` on FreeBSD).

**Do you use vi mode in your shell?**

: No (but I use vi-style keybindings elsewhere).

**How do you navigate files in less?**

: `j`/`k` to scroll (also spacebar to move down large areas).

**How do you use pipes?**

: `tee` to save output, `pv` to see progress, `xargs` to parallelise (I definitely need to get better at `xargs`, and `awk`).

**Do you use a terminal multiplexer?**

: `tmux`, `screen` (eventually want to move all to `tmux`, but muscle memory is a powerful thing).

**What's the most frustrating thing about using the terminal for you?**

: (Scripts and examples that assume you use `bash`, you have it installed, or that `sh` is an alias of `bash`).
