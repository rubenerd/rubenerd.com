---
title: "Being keen for new macOS releases"
date: "2025-03-11T07:41:44+11:00"
abstract: "I used to take my SLR to launch events! No longer."
thumb: "https://rubenerd.com/files/2025/sequoia-spam@2x.jpg"
year: "2025"
category: Software
tag:
- apple
- macos
- mac-os-x
location: Sydney
---
Both [@matdevdug](@https://c.im/@matdevdug/114137494259127155) and my own work Mac laptop reminded me of something recently. A mood, and a feeling, so far gone from my current state of mind that it feels like an age ago. *We used to look foward to Mac OS X releases!*

I literally used to take my SLR down to Apple retailers in Singapore to document each launch, as I [did here for Leopard in 2007](https://rubenerd.com/mac-os-x-leopard-launch-in-singapore/). Wait bummer, a bunch of those photos from that time period no longer work. I'd better fix that. When I was studying in Adelaide, I'd call up Apple in Crows Nest in Sydney to secure my preorder for the next release, with money that I should probably have been spending on trifling things like food.

New Mac OS X releases were a big deal, because you felt like you were *getting something*. Each release introduced a new feature that was legitimitely useful, or refined the UI in a meaningful way, or let us do something on our Mac we couldn't do when we bought it. It felt like, in the most literal sense, an upgrade. We'd chuckle at Apple telling Microsoft to "start their photocopiers" when a new release launch, much as Android blatenely aped iOS for many years. Windows was ubiquitous. The Mac was special.

I think macOS Yosemite was the last release I can remember by name and version number. Thesedays, my Macs spam me with these messages, and I feel either indifference or mild dread:

<figure><p><img src="https://rubenerd.com/files/2025/sequoia-spam@1x.jpg" alt="Screenshot from a prior version of macOS, asking to upgrade to Sequoia" srcset="https://rubenerd.com/files/2025/sequoia-spam@2x.jpg 2x" style="width:500px; height:270px;" /></p></figure>

I suppose part of this comes down to getting older, and having different expectations for what my Macs will need to do. Gone are the days where I'd spend all night tinkering and building software for my Apple machines. They're now for "work", which means I have precious time to be tinkering, upgrading, and potentially breaking something. Time spent doing this could be better allocated basically anywhere else.

My inclination now is to assume all new releases have features I don't need or care about. The few times I *have* checked out an info page, it's about AI or some other nonsense that [most of us don't care about or want](https://www.techradar.com/phones/new-survey-suggests-the-vast-majority-of-iphone-and-samsung-galaxy-users-find-ai-useless-and-to-be-honest-im-not-surprised "Tech Radar: New survey suggests the vast majority of iPhone and Samsung Galaxy users find AI useless – and I’m not surprised"). When there is a substantiative change, like System Preferences becoming System Settings, the [execution was so horrendus](https://lapcatsoftware.com/articles/SystemSettings.html "Jeff Johnson: Why Ventura System Settings is Bad") that I felt the *opposite* of imposter syndrome for my own designs for the first time in years. Wow, my designs suck, but at least I did a better job than that!

Truthfully though&mdash;as opposed to all those other times!?&mdash;my heart and mind left the platform many years ago for greener pastures. This was much a push as it was a pull; Fedora on the desktop makes a decent game machine, as does FreeBSD a workstation, and NetBSD a laptop OS. They now have features I miss when I go back to my Mac. Likewise, Tim Apple isn't the same as Apple Computer Inc, and especially so under the new administration. They lost their software quality mojo years ago, and I'm increasingly convinced they've since lost their soul. I don't relish in this; it sucks.

The Mac is still a *vastly* better platform than Windows for "work", and I feel nothing but relief every time I stroll by those PC laptops in electronics stores, with their flimsy plastic construction, terrible screens, and whatever a Copilot is. But it's still a bit sad that the high water mark for the Mac is now firmly in the past.

Upgrades are a disruptive but necessary evil to ensure one gets security patches, and another bank of switches to toggle off. That's it.
