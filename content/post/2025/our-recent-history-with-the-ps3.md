---
title: "Part two: Our (recent) history with the PlayStation 3"
date: "2025-01-09T16:10:53+11:00"
abstract: "A retrospective about our first PS3 console, and how it became our favourite."
year: "2025"
category: Hardware
tag:
- games
- playstation-3
- retrospectives
location: Sydney
---
This is part two in my shaggy dog story about the [PlayStation 3](https://rubenerd.com/tag/playstation-3/), one of my favourite ever consoles. [In part one](https://rubenerd.com/consoles-and-the-playstation-3/ "Consoles, and the PlayStation 3") I talked about my history (or lack thereof) with consoles as a kid, how I was first fascinated by the PS3 for its ability to run Folding@home and play Blu-ray discs, and how Clara and I ended up with a couple of them.

But how!? I'd say buckle up, but racing games don't require it. Though I'm sure someone has built an over-engineered racing harness chair. Probably not for the PS3 though. Respect if they have though. Wait, how did I get onto this?

...

Clara and I first moved in together in a microscopic studio apartment, so we could start saving for a home of our own (it only took a decade). That description of size was not hyperbole: the lounge area had an alcove barely big enough for a double bed, and the remaining apartment just fit a couch and two desks. The whole space, including kitchenette and bathroom, was barely larger than my parents' walk in wardrobe in the McMansion we lived in [during our time in KL](https://rubenerd.com/operation-move-to-malaysia/ "Operation move to Malaysia"), and smaller than any bedroom I'd ever had as a kid. I think we even had spice cupboards/pantries that rivalled it in size.

Speaking of wardrobes, we had no space for one, so we folded up our clothes in a chest of drawers that doubled as a TV stand. We made that place into a surprisingly comfortable little nest, but it did mean we had to be ruthlessly efficient with what we brought in. As everyone’s favourite chef Alton Brown would say, multitaskers were king.

<figure><p><img src="https://rubenerd.com/files/2025/ps3-super-slim@1x.jpg" alt="Photo of the PS3 Super Slim and controller, by Evan-Amos" srcset="https://rubenerd.com/files/2025/ps3-super-slim@2x.jpg 2x" style="width:500px" /></p></figure>

So when Clara and I saw a 2012 PlayStation 3 Super Slim at a second-hand electronics retailer for less than lunch, we looked through its list of capabilities and hit the proverbial `SELECT` button. Neither of us had ever used or played a PS3 before, but we’d both secretly been interested in it for a long time. And similar to how new gamers bought the original Wii specifically for its controllers, we bought into the prospect of having a PS3 Blu-ray player "with a free console attached".

It turned out to be an *awesome* decision. Despite being a five-year old redesign of an eight-year old console, we used it for Blu-rays, DVDs, USB keys, music CDs, YouTube, Plex, AnimeLab, and ABC iView (I think we even had SBS at one point). We even found a TV-style remote that presented itself as a game controller, so we could more easily browse. Years later we'd get an Apple TV again, but I think the official PS3 controllers and that remote were a far superior interface to the vague and frustrating Apple remote.

The Sony magic also worked on us. Now that we had this media centre device that could play games, we wondered... could we use it to play games? Who would have thought!?

<figure><p><img src="https://rubenerd.com/files/2025/ps3-games@1x.jpg" alt="A selection of PS3 games, as described below." srcset="https://rubenerd.com/files/2025/ps3-games@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

I'm a fan of being a generation or two behind when it comes to tech, and our experience with this PS3 reinforced that. Not paying the *First Movers Tax&trade;* means devices and software are much cheaper, more accessible on the second-hand or refurbished market, and are generally more stable from the extra testing and patches they've received. We went back to the same second-hand store and picked up a bunch of titles for peanuts. Clara latched onto some *Assassins Creed*, and I got copies of *Need for Speed*, my favourite racing franchise from the PC world when I was a kid.

Surprising nobody, fun games were fun! And for someone who hadn't played a 3D game since Windows XP, the graphics were vastly improved (though with that charm that comes from consoles that predate what I'd call the "photo-realism" of the ray-tracing era). It was great for relaxing and logging off the real computer, especially during those years at work when I was doing a lot of overtime. And the Criterion Games release of *Need for Speed: Most Wanted* might be one of the best racing games of all time.

Alas, after years of faithful service, the Super Slim didn't survive one of our subsequent moves. It was connected and worked fine for a time, but soon became a brick. Most worryingly, the internal hard drive couldn't even be recognised on another system, so I suspect it was a victim of a power surge or something more serious internally. We lost our game data, but are probably a bit wiser for it. As Mr Dunham of the Australian International School used to chime at the end of each class: *back up and pack up.*

Clara got an original PlayStation 4 from a colleague shortly after, and we bought an Apple TV for our lounge video requirements. But neither felt quite the same as that sprightly PS3 with its bubbly launch screen and graphics. I suppose your "first" console together will always be the most special. Last month though, we corrected this egregious state of affairs!

I was hoping this would be a three-part blog post series, given it's the PS3. Because what's 3 threes? If you say it with a Singaporean or Irish accent, it's a small forest. **HAH!** What that has to do with the PS3, and our subsequent purchase of a PS3 Slim, is absolutely nothing. *Stay tuned*.

*(First console photo by [Evan-Amos on Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Sony-PlayStation-3-4001B-wController-L.jpg). Second photo showing a random subset of our PS3 games was by me. The photo, not the games. I wrote text-based adventure games in Pascal and "visual" novels in VB6, shaddup).*
