---
title: "Every day is Coffee Cheese Day at our place"
date: "2025-02-12T08:37:14+11:00"
abstract: "Our AppleTV plays the same video, every time we turn it on. Why!?"
thumb: "https://rubenerd.com/files/2025/yt-W2cIOOG4ImE@2x.jpg"
year: "2025"
category: Media
tag:
- apple
- coffee
- james-hoffman
location: Sydney
---
Clara and I have an AppleTV, a curious device made by a company that seemingly cares little for it. This is fantastic news from a large IT company in 2025, because it means it has mostly escaped from being stuffed with new "features" nobody wants. This stands in contrast with their iTelephones, and whatever incidental, trifling other hardware the company makes, for which they used to be famed and respected.

Here's a new tool! Would you like to **Enable It**, or **Maybe Later**? What's that? You *never* want it? Nonsense! We'll ask again in six weeks. And then again, infinitum. You'll use our new feature either because you love it, or because we've worn you down from incessant questioning. It's called innovation! Ignore the fact the word *no* is contained in that larger, innovative word! It's really *in-maybe-later-ivation* if you think about it. But not too hard. Please. We have lines, and they need to go up.

Wow, that went a bit off the rails, even by my albeit nonsensical standards. Let's return to something approaching a normal blog post. Before we do that though, I'd like to remind everyone of something, that I may even remember at some point. Ah yes, brilliant, thank you Ruben. Genius!

...

Anyway, we tend to use our recently-refurbished PlayStation 3 or PlayStation 4 as our primary media playing devices and&hellip; pardon, my apologies, I'm supposed to say "consume content". Pardon me, I'm off to *consume some content*, then I'll *engorge myself* on foodstuffs, because civility and respect for artists, farmers, and chefs are for chumps. Chumps, damn you! We'll get to the foodstuffs again in a moment.

But sometimes we fire up the AppleTV in lieu of the PlayStations, especially if the controllers we use as glorified remote controls are low on juice, or the host system is performing a software update that Australian Internet gleefully delivers at a pace one might best describe as *lackadaisical*. I envy its apparent lack of stress in the face of what must be a gargantuan task, and wish to imbue its spirit when it comes to my own work. I can't even blame the NBN at this stage, because we're on another fibre network. Must be all the pipes.

Our AppleTV has many quirks, not least the fact the YouTube app for it is terrible. *Inexcusably* bad. Prior versions were so sensitive to the remote control, you'd scroll faster than a venture capitalist sprinting to a blockchain summit. This would lead you to *completely* miss the target you were aiming for, resulting in frustration and many inadvertent video starts. This was thankfully rectified, but not before the developers and/or their managers thinking it'd be delightful to list channels not in alphabetical order anymore, but completely shuffled in a way that makes no sense whatsoever. It's a treasure hunt, but for channels! Hope you're patient, *because you'll need to be.*

There are so many bugs in this software. You'll use Apple's bad remote to try and "like" the video, only for the video to fast-forward without warning. Though it's *impossible* to rewind or fast-forward in a deliberate fashion. You don't notice how bad it is until you use a PlayStation controller. Maybe that's the only hardware it was tested against. It's bad hardware, paired with software that isn't so much optimised as regurgitated. Out of order.

But those aren't the funniest issues. Lately the YouTube app on the AppleTV has stared playing **the same video**, **every time** it launches. It doesn't matter what we were watching before, or from whom, or whether the AppleTV came out of sleep or a cold start. **Every time** we start it, we're greeted by James Hoffman, and his video regarding ALDI's [Wensleydale coffee liqueur cheese](https://www.youtube.com/watch?v=W2cIOOG4ImE).

<figure><p><a target="_BLANK" href="https://www.youtube.com/watch?v=W2cIOOG4ImE" title="Play The Horror of Aldi’s Espresso Martini Cheese"><img src="https://rubenerd.com/files/2025/yt-W2cIOOG4ImE@1x.jpg" srcset="https://rubenerd.com/files/2025/yt-W2cIOOG4ImE@1x.jpg 1x, https://rubenerd.com/files/2025/yt-W2cIOOG4ImE@2x.jpg 2x" alt="Play The Horror of Aldi’s Espresso Martini Cheese" style="width:500px;height:281px;" /></a></p></figure>

It's&hellip; it's a sweaty cheese.

Now granted, this is one of my favourite videos of all time on the platform, and may represent peak Hoffman when it comes to his delivery and opinions. Our AppleTV is *exceedingly* lucky that it decided to bug out on something wonderful, otherwise it may have been consigned to being a "hockey puck device" in a more literal sense. Imagine if it started playing something dreadful every time.

It makes no sense whatsoever. But then, the AppleTV YouTube app hasn't made sense in a long time. It's only a hair above being a dumpster fire. It's only saving grace right now is that the video it's stuck playing is a grate one. *Get it... grated cheese*. See, look at what it's done to me. What it's now done to you, having now read this mess.


