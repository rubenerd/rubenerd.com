---
title: "Brain ABIs, and the allure (or not) of sleeping in"
date: "2025-01-19T08:23:10+11:00"
abstract: "I’m an early riser. I appreciate most normal people aren’t."
year: "2025"
category: Thoughts
tag:
- personal
- psychology
location: Sydney
---
I slept in yesterday. I'd had a rough Friday at work, and I wasn't feeling it when I got up at my usual time Saturday morning. I rolled over expecting to sleep for another half an hour, and woke up... at **10:30**.

*Great!* I hear the normal among you say in response. Sleeping in is a wonderful treat for days off, and certainly something many of you don't get to partake in as often as you'd want. Some of you have young children who get up when they want to, others have early commitments you'd desperately rather substitute for something commencing at a more civilised hour.

Sleeping in is one of those unassailable cultural touchstones. We have this image of the late riser, feeling relaxed and under no time pressure, sauntering into the kitchen in their pyjamas to have a late morning coffee, maybe some orange juice, and a slice of Vegemite toast, perhaps with or without Art Garfunkel. Life is grand. We're living the dream. *We got to sleep in*.

<figure><p><img src="https://rubenerd.com/files/2025/fate-for-breakfast@1x.jpg" alt="One of the many covers from Fate for Breakfast." srcset="https://rubenerd.com/files/2025/fate-for-breakfast@2x.jpg 2x" style="width:500px;" /></p></figure>

Alas, that's not how my brain works. Having inadvertently slept in, I spent most of my subsequent Saturday in a foggy, lethargic stupor, as though I *hadn't slept enough*. It took forever for my brain to turn on, and when it finally did, it was bed time. My head hit the pillow that night feeling tired, a bit irritable, and disappointed. I was granted another day on this beautiful planet, and I spent it feeling disconnected from my brain. Blech! **Blech I say!**

This happens every time I sleep in, and I tell myself *never again*. Sleeping in and I aren't compatible. We harbour no ill will towards each other; there's no restraining order required. We just implement different ABIs. We respect that about each other. I don't sleep in, and life is good.

You see, I'm one of those weirdos that likes going to bed early, and rising early in the morning. I have been my whole life. My sister and I used to joke that during school holidays, hers and my sleep patterns would drift further and further apart, until she was heading to bed around the time I was waking up. For those precious moments where we *were* awake at the same time were cause for celebration, and a reason to get some tasty food.

Mornings are wonderful. The air is probably the freshest it'll be all day; "smelling of Pears Soap" as Norman Lindsay wrote in *The Magic Pudding*. Most people are still asleep, so you get some quiet time free of distractions. In places like Singapore, the early morning is the coolest part of the day before the heat and humidity set in, making it perfect for long walks or hikes in the nature reserves.

Perhaps most critically, I also feel as though my brain only engages when it's woken up in the morning. I need that mental time to prepare for the day, but it's also more fundamental than that. If my brain doesn't have that tasty coffee, the fresh morning air, and some RSS, I feel unfulfilled, listless, and... lazy?

To be clear, this isn't exactly a negative revelation. Early risers are seen as being more studious, lumped in with those people who have "side hustles" or those who are "addicted to studying". Tell a recruiter that you like getting up early is akin to saying your biggest weakness is that you "work too hard". It's certainly convenient preferring early mornings when most of working society is configured to expect people turn up to jobs at that time.

Like most stereotypes though, I don't think it's *entirely* fair. I don't feel any more productive, happy, or useful than anyone who gets up later. In fact, the most intelligent people in my life would all probably get up at 14:00 if society let them. In the case of where I work, some of the most talented engineers have negotiated exactly that. 

Not me though. Give me early mornings. Well, that is, unless I've had a bad night or was forced to stay up late the previous day. Then I probably will need the sleep in. Or not, sometimes I find getting up at the same time is important. Maybe. It depends. Sleep is hard.
