---
title: "What do people do in coffee shops?"
date: "2025-02-25T08:31:05+11:00"
abstract: "More of you read that do work, which honestly sounds wonderful."
year: "2025"
category: Thoughts
tag:
- coffee
- health
- reading
location: Sydney
---
I've made no secret of the fact I love working from coffee shops. I don't know if it's the surroundings, or being an introvert who can be around people without having to interact with them, or the smell and taste of the coffee itself. It's magic, in the sense that it makes no sense but the result is great.

I asked some of you who I know are coffee enthusiasts about what *you* do in coffee shops late last year, and promptly forgot! Here are the results, aggregated by theme:

* Read a book, newspaper, magazine: **4**
* Work using a laptop or tablet: **3**
* I don't, I only get takeaway: **2**

Okay, a small sample size. But still interesting.

I might need to give the *reading* thing a try. I have a never-ending pile of work, but I also have a stack of books. The latter probably deserves some mental space and attention sometimes too. I'm on a bit of a Bill Bryson kick again, and those *Monogatari* light novels won't finish themselves.
