---
title: "The Flufi third-party Instagram photo browser"
date: "2025-03-09T09:59:11+11:00"
abstract: "Use this so you don’t need an Instagram login."
year: "2025"
category: Internet
tag:
- photos
- social-networks
location: Sydney
---
A lot of musicians and coffee shops I love host their material exclusively on Instagram. I used to have an account there, but I haven't felt compelled to give The Zuck any more of my attention.

A friend clued me into **[Flufi](https://flufi.me/)**, an alternative web interface to Instagram. You type the name of the handle, or a link to the image you need to view, and it will present it in-browser without requiring a login. *Huzzah!*

The real solution is to get the aforementioned creative people and comestible establishments off a closed, increasingly hostile platform. But in the meantime, for as long as the service remains an inextricable&mdash;though involuntary&mdash;part of my life, this is a helpful alternative.
