---
title: "Andreas, Tim, Michał, and I don’t need new machines"
date: "2025-01-17T20:24:02+11:00"
abstract: "Old stuff works fine. In fact, it might even work better. Quoting some blogging friends here on their ideas about older tech."
year: "2025"
category: Hardware
tag:
- apple
- shopping
- thinkpads
- whats-old-is-new
location: Sydney
---
Early posts on this blog&mdash;at least, the technical ones&mdash;were spent writing about my adventures getting my ideal graphical desktop going on FreeBSD and Fedora. This is as opposed to the non-technical posts, which tended to be written about adventures *not* involving getting my ideal graphical desktop going on FreeBSD and Fedora.

Thanks Ruben, once again, that was an excellent clarification. I know so much more now after you explained some posts are about something, and other posts are not. Your wit is matched only by redundant paragraphs you put in posts chiding yourself for writing nonsense. What's for dinner, by the way?

As a university student without much money, I'd gravitated towards second-hand ThinkPads. They were plentiful, shockingly affordable, felt great to type on, and *exuded* class. People had fancier laptops from other PC makers, and they looked worse than my ancient X40 or X61s. In fact I'd say Apple were the only other company making kit that looked half reasonable at the time, though their higher resell value limited access. ThinkPads were like the plague, they were everywhere. But in a good way. So, not like the plague.

This post is going great.

Fast forward a decade or so, and I still only buy refurbished kit. This is mostly directly from Apple, because I need Microsoft Office and a few other commercial software packages, and as bad as macOS is, it's leagues ahead of Windows... though that's setting a bar so low it may as well be used as a cross beam for a floor.

But I do still miss those older ThinkPads. I kinda want to get another one again. I think I derived more utility from that older kit than newer stuff.



This is where a post from Andreas over at 82MHz comes in, titled [I will never need to buy a new computer again](https://82mhz.net/posts/2025/01/i-will-never-need-to-buy-a-new-computer-again/)\:

> If you’re in your early thirties or older, you remember the breakneck pace at which computers were improving in the 90s and 2000s. If you brought home a shiny new 400MHz Pentium 2 system in 1998, it was literally outdated and replaced by a faster model with higher clock speed and more RAM/bigger harddrive by the time you had set it up and gotten familiar with it. But that’s not the case anymore, and it hasn’t been the case in a long time.
>
> I’m starting to feel that same itch again. I haven’t upgraded my computer in a while, so maybe it’s time for something new?
>
> But here’s the thing: I don’t need it. I don’t have a single usecase for which I would need this much processing power. In fact, I could still use that i5 from 2011 and it would do everything I want it to do perfectly fine. 

[Tim Chase responded](https://mastodon.bsd.cafe/@gumnos/113838443524696377)\:

>  My daily driver is a 14-year-old laptop and it still does pretty much everything I need. I've upgraded the RAM (4GB→10GB) and the drive (HDD→SSD), but it still runs like a champ.

And we round off with Michel, titled [I may never buy a new electronic device ever again](https://crys.site/blog/2025/i-may-never-buy-a-new-electronic-device/)\:

> I get that those devices are done. Audio peaked in mid 90s, TV sets have no new cool features (4k is a LOT), a microwave is a microwave. The decision makers think that we want “more”. Maybe we do? I certainly don’t. And since “better” is no longer an option, “more” means adding useless features and gathering as much data as an internet connection can transfer.
>
> I won’t be able to afford it now, but in few years where my 20 years old car breaks, down I will be able to pick something on the used marked and be happy with it.

I couldn't agree more. Tech hasn't moved meaningfully enough for me for a while now, which is bittersweet. I do miss the same joy from getting a massive upgrade. As I always say here, we use these machines *every day of our lives*, so it makes sense we'd want something nice.

But barring exceptional circumstances, I'm in the same boat as these gentleman. My current kit works fine. It does what I want, not what some other company wants. That's... honestly quite liberating.

At the very least, staying a generation or two behind reaps huge benefits. And its not as though you don't get joy out of it: I've been having **so much damned fun** with Clara's and my [PlayStation 3 Slim](https://rubenerd.com/tag/playstation-3/) of late!

You know what, fuck it. I think my next machine might be a second-hand ThinkPad again.
