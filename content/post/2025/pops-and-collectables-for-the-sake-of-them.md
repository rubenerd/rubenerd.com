---
title: "POPs, and collectables for the sake of them"
date: "2025-03-02T08:32:43+11:00"
year: "2025"
category: Anime
tag:
- collectables
- doctor-who
- shopping
location: Sydney
---
Remember those *POP Vinyl* things? Take a look through an [image search](https://duckduckgo.com/?t=ffab&q=pop+vinyl "DuckDuckGo web image search for POP Vinyls") if you haven't seen them before. They were small plastic shells licenced from shows, anime, movies, music, and so on. They were *everywhere* in pop culture stores for a time. If you knew a character from somewhere... *anywhere*... chances were there was a POP of them. Or six!

POPs consisted of a large, boxy head and a tiny body, with precious few variations. They differed from traditional action and anime figures with their intentional uniformity and lack of detail.

This was marketing genius for a few reasons:

1. It made them trivial to rubber stamp and colour for whichever character they wanted, making them cheap to design and pump out at scale with the same moulds.

2. Comes after 1.

3. Their top-heavy design made them so ill-suited to being action figures, people would keep them in their boxes. This made them more collectable, tradeable, and offered free advertising long after the initial sale.

POPs were expensive for what they were, but were still far more affordable than most novelty items for a given franchise. Some didn't really even exist before a POP of them was made; how many Seinfeld action figures had you seen before? They were steeped in nostalgia for many people, even if the resemblance to their depicted character was impossible to discern without a prompt. That lead me to realise something we'll come back to shortly.

I had a POP Vinyl for a while! The ninth Doctor, played by Christopher Eccleston. He was my favourite, but given his short run he didn't have many traditional dust collectors of which to speak.

But after owning a few, I realised perhaps the obvious fact that&hellip; they just weren't that good. Not only that, they weren't good *by design*. They were made first and foremost to be a collectable, meaning they derive their value from being bought as a part of a set you buy and trade for to complete. But their build quality was poor, paint was sloppy, and they had more visible gaps and seams than a Tesla.

*(I'll admit POPs could be construed as being a gateway to the wider world of statue art, whether you go down the rabbit hole of Western or Japanese figures. But by the same token, is a phrase with five words. More often I saw them instead *crowding out* better things. POPs lined entire walls of pop culture stores, even book shops. I missed what was there before).*

This is where I think the comparison between these and, say, Beanie Babies was unfair. Sure, Beanie Babies were the overhyped tulips of the late 1990s, but they were well made. In a [Paul Simon-esque way](https://www.youtube.com/watch?v=FAb2Mu0CRk4 "Diamonds on the Soles of Her Shoes"), Japanese anime figures have more details on the soles of their shoes than the entirety of a western action figure, let alone POPs.

Ultimately, POPs taught me something interesting about myself. I ended up selling my Ninth Doctor POP when I realised I liked the *idea* of it more than its execution. That in turn has lead me to recognise this pattern of behaviour when I develop an odd fixation on a specific thing, and want to *catch them all*. Am I after those things, or the meta idea of collecting them?
