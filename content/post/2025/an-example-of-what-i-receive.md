---
title: "An example of what I receive, from [redacted]"
date: "2025-02-15T08:26:22+11:00"
abstract: "I have taken down the post where I quoted them, and respond to their concerns. I consider this matter closed."
year: "2025"
category: Internet
tag:
- feedback
- trolls
location: Sydney
---
**Update: 18:47 Sydney time**.

I've had a talk with someone who has more details, and have decided to take this post down in its entirety. I maintain the person involved was unreasonable, but am willing to look past this and move on.

I've since codified some [feedback rules](https://rubenerd.com/feedback-rules/) so this doesn't happen again. Making lemonade from lemons, and all that.

Thank you!
