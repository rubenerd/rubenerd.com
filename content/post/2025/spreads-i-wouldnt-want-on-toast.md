---
title: "Spreads I wouldn’t want on toast"
date: "2025-01-20T08:32:57+11:00"
abstract: "Bedspreads, among other things."
year: "2025"
category: Thoughts
tag:
- lists
- pointless
location: Sydney
---
In no particular order:

* Bedspreads

* The town of [Spread, West Virginia, USA](https://en.wikipedia.org/wiki/Spread,_West_Virginia)

* A credit or yield spread, at least in financial terms

I wouldn't mind a [spread distribution](https://en.wikipedia.org/wiki/Statistical_dispersion) though, especially if it possesses excellent dispersion. Of peanut butter.

As an aside, I had to look up the word *possesses*. It looked like it had way too many letter Ss. Even "letter Ss" has too many Ss.
