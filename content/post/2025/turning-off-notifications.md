---
title: "Turning off notifications"
date: "2025-02-24T07:31:27+11:00"
abstract: "We’re all realising that they’re mostly a bad idea."
year: "2025"
category: Hardware
tag:
- health
- phones
location: Sydney
---
When I finally sought help after last week's misadventures, one of the topics that was raised was whether I had notifications turned on. I joked that it almost felt like a screening question, to which I was told "there's no joke about it, and it definitely now is". Huh!

Everyone seems to be finally realising that notifications are mostly a bad idea, especially if you're prone to anxiety, doom scrolling, or anxious doom scrolling. Whereas before they may have presented you with timely and important information, today they're another dark pattern exploited to drive engagement and keep you looking at that slab of glass and no don't look away what are you doing!? Our attention has been fractured in so many ways by modern society, and this is one of the most pernicious ways how.

I was truthful and said I had notifications turned off&hellip; partly. I disabled notifications for email and social media years ago, because I only check those a few times a day, on *my* schedule. Honestly, a few times in a day is still probably too many. I have VIP profiles set for my closest family and friends so they can ping me whenever, but I don't need (or want) anything else. Truthfully, honestly, and all that.

But I also carry a work phone, and I receive work email, calls, and messages. The nature of my work entails having dozens of different IM platforms (it's quite frustrating, but that's a topic for another time), so being able to mute them outside office by locking out that entire phone has been great. I still need to do work on refining what messages *do* make it through though; most stuff I get on my work phone is either spam, or alerts that aren't actionable.

Given the proliferation and necessity of their devices, I think phone hardware makers have dropped the ball here big time. This stuff should be a lot easier to manage and approach in a healthy way. I know they're incentivised by that aforementioned dark pattern though, and including stats like "screen time" help them pass the buck in the way a personal "carbon footprint" does. But it's not good enough.

At the risk of becoming too philosophical here on a Monday morning, we have a finite time on this planet. I'd rather be lost in thought or an experience, not seeing my phone ping me for the fiftieth time that hour!
