---
title: "Où Est Le Soleil?"
date: "2025-01-13T08:48:13+11:00"
abstract: "Today’s Music Monday is a very wacky little thing by Paul McCartney."
year: "2025"
category: Media
tag:
- music
- music-monday
- paul-mccartney
location: Sydney
---
Today's *[Music Monday](https://rubenerd.com/tag/paul-mccartney/)* features one of Paul McCarney's sillier tunes that's an absolute early 1990s earworm:

<figure><p><a target="_BLANK" href="https://www.youtube.com/watch?v=IQesWC2rb-s" title="Play Où Est Le Soleil? (Remastered 2017)"><img src="https://rubenerd.com/files/2025/yt-IQesWC2rb-s@1x.jpg" srcset="https://rubenerd.com/files/2025/yt-IQesWC2rb-s@1x.jpg 1x, https://rubenerd.com/files/2025/yt-IQesWC2rb-s@2x.jpg 2x" alt="Play Où Est Le Soleil? (Remastered 2017)" style="width:500px;height:281px;" /></a></p></figure>

As quoted in the [Paul McCartney Project](https://www.the-paulmccartney-project.com/song/ou-est-le-soleil/)\:

> “A very wacky thing where we decided to make something up…Trevor said ‘Have you got anything for one of the verses?’ I said ‘Well I’ve got this really silly idea…’, which is like just some French words that say ‘Ou est le soleil? Dans la tete. Travaillez.’ Those are the complete lyrics…So we’ve got this silly French dance track now, which I love!

It has made it onto the *Capacitive Duractance* area of the sidebar.
