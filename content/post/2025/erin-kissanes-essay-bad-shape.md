---
title: "Erin Kissane’s essay “Bad Shape”"
date: "2025-01-12T09:18:54+11:00"
abstract: "“If a technological system makes human problems worse, you have to fix the system or break it and build a better one”."
year: "2025"
category: Software
tag:
- business
- social-media
- writing
location: Sydney
---
Her [latest post for wreckage/salvage](https://www.wrecka.ge/bad-shape/) didn't pull any punches. It's about social media, corporate governance, the tech industry, and the behaviour of Mad Kings. Make a coffee, sit down, and give it a proper read.

My favourite line, among too many to quote or mention:

> If a technological system makes human problems worse, you have to fix the system or break it and build a better one.
