---
title: "Silly word search for 2025"
date: "2025-01-05T09:45:58+11:00"
abstract: "The first word you see will define your year. Remember these!?"
year: "2025"
category: Thoughts
tag:
- pointless
location: Sydney
---
One of the low-effort "engagement" tricks people used to do on Twitter would be to come up with word searches to define specific times of the year. They'd put controversial or risqué words in to drum up anger and discussion, and the "engagement" would flow!

Well this year I've finally decided to make my own. This is based on topics near and dear to my heart; therefore I can only assume they are for everyone else as well. I'm not sure how alt text would work for a puzzle like this, so I've [posted the word list in the lunchbox](https://codeberg.org/rubenerd/lunchbox/src/branch/trunk/2025-word-search.txt).

Good luck! Just don't say I didn't warn you. Good thing I reminded myself to do that.

<figure><p><img src="https://rubenerd.com/files/2025/2025-word-search.png" alt="" style="width:500px; height:460px;" /></p></figure>
