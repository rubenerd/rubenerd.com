---
title: "Nice spot for afternoon coffee"
date: "2025-02-21T14:23:47+11:00"
abstract: "I’m working remote today, so I tried somewhere new."
thumb: "https://rubenerd.com/files/2025/northern-coffee@1x.jpg"
year: "2025"
category: Thoughts
tag:
- coffee
- sydney
location: Sydney
---
I'm working remote today, so I tried somewhere new. The coffee shop/roastery smells amazing, and has bar chairs and a long, thin table overlooking the street and surrounding landscape. It's lovely!

Though let's just say I'm glad I brought an umbrella in case.

<figure><p><img src="https://rubenerd.com/files/2025/northern-coffee@1x.jpg" alt="Photo showing a lot of trees, though a somewhat threatening sky." srcset="https://rubenerd.com/files/2025/northern-coffee@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>
