---
title: "Sometimes nerds just have to explain something"
date: "2025-01-21T15:30:50+11:00"
abstract: "Good intentions are not sufficient for a positive social interaction."
year: "2025"
category: Thoughts
tag:
- psychology
location: Sydney
---
I mean this with as much love and respect as I can muster, but nerds like us can be insufferable. One behavior I've witnessed in us&mdash;and yes, that includes me&mdash;is the constant need to *explain things*. I mean, that's the entirety of this blog.

I've had it happen to me. One time early in my career, a former colleague heard about my late mum's cancer. He took this as an opportunity to explain how chemotherapy and radiation treatment worked, why cancer was bad, and how the disease and the treatments ravage the body in different ways. I knew *absolutely all of this* of course; my family had lived it every day for twenty years. I know this is true for many of you too. But I didn't want to seem rude (though they clearly didn't care) so I nodded along.

Most of the time it's far more mundane. I was at a coffee shop&mdash;no, wait, really!?&mdash;when I overheard a gentleman explain why their POS machine wasn't working. It was clear to me from the barista's expression and body language that she already knew *exactly* why it wasn't working, but she entertained his long-winded description of 5G networks, wireless antennae, and Mercury being in retrograde. When I got to the head of the queue I widened my eyes as if to say "wow, that guy!" which made her laugh.

I don't know where the compulsion, the *drive*, comes from in us to explain how life, the universe, and everything works to anyone with a pulse, or to any plant with sufficient mobility like Venus flytraps.

On the one hand, I think it's part of our drive to want to help people, which is a noble thing. The world depends on significant scientific complexity, yet lacks so much layperson understanding of how any of it works, to paraphrase Carl Sagan. We see people seemingly struggle with something, whether pressing the `Submit` button on a web form with their mouse cursor when hitting `Return` would have sufficed, or a struggle involving a complicated process where the outputs don't make sense, and we *want to help*.

It's also a way we troubleshoot things ourselves. I know that I've solved many a problem explaining something to Clara, who may or may not have been too tired to understand half of what I was saying (I'm getting better at this, but I still catch myself far too often). I generally prefer remote work, but this is one thing I do enjoy doing when meeting in the office once or twice a week. Even introverts are social creatures, to a point.

There's also, unfortunately, a bit of an arrogance streak in some of this too. We think we're smarter than "regular people" (well, I don't, but I suspect a lot do), so we want to impart our wisdom onto them. This goes down as well as you'd expect, though if you lack sufficient understanding of social cues, you may not notice. It's this disconnect that I think gets a lot of people in trouble, and why I advocate for politeness if you're unsure.

I guess... try and see if people are receptive to your explanations, or consider if its an appropriate time or place to be imparting such information. This is as much a reminder for myself as anyone else.
