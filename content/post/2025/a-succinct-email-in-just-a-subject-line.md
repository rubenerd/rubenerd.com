---
title: "A succinct email in just a subject line"
date: "2025-03-11T07:01:09+11:00"
abstract: "I’d never heard of an end-of-message signifier in an email subject."
year: "2025"
category: Internet
tag:
- email
- work
location: Sydney
---
I love going through old 43 Folders posts when I either need inspiration, or am stuck on something. In a post about email, he [introduces the concept](https://www.43folders.com/2005/09/19/writing-sensible-email-messages) of the *End of Message* marker:

> In fact, if you're relating just a single fact or asking one question in your email, consider using just the subject line to relate your message. As I've mentioned before, in some organizations, such emails are identified by adding (EOM)—for end of message—at the end of the Subject line. This lets recipients see that the whole message is right there in the subject without clicking to the view the (non-existent) body. This is highly appreciated by people who receive a large volume of mail, since it lets them do a quick triage on your message without needing to conduct a full examination.

This sounds fantastic, though I'll admit I've never encountered this outside old school forums. Maybe it'd work for internal email where you've established the protocol, otherwise I suspect you'd just confuse people.

Incidently, Wikipedia has an article about [End of Message](https://en.wikipedia.org/wiki/End_of_message), but all the references are the same vintage as that 43 Folders post. 2005 was twenty years ago, cripes.
