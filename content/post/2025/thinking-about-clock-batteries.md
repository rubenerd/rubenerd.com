---
title: "Thinking about clock batteries"
date: "2025-02-10T08:28:34+11:00"
abstract: "Completing small tasks feels great. Then I started thinking about user serviceability."
year: "2025"
category: Hardware
tag:
- personal
- repairs
location: Sydney
---
I've had it on my *forever* list of tasks to replace the battery on our fridge-mounted magnetic clock from Muji. It's up there with *grease the fire escape door hinge*, *sweep the balcony*, and my personal favourite, *set daylight savings time on my alarm-clock radio*. It's at the point where I've been mentally calculating an hours difference from it this entire time whenever I look at it, which is profound in its sillyness.

Replacing this clock battery immediately resulted in three observations:

* Wow, replacing this battery was easy. I popped the back cover off, saw it was a standard coin-cell, and swapped it out.

* Wow, why had I been making such a big deal out of this, when it was that simple and straight-forward to do?

* Wow, that felt good.

All these occur with tasks I assign myself on a shockingly regular basis. Work tasks? No problem. Tasks for myself? Nah, that's not important. And besides, it sounds difficult and time consuming, and there's no way I'd get a sense of satisfaction from doing it. *Fool!*

I also couldn't help but contrast the first point with how we've been lead to think about our modern world, as I'm wont to do thesedays. Had a modern IT firm delivered that clock, the battery wouldn't have been user serviceable or simple to replace at all:

* It would have been held in place with layers of glue and tape, the screws would have been unusual, and even if you managed to swap it out without breaking some deliberately-fragile clip, it wouldn't quite fit back together correctly.

* The battery would be proprietary with odd dimensions, would draw a *specifically* weird amount of current, and would be nigh impossible to find at a reasonable price, if at all.

* If it was a generic-looking battery, the clock would refuse to recognise your replacement, citing safety or warranty it, because it doesn't have their DRM on it. Then you'd have to source a daughter board for the battery that adjusted it to the obscure specs it uses to validate it has a "genuine" part.

* After connecting the battery, the analogue display would somehow still show a warning in the corner that its power source voided its warranty, and that you should feel very bad about not taking it to one of their authorised service centres where they would have been more than happy to replace your battery for a staggering amount of money, and where a five minute job would take them a month, and sorry, they don't have slots available right now, how does June sound?

There's something reassuring about simple tech that's simple to use and simple to service. Simple! I wish we had more of it.
