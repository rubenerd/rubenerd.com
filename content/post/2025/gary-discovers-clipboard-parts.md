---
title: "Gary Castle was tempted by clipboard parts"
date: "2025-01-02T20:03:30+11:00"
abstract: "“I've got a guy who can get me clipboard parts at a really good rate.”"
year: "2025"
category: Hardware
tag:
- pointless
location: Sydney
---
[Mark Ericsson's](https://www.marksinfinitesolutions.com) friend Gary Castle had a [Corner](https://www.marksinfinitesolutions.com/garyscorner.html), and he had quite the revelation in March 2007:

> **WHOLESALE CLIPBOARD PARTS**
>    
> I've got a guy who can get me clipboard parts at a really good rate. These things are really cheap. The catch: everything comes unassembled. He keeps telling me that it's not difficult to put them together, and that everyone's buying clipboards these days. It feels like a hard sell, and I don't really have the time to start another business, but I'll be honest. I'm tempted.

Almost twenty years later, and I'm tempted to ping him asking how much bank he made, either being a clipboard part distributor, or a retailer of finished boards.

The closest I came to an assembly gig was with mechanical pencils, but I kept shocking myself on the batteries.
