---
title: "Franksting’s new blog: features, value, and opt-in"
date: "2025-01-07T15:24:57+11:00"
abstract: "“If you can’t clearly explain how a new feature benefits users, the setting for that feature should be turned off by default.”"
year: "2025"
category: Software
tag:
- friends
- engineering
location: Sydney
---
Long-time Twitter and now Mastodon friend [@Franksting](https://aus.social/@franksting@theblower.au) has [a new blog](https://franksting.writeas.com/), and he's [started with a topic](https://franksting.writeas.com/enhanced-visual-value) near and dear to me:

> If a product team has done a good job, users will clearly understand a new feature's value and they can turn it on whenever they choose.

And this, which should be stapled to the forehead of every manager and decision maker within a software team:

> No matter how long the list of available settings, if you can't clearly explain how a new feature benefits users, the setting for that feature should be turned off by default.

Here's his [RSS feed](https://franksting.writeas.com/feed/) too.
