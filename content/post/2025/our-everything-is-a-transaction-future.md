---
title: "Our everything–is–a–transaction future"
date: "2025-01-06T07:02:23+11:00"
abstract: "A quote from Philip K. Dick’s Ubik about a recalcitrant door"
year: "2025"
category: Media
tag:
- books
- finance
- quotes
location: Sydney
---
Today we have a quote from [Philip K. Dick](https://www.goodreads.com/quotes/7444685-the-door-refused-to-open-it-said-five-cents-please), via the imitable [Emil Oppeln-Bronikowski](https://helo.fuse.pl/@emil/statuses/01JG57NTTM2Z7HKWGDR2F88QNQ) on Mastodon. Ubik has made the reading list!

> “The door refused to open. It said, “Five cents, please.”
> 
> He searched his pockets. No more coins; nothing. “I’ll pay you tomorrow,” he told the door. Again he tried the knob. Again it remained locked tight. “What I pay you,” he informed it, “is in the nature of a gratuity; I don’t have to pay you.”
>
> “I think otherwise,” the door said. “Look in the purchase contract you signed when you bought this conapt.”
>
> In his desk drawer he found the contract; since signing it he had found it necessary to refer to the document many times. Sure enough; payment to his door for opening and shutting constituted a mandatory fee. Not a tip.
> 
> “You discover I’m right,” the door said. It sounded smug.
> 
> From the drawer beside the sink Joe Chip got a stainless steel knife; with it he began systematically to unscrew the bolt assembly of his apt’s money-gulping door.
>
> “I’ll sue you,” the door said as the first screw fell out.
>
> Joe Chip said, “I’ve never been sued by a door. But I guess I can live through it.” 
