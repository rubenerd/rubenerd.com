---
title: "Cursed curly brackets in code"
date: "2025-01-20T08:20:40+11:00"
abstract: "An old professor of mine insisted on a scheme that looked bad, and made writing code harder."
year: "2025"
category: Software
tag:
- design
- programming
- nostalgia
location: Sydney
---
I grew up writing C-style code, a little, like this:

    sub testing() {
        say("Hello, world");
    }

Though lately I've been doing this instead for readability:

    sub testing()
    {
        say("Hello, world");
    }

These are the only bracket layouts that look good to me, though I appreciate it's subjective. If your project style guide says I should indent brackets into something I think looks awkward, I'll still comply.

I bring this up though, because I was going through some old assignments out of nostalgia, and remembered this truly cursed style one lecturer insisted we submit code with:

    sub testing
    {   say("Hello, world"); }

You're seeing that right, the formatting wasn't broken or changed by your browser. I remember seeing his lecture slides the first time and thinking... *wait, what?*

The reason for starting with a tab was so multiple lines would still line up:

    sub testing
    {   say("Hello, world");
        &someMethod(); }

I mean, *it works*. So did my paintbrush attached to an old vacuum cleaner pipe, complete with handle. I wouldn't recommend it.

Aesthetically, it's a mess. The tab at the start means the spacing between brackets is unbalanced, unless you also remember to add a tab before the closing bracket too. Which this lecturer *deducted marks* for. Elegant, functional code that's presented well can be beautiful. *This ain't it, chief!*

But it gets worse. Say I wanted to add a new line above my print statement. In any other bracket layout, you'd just add the line. But you can't here, because the first line *also has the opening bracket!* The number of times I accidentally did this drove me bananas: 

    sub testing
        &methodToInsert("Whoops!");
    {   print("Hello, world"); }

It reminds me of inserting code into 8-bit BASIC with numbered lines. The professor turned Java and C++ into BASIC. Incredible!

I've been feeling a bit nostalgic for university days again of late. But I don't miss this. This was terrible.
