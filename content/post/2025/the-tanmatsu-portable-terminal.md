---
title: "The Tanmatsu portable RISC-V terminal"
date: "2025-01-03T11:03:42+11:00"
abstract: "I’ve been looking for a device to hack on while commuting. This is another strong contender, and they're taking expressions of interest!"
year: "2025"
category: Hardware
tag:
- indie-hardware
- handhelds
- risc-v
location: Sydney
---
I wrote a few posts last year talking about [my desire for a portable computer](https://rubenerd.com/an-oqo-or-hand-386-successor-for-commuting/ "An OQO or Hand386 successor for commuting?") in a slab form-factor I could use while commuting on the train. This is opposed to a commuting form-factor I could use while slabbing on the train. That sounds like planking. Wow, 2011 called, they <a title="Wikipedia: Planking fad" href="https://en.wikipedia.org/wiki/Planking_(fad)">want their meme</a> back.

There have been some great suggestions, some of which I may have pre-ordered for... "testing" (cough), but I've still been keeping an eye out for one that ticks all the proverbial boxes.

Another contender has entered the ring, [this time from Nicolai Electronics](https://nicolaielectronics.nl/tanmatsu/) in the Netherlands:

<figure><p><img src="https://rubenerd.com/files/2025/tanmatsu@1x.jpg" alt="Photo of a small, purple handheld computer with a flat form factor and physical buttons." srcset="https://rubenerd.com/files/2025/tanmatsu@2x.jpg 2x" style="width:350px; height:225px;" /></p></figure>

From the [documentation page](https://nicolaielectronics.nl/docs/tanmatsu/)\:

> Tanmastu is the dream terminal device for hackers, makers, and tech enthusiasts. Based around the powerful ESP32-P4 microcontroller, this device provides an accessible way to make, hack, and tinker on the go.
>
> Tanmatsu lets you program on the go and communicate over long distances using LoRa whilst also providing advanced connectivity and extendability options for hardware hacking and development.

It has a dual-core ESP32-P4 400 MHz RISC-V CPU with 32MB of PSRAM, an 800×480 display (for running at 1×, no awkward fractional scaling), ESP32-C6 module with Wi-Fi 6, Bluetooth LE 5 and Zigbee, and even a LoRa radio. Best of all, it's a slab form factor with a tactile keyboard!

I wanted something I could hack on while on the train. This might be it.

They're accepting [expressions of interest now](https://nicolaielectronics.nl/tanmatsu/).
