---
title: "The OM SYSTEM OM-3 is the camera of my dreams"
date: "2025-02-11T12:32:24+11:00"
abstract: "It has the mechanical dials of my older Olympus camera, but with modern internals!"
thumb: "https://rubenerd.com/files/2025/om3-dials@2x.jpg"
year: "2025"
category: Hardware
tag:
- cameras
- olympus
- om-system
location: Sydney
---
Oh hi, how are you today? Something happened last night which has me so excited I can't sit still. I saw something new announced while on the train, and the shaking poor Clara had to endure as I took her shoulders and exlaimied *they did it!* could probably be felt on the upstairs deck of the carriage.

I'd like to begin by showing you a press picture of the **Olympus OM-D E-M10 Mark II**, the camera I consider to be one of the best pieces of tech I've ever owned, right alongside the iPod 3G, and the ThinkPad X40:

<figure><p><img src="https://rubenerd.com/files/2025/om-d-e-m10-mark-ii@1x.jpg" alt="Olympus OM-D E-M10 Mark II" srcset="https://rubenerd.com/files/2025/om-d-e-m10-mark-ii@2x.jpg 2x" style="width:500px; height:362px;" /></p></figure>

I saw this camera for the first time in 2017, ostensibly when I'd gone in to buy a new Nikon D5600 DSLR for Clara's and my first overseas trip together. The retro styling was the initual allure, but my mind was made up when I held the demo unit, adjusted its impossibly small pancake lens, and turned those dials with their precise mechanical feedback. I walked out an Olympus guy again, and to this day it remains the most expensive impulse purchase I've ever made.

As I've said many times, I've had other cameras after that Olympus that beat it on paper, but not in experience. I'm but a middling photographer, but I love the hobby, and it adds a whole new layer of awesomeness to every trip. We only took our new(ish) iPhones on our last Japan holiday, and it was so much less fun without a real camera. The pictures from the 15 are good, but they lack the depth, control, and... *fun!* Which honestly, isn't that the whole fscking point of a hobby?

Hence this year I decided to save and buy an **OM-1 Mark II**. This is an important camera in many ways, but it proved that the new OM SYSTEM company were able to carry the torch they bought from Olympus. It has 4K/60 video and a stacked sensor, yet retains that Micro Four Thirds format and resulting glass size that made the system so appealing and fun for travel:

<figure><p><img src="https://rubenerd.com/files/2025/om-1-mark-ii@1x.jpg" alt="Press photo of the OM-1 Mark II" srcset="https://rubenerd.com/files/2025/om-1-mark-ii@2x.jpg 2x" style="width:500px; height:358px;" /></p></figure>

But do you notice something? I wasn't that fussed about the more modern styling, but I'll admit to being a *tad* disappointed at the reduced number of mechanical dials. Mapping those to common settings on my E-M10 was a game changer in a practical sense, and made using the camera such a joy. If only OM SYSTEM made a camera with those manual controls, but with the modern internals of the OM-1.

Uh oh:

<figure><p><img src="https://rubenerd.com/files/2025/om3-dials@1x.jpg" alt="" srcset="https://rubenerd.com/files/2025/om3-dials@2x.jpg 2x" style="width:500px; height:384px;" /></p></figure>

What you're looking at here is a press image from the brand new **[OM SYSTEM OM-3](https://explore.omsystem.com/au/en/om-3 "The OM SYSTEM OM-3")**, announced this month. It's... gorgeous!

Based on the design language of the Olympus OM-3 film camera, the new OM-3 also bears striking resemblance to my OM-D E-M10. All those glorious dials and buttons are back, and there's even a new dial on the front for playing with filters. It shares many of the same components as the OM-1 Mark II, though it lacks the second SD card slot. It also represents the first entirely new line under the OM SYSTEM umbrella, not a refinement or revision, which is encouraging to see.

In summary, it's as though someone within OM SYSTEM had been reading my rambling nonsense here over the last month, and taken notes. It's *scary* how perfect this is for my use case. My money is now being directed into saving for one of these instead.

I should clarify that OM SYSTEM has not sponsored any of these glowing posts; I just love their kit. Though I'd graciously accept any spare OM-3s that OM SYSTEM have kicking around. I'm quite generous like that.
