---
title: "Building an Atari 1040STᴱ chip manifest"
date: "2025-02-03T08:40:46+11:00"
abstract: ""
year: "2025"
category: Hardware
tag:
- atari-1040ste
- retrocomputing
location: Sydney
---
Last year I [bought an Atari 1040STᴱ](https://rubenerd.com/my-atari-1040ste/), a machine by many of those former Commodore engineers that I've been [fascinated with](https://rubenerd.com/the-atari-st-is-my-favourite-16-bit-machine/) for years. With this, I had another opportunity to embark on one of the biggest joys in my life: figuring out how an unfamiliar computer works! Sure, I know the basics about memory, CPUs, and chipsets, but the chance to almost start again from first principles on a new architecture is the absolute best. Modern PC building has nothing on it.

Once I've determined a retrocomputer's operating state (less I mess something up later), I carefully pull them apart and make note of where screws and cables go with photos. Then I can do a light dusting and begin looking at the motherboard. It's fun seeing how an old machine was put together, and circuit board design was as much an art as it was a science. 

<figure><p><img src="https://rubenerd.com/files/2025/atari-1040ste-board@1x.jpg" alt="Photo of the Atari disassembled on a table." srcset="https://rubenerd.com/files/2025/atari-1040ste-board@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

Which gets me to ICs. My silly [Retro Corner](http://retro.rubenerd.com/) started as a single page with chip manifests for old machines. These include date codes, model numbers, manufacturers, motherboard locations, function, and operating state (if known). I started doing this in a spreadsheet, and that's still probably the most logical place to put them if you don't also have nostalgia for HTML 3.2, like a schmuck.

Building these tables teaches me a *lot* about the logic gluing a machine together. For example, my heart sank when I saw an empty `U101` socket for the BLiTTER chip, but I read on forums that this is expected on later models where Atari incorporated its functions into the GLUE chip in `U400` to save cost. It's also fun to guess what certain chips are for: for example, I suspected a `WD1772` near the disk drive was a controller of some sort. If I can find the spec sheets for them online, I grab them as well.

Manifests are also *hugely* helpful if I'm troubleshooting a fault. Chip substitution isn't always feasible, but for someone who only got into electronics and circuits a few years ago after spending his life in the software realm, it can be a quick way to address a problem. No sound? Swap the sound-generating chip. It might be useful to have some of these handy anyway. I also know for future reference what specific chips and model numbers go into my machine lest I need to replace any of them.

<figure><p><img src="https://rubenerd.com/files/2025/atari-1040ste-cpu@1x.jpg" alt="Zoomed-in photo of the Motorola 68000 inside the Atari." srcset="https://rubenerd.com/files/2025/atari-1040ste-cpu@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

How *gorgeous* is that font!? I love my 486 chips, but the 68000 holds its own in the design department. *But I digress.*

The state of a machine's ICs can give clues as to the history of a machine. Chips that look physically newer, or have large discrepancies in their date codes indicate the machine was worked on before, or some of the ICs have been replaced. For soldered components outside of a socket, leftover flux may indicate the same thing. It's like electronic archaeology, where you have output of what was done, but you're guessing as to how it got there, who did it, when, and for what reason.

My [Atari 1040STᴱ now has a page](http://retro.rubenerd.com/1040ste.htm) on the Retro Corner for these chips, which I hope to add to. I also wanted to say <span lang="de">danke schön</span> to the Forum64 site for this [fantastic thread](https://www.forum64.de/index.php?thread/3436-die-chip-nummern-und-was-dahinter-steckt/) posted in 2004 (!) detailing what each of these chips do. I definitely printed a PDF of this for my own archives.
