---
title: "Consoles, and the PlayStation 3"
date: "2025-01-06T10:29:08+11:00"
abstract: "My parents didn’t like consoles, but I was intruiged by the PS3. The first in a series about this machine."
year: "2025"
category: Hardware
tag:
- games
- playstation-3
location: Sydney
---
This is a bit of a [shaggy-dog story](https://en.wikipedia.org/wiki/Shaggy_dog_story "Wikipedia article on shaggy dog stories"), but it was fun to think back on. It also lead me to dive back into some older hardware again, and even fix and upgrade some of it! This is part one on my series on the PlayStation 3, though it might take me a few posts to get there. I know, this surprises nobody.

I arrived late to consoles. My parents believed in buying *computers*, because at least they could be operated under the pretence of doing school work, programming, and other productive things. Maybe those [William Shatner VIC-20 ads](https://www.youtube.com/watch?v=UK9VU1aJvTI "Video ad from the early 1980s from Commodore featuring William Shatner") imprinted on them two decades prior, who knows! While friends had the usual assortment of gaming devices&mdash;*especially* my Japanese friends&mdash;our family’s first console was a Nintendo Wii after my sister and I already left high school. The Wii was a fascinating outlier in that respect for a lot of people, and is worth a retrospective in its own right.

I harbour no resentment for not having consoles, to be clear! The PCs my parents bought were almost certainly more expensive than the consoles they so desperately wanted to avoid, and I loved the hours playing the *SimCity* and *Sims* franchises, *Need for Speed*, *Worms*, and *Age of Empires II*. They also gave me the opportunity to write my own text-based adventure games in Visual Basic and Pascal, which I might release one day after I've prepped sufficient embarrassment insurance.

*(This aversion to consoles meant my parents were also more than receptive to the flimsy excuses I gave to upgrade various components to play games better. Of course you need a better graphics card to play visual novels, and to compile Delphi and C++Builder! Did I mention you need more RAM if you want to transduce homework with flux mogrifiers? You could say I learned the art of strategic technobabble at a young age, though I'm also certain my parents saw right through it).*

This was the late 1990s and early 2000s, so my friends mostly had either older Sega and Nintendo kit, or more likely an original Xbox. I'll admit, I did internalise many of those same arguments my parents made, and wondered why you'd buy a single-tasking machine that could only run one thing at a time. Yes Ruben, that's what single-tasking means. What if I wanted to use the machines for other things? How would I build stuff myself? Yet, I did see the appeal of a machine with a sole purpose of playing games: no drivers, no anti-virus tools, no faff installing and fixing OSs. Just insert a disc and go.

<figure><p><img src="https://rubenerd.com/files/2025/ps3@1x.jpg" alt="" srcset="https://rubenerd.com/files/2025/ps3@2x.jpg 2x" style="width:500px" /></p></figure>

Then a couple of interesting things happened which drew my interest to the PlayStation, and specifically the PS3 which was the new hotness. It was well-publicised at the time that the PS3 could run the [Folding@home medical research project](https://foldingathome.org/faqs/high-performance/folding-sony-playstation-3-ps3/), for which I was a regular contributor with my PowerPC and AMD machines:

> The PS3 system was a game changer for Folding@home, as it opened the door for new methods and new processors, eventually also leading to the use of GPUs. [&hellip;] Since it’s induction, over 15 million PS3 users have participated in FAH, in total donating more than 100 million computational hours.

The PS3 Folding@home client was retired in 2012, the same year the PS3 Super Slim was introduced. Maybe Sony were starting to field too many support requests and warranty claims for hardware that had melted from running continuously (cough), or it was becoming too much work for the F@H team to maintain the bespoke client. I really should get this cough checked out, it's seeping into more posts.

<figure><p><img src="https://rubenerd.com/files/2025/Blu-Ray-Logo.svg" alt="The Blu-ray Disc logo, in all its mid-2000s glory" style="width:320px;" /></p></figure>

But it was the PS3's Blu-ray integration that was its real claim to fame at launch. It was a running joke at the time that it was cheaper to buy a PS3 than a dedicated Blu-ray player, and you got a "free" console with it. The Xbox-whatever came with the option for HD-DVD, but who wanted that. I knew of at least a few people who got a PS3 specifically to play Blu-rays, and of course ended up playing games. I guess that's how loss leaders make business sense.

I never got a PS3 (or any other console beyond that aforementioned Wii), and as the years went on I continued mostly playing PC games (though I still maintain an empty text editor is the most fun you can have, cough. There's that cough again). Then in 2017 Clara and I saw a PS3 Super Slim going for basically nothing at a second-hand store, and we pounced.

Our adventures with that, and the Slim we got in December 2024, are for part two. **Spoiler alert:** they're awesome well beyond games, though the games were fun too. And it's also a phrase with two words.

*Photo of the original PlayStation 3 and controller by [Evan-Amos on Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Sony-PlayStation-3-CECHA01-wController-L.jpg).*
