---
title: "Blog question challenge: Technology"
date: "2025-02-13T08:30:28+11:00"
abstract: "Where I got interested in technology, my favourite tech of all time, my favourite right now, and future ideas."
year: "2025"
category: Hardware
tag:
- astronomy
- lists
- personal
- retrocomputing
location: Sydney
---
A [Mastodonian](https://bsd.network/@Rubenerd) and reader of my ramblings here, and the cogent thoughts over at 82MHz, asked me if I'd be up for answering the same [blog challenge](https://82mhz.net/posts/2025/02/blog-questions-challenge-technology-edition/) linked therein. Why not?


### When did you first get interested in technology?

This one is easy, though I don't think I've ever shared it before. My late maternal grandad became a home astronomer when he retired, to the point where he moved up the Australian coast to a remote plot of land and built his own telescope and small observatory from scratch. One of my earliest memories was sitting on his lap as he dialed the telescope in by hand and showed me Jupiter.

My heart skipped a beat. The awe I felt was difficult to put into words as someone barely four years old, and it's still hard to explain now. Here was this planet, impossibly far away and hard to see... but this piece of technology made it as crystal clear as a model I would have held in my hand. It's still one of the most vivid memories I have.

My parents had a 486SX I became enamored with in short order, but that one event did more than anything before or since to shape my views on technology and its potential for wonder, and the betterment of humanity. I often revisit those feelings when I'm reading the latest dystopian tech news, and remind myself that there's good here as well.


### What’s your favourite piece of technology of all time?

My granddad's telescope? :).

This one is tricky, because I love various pieces of tech in different ways, and for different reasons. But as I mentioned in my recent camera post, I'd say they'd boil down to these four:

* **ThinkPad X40**. This was my first "ultrabook" laptop, which I bought second-hand for peanuts. It had a wonderful keyboard, fit in any bag, and ran FreeBSD beautifully. It got me through the first years of university. I've had other laptops before and since, but this remains the GOAT.

* **Third-gen iPod**. I got this slab of white gold for my birthday one year, and it reshaped how I listened to music. No more discs or SD cards, it was just a device that would plug into my Mac and transfer music from my well-manicured iTunes library and iPodder/Juice Receiver podcast client. This was also before the click wheel, so it still had those touch buttons along the top for navigation. I’ve been chasing that experience since.

* **Olympus OM-D E-M10 Mark II**. This entry-level Olympus mirrorless camera was built like a watch, and made me fall in love with photography in a way no other camera or manufacturer has. Remapping and turning those mechanical dials and controls has spoiled every other camera I've used.

* **Technics SL-J300R turntable**. This is an automatic, linear tracking, direct drive, track-selecting record player, and my introduction into vinyl again after growing up in a household with records and CDs everywhere. I'd say it represents the peak of analogue music hardware (well, before we get into tube amps and so on). It's *so* much fun, yet also bittersweet to think that modern stuff possesses but a shadow of what this can do.


### What’s your favourite piece of technology right now?

I agree with Andreas here, so much modern tech is ho-hum to me. It's something with a phone interface, or an incremental improvement on something we've done before (albeit with steeper power requirements). *Whoopie!*

I'm going to cheat and answer in the aggregate: open hardware for retro devices The ingenuity and creativity of people teaching this old hardware new tricks with a new expansion cartridge, card, or device makes me so happy. I love that you can buy an analogue of a Gravis Ultrasound, or a memory expansion unit, or a modern disk drive, without breaking the bank.

These devices are possible thanks to the fact independent designers can prototype, design, program, and print their own circuit boards based on commodity components like the ESP32 and the RP2040, and deliver them to eager users and customers thanks to the Web. This is collaborative technology, by us, for us. And it's wonderful.


### Name one new cool piece of technology we’ll have in 25 years!

I'll name it the *Tealbox*, after my favourite colour and cuboid shape respectively. I wouldn't pretend to have any unique insight into what it does, how it works, or what it solves.

I know it's boring, but I'm *really* looking forward to additional storage density. I'd **love** an affordable NAS chock full of SFF or NVMe that can sit at home, or at a relative's house, and provide an alternative to a massive data centre somewhere. The Fediverse has shown there's sufficient interest in small tech again, and I'd love to see this continue to blossom.
