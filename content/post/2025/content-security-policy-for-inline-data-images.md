---
title: "Content Security Policy for inline data:image"
date: "2025-02-09T08:45:21+11:00"
abstract: "Use the 'data:' directive."
year: "2025"
category: Internet
tag:
- security
location: Sydney
---
This is a bit of a nuts-and-bolts post, but I was trying to get inline images on a page working with [Content Security Policy](https://developer.mozilla.org/en-US/docs/Web/HTTP/CSP). A target page had the following:

    <img src="data:image/png;base64,[...]==" />

I thought images expressed in `data` would be an `unsafe-inline`, just like inline CSS styling. I was wrong:

    Content-Security-Policy: default-src 'self'; 
        style-src 'self' 'unsafe-inline';
        img-src 'self' 'unsafe-inline';

The solution is to use `data:`, which also doesn't need quotes

    Content-Security-Policy: img-src 'self' data:;

Thanks to the responses on the [Information Security Stack Exchange](https://security.stackexchange.com/questions/249694/content-security-policy-https-and-data-meaning) for pointing me in the right direction.
