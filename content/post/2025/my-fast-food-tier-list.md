---
title: "My fast food tier list"
date: "2025-01-10T20:58:33+11:00"
abstract: "A completely pointless ranking of bad things that are bad, and that I should feel bad. Thank you."
thumb: "https://rubenerd.com/files/2025/tier-list-western-ff@2x.jpg"
year: "2025"
category: Thoughts
tag:
- food
- pointless
- tier-lists
location: Sydney
---
It's a Friday night after a long week back at work, so what do you do? You sit down with your partner and rank western-style fast food chains, with a healthy (hmm) dose of childhood nostalgia.

**Disclaimer:** Fast food is bad, I should feel bad, and giving soulless multinational companies free advertising is bad.

With that out of the way, here's my list. Listed countries are where I had them, not necessarily where they're from.

<figure><p><img src="https://rubenerd.com/files/2025/tier-list-western-ff@1x.jpg" alt="A S to F tier list, with the outlets listed below in order of ranking." srcset="https://rubenerd.com/files/2025/tier-list-western-ff@2x.jpg 2x" style="width:500px; height:528px;" /></p></figure>


### S-tier

* **MOS Burger**, *Japan*. God-tier fast food. The burgers come out looking exactly like the picture, and the taste is matched only by the meticulous wrapping. Family and friends I've travelled with said it was no good. I love them dearly, but they're wrong.

* **Kenny Rogers Roasters**, *Singapore and Malaysia*. Long since gone from the US, but still thriving in Asia. I would eat my body weight in Kenny's Corn Muffins while sitting under a buzzing neon sign saying *Ruben James*. You'd think the novelty would wear off, but it doesn't! Well... not in Malaysia evidently.

* **Pizza Hut**, *Everywhere*. They were a fixture of our childhoods in Australia and Singapore. Pizza Hut in Hong Kong and Japan tastes *exactly* the same as Singapore; must be the lactose-free cheese. Also CC from *Code Geass* eats it.


### A-tier

* **KFC**, *Everywhere*. I would get so excited when my parents brought this back. They have the best chips in Australia, though their Malaysian franchise is by far the tastiest. They also sell Pepsi, which is better.

* **Bunnings**, *Australia*. Also known as "Hammer Barn" in some areas. If you know, you know.

* **Carl’s Jr**, *Singapore*. Australians think they're meh, but they were definitely more premium in Singapore. Their Santa Fe grilled burgers were so good, as were their criss-cross chips.

* **Red Rooster**, *Australia*. Probably the best Australian fast food after Bunnings. Their patchy availability in Sydney has been the subject of socioeconomics essays. I would put their herb sauce on my breakfast cereal. Don't quote me on that.

* **A&amp;W**, *Singapore*. They're gone from Singapore now unfortunately, but I loved them. The *decor* hadn't changed in thirty years, which might have been a contributing factor. I imagine MacDonalds being that good in an alternate universe.


### B-tier

* **Hungry Jacks**, *Australia*. "Fun facts!" YouTubers love pointing out that a trademark dispute caused them to be the Australian *Burger King*. They're wrong, the burgers are better at Hungry Jacks.

* **Oporto**, *Australia*. Spicy fast food chain started at Sydney's Bondi Beach, most famous for leasing its colour to the first iMac for 99 years before they must be painted beige again. I quite like them, even if the quality can be a bit hit or miss. Like that first iMac.

* **IKEA**, *Everywhere*. Getting a soft serve or hot dog after making it through the checkout feels like a prize. Mental note: include the upstairs restaurant in the "dine in" tier list.

* **Chipotle**, *United States*. Not as good as I was sold, but still embarrassingly better than **any** of the Mexican-adjacent stuff in Australia. Even their salads were good.

* **Lord of the Fries**, *Australia*. This Melbourne-based chain makes vegetarian burgers and hot dogs that split opinion. All I know is they taste better than more than half the food on this list, which is saying a lot, or very little, depending on your perspective. Personally, I quite like them.


### C-tier

* **Henny Penny**, *Australia*. They were the first Australian fast food chain, so my parents used to tell me. A few branches still exist; I tried the one in Raymond Terrace, NSW. They were fine; maybe a shadow of their former self. I reckon they should lean into their heritage and make Vegemite burgers and lamington drinks. I'm a ridgy-didge genius.

* **Burger King**, *Singapore*. I used to love their chicken strip sandwiches and onion rings, though I thought *Hungry Jacks* in Australia was better. Lots of fun memories sitting around kids parties wearing those paper crowns. Burger King in the US was awful though (and I'm sensing a theme).

* **MacDonalds**, *Everywhere*. Conventional wisdom says they're consistent everywhere. Nonsense! MacDonalds in the US was dreadful. Malaysian MacDonalds is surprisingly good. In most places, I think we can all agree the Fillet of Fish is the best offering. Just don't eat too much, especially if you're about to board a flight. Don't ask teenage Ruben or his sister how they know.

* **O'Briens**, *Singapore*. Nice sandwiches but nothing to write home about, though evidently enough to blog about!? Basically what Subway could be if they wanted to be.

* **Guzman y Gomez**, *Australia*. Australian Mexican-adjacent food. Consistently fine, but trends towards the bland and simplistic at times. They're helped by virtue of the fact other Australian Mexican-adjacent food is terrible. They're even in Japan now, which blew my Australian mind when we saw them.


### D-tier

* **Dominos**, *Australia*. You know when you crave pizza, but you're shy and don't want to deal with deliveries, so you're willing to accept something you otherwise wouldn't? That's Dominos. Not bad, but not good. Just like this blog.

* **Taco Bell**, *Singapore*. They're the "Flamin' Hot" of fast food. Somehow a lot of spice, but not much flavour. They're a riddle wrapped in a mystery inside a burrito. Or at least, food cosplaying as such. I go there for nostalgia sometimes, but I'm not happy about it.

* **Subway**, *Everywhere*. They have some good stuff, and some awful stuff. The key is knowing what fillings and bread will get you something decent when it's late, you're tired, and they're still open. I used to like them a lot more, but maybe I outgrew them?

* **Long John Silvers**, *Singapore*. This is a tiny bit heartbreaking, because I'd love to say I like them more. Again, lots of childhood memories of birthday parties and hanging out with friends. But I always left feeling... not great. It was the way they battered stuff.


### E-tier

*Nothing in this tier. Maybe I was feeling charitable!*


### F-Tier

* **Mad Mex**, *Australia*. They make GYG taste like A-tier.

* **Five Guys**, *Australia*. Went to their first branch in Sydney for *two* client meetings, if you can believe it. If I wanted bland food with that much grease, I'd siphon one of those... hmm, let's just stop that imagery right there.

* **Krusty Burger**, *The Simpsons*. Wait, a "Krusty Burger"? Hmm, that doesn't sound too appetising. I've also been told they don't have stew today.


### Haven't tried

* **Jollibee**, *Philippines*. This chain has become a sensation across Asia, and I think they're in North America now too. I've gone past them in Singapore a few times but have never tried.

* **Panda Express**, *United States*. Clara and I really interested to try American-Chinese, given how Australian-Chinese is its own unique style with a fascinating cultural history. People who say it's "not real Chinese" are wasting their talents on us mere mortals. They probably say MSG causes headaches too.

* **White Castle**, *United States*. I've never had a slider before. Also, you know, that bad movie that was huge when I was a kid. Clara and I saw one in New York, but we ended up at a cute bakery instead. Probably the better option.

* **Popeyes Chicken**, *United States*. Again, for another movie from that time: *Little Nicky*. Wow, my tastes really have changed, it's like I've grown up or something! Well, maybe not too much.
