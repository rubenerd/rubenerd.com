---
title: "Stuff doesn’t work"
date: "2025-01-19T09:04:26+11:00"
abstract: "I collected a list of things that didn’t work over the course of ONE DAY. It was somehow worse than I expected."
year: "2025"
category: Hardware
tag:
- rant
location: Sydney
---
I have a draft riffing on something I read recently about how people *don't care about things*. It started getting a bit long, and included a lot of semi-related stuff about tech that I thought would make more sense breaking out into its own post. Hey look, that's this one!

Have you ever noticed that so many things in our daily lives often just... don't work? How the years go by, and the number of things that don't work doesn't seem to be getting any better? Despite our years of accuumulated experience designing, building, and fixing stuff?

I decided to collate a list of things that didn't work last Saturday:

* A driver update for my graphics card broke my desktop. Software would freeze when launching, there were visual artefacts everywhere, and I had to type certain commands blind.

* Clara and I created a shared calendar at our paid email provider, following the instructions on their site exactly. Clara didn't see the shared calendar appear in her account, as the instructions said it would. She created a shared calendar for me, and I couldn't see it either.

* I couldn't delete a multipart upload on an object store. The command would advise that it was deleted, but then it wasn't. Was it not GC'd correctly? Did the command actually break? Who knows.

* My work laptop decided that I didn't *really* want to tether to my work phone, so I had to fall back to using my personal phone. Which also only worked after trying to connect three times.

* The lift in our building now blares out a shrill beep whenever it does something. It can be heard echoing down the hallway at all hours of the night.

* The POS machine at our local op shop hung using our payment card, so they had to put it through again. So far our statement shows only one transaction, but I still worry a second will appear that I'll have to chase up.

* A search engine's unwanted AI tool suggested a command to use to fix something which, if I had followed it, would have made the situation *comically* worse.

* We tried paying a water bill, and their site got stuck in an authentication loop, eventually resulting in a blank page.

* eBay sent me a notification about a retrocomputer saved search that, when I clicked on it, informed me that it doesn't ship to my country. It also "suggested" another item to me that didn't match any of the keywords, and was in fact an inflatable lawn chair. They have a sense of humour at least.

* My top-of-the-line iPhone can't scroll smoothly anymore. I feel like I'm back on my 2007 Palm Centro, and that thing was at least affordable and fit in a pocket.

* The Sydney Trains turnstile at our local station refused to engage and open when I tapped my Opal card. Of course when I tapped again, the gate refused to move, citing the fact that I had "already tapped on".

* We had to use one of those self-service checkouts at a restaurant, and the thermal printer jammed printing our receipt. I'm so used to this happening, I fortunately was able to snap a photo of our order number before the screen cleared.

* A website I tried to access had a cookie permission popup that was impossible to dismiss. I ended up right-clicking and blocking it with UBlock Origin. But because it had a randomly generated HTML class, I had to do it again when I went to the next page.

* Our TV refused to see the HDMI signal from our homelab box, despite being able to SSH into it. A reboot was required.

That was over the course of **one day**. And I'm sure I'm missing a bunch of other smaller issues.

Now I know what you're thinking. Ruben, I don't have that many issues, it's PEBKAC. Or it's confirmation bias: I see patterns and behaviour when I seek them out. Technology is infinitely more complicated and complex than it used to be, so in some sense it's miraculous the number of problems hasn't jumped up by a similar amount. Legacy tech had its frustrating problems too, back in my day we had punch cards and literal bugs. It's because of enshittification, specific economic systems, Wall Street, Mercury retrograde, and all that.

I'm sure it's all true, or at least has whiffs of being so. I'm just pointing out the fact that thirty-something me feels like he's having the same amount of problems that twenty-something me did. And I have this nagging feeling that fourty and fifty-something me will too.

Do you think that's part of the reason so many of us are anxious, exhausted, and a bit angry thesedays? We have to use this tech more and more (and more), and the stuff just doesn't work. They're like microagressions that build up over the course of a day, until I reach a point where I say *NO MORE!* So I reach for the TV remote to unwind for a bit, and ABC iView has crashed.
