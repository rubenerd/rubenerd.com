---
title: "MediaWiki with PostgreSQL"
date: "2025-01-28T08:05:28+11:00"
abstract: "It seems to work well from our testing, I'll keep it around and see how it goes."
year: "2025"
category: Software
tag:
- mediawiki
- self-hosting
- postgres
location: Sydney
---
I've been on a bit of a kick lately replacing MySQL (and equivalents) with Postgres. I harbour this dream of only maintaining a single database stack, silly as that sounds.

I completely forgot that [MediaWiki also supports Postgres](https://www.mediawiki.org/wiki/Manual:PostgreSQL), albeit with caveats:

> MediaWiki supports PostgreSQL since 1.7, but please note that this is second-class support, and you may likely run into some bugs. The database most commonly used with MediaWiki is MySQL. See Phabricator for a list of issues. MediaWiki requires PostgreSQL 10.0 or later, and PG database support enabled in PHP.
>
> Most of the common maintenance scripts work with PostgreSQL; however, some of the more obscure ones might have problems. 

As an aside, I'm still so crushed that they replaced that charming sunflower logo with a [British Gas company](https://rubenerd.com/mediawikis-new-logo/). But I digress.

Normally I'm pretty conservative with what I run, jog, and sit on, but I cloned Clara's and my other wiki and did a database migration to PostgreSQL over the long weekend, and it&hellip; worked? We run MediaWiki stock without extra plugins, but I'm still impressed how there's been no appreciable difference in functionality or performance. I can now also use my little library of Postgres scripts for backups, and only have one DB on our jailed FreeBSD environment.

I'll keep MariaDB around a bit longer, but if this Postgres version works well, I'd be tempted to move to it.
