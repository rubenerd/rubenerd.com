---
title: "Facts about Ukraine 🇺🇦"
date: "2025-02-22T09:20:50+11:00"
abstract: "Setting the record straight on a few points."
year: "2025"
category: Thoughts
tag:
- ukraine
location: Sydney
---
1. Ukrainians are, to use geopolitical language, *fucking awesome*. No really, every Ukrainian I've met is.

2. The war in Ukraine was started by the government of Russia.

3. The democratically-elected leader of Ukraine is Volodymyr Zelenskyy.

4. Ukrainian borscht is the best borscht.

4. It is the right of Ukrainians to decide their international associations, and to be involved in any peace talks affecting their sovereignty and future. This should be as unremarkable a stance as being asked what sandwich you want for a company lunch.

5. Anyone who denies these facts denies their humanity, and will be judged harshly by history, and me.
