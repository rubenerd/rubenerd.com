---
title: "A submission for CRANK: Restaurant QR codes"
date: "2025-02-23T06:39:30+11:00"
abstract: "Answering the bat signal from Deborah Pickett!"
year: "2025"
category: Thoughts
tag:
- crank
- food
location: Sydney
---
I received the [bat signal from Deborah Pickett](https://old.mermaid.town/@futzle/114043937275420976) regarding submissions for a revived *CRANK*. Let's do this!

Food delivery was a lifeline during Covid lockdowns. It allowed restaurant staff to cater to people at a distance, and spared those of us with hectic jobs the thought of reheating another frozen meal. Those delivery drivers also went above and beyond, leading me to quip at the time they should have received *Australian of the Year*.

Assuming people liked this arrangement, techbros decided to bring that feeling of isolation and dread into the modern retail experience. Replacing the ubiquitous and time tested *menu* with another rent-seeking mobile app, restaurants were convinced that it was in their best interests to require their patrons scan a QR code to browse, customise, and order their comestibles without talking to staff or feeling good about themselves.

These QR code apps have all the joy of ordering a database licence from a distributor. They bring the experience of selecting programmes on a mediocre Smart TV to food. *Tap, scroll scroll scroll, where is it? Is it a side or a main? Tap tap, scroll scroll scroll. No, decaf isn't an option for the cap, it looks like it's a separate item? Wait, but how do I... sorry I think the site just went down.*

Did your therapist say you spend too much time on mobile phones? Do you not have one at all? What about arthritis, or carpel tunnel, or another condition that makes scrolling painful or frustrating? *No soup for you!*

I propose a solution, and it comes from the ballot box and banks of yore: paper forms. You can have a list of every food and beverage on offer, printed out with every SKU available (because of course you'd be referring to your food with SKUs). Heck, it can even have small photos too! Customers can then circle the specific digestible substance to shove in their faces, and present it to you without further interaction. Restaurants get to maintain their disdain for customers without paying *yet another* subscription fee to a tech company, and their patrons can continue to avoid humanity as much as possible.
