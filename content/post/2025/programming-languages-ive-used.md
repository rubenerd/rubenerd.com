---
title: "Programming languages I’ve used"
date: "2025-01-22T20:01:10+11:00"
abstract: "A bunch of random stuff starting with BASIC and Pascal :')."
year: "2025"
category: Software
tag:
- programming
location: Sydney
---
I'm not sure what the spark was for this post; maybe it was my [recent adventures with Python](https://rubenerd.com/a-perl-guy-learning-python/ "A Perl guy learning Python"). I haven't been a full-time developer for a while, and I'll be there are more languages I've forgotten. I started writing these in order of when I first used them, but then went a bit all over place.

Wow, I'm really selling this!


### Used frequently

* **BASIC**. I was born too late to experience BASIC on an 8-bit home computer when they were new, but all my first programs were written in the QuickBASIC bundled with MS-DOS. It was fun!

* **Pascal**. My first real programming love. I learned of it from Microsoft’s short-lived QPascal we got to tinker with at school. It was more capable than BASIC, but I didn’t feel as though it was any more difficult? Maybe it benefited from being my second language, not my first.

* **Visual Basic 5/6**. This is what they taught us in high school. It was so instantly validating being able to drop controls onto a form, click them, and add event driven code without worrying about listeners and so on. Then VB.NET came along, and it wasn’t fun anymore.

* **C++**. Yup, I learned this before C! My dad picked me up a copy of Borland C++Builder which came with a great instruction book. I made some horrendous code, but the Borland IDE made GUI development approachable like VB. Which reminds me, the next one is…

* **Delphi**. Still easily my favourite language for rapid GUI development. Delphi takes its cues from Pascal. That reminds me, I need to resume my fun personal project from this one day.

* **Perl**. A boss at my first IT job showed me the ropes, and I still use it almost daily, even now. Few technologies have had as profound an impact on my life, for which I’m grateful. Thanks Larry!

* **Java**. Don’t tell anyone, but I enjoyed Java at university. I tend to be a verbose person in my method and variable naming, so Java’s massive ones made me feel right at home. Swing was a pain for GUI apps at first, but I got the hang of it. Ain’t no Delphi through!

* **Ruby**. I briefly had a blog where I just talked about learning Ruby. A lot of what I loved in Perl was there, but it also made a lot of things easier. I never really got into Rails though, but ActiveRecord was nice.


### Tinkered with

* **Excel Macros/VBA**. Did for some high school work experience to automate part of a payroll system. It even connected to Access! I’ll bet none of that runs now. Or at least, I hope it doesn't.

* **REXX**. This was an interesting addition to PC DOS after the Microsoft-IBM split. It definitely felt BASIC-adjacent, but I thought the syntax was easier to grok? Might just be me. I might try writing some more on my Japanese 486.

* **PL/SQL**. The procedural programming language included with Oracle. I did a stint with it at work, and for a uni assignment. Felt right processing data in the DB before shipping it elsewhere.

* **JavaScript**. Briefly. First language I actively disliked. What a sackbutt. TypeScript is somehow even worse.

* **C**. Would probably be more accurate to say **ncurses**, which came with some helpful guiderails. ncurses was fun!

* **C#**. Did one assignment on it in uni, after translating stuff in my head from VB.NET (see above). Liked Java more. Sorry, Ari Bixhorn.

* **Objective C**. I didn’t have much of an interest in making Mac software per se, but I loved OpenSTEP and all those goodies. The idea of Smalltalk messages with C felt so much nicer and more natural than C++.

* **Python**. Only finally getting around to it this year. It’s definitely a departure from Perl, but I'm enjoying it a *lot* more than I thought.

* **Lua**. Dabbled a bit for Wikipedia, of all places. It's intriguing.

* **6502 and Z80 assembler**. Only done the absolute basics, but they've both scratched a mental itch nothing else has for a long time. Doing both at the same time also makes certain decisions behind the architectures really interesting. I'd love to do some more during my long service leave this year, maybe for the Commodores or Apple //e.
