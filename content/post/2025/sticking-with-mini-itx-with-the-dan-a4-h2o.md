---
title: "Sticking with Mini-ITX with the cute Dan A4-H2O"
date: "2025-02-25T10:18:26+11:00"
abstract: "I was won over by another case that actually works better than the previous one."
thumb: "https://rubenerd.com/files/2025/a4h2o-beigetastic@2x.jpg"
year: "2025"
category: Hardware
tag:
- cases
- design
- mini-itx
location: Sydney
---
Last December I wrote a meandering post describing the [challenges with Mini-ITX builds](https://rubenerd.com/bidding-farewell-to-mini-itx-maybe/ "Bidding farewell to Mini-ITX and SFF, maybe"). While the form factor lets you build in beautiful little cases like my [old NCASE M1](https://rubenerd.com/mini-itx-cases/), there are financial, thermal, and feature compromises that may prove limiting, especially when it comes to upgrades and future expansion. You're essentially engineering yourself into one of eight physical case corners, albeit cute ones.

I'm not sure if I was trying to convince myself or you, now that I've read it back. Because as I've [learned with cameras again](https://rubenerd.com/the-om-system-om-3-is-the-camera-of-my-dreams/ "The OM SYSTEM OM-3 is the camera of my dreams") recently, the [best choice isn't always](https://rubenerd.com/your-favourite-doesnt-have-to-be-the-best/ "Your favourite doesn’t have to be the best") the... best choice. Or more specifically, what you might measure "best" against may not be the same as what other people choose. Nobody knows your circumstances, priorities, and tastes better than you, despite what others may assert by decrying your preferences as illogical.

But I'll admit I left something out of that Mini-ITX post: I'd bought a larger Micro-ATX case. I was still using my Mini-ITX Ryzen board inside, but at least I had a future upgrade path that was more flexible, cost effective, and had a greater ceiling for active cooling like radiators and fans. It was bigger, easier to build in, and made rational sense. It was even beautiful in its own right, with its light wood accents and rounded edges. Maybe I'll do a post about it at some point.

Alas, then I'd look over at that little [NCASE M1 I bought in 2016](https://rubenerd.com/mini-itx-cases/) and gave to Clara last year, and everything came back. I loved how little space it took up on the desk, how *precisely* everything fit, and that minimal design language. Deep down inside I realised I still wanted something that matched. So I pounced when I saw a [Dan/Lian-Li A4-H2O](https://lian-li.com/product/a4h2o/) at a steep discount at a local distributor.

Pictured below is the beautiful but large A3-mATX (left), the new A4-H2O (centre), and Clara's NCASE M1 (right)\:

<figure><p><img src="https://rubenerd.com/files/2025/case-comparison@1x.jpg" alt="Photo of Clara's and my work desk, showing the above cases. The A4-H2O and M1 are significantly smaller." srcset="https://rubenerd.com/files/2025/case-comparison@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

The history of Dan and NCASE is beyond the scope of this post, but the family resemblance is undeniable. The A4-H2O and the M1 both use aluminium panels with tool-less snaps that attach to a black inner frame. They're lightweight but surprisingly rigid. And while the fit and finish aren't at the level of an Apple Mac Pro of yore, it hits that similar aesthetic I love.

The A4-H2O employs a "sandwich" layout, similar to the older Dan A4-SFX I [talked about back in the day](https://rubenerd.com/mini-itx-cases/). For those unfamiliar, sandwich layouts are delicious. They generally consist of a central bracket upon which everything is mounted, and which serves to split the case in two. This helps isolate the two hottest components of a gaming PC&mdash;the CPU and the graphics card&mdash;into their own thermal zones, and permits the case to be shrunk horizontally while still supporting the same hardware. A riser cable is used to connect the graphics card in the second chamber across to the motherboard in the first, as this diagram from Lian-Li demonstrates:

<figure><p><img src="https://rubenerd.com/files/2025/a4h2o-inside@1x.jpg" alt="Opened view of the case showing the liquid cooler installation" srcset="https://rubenerd.com/files/2025/a4h2o-inside@2x.jpg 2x" style="width:500px; height:355px;" /></p></figure>

The name A4-H2O also highlights its specific use case. I'm currently using the same slim CPU air cooler as I was in the Fractal Ridge for now, but my aim is to use the recommended 240 mm liquid-cooled AIO radiator and fans mounted to the top when finances permit. This may be overkill for my older 65 watt Ryzen 5700X, but that higher thermal ceiling opens up a whole class of mid-tier CPUs that I wouldn't have been game to put into other cases of a similar size. A 5800X3D, or some 9000-series AMD silicon? Who knows!

The case design also came with a couple of other unexpected surprises. Because the graphics card is mounted vertically against the case, the fans can pull in fresh air directly. Despite this case having less than *half* the volume of the Micro-ATX case it's replacing, the GPU actually runs cooler and quieter here. Thanks to some clever layout decisions, I also found tidy cable management was easier in this than cases twice the size, despite what some YouTube channels suggested.

I had my entire machine moved across to the A4-H2O within twenty minutes. Mini-ITX cases can be painful owing to their their tight tolerances, but this was *surprisingly* easy. About the only thing that caused mild frustration was the CPU power cable for the motherboard, which I had to detach and move to a closer port on the SFX power supply for it to reach.

<figure><p><img src="https://rubenerd.com/files/2025/a4h2o-beigetastic@1x.jpg" alt="The A4-H2O alongside an IBM Aptiva from 1999, and my Shinri HOLOSTARS mug." srcset="https://rubenerd.com/files/2025/a4h2o-beigetastic@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

It's probably too obvious to state, but Mini-ITX isn't for everyone. Being a FreeBSD and NetBSD desktop user&mdash;heck, even Linux to be honest&mdash;forces you to be a bit more deliberate with your part choices, and it's the same with builds like this. As I said in that original Mini-ITX post, I can only do this because I have an Antec 300 in the other room with half a dozen hard drives, almost as many SSDs, and all our home server stuff. That lets this machine be my cute little workstation and game PC, in a case that now matches my beloved NCASE M1 of yore.

If you have a mid-tier CPU, a 3-slot or smaller graphics card, a plan to use an AIO for liquid cooling, and enjoy that perforated aluminium aesthetic of these sorts of cases, I can't recommend the A4-H2O highly enough. The NCASE M2 may be a better fit if you deviate even slightly from this "Goldilocks" configuration, but otherwise I think this looks, feels, and acts more like a true NCASE M1 successor.

I was lucky I was able to scoop one up on special, because they're getting harder to find in Australia. I'm not sure if they're about to be discontinued, but I'd keep an eye out if you're interested.
