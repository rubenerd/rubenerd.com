---
title: "Trying ALDI coffee beans"
date: "2025-02-05T09:05:55+11:00"
abstract: "They might be my new freezer backup."
year: "2025"
category: Thoughts
tag:
- coffee
location: Sydney
---
Long-term readers know I love ALDI. With a few rare misses, the German discount supermarket chain has stuff as good as the others, at better prices, and in a well-designed environment that doesn't trigger anxiety like the blaring lights and music of Colesworth. I wrote more about the [enduring appeal of Aldi](https://rubenerd.com/the-enduring-appeal-of-aldi/) back in June last year.

But I hadn't ever tried their coffee beans before. I've been spoiled by beautiful single origins that are roasted within walking distance, but when money is tight I couldn't resist. Could it be any good?

Recently I bought a bag of [ALDI Lazzio Medium beans](https://www.aldiunpacked.com.au/aldi-australias-coffee-crowned-worlds-best-at-the-superbowl-of-coffee-awards/), which was a good sign. Most supermarket coffee is far too dark and bitter for the way I drink black coffee, which gives them a burnt taste. I still expected these to be darker than I'd like, but I'm also generally fine with medium roasts.

I dialled our new Baratza ESP grinder a step coarser to compensate, but otherwise used the same AeroPress and Clever Coffee Dripper recipes I use for those lighter roasts. Then I braced for impact!

<figure><p><img src="https://rubenerd.com/files/2025/aldi-coffee@1x.jpg" alt="Press photo from ALDI showing a range of their beans." srcset="https://rubenerd.com/files/2025/aldi-coffee@2x.jpg 2x" style="width:500px;" /></p></figure>

I wasn't sure what to expect. ALDI stuff is sometimes... fine. Other times I'm shocked at how exceptionally good it is. Very occasionally, it's dreadful. I'm sad to say that I disliked all the Expressi pod system coffees I tried, after having more than a few in a share office. But then, their instant which I make in a pinch&mdash;sorry James!&mdash;is better than I expected.

The smell coming from the grinder and the fresh cup of coffee itself were both  wonderful, just as rich and mellow as some of the single origins I've had. And the taste was... *surprisingly good.* There was little in the way of interesting acidity or flavour notes (which *wasn't* surprising) but it wasn't bitter at all (which *was* surprising). It was very easy to drink, even black.

I'm always after a freezer "backup" coffee I can stash away for those times when I run out of the good stuff, or when the roasters down the road are closed for public holidays, and so on. I think this might be it.
