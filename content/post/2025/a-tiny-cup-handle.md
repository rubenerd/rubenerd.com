---
title: "A tiny cup handle"
date: "2025-02-15T15:40:57+11:00"
year: "2025"
category: Hardware
tag:
- coffee
- design
- ergonomics
location: Sydney
---
Clara and I went to get some encouragement coffee and cake after the last couple of days of online adventures, and the cups have the tiniest handles I've ever seen. Note the small teaspoon on the saucer for reference:

<figure><p><img src="https://rubenerd.com/files/2024/tiny-handle@1x.jpg" alt="Photo of a coffee cup with the world's tiniest handle" srcset="https://rubenerd.com/files/2024/tiny-handle@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

The best we could do is hold the handle between our index finger and thumb, as though the hole wasn't there. It made us question why the hole, or indeed the handle, was there in the first place. I mean, it's *adorable*, but still. Maybe we have our answer there.

German beer steins have the best handles. They're big, sturdy, and accomodate hands of all shapes, sizes, and muscular capabilities. Coffee cups need the same thing!
