---
title: "Tell us something good"
date: "2025-02-13T09:42:14+11:00"
abstract: "A list inspired from a recent MetaFilter thread."
year: "2025"
category: Thoughts
tag:
- coffee
- pompompurin
- retrocomputing
- vocaloid
location: Sydney
---
[MetaFilter gave me the idea](https://www.metafilter.com/207595/Tell-us-something-good-a-free-thread) for this one. Turns out I had several things:

* My little external mouse has had the unintended side-effect of removing pain in my right hand when working remote. This dead touchpad may have been a blessing in disguise!

* This iced long black I'm having right now is delicious. So good. It's like coffee, but cold, and refreshing. Ice.

* I think I was mentioned on Lobsters and Hacker News again, but most of the resulting email wasn't rude this time. The relief is immesurable.

* There's a new [Vocaloid/Sanrio character crossover](https://hatsunemiku-cinnamoroll-minmarche.com/)! Pompompurin as Kagamine Rin is amazing, especially given he's the [best character](https://wiki.sasara.moe/Pompompurin).

* I found my [Monotech DoubleROM](https://monotech.fwscart.com/product/doublerom---ide-size-limit-remover---dual-bootable-rom-isa-card) XT-IDE card in a box, which means I can boot my [Am386](http://retro.rubenerd.com/dfi386.htm) with storage again. *Boom*.

* I had to postpone my long service leave by a month, which sucks. But I'll now be able to go to Japan during it, which is a win. And maybe get a new camera of a specific variety, which is cough.
