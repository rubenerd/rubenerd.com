---
title: "In Sydney? Want to buy a 2017 Suzuki GSX-R600?"
date: "2025-02-13T09:22:43+11:00"
abstract: "My friend is selling."
thumb: "https://rubenerd.com/files/2025/en9odhw306ertakpkk5u0jzc1@2x.jpg"
year: "2025"
category: Thoughts
tag:
- selling
location: Sydney
---
[A friend of mine is selling](https://www.bikesales.com.au/bikes/details/2017-suzuki-gsx-r600/SSE-AD-5570509) a handsome dual-wheeled motorised conveyance:

> This 2017 Suzuki GSX-R600 is kept in amazing condition with extremely low km's on it, so there is plenty of time to enjoy this amazing bike throughout the summer year! There is nothing wrong with the bike, always kept in a garage. I'm selling it only because i will be moving and won't have anywhere to store it.
> 
> The bike comes pre-installed with a M4 Slip-on exhaust, Oggy Knobbs, Gas cover protector sticker, Tank Grip, R&G Tail tidy. I do have the stock exhaust and stock tail tidy if required.

Here's a [photo from the listing](https://www.bikesales.com.au/bikes/details/2017-suzuki-gsx-r600/SSE-AD-5570509), and for no reason a tasty, <strong><em>TASTY</em></strong> florenine you could be enjoying after riding to a coffee shop on your new bike. 

<figure><p><img src="https://rubenerd.com/files/2025/en9odhw306ertakpkk5u0jzc1@1x.jpg" alt="Photo of the aforementioned motorbike" srcset="https://rubenerd.com/files/2025/en9odhw306ertakpkk5u0jzc1@2x.jpg 2x" style="width:450px;" /><br /><img src="https://rubenerd.com/files/2025/florentine@1x.jpg" alt="Photo of a TASTY florentine" srcset="https://rubenerd.com/files/2025/florentine@2x.jpg 2x" style="width:450px;" /></p></figure>

Do it. You know you want to. Ride the bike, get the florentine. Zoom zoom, yum yum. 🏍️ 🍪

