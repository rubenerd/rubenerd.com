---
title: "The Kinesis mWave Keyboard"
date: "2025-01-30T10:28:27+11:00"
abstract: "An ode to the Microsoft Natural Keyboard, but with mechanical Gateron switches!"
year: "2025"
category: Hardware
tag:
- ergonomics
- keyboards
location: Sydney
---
I've talked here about my preference for split keyboards, though I [currently don't use one](https://rubenerd.com/typing-on-my-new-gateron-north-pole-2-keyboard/ "Typing on my new Gateron North Pole 2.0 keyboard"). I *loved* my family's original Microsoft Natural Keyboard in particular, and I [used the 2019 model](https://rubenerd.com/the-microsoft-ergonomic-keyborad-2019/) for several years in spite of its mushy keys.

Kinesis, known for their ergonomic kit, has announced a mechanical board with a similar split design to the *Natural Keyboard*. This is the Mac layout:

<figure><p><img src="https://rubenerd.com/files/2025/kinesis-natural-mac@1x.jpg" alt="A white split keyboard in the design of more modern Natural keyboards from Microsoft." srcset="https://rubenerd.com/files/2025/kinesis-natural-mac@2x.jpg 2x" style="width:500px; height:304px;" /></p></figure>

And the PC layout:

<figure><p><img src="https://rubenerd.com/files/2025/kinesis-natural-pc@1x.jpg" alt="A black split keyboard in the design of more modern Natural keyboards from Microsoft." srcset="https://rubenerd.com/files/2025/kinesis-natural-pc@2x.jpg 2x" style="width:500px; height:304px;" /></p></figure>

They've nailed the aesthetic! It has the same integrated palmrest, key layout, split spacebar, and modifier key positioning, along with a separate numpad like later models. It's like I've stepped into an alternate reality where Microsoft card about keyswitch quality, instead of opting for laptop scissor keys or mushy rubber domes.

My heart sank a bit when I saw it was Bluetooth, but this can also be swapped out in the best possible way:

> If you prefer to use the keyboard in a wired configuration, you can connect the included USB-C to USB-A cable and run the keyboard in USB mode with the keyboards radio disabled full-time.

The only downside (for me) is their choice of switch. Kinesis went for low-profile Gateron "Chocolate" mechanical switches which, if you're familiar with Cherry keys of yore, indicates its a tactile mechanism. Gateron made me a linear convert with their *stunning* Milky Yellows, and their North Pole 2.0 switches are the closest anyone has got to my beloved Topres in feel. I'll hold out hope that they'll come out with another SKU in the future.

Honestly though&mdash;as opposed to all those other times?&mdash;I think I'd still be tempted to give it a try. My favourite keyboard layout of all time, wed to mechanical switches, would be spectacular. We're saving for a trip right now, but this might go on the list for later this year when they're released.
