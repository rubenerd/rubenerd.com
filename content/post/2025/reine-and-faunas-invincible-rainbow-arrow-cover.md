---
title: "Reine and Fauna’s Invincible Rainbow Arrow cover!"
date: "2025-01-06T08:33:35+11:00"
abstract: "AAAAA! This was the best send-off we could have asked for :')."
thumb: "https://rubenerd.com/files/2025/reine-fauna-hats@2x.jpg"
year: "2025"
category: Media
tag:
- pavolia-reine
- ceres-fauna
- hololove
- music
- music-monday
location: Sydney
---
Today's *[Music Monday](https://rubenerd.com/tag/music-monday/)* is something really special. 💙💚

I was saving this from last week, but my oshi [Pavolia Reine](https://www.youtube.com/@PavoliaReine/streams "Pavolia Reine's YouTube channel") from Hololive Indonesia did a send-off with [Ceres Fauna](https://rubenerd.com/faunas-farewell-stream/ "Fauna's farewell stream") in a cover of *Invincible Rainbow Arrow*, and it's everything fans of both them could have hoped for! 

<figure><p><a target="_BLANK" href="https://www.youtube.com/watch?v=JHp-EB2jFn0" title="Play 【Cover】Invincible Rainbow Arrow - 虹ノ矢ハ折レナイ(英語版) / Pavolia Reine × Ceres Fauna"><img src="https://rubenerd.com/files/2025/yt-JHp-EB2jFn0@1x.jpg" srcset="https://rubenerd.com/files/2025/yt-JHp-EB2jFn0@1x.jpg 1x, https://rubenerd.com/files/2025/yt-JHp-EB2jFn0@2x.jpg 2x" alt="Play 【Cover】Invincible Rainbow Arrow - 虹ノ矢ハ折レナイ(英語版) / Pavolia Reine × Ceres Fauna" style="width:500px;height:281px;" /></a></p></figure>

For context, Reine and Fauna bonded early over their love of *AI: The Somnium Files*. To hear Fauna sing one last time, and with Reine, and to the most important tune from that game... it was the ultimate fan send-off. The art, the references, everything, **AAAAAAH!**

Reine has a natural chemistry with everyone she collabs with, which is part of what makes watching her streams so fun. But there was something special when she paired with Fauna. Their [Indonesian lesson](https://www.youtube.com/watch?v=6qxx91dcB8Y) was the most fun of any of EN, and their legendary, spicy-fanfic Minecraft stream [sharing those mothertrucker fishing hats](https://www.youtube.com/watch?v=cLDxyxMup80) was *top tier*. I know I've loved a stream when I go back and revisit it from time to time.

<figure><p><img src="https://rubenerd.com/files/2025/reine-fauna-id@1x.jpg" alt="Reine and Fauna's Indonesian lesson!" srcset="https://rubenerd.com/files/2025/reine-fauna-id@2x.jpg 2x" style="width:500px; height:280px;" /></p></figure>

Part of what made me sad when [Watson graduated](https://rubenerd.com/goodbye-amelia-watson/ "Goodbye, Amelia Watson") was the thought of never seeing her collab with Reine and the other girls again, which I suppose is the same with Fauna now. But I'm glad they parted in the best way possible. I don't think fans could have asked for anything moire.

<span lang="id">Terima kasih banyak, Reine dan Fauna</span>. You were the best. 🙏

<figure><p><img src="https://rubenerd.com/files/2025/reine-fauna-hats@1x.jpg" alt="Reine and Fauna in their epic fishing hats" srcset="https://rubenerd.com/files/2025/reine-fauna-hats@2x.jpg 2x" style="width:500px; height:280px;" /></p></figure>
