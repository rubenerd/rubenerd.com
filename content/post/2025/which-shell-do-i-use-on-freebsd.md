---
title: "Which shell do I use on FreeBSD?"
date: "2025-01-12T09:21:12+11:00"
abstract: "I used to run tcsh(1), but now I’m mostly on the Portable OpenBSD Korn Shell."
year: "2025"
category: Software
tag:
- bsd
- freebsd
- oksh
location: Sydney
---
This has come up a few times, but [Steven G. Harms](https://techhub.social/@sgharms/113807082543736061) was the latest to ask. Not that I need an excuse to write about BSD, mind!

I ran `tcsh(1)` from base as my shell for most of my time as a handsome&mdash;or not&mdash;FreeBSD user. I prefer the C-style syntax for dotfile configuration and scripting, even if its use was "widely considered harmful". I just found it more natural to read and write, though I've also said that about Perl, so take that how you will.

I'm also a firm believer in **getting good at the defaults**. Until recently, `tcsh(1)` was the default shell for root, meaning you were guaranteed to be dropped to it if you were tasked with logging into a FreeBSD machine somewhere. That is, unless someone changed the shell, hopefully to something else from base lest the ports system break! Ask me how a younger and more reckless Ruben learned that the hard way.

I'm glad I spent the time learning the muscle memory for basic loops and other structures in `tcsh(1)`, just as I took the time to [learn nvi back in the day](https://rubenerd.com/graduating-from-nvi-kinda/ "2010: Graduating from nvi, kinda"). It sucks being in a situation where you have to fix something, and are stymied by an unfamiliar environment or set of tools you can't easily use. This is also why I'm a fan of rational, reasonable defaults, but that's a post for another time.

Today, I use two different shells depending on context. I use plain `sh(1)` for servers and remote machines that I won't otherwise be regularly interacting with because, again, it's now the default. Scripts written against this are also easier to be made portable, so I can run them on NetBSD, illumos, or Linux. If I do need to something more complicated, I still have `tcsh(1)` to fall back on (though I find myself reaching for it less and less now, which is bittersweet).

*(Thanks to dear Internet friend [Tim Chase](https://mastodon.bsd.cafe/@gumnos)&mdash;gah not Jim!&mdash;for pointing out that I meant "remote" there, not "remove"! Me, make a spelling mistake on my blog? That's unpossible).*

For regular interactive use on desktops and laptops on FreeBSD, NetBSD, and even Penguins though, I use the [Portable OpenBSD Korn Shell](https://www.freshports.org/shells/oksh/), or `oksh(1)`. This is the variant of `ksh(1)` on OpenBSD, and its default shell. It's fast, well documented, implements the subset of interactive features I actually use, and is easily customised with dotfiles using years of `ksh(1)` examples. It's another tool alongside OpenSSH, LibreSSL, and `doas(1)` from OpenBSD I benefit from, even though I don't run the OS. I hope to add their permissive-licenced Game of Trees and `openrsync(1)` to that list at some point too.

Nothing too exotic I suppose, but it works great for me :).

