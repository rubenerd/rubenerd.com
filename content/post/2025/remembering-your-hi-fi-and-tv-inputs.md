---
title: "Remembering our hi-fi and TV inputs"
date: "2025-01-08T06:56:19+11:00"
abstract: "Going down the route of stickers, before printing a guide instead. George Credenza."
thumb: "https://rubenerd.com/files/2025/george@2x.jpg"
year: "2025"
category: Hardware
tag:
- hi-fi
- playstation-3
- retrocomputers
location: Sydney
---
Clara and I are faced with a predicament thanks to our renewed interest in [hi-fi](http://retro.rubenerd.com/hifi.htm "Ruben's Retro Corner: Hi-Fi") and [retro(ish) console gear](https://rubenerd.com/consoles-and-the-playstation-3/ "Consoles, and the PlayStation 3") in the lounge. Namely, how do we track what device is connected to what input? There are three key challenges:

* The TV only has inputs numbered `1-8`, which isn't helpful. Some devices publish a name via the HDMI spec, such as the AppleTV. But everything else just appears as a number.

* Our beautiful [1983 stereo JVC amplifier](https://www.hifiengine.com/manual_library/jvc/a-k100.shtml) either wasn't intended for use with certain devices, or it predates them by many years. This means the labels on the input buttons don't always correspond to what the connected device is. Save for the onboard preamp hardwired to the vinyl input, these labels are arbitrary anyway.

* This bullet point is after the previous one.

I've tried to work around these in what I'd call *dubiously creative* ways. The amplifier has a **Radio Tuner** button, which I replace in my head with **TV Tuner** as I look at it. **DAD/AUX** looks close enough to DVD, so we can use that for our DVD/CD changer. And **Tape Monitor**, well connecting that to a tape deck via a graphic equaliser was probably expected, even if I sometimes forget that our Pioneer GR-777 doesn't have passive pass-through, meaning you can start a tape and hear nothing if you forget to turn it on in the chain. I'd say *but I digress*, but you probably can't hear it.

I've also tried to match names to numbers on the TV. For example, the **PlayStation 3** is on TV input `3`, and the **PlayStation 4** is on&mdash;say it with me now&mdash;TV input `5`. Though I intend to connect it to input `4` once I get a replacement HDMI cable of sufficient length to reach that far in what we dub our *George Credenza*.

<figure><p><img src="https://rubenerd.com/files/2025/george@1x.jpg" alt="George Costanza is unimpressed" srcset="https://rubenerd.com/files/2025/george@2x.jpg 2x" style="width:400px; height:197px;" /></p></figure>

Thank you.

Still though, is a phrase with two words. Is there a better way to do this? Is there a way for us to remember what is plugged into where without performing minor feats of mental gymnastics every time I want to watch an anime about gymnastics? Actually I don't think I've watched an anime about gymnastics, though given the explosion of so-called *sports anime* of late, I'm sure it either exists or will soon. Fingers crossed it's of the *Yuri on Ice* variety.

The most obvious solution would be to *label* the devices with their respective inputs. This would involve affixing labels on the amplifier input buttons for the TV and DVD/CD changer. For the TV inputs, those are selected with the remote control, so I'd probably need to label the end devices themselves.

Unfortunately, labels come with a host of issues:

* Most of our devices are black, and the label printer we have to print labels&mdash;thanks Ruben, that clarification was extremely helpful&mdash;doesn't offer white-on-black label cassettes. White or a gaudy colour would look really cheap, and would stick out like a [sprained index finger](https://rubenerd.com/spraining-a-finger/ "Spraining a finger").

* Labels leave sticky residue behind, which would be a pain to clean if we ever had to remove, replace, or update them. I also don't want to damage the surface on some of our older components.

* The textured plastic on some of the components also isn't the most hospitable surface for affixing stickers. The PlayStation 4's label has already fallen off so many times I've given up. I tried so hard to come up with a pun invovling DualShock and labels, but I couldn't make anything stick.

**AAAAAA!**

<figure><p><img src="https://rubenerd.com/files/2025/george@1x.jpg" alt="George Costanza is unimpressed" srcset="https://rubenerd.com/files/2025/george@2x.jpg 2x" style="width:400px; height:197px;" /></p></figure>

Which leads me to the ultimate solution that seems so obvious in retrospect, I feel like a blithering fool for not doing it from the start. *Have a card on the coffee table with the inputs on it*.

So I threw this together and printed it:

<pre style="line-height:1em! important">
╔═════════════════════════════════════════════╗
║ KIRIBEN ENTERTAINMENT SYSTEM LAYOUT… SYSTEM ║
╚═════════════════════════════════════════════╝
╔═════════════════════════════════════════════╗
║ AMPLIFIER                                   ║
╟──────────────┬──────────────────────────────╢
║ Tape Monitor │ Graphic equaliser, tape deck ║
║        Phono │ Vinyl turntable              ║
║        Tuner │ Television                   ║
║      DAD/AUX │ CD/DVD changer               ║
╚══════════════╧══════════════════════════════╝
╔═════════════════════════════════════════════╗
║ TELEVISION                                  ║
╟─────────────┬───────────────────────────────╢
║      HDMI 1 │ Raspberry Pi                  ║
║      HDMI 2 │ AppleTV                       ║
║      HDMI 3 │ PlayStation 3                 ║
║      HDMI 4 │ PlayStation 4                 ║
║      HDMI 5 │ Nintendo Switch               ║
║       VGA 6 │ RetroTINK                     ║
║ Composite 7 │ VCR                           ║
║ Component 8 │ Nintendo Wii                  ║
╚═════════════╧═══════════════════════════════╝
╔═════════════════════════════════════════════╗
║ …SYSTEM                                     ║
╚═════════════════════════════════════════════╝
</pre>

**DISCLAIMERS:** Yes, I run the RetroTINK with an HDMI-to-VGA connector. Yes, I'm aware that negates half the point of having a digital upscaler and conveter for retrocomputers. Yes, it still works great. Yes I know we only have the CD/DVD changer connected to audio. Yes, this is because we only use it as a CD changer, and use the PlayStation 3 for DVDs and Blu-ray discs. Yes, I know I could use the PlayStation 4 for these as well. Yes, you can't spell "genius" without "me". Yes, that was a joke.

Now I'm thinking I'll get one of those little A5-sized plastic stands like you see at Daiso, and have it sitting on our coffee table. Which at this stage I feel like we should probably called Elaine Benes. Maybe I could get Clara to draw a chibi version of her and George for it, like she did for the site mascot. We could call the whole thing our Sign-feld.

**AAAAAAAAAAAAAAAAAAAAAA!**

<figure><p><img src="https://rubenerd.com/files/2025/george@1x.jpg" alt="George Costanza is unimpressed" srcset="https://rubenerd.com/files/2025/george@2x.jpg 2x" style="width:400px; height:197px;" /></p></figure>

