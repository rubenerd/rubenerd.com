---
title: "My A-Z toolbox: cowsay"
date: "2025-01-16T09:19:25+11:00"
abstract: "Life is too short not to have cowsay."
year: "2025"
category: Software
tag:
- az-toolbox
- whimsy
location: Sydney
---
This is the third post in my [A-Z Toobox](https://rubenerd.com/tag/az-toolbox/) series, in which I'm listing tools I use down the alphabet for no logical reason.

The letter C has the [certbot](https://certbot.eff.org/) utility which automatically provisions, allocates, and installs TLS certificates using Let's Encrypt that you can run via cron. Speaking of cron, there's [cron and crontab](https://man.freebsd.org/cgi/man.cgi?crontab), the age-old system utilities for scheduling tasks. There's [cdrtools](https://codeberg.org/schilytools/schilytools), which lets you author and burn optical disc images with options that make it suited to modern and legacy machines across a host of different OSs. [checkbashisms](https://linux.die.net/man/1/checkbashisms) is useful for determining if you've written your shell script in a portable fashion, which makes it useful for the BSDs and illumos, among others. And [calcurse](https://www.calcurse.org/) is a fun personal organiser that runs entirely out the shell.

But for me&mdash;as opposed to anyone else?&mdash;the tool that makes it to letter C in my toolbox is the venerable `cowsay`, seen here informing me what OS I'm running:

     _______
    < uname >
     -------
            \   ^__^
             \  (oo)\_______
                (__)\       )\/\
                    ||----w |
                    ||     ||

Wait, that's `$(cowsay uname)`, not `$(uname | cowsay)`. Let's try again: 

     _________
    < FreeBSD >
     ---------
            \   ^__^
             \  (oo)\_______
                (__)\       )\/\
                    ||----w |
                    ||     ||

`cowsay` is one of those utilities you don't need, and also desperately need at the same time. She imparts a sense of joy and levity into what may otherwise be a dry day of technical implementation. Ansible even supports her, and [buzzkills failed](https://github.com/ansible/ansible/issues/10530) in their attempt to remove her by default.

Life is too short to not have `cowsay`. Get her and be happy. Or as `cowsay` herself may say:

     _______
    < uname >
     -------
            \   ^__^
             \  (oo)\_______
                (__)\       )\/\
                    ||----w |
                    ||     ||

Wait, damn it.
