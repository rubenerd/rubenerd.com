---
title: "Watching Nimi Nightmare’s debut! 💭 #naplings"
date: "2025-01-19T09:45:17+11:00"
abstract: "It was so great to hear her voice again!"
year: "2025"
category: Anime
tag:
- dooby
- nimi-nightmare
- vtubers
location: Sydney
---
You know how I blogged recently about a certain VTuber's graduation from Hololive? Unrelated to this, [Nimi Nightmare](https://www.youtube.com/@niminightmare) debued! (<a href="https://www.youtube.com/feeds/videos.xml?channel_id=UCIfAvpeIWGHb0duCkMkmm2Q">Atom feed here</a>).

<figure><p><img src="https://rubenerd.com/files/2025/nimi-nightmare@1x.jpg" alt="Screengrab from her debut." srcset="https://rubenerd.com/files/2025/nimi-nightmare@2x.jpg 2x" style="width:500px; height:283px;" /></p></figure>

It was wonderful hearing her voice and jokes again. Her artists did an amazing job matching her design to her personality as well. [Caspurr Catachini](https://www.youtube.com/@CaspurrCatacini), Clara's and my favourite male VTuber, also lent his creative skills with her lore video which was a fun easter egg.

Based on the fact her merch sold out and had to be changed to pre-order within an hour of her debut, I don't think I need to wish her any luck or success. But I will anyway :).

Apropos of nothing, I hope she's able to collab with [Dooby](https://www.youtube.com/@dooby3d) soon (shifty eyes). 💛💚
