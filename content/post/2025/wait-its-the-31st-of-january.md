---
title: "Wait, it’s the 31st of January!?"
date: "2025-01-31T07:06:11+11:00"
abstract: "We’re almost into the second month of 2025, already."
year: "2025"
category: Thoughts
tag:
- dates
- pointless
location: Sydney
---
I saw that date under the title of my previous post as I was drafting it, and did a double-take. We're almost into the second month of 2025. Already. What the `fsck(8)`? We'd better get the Xmas decorations down soon.

I'm proud that Clara and I had five key objectives for the year, and we've now achieved one of them this month. We should be done by May at this rate! Don't quote me on that.

Here's a stock image of a duck.

<figure><p><img style="width:160px;" src="https://rubenerd.com/files/2023/duck.gif" alt="Woof" /></p></figure>
