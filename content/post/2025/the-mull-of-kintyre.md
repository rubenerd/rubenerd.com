---
title: "The Mull of Kintyre"
date: "2025-02-10T09:14:18+11:00"
abstract: "“It's amazing how a song written by two English men could make a Scots guy like me feel so patriotic.”"
year: "2025"
category: Media
tag:
- family
- music
- music-monday
- scotland
location: Sydney
---
Today's *[Music Monday](https://rubenerd.com/tag/music-monday/)* is another Paul McCartney-adjacent tune, this time with Linda and Denny Laine again in their Wings days. May they rest in peace.

<figure><p><a target="_BLANK" href="https://www.youtube.com/watch?v=Plhtk_XJqhM" title="Play Wings - Mull Of Kintyre"><img src="https://rubenerd.com/files/2025/yt-Plhtk_XJqhM@1x.jpg" srcset="https://rubenerd.com/files/2025/yt-Plhtk_XJqhM@1x.jpg 1x, https://rubenerd.com/files/2025/yt-Plhtk_XJqhM@2x.jpg 2x" alt="Play Wings - Mull Of Kintyre" style="width:500px;height:281px;" /></a></p></figure>

I thought Pete's comment was especially apt!

> It's amazing how a song written by two English men could make a Scots guy like me feel so patriotic.

My late mum's side of the family are from Scotland. I need to take Clara there one day. 🏴󠁧󠁢󠁳󠁣󠁴󠁿
