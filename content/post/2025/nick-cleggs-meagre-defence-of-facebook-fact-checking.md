---
title: "Nick Clegg’s meagre defence of Facebook fact checking"
date: "2025-01-23T08:19:46+11:00"
abstract: "“Look at the substance”"
year: "2025"
category: Internet
tag:
- news
- social-media
location: Sydney
---
There's that old adage that you can tell a politician, or ex politician, is lying based on whether their mouth is moving. I don't buy that; dismissing all politicians as liars grants lying politicians cover.

Here comes the proverbial posterior prognostication... *but*, I have my own tells for when a politician wants to obfuscate. One of them is when they invoke phrases such as "look at the substance". This achieves two things:

* It redirects attention away from specific points you find uncomfortable, and onto ones you want to discuss.

* It gaslights those who *have* looked at the substance of a proposal into thinking they're the problem. You must have misread it, or glossed over something, or didn't interpret it correctly.

Which leads us to Nick Clegg, former British deputy prime minister who gave David Cameron another crack at power. You know, the gent responsible for Brexit that even *he* didn't want, but thought would improve his polling numbers. Smashing!

[Here's The Guardian](https://www.theguardian.com/technology/2025/jan/22/nick-clegg-metafacebook-instagram-factcheckers-wef-davos)\:

> “I would urge you to look at the substance of what Meta announced. Ignore the noise and the politics and the drama around it,” he said in comments to the World Economic Forum in Davos, insisting the new policy was “circumscribed and tailored”.

Facebook haven't earned the benefit of the doubt from us (I've argued this for decades, but I'm relieved more people are finally seeing the business for what it is). It's transparent what they're doing here; they're offloading responsibility of checking facts to the community.

Here's some substance for you to look at Mr Clegg: *you're a bellend!* Which is a shame, because your past positions regarding House of Lords reform were actually solid. Pity you made a deal with the devil and had to can them.

One question I haven't read any journalist ask: why can't you do both? There's this assumption that if you do community posts like Twitter has, the need for Facebook's own content moderation and fact checking is negated. I also reject this.
