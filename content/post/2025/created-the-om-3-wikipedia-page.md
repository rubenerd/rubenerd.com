---
title: "Creating the Om System OM-3 Wikipedia page"
date: "2025-03-02T08:48:46+11:00"
abstract: "If you have a Creative Commons-licenced photo of the camera itself, or further technical details, feel free to add."
year: "2025"
category: Hardware
tag:
- cameras
- wikipedia
location: Sydney
---
It didn't exist, so [I added what I could](https://en.wikipedia.org/wiki/OM_System_OM-3):

> The **OM System OM-3** is a retro-styled mirrorless interchangeable-lens camera produced by OM Digital Solutions on the Micro Four Thirds system. It is styled on the original Olympus OM-1 35mm camera. It is the first new product line introduced by OM System after its acquisition of the imaging divisions of the camera manufacturer Olympus in 2021.
> 
> The OM-3 was announced on the 6th of February 2025. It includes the same 20.4MP stacked sensor found in the flagship OM System OM-1 Mark II, though only one SD card slot. It features a Creative Dial as found on the Digital PEN-F cameras for choosing colour modes and filters, and is weather sealed to IP53 standards.

If you have a Creative Commons-licenced photo of the camera itself, or further technical details, feel free to add. Admittedly much of the page came from the Om Systems OM-1 page, given its shared sensor and other features.

**Update, 2025-02-03:** A photo gallery has been added by <span lang="jp">昼落ち</span> on <a href="https://commons.wikimedia.org/wiki/File:OM_System_OM-3_28_feb_2025a.jpg">Wikimedia Commons</a>! <span lang="jp">ありがとうございます</span>.

<figure><p><img src="https://rubenerd.com/files/2025/om3-photo@1x.jpg" alt="Photo of the OM-3 by 昼落ち" srcset="https://rubenerd.com/files/2025/om3-photo@2x.jpg 2x" style="width:420px;" /></p></figure>
