---
title: "Rust Coreutils 0.0.30 shows impressive progress"
date: "2025-03-10T07:14:19+11:00"
abstract: "The permissively-licenced alternative to the GNU coreutils continues to improve."
year: "2025"
category: Software
tag:
- linux
- rust
location: Sydney
---
[Michael Larabel over at Phoronix](https://www.phoronix.com/news/uutils-Coreutils-0.0.30) linked to the [latest point release](https://github.com/uutils/coreutils/releases/tag/0.0.30) of the Rust Coreutils which was pushed yesterday. It's fun reading all the changes, fixes, and improvements to such tools.

I've namedropped the project here a few times here, but never called it out specifically in a post. Like Busybox, uutils coreutils offer another alternative to GNU coreutils. The aim of its developers is to replace many core OS components with safe, performant Rust versions. Crucially, they benchmark themselves against GNU's own tests.

While licencing isn't a core tenant of what they do, it does make it a more attractive option for many applications and use cases. The [ExitGNU](https://exitgnu.github.io/) site lists more options as well, if you're precluded from running GPL-encumbered software, or prefer not to.
