---
title: "M43, and craving the OM System OM-1 Mark II"
date: "2025-01-21T18:02:43+11:00"
abstract: "I love Micro Four Thirds! And this might be the best camera ever in the format. Damn it."
thumb: "https://rubenerd.com/files/2025/om-1-mark-ii@2x.jpg"
year: "2025"
category: Hardware
tag:
- olympus
- om-system
- photography
- wish-list
location: Sydney
---
I fucking adore OM System (ne. Olympus) cameras. My 2023 post about how [camera specs aren’t everything](https://rubenerd.com/camera-specs-dont-tell-the-whole-story/) is basically a love story to the Micro Four Thirds system, and specifically the OM-D E-M1 Mark II that I carried around with me everywhere in the late 2010s. It’s mechanical dials and intuitive design made it the most enjoyable piece of consumer electronics I’ve ever owned, something that has become apparent after “upgrading” to a different camera and immediately missing it.

<figure><p><img src="https://rubenerd.com/files/2025/m43.svg" alt="Micro Four Thirds" style="width:240px;" /></p></figure>

Micro Four Thirds cops a lot of flack for having a smaller sensor than the APS-C found in most mainstream digital cameras, let alone professional “full frame” kit. But the smaller size comes with *massive* benefits when travelling:

* The lenses are significantly smaller and lighter; my pancakes were practically flush with the “pentaprism” hump of that camera, which made it *pocketable* in my massive winter jacket!

* Okay, this is the same point as above, but the smaller size also means I can carry more glass with me. My favourite was a pancake for daily shooting, and a larger f/1.4 prime for specific photography days. I intended to get a telephoto, but I never did? I guess I didn't need one.

* The slightly smaller sensor also makes it easier to implement great image stabilisation, which comes in handy when you’re a tourist taking photos at the spur of the moment, or without a tripod, or because you’re anxious and shake like me!

* It's still *significantly* larger than any camera phone, which is something you don't think you'd notice until you get home from a trip and see all that **stunning** depth of field and detail. There's nothing else like it. Well, maybe film and a scanner.

To each their own, but I loved Olympus and this format. And I want to come back.

Unfortunately, OM Systems heard me, and are trying to convince me to spend our travel savings not on food or airfares, but instead on their highest-end M43 camera: the OM-1. The second revision came out this month, the OM-1 Mark II:

<figure><p><img src="https://rubenerd.com/files/2025/om-1-mark-ii@1x.jpg" alt="Press photo of the OM-1 Mark II" srcset="https://rubenerd.com/files/2025/om-1-mark-ii@2x.jpg 2x" style="width:500px; height:358px;" /></p></figure>

You can read the [OM-1 Mark II product page](https://explore.omsystem.com/au/en/om-1-mark-ii-revealing-wonder) for more details. Which is worth mentioning, because... can we all stop and appreciate it for a moment? It's a regular web page, with stunning photos. There aren't any scrolling tricks like the Apple site, or popups, or other crap. I want to buy this camera anyway, but if it were a tossup I'd get this just to reward them for making a page that *respects us!*

Of course, the price of this beautiful, perfect camera for me is way out of my league right now, especially if I want to keep the mortgage offset topped up, and food in my stomach. But maybe I save for it.
