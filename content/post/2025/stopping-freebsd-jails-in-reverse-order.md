---
title: "Stopping FreeBSD jails in reverse order"
date: "2025-02-18T08:12:23+10:00"
abstract: "sysrc jail_reverse_stop=YES"
thumb: "https://rubenerd.com/files/2020/beastie@2x.png"
year: "2025"
category: Software
tag:
- bsd
- freebsd
- jails
location: Sydney
---
Today I learned that you can stop FreeBSD jails in reverse order from when they were started.

Say you have the follwing configured in your `/etc/rc.conf`:

    jail_enable="YES"
    jail_list="database web varnish"

You can add this:

    sysrc jail_reverse_stop="YES"

And those jails will be stopped in reverse order.

This is perfect example of assumptions. I didn't know you could do this, or assumed I couldn't, so I wrote a script to do it for me years ago (in `tcsh(1)` no less)! The answer was right there the whole time.

Why would you want to do this? Databases are the best example I can think of. I need my source of truth running before I start app servers, and I'd want read replicas of DBs stopped before primary. Or maybe you just like to mix things up. I mean, you must have a bit of a streak of this running a BSD in 2025. Respect :).

