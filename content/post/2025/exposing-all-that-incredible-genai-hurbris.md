---
title: "Exposing all that incredible genAI hubris"
date: "2025-02-07T09:05:11+11:00"
abstract: "One upstart in mainland China did more to expose Valley hubris than anything or anyone else."
year: "2025"
category: Ethics
tag:
- business
- genai
location: Sydney
---
I haven't talked about the whole DeepSeek thing here, save for a [delicious piece of schadenfreude](https://rubenerd.com/genai-also-killed-irony/ "genAI also killed irony") where an American genAI company accused them of plagiarism. You couldn't work that into a script for a movie; you'd be accused of being too on the nose.

More details about DeepSeek are now coming to light, from the hedge fund that funded it, to the developers behind it, and how it trains, censors, and delivers information. I've been the first to admit that this tech doesn't interest me, but its impact on society does. You know, because I live in one.

*(An earlier version of this post had "hedge fund" typo'd as "hedge fun", which sounds like of those childhood mazes, or that massive puzzle Ollie made in the Hololive JP Minecraft server back in the day. This is all a completely pointless observation, but it was fun).*

All we've been reading for the last few years&mdash;with few exceptions&mdash;is breathless reporting about how genAI is going to transform the world. The fact its bullshit (a non-thinking entity can't reason, lie with intent, or hallucinate) isn't dangerous or unavoidable. That its output isn't creepy, milquetoast, or inefficient. And all it takes is smashing climate targets; wasting the equivalent GDP of industrialised nations on silicon, water, and electricity; and harvesting stolen information from billions of people to train models without their victims' knowledge, attribution, permission, or compensation. We're well into Blockchain 2.0 with this hype, it's all the same unscrupulous snakeoil traders peddling it, and all the analysts and reports are buying it. It's equal parts grim and fascinating.

DeepSeek is still unethical in the same ways. Claims it's open source are also, at best, overstated. But the fact it does what it purports with such comparative efficiency compared to American models, it resulted in one of the largest stock value drops in history. Trillions of fake money disappeared when investors asked the same questions some of us have been asking for years at this point. How is any of this stuff sustainable? Have we been sold a lie? *How deep does this go?* One upstart in mainland China did more to expose Valley hubris than anything or anyone else.

I've written before about [being hopeful](https://rubenerd.com/where-does-the-industry-go-after-ai/ "Where does the industry go after AI?") for what our industry might be able to achieve when this next bubble pops. I think it's too soon to write this current one off yet; there are still plenty of marks. But the hope is this is the first crack in the Emperor's clothes. Which is a butt.
