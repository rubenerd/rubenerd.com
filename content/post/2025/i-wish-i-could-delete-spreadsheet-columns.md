---
title: "I wish I could delete spreadsheet columns"
date: "2025-02-21T08:51:54+11:00"
abstract: "Google Sheets let you delete columns. LibreOffice and Excel can only clear them."
year: "2025"
category: Software
tag:
- libreoffice
- spreadsheets
- wishlists
location: Sydney
---
You know the feature I wish LibreOffice Calc and others would implement from Google Sheets? Or from database views, in a matter of speaking? Being able to delete columns and rows.

To be clear, I don't mean:

* Clear columns
* Remove the contents of columns
* Collapse columns
* Hide columns
* Resize columns to nothing
* In Excel parlance: "delete" columns
* Run around with columns
* Desert columns
* Or any combination thereof

I mean, *delete* colums. Remove them, not just from view, but completely. Have them gone!

Maybe it's a mild OCD thing (yes, I'm allowed to use that term!), but for all the things I dislike about Google Sheets, I love that I can have a tab with five columns, and that's it. It looks cleaner, I don't have this endless sea of cells, I don't accidentally teleport into the void by invoking the wrong key combination, it's great! It's as though I've *bound* my sheet to those columns.

In the case of Google Sheets et.al., being able to delete rows and cells is a way to conserve memory. These tools often have limits to how many cells one can have in a workbook, so being able to delete unused ones makes sense. But they're also nicer to use in that one respect as a side effect.

Is it just me? It probably is, if I have to ask the question.
