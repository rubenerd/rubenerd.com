---
title: "GNOME to switch from the Cantarell font"
date: "2025-02-02T08:43:35+11:00"
abstract: "Cantarell was such an awkward choice for a desktop. This is great news!"
year: "2025"
category: Software
tag:
- accessibility
- design
- fonts
location: Sydney
---
Some [great news from Allan Day](https://discourse.gnome.org/t/185-adwaita-sans/26597), via Felix on the GNOME team:

> GNOME changed its UI and monospace fonts this week, in a long anticipated change that is planned for GNOME 48. The new fonts are called Adwaita Sans and Adwaita Mono. Adwaita Sans is a modified version of Inter, and replaces Cantarell as the UI font. 

*Hallelujah!*

I don't use GNOME that often, but I always thought <a href="https://en.wikipedia.org/wiki/Cantarell_(typeface)">Cantarell</a> looked so out of place on the desktop. It was difficult to read in smaller sizes, it had inconsistent vertical spacing and kerning, and the shapes of letters were *deeply* unappealing in a way that grates the more you see it plastered everywhere. For a desktop priding itself on a polished, minimal UI, it was akin to seeing the *Water Lilies* in a museum with plaques underneath replaced with PostIt Notes.

You could always change this using the GNOME Tweak Tool, but *The Tyranny of Default* ensured people had this font on their screens. Now they'll be getting something far nicer and more accessible, which is a net benefit to everyone.
