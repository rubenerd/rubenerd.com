---
title: "The Great Sydney Storm of 2025"
date: "2025-01-16T07:07:01+11:00"
abstract: "Wow."
thumb: "https://rubenerd.com/files/2025/storm-2025@2x.jpg"
year: "2025"
category: Thoughts
tag:
- weather
location: Sydney
---
Clara and I had the brilliant idea of checking out a new Japanese restaurant two suburbs over last night for dinner. It was a bit of a walk, but absolutely worth it. Teriyaki salad with sesame sauce, and a small bowl of miso... wow. The way to my heart is hugs, Arabica, and simple but tasty Asian food.

We're into year two of our 10,000 steps a day challenge (!!!), so naturally we decided to walk home instead of catching the train. There was a light drizzle, but we were in high spirits having recharged our Japanese batteries again. It was cool but not cold, the tree-lined roads were quiet, and we could hear all those wonderful Australian birds. There's nothing else like it in the world.

Then it started. Lightning in the distance so bright it lit up the night. It was continuous and unrelenting, as though it was striking the Earth every other second. The wind hit soon after; a gust so strong it nearly knocked us over, and made mincemeat of our golf umbrella. And then... the rain.

We were near a small office building, so we rushed into its undercover car park for shelter. We looked back and saw the street lights illuminating the near horizontal lashings of water; a continuous steam that was only growing in intensity. Trees and poles were swaying to an extent I didn't think possible. The faint sounds of sirens, the tinkling of glass, and the flickering of lights. Fun.

<figure><p><img src="https://rubenerd.com/files/2025/storm-2025@1x.jpg" alt="Screengrab from Clara’s phone showing the rain smashing down under the light from a street lamp." srcset="https://rubenerd.com/files/2025/storm-2025@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

Singapore had some violent storms, but the strongest I ever experienced was while [travelling with Clara in Hong Kong](https://rubenerd.com/the-best-blog-cafe-in-the-world/) a few years ago. We were in transit to Kansai, but decided to spent a few days there. It was the monsoon season, and I don't think I've ever been that drenched in public outside a pool before. Umbrellas are useless against that violent, swirling wind and rain. I still remember making it back to the hotel absolutely soaked through, and having to go buy waterproof shoes the following day!

Back in the present, we agreed this was almost as violent. We kept peeking back outside the relative safety of the undercover car park, hoping the water and wind would eventually subside, and we could go home. Unfortunately, it was not to be. Half an hour passed, and we agreed we had no choice.

We left the comforting refuge of the empty, quiet car park and sprinted out as carefully as we could to the intersection across from our home. It took less than ten seconds for our clothes to be saturated, which immediately became bone-chillingly cold with the howling wind! The rain drops hitting my face actually *hurt*, as though we were being shot at. I could barely hear what Clara was saying.

It felt like we were standing there, totally exposed to the elements at that `fscking` intersection for an age, but eventually the light gave us permission to cross. We sauntered across in our sopping clothes, and made our way across the pavement and up the stairs to our building. There's certainly a sense of freedom about splashing into puddles and not caring about your shoes, because they're already so waterlogged they're more rain than foot.

We threw all our clothes off into the spare bathroom and cranked up the dehumidifier! To my surprise, I saw small nicks in my face right where the water had hit. Validation! But also, ow? How is that possible?

I unmounted my FreeBSD ZFS pools and NetBSD cgd stores, then turned off and unplugged our computers and Hi-Fi gear. I'd lived through enough electrical storms back home that I didn't want to risk it. Alas, I'd also wired the floor lamps into the same board as the PlayStations which was now disconnected, so we didn't have much light. Oh well, it definitely added to the mood. Alas, our poor little pot plant outside was knocked off his perch, though fortunately he was still in one piece.

Phew. If that Hong Kong monsoon was the most violent storm I'd ever been in, this came in second. Wowza.
