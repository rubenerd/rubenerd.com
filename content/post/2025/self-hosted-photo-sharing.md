---
title: "Thinking aloud about self-hosted photo sharing"
date: "2025-01-27T10:11:13+11:00"
abstract: "Nostalgia about my first Perl powered site, and what I could do now."
thumb: "https://rubenerd.com/files/2025/stanley-irl@1x.jpg"
year: "2025"
category: Media
tag:
- photos
location: Sydney
---
All this talk about Pixelfed on the Fediverse has me thinking about photo sharing again. I'm not a great photographer&mdash;unlike [some talented people](https://rubenerd.com/robin-wong-is-still-writing-about-photography/ "Robin Wong is still talking about photography")\!&mdash;but I'd love to have a place where I can put photos of places and events I care about, and refer back to in years gone by. A bit of the original Apple Aperture, but online. Remember when Apple made software? Good times.

Fun aside, my first ever Perl CGI script wasn't for powering this blog, it was for a photo sharing page that resembled Web 2.0 Flickr. I had an XML file of assets that I hand wrote, and it would slurp it up and generate galleries and tag archives on the fly. It was incredibly slow, but it worked!

<figure><p><img src="https://rubenerd.com/files/2025/stanley-irl@1x.jpg" alt="Level 2 at NEXTDC S2 in Sydney." srcset="https://rubenerd.com/files/2025/stanley-irl@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

Anyway, nostalgia aside, I'm thinking of creating my own again and hosting it locally. This would let me include larger photos than the smaller thumbnail-sized images I've uploaded and included inline here since forever. Ideally it'd also generate the ever-increasingly complex HTML for handling `srcset` and `figures`, which right now I only half-do with some basic shell scripts.

Now that I think about it, is a phrase with six words. This would involve me actually putting my poor-man's CDN into prod, so people in Europe aren't spending five hours downloading every image. ActivityPub integration is less important to me, I just want a gallery that's easy enough to upload to without a million steps, and handles things like thumbnails and galleries.

I'm thinking this might be the first serious project I write to learn Python, maybe in Django? Maybe another little project for my long-service leave.
