---
title: "Earth’s oldest known crater found in rural Australia"
date: "2025-03-09T09:42:28+11:00"
abstract: "I got a kick out of their description of the old crater; these are people who love their jobs."
year: "2025"
category: Thoughts
tag:
- australia
- geology
- science
- western-australia
location: Sydney
---
[ABC News reported](https://www.abc.net.au/news/science/2025-03-06/oldest-known-crater-pilbara-geology-3-47-billion-years-old/105019606) on an article in the [Nature Communications](https://doi.org/10.1038/s41467-025-57558-3) journal about this discovery in the Pilbara region in north-west Western Australia:

> The crater, estimated to be 3.47 billion years old, could have been 100 kilometres wide. Today, we can still see a raised area about 35km in diameter in the middle, called the North Pole Dome.

The enthusiasm and joy from the exploration team really comes across when they describe how they discovered it:

> Seeking on-the-ground evidence of old craters, they headed to North Pole Dome, which sits in the craton. After only an hour on-site the team discovered shatter cones.
> 
> "They're these beautiful, delicate little structures that look a little bit like an inverted badminton shuttle cock with the top knocked off," Professor Johnson said.
>
> "So, upward facing cones with delicate feathery-like features."

Prior to this discovery, the Yarrabubba Crater was considered the Earth's oldest, also located in Western Australia.

Not to always tie such fun stories back to a personal note, but this is just further evidence that I need to explore the inland of Australia more. This is a massive, beautiful, *ancient* continent, and I only ever see the temperate coast and maybe a short work trip "inland" to Canberra. Maybe it's time to save for the [Indian Pacific](https://www.journeybeyondrail.com.au/indian-pacific/) train journey, or the [Ghan](https://www.journeybeyondrail.com.au/the-ghan/)!
