---
title: "The Hyper7-R4 is ridiculous and I want it"
date: "2025-02-08T09:24:19+11:00"
abstract: "A 173% keyboard with a sloped upper panel and beautiful keys!"
thumb: "https://rubenerd.com/files/2025/hyper7-r4@2x.jpg"
year: "2025"
category: Hardware
tag:
- colour
- design
- keyboards
location: Sydney
---
I have a thing for keyboards, as my recent post about the [Kinesis mWave](https://rubenerd.com/the-kinesis-mwave-keyboard/) demonstrated. One day I hope to dust off my old [Unicomp buckling spring board](https://rubenerd.com/my-unicomp-keyboard/) for use in the study, with the door closed.

But this might be the [greatest one I've ever seen](https://mechboards.co.uk/products/hyper-7-v4).

<figure><p><img src="https://rubenerd.com/files/2025/hyper7-r4@1x.jpg" alt="Photo of the Hyper7 R4." srcset="https://rubenerd.com/files/2025/hyper7-r4@2x.jpg 2x" style="width:500px;" /></p></figure>

It's eye-wateringly expensive, comes in several colours including beige, and has more buttons than you could throw a tenkeyless at. The font, colour scheme, the sloping upper panel, the macro potential, I'm here for absolutely all of it. The latent Emacs fan in me especially, though this is being typed in Vim.

It reminds me of those nuclear power station control panels I used to have a book of as a kid (don't ask). One of these paired with a chonky battlestation-style computer case would be incredible. The only question would be what mouse to pair it with, assuming I'd still need one when so many buttons would clearly render such an input device obsolete.
