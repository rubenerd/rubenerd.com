---
title: "Illy Intenso coffee beans"
date: "2025-02-18T08:21:09+11:00"
abstract: "When I made it the right way, it was surprisingly okay."
year: "2025"
category: Thoughts
tag:
- coffee
- reviews
location: Sydney
---
When I first started making coffee for myself and my family when we lived in Singapore in the early 2000s, I reached for a few different types of beans. Illy coffee was one of them. It was consistently decent, widely available, and adverb coffee. When we moved to Malaysia, ditto.

James Hoffman recently did a review of an old Italian espresso machine, and used some Illy coffee to achieve that "classic" flavour profile and roast level for which the machine would have been optimised. This did two things for me: it proved James cares about what he's doing, because you should always test old kit with contemporaneous stuff! And secondly, it made me curious to try Illy coffee again. Would I still remember the taste after all these years? Would I like it?

<figure><p><img src="https://rubenerd.com/files/2025/illy-intenso@1x.jpg" alt="An Ilia coffee can, among other various coffee-based stuff." srcset="https://rubenerd.com/files/2025/illy-intenso@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

Illy coffee is in most Australian supermarkets, though finding the whole bean variety proved more difficult than we expected. I don't buy pre-ground anymore if I can avoid it, owing to its lacklustre taste and proclivity to go stale quicker than I can shout *robusta*.

Precisely *one* supermarket within a few suburb radius of us had wholebean Illy coffee among the tins of pre-ground. It wasn't the Classic red version I was looking for specifically, but I thought it was still worth a try.

Now I'll admit, alarm bells did sound when I read the word *intenso* on the handsome tin. When supermarkets and large coffee roasting companies brand something as being stronger or more intense, they're almost always referring to the roast level. I've been spoiled by light roasts sourced from small farms, so anything more than a medium roast tastes like charcoal water.

The first coffee I made from it was undrinkable. I used the same Clever Coffee Dripper recipe I would for light roasted beans, including the same grind settings and steep time. It yielded a coffee so harsh and bitter, not even a splash of milk could save it. If I hadn't just opened the tin and smelled those wonderful roasted aromas, I'd swear it was stale.

Surprising precisely *nobody* among coffee nerd circles though, all it took was a few adjustments. I set the coffee grinder a few steps coarser, used slightly cooler water, and substituted the AeroPress in lieu of the pourover-style Clever Coffee Dripper.

I'm drinking this now on the balcony as I type this. It's not that bitter at all now, though it's definitely weaker and lacks any interesting or distinct flavours. It tastes like "coffee" like I'd get at a place that sells it without any further description of what it is or where it came from. Shockingly, it's better than some cafe coffee I've had!

James Hoffman calls this a "classic" flavour profile, which is probably right. For me, I'd more specifically refer to it as "neutral". Which maybe if I've had an episode and am seeking a lower-sensory experience, this might fit the bill.

