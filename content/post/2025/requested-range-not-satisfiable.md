---
title: "yt-dlp: “Requested range not satisfiable”"
date: "2025-01-04T08:32:42+11:00"
abstract: "If yt-dlp gives you this error, you probably have to start again."
year: "2025"
category: Internet
tag:
- troubleshooting
- video
location: Sydney
---
I was resuming the downloading of a video from a site using [yt-dlp](https://github.com/yt-dlp/yt-dlp), like a gentleman, when I got this message:

    [download] Resuming download at byte 695556181
    ERROR: unable to download video data: 
        HTTP Error 416: Requested range not satisfiable

I got the same error when looking at ranges, and the guy tried to sell me a gas unit when we already said we wanted induction. Because a freestanding oven/cooktop here is called a range? *Get it!?* You know when you have a joke in your head, and when you say it you realise it wasn't very...

If you get this error, your best bet is to delete the partial download and start again, unfortunately.
