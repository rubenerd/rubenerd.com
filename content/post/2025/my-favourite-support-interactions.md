---
title: "My favourite support interactions"
date: "2024-10-29T07:29:09+11:00"
year: "2024"
category: Thoughts
tag:
- work
location: Sydney
---
1. **Me**: Hi, I need this specific info.

2. **Them**: Here's a wall of unrelated stuff.

3. **Me**: Thanks, but I need this *specific* info.

4. **Them**: A cropped screenshot?

5. **Me**: Thanks, but I need this ***specific*** info.

6. **Them**: I like pie.

7. **Me, after digging for an hour**: I resolved the issue.

8. **Them**: Oh cool, how?

9. **Me**: I manually looked up this ***specific*** info [that you could have provided me in five seconds in point 2].

10. **Them**: Great! You should have just asked.
