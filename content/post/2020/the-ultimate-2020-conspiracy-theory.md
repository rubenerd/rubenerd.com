---
title: "The ultimate 2020 conspiracy theory"
date: "2020-10-13T15:03:09+11:00"
abstract: "Time travel is real, via Alyssa Miller."
year: "2020"
category: Thoughts
tag:
- covid-19
location: Sydney
---
I'd had inkings of this already, but it took someone else writing it so plainly and beautifully:

> My conspiracy theory is that time travel IS real and someone keeps trying to fix 2020 by changing something. But every time they do, they unwittingly make it worse.

From InfoSec's very own [Alyssa Miller](https://twitter.com/AlyssaM_InfoSec/status/1315752032557568000), via my sister. I've taken a [Twitter break](https://twitter.com/Rubenerd/status/1314160427178188802) but this was too good not to share. 

