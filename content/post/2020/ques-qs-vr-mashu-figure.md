---
title: "Ques Q’s VR 1/7 Mashu figure"
date: "2020-12-14T15:49:00+11:00"
abstract: "Still not sure if I like her VR outfit, but this fig is awesome!"
year: "2020"
category: Anime
tag:
- art
- fate
- fate-grand-order
- figs
- mashu
location: Sydney
---
Clara and I were only just in town looking at [this beautiful scale fig](https://www.amiami.com/eng/detail/?gcode=FIGURE-045779) of everyone's favourite eggplant from the *[Fate/Grand Order]()* mobile game and franchise, so it was *monumentally* serendipitous seeing her appear again as a [AmiAmi preorder](https://www.amiami.com/eng/detail/?gcode=FIGURE-121541). I want it known that even though Umu is one of my all-time favourite characters, Mashu was my reintroduction into the *Fate* universe and holds a special place.

We collected enough figs over the years to have informed opinions about the quality, value, and faithfulness of specific manufacturers, but I didn't recognise *Ques Q*. Turns out they've had a *hectic* release schedule, I've just drifted out of touch. They pretty much own the *Touhou* space, and have also done plenty of *Fate* characters I recognise.

<img src="https://rubenerd.com/files/2020/quesq-mashu-1@1x.jpg" alt="" style="width:451px; height:600px;" srcset="https://rubenerd.com/files/2020/quesq-mashu-1@2x.jpg 2x" />

I also didn't recognise the sculptor Aru Momiji; all I could find of hers was a Passionlip fig from the same game. But I think she's done a beautiful job rendering Mash's reserved expression and the motion of her hair. Compare her to the most popular versions of her Stronger-brand figs [with](https://myfigurecollection.net/item/532982 "MyFigureCollection.net: Fate/Grand Order - Mash Kyrielight - 1/7 - Shielder" ) and [without](https://myfigurecollection.net/item/674830 "MyFigureCollection.net: Fate/Grand Order - Mash Kyrielight - 1/7 - Shielder, Limited ver.") her skirt armour; they're just as detailed but I don't see a resemblence in her face at all. This is a bugbear of mine.

But *here's the thing:* I'm still undecided how much of a fan I am of Mashu's Ortinax costume, as much as it pains me to admit. I like the electronic, cyperpunk'esque touches on her new shield, and appreciate her VR goggles are *above* her eyes so we can see actually see them here. But her disproportionately-chunky boots aren't exaggerated; that's how she looks in her next ascension. And those weird, bladder-shaped things around her leotard look even more superfluous and strange in 3D... I half expect her to pull out a straw [like a cyclist](https://duckduckgo.com/?q=cyclist+hydration+bladder).

<img src="https://rubenerd.com/files/2020/quesq-mashu-2@1x.jpg" alt="" style="width:451px; height:451px;" srcset="https://rubenerd.com/files/2020/quesq-mashu-2@2x.jpg 2x" />

That said, is a phrase with two words. The photographer of these press photos did a great job! I need to experiment more with distinct light sources; I always find them more interesting than just a diffuser in a bright room. Maybe I can do it with [the Mashu fig](https://myfigurecollection.net/item/331203 "Fate/Grand Order - Mash Kyrielight - 1/7 (Aniplex)") Clara and I did end up buying second hand in Ōsaka a few years ago. Remember travel?

