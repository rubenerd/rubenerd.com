---
title: "Neptunia fig re-releases, and Yuru Camp"
date: "2020-12-20T23:46:00+11:00"
abstract: "My favourite character Inuyama Aoi has one and it's adorable!"
thumb: "https://rubenerd.com/files/2020/inuyama-aoi-fig@1x.jpg"
year: "2020"
category: Anime
tag:
- figs
- neptunia
- yuru-camp
location: Sydney
---
I sure spend a disproportionately large amount of my time looking at dust collectors for someone claiming to abstain from their procurement in the interests of saving money and shelf space. It's not that our shelves are sagging under the weight of these slabs of gold-pressed latinum cleverly disguised as anime characters, so much as their physical dimensions not being wide or deep enough to contain additional ones without stacking.

The first two off the shelf&mdash;hah!&mdash;are Alter's re-releases of Purple and Black Heart from the inexplicably fabulous *Hyperdimension Neptunia* series. Alter are Clara's and my favourite fig company by far; and Neptunia's premise is so wonderfully absurd. I wonder if the likes of Netgear realise they are anthropomorphic versions of their hardware set in a surreal universe with Random Access Memory and all their friends. Look at me go with all these adverbs we're not supposed to be using anymore!

<img src="https://rubenerd.com/files/2020/nepp@1x.jpg" alt="Purple Heart (left) and Black Heart (right)" style="width:500px; height:425px;" srcset="https://rubenerd.com/files/2020/nepp@2x.jpg 2x" />

Regardless of whether you know or are interested in the series, I can't get over the obsessive detail in figs like this. The level of material science manufactures would have had to go through to produce these, and have them *literally* stand the test of time is something else, especially with all those top-heavy parts. The west doesn't do anything close.

*(Funny story, I was at a SMASH! or similar convention one year&mdash;remember conventions?&mdash;and I was waiting in line for something with someone cosplaying as Purple Heart. I was in one of my Gundam uniforms. I remember looking at each other and laughing; she worked at an auditing firm, and I was in IT. "Is this what grown-ups do on their days off?")*

But the one I'm most excited for is Inuyama Aoi from *Yuru Camp*. That series should have been released *this* year to help us all cope; it was chill, fun, and the characters and sets were all wonderful. It's easily one of our favourite anime series of all time. *And she has Japanese curry and a removable hat!*

<img src="https://rubenerd.com/files/2020/inuyama-aoi-fig@1x.jpg" alt="With and without hat!" Style="width:500px; height:370px;" srcset="https://rubenerd.com/files/2020/inuyama-aoi-fig@2x.jpg 2x" />

Anyone have some spare shelves?
