---
title: "American friends: please vote"
date: "2020-10-29T09:26:41+11:00"
abstract: "All I can say. 🇺🇸"
year: "2020"
category: Thoughts
tag:
- philosophy
- politics
- united-states
location: Sydney
---
While I'm [talking about the US](https://rubenerd.com/om-maliks-gr-iii-san-francisco-photos/) today, please vote if you can. All I can say. 🇺🇸

