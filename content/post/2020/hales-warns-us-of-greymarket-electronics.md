---
title: "Hales warns us of greymarket electronics"
date: "2020-11-03T09:16:54+11:00"
abstract: "Hales on Halestrom quotes his laws of greymarket electronics."
year: "2020"
category: Hardware
tag:
- comments
- hales
- warning
location: Sydney
---
I have two unrelated Hales folks who regularly post comments to me, which I always appreciate! Sorry Rebecca, I'm getting to yours soon too. Hales of [Halestrom.net](http://www.halestrom.net/darksleep/), which you should [subscribe to](http://www.halestrom.net/darksleep/feed.atom), had a comment about my [fake copper heatsync](https://rubenerd.com/wait-that-heatsync-isnt-really-copper/) post. There's enough good stuff that I'll be splitting into a couple of posts. 

*(As an aside, I'm going to be quoting feed URLs directly for independent authors and bloggers going forward. I encourage everyone to do this if you publish online too... let's get a movement going).*

Today's shamelessly-republished portion of his comment concerns greymarket electronics:

> Reading your article: I'm getting some hints that you think the seller saying "Copper Tone Aluminium" will be different to the more expensive options.  Beware the laws of greymarket electronics!
>
> 1. **Law of packaging inertia.** If it looks the same then it is the same, just with a different label attached.
>
> 2. **Law of blind merchants.** Found an item that's the same price, but looks a bit different/nicer than the rest?  There is a 50/50 chance they ran out of that stock and are too lazy to update the photos; so you get sent the same item as what everyone else is selling instead. 
> 
> 3. **Law of dogs howling.** Many apparently independent sellers are not; they're either the same person or they don't hold stock themselves, instead getting their items from the same guy.  
> 
> 4. **Law of colour penetration.** Colours and material descriptions are used interchangeably.  Copper, silver, titanium, gold, crystal, glass, leather, etc.
>
> Sources for these rules: [Equals Zero](http://www.etotheipiplusone.net/) for rule (1) and the rest from my experiences buying & using greymarket electronics. [Fakes are fun](https://halestrom.net/darksleep/blog/038_fakeopamp/) in all forms.

These are brilliant. I replied that I had my first inkling of some of these when buying floppy emultors from eBay for some vintage computer projects. I bought a few, based on different photos and model numbers, only to get the same subpar device three times. It reminds me of Techmoan when he reviews modern cassette players, and they all use variations on the same crappy mechanism.


