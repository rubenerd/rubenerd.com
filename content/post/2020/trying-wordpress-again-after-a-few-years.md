---
title: "Trying WordPress again after a few years"
date: "2020-12-09T09:19:07+11:00"
abstract: "Within WordPress there is a much smaller and cleaner blog platform struggling to get out."
year: "2020"
category: Internet
tag:
- blogging
- reviews
- wordpress
- writing
location: Sydney 
--- 
*This was originally written last weekend but failed to post; ironic given the subject matter!*

I experimented with the latest WordPress again after a few years away from the platform. To borrow Bjarne Stroustrup's words, "within [WordPress], there is a much smaller and cleaner [blog platform] struggling to get out".

This blog was powered by WordPress [from 2005](https://rubenerd.com/wordpress-and-webserver-status/) to 2015. I started blogging right at the tail end of Radio UserLand's existence, and Movable Type had also gone commercial by that stage. I jumped around to a few different local and server-hosted tools before being swept up in the WordPress tide. It offered the killer combination of being:

* affordable, in upfront cost and time to install
* open source, so it was easy to hack on
* widely used, so it was easy to find help
* easy to run on shared web hosting, before I ran VMs
* didn't need anything more special than a LAMP stack in a VM
* simple enough to use with a basic web UI
* and I wasn't tied to posting from just one machine

It worked surprisingly well. I knew this because I spent most of my time in WordPress *writing*, not tinkering. I had just learned of the majesty and fun of working from coffee shops by this stage, so some of the most fun I had in my 20s was drinking coffee, messing with VMs, and writing about my experience. The old WordPress admin interface made jumping in simple, and I loved seeing when they added new features. Remember when they [added tag support](https://rubenerd.com/using-wordpress-categories-as-tags/)? That was massive!

*(Installing and upgrading WordPress is also easier than it's ever been thanks to tools like Ansible and [wp-cli](https://wp-cli.org/). I can stand up a new blog on an OrionVM or Amazon AWS instance in two lines, on FreeBSD or Debian. My concerns in this post are more about the end-user experience).*

But then something started to change. I roughly correlate it with when WordPress moved the admin tabs to a sidebar to accommodate all these new content management features it was starting to adopt. WordPress's team clearly had ambitions for the platform beyond publishing blogs with simple landing pages and uploaded graphics. It always had certain CMS abilities, but time was you'd want to use Drupal or one of its ilk to run anything more complicated.

My early concern was this evolution fundamentally compromised its accessibility as a blogging platform. The installer gradually got larger and larger, the UI more complex, and the new features didn't help with what used to be its core function: writing! Is it an unwritten rule of software that it has to follow this trajectory?

I moved my personal stuff off WordPress to static site generators to simplify my life a bit; a decision I still think was a mixed blessing. The WordPress sites I maintained for other people and non-profits now run the Node-powered Ghost, largely for the same reasons above. I bristle at running JavaScript server-side, but then I was never much of a fan of PHP either. And Ghost offers something that WordPress used to: it's unapologetically a writing platform. Until they follow the above software trajectory and we move elsewhere again.

I bring all this up because I spun up a new WordPress install for the first time in ages to see where it had got up to. There's still a lot to like, and I can see how the blocks feature would make creating flashy new pages&mdash;a deliberate word choice&mdash;easier. But the fact it even needs a [distraction-free writing Mode](https://make.wordpress.org/support/user-manual/content/editors/distraction-free-writing/) now is telling. I wish it were just a lean, kickarse writing platform for *word pressing* again. But then, can you blame them? Most people lock up their writing on social networks, relegating most to being self-promotion tools that [have to be beautiful now](https://rubenerd.com/building-beautiful-websites/) above all else. It's all a tad superficial.

I used to joke that WordPress was simple to use and secure if you disabled half its features, either by overriding PHP, blackholing them with nginx config, forcing file system permissions, or running in a FreeBSD jail or a Linux chroot. There may be a need for me to run WordPress for someone at some point, in which case I might write a patch list for these sorts of things. I could call it *WordPress for Writers!* or something.
