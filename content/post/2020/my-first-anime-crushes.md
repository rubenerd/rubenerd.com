---
title: "My first anime crushes, via @_BADCATBAD"
date: "2020-11-18T23:40:42+11:00"
abstract: "None of these are probably very shocking."
thumb: "https://rubenerd.com/files/2020/first-anime-crushes@1x.jpg"
year: "2020"
category: Anime
tag:
- clannad
- fujibayashi-kyou
- nagato-yuki
- sailor-mercury
- sailor-moon
- tohsaka-rin
location: Sydney
---
Fellow Anime@UTS alumni @_BADCATBAD [tweeted her first anime crushes](https://twitter.com/_BADCATBAD/status/1328883024642011136). It didn't take me long to figure out who mine were, nor should they probably come as any surprise to anyone who's read this blog over the years.

<p><img src="https://rubenerd.com/files/2020/first-anime-crushes@1x.jpg" srcset="https://rubenerd.com/files/2020/first-anime-crushes@1x.jpg 1x, https://rubenerd.com/files/2020/first-anime-crushes@2x.jpg 2x" alt="" style="width:500px" /></p>

From top left to bottom right:

1. **Mizuno Ami**, aka Sailor Mercury from the *Sailor Moon* franchise
2. **Nagato Yuki**, from the *Suzumiya Haruhi* franchie
3. **Tohsaka Rin**, from the *Fate* franchise
4. **Fujibayashi Kyou**, from *Clannad*

Full disclosure, is a phrase with two words. I did unabashedly like **Asahina Mikuru** from *Haruhi* at the time as well, but not to the same extent. **Shigure Asa** was also a fantastic standout character from an otherwise awful harem series, but she missed out being on this list by a matter of weeks. Ditto basically every character from *K-On!*, one of the single greatest shows and four-panel comic series of all time.

I also noticed all but one of the pictures I found within five minutes also have other characters from their respective shows partly in shot. Tohsaka Rin's was deliberately chosen because she was blushing&mdash;cough&mdash;but the others were entirely coincidental. At least I got Kyon's magnificent hair in.