---
title: "Om Malik’s GR III San Francisco photos"
date: "2020-10-29T08:54:36+11:00"
abstract: "I forgot he loved this camera as much as I do."
thumb: "https://rubenerd.com/files/2020/om-sf@1x.jpg"
year: "2020"
category: Media
tag:
- om-malik
- photos
- san-francisco
location: Sydney
---
I respect Om Malik a great deal. His GigaOm site was among the best infocomm news outlets out there, as evidenced by its civil and fun comment sections. I have all the time in the world for his takes on the personal and wider impact of technology and design, especially when the rest of the industry is off chasing the new shiny. [Please read](https://om.co/) and [subscribe](https://om.co/feed/) to his blog where he also podcasts from time to time.

I was pleasantly surprised to see a recent post to see he loves his Ricoh GR III as much as I do! It's such a polarising&mdash;heh&mdash;camera; Ken Rockwell and his followers don't see the point in it, and others say it makes too many compromises. It's perfect for my needs: an SLR-sized sensor wed to the sharpest lens I've ever used, with an intuitive focusing system and beautiful colours. The best camera is the one you have on you, and this tiny one always is. I don't have an SLR or an interchangeable lens camera anymore.

Om took his GR III out around San Francisco one early evening and [took some beautiful shots](https://om.co/2020/10/25/changing-mission/ "Om Malik: Changing Mission"). I especially love the colour gradients in the sky and landscapes. Makes me think I should go out there and share more of my photos too, rather than just talking about it!

<p><img src="https://rubenerd.com/files/2020/om-sf@1x.jpg" srcset="https://rubenerd.com/files/2020/om-sf@1x.jpg 1x, https://rubenerd.com/files/2020/om-sf@2x.jpg 2x" alt="Photo down a long street in the Mission area of San Francisco, by Om Malik." style="width:420px; height:630px;" /></p>

I'd love to go back to San Francisco, New York, and Philadelphia again once the winds over there blow over. Travelling and working from there was still one of the highlights of my life.

