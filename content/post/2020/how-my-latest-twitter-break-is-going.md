---
title: "How my latest Twitter break is going"
date: "2020-10-18T10:10:11+11:00"
abstract: "Pretty well, thanks for asking!"
year: "2020"
category: Internet
tag:
- anxiety
- social-media
- twitter
location: Sydney
---
Pretty well, appreciate you asking! How are you?

I've been off Twitter for about a week now. I've have an account on it for longer than most, and have made long-lasting friendships with people on it. I've even started reconnecting with people from years past which I treasure. I call this Good Twitter, and want to see it preserved somehow.

On the other side, Twitter is wire service. I could tell that seeing the minute-by-minute analysis of every despot and policy left me with a contradictory mix of dread and the urge to consume more, especially first thing in the morning where my emotional state and attitude for the whole day is so often informed. Is this what smoking feels like?

My old man quipped that things aren't any worse now than before, it's just social media percolates on issues and bubbles them to the surface more than curated news media ever did. There's probably some truth to that, though I think it's as ripe for abuse; albeit in a different form.

I'll probably be back for my fix in the next week or so, but I'm already thinking what filters I'll add this time around. I want to be informed, but being anxious and angry also isn't helpful. The fact my first instinct was to say *fix* above suggests this is still an unhealthy relationship. No wonder I retreat to work projects, this blog, and [remote Minecraft islands](https://rubenerd.com/minecraft-and-on-freebsd/).
