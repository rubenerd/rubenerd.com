---
title: "CentOS, as we know it, ends"
date: "2020-12-10T08:53:00+11:00"
abstract: "Can any of us say we're surprised?"
year: "2020"
category: Software
tag:
- centos
- linux
location: Sydney
---
[24th September 2019](https://www.zdnet.com/article/red-hat-introduces-rolling-release-centos-stream/):

> So, if you need a stable RHEL-like operating system, CentOS will still be there for you. But, if you need to keep up with your competitors who are building new cloud and container-based applications, CentOS Stream will work better for you. [..] Old school CentOS isn't going anywhere. Stream is available in parallel with the existing CentOS builds. In other words, "nothing changes for current users of CentOS."

[8th December 2020](https://blog.centos.org/2020/12/future-is-centos-stream/):

> The future of the CentOS Project is CentOS Stream, and over the next year we’ll be shifting focus from CentOS Linux, the rebuild of Red Hat Enterprise Linux (RHEL), to CentOS Stream, which tracks just ahead of a current RHEL release. CentOS Linux 8, as a rebuild of RHEL 8, will end at the end of 2021. CentOS Stream continues after that date, serving as the upstream (development) branch of Red Hat Enterprise Linux.

That didn't take long.
