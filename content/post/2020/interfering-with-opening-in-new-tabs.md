---
title: "Interfering with opening in new tabs"
date: "2020-11-17T13:51:00+11:00"
abstract: "Web devs: don't!"
year: "2020"
category: Internet
tag:
- accessibility
- design
- javascript
location: Sydney
---
I make regular use of command/control while clicking links to open target pages in new browser tabs. You probably do too! It's especially useful when browsing search result pages, so you're not constantly flicking between each result and the original search. This is established, expected behaviour... so naturally, *JavaScript to the rescue!*

There are certain sites that prevent you opening a new tab when clicking a link, either due to a deliberate design decision, or as a side-effect of poorly-written code. Both of these are bad design for accessibility, and for me wanting to shop on your site.

Here's an example where I saw a site using an event handler to redirect people to the correct product page:

    <a href="$PRODUCT" class="js-gtm-push-event-with-callback"
    data-gtm-push-event="{"event":"productClick","ecommerce":{"click":{"actionField":{"list":""}},"products":{"name":"$PRODUCT","id":"$ID"}}}">$PRODUCT</a>

Despite having the target page in the href attribute as a link is supposed to, the JavaScript kicks in upon clicking it. This prevents the new tab request.

I'm leaning towards using NoScript full time again. It breaks so many sites, but counter-intuitively it breaks others into working again. Isn't that the modern web in a nutshell?

