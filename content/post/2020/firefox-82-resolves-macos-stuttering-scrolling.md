---
title: "Firefox 82.0 resolves macOS stuttering scrolling"
date: "2020-10-25T10:40:14+11:00"
abstract: "Firefox 81.x was like running VESA all over again."
thumb: "https://rubenerd.com/files/2020/icon-firefox@2x.png"
year: "2020"
category: Software
tag:
- browsers
- errors
- firefox
location: Sydney
---
My new MacBook Pro coincided with the release of Firefox 81.x, which lead me to think there was something wrong with the discrete GPU on this refurbished machine. Each time I loaded a site and scrolled, regardless of how heavy the page was, it would occasionally stop then lurch in an attempt to catch up. I joked with colleagues that it was a \*nix VESA desktop emulation mode.

Safari and Vivaldi didn't have the same issue, which thankfully ruled out a hardware.

<p><img src="https://rubenerd.com/files/2020/icon-firefox@1x.png" srcset="https://rubenerd.com/files/2020/icon-firefox@1x.png 1x, https://rubenerd.com/files/2020/icon-firefox@2x.png 2x" alt="Firefox icon" style="width:128px; height:128px; float:right; margin:0 0 1em 2em;" /></p>

I'm pleased to report now that the issue is gone as of Firefox 82. Either that, or an [extension I use](https://rubenerd.com/omake/software/#firefox "List of Firefox extensions I use") also updated in the interim. Either way, I'm unreasonably happy.

I used to use Phoenix/Firebird/Firefox back in the day to push against IE. Now the few of us still using it are at it again, only we use it to push against Chrome hegemony. Please use it; it's a great browser and especially quick since the Quantum update. We need its user agent in server logs to show the world there's still value in cross-browser testing and development. We're already starting to see Chrome-only sites again, presumably written by people who either weren't alive or don't remember the lessons of the first browser wars.

Special thanks to [these fine contributors](https://github.com/Homebrew/homebrew-cask/commits/master/Casks/firefox.rb) for maintaining the Homebrew Cask for Firefox, the [FreeBSD Gecko](https://www.freshports.org/www/firefox/) team, and [ryoon for pkgsrc](https://pkgsrc.se/www/firefox). A lot of work goes into people like me being able to install Firefox on our various platforms with a single command.

