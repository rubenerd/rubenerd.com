---
title: "Now Captchas are just messing with me"
date: "2020-10-13T09:17:35+11:00"
abstract: "That’s a long traffic light."
thumb: "https://rubenerd.com/files/2020/captcha-messing-with-us@1x.jpg"
year: "2020"
category: Internet
tag:
- captchas
- usability
location: Sydney
---
That's a *long* traffic light below. And as usual, it overflows slightly into another square. Are we supposed to click that one as well? What about the pedestrian light? Nobody knows.

<p><img src="https://rubenerd.com/files/2020/captcha-messing-with-us@1x.jpg" srcset="https://rubenerd.com/files/2020/captcha-messing-with-us@1x.jpg 1x, https://rubenerd.com/files/2020/captcha-messing-with-us@2x.jpg 2x" alt="" style="width:385px; height:506px;" /></p>

Also, does anyone get the distinct impression we're training algorithms for self-driving cars? Why else would these verification tools *this* obsessed with traffic lights? I expect we'll be asked to check signs next.

One small bit of fun I've been having is also intentionally selecting squares that don't contain the item in question. I've been surprised how often it still lets me through. Take *that* outlier!

