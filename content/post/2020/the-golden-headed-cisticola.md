---
title: "The golden-headed cisticola"
date: "2020-12-09T10:53:00+11:00"
abstract: "We haven't had a Wikipedia picture of the day bird for a while"
year: "2020"
category: Media
tag:
- birds
- photos
- wikipedia
location: Sydney
---
We haven't had a [Wikimedia Commons bird](https://rubenerd.com/tag/birds/) in at least a year. Yesterday Wikipedia featured this handsome speciman by JJ Harrison.

<a title="Link to the Wikimedia Commons page for the photo"  href="https://en.wikipedia.org/wiki/File:Cisticola_exilis_-_Cornwallis_Rd.jpg"><img src="https://rubenerd.com/files/2020/cisticola-exilis@1x.jpg" alt="Photo of a golden-headed cisticola taken by JJ Harrison." srcset="https://rubenerd.com/files/2020/cisticola-exilis@2x.jpg 2x" /></a>

From its [Wikipedia article](https://en.wikipedia.org/wiki/Golden-headed_cisticola):

> The golden-headed cisticola (Cisticola exilis), also known as the bright-capped cisticola, is a species of warbler in the family Cisticolidae, found in Australia and thirteen Asian countries. Growing to 9–11.5 centimetres (3.5–4.5 in) long, it is usually brown and cream in colour, but has a different appearance during the mating season, with a gold-coloured body and a much shorter tail. It is an omnivore and frequently makes a variety of vocalizations. Known as the "finest tailor of all birds", it constructs nests out of plants and spider threads. It mates in the rainy season. It has a very large range and population, which is thought to be increasing. 

The bird is the word.
