---
title: "Another reminder not to use seasons in tech press"
date: "2020-10-29T12:56:05+11:00"
abstract: "Do you mean your spring, or ours?"
year: "2020"
category: Media
tag:
- journalism
- news
- writing
location: Sydney
---
I subscribe to ZDNet Australia's RSS feed which [carried this story](https://www.zdnet.com/article/microsoft-is-planning-a-big-refresh-to-the-windows-10-ui-with-sun-valley-in-21h2-report/#ftag=RSSbaffb68 "Microsoft is planning a big refresh to the Windows 10 UI with 'Sun Valley' in 21H2: Report"), presumably from their American parent company:

> Now that Windows 10 20H2 has started rolling out, what comes next? Windows 10X in the spring and Windows 10 21H2 with some big UI changes in the fall?

When I read "spring", my mind went to that place that everyone from Chile to South Africa has to go to: *do they mean our spring, or theirs?* It was confirmed theirs when they said "the fall"; a phrase we don't use.

In the words of Michael Franks in *[Jardin Botanico](https://www.youtube.com/watch?v=hH7u5CvOIm8)*:

> I abandon the bleak December chill;   
> There's nothing like Christmas in Brazil.   
> The weather's completely upside down;   
> When we touch down.

Technical journalists, podcasters, Wikipedia editors, companies, PR departments, social media posters, all of you: I *implore* you once again to not use seasons to denote time. They're not only vague and ambiguous, they literally ignore half the planet. Possibly slightly more than half, given that seasons also mean absolutely nothing to people on or near the Equator.

I'm okay with the press denoting American currency as simply "the dollar", even when multiple other countries use it. It was the first one, and its overwhelmingly the largest one in circulation and global reach. But nobody has a monopoly on the seasons or time, despite the best efforts of twisted mad scientists hunkering in their basements plotting to take over the world with their chronoparticles.

Not that this frustrates me, or anything. Find out next *hivenglaven* why!

