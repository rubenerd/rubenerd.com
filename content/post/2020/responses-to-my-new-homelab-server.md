---
title: "Responses to my new homelab server"
date: "2020-12-05T22:04:00+11:00"
abstract: "Why not AMD, is ECC worth it, why no dedicated GPU, Noctua gripes, and more."
year: "2020"
thumb: "https://rubenerd.com/files/2020/noctua_nh_u12a_1_2.jpg"
category: Hardware
tag:
- allan-jude
- amd
- bsd
- ecc
- epyc
- freebsd
- homelab
- intel
- memory
- nostalgia
- ryzen
- xeon
location: Sydney
---
People *like* reading about [new server hardware](https://rubenerd.com/building-a-new-homelab-server/ "Building a new homelab server")! I've had more than a dozen emails, some from the usual suspects among you, and a few from people who've never posted. There are common themes in all of them, so I'm aggregating them into the following:

**Why not Ryzen/Epyc?**
: I'm over the moon that AMD are smashing it again; I [used their kit](https://en.wikipedia.org/wiki/Athlon#Athlon_XP/MP "Athlon XP on Wikipedia") almost exclusively in machines I used to build myself. I went with a Xeon because I got an unreasonably good deal on a motherboard, and because they still have a slight&mdash;albeit shrinking&mdash;edge in hypervisor compatibility.<p></p>

**Why not a second-hald Dell rackmount unit, etc?**
: I could have got one, and then have had iDRAC etc. But I live in a studio apartment, and a desktop tower is already pushing space and accoustic limits. I'd have absolutely considered a LACK rack with one or more of these if I had a loungeroom or basement.<p></p>

**Why FreeBSD?**
: New readers may be unfamiliar, but [FreeBSD](https://www.freebsd.org/) has been my preferred server OS for a long time. I detail why on my [software](https://rubenerd.com/omake/software/) page. It's OpenZFS integration is the most pertinent for this build; it's the only file system I trust with my data. I also run [Debian](https://www.debian.org/) VMs to keep a foot in the Linux world, and because we run it at work.<p></p>

**Is ECC memory worth spending that much extra?**
: Yes, especially when refurbished ECC-capable hardware is less than new memory without error correction. Allan Jude has quipped that ZFS without ECC is still more trustworthy than regular file systems *with* ECC. But why not go all out.<p></p>

**Integrated graphics are terrible, use a GPU!**
: It's mostly a moot point for a server that'll only ever been SSD'd, Plex'd, or Minecraft'd into. I have no idea how offloading works, but potentially I'd be up for getting a GPU if I could do transcoding on it, or if its feasible.<p></p>

**Noctua are fine, but they're overpriced. Get XYZ instead**
: First, too late! And second, as I mentioned, this was the one component I splurged on because I love their engineering. Noctua's used to be aspirational devices I never thought I'd justify or own, but with all the money I saved elsewhere, and given how shambolic this year has been, I thought *why not*.<p></p>

**You could get an Adaptec SCSI PCI card for those Iomega drives**
: People cottoned onto why I wanted this specific board :). I'm not sure if that'd be remotely feasible, but it'd be fun. My machines have always a bit of nostalgic pointlessness in them.<p></p>

**Will it run Emacs?**
: Okay Dave, I took the bait! No, Emacs is for local hacking. Remote config files on this machine will be [edited via Ansible](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/lineinfile_module.html "ansible.builtin.lineinfile – Manage lines in text files"), or the built-in [nvi editor](https://www.freebsd.org/cgi/man.cgi?query=vi). I like keeping my servers as sparse as possible, and use tools provided by the system.
