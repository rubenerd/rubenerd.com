---
title: "Microbreaks"
date: "2020-10-22T18:35:41+10:00"
abstract: "Those moments between reading each email, or while on an audio call, or when you've paused writing to think of the right word or phrase."
year: "2020"
category: Thoughts
tag:
- personal
- philosophy
location: Sydney
---
I sit on the balcony most days to do work. It makes the most sense when living in a studio apartment; balancing the need for boundaries, and not wanting to interfere in Clara's own video conferences and meetings. It also gives back a bit of the vitamin D I miss from not commuting, and the fresh air helps.

It's also helped in some unexpected ways. I didn't realise how much I depend on being able to look elsewhere from a computer screen for inspiration and what I call *microbreaks*. They're those moments in time between reading each email, or while on an audio call, or when you've paused writing to think of the right word or phrase. They can last anywhere from a few seconds, to a minute or two. They're also generally unplanned, but are welcome each time.

Having a balcony is great for these microbreaks. Even six floors up, a dad and his kid caught my eye and waved up at me as they walked out of reception. That wouldn't have happened if I were staring blankly at my computer screen.

I've also been told they're useful for staving off myopia by giving your eyes some exercise.

