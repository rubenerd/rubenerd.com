---
title: "Becky Hansmeyer, Dan Benjamin on the iPhone 12"
date: "2020-10-27T16:28:51+11:00"
abstract: "It just seemed... off."
year: "2020"
category: Hardware
tag:
- android
- ios
- iphone
- phones
location: Sydney
---
Apple developer and awesome person [Becky Hansmeyer wrote](https://beckyhansmeyer.com/2020/10/16/off-the-tock/) about Apple's latest telephonic keynote, emphasis added:

> Still, I’m left with a strange feeling after watching Tuesday’s iPhone event. **It just seemed…off**. The 5G marketing bonanza felt forced and nonsensical. Apple knows 5G doesn’t matter to most people, or at least, they should. The whole thing had a vibe of “we didn’t get to make the phone we wanted to make this year due to time, supply chain, and technological constraints but by golly we have to sell phones this year or our shareholders will eat us so HERE’S SOME 5G.”

I'm glad someone else thought this. I haven't been excited, or even that interested, in new phone hardware for a long time. But the highlight clips I watched were especially cringe; and this coming from a company that unironically used *courage* to describe the headphone jack's removal.

*(It also doesn't help that iOS has transitioned from something I prefer because it's generally good, to something I tolerate because it's better than Android. Insert obligatory reference to the old webOS and missing Palm).*

Dan Benjamin of [Hivelogic](http://hivelogic.com/), [5by5](https://5by5.tv/), and [Fireside](https://fireside.fm) fame posited a few years ago (paraphrased) that nobody *wants* to use a phone, we need it for certain features. Specifically, if something else comes along that lets us keep in touch with those around us and organise our lives, we'll ditch these breakable glass slabs. Case in point, who other than nostalgic fools like me still carry PDAs?

Which reminds me, I found a Palm V modem on eBay, but the budget envelope for pointless stuff is empty for October. I hope it doesn't get snatched up in the interim.

