---
title: "Clara’s and my first buried treasure"
date: "2020-11-15T22:55:37+11:00"
abstract: "Saving this Minecraft advancement here for posterity."
thumb: "https://rubenerd.com/files/2020/minecraft-treasure@2x.jpg"
year: "2020"
category: Software
tag:
- games
- minecraft
location: Sydney
---
<p><img src="https://rubenerd.com/files/2020/minecraft-treasure@2x.jpg" alt="Sign reading: Our first BURIED TREASURE with Heart of the Sea" style="width:500px; height:500px;" /></p>

We're saving this Minecraft advancement here for posterity.

