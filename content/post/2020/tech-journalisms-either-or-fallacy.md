---
title: "Tech journalism’s either/or fallacy"
date: "2020-10-07T14:40:41+11:00"
abstract: "You can’t just treat the cause, nor should you only want to."
year: "2020"
category: Media
tag:
- journalism
- social-media
location: Sydney
---
The Stanford Digital Economy Lab [tweeted this](https://twitter.com/DigEconLab/status/1313200689405857792) by Sinan Aral:

> Breaking up Facebook will not solve any of the market failures of big tech. We need social network portability, data portability, and social network portability. We need structural reform. Competition is key.

I have a lot of time Sinan, especially his commentary about social media's impact on our mental health in *The Hype Machine*. He echoed what I've been saying here for years: we need to get back in control of our data, and give us agency over its use.

Here comes the *but*... his Facebook point is a fallacy. Politicians and journalists routinely deflect by saying we should only be attacking causes. It sounds superficially logical, but there's a reason we also treat symptoms.

I don't see why we can't break up abuse companies, *and* reform the system. I agree just doing the former will only result in another hydra taking its place, but that's where the reforms he advocates for come in. Letting the foxes run the hen house has left us with nothing but feathers thus far.

