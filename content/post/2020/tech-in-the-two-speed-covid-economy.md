---
title: "Tech in the two-speed Covid economy"
date: "2020-10-30T10:23:51+11:00"
abstract: "Breathless discussion about record profits as millions lose their jobs... seems a bit distasteful, maybe?"
year: "2020"
category: Thoughts
tag:
- covid-19
- economics
location: Sydney
---
I'm reading a *lot* of corporate estimates and quarterly revenue updates in the tech industry, all of it rosy and bullish about the future. Expectations were either met or exceeded, and profits are all up. I walked past a corporate office in Sydney this morning, and the breakfast business show projected on the TVs showed all smiles.

In the tech front, Apple announced their new expensive phones, and Samsung now have ones that are upwards of $3,000. The tech press have been fawning over them; either obliviously or as a form of escape.

Meanwhile, Covid cases around the world continue to soar, and people are still losing their jobs and livelihoods by the thousands. Places like Taiwan, Australia, and New Zealand that have contained the spread watch with clenched teeth as people elsewhere suffer, knowing full well that our economies can't escape the international fallout indefinitely.

Never before have I been more acutely aware that I'm engaged and benefiting from what my economics professor called the *two-speed economy*. The tech sector hasn't come out unscathed, but those among us lucky to have a job before this started, and to be retaining it now in one of the few sectors doing well in this crisis... it feels in equal parts unreal and undeserved.

I don't work any harder than someone in the travel or hospitality industries who've seen their jobs evaporate. And I damn well don't work harder than nurses or cleaners that are keeping us safe on the front lines, from hospitals to trains. So why should my industry disproportionally reap the benefits without any afterthought or introspection? It grates for me, and I benefit from it! Imagine being someone unemployed with little savings seeing these reports.

I wish I saw more of a reflection on this in the press when Tech Company X breathlessly announces a record profit. Companies in the world wars were expected or compelled, as part of patriotic duty, to contribute to the war effort. Why is it not being demanded now? Surely I can't be the only one noticing the silence on any of this.
