---
title: "Where to find the BBC’s RSS feeds"
date: "2020-11-01T11:11:01+11:00"
abstract: "They still work, but they’re not publicised anywhere."
year: "2020"
category: Internet
tag:
- rss
- united-kingdom
location: Sydney
---
I've been subscribed to some of the BBC's RSS feeds for years, but I only just noticed their website doesn't publish them anywhere obvious anymore. The RSS icon and site headers no longer exist, so people coming to the site wouldn't realise they publish them. This is sad.

The XSL for the feeds reference [this help page](https://www.bbc.com/news/10628494#userss "News Feeds from the BBC") which hasn't been updated for a decade, and the copyright notice is for 2008! I'm putting the links below in case this page disappears:

* [Top Stories](http://feeds.bbci.co.uk/news/rss.xml)
* [World](http://feeds.bbci.co.uk/news/world/rss.xml)
* [United Kingdom](http://feeds.bbci.co.uk/news/uk/rss.xml)
* [Business](http://feeds.bbci.co.uk/news/business/rss.xml)
* [Politics](http://feeds.bbci.co.uk/news/politics/rss.xml)
* [Health](http://feeds.bbci.co.uk/news/health/rss.xml)
* [Education](http://feeds.bbci.co.uk/news/education/rss.xml)
* [Science and Environment](http://feeds.bbci.co.uk/news/science_and_environment/rss.xml)
* [Technology](http://feeds.bbci.co.uk/news/technology/rss.xml)
* [Entertainment](http://feeds.bbci.co.uk/news/entertainment_and_arts/rss.xml)

I almost don't want to ask the BBC to link to these from somewhere again, in case they forgot they published them and turn them off.

