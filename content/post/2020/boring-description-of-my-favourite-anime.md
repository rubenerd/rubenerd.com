---
title: "Boring description of my favourite anime"
date: "2020-11-25T09:50:00+11:00"
abstract: "Of course it's K-On!, what else would it be?"
year: "2020"
category: Anime
tag:
- k-on
- keion
- akiyama-mio
location: Sydney
---
This Twitter meme went by a few days ago. [I wrote](https://twitter.com/Rubenerd/status/1331115407801942016)\:

> Four (then five) girls have tea and cake, and sometimes play music. Wait, that still sounds aspirational and wonderful. Time for a rewatch, me thinks.

We need them again, now more than ever.

<img src="https://rubenerd.com/files/2020/kon-tea-time@1x.jpg" alt="Image of the K-On! girls, from the Kyoto Animation adaptation." style="width:500px; height:343px;" srcset="https://rubenerd.com/files/2020/kon-tea-time@2x.jpg 2x" />
