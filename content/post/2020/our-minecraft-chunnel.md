---
title: "Our Minecraft Chunnel"
date: "2020-10-25T10:21:16+11:00"
abstract: "Connecting our homes thousands of blocks apart with a rail tunnel under the sea."
thumb: "https://rubenerd.com/files/2020/minecraft-breakthrough@1x.jpg"
year: "2020"
category: Software
tag:
- games
- minecraft
location: Sydney
---
One of my favourite photos of all time is the view of the French and British engineers grinning at each other and swapping flags after they connected their sides of the Channel Tunnel. It's such a powerful statement of unity, along with being a damn impressive feat of engineering.

Clara and I had been separated on our Minecraft map with homes thousands of blocks apart, so we decided to try the same thing. We tunneled down below the sea floor, then across in a L shape. We hit a couple of deep sea trenches which had to have underwater tunnels built, but it was an opportunity to line the walls with glass for a view.

After a solid three evenings of tunneling and dozens of used pickaxes, we broke through! And I kid you not, the meeting point was lined with gold on the top. We decided to keep it there, along with a silly sign.

<p><img src="https://rubenerd.com/files/2020/minecraft-breakthrough@1x.jpg" srcset="https://rubenerd.com/files/2020/minecraft-breakthrough@1x.jpg 1x, https://rubenerd.com/files/2020/minecraft-breakthrough@2x.jpg 2x" alt="View of Clara's avatar after we broke through in our Chunnel" style="width:500px" /></p>

Now what used to take at least 6 minutes of boating around a couple of massive peninsulas and labourious use of the W key is now a redstone-powered automatic rail cart ride. Of course now that we've been bitten by this bug, we're thinking where we can extend the line to.

It's funny that I play more games in my early 30s now than I ever did before. Maybe it's a release?

