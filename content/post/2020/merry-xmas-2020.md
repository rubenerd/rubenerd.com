---
title: "Merry Xmas, 2020"
date: "2020-12-25T08:55:00+11:00"
abstract: "To you, not the year!"
thumb: "https://rubenerd.com/files/2020/xmas-2020@1x.jpg"
year: "2020"
category: Thoughts
tag:
- christmas
- family
- personal
location: Sydney
---
Not Merry Xmas to the *year*, it can hurry up and get in `/var/tmp` where our life cronjob will promptly clear it away. But Merry Xmas and Happy Holidays to you, your family, friends, and everyone else.

I can say that your emails and tweets have been among the highlights of this year, and relegated that 2020 ennui to the aforementioned temporary folder. For those of you also isolated from family, or doing it tough this year, I hope that my silly, long-winded ramblings here were helpful in some small way too.

<img src="https://rubenerd.com/files/2020/xmas-2020@1x.jpg" alt="Our little tree this year." style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2020/xmas-2020@2x.jpg 2x" />

It's hard to see in this shaky, high ISO camera-phone image I took last night on a while, but this is Clara's and my little tree. A studio apartment doesn't give one much space for sprawling decorations, but we've been carrying this one since we moved in together. The heart in a circle and the blue bauble with the star and moon came from my late mum's collection; the white snowball I bought with pocket money when I was 8; and that green bird came with my dad when he emigrated from post-war Germany with his family to Australia in the 1950s. The summertime Xmas koala was a recent Clara addition!
