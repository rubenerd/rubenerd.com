---
title: "Pet Shop Boys, DJ Culture"
date: "2020-11-09T09:50:17+11:00"
abstract: "...may I say nothing?"
year: "2020"
category: Media
tag:
- lyrics
- music
- music-monday
- pet-shop-boys
location: Sydney
---
It's a shorter *[Music Monday](https://rubenerd.com/tag/music-monday/)* today, from [everyone's favourite](https://www.youtube.com/watch?v=eXuD2MCNd1k "Pet Shop Boys: DJ Culture") electronic duo:

> I've been around the world;   
> for a number of reasons.   
> I've seen it all;   
> the change of seasons.   
> And I, my lord... may I say nothing?

