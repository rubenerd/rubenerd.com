---
title: "Abandoned virtual worlds"
date: "2020-10-27T08:51:47+11:00"
abstract: "The only difference is there is no decay, leading to a strange transcendent dream-like appearance."
year: "2020"
category: Software
tag:
- minecraft
- open-worlds
location: Sydney
---
I used to preface Reddit posts by warning people of its dumpster fire nature, but I'm a long-term Twitter user. People who live in glass houses shouldn't something something.

[/u/Zamaros has a couple of angles](https://www.reddit.com/r/LiminalSpace/comments/jihtp1/worldsnet_is_just_the_backrooms_of_the_internet/ga6x8m3/?utm_source=UTM-IS-SPAM&utm_medium=UTM-IS-SPAM) I hadn't thought about for a long time:

> What really gets me is the idea that if reality truly is just the culmination of outside stimuli, then the virtual world is every bit as meaningful as the real one. For every minecraft world, for every empty worlds.com world, for every abandoned sims game, for every bit of lost data, there is a deficit in the prime reality. These images are no different than those of Stonehenge, Machu picchu, the coliseum, the ruins of Crete, or the ancient city of Babylon. The only difference is there is no decay, leading to a strange transcendent dream-like appearance.

I can't count how many times I've logged into an old VM with a copy of the Sims I abandoned years ago, or SimCity cities I made when I was a teenager. Even firing up an exploring an old version of Need For Speed, or Midtown Madness. All those worlds are still there, and they've aged and not aged at all.

I remember writing in an old school assignment that it's hard to keep things clean in the real world, and harder to make things look dirty in virtual ones. I didn't realise at the time I was describing entropy.

