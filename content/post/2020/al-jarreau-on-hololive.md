---
title: "Al Jarreau on Hololive"
date: "2020-10-26T15:18:04+11:00"
abstract: "Okay not even close, but still fun. Music Monday time."
thumb: "https://rubenerd.com/files/2020/al-jarreau-hololive@1x.jpg"
year: "2020"
category: Thoughts
tag:
- al-jarreau
- hololive
- music
- music-monday
location: Sydney
---
It's [Music Monday](https://rubenerd.com/tag/music-monday/) time! Each and every Monday, except when I don't, I write about some music without fail. The consistency of the writing is without fail, not the music itself. It's as though I named this series specifically for this audible content, and to coincide with a specific day of the week. Though I would take great pains to point out that the alliteratic spelling is mere coincidence. That statement is as true as *alliteratic* being a word. It's perfectly *cromulent* and *gurt by sea*.

Today's entry is, as you've no doubt surmised by now, entirely pointless. It's the inevitable outcome of watching a Hololive EN stream while rearranging your turntable preamp cables, and inadvertently putting one of your favourite album covers against the TV, such that the gentleman's face looks as though he has an avatar who's joined in playing Minecraft.

<p><img src="https://rubenerd.com/files/2020/al-jarreau-hololive@1x.jpg" srcset="https://rubenerd.com/files/2020/al-jarreau-hololive@1x.jpg 1x, https://rubenerd.com/files/2020/al-jarreau-hololive@2x.jpg 2x" alt="" style="width:500px" /></p>

I wonder what the Venn Diagram looks like of people who love Al Jarreau, and who watch Hololive EN Minecraft streams? Is it just Clara and I? I'd commission a study, but I spent all my mental CPU cycles today writing a two-hundred word blog post about nothing.

You can [buy the album on iTunes](https://music.apple.com/au/album/high-crime/276974433), or preview on [The YouTubes](https://www.youtube.com/watch?v=uLEWBWOSObc&list=OLAK5uy_kZoFYUCWbdQfWhlnSvPXSRdSdInNCmieI). 7Digital, please get more of his back catalogue! And don't watch Hololive EN; especially in the evening in the background to relax while doing other things.

