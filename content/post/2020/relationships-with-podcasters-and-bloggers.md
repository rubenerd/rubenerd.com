---
title: "Relationships with podcasters and bloggers"
date: "2020-11-11T18:41:48+11:00"
abstract: "We welcome these hosts into our lives every week to be entertained and inspired."
year: "2020"
category: Media
tag:
- john-roderick
- podcasters
location: Sydney
---
My follow-up post [about a podcast host](https://rubenerd.com/john-roderick-did-have-some-points-about-millennials/) still gets responses, right up there in count with Hacker News links. I'd wondered at the time why a flippant message from someone I listen to hit me harder than I would have expected, and how it generated so much interest.

Jonathan Poritsky had some [interesting insight](http://www.candlerblog.com/2012/05/20/they-write-about-podcasts/ "They write about podcasts") a decade ago when John Gruber's Talk Show split without explanation from 5by5:

> People care about these shows. They listen to them regularly and discuss them among friends the way a bygone generation would talk about Uncle Miltie. Listeners are personally connected to these shows. We welcome these hosts into our lives every week to be entertained and inspired. There is absolutely a relationship between host and listener and it is a very personal, intimate one. Otherwise what’s the point?

