---
title: "Context switching"
date: "2020-11-15T09:17:14+11:00"
abstract: "Only way I’ve found to help stave it off is blocking out calendar time."
year: "2020"
category: Thoughts
tag:
- microbreaks
- productivity
- work
location: Sydney
---
It wasn't until I was promoted to my current position/role/etc that I learned about the phrase *context switching*, though I realise I've been affected by it for a long time.

Context switching is variously defined as interruptions, distractions, or attempting to multitask. It can be self-inflicted, but I think the implication is it's unwanted and imposed. Who among us hasn't felt frustrated thinking we could be productive working on what we need, paid, or want to do, if *things* didn't keep coming up.

Unlike regular distractions, context switches necessarily require your full attention, and the marshalling of resources and mental capacity to complete or move the needle on disparate, unrelated tasks while you were preciously engaged elsewhere. That was supposed to be *previously* engaged, but I'm taking the typo as a Freudian slip!

It's destructive not merely for pushing out deadlines on things we're supposed to do, but perversely results in a classic 1+1<2. We'd have finished two tasks had we been given sufficient space to complete the first before being poked about the second.

I like some distractions during the day. I've talked about [microbreaks](https://rubenerd.com/microbreaks/) before, like looking out the window. Longer breaks reading RSS feeds, walking to the Aeropress for a coffee, or taking a stroll around the block, are great because I take them on my terms. A context switch is a phone call to attend to something, or an email from a client, or a coworker asking a question.

*(Emails and phone calls are part of my job, and as an introvert I'm surprised at how much I actually like talking with people about their system requirements. Maybe it's because meetings have a set agenda, a firm exit time, and for the most part finish after a certain hour).*

Seminars, blogs, books, and podcasts routinely talk about context switching as exclusively affecting people's `#productivity`, as though we exist only for our economic output. But it also affects qualitative metrics like job satisfaction, even optimism. Someone who's constantly expected to put a task on hold to start another is likely not going to be jazzed about doing it; or worse, lash out as a result.

I think it's why I get some of my best work done before work, and late in the evening. Without the expectation that I'm always available to be poked, I can get extended time periods to do things. It goes as much for personal projects as well; I may have been guilty in the past of telling people I sleep in so I could get in a couple of hours of introvert time at a coffee shop.

For work time, the best I've found is to block out a couple of hours in your company calendar for focus. This doesn't help external parties, but it stops you being roped into internal meetings, discussions, or chat notifications. Again, I didn't understand why my bosses used to do this in previous roles and companies, but I sure get it now!

Software and web services like time and task managers are so often pitched as either a panacea or a silver bullet, but they'll never be a substitute for setting expectations.

