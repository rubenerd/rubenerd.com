---
title: "Spelling mistakes for week 48, 2020"
date: "2020-11-27T09:42:00+11:00"
abstract: "Is this what people tune in for?"
year: "2020"
category: Internet
tag:
- pointless
- spelling
location: Sydney
---
Posts on this blog wouldn't be complete without a litany of spelling mistakes. Here was my post on [video conference fatigue](https://rubenerd.com/video-conference-call-fatigue/):

> I get that people think video calls make them more personal and engaging, and I’ll bet certain bosses think they’re a great way to micromanage and make sure they’re employees are paying attention. But it’s counterproductive, at least most of the time.

*They are employees are paying attention?* Isn't English great?
