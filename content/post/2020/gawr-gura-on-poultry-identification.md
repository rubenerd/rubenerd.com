---
title: "Gawr Gura on poultry identification"
date: "2020-11-22T22:01:52+11:00"
abstract: "Can I dig here?"
thumb: "https://rubenerd.com/files/2020/gura-chickens-1@1x.jpg"
year: "2020"
category: Media
tag:
- hololive
- video
- youtube
location: Sydney
---
<p><a title="Play video in new window" target="_blank" href="https://www.youtube.com/watch?v=ZmTcW28YEgo"><img src="https://rubenerd.com/files/2020/gura-chickens-1@1x.jpg" srcset="https://rubenerd.com/files/2020/gura-chickens-1@1x.jpg 1x, https://rubenerd.com/files/2020/gura-chickens-1@2x.jpg 2x" alt="Can I dig here? Will it mess up my..." style="width:500px" /><br /><img src="https://rubenerd.com/files/2020/gura-chickens-2@1x.jpg" srcset="https://rubenerd.com/files/2020/gura-chickens-2@1x.jpg 1x, https://rubenerd.com/files/2020/gura-chickens-2@2x.jpg 2x" alt="... why is there a chicken here?" style="width:500px; height:281px;" /></a></p>

