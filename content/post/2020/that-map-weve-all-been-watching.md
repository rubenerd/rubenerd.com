---
title: "That map we’ve all been watching"
date: "2020-11-06T09:57:47+11:00"
abstract: "The Australian ABC has one of the better graphics I’ve seen, even if their other reporting is medicore."
thumb: "https://rubenerd.com/files/2020/fuck-off-trump@1x.png"
year: "2020"
category: Thoughts
tag:
- politics
- united-states
location: Sydney
---
I thought back this morning to the first American election map I ever watched. My high school friend Felix and I had my iBook G3 open at a Coffee Bean while we lived in Singapore, fingers crossed for John Kerry. It seemed all but assured given Bush's international respect was non-existant, and his bumbling had become such a source of ridicule.

That's when I learned that domestic popularity was *entirely* different. Ah, to be that young and innocent.

<p><img src="https://rubenerd.com/files/2020/fuck-off-trump@1x.png" srcset="https://rubenerd.com/files/2020/fuck-off-trump@1x.png 1x, https://rubenerd.com/files/2020/fuck-off-trump@2x.png 2x" alt="" style="width:500px" /></p>

But I digress. I've tried *hard* to avoid news about the current election this time around. I already feel too emotionally invested in the outcome, especially in this Covid climate. But every now and then I refresh the [Australian ABC's graphic above](https://www.abc.net.au/news/2020-11-04/2020-us-election-live-results-who-is-winning-who-is-president/12762210), which I think does the best job of any I've seen putting all the electoral college votes into perspective.

