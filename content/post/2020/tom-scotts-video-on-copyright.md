---
title: "Tom Scott’s video on copyright"
date: "2020-11-21T11:10:32+11:00"
abstract: "It was well considered and researched, but I think it missed a couple of things."
thumb: "https://rubenerd.com/files/2020/yt-1Jwo5qc78QU@1x.jpg"
year: "2020"
category: Media
tag:
- copyright
- ethics
- media
location: Sydney
---
Tom Scott's video about YouTube copyright was detailed, well-researched, considered, and to the best of my knowledge, factually correct. He's deliberate and careful to make the distinction between legality and ethics, and makes a powerful case for YouTube's content matching as it currently exists. In the current system, I'm convinced. 

<p><a target="_BLANK" href="https://www.youtube.com/watch?v=1Jwo5qc78QU" title="Play YouTube's Copyright System Isn't Broken. The World's Is."><img src="https://rubenerd.com/files/2020/yt-1Jwo5qc78QU@1x.jpg" srcset="https://rubenerd.com/files/2020/yt-1Jwo5qc78QU@1x.jpg 1x, https://rubenerd.com/files/2020/yt-1Jwo5qc78QU@2x.jpg 2x" alt="Play YouTube's Copyright System Isn't Broken. The World's Is." style="width:500px;height:281px;" /></a></p>

I only had two niggling concerns. The premise of copyright itself wasn't questioned. That was likely intentional; he wanted to stick to the facts. But I want to know, to what extent should we be allowed to profit from our creative works? And if we know what we should have, does copyright allow us to effectively do so in the current century? The evidence presented thus far isn't compelling and, as Tom mentions, skews towards corporations with legal teams.

The other major point he didn't directly discuss is the role of civil disobedience as a necessary factor for change, or if he thinks it's necessary. I'd say history is littered with laws people thought were unjust and rebelled against, and copyright law as it exists today with its arguably unreasonable extensions are no exception. Part of me thinks the only way we'll affect change is to constantly demonstrate the absurdity of the system. I suppose sites like Giffy that Tom raised are already doing this.

I still come back to JD Lasica's *[Darknet: Hollywood's War against the Digital Generation](https://rubenerd.com/j-d-lasica/)* as my canonical source here. What is the legal purpose of suing people for the use of copyrighted work where clear financial losses can't be demonstrated, if not to just extract money in an unethical way? What ends, and who, does this legislation serve? Should financial compensation even be the yard stick we use to measure this?

These are the conversations we *need* to have, though pragmatically I suppose I'll take reforms in the meantime.

