---
title: "Word of the day: Diffident"
date: "2020-11-05T11:11:09+11:00"
abstract: "Lacking self-confidence; timid; modest"
year: "2020"
category: Media
tag:
- books
- john-j-nance
- language
location: Sydney
---
Today I leaned a new word while reading John J. Nance's *Lockout*. [Wiktionary defines it as](https://en.wiktionary.org/wiki/diffident)\:

> (archaic) Lacking confidence in others; distrustful.   
> Lacking self-confidence; timid; modest

And Microsoft Bookshelf 1991 for DOS on CD-ROM defined it on my retro computer tower next to me as:

> diffident **adj.** Hesitant to assert oneself from a lack of self-confidence; timid. *[ME < Lat. diffidens, pr.part. of diffidere, to mistrust : dis-, not + fidere, to trust.]*

The pilot in the book was using the archiaic, mistrusting definition. It fits, given the archaic hardware I used to access the latter definition for pointless fun.

I don't think I've ever heard the word before; it sounds more like a brand of toothpaste. *Diffident: For when you don't trust other brands.*

