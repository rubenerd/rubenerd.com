---
title: "Do good"
date: "2020-10-24T21:32:54+11:00"
abstract: "The good you do today will be forgotten tomorrow; do good anyways"
year: "2020"
category: Thoughts
tag:
- good
- philosophy
location: Sydney
---
Via an [old Minecraft forum thread](https://www.minecraftforum.net/forums/minecraft-java-edition/suggestions/80257-craft-slabs-and-stairs-back-into-blocks) that I liked:

> "The good you do today will be forgotten tomorrow; do good anyways" ~ Abel Muzorewa

