---
title: "Talking about chips"
date: "2020-12-15T21:58:00+11:00"
abstract: "I understand that Samboy Chips are back. Is that good news for Australians?"
year: "2020"
thumb: "https://rubenerd.com/files/2020/talking-about-samboy-chips-2@1x.jpg"
category: Media
tag:
- pointless
- video
- youtube
location: Sydney
---
<p><img src="https://rubenerd.com/files/2020/talking-about-samboy-chips-1@1x.jpg" srcset="https://rubenerd.com/files/2020/talking-about-samboy-chips-1@2x.jpg 2x" alt="Hughsey, I understand that Samboy Chips are back. Is that good news for Australians?" style="width:500px; height:281px;" /><br /><img src="https://rubenerd.com/files/2020/talking-about-samboy-chips-2@1x.jpg" srcset="https://rubenerd.com/files/2020/talking-about-samboy-chips-2@2x.jpg 2x" alt="It is for Australia" style="width:500px; height:281px;" /></p>

Ah Australia, even your [infomercials](https://www.youtube.com/watch?v=Ret1wyCNPOk) are amazing.
