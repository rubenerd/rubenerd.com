---
title: "Cleaning another Mac Bitcoin miner"
date: "2020-10-05T09:55:57+11:00"
abstract: "Could be being spread by fake software."
year: "2020"
category: Software
tag:
- macos
- security
- viruses
location: Sydney
---
I was remote-troubleshooting another person's Mac recently, and discovered why it had been running slower of late. Buried in their `tmp` folder they had some suspicious binaries:

    $ ls -1F /private/tmp
    ==> i2pd/
    ==> i2pd2/
    ==> jFCQhxfZDM*
    ==> mtfsftpnhl*
    ==> vgcqphbkrm
    ==> vgcqphbkrm_md5

Ten of the scanners on VirusTotal reported a trojan in the two executables, and none of these were the names everyone knows, which is a tad worrying. GData and SentinelOne called it out specifically as a coin miner. The obfuscated data file and md5 hash didn't trigger alarms.

I immediately killed the processes and deleted them without grabbing any usage data to quote below, but at a glance `mtfsftpnhl` was using around 20% of a core. The designers of this were smart enough not to peg a whole CPU and make it obvious.

What I found interesting this time were the two folders at the top of the listing. Searching returned [this project](https://i2pd.readthedocs.io/en/latest/):

> i2pd (I2P Daemon) is a full-featured C++ implementation of I2P client.
> 
> I2P (Invisible Internet Protocol) is a universal anonymous network layer. All communications over I2P are anonymous and end-to-end encrypted, participants don't reveal their real IP addresses.
> 
> I2P client is a software used for building and using anonymous I2P networks. Such networks are commonly used for anonymous peer-to-peer applications (filesharing, cryptocurrencies) and anonymous client-server applications (websites, instant messengers, chat-servers).

So presumably this is how the payload and c&c operations were delivered. I liked that they specifically called out cryptocurrencies.

The person in question may or may not have freely admitted to trialling pirated versions of major Apple applications like Logic and Final Cut. I couldn't correlate them specifically, but this hints at the possibility that illegitimate software is now being distributed with coin miners.

At that stage, it was time for a wipe and reinstall.

