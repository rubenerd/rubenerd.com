---
title: "Some mid-spring Sydney fog"
date: "2020-10-08T21:36:50+11:00"
abstract: "Nothing like Karl from San Francisco, but still adds an air of mystery... and water vapour."
thumb: "https://rubenerd.com/files/2020/sydney-oct2020-fog-1@1x.jpg"
year: "2020"
category: Travel
tag:
- karl-the-fog
- san-francisco
location: Sydney
---
Last weekend felt like summer, but it got chilly and foggy again this week. Nothing like [Karl](https://www.kqed.org/news/11682057/how-the-bay-areas-fog-came-to-be-named-karl "KQED: How the Bay Area's Fog Came to Be Named Karl"), but still adds an air of mystery. By which I mean water vapour.

<p><img src="https://rubenerd.com/files/2020/sydney-oct2020-fog-1@1x.jpg" srcset="https://rubenerd.com/files/2020/sydney-oct2020-fog-1@1x.jpg 1x, https://rubenerd.com/files/2020/sydney-oct2020-fog-1@2x.jpg 2x" alt="" style="width:500px" /><br /><img src="https://rubenerd.com/files/2020/sydney-oct2020-fog-2@1x.jpg" srcset="https://rubenerd.com/files/2020/sydney-oct2020-fog-2@1x.jpg 1x, https://rubenerd.com/files/2020/sydney-oct2020-fog-2@2x.jpg 2x" alt="" style="width:500px" /></p>


