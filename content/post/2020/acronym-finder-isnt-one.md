---
title: "Acronym Finder isn’t one"
date: "2020-11-28T21:32:00+11:00"
abstract: "Unless we pronounce it arf, which would be base AF."
year: "2020"
category: Internet
tag:
- abbreviations
- acronyms
- language
- pointless
location: Sydney
---
<img src="https://rubenerd.com/files/2020/Acronym-Finder-192.png" alt="AF: Acronym Finder logo." style="width:96px; height:96px; float:right; margin:0 0 1em 2em;" />

It just occurred to me that the Acronym Finder's logo is an initialism, not an acronym. Unless we say it as *Ahf*, or *Arf*. Wait, those are pronounced the same way. This post is *base AF.*

You know what AF **couldn't** stand for? *Bagel crisp*. You know what it **could** stand for? *Bagel crisp*, if you misspelled it. [Wikipedia](https://en.wikipedia.org/wiki/Bagel_crisp) describes these culinary masterpieces as:

> Wikipedia does not have an article with this exact name. Please search for Bagel crisp in Wikipedia to check for alternative titles or spellings. 

It's this kind of insightful commentary I'm sure everyone comes here to read. Comments in response to this post will be ignored.
