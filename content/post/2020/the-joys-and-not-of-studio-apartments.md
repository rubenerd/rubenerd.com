---
title: "The joys, and not, of studio apartments"
date: "2020-12-27T09:29:00+11:00"
abstract: "We still think it's worth it, but your lifestyle has to *really* be compatible for them to work."
year: "2020"
category: Thoughts
tag:
- lifestyle
- personal
location: Sydney
---
Clara and I have been living in studio apartments since we moved in together in the late 2010s. They offer several distinct advantages from regular apartments or houses if you can make your life work around them, though we've discovered a few issues that are now making us reevaluate too. Buckle in for what I'm sure aren't all that many surprising observations!

### The joys

The biggest, and most obvious advantage, is cost. Studio apartments are cheaper, and let you live in buildings and areas you either couldn't easily afford otherwise, or where you'd have to sacrifice on other things. Clara and I lived in North Sydney, meaning we could walk to work across the Sydney Harbour Bridge. We now live in Chatswood, a suburb a bit further north but no less convenient. I feel right at home here having grown up in Singapore; the shops, food, languages, and ethnic diversity are all wonderful and familiar.

We've used all that saved money to invest, build a nest egg, and finance way more international holidays&mdash;remember those?&mdash;than we otherwise could have, without putting anything on credit [that we're not immediately paying back so we're just using them to earn points]. Clara and I are cautious, and travel is our favourite thing in the world, so it made sense to prioritise these.

Another tangible way studios save you money is forcing you to downscale and being careful when buying new things. *Where would you put it?* There's also something oddly fun about working within space constraints for certain hobbies, like finding a Laserdisc player that could *also* double as a CD, VCD, and DVD player; or figuring out a way to shoehorn and virtualise an entire homelab into a single FreeBSD server tower.

*(Whether you would classify vintage Hi-Fi gear or easily-replaceable servers as junk you don't need is another question! That's the thing about hobbies).*

Studio apartments are also easier to keep clean, and you're more motivated to do so. You can make light work of vacuuming without navigating the around doors and walls and tripping on the cable. You can't let dishes accumulate in the kitchen because you'll be sleeping in the same room. There's no out of sight, out of mind in a studio; it's either messy or clean.

They also *feel* larger without those subdividing walls, which I wasn't expecting. Most one bedroom apartments I've visited feel cramped and less airy compared to our single larger room, even if they have a greater overall floor area. It means you have a bigger loungeroom during the day, and a bedroom at night.


### The nots

The biggest, and most obvious, is space. We're in a larger studio now than we were in North Sydney, but I do miss having my own little space for computer stuff. Forget about them if you have housemates, a larger family, or a partner you're not thrilled to be spending your life with. Also forget them if you have expansive hobbies like anime figure collecting; we had to be much pickier about which characters we bring in now!

The other quibbles could be classified under sound and light. Clara and I often work overtime or are on-call in the evenings; the light from our displays and the furious sound of typing makes it nigh impossible for the other person to sleep. I love getting up early and Clara likes staying up late, so I have to tiptoe around the kitchen like a mouse each morning while boiling water and making coffee.

I get cabin fever at the best of times, but being stuck in a single room for months during Covid times was the ultimate test. I was *extraordinarily* lucky that we bought some balcony furniture and largely had nice weather, so I could set up a laptop workstation and take meetings without disturbing Clara while she worked inside. Still, there were times I'd be sitting out there, feeling claustrophobic and anxious, wondering why we'd subjected ourselves to this living arrangement!

I can count on one hand the number of times we've had people over, but this can also be difficult. We have one couch, and a tiny coffee table which doubles as our dining table. We'd get bean bags or extra cushions, but where would we put those?! It's not the best arrangement for entertaining; though if you're introverted like us it works as a convenient excuse (cough).

And finally, storage; or lack thereof. Our entire walk in laundry is full of plastic tubs from our last move, and we have the box for the TV sitting against the wall because there's nowhere else big enough to put it. We'll jettison this stuff when we eventually buy a place, but renters need to be able to pack everything at the drop of a landlord's hat.


### Conclusion

Clara and I still think the pros outweigh the cons, though this year has definitely skewed it more towards the latter for the first time. But then we think about going back to Japan at some point again in the future, and we think it's worth it.

YMMV, as they say.
