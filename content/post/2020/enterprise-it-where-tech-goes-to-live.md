---
title: "Enterprise IT, where tech goes to live"
date: "2020-11-05T09:12:03+11:00"
abstract: "It started talking about Flash’s impending deadline."
year: "2020"
category: Software
tag:
- flash
- java
- minecraft
location: Sydney
---
It's tempting in the consumer space to think certain technologies are done and dusted. Java, Flash, and IE on the desktop, for example. Beyond niche use cases, or software like Minecraft that bundle the runtimes in their packages, this is largely true.

*(Java I'll admit to missing; I'll cop the hipster cred for that. Do people still say hipster? But wow I'm happy to put Flash and IE on that list).*

Then you get into the enterprise, and all bets are off. On the one hand you have large corporate systems that were written specifically for a platform, and its scale, complexity, and technical debt ensure it won't be replaced any time soon. Then in hardware you have servers with years of uptime with BMC controllers that need specific versions of outdated software to render their interfaces. So these things keep running, even when the rest of the industry has moved on.

[Jackson Zed reminds us](https://www.servethehome.com/adobe-flash-player-sunset-looming-over-enterprise-it/) over at Serve The Home ([RSS feed here](https://www.servethehome.com/feed/)) that just running outdated Flash isn't tenable:

> Adobe Flash Player will reach its planned end of life on December 31st 2020 and, while most public websites long ago moved away from Flash-based content, a lot of enterprise software still relies on Flash (particularly older software). Making matters significantly worse, Adobe added a date-based check to outright disable Flash Player starting in January and the major web browser makers—Microsoft, Google and Mozilla — will each be updating their browsers to prevent the future use of the Flash Player plugin. 

The solution for me in these cases is to keep a few different VMs around of a certain vintage. It's not new or unique; one of the jobs I did in high school was set up a small-scale SCADA system with a DOS VM so it could run on newer, supported hardware.

Will it become accepted practice to do this for Flash, on a VM with the time set to 2000? That would introduce so many additional problems such as certificate verification, but what other choice is there? There are Flash decompilers and open source viewers, but I've read there's mixed success there. I've used that phrase a few times lately.

Anyone who could crack the enterprise IT nut with an easily-updatable system that can still tick compliance boxes and either automate or navigate tedious change management and verification could write their own paycheque.

