---
title: "An abandoned coffee shop full of letters"
date: "2020-11-12T13:44:31+11:00"
abstract: "What’s the point of delivering mail that will never be read?"
year: "2020"
category: Thoughts
tag:
- australia
- cafes
- north-sydney
- sydney
location: Sydney
---
I walked past a closed coffee shop in North Sydney that shut down for renovations a while ago and didn't reopen, presumably because of COVID. The large glass doors were coated in a thin layer of dust from the inside, and a patch of bright paint on an otherwise faded wall hinted at what its name used to be.

*(I sat there a few times when Clara and I lived in the area. Chances are some posts here from 2018 were written there)!*

What struck me was how many letters had been stuffed under the door. Even just at a glance I saw several "To the occupant", and more with a person's name. So many pizza coupons.

It made me think how it would have got to that stage. A junk mailer would have had to stuff it there, with a clear view that the place was abandoned. A [postie](https://www.urbandictionary.com/define.php?term=Postie) would have seen the pile of unread mail and continued to push it under the door.

These actions make logical sense when you understand their motivation. A junk mailer is paid by number of items distributed in a given area. A postie is contractually obligated to deliver mail, in one piece, to the place to which it's addressed. Neither of these actors care, or need to, that the mail is being received and read. It's not in their job descriptions, nor are they being paid for it. Perversely, withholding their communiques would go *against* what's expected of them.

It's all so *backwards*. What's the point of mail if it's not being read?

