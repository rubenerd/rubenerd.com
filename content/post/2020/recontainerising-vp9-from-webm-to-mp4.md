---
title: "Re-containerising VP9 from webm to MP4"
date: "2020-10-08T16:44:30+11:00"
abstract: "I didn’t think software would recognise VP9 in MP4 containers."
year: "2020"
category: Software
tag:
- encoders
- guides
- mp4
- video
location: Sydney
---
Recently I learned that VP9 video can be re-containerised to something other than webm, without re-encoding the original video and/or audio. This is useful for importing into tools that support the VP9 codec, but not the webm container.

Recent versions of `ffmpeg(1)` have a `-c copy` shorthand for instructing the audio and video encoders to just copy the source: 

    $ ffmpeg -i example.webm -c copy example.mp4

Note that you'll get an error if you try to output to a mov container, as I've seen plenty of bloggers and forum posts erroneously suggest:

    [mov @ 0xHEX] vp9 only supported in MP4

ffmpeg can also re-containerise into my preferred Matroska, which I've also since learned is the format on which webm is based:

    $ ffmpeg -i example.webm -c copy example.mkv  

Tools like DaVinci Resolve can now import your VP9 video, albeit with variable results. You may need to re-encode entirely if the output is garbled.

    $ ffmpeg -i example.webm example.h264.mp4

