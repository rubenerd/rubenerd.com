---
title: "The media's culpability in the NBN"
date: "2020-12-18T09:08:00+11:00"
abstract: "Jeremy Ray pulls no punches in his article for The Shot"
year: "2020"
category: Internet
tag:
- australia
- nbn
- politics
location: Sydney
---
[Jeremy Ray pulled no punches](https://theshot.net.au/general-news/false-balance-how-australias-media-fucked-up-the-nbn/) in his retrospective on Australia's National Broadband Network for The Shot earlier this month:

> In 2013, Australia reached a fork in the road. One side, Kevin Rudd’s governing ALP, had a truly world-class proposal for the future of the country’s internet. The other, Tony Abbott’s LNP Coalition, did not. And our mainstream media fucked it up colossally. Not just Murdoch, all of it.
> 
> [..] Communications technology was a less sensational issue buried amongst all the borders and the boats. The NBN was a chance for politicians to do their businessman impersonations, using focus-grouped phrases like “Multi-Technology Mix” while hoping journalists wouldn’t ask why they were allergic to fibre – moral or otherwise. And the caper worked. The fourth estate slept on Australia’s future and we hurtled to our copper fate. 

He also discussed the ABC's dispicable treatment of Nick Ross, who had the audacity to write qualified articles and correct predictions about the Coalition's more expensive, sub-par NBN alternative.

It seemed everyone in the industry was warning about how bad it was at the time, yet the general press ignored us. The Coalition's lines went unchallenged, and the voting public took it as fact that Labor's plan would have been worse. We're now living with the consequences.

It reminds me of my dad talking about how grumpy he'd get reading about his industry in the media, and how many details they routinely got wrong. His lightbulb moment was realising that if they could get *his* industry wrong, do they *ever* get anything right? I don't think it's as bleak as that, though this NBN coverage&mdash;or lack thereof&mdash;has been illustrative in how the press handles technical issues their financial backers want buried. If I've seen it play out in my own niche, where else is it happening? 

About all I took exception with in the original article was Jeremy's chariterisation:

> Rare is the meaningful difference between Australia’s two major political parties 

They really are more meaningfully different than people think, though the same media that buried the NBN would have us believe [they're the same](https://rubenerd.com/what-are-the-biggest-lies-youve-been-told/ "What are the biggest lies you’ve been told?"). As the case with the NBN, we can't let them get away with that.
