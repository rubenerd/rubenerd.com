---
title: "Thirty years since German reunification"
date: "2020-10-03T18:07:07+10:00"
abstract: "East Germany folded into the West on the 3rd of October 1990."
thumb: "https://rubenerd.com/files/2020/berlin-wall-fall@1x.jpg"
year: "2020"
category: Thoughts
tag:
- germany
- history
- politics
location: Sydney
---
It was thirty years ago today when the Deutsche Demokratische Republik, or East Germany, ceased to exist. On the 3rd of October 1990, the DDR was dissolved, and its post-war Bezirke districts were reconstituted into their original Länder provinces, before being incorporated into the Bundesrepublik we now know simply as Germany. 

It's surreal to think my sister and I were around for this huge occasion, but were too young to be aware of it. My mum told me years later that my German dad cried when the news broke, saying "the war is over".

Challenges still remain thirty years after reunification, but at least we can all take a little comfort knowing Margaret Thatcher didn't like it!

<p><img src="https://rubenerd.com/files/2020/berlin-wall-fall@1x.jpg" srcset="https://rubenerd.com/files/2020/berlin-wall-fall@1x.jpg 1x, https://rubenerd.com/files/2020/berlin-wall-fall@2x.jpg 2x" alt="Police personnel (NCOs and enlisted men) of the East German Volkspolizei wait for the official opening of the Brandenburg Gate, December 22nd 1989." style="width:500px" /></p>

[This photo](https://commons.wikimedia.org/wiki/File:Volkspolizei_at_the_official_opening_of_the_Brandenburg_Gate.jpg) has stuck with me ever since I started reading about modern German history. These young men were part of the East German police on the day the Berlin Wall fell, taken by SSGT F. Lee Corkran of the US military in 1989. East Germany dissolved a year later. I've always wondered what they were thinking.

The Scorpion's *Wind of Change* will be my next Music Monday. I think we all need a bit of optimism during these times.

