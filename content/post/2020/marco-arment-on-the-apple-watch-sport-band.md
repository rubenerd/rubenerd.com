---
title: "Marco Arment on the Apple Watch sport band"
date: "2020-11-22T10:34:35+11:00"
abstract: "The sports band is the cheapest, and the best."
year: "2020"
category: Thoughts
tag:
- apple
- apple-watch
- atp
- marco-arment
- watches
location: Sydney
---
I'm glad to hear Marco is doing okay, big love from Clara and I to him and his family. ♡ On the subject of Apple Watches, Marco talked about how he'd gone back to the cheapest sports band on the [most recent ATP episode](https://atp.fm/405 "Accidental Tech Podcast #405"):

> I don't know if its just the way the sizing works on me, but the sport band is just a little bit more comfortable. And I find the loop to be a little hotter. The sport band has that little excess tail that tucks under it, and because it has the nine holes in it&mdash;or you can go even holier with them&mdash;there is some degree of ventilation that you get. [..] the solo loop almost fits too well, and so as a result I find it less comfortable.

The guys also mentioned that you could mix and match different sizes for the two discrete parts of the sports band for a better fit as well.

I thought I was the only one who thought this! I tried a few different bands when I used to wear Apple Watches, and the sports band was easily the most durable, comfortable, and easiest to clean. This is especially important when you live in hot and humid places, and a genuine surprise given it's two glorified slabs of rubber.

It's a moot point now given I wear a beautiful little $25 analogue Casio I got in Akihabara, but I like keeping an eye on the space.

