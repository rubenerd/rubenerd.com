---
title: "Building a new homelab server"
date: "2020-12-03T15:00:00+11:00"
abstract: "It all started with a beautiful little Supermicro board on Gumtree for peanuts!"
thumb: "https://rubenerd.com/files/2020/x11sae-m-top-view_0.jpg"
year: "2020"
category: Hardware
tag:
- antec-300
- bsd
- corsair
- ecc
- freebsd
- homelab
- iomega
- jaz-drive
- microservers
- minecraft
- noctua
- zfs
- xeon
- zip-drive
location: Sydney
---
I love my little HP Microservers cubes, but they're starting to show their age. They've performed admirably over the years and taught me everything from Xen and InfiniBand, to Minecraft and Plex. But I've extended them well beyond what they were designed for, and they're loud when under the concurrent loads Clara and I subject them to now. I also want some better performance for media transcoding, and CPU features like AES-NI for encrypted storage. In the words of Gough Whitlam, *it's time*.

I enjoyed building my game machine more than the games I ended up playing on it, funnily enough, so I decided to channel my interest into building a new homelab server again. The first spark came from seeing a few generations old workstation Supermicro board on Gumtree for peanuts, and rediscovering the majesty of the Antec 300 case. Suddenly I was down the rabbit hole again researching the best parts for the dollar for this new machine.

This is what I've cobbled together over the past few months for far less money than I would have expected! All are refurbished or second hand except the CPU cooler and power supply.

<img src="https://rubenerd.com/files/2020/x11sae-m-top-view_0.jpg" alt="Supermicro X11SAE-M board" style="width:128px; float:right; margin:0 0 2em 2em;" />

* [Supermicro X11SAE-M](https://www.supermicro.com/en/products/motherboard/X11SAE-M). Micro-ATX LGA1151 board with 8 SATA ports and USB 3 headers, and [C236](https://ark.intel.com/content/www/us/en/ark/products/90594/intel-c236-chipset.html) for ECC memory and VT-d support. The case could easily fit a larger board, but this does all I need.

* [Xeon E3 1275 v6](https://ark.intel.com/content/www/us/en/ark/products/97478/intel-xeon-processor-e3-1275-v6-8m-cache-3-80-ghz.html). Best cheap chip you can get for this board before the price and TDP shoot up. 4C/8T, 3.8 GHz with 4.2 GHz burst, and integrated graphics so I don't need a separate GPU card to view POST before jumping to SSH. Insert benchmark disclaimer here, but its Geekbench score is more than 5x the Microservers’.

* 2x 16 GiB [Kingston PC4-19200 DDR4-2400 ECC](https://www.newegg.com/global/au-en/kingston-16gb-288-pin-ddr4-sdram/p/0ZK-06BE-00006?Item=0ZK-06BE-00006), unbuffered, unregistered DIMMs. Was aiming for Crucial/Micron, but got these tested second-hand for almost nothing.

* [Corsair HX-750 Platinum](https://www.corsair.com/us/en/Categories/Products/Power-Supply-Units/hxi-series-2017-config/p/CP-9020137-NA). Modular, quiet power supply from a brand that's earned my trust over the years, and supports 9x SATA devices. Spent bit extra for Platinum over Gold because it'll be running 24/7. 

<img src="https://rubenerd.com/files/2020/noctua_nh_u12a_1_2.jpg" alt="Photo of the Noctua NH-U12A" style="width:128px; float:right; margin:0 0 2em 2em" />

* [Noctua NH-U12A](https://noctua.at/en/nh-u12a). CPU radiator and cooler with the best thermal performance and noise in its class, as expected. Only thing on this list I splurged on, because it's such a beautiful device! I love precision engineering like this.

* Internal Zip, Jaz, and Hex Speed Creative CD-ROM from my first computers in the 1990s, or bought for nostalgia. Will the ever be connected internally? Probably not! But it tickles me having them in a modern(ish) tower.

* [Lenovo Ultrabay 9.5mm Multi Burner](https://support.lenovo.com/au/en/solutions/pd024257-thinkpad-ultrabay-dvd-burner-95mm-slim-drive-iii-overview) in an [Icy Dock Flex Fit Duo](https://www.newegg.com/global/au-en/icy-dock-mb343spo/p/N82E16817994210), alongside the aforementioned Jaz drive. It's all this massive tower has space for after I've added all that other pointless hardware above! Will be mostly used for reading CDs and DVDs to rip, etc.

And on the software side:

<p><img src="https://rubenerd.com/files/2020/venusheart@1x.jpg" alt="Sailor Venus-chan" srcset="https://rubenerd.com/files/2020/venusheart@2x.jpg 2x" style="width:128px; float:right; margin:0 0 2em 2em;" /></p>

* [FreeBSD](https://www.freebsd.org/) 12.x or 13-CURRENT [with Xen](https://www.freebsd.org/doc/handbook/virtualization-host-xen.html), running FreeBSD and Debian guests VMs. The exact configuration is TBD, including what gets a guest or a jail.

* She's inheriting the hostname aino.lan, from Sailor V and Sailor Moon. My first Pentium 1 tower got the name Mizuno Ami, so she's playing the supporting role! Thinking of getting [this RedBubble magnet](https://www.redbubble.com/i/magnet/Sailor-Moon-Heart-by-KokoroPopShop/35961352.FF85S) to make it official, she's adorable.

Next steps:

* Getting all these parts delivered at some point! They're slowly trickling in, but might be a while still.

* Getting some matching coloured SATA cable pairs for my mirrored ZFS pools. [PC Case Gear](https://www.pccasegear.com/category/19_104/cables/sata-cables) has some nice coloured-sleeve ones.

Stay tuned for more updates :).
