---
title: "Friedrich Nietzsche on purpose"
date: "2020-12-17T17:23:00+11:00"
abstract: "My time has not yet come either"
year: "2020"
category: Thoughts
tag:
- friedrich-nietzsche
- philosophy
- quotes
location: Sydney
---
From Ecce Homo, 1888:

> My time has not yet come either; some are born posthumously.

