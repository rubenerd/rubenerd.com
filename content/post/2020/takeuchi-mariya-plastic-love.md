---
title: "Takeuchi Mariya, Plastic Love"
date: "2020-11-23T22:09:00+11:00"
abstract: "It risks being a cliché, but it's still so good."
year: "2020"
thumb: "https://rubenerd.com/files/2020/yt-3bNITQR4Uso@1x.jpg"
category: Media
tag:
- city-pop
- electronica
- japanese
- music
- music-monday
location: Sydney
---
Today's [Music Monday](https://rubenerd.com/tag/music-monday/) was found entirely by accident, but now I fear I'm entering the dangerous world of 1980s Japanese *city pop*. This song appears in so many electronica playlists and recommendations that it risks becoming a clich&eacute;, but it's absolutely worth it!

<p><a href="https://www.youtube.com/watch?v=3bNITQR4Uso" title="Play Mariya Takeuchi 竹内 まりや Plastic Love"><img src="https://rubenerd.com/files/2020/yt-3bNITQR4Uso@1x.jpg" srcset="https://rubenerd.com/files/2020/yt-3bNITQR4Uso@1x.jpg 1x, https://rubenerd.com/files/2020/yt-3bNITQR4Uso@2x.jpg 2x" alt="Play Mariya Takeuchi 竹内 まりや Plastic Love" style="width:500px;height:281px;" /></a></p>

I'm not that into cyberpunk, but I do love that 1980s and early 1990s aesthetic. I could easily imagine someone cruising around in the evening listing to this song, or sitting on the MRT with my cassette Walkman.

You can buy her [whole album on iTunes](https://music.apple.com/jp/album/variety/937664629?l=en), which I encourage you to do. I may have also snagged a hard copy off Jauce, not that I'd admit to this.

