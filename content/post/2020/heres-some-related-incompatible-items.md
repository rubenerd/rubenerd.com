---
title: "Here are some related, incompatible items"
date: "2020-11-24T09:42:00+11:00"
abstract: "Related items don’t work for specific electronic components."
emacs: "2020"
category: Hardware
tag:
- cpus
- design
- shopping
location: Sydney
---
Picture this: you're arrived at an online store to buy a specific CPU for a new server build. It might be a new bhyve box you're building, or a Minecraft server, or a new NAS to test the new ZFS inline encryption and RAID options, or *why not both?* That only works if I had two uses there, not three.

As is typical in Australia, the online store doesn't have that CPU in stock, and haven't for months, but they keep the listing up for search engine juice, and say they can order it in for it to arrive in the next three to four years. If you don't have the time for the heat death of the universe for their false advertising to come true again, they will also list some "related items" under the device in question.

Thing is, while an AMD EPYC CPU does look interesting, and is an aspirational item on my wish list, it's not at all related to a Xeon E3 CPU needed for a C236-chipset motherboard. It'd be akin to having a bottle of chilli sauce listed as a related item for breakfast cereal because they're both food, or other *Nisekoi* character stickers when you were after one for best girl Onodera.

The fact these two devices are CPUs *do not* make them related items. Matching superficial attributes like this is entirely meaningless, as they have different system requirements, incompatible sockets, and aren't even the same generation. You could make a click-baitey video where you force an EPYC CPU onto a H4 socket, then redirect video from another AMD computer to pretend it worked, but those are the sorts of shenanigans I definitely, absolutely wouldn't do to make a point.

People joke about how their recommended lists will be full of mattresses after they buy a bed, despite not needing one anymore because they just bought it! I'd say this is a close second.

Related items for this post: a picture of Neptune I [blogged about in 2008](https://rubenerd.com/p2921/). These are both blog posts, right?
