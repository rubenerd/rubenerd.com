---
title: "How we respond to crises"
date: "2020-12-18T13:53:00+11:00"
abstract: "Bad things are going to happen. That's not negotiable. What is, is how you deal with it."
year: "2020"
category: Thoughts
tag:
- covid-19
- keanu-reeves
- quotes
location: Sydney
---
From [Respectful Memes](https://twitter.com/RespectfulMemes/status/1339614150226563083/photo/1)\:

<img src="https://rubenerd.com/files/2020/keanu-bad-things@1x.jpg" alt="Bad things are going to happen. That's not negotiable. What is, is how you deal with it" srcset="https://rubenerd.com/files/2020/keanu-bad-things@2x.jpg 2x" style="width:360px;" />
