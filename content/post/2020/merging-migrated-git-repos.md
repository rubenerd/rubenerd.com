---
title: "Merging migrated Git repos"
date: "2020-10-11T17:03:31+11:00"
abstract: "Because GitHub would rather host software for ICE."
year: "2020"
category: Software
tag:
- git
- guides
location: Sydney
---
I was moving some more repositories off GitHub this afternoon. You'd have to disappear into the mountains if you refused to use things made by anyone you didn't 100% agree with, but the effort to change easily passes the threshold considering the [ghastly things](https://www.theverge.com/2019/10/9/20906213/github-ice-microsoft-software-email-contract-immigration-nonprofit-donation) they're proud to take money to host.

Turns out I'd already migrated some repos a few months earlier, and forgot to [change the origin URL](https://rubenerd.com/change-your-git-repo-source-url/). I'd still been committing changes to GitHub and didn't notice. Whoops!

So now I had an updated repo on GitHub, and stale repo at my target that I wanted to use. How to merge the changes on the GitHub version back into the target? [Smar had the perfect answer](https://stackoverflow.com/a/6316468) for this use case:

    $ git clone $TARGET_REPO_URL
    $ cd $TARGET_REPO_NAME
    $ git fetch $GITHUB_REPO_URL master:merging
    $ git merge --allow-unrelated-histories merging
    $ git commit

Some of this is [going to my Codeberg](https://codeberg.org/rubenerd) for now while I finish building my new FreeBSD services box. I might be setting up an OrionVM VM with jails for WebDAV/CalDAV, Git, Subversion, and Minecraft. But you didn't read that last one yet.

