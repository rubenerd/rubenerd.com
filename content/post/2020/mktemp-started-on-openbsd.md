---
title: "Mktemp started on OpenBSD"
date: "2020-11-11T09:04:14+11:00"
abstract: "It's worth remembering just how much of the Internet, and even Linux distributions, depends on the BSDs."
year: "2020"
category: Software
tag:
- bsd
- freebsd
- openbsd
- openssh
- security
location: Sydney
---
Today I learned the indispensible tool for creating safe temporary files and folders began life on OpenBSD. [From the project's website](https://www.mktemp.org/)\:

> Mktemp is a small program to allow safe temporary file creation from shell scripts. The current release is version 1.7, released on April 25, 2010. Mktemp is free software and is distributed under a ISC-style license. Mktemp was written by Todd C. Miller.
>
> A different, but compatible, implementation of mktemp is available as part of GNU coreutils.

It's worth remembering just how much of the Internet, and even Linux distributions, depends on infrastructure and tech that either started life on the BSDs, or is still actively maintained in a BSD project. OpenBSD especially punches well above its weight, often with little or no recognition in the Linux community.

It makes me wonder when tools I otherwise love like Ansible get bought for millions of dollars. Ansible depends *entirely* on OpenSSH for its operation. Did OpenBSD get a cent from that? What about Python? BSD/ISC/MIT and the GPL don't require it, but seems like a [good insurance policy](https://rubenerd.com/firefoxs-situation-reminds-me-of-openssl/), if not just common courtesy.

*You can [donate to the OpenBSD Foundation here](https://www.openbsdfoundation.org/donations.html).*

