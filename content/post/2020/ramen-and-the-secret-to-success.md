---
title: "Ramen, and the secret to success"
date: "2020-11-14T20:36:35+11:00"
abstract: "I felt this news article in my soul. I long hard for a future when we can experience it again."
year: "2020"
category: Thoughts
tag:
- food
- japan
- ramen
location: Blue-Mountains
---
I was catching up on RSS from our little Blue Mountains hotel where we're spending a few days leave, and read [Justin McCurry's fantastic article from Yokohama](https://www.theguardian.com/world/2020/nov/13/return-of-a-ramen-pioneer-gives-boost-to-japans-covid-hit-restaurant-sector)\: 

> More than a century after it welcomed its first ravenous customers in downtown Tokyo, Rai Rai Ken is back in business. On a recent afternoon, diners at its new premises in the bowels of the Shin-Yokohama Ramen Museum barely looked up as they demolished servings of noodles made according to the restaurant’s original recipe.

I felt that passage in my soul. Some of my most treasured memories are sitting with Clara in tiny roadside restaurants slurping down hot food in the cold in Japan. I long hard for a future when we can do it again.

Justin quotes ramen writer and researcher Kazuaki Tanaka about the secret to success in the competitive world of ramen. There's advise for all of us: 

> “The secret lies is doing something different to everyone else,” he says. “It’s about the soup, the noodles, the selection of toppings, and how they all work together. If you can do that properly then people will queue up to eat your ramen. And they will keep coming back.”

