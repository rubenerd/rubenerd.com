---
title: "The world’s chunkiest card reader"
date: "2020-12-30T08:28:00+11:00"
abstract: "An old Iomega combo USB optical drive and card reader now graces my FreeBSD tower for no good reason!"
year: "2020"
thumb: "https://rubenerd.com/files/2020/iomega-cdrw@1x.jpg"
category: Hardware
tag:
- iomega
- nostalgia
location: Sydney
---
Those of you familiar with my ramblings know I have a penchant for vintage computer hardware, *especially* if I can somehow shoehorn it into a contemporary system. There's something so fun, and almost magical, about having lights blink from a piece of hardware long past its use-by date. It's like I'm literally keeping a part of computing history alive. 

I also have a specific interest in Iomega hardware from the 1990s and early 2000s, having grown up using their kit and been interested in their advertising and designs. They were doing coloured peripherals right alongside SGI, and years before Apple introduced the iMac. They weren't the most reliable drives by *any* stretch, but I adored my Zip drive for backups, ferrying school work, and stretching my paltry hard drive. I understood why writable optical media made more sense economically and for compatibility by the end of the decade, but super-floppies were way more convenient.

<img src="https://rubenerd.com/files/2020/iomega-cdrw@1x.jpg" alt="Photo showing the CD-RW card reader on top of my FreeBSD server" style="width:500px; height:333px" srcset="https://rubenerd.com/files/2020/iomega-cdrw@2x.jpg 2x" />

Clara and I were at Vinnies looking for some second-hand RCA cables and records for our Hi-Fi system, when I chanced upon an external Iomega CD-RW Plus Drive, model 32888. I'd had my eye out for one of their external purple "Zip" branded units, and a silver USB 32971 floppy drive with card reader, but I'd never seen this chunky boy before. And I couldn't refuse for $20!

The drive and cables were in immaculate condition, almost as though it was never used. [CNET](https://www.cnet.com/products/iomega-cd-rw-52x-dvd-rom-plus-7-in-1-card-reader-usb-2-0-external-drive-usb-1-1-card-reader/) lists it as being compatible with any 75 MHz machine running classic MacOS or Windows 98! It has a CD-RW and DVD-ROM optical drive (52x24x52x/16x), a USB port, and a card reader for SD/MMC, CompactFlash, SmartMedia (!) and M8 Memory Stick.

Unlike other drives which multiplex across one cable or use an internal hub, this drive has two discrete USB 2.0 cables for each of the devices. This meant I could plug it into my new FreeBSD tower, and it just worked! Here's `dmesg(8)` for the CD:

	ugen0.6: <vendor 0x059b IOMEGA CDDVD522416EC3-C> at usbus0
	umass3 on uhub0
	umass3: <vendor 0x059b IOMEGA CDDVD522416EC3-C, class 0/0, rev 2.00/1.03, addr 6> on usbus0
	umass3:  SCSI over Bulk-Only; quirks = 0x0100
	umass3:12:3: Attached to scbus12
	cd2 at umass-sim3 bus 3 scbus12 target 0 lun 0
	cd2: <IOMEGA CDDVD522416EC3-C 0P5B> Removable CD-ROM SCSI device
	cd2: 40.000MB/s transfers
	cd2: Attempt to query device size failed: NOT READY, Medium not present - tray closed
	cd2: quirks=0x10<10_BYTE_ONLY>

And for the card reader, with each slot being mapped to its own device:

	ugen0.5: <IOI MediaBay 7 in 4> at usbus0
	umass2 on uhub0
	umass2: <IOI MediaBay 7 in 4, class 0/0, rev 1.10/1.00, addr 5> on usbus0
	umass2:  SCSI over Bulk-Only; quirks = 0xc000
	umass2:11:2: Attached to scbus11
	da2 at umass-sim2 bus 2 scbus11 target 0 lun 0
	da2: <IOI MediaBay 7 in 4 1.00> Removable Direct Access SCSI device
	da2: Serial Number 9202261
	da2: 1.000MB/s transfers
	da2: Attempt to query device size failed: NOT READY, Medium not present
	da2: quirks=0x2<NO_6_BYTE>
	da3 at umass-sim2 bus 2 scbus11 target 0 lun 1
	da3: <IOI MediaBay 7 in 4 1.01> Removable Direct Access SCSI device
	da3: Serial Number 9202261
	da3: 1.000MB/s transfers
	da3: Attempt to query device size failed: NOT READY, Medium not present
	da3: quirks=0x2<NO_6_BYTE>
	da4 at umass-sim2 bus 2 scbus11 target 0 lun 2
	da4: <IOI MediaBay 7 in 4 1.02> Removable Direct Access SCSI device
	da4: Serial Number 9202261
	da4: 1.000MB/s transfers
	da4: Attempt to query device size failed: NOT READY, Medium not present
	da4: quirks=0x2<NO_6_BYTE>
	da5 at umass-sim2 bus 2 scbus11 target 0 lun 3
	da5: <IOI MediaBay 7 in 4 1.03> Removable Direct Access SCSI device
	da5: Serial Number 9202261
	da5: 1.000MB/s transfers
	da5: Attempt to query device size failed: NOT READY, Medium not present
	da5: quirks=0x2<NO_6_BYTE>

I don't think the card reader will be compatible with any of the current SD cards I use, and even if it were, it'd be too slow to copy over all the raw images in a reasonable time. I'm also not sure if I'll ever be burning CDs again. But that's not the point :).
