---
title: "The United States of Apathy?"
date: "2020-10-27T13:03:44+11:00"
abstract: "Voter disenfranchisement, bad candidates, and inaccessibility aren't apathy."
year: "2020"
category: Thoughts
tag:
- politics
- united-states
location: Sydney
---
I'm invoking [Betteridge's Law](https://en.wikipedia.org/wiki/Betteridge%27s_Law_of_Headlines) here, because this post discusses the [widely-circulated map](https://twitter.com/simongerman600/status/990908747231920128) by Philip Kerney titled as such. It compared the outcome of the 2016 US elections with the number of people who didn't vote, with the implication that both Ms Clinton and Mr Orange lost to "Nobody".

It's an interesting visualisation, though comes with the caveat "if abstention from voting counted as a vote for Nobody". But read the comment threads where this is posted, and a surprising number don't read beyond the "Apathy" in the heading.

Voter disenfranchisement is a real and terrifying phenomena, even in the world's second-largest democracy. Elections are called at times where large populations of people can't get a work break, and in a Dickensian industrial-relations environment where they can't demand it. People might be stuck at home. Absentee and mail ballots exist, but there are vested interests keeping people away from those.

Apathy can come from not caring to vote, but is also a reflection of attitudes towards the candidates themselves. Politics is the art of the possible, and Hillary Clinton simply didn't resonate with voters enough to turn them out against Mr Orange. Even those who would have sided with her didn't feel pressure to contribute, given the press call the odds in her favour. And all Mr Orange's embarrassing bluster didn't change the fact a record low number of people voted for him as well.

If push came to shove, why *would* you sacrifice precious pay and work hours to vote for a candidate who doesn't speak to you, or in a state where the winner is almost guaranteed? And most perversely of all, it's the people living paycheque to paycheque like this who are disproportionally affected by changes in government. 

My first thought is to analyse the motivation behind the messaging when I read commentary about lazy voters, or lazy social security recipients. Here it comes across as blaming voters for difficulties voting, and for poor quality candidates. Should people put the effort in to vote? Yes, if they can. Are both sides of politics as bad as each other? [No, not even close](https://rubenerd.com/what-are-the-biggest-lies-youve-been-told/ "Blog post: What are the biggest lies you’ve been told?"). But even if voters are at fault here for not engaging in their civic duty and responsibility, there are plenty of other stakeholders who should equally share the blame. The political machines. The parties. The press. It wasn't just apathy.

*(I say all this as someone who's voted in every Australian state and federal election for which I've been eligible, in case people raise this).*
