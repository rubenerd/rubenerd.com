---
title: "Block copy disk over SSH with gzip"
date: "2020-12-08T17:07:00+11:00"
abstract: "dd if=$SOURCE_DISK | gzip -1 - | pv | ssh $TARGET_SERVER 'gunzip -1 - | dd of='$TARGET_DISK'"
year: "2020"
category: Software
tag:
- bsd
- debian
- freebsd
- guides
- linux
- netbsd
- openbsd
- ssh
- things-you-already-know-unless-you-dont
location: Sydney
---
We haven't had a [things you already know, unless you don't](https://rubenerd.com/tag/things-you-already-know-unless-you-dont) installment since March. So many things have happened in the interim, not least the need to copy disks over a network.

$SOURCE

	$ dd if=$SOURCE_DISK | gzip -1 - | pv | ssh $TARGET_SERVER "gunzip -1 - | dd of=$TARGET_DISK"

This will copy a block device across SSH to a remote server, compressing with gzip to remove empty space, and pv to show progress.
