---
title: "Security flaws in smart doorbells"
date: "2020-11-27T08:15:00+11:00"
abstract: "The S in IoT stands for security"
year: "2020"
category: Hardware
tag:
- news
- privacy
- security
location: Sydney
---
The BBC's technology news site ([RSS feed here](http://feeds.bbci.co.uk/news/technology/rss.xml)) ran a story about [insecure doorbells being sold](https://www.bbc.com/news/technology-55044568) on large, trusted websites: 

> The watchdog tested 11 devices which were purchased from popular online marketplaces in the UK. Brands included Qihoo, Ctronics and Victure. It found that among the most common flaws were weak password policies, and a lack of data encryption. Two of the devices in the test could be manipulated to steal network passwords and then hack other smart devices within the home.

Once again I'm compelled to quote FreeBSD maintainer and author George Neville-Neil: *the S in IoT stands for Security*. We in the industry had the chance to introduce a new market segment where security and trust were foundational to its design, and we didn't. We won't for the next class of devices either, because business priorities simply don't align with data privacy and security.

The other overarching issue here is the blaz&eacute; attitude about smart devices like this. People acknowledge that these things need secure passwords, but so what if someone gains access? It's *just* a smart lightbulb, or a doorbell. I don't even fault the general public for this; these devices are sold with the words "encryption", "secure", and most dangerous of all: "simple".

The word *virus* is such an elegant descriptor to describe attacks, even though we've agreed they only describe automated, self-propagating tools. All a disease needs is a foothold, after which it can spread and cause damage. This is precisely why these IoT devices are so dangerous: people think they're low risk, and they're connecting them to the same networks that host their computers and phones. 

So where do we go from here? How do we advise loved ones and friends in our capacity as nerds from whom they ask advice? I don't think I got that sentence right.

There are online guides that argue these devices should be run on isolated networks to reduce the impact of issues, such as on a new VLAN or wireless hotspot. They're important for underscoring why these devices are dangerous, and their advice is practical. But leaving aside the idea that laypeople would even know to search for these guides let alone understand them, they unwittingly pass the buck and responsibility from the businesses selling these devices onto consumers. Where have we all heard that before? And how does that fit with being billed as "easy to install and use"? *Caveat emptor* is a tough sell for people who aren't in IT, and where false advertising is so abundant.

I've since read there are people who's quality of life has been dramatically improved by the improvements to accessibility these devices bring. That makes me unreasonably happy, provided they're installed and maintained properly; the latter of which is a *whole other topic* about IoT! But I argue the cost simply isn't worth it for most people. Not until we get some proper protections in place, in whatever form they end up taking. 

Heaven help me if a Hololive-themed smart clock or something equally pointless were to ever come out though *cough*.
