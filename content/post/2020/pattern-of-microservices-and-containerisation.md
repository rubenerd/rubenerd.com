---
title: "Microservices and containerisation"
date: "2020-11-04T10:46:39+11:00"
abstract: "Good ideas taken too far and applied too bluntly."
year: "2020"
category: Software
tag:
- containers
- jails
- microservices
- zones
location: Sydney
---
If you want a well-researched, detailed, and useful distraction from the news today, check out [Vasco Figueira's microservices post](https://vlfig.me/posts/microservices); emphasis added.

> Some recent backtracking from what we have been calling “Microservices” has sparked anew the debate around that software architecture pattern. It turns out that for increasingly more software people, having a backend with (sometimes several) hundreds of services wasn’t that great an idea after all. The debate has been going on for a while and much has already been said, but there are still a couple of things I’d like to say.
> 
> TL;DR “Microservices” was a **good idea taken too far and applied too bluntly**. The fix isn’t just to dial back the granularity knob but instead to 1) focus on the split-join criteria as opposed to size; and 2) differentiate between the project model and the deployment model when applying them.

I see parallels with the industry's current infatuation with containerisation, or as a client once so elegantly put it, *more leaky abstractions that break in new and wonderful ways*. They both have their uses, but broadly if our aim was to simplify, secure, and optimise these ever more complex architectures, microservices and containers have been&mdash;at best&mdash;a mixed blessing.

*(I say this as someone who uses FreeBSD jails and virtualisation at the drop of a hat, and wished the industry had broadly adopted Solaris/illumos-style zones instead. Nothing so perfectly encapsulates the industry's hubris and self-justification than k8s. But that's a topic for another post. Don't email me!)*

Even if all this sounded like word salad to you, this is the highlight of the whole article which could easily apply to every technical human endeavour:

> Having a ready answer when the thinking gets tough is a soothing lie that just moves complexity about. There is no substitute [for the] application of cognitive power to a problem.


