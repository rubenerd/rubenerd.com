---
title: "Raf Czlonka on BSD licencing"
date: "2020-10-24T21:37:24+11:00"
abstract: "Acceptable licence policy on OpenBSD, and FreeBSD FAQ links."
year: "2020"
category: Software
tag:
- bsd
- freebsd
- openbsd
location: Sydney
---
Raf emailed with a useful comment about my [FreeBSD licencing post](https://rubenerd.com/and-im-surprised-theyre-surprised/ "People asking about FreeBSD licencing"), and why people routinely ask for GPL software in base:

> It might be worth noting that OpenBSD has [acceptable licenses policy
page](https://www.openbsd.org/policy.html) - linked from [goals page](https://www.openbsd.org/goals.html) (itself linked to from the main
page). I see that FreeBSD has [something similar](https://www.freebsd.org/internal/software-license.html) and it is even
touch upon in the [FAQ](https://www.freebsd.org/doc/en_US.ISO8859-1/books/faq/introduction.html#idp44797176) but perhaps these aren't visible enough
or, more likely, people simply don't (like to) read.

Agreed. I still lean towards thinking it's more about awareness, and it's an opportunity to help reframe the discussion away from the GPL. But eventually people do need to be motivated to find things out themselves as well.

But it does raise another interesting point: why don't people like reading FAQs? Are they considered ancillary, a waste of time, or boring? Is there anything we can do there too? I'm not sure.

Also, I keep being given reasons to try OpenBSD. You sneaky people. 🐡
