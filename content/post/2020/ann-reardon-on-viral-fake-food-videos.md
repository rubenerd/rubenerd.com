---
title: "Ann Reardon on viral fake, food videos"
date: "2020-12-20T10:07:00+11:00"
abstract: "Deconstructing why they exist, and whey they're harmful"
thumb: "https://rubenerd.com/files/2020/how-to-cook-that@1x.jpg"
year: "2020"
category: Media
tag:
- ethics
- video
- youtube
location: Sydney
---
I spent so long on the title for this before giving up and removing doubt with extra commas. Was it a viral video about fake food, or the fakeness was viral, or the viral fake food had a video? I suppose all of them apply.

Ann Reardon of *[How to Cook That](https://www.youtube.com/channel/UCsP7Bpw36J666Fct5M8u-ZA)* is Clara's and my favourite food scientist on YouTube, and she posts from Australia! She has made some spectacular things, including a chibi anime cake for Final Fantasy, and my current favourite 3D optical illusion. She's also known for her well-researched, thorough, and fair debunking videos where she takes some of the most atrocious "five minute hacks" and the like that we've all seen recommended to us at some point, and schools us on why they're either vague, misleading, or outright lies.

One of the most important antidotes we have to bad information is good information, and I appreciate all the effort people like Ann put into this. Someone with her skills could easily make more money doing, as we would say here, *dodgy shit*. Which gets us to the core issue: readers here would know I'm always interested in understanding and deconstructing the motivation to behave like this. Ann breaks it down towards the [end of this Blossom debunking video](https://www.youtube.com/watch?v=vSBSzWmjXO0)\:

> This sort of stuff is getting promoted by [YouTube's] algorithm. I think unless the algorithm itself changes; unless the platforms Facebook and YouTube take responsibility for what they're promoting, there's just going to be more of this... **because it works.** It would have made them so much money because they've got so many views. So that tells other companies *we should try and deceive and do fake stuff because that's going to get us views.

Ann doesn't have a computer science or information system background, and yet she still easily observed the trend. We've reached the point where these platforms can't feign ignorance anymore; people will continue to upload lies as long as  recommendation engines make it financially lucrative to do so. We *all* know it.

<img src="https://rubenerd.com/files/2020/how-to-cook-that@1x.jpg" alt="Ann Reardon discussing fake food video outlets on How to Cook That" style="width:500px;" srcset="https://rubenerd.com/files/2020/how-to-cook-that@2x.jpg 2x" />

As to whether these fake food videos are harmful, Ann also had a point I didn't consider. In my younger and more cynical years&mdash;some of which coincide with the first few years of this blog, gulp!&mdash;I would have scoffed at the idea because it assumes people aren't applying critical thinking. I still think this is true to an extent; we need to educate people about *how* to think. But here's another angle to consider:

> I had so many comments in that previous video of kids who'd spent their pocket money buying ingredients to cook one of the "So Yummy" recipes and then failing again and again, and they thought that they couldn't cook and they'd stopped baking.

This is tragic. Extrapolate this out further, and how many other people are being lied into thinking they're not capable of something? Paint this issue however you want, but motivation and a sense of agency are powerful forces, especially to impressionable children.
