---
title: "The end of the AstraZeneca vaccine"
date: "2024-05-09T08:40:06+10:00"
year: "2024"
category: Thoughts
tag:
- covid-19
- health
location: Sydney
---
Melissa Davey [reported in The Guardian](https://www.theguardian.com/business/article/2024/may/08/astrazeneca-withdraws-covid-19-vaccine-worldwide-citing-surplus-of-newer-vaccines)\:

> In a statement, AstraZeneca said the decision was made because there is now a variety of newer vaccines available that have been adapted to target Covid-19 variants. This had led to a decline in demand for the AstraZeneca vaccine, which is no longer being manufactured or supplied.

AZ was [the first vaccine Clara and I took](https://rubenerd.com/getting-our-first-covid-jab/ "Getting our first Covid jab") during the height of the first COVID wave. It was a glimmer of hope during an uncertain time. And to all those older Australians who refused to take it because they were [waiting for Pfizer](https://www.abc.net.au/news/2021-09-03/pfizer-over-60s-covid-19-vaccination/100430324) like they were choosing a bottle of chardonnay, we didn't forget your selfishness.

Our heartfelt thanks to all the medical researchers who saved millions of lives, not least ours.
