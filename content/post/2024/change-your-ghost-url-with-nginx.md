---
title: "Change your Ghost blog URL with nginx"
date: "2024-04-23T08:34:25+10:00"
year: "2024"
category: Internet
tag:
- ghost
- guides
- nginx
location: Sydney
---
This always trips me up, so I'm putting it here for posterity. It assumes you're running Ghost, and proxying it through (free)nginx.

First, install and configure Ghost as though its running on localhost. I do this in its own [FreeBSD jail](https://docs.freebsd.org/en/books/handbook/jails/ "FreeBSD Handbook: Jails and Containers"), and with a ghost user, so node doesn't spray stuff everywhere.

Then, as per the [Ghost Developer docs](https://ghost.org/docs/faq/proxying-https-infinite-loops/), add the following to your nginx config. This assumes the default Ghost port:

    server {
        ## Your virtual host URL
        server_name example.com;
            
        ## Optional: Stop Ghost advertising this
        proxy_hide_header X-Powered-By;
            
        ## Proxy requests to Ghost
        location / {
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header X-Forwarded-Proto $scheme;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header Host $http_host;
                
            ## Your Jail URL and port
            proxy_pass http://127.0.0.1:2368;
        }
    }

Then change your URL and restart Ghost:

    $ ghost config url https://example.com/
    $ ghost restart

Ghost also has `ghost setup nginx` and `ghost setup ssl` for Let's Encrypt and web server config, but I prefer to do these manually on my existing proxy.

