---
title: "“I can tell you’re human”"
date: "2024-05-26T09:01:49+10:00"
year: "2024"
category: Internet
tag:
- writing
location: Sydney
---
I'm slowly, **slowly** making my way through email feedback! Thanks to all of you for your patience. There isn't much, but you know, introversion.

Back in February&mdash;sorry!&mdash;I got an email from Derek saying:

> I can tell that you're human and write your posts beacuse you have a sense of humor.

That's been rattling around in my head for months.

Generative AI tools can generate vast quantities of milquetoast copy that may or may not be accurate, timely, or broadly useful. But it's difficult for it to capture and present humour in all but the most banal and obvious ways.

I'm sure its only a matter of time before an LLM is trained specifically on the output of comics, comedians, and surrealist writers to create *Plaigarised Jokes as a Service*. But synthesising and weaving those into technical copy or other commentary is another matter.

Derek is right. If I go to your site and it has a few jokes, irrespective of how terrible they are, it's a decent signal that there's a human behind the keyboard. In my case, I'll bet OpenAI wouldn't want their fingerprints on my sillyness.
