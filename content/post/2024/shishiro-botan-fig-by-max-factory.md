---
title: "Shishiro Botan fig by Max Factory"
date: "2024-02-26T09:36:21+11:00"
year: "2024"
category: Anime
tag:
- anime-figs
- hololive
location: Sydney
---
I haven't watched much of [Hololive's apex lion](https://www.youtube.com/channel/UCUKD-uaobj9jiqB-VXt71mA "Botan's YouTube channel"), but her character design has been one of my absolute favourites since seeing Gen 5 debut. She's **SO COOL.** Her massive hoodie and mismatched leggings are the best things since unmatched socks, and even her shirt isn't symmetrical. Those subtly art deco loops on her boots are also *top shelf*.

<figure><p><img src="https://rubenerd.com/files/2024/botan-3d@1x.png" alt="Botan's offical 3D model pose" srcset="https://rubenerd.com/files/2024/botan-3d@2x.png 2x" style="width:371px; height:669px;" /></p></figure>

Max Factory used her 3D art as the basis for their latest 170 mm fig, and are now taking preorders. I think it's the best rendition of her I've ever seen.

<figure><p><img src="https://rubenerd.com/files/2024/botan-fig-1@1x.jpg" alt="Photos of her anime figure by Max Factory, showing her aforementioned fashion sense." srcset="https://rubenerd.com/files/2024/botan-fig-1@2x.jpg 2x" style="width:275px; height:400px;" /></p></figure>

Also is it just me, or are fig press photographers getting more creative with their settings as well? These photos are from the same set of images all the preorder retailers are using:

<figure><p><img src="https://rubenerd.com/files/2024/botan-fig-2@1x.jpg" alt="Photos of the same figure surrounded by some fancy bottles on a table." srcset="https://rubenerd.com/files/2024/botan-fig-2@2x.jpg 2x" style="width:275px; height:385px;" /></p></figure>

I was not paid to post this, but I'm tempted now myself. Damn it.
