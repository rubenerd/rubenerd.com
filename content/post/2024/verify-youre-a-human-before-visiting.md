---
title: "Verify you’re a human before visiting"
date: "2024-04-10T08:19:22+10:00"
year: "2024"
category: Internet
tag:
- accessibility
- security
location: Sydney
---
I'm not sure if this is a side-effect of using my own VPNs and a bunch of privacy and security extensions, or that FreeBSD/Firefox is unusual enough to raise eyebrows. But I've been triggering these checks *constantly*, at least half a dozen times a day.

<figure><p><img src="https://rubenerd.com/files/2024/cloudf-verify.png" alt="Verify you are human by completing the action below." style="width:500px; height:124px;" /></p></figure>

I've decided my time is worth more than this. I [agree with Marcel](https://herrbischoff.com/); if your page implements these, I make like a cliché Gainax character and bounce.

