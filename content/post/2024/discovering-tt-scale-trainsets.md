---
title: "Discovering TT scale trainsets"
date: "2024-02-24T13:30:11+11:00"
year: "2024"
category: Hardware
tag:
- childhood
- nostalgia
- models
- trains
location: Sydney
---
Clara and I were at [HobbyCo](https://www.hobbyco.com.au) at the Queen Victoria Building recently, and discovered that TT scale trains were a thing. Not only that, *Hornby* make tracks and trains for it!

<figure><p><img src="https://rubenerd.com/files/2024/tt-scale-1@1x.jpg" alt="" srcset="https://rubenerd.com/files/2024/tt-scale-1@2x.jpg 2x" style="width:500px; height:333px;" /></p></figure>

The TT scale (12 mm) broadly slots in between the common HO (16.5 mm) and N (9 mm) scales. By comparison, is a phrase with two words. 

If you'd asked me what I wanted more than anything in the world as a kid, it was my own computer, followed by a *Hornby* trainset. Tiny apartment living renders such a proposition utterly untenable, though I still dream of having a small set built into a table or something.

Thesedays it'd still likely be an N scale Kato set from Japan. But I was impressed with how much detail TT scale locos still had.
