---
title: "Duplicated words across a soft newline"
date: "2024-11-22T09:54:42+11:00"
abstract: "Our eyes are trained to detect patterns, so duplicated words are harder to spot."
year: "2024"
category: Thoughts
tag:
- language
- tricks
location: Sydney
---
Yesterday I blogged about [The Scunthorpe Problem](https://rubenerd.com/the-scunthorpe-problem/), and included this (emphasis added):

> [&hellip;] because they dared **to to** talk [&hellip;]

I didn't notice the duplicated word **to**, because the line happened to wrap between them in my text editor. It wasn't until I saw the post published that the words lined up with each other on the same line, and they stuck out like a sore thumb.

I loved puzzle books as a kid&mdash;I know, shocking&mdash;and there was an entire genre of them that would do little word layout tricks like that. They'd explain on the solutions pages that they were designed to subvert cognitive pattern recognition, because our eyes skim sentences rather than matching each and every word. I'm not sure how true that was, but it's sure caught me out more than a few times.

Now I have yet *another* thing to check before doing `git commit` on a post. I'll add this to my need to regularly spell cheque cheque, and fix broken lynx.

