---
title: "Favourite things in May 2024"
date: "2024-07-02T15:42:27+10:00"
year: "2024"
category: Thoughts
tag:
- favourite-things
- links
- lists
- retrocomputers
location: Sydney
---
Matthew over at TechTea is the latest to publish [montly lists](https://techtea.io/articles/2024/favorite-things-may/) of his favourite things. I love the idea, so I'm going to... borrow it.


### Projects

* [BitsUndBolts](https://www.pcbway.com/project/member/?bmbno=0140E301-54A5-47). This German YouTuber has a collection of shared projects for building your own memory modules and -5 V power rail mods. I'm not up to the level of soldering surface-level components yet, but when I am, these builds will prove hugely valuable.

* [Texelec jr-IDE Rev.B](https://texelec.com/product/jr-ide-for-the-pcjr-by-retrotronics/). This is the latest revision of Mike's awesome expansion card for the IBM PCjr that gives the machine an IDE interface, 1 MiB of static RAM, 512 KiB of flash ROM, and other goodies. This is really interesting for a reason one day I'll be able to discuss (cough).


### Writing

* [Brain Baking: The Fuck Shit Stack](https://brainbaking.com/post/2024/06/the-fuck-shit-stack-called-ai/). Wouter asks if Australians really say "genuine" and "at the end of the day" every odd sentence. I grew up overseas, so my Australian upbringing wasn't exactly genuine. But at the end of the day... yeah, it's true. His page is also a genuine summary of "AI".

* [James' Coffee Blog: The stories for me](https://jamesg.blog/2024/06/29/the-stories-for-me/). A beautiful, introspective view into the act and process of creating written works, from one of the most intelligent people on my web feed list.

* [Luke's Wild Website: It works](https://www.lkhrs.com/blog/2024/it-works/). Luke has been working on a form handler in golang, and I could feel the joy coming from his simple screenshot. A *lot* had to happen to make it that far, and I'm happy for him.

* [Michał's weblog: Yey EU](https://michal.sapka.me/blog/2024/eu-vs-tech-giants/). Michał is encouraged by the trials several large American IT firms are facing within the bloc for anticompetitive practices. I agree. The EU may not always do the right thing, but they're acting as a *sorely* needed check and balance on an otherwise unfettered industry that's used to acting with impunity (I say this as someone who works in said industry, albeit for an infinitely smaller player).

* [TechTea: Elitism, Identity, and the Open Source Community](https://techtea.io/articles/2024/elitism-identity-open-source/). Speaking of TechTea, this was a wonderful summary of my Linux adventure back in April, when I suggested that some people need to run Windows. He explains how communities need to understand that such behaviour is antithetical to their cause.


### Books and Papers

* [The Stanford Chinese Room Experiment (2004)](https://plato.stanford.edu/entries/chinese-room). This paper examines how "programming a digital computer may make it appear to understand language but could not produce real understanding". *Hmm!*

* <a href="https://en.wikipedia.org/wiki/The_Firm_(novel)">The Firm, John Grisham (1991)</a>. I took a break from a couple of the non-fiction books I'm reading about Scotland to dive back into one of my favourite authors. Nobody does legal thrillers like John, so when I saw this book on a neighbourhood mini library swap shelf, I realised I didn't have a choice. It's an absolute page turner; I keep saying *oh no, he didn't just!* under my breath, much to Clara's chagrin.


### Podcasts

* [Behind the Bastards: Bill Gates](https://omny.fm/shows/behind-the-bastards/part-one-the-ballad-of-bill-gates). Uncovering the veil of someone who's tried so desperately to reshape their public image, but they're still the convicted monopolist on the inside. Robert's writing is always so on point, and Andrew Ti is among my favourite of his regular guests.

* [Into Your Head 821: Deep Dive on Overthinking](https://onsug.com/archives/35852). Neil is one of my favourite podcasters of all time, and a fellow contributor to the Onsug. Lots of fun observations, but I especially loved the titular segment. I overthought how best to phrase that.


### Games

* Fishies for the Apple II (1984). More an ambient screensaver than a game, but I've had it running on my 1986 //e Platinum when I'm working from home, and damn it if it hasn't calmed my nerves during complicated calls. I got a disk online, but there are other... ways of acquiring it in 2024.


### Video

* [Call Me Kevin restored a caravan](https://www.youtube.com/watch?v=A6GtUMr2Yxw). Kevin is Clara's and my favourite gaming streamer, and we're so happy to see him break out of his comfort zone and try new things. He's infectiously optimistic, but also willing to share a bit of his vulnerability. He's genuine, curious, and hilarious.

* [Elizabeth Rose Bloodflame](https://www.youtube.com/@holoen_erbloodflame/streams). I've talked about this Hololive English Vtuber a few times here, but *wow*. Her incredible singing voice is matched only by her wit and warm personality. She only debuted last month, but she's already right up there with my other favourites.

* [FreeBSD Stories by Mike Karels](https://www.youtube.com/watch?v=XSziyKlG1ws). Mike passed on recently, which made this fascinating historical talk he presented last year all the more poignant. Mike was a soft spoken, deeply intelligent person who's contributions are in large part why I'm able to share this post with you today.
