---
title: "E-waste still not taken seriously in Australia"
date: "2024-01-08T16:07:04+11:00"
year: "2024"
category: Hardware
tag:
- environment
- e-waste
- recycling
location: Sydney
---
Last year Clara and I packed up a large box of e-waste, carted it on the train, and took it to the Community Recycling Centre (CRC) in the Sydney suburb of Artarmon. This is a facility run by the council to accept recyclable materials like paint cans, plastics, and electronics.

Based on the Google reviews for the facility, we copped it from the same one person on-site that many have experienced. Seeing us with our box, he shouted *CARS ONLY, IT'S ON THE BLOODY SIGN*. When he could tell I wasn't immediately turning back, he threw a few obscenities and trounced off (I can't see a way to link to the Google reviews directly, but it should be in the sidebar of the <a href="https://www.google.com/search?q=Community+Recycling+Centre+-+Northern+Sydney">search results</a>).

I get it, he doesn't make the rules. I spend a lot of time defending retail and service staff on this blog, based on how poorly I see them being treated. But this was the most unprofessional conduct I've ever experienced, and nothing defends such juvenile behavior. He's clearly in the wrong job, given his track record.

There's also the absurdity of the rule itself. I've been to plenty of facilities in other councils and countries where they've managed to make it safe and easy for people on foot to deliver e-waste. The fact this suburban lot near a train line couldn't figure it out is, frankly, embarrassing. And stop calling me Frank.

But the experience is illustrative of a wider issue. Australian councils talk big about the issues of e-waste and recycling, but the lackadaisical follow-through shows they're simply not serious.

For those without a car, our local council refers to annual or quarterly pickups which are insufficient, and an "alternative arrangement" involving a pickup service that charges $5 an item or grocery bag. This disincentive leads people to throw away e-waste irresponsibly. I would know! Half the crap in these bags right now are cables and dead computers I've picked up off the side of the road just in our neighbourhood in the last few months.

Councils have to make it easy and cost effective for people to do the right thing, or only suckers like you and I will bother.

The good news is that retail chains like Officeworks and The Good Guys will accept e-waste, though the volumes I pick up and hand over have raised eyebrows a couple of times. Better that than having it leeching crap into a landfill.
