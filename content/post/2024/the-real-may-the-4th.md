---
title: "Music Monday: The real May the 4th"
date: "2024-05-06T11:20:33+10:00"
year: "2024"
category: Media
tag:
- dave-brubeck
- jazz
- music
- music-monday
location: Sydney
---
With thanks to [Boots Chantilly](https://mstdn.social/@BootsChantilly/112384742246916404)! It even works with ISO-8601.

<figure><p><img src="https://rubenerd.com/files/2024/may-the-forth@1x.jpg" alt="Sorry Nerds, 5/4 is Dave Brubeck Day" srcset="https://rubenerd.com/files/2024/may-the-forth@2x.jpg 2x" style="width:360px; height:360px;" /></p></figure>

For those who need a refresher, Dave Brubeck's most famous work was *[Take Five](https://www.youtube.com/watch?v=tT9Eh8wNMkw)*, performed in the unusual 5/4 time signature. You might not know the name, but I'd wager dollars to doughnuts you recognise the tune. Or bagels, if you prefer savoury pastries with large circular inclusions.

My beloved singer/songwriter Michael Franks also [did a tribute song](https://www.youtube.com/watch?v=dJ8yj-Z7Z5A "Hearing Take Five") in the same vein. It's excellent, and I'd say one of my favourites.

Hey, we got a *[Music Monday](https://rubenerd.com/tag/music-monday/)* out of this! And I was only a couple of days late.
