---
title: "We b-bought a home"
date: "2024-09-16T08:44:45+10:00"
year: "2024"
category: Thoughts
tag:
- finances
- personal
location: Sydney
---
More than a few of you have noticed a drop in the quantity and quality of posts here over the last couple of months (not to mention some performance issues for a bit there). In short, Clara and I have spent our idle mental CPU cycles going through the motions of buying a home. It has consumed every waking moment, and even seeped into our dreams.

But it's now official: **we've bought an apartment!** YAY! 🎉

Well okay, we've hired a conveyancer, done due diligence on the building, entered into a contract with the vendor, paid the deposit, received unconditional approval from the bank, set up our offset account, and so many other steps I've lost count. There's still a lot of paperwork to sort out before the settlement date in a few weeks, but fingers crossed it's all admin work now.

    SELECT count(*) FROM apartment.purchase_steps WHERE completed = false;
    ==> 1,280

AAAH!

This has been [more than a decade](https://bsd.network/@rubenerd/112749225620130327) in the making, so it still doesn't seem real. It's been stressful and exhausting, but we also feel extraordinarily lucky. Both the agent and our conveyancer have been great to work with, and I've been lucky to have a colleague and a few friends who've been around the block a few times help us through the process.

Anyway, I thought I owed you all an explanation. Soon we shall return to our regularly-scheduled programming of insightful, witty, intelligent commentary, or whatever it was I did here. *Can I play the piano when my hand is healed? That's great, because I couldn't before!*

Cheers.
