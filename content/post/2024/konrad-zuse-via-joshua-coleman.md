---
title: "Konrad Zuse via Joshua Coleman"
date: "2024-09-30T08:50:36+10:00"
year: "2024"
category: Thoughts
tag:
- ai
- philosophy
location: Sydney
---
[Joshua Coleman](https://joshuacolemanmakes.wordpress.com/) (<a href="https://joshuacolemanmakes.wordpress.com/feed/">web feed</a>) has so many cool retro hardware projects. But this quote was the highlight for me today:

> The danger of computers becoming like humans is not as great as the danger of humans becoming like computers.
