---
title: "2FA codes without password validation"
date: "2024-05-12T09:43:04+10:00"
year: "2024"
category: Thoughts
tag:
- passphrases
- security
location: Sydney
---
This is an interesting security feature, intentional or otherwise!

A financial account I hold has a phone app that&mdash;among other things&mdash;generates login codes for the desktop web portal. You log into the mobile app, generate the one-time code, then use this to log into the desktop. I was always a bit miffed that I couldn't just add it to my regular 2FA app, but I'll take it over insecure SMSs.

Last week I was confused when *every* login code I was generating didn't work. Was it a time syncronisation issue? Did the app need updating? Was the website hiccuping in some other way? I feared being locked out of the account (it's happened before), so I left it for a week. This morning, I could log in without issues.

After doing some basic experiments, I think I've figured out what happened.

This mobile app doesn't validate your password. It dutifully "logs you in" to the page where you can retrieve a fresh 2FA code. But this code is only valid with the correct mobile password. In other words, type the wrong mobile password, and the generated 2FA token is invalid for the web portal, and is rejected.

This indicates to me that password isn't used for auth itself, but is a salt for the 2FA code. Huh!

This comes with the side effect that the mobile app can't validate whether your "login" was successful, because it doesn't have a record of it or its hash. If it's merely being used as input to generate the 2FA code, technically anything is valid, as I experienced earlier in the week.

Having a login silently fail like this can be a frustrating user experience when you're a goose like me and put the wrong one in. But I could see under limited circumstances where this could prove useful.

Say someone has your phone and has managed to unlock it. They run through the list of passwords they bought from the black hat data broker, and are able to "log in" on first go. They type the 2FA codes into the web portal, and after a few attempts are locked out of the account. From my perspective, this is a good outcome.

I had a chat with a few friends and colleagues, and there are some enterprise VPN systems that generate 2FA codes a similar way. This was the first time I experienced it.
