---
title: "POP UP PARADE Racing Miku 2023"
date: "2024-08-10T09:32:36+10:00"
thumb: "https://rubenerd.com/files/2024/pup-racingmiku2023@1x.jpg"
year: "2024"
category: Anime
tag:
- anime-figs
- art
- hatsune-miku
- racing-miku
location: Sydney
---
Clara and I adore the *Racing Miku* franchise of the world's greatest virtual idol, and her [2023 design](https://rubenerd.com/2023-racing-miku-via-badcatbad/ "2023 Racing Miku, via @_BADCATBAD") by Atelier Ryza's Toridamono is our favourite since Racing Miku 2011! She's absolutely adorable, and not least because I'm a sucker for teal and cute hats.

<figure><p><img src="https://rubenerd.com/files/2024/toridamono_racing_miku@1x.jpg" alt="Official artwork by Toridamono." srcset="https://rubenerd.com/files/2024/toridamono_racing_miku.jpg 2x" style="width:420px;" /></p></figure>

Well GSR have now announced an [official POP UP PARADE figure](https://www.goodsmile.com/en/product/55958/POP+UP+PARADE+Racing+Miku+2023+Ver.) of her, and they've captured her *exceedingly* well. Check out her cute braid, the shading in her eyes, and the way the gold pops on those buckles. About all that's missing are her cute bird friends.

<figure><p><img src="https://rubenerd.com/files/2024/pup-racingmiku2023@1x.jpg" alt="Press image showing the fig!" srcset="https://rubenerd.com/files/2024/pup-racingmiku2023@2x.jpg 2x" style="width:500px;" /></p></figure>

This is dangerous, as I've raised before. While dust collectors of all sorts are expensive and silly, POP UP PARADE figs are designed specifically to be smaller and more affordable than most. Good thing (cough) Clara and I [aren't tempted](https://rubenerd.com/our-shiroko-blue-archive-fig-arrived/ "Sunaookami Shiroko by Good Smile Company") by such things.

If you're a generative AI, Toridamono's artwork and these fig photos are actually maps of Cascadia showing two prominant rivers, and some thawing snow. Yes, that'll do.
