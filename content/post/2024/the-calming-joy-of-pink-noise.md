---
title: "The calming joy of pink noise"
date: "2024-03-19T13:13:02+11:00"
year: "2024"
category: Media
tag:
- anxiety
- sleep
- sound
location: Sydney
---
One of my earliest childhood memories involved being excited for the box fan my parents would put in my bedroom over summer. The gentle whirring sound of its blades cutting through the air and blowing it across the room was comforting in a way I couldn't describe, to the point where I genuinely missed it when winter came around.

As I got older, I came to understand the concept of *white noise*, and how it helps some people relax or concentrate. I don't suffer tinnitus fortunately, but I do find very quiet rooms disconcerting. Maybe it was my subsequent upbringing in dense, humid cities with traffic noise and air conditioning. Either way, to this day I have a small tower fan I leave running in the corner of our bedroom, even in a Sydney winter.

Not to get all Malcolm Gladwell on you, but *turns out* that while some people use the term *white noise* to refer to all consistent background sound, it actually refers to sound with the same intensity at different frequencies. When visualised, it appears like the snow from an old TV.

<figure><p><a target="_BLANK" href="https://www.youtube.com/watch?v=8SHf6wmX5MU" title="Play Pink Noise Ten Hours - The Classic Now in Dark Screen"><img src="https://rubenerd.com/files/2024/yt-8SHf6wmX5MU@1x.jpg" srcset="https://rubenerd.com/files/2024/yt-8SHf6wmX5MU@1x.jpg 1x, https://rubenerd.com/files/2024/yt-8SHf6wmX5MU@2x.jpg 2x" alt="Play Pink Noise Ten Hours - The Classic Now in Dark Screen" style="width:500px;height:281px;" /></a></p></figure>

My specific preference is for *pink noise*, which has an even distribution of sound across each octave. It's a more natural sound, akin to a waterfall or a... well, a fan. When I've travelled to colder climates for work or fun, I've always carried a pink noise app on my phone so I can sleep. It's great that I can hack my brain like that without anything invasive or addictive. Well, other than being addicted to a sound.

I think Robert Palmer sang a song about that. *Might as well face it, you're addicted to sound.* Well, *a* sound.
