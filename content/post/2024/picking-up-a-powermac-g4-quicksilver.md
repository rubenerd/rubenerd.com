---
title: "Picking up a Power Mac G4 QuickSilver"
date: "2024-09-02T07:47:15+10:00"
thumb: "https://rubenerd.com/files/2024/pmg4qs@1x.jpg"
year: "2024"
category: Hardware
tag:
- apple
- design
- netbsd
- power-mac-g4
- retrocomputers
location: Sydney
---
About a week ago I noticed someone selling a working Power Mac G4 in Sydney, for local pickup only. We chatted about its condition and how best to get it from his house, and he agreed to drive to the nearest station for me. He was so nice, and it was in such great condition, I hit the *Buy Now* button (cough).

It turned out to be a really pleasant way to spend a Sunday. The train to his suburb took about an hour, during which time I sketched out some plans for where I'd put it, how I'd cable it into the KVM, and to reacquaint myself with the history of the machine.

<figure><p><img src="https://rubenerd.com/files/2024/pmg4qs@1x.jpg" alt="Photo of the QuickSilver on our outdoor table, where the lighting is a bit more forgiving." srcset="https://rubenerd.com/files/2024/pmg4qs@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

The *QuickSilver* was the second of the three generations of G4 towers Apple released, between 2001 and 2002. I fell in love from the moment I saw one at the EpiCentre in Wheelock Place in Singapore. I spent hours playing with their display units, and spreadsheeting out whether I could ever justify or afford one. Eventually I got a Power Mac G5 to replace my aging DIY Athlon XP machine, but it wasn't the same.

The earlier <a href="https://en.wikipedia.org/wiki/Power_Mac_G4#1st_generation">Graphite</a> and later <a href="https://en.wikipedia.org/wiki/Power_Mac_G4#3rd_generation:_Mirrored_Drive_Doors/FireWire_800">Mirrored Drive Door</a> models have their charms, but I think the QuickSilver is the most beautiful desktop Apple has ever made; in both form and function. The front bezel is understated and clean, and the rear didn't need that array of awkward ventilation holes of the more powerful MDD unit. You can also see where the legendary [G4 Cube](https://en.wikipedia.org/wiki/Power_Mac_G4_Cube) got many of its design elements.

My machine is the entry-level 733 MHz G4 model, with 128 MiB of memory and a 40 GB hard drive. The GPU is a blazing GeForce2 MX with 32 MiB of video memory, attached via AGP alongside five full-length, 64-bit PCI slots. The CPU is mounted to a daughterboard, so Apple could also release dual-CPU versions. It has a DVD-R/CD-RW SuperDrive which flips down a tidy door on the front of the case, and has a bracket for an optional Zip drive.

<figure><p><img src="https://rubenerd.com/files/2024/pmg4qs-door@1x.jpg" alt="Photo of the QuickSilver showing its open door and accessible logic board." srcset="https://rubenerd.com/files/2024/pmg4qs-door@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

I previously owned a Blue and White Power Macintosh G3 which used the same logic-board-on-door design, and I remember them being such a joy to work in. This era of Apple wanted you to tinker, upgrade, and grow into your computer. It's why I love using the [Apple //e Platinum](http://retro.rubenerd.com/iie.htm) as well.

My plan is to use this machine as the ultimate Mac OS 9 box. It was the last computer line Apple sold that supported their original Macintosh OS, so it seems fitting. I'm also going to try booting NetBSD on it, in part because the PowerPC port was my first experience with BSD back in the day.

Upgrades and expansion aren't currently on the cards, on account of *buying and moving house* again. But I'll keep an eye out. In the meantime, I'm fleshing out details on the [Retro Corner](http://retro.rubenerd.com/powermac.htm).
