---
title: "86Box 4.2 supports the wonderful ESS AudioDrive!"
date: "2024-07-31T08:08:17+10:00"
thumb: "https://rubenerd.com/files/2024/86box-audiodrive.png"
year: "2024"
category: Hardware
tag:
- 86box
- ess-audiodrive
- retrocomputing
- sound-cards
- virtualisation
location: Sydney
---
The *Hitchhikers* release of the [86Box PC System Emulator](https://86box.net) is out now, and it has cool new features and bug fixes. Some of my personal highlights from the blog post:

* The inclusion of a Hayes-compliant serial modem!

* Several fixes to the virtual Matrox card, including a bug that would crash Windows 3.x when changing resolutions.

* Fixed on-board device detection issues on the Commodore SL386SX-25 board.

* Fixed hard reset crash on SiS Socket 7 machines.

* New Vietnamese translation (I know I have some readers up there).

But among a host of new sound cards, my favourite additions are the ESS AudioDrive ES688 and ES1688 sound cards for the ISA bus! Why am I so excited? Oh boy, let me tell you!

<figure><p><img src="https://rubenerd.com/files/2024/86box-audiodrive.png" alt="Screenshot showing configuration for the ESS AudioDrive" style="width:640px; height:375px;" /></p></figure>

My first sound card was a Sound Blaster 16 Value which came with a Creative CD-ROM, which was followed up with a Sound Blaster 32 I got one Xmas when I was a kid. They were great, capable cards that all the games and software we ran supported.

This streak ended when I built my [first computer](http://retro.rubenerd.com/mmx.htm) in the late 1990s. The gentleman at the store in Funan Centre in Singapore recommended I go with an [ISA ESS AudioDrive ES1868](https://rubenerd.com/the-ess-audiodrive-es1868-for-sound-and-ide/) for my build, becuase it was an affordable option that would let me allocate more funds to my CPU from a Pentium 1 to an MMX. I took his advice, and it ended it being more consequential that I would have expected.

The ES1868 was an excellent sound card, especially for Windows. All the software I ran supported it, thanks to its Sound Blaster compatibility. It was also the least fiddly card I've ever configured in DOS. But I was immediately shocked at how *quiet* it was. You know how modern phones emulate background hiss from analogue equipment, to reassure us that the line isn't dead? I originally thought this AudioDrive card was DOA, because plugging in our speakers didn't result in any of the Sound Blaster's low-level hiss. But then I installed the drivers, and it sounded... clear, crisp, and wonderful. Without hiss!

Cool (and rich!) people had a Gravis UltraSound card in the 1990s, but I was an ESS convert. So much so that when I wanted to add a CD-ROM to my Am386 tower last year, I reached for the ESS first to [use its IDE interface](https://rubenerd.com/the-ess-audiodrive-es1868-for-sound-and-ide/) for the first time. Which 86Box can do now too!

I'm *stoked* I can now use an ESS card in my emulated PCs.

Don't forget to [support 86Box](https://www.patreon.com/86box) if you get as much joy and use out of it as I did; I've been a Patreon for a while, and it's always fun getting the updates. Also [Esther Golton makes wonderful music](https://esthergolton.bandcamp.com/album/stay-warm), not just for testing Winamp on Windows 95!

<figure><p><img src="https://rubenerd.com/files/2024/esther-ess-audiodrive@1x.png" alt="Screenshot showing the emulated ESS card, with Esther Golton's music playing in Winamp" srcset="https://rubenerd.com/files/2024/esther-ess-audiodrive@2x.png 2x" style="width:640px; height:480px;" /></p></figure>
