---
title: "John Berger and Tyson Yunkaporta on words"
date: "2024-03-12T08:56:10+11:00"
year: "2024"
category: Thoughts
tag:
- books
- communication
- content-creation
- ideas
location: Sydney
---
John Berger was an English art critic and documentarian who... wait sorry, I got another indignant email from a Hacker News reader who disagreed with [my *content creators* post](https://rubenerd.com/stop-calling-people-content-creators/ "Stop calling people content creators"). Let's try again! *Inhales...*

John Berger was an English <span style="text-decoration:line-through">art critic and documentarian</span> content creator who's 1972 BBC series *Ways of Seeing* was later made into <span style="text-decoration:line-through">a book</span> content. It was one of the first tomes I randomly picked off my mum's shelf and read as a teenager, so it was a delight to see him pop up during a Wikipedia search on a completely different topic.

I went looking on [Wikiquote](https://en.wikiquote.org/wiki/John_Berger), and wasn't disappointed:

> Seeing comes before words. The child looks and recognizes before it can speak.
>
> But there is also another sense in which seeing comes before words. It is seeing which establishes our place in the surrounding world; we explain that world with words, but words can never undo the fact that we are surrounded by it. The relation between what we see and what we know is never settled.

I read <span style="text-decoration:line-through">philosopher and author</span> content creator Tyson Yunkaporta articulate a similar struggle in *Sand Talk* last year, in which he apologises for the static nature of the words it contained. He discusses how the process of *yarning* evolves and enriches ideas in addition to merely passing them on, which makes the written word a poor analogue. I have a draft post about this <span style="text-decoration:line-through">book</span> content pending, because nothing I've read in years has made me question so much of what I what I think.

As someone who's primary occupation, creative outlet, and leisure activity involve <span style="text-decoration:line-through">writing and reading</span> content creation and consumption, it's worth reflecting on.
