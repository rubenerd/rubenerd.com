---
title: "AMD’s new 65 watt CPUs are a huge achievement"
date: "2024-08-09T13:36:29+10:00"
year: "2024"
category: Hardware
tag:
- amd
- cpus
- mini-itx
location: Sydney
---
The tech press are largely ambivilent towards AMD's new Ryzen 9600X and 9700X CPUs, but Wendell from Level1Techs [added some context 📺](https://www.youtube.com/watch?v=JZuV35LgjxU)\:

> And then I come back to the power, and AMD is doing this in their **65 watt TDP**, 88 watt board power envelope. That's lower power than the 13700T which is a "35 watt" part that can be 106 watts most of the time.
> 
> And in eco mode, it doesn't make a whole lot of difference for gaming-type workloads. It's really the multicore workloads and some of the other stuff.
> 
> That AMD has done this... in the power envelope that they've done it, is **shocking**.

I agree! They've managed to bring the TDP back down to what my two-generation old 5700X has, and with a significant performance uplift. They use less power and run cooler than 7000 series under equivilent workloads, which is welcome news for Mini-ITX builds, and those who want quieter setups.

It's worth pointing out too that if these improvements make their way to their workstation and server silicon, sysadmins and data centre beancounters will *really* take notice. I know that at work power has been a constraint over space for *years*. AMD already have Intel beat in density, but Zen 5 looks to push that advantage even further.

I'm still happy with the performance of my current platform, but I'm looking forward to my next upgrade again. I'm hopefully consolidating my two towers into one [with this beast](https://rubenerd.com/the-new-silverstone-cs382-homelab-case/ "The new SilverStone CS382 homelab case"), which means I'll be expanding from a Mini-ITX to MicroATX. A nice tower cooler on a 65 watt part would be wonderful.

Good on you, AMD! Some of us appreciate what you've done here.
