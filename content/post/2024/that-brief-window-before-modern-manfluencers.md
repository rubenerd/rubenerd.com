---
title: "The last brief window before modern “manfluencers”"
date: "2024-10-25T09:25:47+11:00"
year: "2024"
category: Thoughts
tag:
- philosophy
- school
location: Sydney
---
Did you know masculinity is under threat? Worse than that though, it may be in irriversable and terminal decline, from which mankind will never recover!? Scary! We hear this every couple of decades or so, along with the requisite pearl clutching and moral panic over what it represents, what society should do, and why young men in particular are letting the world down every time they refuse to whistle at a woman on the street. 

Wait, maybe I shouldn't say *pearl clutching*, only women wear those. Um... modded light truck steering wheel clutching? With their *hands!* Phew, nailed it.

The most recent episode of *[Behind the Bastards](https://www.iheart.com/podcast/105-behind-the-bastards-29236323/)* took a lighthearted look at the modern “manfluencer”, who's here to tell you that men need help, and they conveniently have the solution! Which seems to involve a lot of stereotypes, lobster sociology, and counterproductive behaviour. Oh yeah, and buy their overpriced protein powder that w-will also block 6G, or whatever it is we're up to now.

This is an issue far larger in scope and importance than a short blog post, but there were a few lines that host Robert Evans opened with that had my blue and pink scarf all a quiver:

> I graduated high school in 2006. I think I was in a sweet spot that only lasted for a couple of years. Really briefly, for the last two years of high school, things were kinda healthy for young men, comparatively. There was a switchover that happened between junior and senior high for me. When I was in middle school or junior high, I'd get bullied a bunch for being "the kid" who had D&D books, and played Warhammer and shit. That... didn't go well for me! Then when I was in my junior year, World of Warcraft came out, and suddenly all of that stopped. And it was about the same time people started getting less shitty, and then a *lot* less shitty towards queer kids in school.
>
> There was this brief period where [&hellip;] all the weird kids stopped getting that shit. It didn't last very long.

I graduated high school around a similar time, and this *absolutely* mirrored my experience. Despite being on the other side of the planet and going to an international school in Southeast Asia, there was a noticeable shift in attitudes among the cohort between my mid and late teens.

My friends and I were bulled mercilessly for our interests in *Star Trek*, computers, Japan, and trading card games at the time. I was picked up by the neck, had my locker filled with shaving cream, and was the target of endless practical jokes by the cool girls... all the usual fun stuff! A few of us didn't come out till many, many years later, but even at the time it was clear the way we conducted ourselves wasn't quite what young boys were supposed to do. *And wow were we reminded of that!*

Then something changed around year eleven. At the time, I attributed this to teenagers growing up and being a bit more mature, and computer nerds beginning to become well-publicised billionaires. But overnight we were treated differently. I wouldn't say we were treated *well*, but there was a common understanding. We were all a bit different, but we should treat each other the same. Not doing that made you a dick, and being a dick around people wasn't a good look.

My Australian Zoomer friends now say modern schools are generally very accepting of people with divergent identities and personalities, which is heartening. But it's also hard not to see the rise of people idolising these hyper-pseudo-masculine dudebros who treat others like shit. It's incredible to me that so many vulnerable, lonely men continue to be played like fiddles by these grifters. History has taught us just how lucrative&mdash;and dangerous&mdash;that is.

A bit of a heavier topic today, but I've had messages from some of you that talking publicly about my experiences like this are helpful. There are more of us out there than you realise.
