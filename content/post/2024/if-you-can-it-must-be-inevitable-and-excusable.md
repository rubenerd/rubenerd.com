---
title: "If you can, it must be inevitable and excusable"
date: "2024-03-06T15:14:39+11:00"
year: "2024"
category: Ethics
tag:
- business
- law
location: Sydney
---
[jwz on Mastodon](https://mastodon.social/@jwz/111988020735458002)\:

> This is essentially the argument that there's nothing wrong with doxxing someone if you found their home address through public records, and furthermore there would be nothing wrong with building a tool to automate that. 
> 
> It's the nerd disease of: just because it's possible or even legal doesn't make it right.

This. It's sad how common this [train of thought](https://rubenerd.com/legal-versus-ethical/ "Legality and ethics") is, and how many people line up to defend it.

I haven't thought this though enough for a dedicated post, but I think it's also part of a broader shift to brand dangerous, counterproductive, or predatory technology as *inevitable*, as though the people involved were merely along for the ride. While luck will always play a part, the truth is there were also deliberate, calcuated decisions at every step of the process.

Isn't it also funny that these *inevitable* outcomes always results in someone who wields disproportionate power making *lots* more money? I'd be more inclined to believe the *inevitability* of a tech if it helped the downtrodden sometimes.
