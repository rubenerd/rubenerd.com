---
title: "Vandragon_de’s 3D printable ocean liners"
date: "2024-02-13T14:23:52+11:00"
thumb: "https://rubenerd.com/files/2024/DSC_1402@1x.jpg"
year: "2024"
category: Thoughts
tag:
- 3d-printing
- models
- ocean-liners
location: Sydney
---
[Speaking of models](https://rubenerd.com/alear-from-fire-emblem-engage-getting-a-fig/ "Alear from Fire Emblem Engage getting a fig!") I only just came across this artist's 3D printable model ship collection, and they look incredible.

<figure><p><img src="https://rubenerd.com/files/2024/DSC_1402@1x.jpg" alt="Photo from Vandragon_de's Patreon page, showing profiles of all the ships." srcset="https://rubenerd.com/files/2024/DSC_1402@2x.jpg 2x" style="width:500px; height:125px;" /></p></figure>

If you [subscribe to their Patreon](https://www.patreon.com/vandragon_de "Vandragon_de's Patreon profile"), they give you access to their detailed models you can 3D print yourself, or give to someone to do properly. They even has designs optimised for resin, my favourite material after seeing how well it worked on a recent 8-bit Apple component.

I harboured&mdash;**HAH!**&mdash;an Olympic&mdash;**HAH!**&mdash;obsession with ocean liners as a kid. I can't remember if it started with James Cameron's movie or my interest in Art Deco design, but I amassed a huge collection of books about them. [Visiting the RMS *Queen Mary*](https://rubenerd.com/visiting-the-rms-queen-mary/ "Visiting the RMS Queen Mary") in Longbeach with Clara was a childhood dream come true.

Unfortunately, my attempts at building model Airfix ships have thus far not gone well, despite my best efforts. Maybe something a small, 3D-printed model I can paint would be the answer!

The only question is: which one? I always thought the *Lusitania* was the most beautiful and gracious, though the *Olympic* always had those clean lines.
