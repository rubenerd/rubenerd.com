---
title: "My own American insurance story"
date: "2024-12-12T08:47:18+11:00"
abstract: "We were lucky we got a decade more time with our mum. Others aren’t as lucky; or worse, have been duped to thinking they can’t be."
year: "2024"
category: Ethics
tag:
- business
- family
location: Sydney
---
It's hard not to read the news about that insurance executive in the US. His passing has brought a whirlpool of resentment, frustration, and anger to the surface, from people across all walks of life. It might be wishful thinking to assume this will bring about change by itself, especially with the incoming administration over there. But at least the conversation is being had.

You shouldn't need personal experience to relate to the tragic stories of denied care, death, and suffering. But I do relate, because we had such fun.

My mum Debra had cancer for most of my sister's and my childhood, as was a recurring topic here when she was alive in the 2000s. We were living overseas at the time, which also complicated matters beyond the already difficult and painful emotions one feels being with loved ones who are suffering. My "favourite" moment was being told by the Australian High Commission in Singapore that they didn't know how to repatriate a citizen's body, and didn't care to help. Thank heavens for Singapore Airlines who *did* care, and the New Zealand High Commission of all places offering advice. But I digress.

My dad's business had a company insurance plan, one of which ended up being with an insurer owned by an American conglomerate. They were, in a word, miserable to deal with. They delayed payments, denied treatments her qualified oncologist was demanding she have, and added untold layers of additional stress. I took a fateful call from one of their advisors when I was 15, and remember hanging up the phone shaking from a combination of fear, frustration, and blinding rage I hadn't felt before or since. My face was as red as soup, as my sister said.

We were lucky that my dad was paid well, so we were able to make the ridiculous gap payments, the shortfalls when they refused coverage for something, and cover the delayed payments when critical and timely procedures were needed. It meant my sister and I got to spend another decade with our mum, before she finally passed away from complications of her last chemo round in 2007. She never got to meet Clara, or my brother-in-law, but at least she saw us grow up. This makes me cry everytime I think about it, including as I write this sentence.

There are people in the world who didn't get that additional time, because they're uninsured, or have to make agonising decisions when their insurance company prioritises profit margins over human life and dignity. When we place the care of humanity in the hands of such businesses, it shouldn't come as a surprise. Yet large swaths of humanity have been duped into thinking this is acceptable, or even inevitable.

Today, my old man has little in the way of retirement savings for his years of stressful work, and had to buy a modest dwelling up the coast away from friends and family. My sister and I had to contribute large parts of our paycheques for years to help cover the costs, and I could only afford to pay the deposit for a small apartment with my partner when I reached my late thirties. This struggle could have all been avoided; and most people aren't even this lucky.

These American insurance companies are humanity at it's cynical, unfettered worst, and they deserve all the scorn and ridicule being directed at them.

As for the moralisers saying those revelling in that executive's passing are somehow unethical or callous, I'd counter by saying I'd want to leave a legacy for which people miss me. *Yeah, Debra was so talented, wasn't she? Her wit was dangerous! She always made us feel loved.*
