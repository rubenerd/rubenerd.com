---
title: "Google’s new login notice"
date: "2024-03-04T20:41:51+11:00"
year: "2024"
category: Internet
tag:
- authentication
- design
- security
location: Sydney
---
Is there any more terrifying a prospect?! Don't answer that.

<figure><p><img src="https://rubenerd.com/files/2024/google-login.png" alt="A new look is coming soon. Google is improving its sign-in page with a more modern look and feel" style="width:500px; height:130px;" /></p></figure>

