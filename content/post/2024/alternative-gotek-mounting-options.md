---
title: "Alternative GoTek drive mounting options"
date: "2024-03-09T10:17:51+11:00"
year: "2024"
category: Hardware
tag:
- disk-drives
- gotek
- retrocomputing
- storage
location: Sydney
---
The GoTek (often mistyped as *Gotek*) is a virtual floppy drive device that can emulate hundreds of virtual disks on a single USB key. Like my [FloppyEmu](https://rubenerd.com/ordering-my-first-3d-printed-part/ "My first 3D printed part!"), [SD2IEC](https://www.gamedude.com.au/products/shop/sd2iec-cableless-commodore-64-floppy-drive-emulator-requires-sdcard "The cable-less unit I use"), and [Pi1541](https://rubenerd.com/quick-review-of-the-pi1541-from-shareware-plus/ "Quick review of the Pi1541 from Shareware Plus"), it has made working on older DOS-era machines much easier and more fun. I tend to prefer running period-correct hardware where I can, but storage is one area where I'm happy to cheat.

GoTeks can be run stock, or can be upgraded with the open-source [FlashFloppy](https://github.com/keirf/flashfloppy) firmware, potentiometers in lieu of the back/forward buttons, and their 8-segment displays replaced with cool little OLED screens to display which disk is in use. It's impressive how far people have pushed the original hardware.

But the form factor itself hasn't changed much, save for taller bezels to accommodate the Amiga. It makes sense that using the 3.5-inch form factor makes them drop-in replacements for aging or dead disk drives, but *technically* a printed circuit board and USB port could be mounted in any number of ways. This could offer another path to adding a GoTek to a case that may otherwise not have a free 3.5-inch bay free.

A *free* 3.5-inch drive bay *free!* The Department of Redundancy Department would like a word. If you're free.

As always, 3D printing to the rescue! [SomeDude03 has a PCI slot mount](https://www.thingiverse.com/thing:5207732 "Gotek Floppy Emulator PCI Mount") which might be useful if you have a spare slot but not a drive bay. I could see how this might make changing disks more difficul, but that might be less of an issue depending on your use case and desk layout:

<figure><p><img src="https://rubenerd.com/files/2024/gotek-pci.jpg" alt="A GoTek mounted in a PCI cover." style="width:500px;" /></p></figure>

In a similar vein, is a phrase with three words. Redundancy department.

[Tom_DD has this cool 5.25-bay bracket](https://www.thingiverse.com/thing:4237727) that fits the GoTek under a 3.5-inch drive within a 5.25-inch drive bay. I use something similar on Clara's and my FreeBSD homelab box to mount an optical drive above a 3.5-inch drive. This would work really well in my [486 case](http://www.sasara.moe/retro/#nec-apex) which only has a single 3.5-inch bay.

<figure><p><img src="https://rubenerd.com/files/2024/gotek-525.jpg" alt="A GoTek and 3.5-inch floppy drive in a 5.35-inch bezel" style="width:500px;" /></p></figure>

And finally, we get to something *from my dreams*. While sleeping a few nights ago, I imagined a mythical 3D-printed external case for a 3.5-inch drive that integrated power and data across a single cable that connected into the desktop's floppy controller. I also designed a GoTek that was built into a toaster that launched its SD cards into the air with such force that I destroyed the ceiling, and had to negotiate with my landlord about repairs... who was Harold Holt for some reason.

Let's no read too much into why I dream about floppy drives, but such *Backpack* external drives existed with Centronics interfaces. I wonder if those would work with a GoTek? Or if anyone has built an emulator for *those* boards?

Let me know if you know any other exotic or interesting ideas for these drives.

