---
title: "Give people homes"
date: "2024-06-12T08:30:39+10:00"
year: "2024"
category: Ethics
tag:
- economics
location: Sydney
---
[The Colorado Sun](https://coloradosun.com/2024/06/05/housing-homelessness-mental-health/)\:

> Jefferson Center and WellPower of Denver are building permanent supportive housing complexes modeled after a 2017 Denver building that still has a waitlist.
> 
> Both projects are run by community mental health centers and both invite people who are living outside — the ones burning through taxpayer dollars as they cycle in and out of jail, detox and hospital emergency rooms — to move directly into their own apartments.
>
> Kuenzler often tells skeptics that people without housing are going to live in their neighborhoods anyway, so “whether they are in a tent or whether they are in an apartment with wraparound services really makes a difference.”

Building homes for the homeless seems like such an *obvious* thing to do that merely mentioning it feels ridiculous (then again, I'm on record here asking why so many [dehumanise our fellow travellers](https://rubenerd.com/homeless-people-in-media/)).

It reminds me of the [efforts of the Finns](https://oecdecoscope.blog/2021/12/13/finlands-zero-homeless-strategy-lessons-from-a-success-story/)\:

> Finland’s success is not a matter of luck or the outcome of “quick fixes.” Rather, it is the result of a sustained, well-resourced national strategy, driven by a “Housing First” approach, which provides people experiencing homelessness with immediate, independent, permanent housing, rather than temporary accommodation (OECD, 2020).
>
> The Finnish experience demonstrates the effectiveness of tackling homelessness through a combination of financial assistance, integrated and targeted support services and more supply.

Clara and I are going through the process of buying a place of our own, and I'd still want all this available to people who need it. It's the ethically and financially responsible thing to do.
