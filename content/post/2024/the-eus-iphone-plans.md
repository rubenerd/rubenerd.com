---
title: "The EU’s iPhone plans"
date: "2024-01-27T15:04:10+11:00"
year: "2024"
category: Ethics
tag:
- europe
- law
location: Sydney
---
The European Union is fascinating case study in law and geopolitics, which I may write in more detail about at some point. It also increasingly represents a check and balance on an industry used to getting its way without repercussions, for which some of us outside Europe are thankful.

Those businesses, and their investors, aren't too happy. Take [Steven Sinofsky](https://twitter.com/stevesi/status/1750710198237286625)\:

> A whole new market will form made up of shady companies doing app development with the express purpose of pushing on all the new holes the EU is forcing open. What a train wreck for consumers who get no benefit only risk.

Steven was at Microsoft during their own antitrust adventures in the 1990s, so I'd take this indignation with a grain of salt. But it's also a popular position among Apple talking heads with whom I often agree.

As a counterpoint, [Bruce Schneier submitted](https://www.schneier.com/essays/archives/2022/01/letter-to-the-us-senate-judiciary-committee-on-app-stores.html "Letter to the US Senate Judiciary Committee on App Stores") to the United States Senate Judiciary Committee on App Stores:

> App store moderation frequently misses important threats to privacy and security, including data brokers and scammers. Both Apple and Google have approved apps that egregiously violate user privacy and security, often in direct contradiction to their own policies.5 The incredible volume these marketplaces handle (Apple’s App Store receives 100,000 submissions per week) means that they cannot possibly vet every single app as thoroughly as desired.
>
> Giving tech companies a veto over which software users can and can’t trust is a system that fails badly. That is: it’s one thing to seek a company’s recommendations about what constitutes a security risk, and another to let that company’s judgment override your own. The former requires that the company be reliable, the latter requires that the company be infallible. And companies are not infallible.

That tackles the disingenuous security argument, but I think there's something even bigger going on. What compelled the EU to legislate this in the first place? Or asked another way, had these platforms taken their corporate social responsibility seriously, would European politicians have the ammunition to push through such laws?

This is what is so often missing from discussions like this. There's this view that misguided or heavy-handed laws are dreamed up out of nowhere to screw people and businesses. But this misses the forest for the trees. If a business or industry abuses their position or doesn't act in the interests of society, it has to be forced to. The solution is to not be abusive, not to whinge when someone calls you out.

I'd say the EU's track record on delivering digital legislation is fair to mixed. But at least they're having the debate. Those of us in the Anglosphere should be too, rather than granting these business *carte blanche* to do whatever they want.
