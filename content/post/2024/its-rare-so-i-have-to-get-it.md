---
title: "It’s rare, so I have to get it?"
date: "2024-07-04T08:26:59+10:00"
year: "2024"
category: Hardware
tag:
- psychology
- retrocomputers
location: Sydney
---
Picture this: I'm researching a particular computer part, when I find a listing for a different component or machine that's *exceedingly* rare. The sort of thing that crossed my path this one time, and I'm unlikely to see again. It may be rare on account of being valuable, or it could simply have filled an unusual niche.

How would you respond to the situation?

I cosplay as a rational person, so I mostly ignore them. As my grandad used to say, if you didn't know you needed or wanted something before seeing it, you almost certainly don't. Here comes the proverbial posterior prognostication: *but...* it nags at me. I start hearing that voice in my head that says "yeah, but what if you never see it again? It's so rare!"

As a recent example, I saw a Japanese computer tower from the 1980s that had a limited run in Austrlaia and New Zealand. I remember vaguely reading about them, and thinking they were cool. The price also wasn't unreasonable. I thought "this is never going to come up again, I'd better get it!" I held back, it was later sold, and I felt a weird mix of disappointment and relief.

I suppose a shrewd investor may see opportunities in a specific market, and think they can flip an asset for a profit. But that's not why I'm into retrocomputers as a hobby (though I have sold superfluous stuff in the past). The truth is, I'd almost certainly buy this "rare" thing, and proceed to not use it.

I can see why people like Mat from Techmoan have a self-storage locker. Leaving aside the motivation to create more videos, the compulsion to have something because it's rare, regardless of whether you'd actually need or want it, is strong. Marketers have known this for a long time.
