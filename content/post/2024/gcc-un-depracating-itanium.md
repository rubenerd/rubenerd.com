---
title: "GCC un-depracating Itanium"
date: "2024-10-12T10:25:05+11:00"
year: "2024"
category: Hardware
tag:
- gcc
- itanium
- news
location: Sydney
---
[Michael Larabel reported in Phoronix](https://www.phoronix.com/news/GCC-15-Undeprecates-Itanium)\:

> GCC 15 had planned to remove Itanium IA-64 support to close the book on that Intel architecture. That GCC move followed the Linux kernel removing Itanium support last year and more distributions ending Itanium support although many did so years ago. But open-source developer René Rebe stepped up and wants to maintain Itanium's IA-64 support in the GNU Compiler Collection.

I don't use GCC, but this sort of thing makes me happy in the way all of [NetBSD's ports](https://netbsd.org/ports/) do. Good on you, René! It's one thing to run contemporaneous software and period-correct hardware on legacy machines, but it's another to introduce them into the future. (It's also a bit of a commentary about how wasteful modern tech is with resources, but that's a separate discussion).

Weirdly enough, I did actually mess with Itanium briefly when I was in high school. I remember being hamgstrung by actually being able to boot things, and being frustrated at the limited software support. Little did I know how accurate those (albeit unremarkable) observations were to become.

Part of me does wonder how different the industry would look if everyone followed Itanium instead of amd64. Maybe we would have at least had some more [architectural diversity](https://rubenerd.com/web-tech-needs-diversity/).
