---
title: "My experience with Ironic’s lyrics"
date: "2024-12-23T09:49:32+11:00"
abstract: "Seeing how many of mine mirror my lived experience too. More than I thought!"
year: "2024"
category: Media
tag:
- 1990s
- music
- music-monday
- philosophy
location: Sydney
---
On today's *[Music Monday](https://rubenerd.com/tag/music-monday/)*, we're exploring the lyrics to Alanis Morissette's *[Ironic](https://www.youtube.com/watch?v=Jne9t8sHpUc "Play Alanis Morissette - Ironic")*. You weren't around in the 1990s if you didn't know at least some of the words, or had opinions&trade; regarding whether the song described irony at all, or merely a string of bad luck. It didn't matter, it was an iconic song.

I thought it'd be fun to see how many of them I've experienced. Play along and answer too if you'd like.

<figure><p><a target="_BLANK" href="https://www.youtube.com/watch?v=Jne9t8sHpUc" title="Play Alanis Morissette - Ironic (Official 4K Music Video)"><img src="https://rubenerd.com/files/2024/yt-Jne9t8sHpUc@1x.jpg" srcset="https://rubenerd.com/files/2024/yt-Jne9t8sHpUc@1x.jpg 1x, https://rubenerd.com/files/2024/yt-Jne9t8sHpUc@2x.jpg 2x" alt="Play Alanis Morissette - Ironic (Official 4K Music Video)" style="width:500px;height:281px;" /></a></p></figure>

### Verse One

> An old man turned ninety-eight   
> He won the lottery, and died the next day

Mostly not applicable. The closest I've come to winning a lottery was placing second in a primary school writing competition, which netted me SG$800. This *felt* like winning the lottery for a 12 year old kid! It was how I financed the first computer I built myself, [which is still running](http://retro.rubenerd.com/mmx.htm) to this day. And thankfully, it didn't result in my demise.

> It's a black fly in your Chardonnay

I've had my fair share of insects take a fancy to my cups of coffee and tea. The biggest was a dragonfly when we lived in Malaysia. I didn't know they could get that big, nor was I aware they had a taste for caffeine.

> It's a death row pardon, two minutes too late

Not applicable, fortunately.


### Chorus

> It's like rain on your wedding day

Not applicable personally, though it did drizzle at my sister's wedding a few years ago. I had also just left surgery a couple of weeks before, and almost didn't make it. But I could, which was great :).

> It's a free ride when you've already paid

I don't think this has happened, though I've paid the bill for myself when it was supposed to be a company lunch. Whoops!

Semi-related, during the brief time I was working in San Francisco, I topped up my Clipper card before immediately losing it. I bought a new one, then on the second-last day there, found the original one in a coat pocket. It sucked, because I felt like I'd already paid for all those trips. Then I turned around and the *exact* same thing with an ICOCA in Ōsaka. I did find the original, and it's still my primary IC when Clara and I go to Japan, even outside Kansai :').

> It's the good advice that you just didn't take

I always thought the lyric was "just *couldn't* take", which carries very different connotations. *Couldn't* suggests that you knew it was good advice, but something else (good or bad) was preventing you from carrying it out.

"Didn't take" sounds like you have a bit more agency in deciding not to take advice. I've definitely not taken advice, and subsequently had it blow up in my face. Then again, I've also ignored bad advice, so maybe it's a wash?

> And who would have thought? It figures.

I once showed my calculator to a teacher and said "it *figures*". She had the good nature to laugh at my attempt at a joke. It's funny, I've thought about Ms Harris a lot lately, I hope she's going well.


### Verse Two

> [Details about a plane crash]

Fortunately not applicable. Feel free to [skip to the bridge](#bridge) if you're squeamish.

We did have a narrow miss during a very turbulent landing where I swore I could see the wing clip the ground at Sydney Airport a few years ago. I'm sure it didn't actually, but the pilots weren't taking any chances and aborted the landing with a [full go-around](https://en.wikipedia.org/wiki/Go-around "Wikipedia article on go-arounds"). The engines roared louder than I'd ever heard before, and we took off again.

Given the number of flights I've had to be on for life and work, I continue to be so impressed with the entire industry. We might hate the bean counters for how we're treated in the cabins, but the safety record puts it so far ahead of every other mode of transport I use on a regular basis. If motorists treated their cars the same way, I wouldn't feel so anxious crossing the road.


### Bridge

> Well, life has a funny way of sneaking up on you   
> When you think everything's okay and everything's going right   
> And life has a funny way of helping you out   
> When you think everything's gone wrong   
> And everything blows up in your face   

This is a variation on the theme Baz Luhrmann explored in the Sunscreen Song, when he says "The real troubles in your life are apt to be things that never crossed your worried mind. The kind that blindside you at 4 PM on some idle Tuesday."

I'm sure we've all had well-laid plans fall apart with a random occurrence seemingly outside our control. Save for our house, this has been such a year for Clara and I. It's been exhausting!

I've been lucky that whenever I've reached the precipice, something or someone has pulled me back. And just as Alanis and Baz said, it's surprising how often it comes from somewhere you least expect. Keep an open mind as they say, though not too open or your brain will fall out.


### Verse Three

> A traffic jam when you're already late

Of course! I won't get on my public transit high-horse and say that traffic is bad when there aren't viable alternatives to driving available, so people have to cram themselves in small metal boxes on endless bitumen and concrete.

I've only ever missed one flight, and that was thanks to being stuck in a taxi in traffic. This unfortunately only reinforced my need to overthink and overplan *all the things!* 

> A "No Smoking" sign on your cigarette break

I don't smoke, but I've certainly stood outside a closed café on my coffee break, dismayed that they had to close early. The silver lining was that I got much better at using my office AeroPress, which ended up being my Covid Lockdown AeroPress.

> It's like ten thousand spoons when all you need is a knife

YES! Every time I've moved house, I've always ended up standing in a room somewhere with a screwdriver but no Swiss Army Knife, or a bread knife but no steak knifes, or five BILLION VGA cables and not a SINGLE HDMI! AAARGGHH!!!

> It's meeting the man of my dreams   
> And then meeting his beautiful wife

I had a massive teenage crush on someone in high school, who turned out to have a massive crush on me. They confessed to me in this awkward way, and I responded awkwardly, and it was awkward. There was definitely a part of me that went dark when I learned a few years later they'd married. But then I met Clara at the university anime club, and she was the best ever :).

I guess that's it. I'm surprised, for some reason I overestimated the number of lyrics in the song. Maybe she needs to do a sequel.
