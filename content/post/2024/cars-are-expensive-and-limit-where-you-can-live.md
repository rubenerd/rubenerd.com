---
title: "Cars are expensive, and limit where you can live"
date: "2024-05-02T08:31:06+11:00"
thumb: "https://rubenerd.com/files/2024/chatswood-sydney-cbd@1x.jpg"
year: "2024"
category: Travel
tag:
- cars
- mental-health
- public-transport
- not-just-bikes
- sydney
- urbanism
location: Sydney
---
A recent episode of *The Urbanist Agenda* was eye-opening for exposing just how much cars cost. Reece from *[RM Transit](https://talent.nebula.tv/rmtransit/)* joined Jason from *[Not Just Bikes](https://nebula.tv/notjustbikes/)* laid down everything from... well, I'll let Reece summarise:

> **Reece:** You don't ever really know how much it's going to cost to own [a car]. Your fuel economy goes up and down based on the condition of your car. The cost of fuel constantly changes. Someone might scratch your car, or run into your car, and you have to think about insurance and, hey, now you have to make a calculation: do I wanna use insurance for this or do I want to use an out of pocket cost?
> 
> **Jason:** Oh man, you're bringing back bad memories for me.
> 
> **Reece:** All these layers of incidental cost. Licencing fees. Tolls. All of these things are so random; they're almost impossible to predict. I decided to drive on a different street one day, and now I have a different cost I didn't predict. Tracking the cost of a car is enormously difficult, and that's to the car industry's benefit. 

Jason discussed a story involving his first week back in Toronto where he had a flat and had to get a tow, and another occasion where the onboard computer died, which cost another $800 and took two days (my dad had a similar experience recently with his Ducati).

There are also the intangible costs to one's health and mood that comes from gridlocked traffic, long commutes, and stress when something goes wrong. We all know someone who's been affected by an accident, whether as a driver or a pedestrian. At best, it can be a costly inconvenience. At worst, it can lead to... well, you know. Even if the claims of *millions* of road fatalities a year are exaggerated, it's still *galling* to me how cavalier society is about these metal boxes.

Both these gentleman touch on the inevitable outliers that come up when discussing this. There's always a guy who'll pipe up claiming his $500 used Corolla from 1997 has only ever needed one tyre changed in ten years. Then there are car enthusiasts, for whom cars are an enjoyable part of their lives, not just a commute device. Except, you and I know most people don't fall into these categories.

<figure><p><img src="https://rubenerd.com/files/2024/melbourne-tram-c@1x.jpg" alt="Photo of a D-Class train with horrible backlighting" srcset="https://rubenerd.com/files/2024/melbourne-tram-c@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

*(Photo of a tram I took last month. Melbourne has more trams than anywhere else in the world, but they really need to pedestrianise more of their streets)!*

All of this was interesting in an academic numbers sense, but the part that *really* resonated with me is the cost to lifestyle. This is where my own lived experience informs how I live my life now, and where I choose to live.

My dad was a business traveller, so I grew up in a bunch of cities. These included small apartments in Singapore, and far-flung suburban houses in Sydney, Melbourne, Brisbane, and Kuala Lumpur. I also spent time studying in Adelaide.

My parents loved houses, but they felt like a [Faustian Bargain](https://en.wiktionary.org/wiki/Faustian_bargain "Wiktionary definition") to me. I *loved* living within walking distance from a Singaporean MRT station, and so many conveniences and fun places. You live a fundamentally different life when you do this in a way that's hard to describe unless you try it... not to mention having it *revoked* when you move back out to the sticks. I'll bet I would have loved living in KL if we'd stayed in that inner-city serviced apartment, and not that McMansion 40 km outside the city without any access to public transport.

With Clara's experience living in suburbia too, we decided to rent in one of Sydney's small pockets of awesome urbanism when we moved in together; similar to how Jason describes his old streetcar suburb of Toronto. We have a small apartment within walking distance of train and metro stations, supermarkets, grocers, doctors, dentists, coffee shops, bubble tea, book stores, pharmacies, theatres, and restaurants. Similarly, we love travelling to Melbourne because their CBD has all of this too, and more.

<figure><p><img src="https://rubenerd.com/files/2024/chatswood-sydney-cbd@1x.jpg" alt="Aerial view of Chatswood with the Sydney Harbour Bridge CBD in the far background, by Mark Merton" srcset="https://rubenerd.com/files/2024/chatswood-sydney-cbd@2x.jpg 2x" style="width:500px; height:333px;" /></p></figure>

*(Photo of Chatswood by [Mark Merton](https://commons.wikimedia.org/wiki/File:Aerial_View_Chatswood_to_Sydney_CBD.jpg "Aerial View Chatswood to Sydney CBD"), with the Sydney Harbour Bridge CBD in the far background. Most of those towers are apartments. Talk about the [missing middle 📺](https://www.youtube.com/watch?v=CCOdQsZa15o "The Houses that Can't be Built in America") of housing, though at least the state government is [attempting](https://www.abc.net.au/news/2023-12-08/how-your-suburb-affected-by-sydney-housing-rezoning/103200116 "How your suburb will be affected by Sydney's rezoning to address housing crisis") to address this).*

Except, it's not all luck. A big reason why we can do this, or even be looking at buying a place in an area like this, is because we don't own a car. We maintain meticulous budgets and spreadsheets (it's the only way we feel confident travelling), and even conservative estimates of car ownership made us blanch. And this isn't even Singapore, where you have to contend with additional costs like COEs. Some people see a car as a symbol of freedom, but we'd rather not be throwing money away each month on a mobility tax to live in a remote house.

I guess it all comes down to opportunity costs, and how you want to live your best life. If you can do a bunch of life hacks to get a cheap car and a house, go for it. But I'd rather walk or take the train, and live somewhere wonderful, rather than commute to it.

Thanks again to Jason and Reece, I love their chemistry when they do a show together *aboot* something. Maybe it's all the authentic maple syrup in those stroopwafels, *eh.* Australians who grew up in Asia shouldn't attempt Canadian accents. Actually, probably nobody should. *Wah lao eh*.
