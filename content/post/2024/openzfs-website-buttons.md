---
title: "OpenZFS website buttons"
date: "2024-04-14T08:58:12+10:00"
year: "2024"
category: Software
tag:
- buttons
- openzfs
- pointless
- zfs
location: Sydney
---
We're *well* into the wrong decade for these Antipixel-style 80x15 website buttons. But they're delightful, and I've started including them in my blog theme and their own page again.

We have a few different ones for the various BSD operating systems, but there wasn't one for ZFS. So I pulled the colours from the official logos and created some.

<figure><p><img src="https://rubenerd.com/files/2024/openzfs@1x.png" alt="80x15 OpenZFS button" style="width:80px; height:15px;" /><br /><img src="https://rubenerd.com/files/2024/openzfs@2x.png" alt="160x30 OpenZFS button" srcset="https://rubenerd.com/files/2024/openzfs@2x.png 2x" style="width:160px; height:30px;" /></p></figure>

You can use them in-place, or with the HTML5 [srcset attribute](https://developer.mozilla.org/en-US/docs/Web/API/HTMLImageElement/srcset) so it renders like this:

<figure><p><img src="https://rubenerd.com/files/2024/openzfs@1x.png" alt="OpenZFS" srcset="https://rubenerd.com/files/2024/openzfs@2x.png 2x" style="width:80px; height:15px;" /></p></figure>


