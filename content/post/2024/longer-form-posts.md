---
title: "Longer-form posts"
date: "2024-01-27T10:23:46+11:00"
year: "2024"
category: Internet
tag:
- weblog
location: Sydney
---
I've been trying an experiment this week writing fewer, longer posts instead of more short ones. It's been kind of fun, and also a welcome distraction from some more bad family news.

If there's anything you'd like me to talk about, [flick me an email](https://rubenerd.com/about/#contact)!

The irony isn't lost on me that this is a small post. I contemplated writing a few dozen paragraphs of pointless fluff to pad it out, but fluff tends to compress rather than expand.

That'd be a pretty cutting insult for a technical writer. *Yo Ruben, I gzip'd your writing, and it was 1 byte!*
