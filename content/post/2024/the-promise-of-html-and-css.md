---
title: "The promise of HTML and CSS"
date: "2024-09-18T08:41:40+10:00"
year: "2024"
category: Internet
tag:
- accessibility
- html
location: Sydney
---
I launched *[Ruben's Retro Corner](http://retro.rubenerd.com/)* back in November last year, ostensibly to organise and gather information on my growing retrocomputer collection, but also to relive my primary school days when I hand-coded HTML without a CMS. It also uses plain HTTP 1.1 and valid HTML3, so my oldest 32-bit machines can still write and access it.

Little did I know that it would grow into something (a bit) bigger. I've had email from people saying they use it to test their old machines and browsers for network acccess and browser compatibility. Others have described it as a protest of what the modern web has become, which I'm happy to take on. More than a few have said it's now the home page of their current browser, which I'll admit does make me smile.

As always though, I can't pass up an opportunity to overthink why this is.

The first possible reason is well-worn ground here: the general apathy and distrust people have for the modern Web. Dark patterns, hostile design, and a foreboding sense that one's trust and privacy are regularly violated have lead to a situation where few people say they "love" the Web anymore. Part of this comes down to it becoming a utility, but I'd argue it's also the result of people being worn down by a Web that's no longer designed for *us*. I [understand why Gemini now exists](https://rubenerd.com/i-like-gemini-gopher-but-also-html/ "I like Gemini and Gopher… but also HTML"), though I'll admit to liking regular old HTML as well. 

I've also noted an explosion in posts on Mastodon recently from people talking about what made HTML (and CSS) so great, and how far the Web has come since JS frameworks took over. I thought [Adrian Cochrane](https://floss.social/@alcinnz/113155266474715867) summarised the elegance of a backwards-compatible markup language well. Likewise, Terence Eden's seminal 2021 blog post about [HTML accessibility on limited devices](https://shkspr.mobi/blog/2021/01/the-unreasonable-effectiveness-of-simple-html/) has been shared widely again, and for good reason. Not everyone has access to the latest smartphone, tablet, or souped up workstation. Would *you* be able to survive a personal tragedy armed with the most basic of web-accessible devices? Based on the sites and services I have to use for my daily life, I can confidently say that for me the answer is *no*.

The thing is, HTML never went anywhere. It's still here, albeit buried and abstracted under further layers of inaccessible, hostile, or complicated cruft. We need to make it the default again, where the dynamic overlays are additive, not essential.
