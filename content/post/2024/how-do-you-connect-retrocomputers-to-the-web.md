---
title: "How do you connect retrocomputers to the web?"
date: "2024-03-14T09:03:15+11:00"
year: "2024"
category: Hardware
tag:
- am386
- apple-iie
- commodore
- commodore-128
- psychology
- retrocomputing
location: Sydney
--- 
I know it's a bit of a troll title, but it's a common question I get now. The short answer is *I don't!* Thanks for coming, have a nice day.

The longer answer comes down to two reasons. I'm too young to have nostalgia for BBSs, though I am interested in them from a technical perspective. It's why I run 8-bit machines that were made before when I was born too! Okay, that's clearly a bad reason.

The more meaningful one is that **retrocomputers are an escape**. They're a way for me to scratch my itch for technical tinkering and learning without any pretence of productivity. I have a rewarding and interesting job, but I also need a break.

My daily responsibilities involve talking, so when I come home I want to use these machines like puzzles, not another avenue for social interaction. Introversion is difficult to understand for people who don't think that way, but I feel I have a finite daily capacity for social interaction before it makes me feel tired (and in extreme cases, anxious and irritable).

There's something *so* comforting about using a computer that's isolated from the world. I get a similar feeling when I'm on a plane without Wi-Fi. Your experience with using the machine completely changes. It's just me, a blinking cursor, and a reference book in PDF or paper form.

That's not to say *you* should use your machines that way. I *do* use serial terminal applications on my Am386, Commodore 128, and Apple //e for some fun, which I suppose is a form of networking. But thus far I've resisted taking it a step further and accessing a terminal jump box of some sort, which would give me access to SSH, and so on. That would breach the mental firewall.

The irony also isn't lost on me that I'm about to commit this post to Git, and have it published on my *website*. It'd be even funnier if I admitted that I wrote this on the Apple //e, and exported it using the FloppyEmu.
