---
title: "The housekeeping namespace"
date: "2024-06-23T10:49:03+10:00"
thumb: ""
year: "2024"
category: Thoughts
tag:
- atom
- namespaces
- rss
- specs
location: Sydney
---
The `housekeeping` namespace is a collection of useful backend elements for XML syndication formats, such as RSS and Atom.

*This is version 1.0, last updated 2024-06-23.*


### Namespace declaration

    xmlns:housekeeping="https://rubenerd.com/xmlns-housekeeping/"


### Model

`<channel>` elements in RSS, or `<feed>` elements in Atom:

* `<housekeeping:assert>` (hash)
* `<housekeeping:pointless>` (plain text)
* `<housekeeping:robots>` (values from HTML robots meta tag)
* `<housekeeping:validation>` (URI to external validator)

Attributes defined below are recommended, but not mandatory.


### assert

This is a hash of a secret phrase used to assert ownership of a feed, akin to a Wikipedia [committed identity](https://en.wikipedia.org/wiki/Template:Committed_identity). If challenged, you can present the plaintext. This is looser than signing with a tool like PGP, but easier for an ever-evolving, dynamic document like a web feed.

* A `scheme` attribute defining the hashing algorithm used is recommended.

**Example:**

    <housekeeping:assert scheme="sha512">$HASH</housekeeping:assert>


### pointless

This is freeform text for any purpose, provided such purpose is fun, pointless, useless, and/or ephemeral! I've used it to troubleshoot caching issues.

* A `type` attribute defined as `text/plain` is recommended, to help with documentation.

**Example:**

    <housekeeping:pointless 
        type="text/plain">This has four words!</housekeeping:pointless>


### robots

This conceptually maps to the HTML robots meta tag, and can also be used to express the proposed `noai` and `noimageai` values.

I doubt any generative AI tool respects these, even their originator. The goal here is to express intent and raise awareness of these tools using people's creativity and love without their knowledge, legal consent, attribution, or compensation. Alternatively, one could also specify `ai` and `imageai` to permit this use.

**Example:**

    <housekeeping:robots>noai, noimageai</housekeeping:robots>


### validation

This links to a resource, internal or external, that can be used to validate the feed. The [RSS Board](https://www.rssboard.org/rss-validator/) and [W3C](https://validator.w3.org/feed/) are the two validators of which I'm aware.

* The `rel` attribute maps to the equivalent in HTML. This can be used to define whether the validator is external to the domain of the feed. This is recommended.

* The `type` attribute defines the mimetype of the validator's output. The validator may output JSON, for example.  This is recommended.

**Example:**

    <housekeeping:validation 
        href="https://www.rssboard.org/rss-validator/check.cgi?url=https%3A%2F%2Frubenerd.com%2F" 
        rel="external" type="text/html"/>


### History

* Draft 0.1 included a `housekeeping:mimetype`, to define what resources `<description>` elements contained. Types were introduced in RSS 0.94, but [removed again](https://web.archive.org/web/20080917141227/http://archive.scripting.com/2002/09/06#theRoadToRss20) in RSS 2.0, which introduced ambiguity and hampered interoperability. Ruben since remembered Dublin Core has `dc:format`, which should be used instead. Atom avoids this issue with the `type` attribute in the `<content>` element.

* Draft 0.1 recommended the `xmlns:hk` namespace, but this sounded like Hong Kong. Ruben expects any future `xml:hk` namespace to include references to amazing food, architecture, and cinema, not backend infrastructure.

