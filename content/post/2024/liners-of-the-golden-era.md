---
title: "Music Monday: Liners of the Golden Era"
date: "2024-02-19T13:14:47+11:00"
year: "2024"
category: Media
tag:
- cunard
- music
- music-monday
- ocean-liners
- white-star-line
location: Sydney
---
It's *[Music Monday](https://rubenerd.com/tag/music-monday "View all posts tagged in Music Monday")* time! Each and every Monday (cough), I impart some information about a song, musician, or album such that we may *Ravel* in their artistry. No, I'm not apologising for that.

Today's installment has a fun backstory. For the last couple of years I've been following the progress of the History FX team and their *[Lusitania: The Greyhound's Wake](https://www.historicalfx.com/lusitaniatgw)* efforts. Billed as a virtual museum, this team of talented artists and 3D designers have been faithfully recreating all the major rooms and decks of my favourite ever ocean liner, the RMS *Lusitania*. I can't wait to be able to play it!

Part of their research lead them to record and produce an album of music from the time period, which I've been playing on repeat since I bought and downloaded it. Each song has two versions: a polished studio production, and a vintage-style recording straight from the piano. There's *definitely* something about the latter that places you right there in the dining saloon, or the first class reception as you steam across the ocean at the turn of last century.

*I say old chap, are we about to capture the Blue Riband? Jolly good!*

<figure><p><a title="Liners of the Golden Era: A Musical Tribute - eBook Digital Download" href="https://www.historicalfx.com/store/p/liners-of-the-golden-era-a-musical-tribute-ebook-digital-download"><img src="https://rubenerd.com/files/2024/liners-of-the-golden-era@1x.jpg" alt="Cover of Liners of the Golden Era: A Musical Tribute" srcset="https://rubenerd.com/files/2024/liners-of-the-golden-era@2x.jpg 2x" style="width:500px; height:375px;" /></a></p></figure>

If you're into old-timey tunes with a bit of history, this was a lot of fun. [The landing page](https://www.historicalfx.com/store/p/liners-of-the-golden-era-a-musical-tribute-ebook-digital-download) lets you purchase the accompanying eBook, but they also have links to online stores to download the tracks.
