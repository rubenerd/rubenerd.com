---
title: "Jason Gorman on speed and productivity"
date: "2024-08-18T09:10:49+10:00"
year: "2024"
category: Software
tag:
- development
- engineering
- productivity
location: Sydney
---
[Chef's kiss, via Mastodon](https://mastodon.cloud/@jasongorman/112977584632168576)\:

> If I go by plane, it takes me 8 hours to get to my Dad's house. If the plane flew at twice the speed, it would take 7 1/2 hours.
> 
> In case you were wondering why producing code faster hasn't made much difference to your team's productivity.

This works on *so many levels!*

If you haven't had the chance to travel much before, there's an unwritten rule that short to medium haul flights often constitute the shortest part of a trip, for a few reasons:

* There's a significant amount of what I dub life administration that has to happen on both sides, including booking tickets, parting out what you need in carry-on versus checked-in luggage, working around airline schedules and variable prices, and so on. It's not like tapping a transit card at a station.

* On the day, you need to travel *to* the airport, which is often located well outside the bounds of the city you're leaving from, and again on the other side. Opponents of high-speed rail tend to forget this.

With regards to software development (or network architecture, or system design), the actual writing of code constitutes but a tiny fraction of the work involved. You've got market research, financing, requirements gathering, planning, and proof of concepts on one end, and testing, iterating, staging, fixes, and writing docs at the other. If you're doing agile, you've got feedback. If you're waterfalling, you have milestones. And none of these account for human factors; you know, the people actually designing, implementing, and testing your system.

This is why I didn't ever understand the Facebook concept of *running fast and breaking things* at a conceptual or practical level. I do like the idea of *walking slow and fixing things* though.
