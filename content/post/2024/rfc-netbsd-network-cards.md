---
title: "RFC: NetBSD network cards"
date: "2024-11-21T09:20:21+11:00"
year: "2024"
category: Hardware
tag:
- bsd
- netbsd
- networking
location: Sydney
---
I asked on Mastodon, but posting here as well. What 10G Ethernet cards are people using on NetBSD thesedays? Are there any in particular you would recommend?

I just buy used Intel X550 cards for FreeBSD and Penguins, but I've never messed with 10G on NetBSD. Ideally it would also work in 2.5/5 mode as well. This is for a new homelab cluster and some experiments I hope to do.

Feel free to reply to the [Mastodon thread](https://bsd.network/@rubenerd/113517601581541298), or [send me an email](https://rubenerd.com/about/#contact). Thank you!
