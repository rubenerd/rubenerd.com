---
title: "He’s Nico Cartron"
date: "2024-03-21T17:01:58+11:00"
year: "2024"
category: Internet
tag:
- corrections
- names
location: Sydney
---
My post yeterday about the [WITCH computer](https://rubenerd.com/demonstration-of-the-restored-witch-computer/) mentioned that [Nick Cartron](https://www.ncartron.org/ "Demonstration of the restored WITCH computer") had shared it with me. This was wrong, his nickname is *Nico*.

I'd messaged three different *Nicks* yesterday, so I guess my brain was primed to write it as such. *Pardon*, Nico!

As an apology, I encourage everyone to stop reading here today, and instead head to his blog instead. His recent post about [wandering around Wikipedia](https://www.ncartron.org/wandering-on-wikipedia.html) had some fun links :).
