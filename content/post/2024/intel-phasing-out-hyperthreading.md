---
title: "Intel phasing out hyper-threading"
date: "2024-06-07T10:55:34+10:00"
year: "2024"
category: Thoughts
tag:
- cpus
- news
- security
location: Sydney
---
Michael Crider [watched the Computex keynotes](https://www.pcworld.com/article/2355112/intel-abandons-hyperthreading-lunar-lake-cpus.html "Intel abandons hyperthreading for Lunar Lake CPUs"), so we didn't have to:

> Hyperthreading will be disabled on all Lunar Lake CPU cores, including both performance and efficiency cores. Why? The reason is complicated, but basically it’s no longer wanted as Intel strives to maximize power efficiency in portable laptops.

This [confirms the rumours](https://www.techspot.com/news/101599-intel-arrow-lake-s-desktop-processors-could-ditch.html) from earlier this year.

Of course Intel are going to tout efficiency, but we all know security played a role here too. Hyper-threading was always dubious, and the active mitigations against speculative exploits nullified much of the performance uplift it provided.
