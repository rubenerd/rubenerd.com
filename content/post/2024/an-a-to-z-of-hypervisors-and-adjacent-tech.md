---
title: "A pointless A-Z of hypervisors and adjacent tech"
date: "2024-07-17T08:27:38+10:00"
year: "2024"
category: Software
tag:
- lists
- pointless
- virtualisation
location: Sydney
---
* **A**MD-V 
* **B**hyve
* **C**loud computing
* **d**omU
* **E**SXi
* **F**reeBSD
* **G**rant table
* **H**ypervisor
* **I**nfiniBand
* **J**ails
* **K**VM
* **L**VM
* **M**anagement Agent
* **n**vmm
* **O**VF
* **P**VHVM
* **Q**ubes OS
* **R**DMA
* **S**chedulers
* **T**ype 1 hypervisor
* **U**TM
* **v**md
* **W**PARs
* **X**en
* **Y**ABA
* **Z**FS
