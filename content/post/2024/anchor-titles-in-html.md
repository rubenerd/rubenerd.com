---
title: "Best practices for HTML anchor titles"
date: "2024-02-12T17:48:38+11:00"
year: "2024"
category: Internet
tag:
- accessibility
- standards
location: Sydney
---
Back in October last year I ended my [Gemini-style link experiment](https://rubenerd.com/i-like-how-gemini-handles-links/ "Rubenerd: I like how Gemini handles links"), and [went back to using inline links](https://rubenerd.com/concluding-my-link-experiment/ "Rubenerd: Concluding my link experiment"). Look at that, I've done two already! From the latter post:

> I’m going back to inline links, but I’m making sure each link has alt text. I should have been doing that already for accessibility, but this way I can always render them out into their own section if I end up doing a Gemini (or Gopher) site as well.

James Savage, who has among the coolest names ever, [asked on Mastodon](https://social.axiixc.com/@axiixc/111916739200174321 "James' post on Mastodon asking about best practices") if I had any best practice references when it came to using title attributes. Easy, I thought! But then I went digging.

I had always assumed `title` attributes were meant to match the destination page title, with optional information added if required for context. But was this codified anywhere, or did I just pick it up years ago and take it as rote?

Reading the [XHTML 1.1 specification](https://www.w3.org/TR/xhtml-modularization/abstract_modules.html "XHTML 1.1: Abstract modules")&mdash;the last one I ever read in any great detail&mdash;we're told `a` in the *Hypertext Module* inherits the *Common* set of attributes, which includes the *Core* set, of which one is `title`. All the specification mentions is that it contains `CDATA`, which is about as helpful as your parents telling you a present was smaller than a breadbox. 

The HTML5 "Living Standard" has a [section on the a element](https://html.spec.whatwg.org/multipage/text-level-semantics.html#the-a-element "HTML Standard: Text-Level Semantics: The a Element"), which says *Global Attributes* apply, one of which is `title`. It says this at the time of writing, emphasis added:

> The title attribute represents advisory information for the element, such as would be appropriate for a tooltip. **On a link, this could be the title** or a description of the target resource

This gets closer to what I'd long assumed; the `title` attribute is the title of the target page. But *could* is vaguer than I remember.

Clara and I maintain a [retro webserver](http://www.sasara.moe/ "Sasara.moe") with pages written in HTML 3.2 for a bit of fun, and maybe as form of silly protest for what the modern web has become. I link to the gloriously grokkable [HTML 3.2 Specification](https://www.w3.org/TR/2018/SPSD-html32-20180315/ "HTML 3.2 Reference Specification: The anchor element") there, so I checked out what it had to say.

> **title** An advisory title for the linked resource.

Well then!

🌐

One thing I've been reminded of from reading these, especially the HTML5 Standard, is that titles are no accessibility panacea themselves. While screen readers may use them to aid those with limited visibility, and those on desktops get a nice tooltop describing the target page, people on capacitive-touch mobile miss out. It still probably makes sense for the visible text of the link itself to be meaningful.

As for what constitutes a good title, I think it still makes sense to use the original title, though maybe with the target site's name also included if it isn't in the original.
