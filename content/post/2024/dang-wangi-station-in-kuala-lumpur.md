---
title: "Dang Wangi station in Kuala Lumpur"
date: "2024-03-20T09:05:56+11:00"
thumb: "https://rubenerd.com/files/2024/dang-wangi-station@1x.jpg"
year: "2024"
category: Travel
tag:
- kuala-lumpur
- malaysia
- metro-systems
- public-transport
- trains
location: Sydney
---
I mentioned Kuala Lumpur in my post about a [computer workbench](https://rubenerd.com/having-a-dedicated-computer-workbench-area-again/), which made me nostalgic. Me? Never!

We used to go up to KL fairly often when we moved to Singapore. Even as a little kid I was obsessed with public transit and stations, and KL's metro was really interesting. It was a far cry from Singapore's MRT in reach, capacity, and frequency, but was still cooler than what I remembered in Australia.

<figure><p><img src="https://rubenerd.com/files/2024/dang-wangi-station@1x.jpg" alt="Photo of the platform with access stairs and escalators." srcset="https://rubenerd.com/files/2024/dang-wangi-station@2x.jpg 2x" style="width:500px; height:374px;" /></p></figure> 

*Dang Wangi station photo by <a href="https://commons.wikimedia.org/wiki/File:Dang_Wangi_station_(Kelana_Jaya_Line),_Kuala_Lumpur.JPG">Two Hundred percent</a> on Wikimedia Commons, as it appeared when I first rode on it.*

For one trip in 1999, our local station near the hotel was Dang Wangi on the brand new, automated LRT PUTRA line. I liked the stations; they were bright, simple, modern, and clean. Some of the stations vaguely remind me of Kyōto's Tōzai Subway Line which opened around the same time.

There's an understated, utilitarian style to this era of metro station that we've lost in in recent times with modern architectural excess. Stations today have to be massive, over-engineered, and eye-wateringly expensive to pass review and get built. They feel less comfortable to be in, and modern materials like cut stone and metal cladding have made them noisier. I'm all for taking pride and investing in public transport, but I'd rather *more of it* than fewer stations that look like modern cathedrals. *But I digress.*

The metro trainsets on the PUTRA line resembled those I'd seen in airports, albeit larger and with more carriages. Each station had a platform length well in excess of the operating trains, which my parents explained must be for future growth. I remember thinking this was a brilliant idea. Alas, they didn't agree that exploring the whole system was *also* a brilliant idea, so I vowed to return when I was older.

<figure><p><img src="https://rubenerd.com/files/2024/dang-wangi-refurb@1x.jpg" alt="Refurbished platform photo by " srcset="https://rubenerd.com/files/2024/dang-wangi-refurb@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

*Refurbished platform photo by <a href="https://commons.wikimedia.org/wiki/File:Southbound_platform_of_Dang_Wangi_LRT_Station_November_2017.jpg">Nick-D</a> on Wikimedia Commons. They retained the same bright, simple colour scheme, and added LED indicators. Integrating the boarding arrows into the tile work was a clever touch.*

The PUTRA was subsequently refurbished, and rebranded the Ampang Line when it was integrated into the RapidKL system. This was probably for the best, given the recent opening of the Putrajaya MRT which I'm sure would have caused confusion. We also [moved and lived in KL](https://rubenerd.com/tag/recorded-in-kuala-lumpur/ "View episodes of my silly show recorded in KL") for a couple of years in the 2000s, though as I said in that previous post, we were out in suburbia well out of reach of these cool metro systems.

Clara and I got to [ride more of the system in 2019](https://rubenerd.com/nostalgia-in-mutiara-damansara/ "#KL19 Nostalgia in Mutiara Damansara") when we went up to KL from Singapore for a weekend. My inner child train nerd was screaming the whole time :).



