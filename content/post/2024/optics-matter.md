---
title: "WordPress, work, and why optics matter"
date: "2024-10-09T11:37:41+11:00"
year: "2024"
category: Thoughts
tag:
- engineering
- psycology
- wordpress
location: Sydney
---
*(I couldn't sleep last night, owing to whatever bug it is I currently have, so I wrote this. It's not one of my better ones, but maybe there's something here)?*

Picture the scene. A managing director had to inform his staff that promised pay increases must be deferred or cancelled due to prevailing market conditions, and that certain staff programmes are to be cancelled. A week later, the director is posting pictures of his tropical holiday to the company chat system, captioned with a cheeky *wish you were here!*

How would *you* react upon hearing this story? I broadly see two kinds of responses:

* **Those who see nothing wrong**. The director was well within his rights to take leave, just as any member of staff. The tickets may have been booked in advance, may not be refundable, and were bought with the director's own money. Some may express surprise that anyone would find this objectionable. The company's finances have no bearing on the director's personal life, and conflating the two issues isn't productive.

* **Those who react negatively**. The director is seen as being out of touch with his staff, hypocritical, and is behaving in a manner at odds with his expressed regret a week earlier. The timing is bad, and the photo was tone deaf, or at least made in poor taste. It should be no surprise that morale within the business takes a hit, and that his staff express bemusement, frustration, or anger at the circumstances. The inconvenience of cancelling the trip was precisely why people would find it an honourable gesture.

I think how people react here is telling. Note the difference in approaches: the people who see nothing wrong fall back onto tangible, black and white rules, while those expressing negativity focus instead on social norms and expectations. I think both perspectives are valid at a superficial level, though I think the former forgets basic human psychology.

This leads us to the latest furore in the WordPress community, and how it has exposed a fundamental truth that people in PR have known since they wrote their first press release, but that engineers continue to ignore at their peril: **optics matter!**

Did Matt have a point about WP Engine not contributing their fair share to upstream, financially or with code? What about his trademark concerns? I don't think he has a legal or moral leg to stand on, but it doesn't really matter. People won't remember his perspective, they'll remember his conduct. They'll remember a gentleman who spent decades building good will in an open source community, only to burn it away in a blaze of threats and references to cancer.

Matt isn't alone. Even if we assume he's right, I think as engineers and technical people we fall into the trap of assuming that facts, logic, and reason are sufficient to make a case. That arguments are transactional, no different from a school debate tournament, or loading a program with parameters to arrive at a single outcome. I like to think I learn from it when I make this mistake, but others seem hell-bent on being as stubborn and obtuse on this point as possible, often in spite of people urging them to show restraint.

*(Those engineers, whom in my experience tend to also regard the humanities with disdain, are also the ones training LLMs to answer questions. Ain't that something to think about).*

This is not how the world works. People aren't always rational actors. We're messy, have emotions, and are motivated by qualitative factors that can't be conceptualised in a simple A/B test, or a Y/N question. Salads are healthy, but we buy hot chips with chicken salt. It's not logical! But it's part of the human experience we're all sharing in together. Again, we disregard these factors at our peril.

I've worked in places, and within teams, where whatever point is being made is functionally conceded by the way in which it's delivered, either because it's unprofessional, unbecoming, or unproductive. It *never* reflects well on the person making the point, irrespective of the factual basis of their points.

Matt isn't the first technical person to make this mistake, and he won't be the last. But if there's a lesson in all of this, it's that *words have an effect*. Whether that effect is what we intended is irrelevant when measured against the outcome.
