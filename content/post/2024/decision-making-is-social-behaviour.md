---
title: "Decision making is social behaviour"
date: "2024-07-25T08:31:48+10:00"
year: "2024"
category: Thoughts
tag:
- quotes
- work
location: Sydney
---
American psychologist Lee Roy Beach, [as quoted in WikiQuote](https://en.wikiquote.org/wiki/Lee_Roy_Beach#The_Psychology_of_Decision_Making:_People_in_Organizations,_2005)\:

> Decision making is essentially social behaviour, even when there is nobody else present, because one anticipates how others will react and factors this into the decision. [...] Organizations per se do not make decisions, but individuals in organizations do. And when they do, they must take others into account.

