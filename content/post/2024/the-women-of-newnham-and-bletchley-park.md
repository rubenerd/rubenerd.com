---
title: "The Women of Newnham and Bletchley Park"
date: "2024-03-18T08:25:02+11:00"
year: "2024"
category: Thoughts
tag:
- cryptography
- history
location: Sydney
---
This exhibit by [The University of Cambridge's Newnham College](https://newn.cam.ac.uk/event/newnham-and-bletchley-park-exhibition/) made it across my Mastodon timeline this morning.

> More than 70 students and alumnae were secretly recruited for World War Two codebreaking work at Bletchley Park, thanks partly to the personal connections of three Newnham women: Alda Milner-Barry, former Principal Pernel Strachey and Ray Strachey.
>
> The role of the Cambridge College in the vital work at Bletchley Park has emerged slowly and piecemeal through historical research, as the women involved were sworn to secrecy, the need for which continued after the war ended when Cold War tension followed the armed conflict.
> 
> Over 70 Newnham alumnae were involved in the UK’s World War Two signals intelligence operation, most of them in the wooden huts and brick blocks of Bletchley Park. In spartan conditions, the predominantly female workforce processed thousands of encrypted enemy messages, contributing to vital intelligence that aided the Allied war effort. Newnham women were represented in most key areas of Bletchley Park’s work.

This made my jaw drop. It's been this long, and we're only learning of their critical contributions now?

I also didn't know [Bletchley Park](https://bletchleypark.org.uk/) was an independent museum now too. Clara and I will have to make our way there one day.

