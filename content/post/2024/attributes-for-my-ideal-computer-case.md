---
title: "Attributes for my ideal computer cases"
date: "2024-06-30T10:54:14+10:00"
year: "2024"
category: Hardware
tag:
- 3d-printing
- design
- pie-in-the-sky
- retrocomputing
location: Sydney
---
A few bloggers and YouTubers have written about what they'd want in their dream computer case, so I figured why not throw my own into the mix? There are some <a href="#common-features">common features</a>, but I've broken them down into three distinct categories: the <a href="#the-home-server">home server</a>, the <a href="#the-workstation">workstation</a>, and the <a href="#the-retro-computer">retro computer</a>.

The hope is that one day I may posess the skills and creativity to model such cases in 3D software. That way, I could get them machined or printed. R-right?


<h3 id="common-features">Common features</h3>

* They have to be **beige**. *Of course*, what else would they be?

* **Captive thumb screws** for all side panels.

* A **minimal design.** No glass panels, RGB, boy racer styling, or personality. No, wait. It'd be wonderful if they could somehow all have lines like Apple's classic Snow White design language.

* **Sound-dampening panels**, if they could be accommodated without sacrificing airflow.

* **A bank of status LEDs**. Small, unobtrusive lights like old ThinkPads that could be mapped to CPU, network, and storage activity; power status; and more.


<h3 id="the-home-server">The home server</h3>

Assuming I wanted to maintain a pedestal-style case in lieu of a small server rack, these would be my technical requirements:

* A slew of **ventilated 5.25-inch drive bays**. Ideally these would constitute the entire front of the case. This would give me the flexibility to mount whatever I want, from trayless or caddy-based hot-swap drive bays, LTO tape drives, or optical disc burners/readers.

* **120mm fan-mounts behind the 5.25-inch bays**. This would help pull air through the drive bays, which would remove the need for those tinny fans typically mounted to hot-swap drive bays. I recall some of those later SPARC towers had such an arrangement, but I could be wrong.

* A **dual-chamber internal design** with space for a primary MicroATX board, and a secondary MiniITX. This would be great for running a home lab where you want to do cluster testing, or even have disparate architectures in the one box. Bonus points for a conduit to attach cables that span across both boards, like Ethernet, RS232, or InfiniBand.

* **A dual-power supply design** with space for ATX for the primary board, and maybe SFX or smaller for the MiniITX board. I'd rather not use server power supplies, because their tiny fans would be noisy.


<h3 id="the-workstation">The workstation (and game machine)</h3>

I've only ever used MiniITX for desktops since about 2014, and they've worked really well. There may already be cases like this:

* A small case, but **not too small**. A Clara-sized case, in other words. Clara if you're reading this, that was a typo. Ideally something to fit a MiniITX board and a 3-slot graphics card.

* Another **mini PCI slot somewhere**, where I could mount another card that isn't a GPU, like a decent sound card. I currently have a PCIe riser connected to the onboard normally reserved for Wi-Fi.

* At least one, ideally two, **5.25-inch drive bays**. This would be useful for one of those integrated slimline optical drive and 3.5-inch drive enclosures, which I could use for a card reader for digital cameras. Alternatively, it could include a beige flap to cover up a black optical drive in case a beige one can't be acquired.

* Space for **two 3.5-inch drives**. These could always have SSDs mounted as well. Bonus points if they're somehow incorporated into the case with a trayless popout mechanism.

* Front and back **140mm fan intakes**. They may be able to just stretch from one side of the case to the other.

* **Front-mounted ports** for USB-C, and separate headphone and line input jacks. The wires for the latter two would be shielded in their own conduit to prevent distortion and interference, which seemingly modern PC cases don't care about.


<h3 id="the-retro-computer">The retro computer</h3>

Wait, a *modern* retro computer case? Why not just get an old one? Well for one, is a phrase with three words. Finding the one you want in good condition might be hard.

This one is a bit more complicated, but here's what I'm thinking:

* An open back panel, in which **custom port layouts** and **card slots** can be printed and mounted. Putting an IBM 5150 with just an AT connector and 5 ISA slots? Sure! What about a Commodore 128D? No problem, the ports are all in the right place, and it can even include a cartridge expander so you can connect multiple C64 and C128 carts at at time.

* **Standoff mounting points** for a range of motherboard types. None of those awful plastic tab things!

* An **internal bay for mounting upscalers and video converters**, with a lead that goes to the 5 V rail on the power supply. No more messy cables, or boxes stuffed behind monitors, just a single VGA or HDMI output.

* **Case recesses for attaching feet** at the bottom for a tower case, or on the side for a classic flat desktop. Maybe they'd be incorporated into the design somehow, so they don't look out of place.

* A few **seven-segment displays** for showing turbo status, processor speed, drive activity, POST codes, a crude version of Pong, or any other information you'd want.

Are all the features of these cases feasible? Probably not. Do some contradict others? Sure! But one can dream.
