---
title: "Fixed a few bugs"
date: "2024-04-15T08:21:44+11:00"
year: "2024"
category: Internet
tag:
- bugs
- weblog
location: Sydney
---
In no particular order:

* Is a phrase with four words. Mmm, that's quality humour.

* There were a few elements not showing up properly in dark mode on single pages. You might need to force a refresh to get the most recent CSS.

* Some Dublin Core metadata wasn't quite correct.

* I didn't have a cup of coffee in my hand, or on the desk in front of me. This has been rectified.

* For some reason a few URLs were injected as plain text in some of my recent Melbourne posts. I've removed them.
