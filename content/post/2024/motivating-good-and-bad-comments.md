---
title: "Motivating good and bad comments"
date: "2024-11-27T09:15:27+11:00"
abstract: "People left me comments on Mastodon about why rude feedback is always forthcoming."
year: "2024"
category: Internet
tag:
- feedback
location: Sydney
---
Friend of the blog [Josh](https://social.chinwag.org/@josh) has a locked account, so I've paraphrased:

> I've wanted to leave comments on your posts before, but I've probably only been able to muster it 10% of the time. It floors me that some people want to take the time to do this via email to sent something rude, rather than positive.

[And mms](https://mastodon.bsd.cafe/@mms/113543415037381054)\:

> how many readers do you have?! Such a quick angry feedback is something I can not imagine :-)

[And Marcel](https://mastodon.social/@herrbischoff/113543880155337569)\: 

> Don’t underestimate the enormous entitlement issues present, arising from insecurity, confusion and largely missing common sense.

People are motivated to correct things; cue the xkcd "someone is *wrong* on the Internet" reference. And while it takes fewer muscles to smile than to frown, it takes fewer keystrokes to be a dick (maybe?).

I suppose it's the same phenomena online review sites face. People are motivated to leave a bad review when something didn't meet their expectations, or they feel slighted. Conversely, a positive outcome is expected, so leaving a positive review feels less urgent.

I'm lucky that I still get more positive email, but unfortunately it's in my personality to remember the bad ones! In the words of the *Sunscreen Song*:

> Remember compliments you receive;   
> Forget the insults.   
> If you succeed in doing this;   
> Tell me how.

