---
title: "Stein’s Law"
date: "2024-08-16T09:43:51+10:00"
year: "2024"
category: Thoughts
tag:
- economics
- philosophy
location: Sydney
---
[Bob Wyman](https://mastodon.social/@bobwyman/112968594497431232) linked to [this policy brief](https://www.cepweb.org/if-something-cannot-go-on-forever-it-will-stop/ "Council of Economic Policies") by William White published in 2021, in which he quotes Herb Stein, Senior Fellow at the American Enterprise Institute:

> “If something cannot go on forever, it will stop.”

As William writes, it's such an *obvious* statement, but one that takes on grave significance when you realise it.

