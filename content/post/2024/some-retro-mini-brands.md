---
title: "Some fun little retro Mini Brands"
date: "2024-08-25T09:26:04+10:00"
thumb: "https://rubenerd.com/files/2024/retromini@1x.jpg"
year: "2024"
category: Hardware
tag:
- nostalgia
- pointless
- retrocomputers
location: Sydney
---
I'm conflicted by *Gashapon* capsule machines and their various offshoots, for a whole host of reasons I won't get into here. Here comes the proverbial posterior prognostication: *but...*, Clara and I saw there was a "retro" *Mini Brands* series, so we got a couple for fun.

*Mini Brands* are one of many capsule manufactuers that sell tiny analogues of real world devices and foods. Unlike a traditional capsule machine, each sphere contains *five* paper bags with the novelty toys. Romanian/American [GB Kuhleen](https://www.youtube.com/channel/UCwvnWhROLSymlRgspkKBRuQ) has an entire channel dedicated to opening and sharing these miniatures, as well as customising and making artwork from them which looks like a lot of fun.

<figure><p><img src="https://rubenerd.com/files/2024/retromini@1x.jpg" alt="Photo showing the various Mini Brands items, described below. So cute!" srcset="https://rubenerd.com/files/2024/retromini@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

Clara and I got a couple of 35 mm films, a 5.25-inch floppy disk, a VHS tape, some Pez, and a bunch of other goodies! Given how many items are in this "retro" series, I couldn't believe our luck. The [Proton Saga Hotwheels car](https://rubenerd.com/some-proton-saga-hot-wheels/) is for scale.

Maybe Clara's Bear Cats will be able to use them :).
