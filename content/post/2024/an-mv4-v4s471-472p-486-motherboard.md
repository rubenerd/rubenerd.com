---
title: "An MV4-V4S471/472P 486 motherboard from 1995"
date: "2024-12-22T08:45:01+11:00"
abstract: "A VL Bus board with UMC Green CPU support in great condition, save for a bit of usual Varta fun."
year: "2024"
category: Hardware
tag:
- i486
- retrocomputing
- v4s471
- vlb
location: Sydney
---
*(I was saving this post for when I powered on the board and ran it through its paces, but I posted about it on Mastodon yesterday and got some interest. Consider this a part one).*

The 80486 is *unashamedly* my favourite computing platform. I love every retrocomputer in existence&mdash;which has proven dangerous&mdash;but you'll always have that place in your heart for your first machine. Some of my most treasured memories are basking in the glow of that 14-inch CRT on a weekend as my dad showed me how to use DOS and launch Commander Keen. Amigas and Macintoshes were more capable (heck, even Atari machines came with better stock audio capabilities), but that 486 SX was *ours*.

I also find it fascinating retrospectively. 486 boards had no fewer than five different card slots, from your typical 8/16-bit ISA and PCI, to the [backwards-compatible EISA and Frankenstein VESA Local Bus](https://rubenerd.com/between-isa-and-pci-we-had-vlb/ "Between ISA and PCI, PCs had EISA and VLB"). CPUs from other manufacturers were cloned, reverse engineered, and manufactured, some of which landed their companies in [hot water](https://en.wikipedia.org/wiki/UMC_Green_CPU "The UMC Green CPU"). Intel's failure to trademark a number resulted in their release of the Intel DX4 CPU without the 486 moniker, and their decision to dub their 586-generation CPU the *Pentium*. There was even a controversy around [fake cache chips](https://rubenerd.com/fake-486-cache-chips/ "Fake 486 cache chips").

Into the fray came this Baby AT motherboard, which I picked up a few months ago. After bouncing around various shipping depots in the US and Australia, it arrived yesterday and I was over the moon:

<figure><p><img src="https://rubenerd.com/files/2024/v4s471@1x.jpg" alt="" srcset="https://rubenerd.com/files/2024/v4s471@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

This is the `MV4-V4S471/472P` (gesundheit), variously referred to as the `QDI V4S471` and the `QDI SIS486 P3` according to [The Retro Web](https://theretroweb.com/motherboards/s/qdi-v4s471-472p "The Retro Web: QDI V4S471/472P"). There are otherwise no markings on the front or back of the board indicating the manufacturer, which if you'll pardon the pun is *on brand* for this generation of hardware.

This board isn't unusual, but it does have a few unique capabilities that made it compelling for my specific use case. First, it has VESA Local Bus slots, which I'm going to use for testing some specific SCSI and VGA cards (two act as bus masters). It has an integrated SIS chipset, which are among the better ones in my experience. It has four banks of 72-pin FPM RAM in lieu of SIMM or SIPP sockets, which will make populating RAM easier and cheaper. It also supports both 5v and 3.3V CPUs without needing to use rarer Overdrive CPUs or expensive voltage regulating boards. It basically does everything.

Most interesting of all, the board has jumper and BIOS support for the UMC Green CPU family. Once I read the manual on The Retro Web and saw them referenced in the table of supported CPUs, I lunged! This means I'll be able to power up my UMC Green 40 for the first time.

<figure><p><img src="https://rubenerd.com/files/2024/bad-varta@1x.jpg" alt="" srcset="https://rubenerd.com/files/2024/bad-varta@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

In terms of condition, it's basically immaculate with no dust whatsoever. It did come shipped with the dreaded Varta battery of death, but this twisted off with almost no effort and with only cosmetic board damage. I did some continuity tests around the battery, and all the traces passed. I'll still be spending this afternoon cleaning it up with some vinegar and IPA though; I wouldn't want any of this spreading. I've also bought another 4-pin header and a 3× AA holder from Jaycar which I'll write about building and installing on the board's external battery header.

I'll post [more about this](https://rubenerd.com/tag/v4s471/ "Posts tagged with v4s471") in the future! In the meantime, you can also check out the [evolving page on my retro site](http://retro.rubenerd.com/v4s471.htm) if you're interested.
