---
title: "No, AI won’t solve burnout!"
date: "2024-04-13T10:21:36+10:00"
year: "2024"
category: Ethics
tag:
- ai
- mental-health
- work
location: Sydney
---
I subscribe to a couple of "channel" focused IT publications in Australia, because I work at a company that only deals [B2B](https://en.wikipedia.org/wiki/Business-to-business "Wikipedia article on business to busienss"). I didn't realise until I started working there that "channel" referred to such an arrangement; I thought it was a passage of water permitting the traversal of watertight vessels, or something your TV comes with that you ignore after you install Plex.

One publication was so proud of a specific report they put together with a managed-service provider, they sent a dedicated invite to it:

> The workforce – lets face it – is exhausted. Burnout, quiet-quitting, and a looming productivity crisis are signs of an overworked culture of the brink, with urgent ramifications for the success, or failure, of technological investment.

I'll pretend "technological investment" said "people's mental health", but you do you.

> AI is expected to lift the burden,

Okay, I'm going to stop you right there. Did Jim Cramer write this!?

*Deep breaths. Where do I even start? Think of that view of Lake Biwa from Ōtsu. You'll be fine.*

...

You know how there are certain phrases that immediately disqualify you as someone earnestly engaging with a topic? *Vaccines cause autism* is one. *Ruben needs Atelier Ryza figs* is another.

The idea that AI will, in *any* way, "lift the burden" on such mental health issues is so preposterous, so unhinged, so utterly disconnected from reality, that I physically can't bring myself to think about it without fits of laugher at the sheer, uncut hubris.

*(I swear this is true, when I was drafting this post yesterday, I was giggling so much the barista asked me what was so funny. I said "techbros" which I'm not sure he understood, but he shared a grin).*

I was going to link to articles discussing how gains in efficiency clearly haven't resulted in a Jetsonian future of less work, and that AI has already [*increased* my daily workload](https://rubenerd.com/ai-security-companies-spamming-abuse-lines/) in at least four ways now. But even engaging in that discussion feels like a concession. There are certain perspectives that are so beneath contempt, so patently ridiculous, they don't dignify a response.

Instead, I wanted to call it out as an example of the kinds of things people discussing AI ethics, mental heath, and the future of work are battling. There is important progress being made, but the fact it's drowning in articles like this is proof large swaths of our industry have a long way to go before they graduate from comedy. Don't get me wrong, being able to laugh at oneself is a sign of mental maturity and resilience. But in this case, it's lacking that key requirement of self-awareness.
