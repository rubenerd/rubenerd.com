---
title: "Cover letters"
date: "2024-11-06T09:07:09+11:00"
year: "2024"
category: Thoughts
tag:
- life
location: Sydney
---
For someone living the *digital lifestyle* and working in a *paperless office*, I still print and post a *lot* of documents. And yes, [also fax](https://rubenerd.com/my-life-was-directed/ "On life being directed"), which a few of you were mortified to read about. The latter is only for fun; the printing is because I demand delivering hardcopies of important forms like contracts in lieu of whatever fancy online form or email address a company wants me to use. I work in IT, and therefore don't trust these things! Maybe it'd only be marginally safer in the post, but I'm still more comfortable.

The downside to posting random forms is that the recipient may not have sufficient context to know the purpose of the form, at least at first glance. An email can include an entire chain of thought, and might be automatically processed. You want your paper document to be as easy for the recipient to understand and process as possible, otherwise you're just adding *even more* time over what could have been near-instantaneous with an email, or a web form the designers claim has "[military-grade encryption](https://rubenerd.com/encryption-spam/)".

Clara and I sent cover letters with everything, even if the explanation is only a line or two. They're useful when the form you're sending didn't have a comments field, or we need to provide additional information than what the form asked. The layout is your bog standard business letter that looks like this:

<blockquote style="clear:both">
<p style="float:right;"><strong>Ruben Schade <span style="font-style:normal;">☕️</span> and Clara Tse <span style="font-style:normal;">🧋</span></strong><br />Our Address<br />Our Numbers</p>

<p style="clear:both;"><strong>Recipient Name</strong><br />Recipient Address<br />Recipient Numbers</p>

<p>Dear Recipient,</p>

<p style="text-align:center;"><strong>Subject line</strong></p>

<p>The body of the letter, as well as the hands and feet.</p>

<p>Regards,</p>

<p>Signatures<br />Date</p>

</blockquote>

We've literally had agents tell us they picked us to rent a property back in the day because we "put in the effort" to do this. That I think says more about how estate agents are swayed by silly and superficial things, but we perhaps we already knew that. We've even had a financial company tell us a form we sent wasn't entirely correct (despite them sending us that exact form for that exact requirement), but that the additional details in the signed cover letter allowed them to process it without going back and forth again.

We send them with applications, change of address forms, legal threats to our enemies who've done us wrong (cough!), you name it. Well technically its *us* naming it. With a cover letter.

I don't have much advice for younger people, but sending cover letters is an easy one to impart. Yes, they're probably not necessary in the majority of cases, but if including one puts you even slightly ahead, and makes things easier for the recipient to understand, why not? It's five minutes of work for potentially a big upside.
