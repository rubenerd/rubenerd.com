---
title: "Finally got my ultimate retro KVM setup working!"
date: "2024-03-17T18:23:24+11:00"
thumb: ""
year: "2024"
category: Hardware
tag:
- am386
- apple-ii
- commodore
- nec-apex
- pentium-mmx
- retrocomputing
location: Sydney
---
It took almost a year of tinkering, buying parts, testing, configuring, fixing, cursing, and shouting with excitement, but I now have a KVM setup that finally works, across two decades of computer history from a Commodore VC-20 to a Dell Dimension Pentium III!

We start with this assortment of parts to take signals and convert them to VGA for my KVM, inspected for quality by one of Clara's Prince Cats. Some of these ended up being replaced or not used.

<figure><p><img src="https://rubenerd.com/files/2024/kvm-parts@1x.jpg" alt="Photo showing the parts listed below." srcset="https://rubenerd.com/files/2024/kvm-parts@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

Clokwise from left to right we have:

1. [The RetroTINK-2X-Pro](https://www.retrotink.com/product-page/retrotink-2x-pro), with a Belkin HDMI to VGA adaptor. This box of wonders converts and upscales the S-Video from my 8-bit Commodore machines with zero configuration and fuss. The Belkin converts this to a crisp VGA signal for the KVM. 

2. [A DB13W3 connector](https://rubenerd.com/the-sun-sparcstation-5s-13w3-connector/ "The Sun SPARCStation 5’s 13W3 connector"). This converts the Sun TurboXGX frame buffer card on my Sun SPARCStation 5 to VGA.

3. [The GGLabs CGAtoRGBv2](https://www.tindie.com/products/gglabs/cga2rgbv2-digital-to-analog-rgb-for-mdacgaega/). This converts the EGA signal from my Am386 tower to 15 KHz VGA, which surprisingly my VGA LCD accepts. It might also work with my Commodore 128's 80-column mode eventually, if I get its VDC working.

4. [A cheap S-Video and Composite to VGA converter](https://www.jaycar.com.au/composite-video-rca-svideo-to-vga-converter/p/XC4906) (retired). I used this with the Apple //e before I got the ∀2 Analog VGA card. It was fine.

5. [Luis Antoniosi's MCEtoVGA converter](https://github.com/lfantoniosi/mce2vga) (retired). Worked fine for EGA, but the GGLabs card has no artefacting or fuzziness.

6. [The ∀2 Analog](https://www.v2retrocomputing.com/analog) (not pictured). This card generates a VGA signal from any slot on an Apple ][, including my //e Platinum. 80 column colour is not only feasible on this machine now, but it looks stunning!

The next piece was my handsome beige KVM, which I got on eBay for a steal because it had no cables; something I [later came to regret](https://rubenerd.com/always-buy-kvms-with-cables/ "Always buy old KVMs with cables")! But I finally have cables now.

<figure><p><img src="https://rubenerd.com/files/2024/kvm-unit@1x.jpg" alt="Photo showing the Master View KVM switch above the NEC APEX 486." srcset="https://rubenerd.com/files/2024/kvm-unit@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

With these connectors, we now have the KVM ports set up like this:

1. **Am386 EGA** &rarr; CGAtoRGBv2 &rarr; VGA
2. **NEC APEX 486 DX2** &rarr; VGA
3. **DIY childhood P1** &rarr; VGA
4. **Dell Dimension 4100 P3** &rarr; VGA
5. **Sun SPARCStation 5** &rarr; DV13W3 adaptor &rarr; VGA
6. **8-bit Commodores** &rarr; RetroTINK &rarr; HDMI &rarr; VGA
7. **Apple //e Platinum** &rarr; ∀2 Analog &rarr; VGA
8. *Reserved for another upstream KVM? Gulp*

And it works! I can press the button on the KVM, or invoke a key command, and jump between DOS 3.3 on my Apple //e, to NetBSD on the SPARCStation, then across to GEM on the 386, then check where my BNSF GP38-2 is up to in Train Simulator on my Dell.

It's a hornet nest of cables, and I can't tell you how happy I am now! Well, I guess I just did! Now it's time to [update Sasara.moe](http://www.sasara.moe/retro/) with all this stuff.

<figure><p><img src="https://rubenerd.com/files/2024/kvm-final@1x.jpg" alt="The Apple //e later in the evening booting into DOS 3.3 via the V2 Analog card and the KVM." srcset="https://rubenerd.com/files/2024/kvm-final@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

### FAQs

* **Why did you do a lossy conversion from HDMI to VGA?** Beacuse the RetroTINK outputs HDMI, and my KVM uses VGA.

* **Why use a VGA KVM, not DVI or HDMI?** Because VGA is the highest common denominator across all these machines, and my beige NEC LCD uses VGA.

* **Surely your Dell has DVI?** Yes, but see above.

* **Which 8-bit Commodore machines do you use with this setup?** Right now it's a VC-20 [sic], 64C, C128 (40-column mode only, the 80-column VDC circutry is currently borked), C16, and a Plus/4. Clara's and my shared retro server [Sasara.moe](http://www.sasara.moe/retro/) has the full list. The 64C is my daily driver, with the others in a glass cabinet to the side where I can easily access them and swap them out (unlike the massive Apple //e)!

* **I can never get these converters! How did you?** It can be tricky, they come in and out of stock constantly. The best thing to do is to subscribe to Tindie sellers so you get an update as soon as they're available, and keep some money budgeted to the side so you can pounce.

* **You should use $FOO or do $BAR instead.** Where were you yesterday?

* **What did you use for the keyboard and mouse?** I used AT to PS/2 for the keyboards, and serial to PS/2 for the mouses where necessary. This did rule out having a more modern optical mouse unfortunately, but I have nostalgia for that Microsoft Dove soap bar mouse anyway.

* **Have you tried the [RGBtoHDMI](https://github.com/hoglet67/RGBtoHDMI)?** I've heard great things, but this setup works for me.

* **Can you help me troubleshoot my own complicated video setup?** In the words of a ship's rigger, I'm a frayed knot. This was all trial and error, mostly error, and I'm not in a hurry to repeat the experience.

* **What VGA cables did you use?** I ended up finding some second-hand Belkin KVM cables, and a high-quality shielded cable to go to the monitor.

* **What about your other machines?** I have some other late 1990s PCs, but their function mostly overlaps with the Dell that has the best specs. At some point I'll wire them into this spaghetti, but right now they're museum pieces until I get more space.

* **Was it worth it?** If you're in a tiny apartment like me, its the difference between having retrocomputing as an accessible hobby, and not. If I had a decent-sized place with a hobby room, I'd much rather have dedicated setups for each machine, or at least each generation of machine.

* **Do any of Clara's Prince Cats help with configuring the machines too?** As a matter of fact, yes.

<figure><p><img src="https://rubenerd.com/files/2024/kvm-inspection@1x.jpg" alt="More of Clara's Prince Cats inspecting the VC-20." srcset="https://rubenerd.com/files/2024/kvm-inspection@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>
