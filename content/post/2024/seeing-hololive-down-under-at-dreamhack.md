---
title: "Seeing Hololive Down Under at Dreamhack"
date: "2024-04-28T08:44:01+10:00"
thumb: "https://rubenerd.com/files/2024/holo-dreamhack@1x.jpg"
year: "2024"
category: Anime
tag:
- hololive
- japanese
- music
- pavolia-reine
location: Sydney
---
What can I say, this was a dream come true! We got into Hololive during Covid, so to see them iRL (in a matter of speaking) was incredible and a bit unbelievable.

<figure><p><img src="https://rubenerd.com/files/2024/dreamhack-poster@1x.jpg" alt="The official Hololive Dreamhack key visual for the event." srcset="https://rubenerd.com/files/2024/dreamhack-poster@2x.jpg 2x" style="width:500px; height:281px;" /></p></figure>

This was their first performance in Australia, and they *slayed* it. Bae hails from here, and got emotional talking about performing at home. She also teased/trained Cali how to say *Melbourne* (for my American readers, it's pronounced *Mell-borne*, as in Jason Bourne, cough).

Clara and I have tried a couple of times to go to Japan for one of the official Hololive events, but circumstances and life have always got in the way. So when we heard there was going to be a smaller event in Australia that was only an hour flight away? We didn't have a choice!

<figure><p><img src="https://rubenerd.com/files/2024/holo-dreamhack@1x.jpg" alt="The stage at Dreamhack showing the Hololive performers before the event." srcset="https://rubenerd.com/files/2024/holo-dreamhack@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

This was also my first "Japanese idol" concert, with the glow sticks and nerds dancing awkwardly in their chairs. People had posters, plushies of their favourite characters, it was such a different vibe to anything I'd ever experienced before. There were teenagers, boomers, and everyone in between in cosplay vaguely singing along in broken Japanese to songs we've all been listening to now for years. It felt inclusive and fun, and I loved it.

It's also a bit weird to admit, but this is also one of my first events since I started dealing with this latest bout of anxiety. I legitimately thought I'd be on the flight from Sydney to Melbourne before being gripped by it, and rushing to the front of the plane to ask the pilots to turn around. But Clara held my hand throughout, and I ended up having an amazing time.

To our local Bae, my oshi Reine, Cali, Ollie, Towa, and Marine, thank you! You were all, to use the local Australianism: すごい! Wait.
