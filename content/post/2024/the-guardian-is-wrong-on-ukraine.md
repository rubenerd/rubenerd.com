---
title: "The Guardian is wrong about my Ukrainian friends"
date: "2024-03-06T14:02:36+11:00"
year: "2024"
category: Thoughts
tag:
- europe
- politics
- ukraine
location: Sydney
---
I link to The Guardian semi-regularly here, but they're not immune from publishing codswallop. I'd know; have you read some of what *I* write?

Simon Jenkins writes poignant articles about public health, and I thoroughly enjoyed his history book on the Celts. But then he turns around and <a href="https://www.theguardian.com/commentisfree/2025/mar/05/nato-ukraine-russia-germany-military-leak#comment-166774805" rel="nofollow">sneezes this</a> into my RSS reader:

> While it should sustain its logistical support for Ukrainian forces, [Western Europe] has no strategic interest in Kyiv’s desire to drive Russia out of the majority Russian-speaking areas of Crimea or Donbas.

Mr Jenkins... come here for a second please? *No* strategic interest? *None whatsoever?* Are you gunning for a spot at The Onion or Daily Mail? What in the ever-loving `fsck(8)` are you talking about!?

He's regularly [debunked](https://www.theguardian.com/world/2022/jan/25/ukraines-independence-must-be-supported-by-britain "Ukraine’s independence must be supported by Britain") by [actual experts](https://rubenerd.com/erwin-knolls-law-of-media-accuracy/ "Media accuracy on topics with which you’re familiar") and people with firsthand experience, but clearly none of it has resonated. What was it [Upton Sinclair said](https://quoteinvestigator.com/2017/11/30/salary/ "It Is Difficult to Get a Man to Understand Something When His Salary Depends Upon His Not Understanding It") in 1934?
