---
title: "Our personal home database"
date: "2024-12-08T09:04:32+11:00"
abstract: "We just run Postgres and pgAdmin. I can’t recommend this to everyone, unless you’re a bit weird."
year: "2024"
category: Software
tag:
- databases
- home
location: Sydney
---
[Abigail](https://www.akpain.net)&mdash;who has an awesome site, and who encourages you to [have your own space as well](https://www.akpain.net/blog/the-modern-web-sucks/)&mdash;emailed asking what database system we used to organise stuff, as I described in my ill-advised [MAC address filtering](https://rubenerd.com/using-mac-address-filtering-in-2024/) post:

> Clara and I maintain a personal DB which includes all sorts of data, from budgets to media collections. One of the tables tracks our assets, including serial numbers, dates of purchase, warranty information, hostnames, ZFS pools, and… MAC addresses.

I'll admit we're doing nothing special, it's literally a PostgreSQL database hosted on a NetBSD box at home, with pgAdmin as the frontend on our machines (thanks to Adam@ for maintaining). Until this year it was a MariaDB/MySQL server, and long before that it was a SQLite3 file sync'd with Dropbox. That last one still makes me nervous thinking about even now.

I've always had a database of some sort to organise my life. I was a DBA briefly, and while it's *ridiculous* overkill for what we use it for, it does come with some nice benefits:

* We don't have to worry about syncing or backups, because the database is always consistent, current, and replicated. We're rarely editing something at the same time anyway, but having that safeguard is nice.

* Like our Minecraft server and other hosted stuff, it's accessible everywhere over the same Wireguard VPN, which keeps things simple. Well, "simple". I can tinker on the phone on a crowded train, or a laptop.

* It's easy to script against, which is how I maintain the MAC address filters I discussed. It's our "source of truth" for things.

* It's also a useful testbed for learning, because I teach myself things by designing, tinkering, an upgrading over-engineered stuff for home. Labs are always more useful if you have real data and use cases, rather than synthetic tests.

* In a pinch I can drop to `psql(1)` and just run rote raw commands that I want.

I can't say I can recommend this to everyone; it's a bit silly. Actually scratch that, it's *more* than a bit silly. But if you're weird like me, go for it!
