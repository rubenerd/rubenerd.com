---
title: "A letter to Jim Kloss, a year on"
date: "2024-05-14T09:11:16+10:00"
year: "2024"
category: Thoughts
tag:
- jim-kloss
- whole-wheat-radio
location: Sydney
---
> Jim!
> 
> I hope this finds you well, and you're settling into your new digs.
>
> My mum Debra and uncle Dave should have introduced themselves by now. I think you'll get along with them like a Wheat Palace on fire; they were folk and jazz musicians, and heard a lot about you over the years. I see them in some alternate universe setting up a WWR branch in rural coastal Australia for house concerts and art exhibitions, where the music and coffee flow, and everyone is healthy and happy.
>
> Ah look at me, rambling again. What have you been keeping busy with?
>
> Things here have been on the up here since we last spoke. Clara and I are looking to buy our first house, which is exciting and terrifying (yay Australia's housing market)! Remember that advice you gave me at that cafe in Philly about setting up automatic investments with amounts small enough to ignore? Those now constitute a significant chunk of the deposit. Talk about setting us up, quite literally.
>
> You've also helped a lot in a few ways that are hard to describe. I was in a few difficult client and family meetings recently, and I had to be assertive to get my position across. I know, me!? Turns out, I just had to think about how you would react if you were sitting there with us. After one especially thorny chat I could see a shadow move besides me, and I swear I could see you nodding your head and smiling as you did when we'd be on a video chat together and something clicked. I could have left there a nervous wreck, but instead I thought "yeah, we did it. We really did it".
>
> I suppose what I'm trying to say: you're still an indelible source of strength and moral direction in everything I do. You live on through the actions of everyone who's lives you influenced, not least mine.
>
> <span style="font-style:normal;">☕️ 🌲 🌷</span>
> 
> I won't mince words, it's been tough thinking about how we'll never be able to catch up again. I know Esther and the rest of your family miss you dearly too. It's a testament to the kind of person you were that you're remembered so fondly by so many people, from those in your inner circle, to some random schmuck online who first found you via your audio magazines.
>
> If you're able to set your sights on North America tonight, I know Esther and a lot of your Alaskan friends are holding a celebration and remembrance for you. I'm sure there will be some wonderful stories.
> 
> I will say, you're a bit of a showoff for organising that global Aurora light show to coincide with it though. But then, you were always a wizard at pulling rabbits out of hats, electronic and otherwise. You could say there's been `NOCHANGE` (the BBS you ran, for the others who read this. No, Jim didn't sanction that joke).
>
> **AND THE BEES ARE FLYING AROUND NEIL ARMSTRONG!**
>
> Lots of love from myself, Clara, and my sister Elke. You're always in our thoughts.
> 
> Your friend,   
> Ruben
