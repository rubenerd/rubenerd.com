---
title: "The 003rd post with three-digit integers"
date: "2024-12-18T13:42:41+11:00"
abstract: "I posted about an ad site with 912 partners, and the Glasgow Subway’s 128th anniversary. This is the 003rd!"
year: "2024"
category: Thoughts
tag:
- numbers
- pointless
location: Sydney
---
The last two posts on this site were as follows:

* [Our 912 partners](https://rubenerd.com/our-912-partners/)
* [128 years of the Glasgow Subway!](https://rubenerd.com/128-years-of-the-glasgow-subway/)

This is now the 003rd consecutive post to include a three-digit integer somewhere in its title. *Hot ziggity*.

Could I have said the *third* post instead? Absolutely. Was this post pointless? Almost certainly. Did I post it anyway? I mean, yes, you're reading it now.

Sometimes I have to remind you all that I provide valuable insights here, otherwise you may forget.
