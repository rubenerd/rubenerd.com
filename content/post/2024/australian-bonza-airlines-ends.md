---
title: "Australian Bonza Airlines ends"
date: "2024-05-01T09:47:46+10:00"
year: "2024"
category: Travel
tag:
- australia
- aviation
- business
location: Sydney
---
Outlets from [ABC News](https://www.abc.net.au/news/2024-05-01/bonza-airline-administrator-hall-chadwick-fleet-grounded/103786964 "Bonza airline's administrator Hall Chadwick says fleet grounded for coming days") to [The Guardian](https://www.theguardian.com/australia-news/2024/may/01/grounded-bonza-set-to-join-long-list-of-failed-australian-airlines) are reporting on the collapse of Bonza, the upstart Australian airline that took to the skies a year and a half ago. [Ian Verrender](https://www.abc.net.au/news/2024-04-30/bonza-doomed-to-failure-from-the-start/103785860 "Bonza doomed to failure from the start, just like so many Australian airlines before it") had a poignant line:

> Former Qantas boss Geoff Dixon used to regale clients and investors with a standing joke every time another domestic airline went under. The Australian market, he reckoned, was only big enough for one and a half airlines.

Australia is a weird market for aviation. On the one hand, we have a vast country that even high-speed train advocates like me have to concede wouldn't work for most city or town pairs. It's significantly emptier than Europe, mainland China, or North America, so routes are thin and far apart. But while we have a tiny population, we also have the Melbourne-Sydney corridor, one of the most travelled and profitable routes in the world.

Journalists narrowed in on Bonza's use of larger single-isle aircraft to regional centres as a big reason for their downfall, when airlines like Rex uses turboprops. That may be true, but I suspect its more to do with the squeeze budget airlines have faced after demand picked up after 2021, and the cost to lease aircraft skyrocketed. Petter over on [Mentour Pilot](https://www.youtube.com/channel/UCwpHKudUkP5tNgmMdexB3ow) has discussed this a few times.

I never flew Bonza, but I saw their purple logos everywhere at domestic airports in Sydney and Melbourne. Air travel is booming again, so I hope their staff are able to find work quickly.
