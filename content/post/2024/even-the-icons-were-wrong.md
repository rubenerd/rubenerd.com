---
title: "Even the icons were wrong"
date: "2024-06-19T22:07:08+10:00"
thumb: "https://rubenerd.com/files/2024/wrong-icons.png"
year: "2024"
category: Software
tag:
- apple
- errors
- macos
location: Sydney
---
You know your Mac is borked when even the *icons* aren't right for specific applications in the **Force Quit Applications** window. It's also not correctly showing the frozen application for some reason.

I do like the idea of the Music app being secured by KeePassXC though, that's kinda cute. I can't let anyone know about the weird jazz music I listen to.

<figure><p><img src="https://rubenerd.com/files/2024/wrong-icons.png" alt="Force Quit Applications, showing the wrong icons for various applications" style="width:450px; height:425px;" /></p></figure>
