---
title: "Instructions for things that should be intuitive"
date: "2024-12-30T10:08:57+11:00"
abstract: "If your software, websites, appliances, or bathroom fixtures require instructions for basic functions, that's a failure of design."
year: "2024"
category: Thoughts
tag:
- accessibility
- design
location: Sydney
---
I was in a public restroom washing my hands yesterday&mdash;like a gentleman&mdash;when I realised there were no taps to speak of. The spout protruded from the sink as one would expect, alongside an attractive but over-engineered soap dispenser to the side. But... how was one meant to use it? I'll admit I spent an embarrassing amount of time just staring at this sleek but confounding contraption. I waved my hands around it, tapped the spout, pleaded nicely under my breath. Nothing.

Eventually, I glanced up and saw a small sticker above the basin that had clearly been placed there after the fact. It read "hold hand under tap for 3 seconds". Sure enough, holding my hands there for that weirdly extended period was sufficient, and water was delivered henceforth. Sure, it deactivated after a few seconds for no reason, requiring regular waving to turn it back on, but at least I could make something work.

*(As an aside, isn't it wild that we're told it should take minutes to wash our hands, yet public taps routinely deactivate in seconds? Why do we accept this? Why is this permitted? Who signed off on it)!?*

It struck me&mdash;as I stood there drying my hands under a fetching Mitsubishi Air Towel&mdash;that we've been making taps for hundreds of years. They're an understood interface. Sure, we've merged hot and cold taps into the one with a temperature gradation, and moved from two spouts to one, but the fundamentals remain the same. We all know how to use them. Or in the case of most people in public, how to *avoid* them, because almost nobody washes their hands properly. And we wonder how disease spreads so quickly.

Anyway, I'm willing to entertain the idea of public restrooms without physical taps. I consider any reduction of contact surfaces during *These Times&trade;* to be a net positive. But if your device requires you to hold your hands under the tap *for three seconds*, two things will emerge:

* Even fewer people will bother to wash their hands. You've created a disincentive right where we don't need one. "Oh, the tap is broken, that's fine, I'm sure wiping my hands on my pants and the bathroom door will suffice".

* You'll need to provide instructions for the few remaining people who *do* want to wash their hands in a responsible manner. That's right: instructions... for a tap!

This leads us to the core issue. I've used taps with sensors that are easy to grok. They activate instantly, have indicator lights or large black lenses that clearly indicate a sensor to a potential operator. *We know how to do this right.* And yet, this tap was designed so badly, it required someone print *instructions* on a ratty label printer label to operate it.

If your design for software, websites, appliances, or even bathroom fixtures require instructions to perform basic functions, that's a failure of design. It's easy to blame the operator or cry out *PEBKAC*, but it's harder to care about how people will use what you develop.
