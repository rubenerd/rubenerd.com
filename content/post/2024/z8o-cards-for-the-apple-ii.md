---
title: "Z80 cards for the Apple //e"
date: "2024-04-08T07:14:05+10:00"
thumb: "https://rubenerd.com/files/2024/softcard@1x.jpg"
year: "2024"
category: hardware
tag:
- apple-ii
- retrocomputing
- z80
location: Sydney
---
I'll admit, I use retrocomputers in *exceeding* boring ways. Everyone I watch and read restores these machines as a means to play games, and it's kind of assumed everyone does. I mostly use them to tinker and explore how legacy word processors, spreadsheets, databases, and Pascal environments worked (and multimedia CD-ROMs, in the case of my old PCs). Before we standardised on what a lot of what those terms mean, home and microcomputer software really were the wild west.

A *big* part of the reason I was obsessed with the Commodore 128, and why Josh from [The Geekorium](https://the.geekorium.au/) graciously gave me his, was because of its third operating mode that would allow it to run such software. Co-processor cartridges existed for the legendary Commodore 64, but Bil Herd and his team managed to integrate a Z80 CPU directly on the C128's motherboard, allowing it to boot CP/M off floppy disks in 80-column mode.

Mixed-architecture machines aren't anything new. RAID controllers and [SCSI cards](a-cool-adaptec-aha-1542cp-isa-scsi-card "A coll Adaptec SCSI card with a Z80") (among others) have had their own CPU and memory for years, and the Macintosh even had an Apple II card. But buying a co-processor card for a computer you bought, to take over much of the system? Imagine booting into a native POWER or RISC-V system thanks to a card in your AMD Ryzen tower!

<figure><p><img src="https://rubenerd.com/files/2024/softcard@1x.jpg" alt="Photo of a Microsoft SoftCard by OlivierBerger" srcset="https://rubenerd.com/files/2024/softcard@2x.jpg 2x" style="width:500px;" /></p></figure>

It's the other reason I fell for [buing an Apple //e Platinum](https://rubenerd.com/remembering-the-apple-iie-platinum-and-finally-finding-one/) last year. Many different co-processor cards existed for it, but none were as widespread as Microsoft's SoftCard; [photo by OlivierBerger](https://commons.wikimedia.org/wiki/File:Microsoft_Softcard_Z80_coprocessor_for_Apple_II.jpg). Like the C128, the SoftCard embedded a Z80 with its own onboard memory to run CP/M. Couple that with an 80-column card and a monochrome monitor for sharp text, and you had a decent productivity machine you could reboot back out of when you wanted. The card also permitted a degree of visibility with the host 6502 system, with machine-level instructions you could invoke if you knew what you were doing.

The SoftCard spawned many clones, culminating in the PCPI Applicard which had more memory and a faster Z80. Its this card upon which modern reproductions seem to be mostly based. The [AppleII-VGA](https://github.com/markadev/AppleII-VGA/) derived [∀2 Analog](https://github.com/V2RetroComputing/analog) can be reconfigured to boot as an Applicard instead of a VGA output, and GGLabs have a [monster 20 MHz](https://gglabs.us/node/2321) card that may even outperform some of my earlier 32-bit PCs. In an Apple ][!

The //e has such a wonderful keyboard, and its power supply was only just refurbished by its original owner, so I'm really tempted to give some of these Z80 cards a try when money becomes a thing again.
