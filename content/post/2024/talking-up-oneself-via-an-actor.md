---
title: "Talking up oneself, via an actor"
date: "2024-12-17T08:30:08+11:00"
abstract: "How do you talk yourself up without seeming insufferable?"
year: "2024"
category: Thoughts
tag:
- personal
- philosophy
location: Sydney
---
Clara and I were having lunch yesterday, as people tend to do in the middle of the day, when a couple sat at the table next to us. I don't make a habit of eavesdropping on people, but sometimes the volume of their speech makes it unavoidable.

We quickly released it was an actor, and a theatre company employee who wasn't a recruiter or talent scout *per se*, but someone who perhaps held some sway. And boy was the actor taking advantage of it.

Some of his lines were amazing.

> I can be your dream realised.

I want that on a quilt cover.

> I fill in when other actors fail. I consider it a moral duty. I'll be on stage with an actor, and see my fellow actors are not performing adequately, so I'll leap in and fill their role as well. Directors love me, I'm very motivated.

Do you reckon directors love him when he does this?

> I'm humble in the sense that I don't show off my achievements, but everyone knows I'm the best. I tap into the zeitgist and [endless buzzwords] like nobody else does. The experience is what sells a show, and we know they're there to experience me.

If only I could be that humble!

He had so many incredible one-liners, but the takeaway from it was something I've struggled with for years: how do you talk yourself up, or sell yourself to the world, without coming across as insufferable?

My solution thus far has simply been not to do it, even if it ends up biting me, or causing me to miss opportunities. The line between confidence and arrogance is a thin one, and my *severe* aversion to the latter means I'll avoid the former, to the point where I'll try launching or presenting something, and immediately pull back. Heck, even my silly [Omake XML format](https://rubenerd.com/omake.xml) sat there for years before I got fed up and hit publish, and the closest I got to a recent public CV was a post literally explaining that I [don't know most things](https://rubenerd.com/we-cant-be-informed-about-everything/ "We can’t be informed about everything"), coached with silliness and a further list of nonsense! It'd be a stretch to call it crippling, but surely there's a happy medium somewhere.

Maybe the lesson here is that you can talk yourself up *a bit*. Just don't say your modesty is world renowned, and that you talk over other actors with your brilliance. Or maybe... that was also an act?
