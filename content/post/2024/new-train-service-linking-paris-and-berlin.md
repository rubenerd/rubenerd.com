---
title: "New train service linking Paris and Berlin"
date: "2024-09-25T22:09:12+10:00"
year: "2024"
category: Travel
tag:
- europe
- france
- germany
- trains
location: Sydney
---
[Deutsche Welle reported](https://www.dw.com/en/deutsche-bahn-new-train-to-link-berlin-and-paris-in-8-hours/a-70314292):

> A new high-speed train, operated by German rail operator Deutsche Bahn and France's SNCF, will allow passengers to travel between Berlin and Paris in approximately eight hours, Deutsche Bahn announced on Tuesday.
>
> Deutsche Bahn is also planning other new international connections. Among them are routes from Munich to Milan and Rome, in collaboration with Trenitalia, scheduled for 2026.

Say what you will about the reliability of DB of late, but this Australian on a far-flung, isolated continent can only look on with envy. I'd love to travel to the capital of another country in eight hours by train, and *especially* these two!

Clara and I are hoping to get to Europe in the next couple of years. This may now be on the itinerary... and not just because we're massive train nerds.

As an aside, I'd love a high-speed train between Sydney and Melbourne. Alas, I doubt I'll see it in my lifetime.
