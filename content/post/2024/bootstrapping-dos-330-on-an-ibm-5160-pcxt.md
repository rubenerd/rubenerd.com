---
title: "Bootstrapping PC DOS 3.30 on an IBM 5160 PC/XT"
date: "2024-06-09T07:40:44+10:00"
year: "2024"
category: Software
tag:
- ibm-xt
- retrocomputing
location: Sydney
---
I didn't expect such interest from my [IBM 5160 XT retrospective](https://rubenerd.com/the-ibm-5160-xt/) post last Thursday! Other 8-bit machines from Commodore, Apple and the like are *cool*, but I didn't realise there were as many retrocomputing fans of early IBM too. I suppose it makes sense.

A few of you asked how I bootstrapped a workable operating environment on one of these machines. Being a PC, it has more in common with the disks, formats, and setup processes as current machines compared to, say, a Commodore 64. But there are obviously enough quirks to make the initial setup process a bit different.

This post discusses the 5160 XT because it's what my old man used, but probably also applies to the 5150 PC, and maybe the 5170 AT. I don't have an XT (sadly!), so I'm using 86Box. Don't forget to [become a patron](https://www.patreon.com/86Box/posts) and support their work if you love it as much as I do.

<figure><p><img src="https://rubenerd.com/files/2024/ibm5160@1x.jpg" alt="Photo of an IBM 5150" srcset="https://rubenerd.com/files/2024/ibm5160@2x.jpg 2x" style="width:500px; height:333px;" /></p></figure>

*Photo by <a href="https://commons.wikimedia.org/wiki/File:IBM_PC-IMG_7271_(transparent).png">Rama &amp; Musée Bolo</a> on Wikimedia Commons.*

### Specifications

The XT is an 8-bit machine built around the Intel 8088, or an NEC V20 if one upgraded. It came stock with two 5.25-inch disk drives, or one disk drive and an MFM hard drive. It came with PC DOS 2.0, but CP/M was also an option.

I'm "upgrading" my virtual XT to use IBM PC DOS 3.3, because it's what I have, and 3.x is the best version for 640 KB systems. You can also use MS-DOS 6.22 if you want.


### Configuring 86Box

86Box comes configured for an original IBM 5150 PC, so changing to a 5160 is easy. From the **Settings** screen:

* Under **Machine**, choose `[8088] IBM XT (1986)`. This lets you easily populate your virtual board with a full 640 KB.

* Under **Display**, I keep it as `[ISA] IBM CGA`. Leave the settings as default if you want glorious CGA colour, or you can click Configure and choose `Green Monochrome` or `Amber Monochrome` from RGB type if you want to emulate one of those crisp monochrome displays for text.

* Under **Sound**, I chose `[ISA] AdLib` for a contemporary sound card, though an exploration of that will be in a future post.


### First boot

When you start the machine, you should see the memory check, before the XT boots into Cassette BASIC. This was licenced from Microsoft, and is located on four ROM chips on the motherboard. It's weird to think of a modern PC coming with a BASIC interpreter on the motherboard, but this was the 1980s.

You might also want to configure your virtual display by going to the **View** menu and choosing **Force 4:3 Aspect Ratio**. This closer mimics what you would have seen on a CRT.

Once you've confirmed this works, we can start configuring your boot volume.

<figure><p><img src="https://rubenerd.com/files/2024/xt-basic.png" alt="The Cassette Basic welcome screen after first launch" style="image-rendering: pixelated;width:500px; height:333px;" /></p></figure>


### Configuring the hard drive

MFM warrants its own post, but in brief it was an early consumer hard drive interface. Unlike IDE with its *integrated* drive controller, MFM drives acted more like a floppy drive, with an external ISA card performing low-level operations via two ribbon cables. As such, one couldn't take an MFM drive from one controller and connect it to a different one to read data, though you could probably format it.

The majority of IBM retrocomputer enthusiasts today use the [XT-IDE](http://www.glitchwrks.com/xt-ide) BIOS, or more recently the [PicoMEM](https://texelec.com/product/picomem/) to provide or emulate IDE instead. I emulate an MFM drive, because I'm a raging masochist. 86Box also helps here by configuring our "jumpers" on the MFM card for us.

Back under Settings:

* Under **Storage Controllers**, I chose an `[ISA] IBM PC Fixed Disk Adapter (MFM)`, though you can choose `[ISA] XT-IDE` if you're rational.

* Under **Hard Disks**, click **New**, and choose `MFM/RLL` bus and `20 MB (CHS: 306, 8, 17)`. This was one of the two drives IBM shipped, along with a 10 MB model.


### Booting with a hard drive

When you start the machine, you should see the memory check, followed by a long pause as the MFM controller attempts to locate a drive. You'll probably also get a enterprising `1701` error, because the drive hasn't been formatted yet.

MFM drives require low-level formatting before they can be recognised by `fdisk` on DOS. To do this, we need the **IBM Advanced Diagnostics** disk which you can get from [Minus Zero Degrees](https://minuszerodegrees.net/ibm_xebec/ibm_xebec_llf_add.htm) or [Winworld](https://winworldpc.com/product/ibm-pc-diagnostics/2x). Attach the disk image to the first floppy icon in the tray of 86Box, then reboot the system.

When the `ADVANCED DIAGNOSTICS` prompt appears, choose the following options.

1. Type `0` for `SYSTEM CHECKOUT`. This will take a while.

2. You should see `1 FIXED DISK DRIVE(S) & ADAPTER`. Press `Y` to continue.

3. Type `0` for `RUN TESTS ONE TIME`.

4. Type the number next to `1 FIXED DISK DRIVE(S) & ADAPTER`.

5. Type `2` for `FORMAT FIXED DISK`.

6. Type `C` for Drive C, and confirm the operation with `Y`.

This will take a while, of course! When done, you'll be given the menu again. 

<figure><p><img src="https://rubenerd.com/files/2024/xt-mfl-low.png" alt="" style="width:500px; height:333px;" /></p></figure>

### Installing DOS

From here, installing DOS is broadly the same as on a more modern 32-bit PC. With the PC DOS 3.3 floppy disk image you've legally obtained from somewhere (<a href="https://winworldpc.com/product/pc-dos/3x">cough</a>), eject the Advanced Diagnostic disk, attach the PC DOS disk image to the first virtual drive, and restart.

When DOS has finished booting, type `fdisk`, then:

1. Type `1` for `Create DOS partition`.
2. Type `1` for `Create Primary DOS partition`.
3. Type `Y` for maximum size and setting the partition active.

Then restart the machine.

<figure><p><img src="https://rubenerd.com/files/2024/xt-fdisk.png" alt="" style="width:500px; height:333px;" /></p></figure>

When you're back in DOS, type `FORMAT C: /V/S` to format and copy across system files with verbosity. You'll be shown the active head and cylinders, which is pretty cool. Then copy the remainder of the DOS system files:

1. Type `MKDIR C:\DOS`

2. Type `COPY A:\*.* C:\DOS`

<figure><p><img src="https://rubenerd.com/files/2024/xt-format.png" alt="" style="width:500px; height:333px;" /></p></figure>


### Booting

Eject the floppy and restart. If all goes well, you should be at a `C>` prompt on your virtual IBM PC/XT! Yay! Wasn't that fun!?

From here, you could upgrade your XT with some AdLib sound, maybe a SixPakPlus to get a real-time clock and some additional memory, or swap out for an NEC V20 and feel that turbo performance.

<figure><p><img src="https://rubenerd.com/files/2024/xt-dos.png" alt="" style="width:500px; height:333px;" /></p></figure>
