---
title: "When a professional got a passphrase wrong"
date: "2024-02-29T18:47:50+11:00"
year: "2024"
category: Software
tag:
- drives
- encryption
- mistakes
- storage
location: Sydney
---
I've said here before that I think it's important to [share our mistakes](https://rubenerd.com/a-morning-mistyping-a-vim-plugin/ "Mistyping a Vim plugin on my FreeBSD laptop"), irrespective of how trivial they are, to remind everyone we're all human. It's also a way to vent at... myself, especially when I need a good talking to for doing something silly.

Today I spent an *embarrassing* amount of time trying to figure out why I couldn't unlock a drive. I'll leave the specific platform, OS, and software invoked out, but suffice to say I was pasting the passphrase correctly from my secured store, and the decryption software refused to accept it, claiming it was invalid.

Your *face* is invalid, secure store.

Here's a question for you: what motions go through your head when you encounter an issue like this, or similar?

My first thought was that the passphrase itself was wrong. Maybe I stored it wrong in the secure store, perhaps by inadvertently truncating it when pasting, adding an errant character, doubling up a part of it, or even using the wrong character set when your localisation or other settings are wrong. I've been bitten by all of these over the last decade in some form; they're easy mistakes to make.

I doubted this, because I was sure I'd used the same passphrase from the same version of the secure store recently without issues. Maybe my memory was playing tricks?

Next, I tried to see if the target machine was correct. Maybe I was typing the hostname wrong, or had messed up it's SSH parameters after some recent changes to my SSH config file. I confirmed I was connecting to the right machine with the right credentials, and the unique identifier of the target disk was also correct. I was on the right box, with the right disk, and the right passphrase.

*<strong>UPDATE:</strong> A draft version of this post was uploaded by accident, and cut off here. It continues below. No, the irony isn't lost on me that a post about mistakes had a mistake.*

I started looking at what may have changed in the time I last accessed the server. Maybe a software update had broken a mounting script, or subtly changed how something worked? I checked the logs on the box to determine if any updates had been applied, and whether they touched anything to do with the disks. An update *had* been applied, but as far as I could tell it didn't impact storage.

I felt like I was grasping at straws by this stage, so I went back to verifying the state of this disk. The mount scripts were indeed using the correct cipher, and the passphrase was being passed down without being changed. Maybe the disk itself was corrupted, and I'd have to restore it from backups first? Not the end of the world, but it'd be a bit of work.

The other engineers among you have likely spotted the issue, but I set the appropriate volume verification tool to work while I went off to lunch. Only when I came back, coffee in hand, did I realise my obvious mistake.

*This volume didn't use a **passphrase**, it used a **key**. The passphrase was to unlock the **key**, not the encrypted volume.*

Sure enough, when supplied with the correct key and passphrase, it mounted, and everything was fine. *Welp!* Lesson learned... at least, for now.
