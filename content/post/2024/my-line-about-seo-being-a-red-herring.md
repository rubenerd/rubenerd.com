---
title: "My line about SEO being a red herring"
date: "2024-01-22T09:43:00+10:00"
abstract: "SEO is the digital equivalent of claiming seat belts are “law optimised”"
year: "2024"
category: Internet
tag:
- feedback
- metadata
- security
location: Sydney
---
Back in 2022 I wrote a post about [web engagement](https://rubenerd.com/engagement-is-sending-the-web-backwards/), and I included this throwaway line:

> SEO (an abbreviation I’ve long thought a red herring)

More than a few people have since asked what I meant by that, so I thought I'd quickly elaborate.

There's a few parts to this, the most important being that SEO *is* an abbreviation, not an acronym. Acronyms are pronounced like words, like laser. But I concede that *abbreviation* technically cover both; the specific term I should have used is *initialism*. Wait, where are you going?

My view has long been that good pages rank well by virtue of being good pages. They'll usually have most, if not all, of these attributes:

* stuff people want to read and link to
* accessible design
* correct and easily-parsed markup
* meaningful and accurate metadata
* reasonable performance

You *could* call all this "search-engine optimisation", but I don't find that limited scoping useful. It's akin to saying drunk driving isn't "law optimised". I mean, it's *not*, but that's not the only reason ethical people avoid it. Likewise, optimising one of these over everything else isn't productive beyond the technical equivalent of a sugar hit.

SEO companies tend to make sweeping statements that aren't based on quantifiable data, and recommend changes to sites in cookie-cutter reports that are, at best, snakeoil. I have plenty of experience fixing sites that had been "optimised" by such outfits, often times by installing bloated, insecure, and redundant plugins. I think you know the ones of which I speak!

I'm thankful for the term SEO though, because people who self-identify with doing it are usually best avoided. I got SEO offers for my site here at least weekly before I started filtering on the term.

That reminds me, I never did get back from that guy who was offering an SEO report to fix my "Linux" for my "WordPress" site here. I'd be half tempted to open a FreeBSD jail for him to log in and see how far he gets.
