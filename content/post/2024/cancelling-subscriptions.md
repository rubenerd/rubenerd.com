---
title: "Cancelling subscriptions"
date: "2024-01-04T08:12:32+11:00"
year: "2024"
category: Internet
tag:
- finances
location: Sydney
---
A new year resolution for Clara and I was to review and cut our number of subscriptions. These recurring bills manage to slip by even with our weekly manual account reconciliations, so we can absolutely empathise with those who only take cursory looks at their accounts.

It's embarrasing to admit, but we each managed to cull more than $50 a month, including amortised annual expenses (wait, can you "amortise" a yearly subscription? The way we budget, we accumulate $10 a month in specific "envelopes" for a $120 a year cost). That's more than $600 *a year*.

Some of these were easy. Turns out we were double-paying for a few things, so one of us were able to cancel and merge our accounts into family ones. Other accounts and sites we hadn't logged into for ages, or our tastes and interests had moved on.

Clara noted many of them amounted to "aspirational" expenses, or those you pay because of some vague idea you'll take advantage of them in the future. My laundry list of domain names is a classic example; I had so many half-finished projects I carried forward each year without thought, despite clearly not making progress on any of them (cough)!

It was also easy to cancel commercial sites and services when we considered we'd rather send money to individuals and people we care about. Patreon is now the site we spend the most on, which to me feels more meaingful and worthwhile.

