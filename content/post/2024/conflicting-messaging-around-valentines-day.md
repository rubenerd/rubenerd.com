---
title: "Conflicting messaging around Valentine’s Day"
date: "2024-02-15T09:34:33+11:00"
year: "2024"
category: Thoughts
tag:
- events
- sociology
location: Sydney
---
Yesterday means different things to different people:

* A cynical, dastardly ploy by confectionery and greeting card companies to get you to associate joy and affection with their consumable stuff.

* A guilt trip from family or society that your lifestyle doesn't conform to a specific or accepted norm.

* A fun day to express feelings for people you care about.

* A rude, mandatory, inescapable reminder of one's loneliness.

These are more in conflict than I think people realise.

I was a shy kid, an awkward teenager, and had my first girlfriend in my late 20s. I know intimately well the feelings of isolation and low self-esteem that come from feeling like nobody wants you; and good luck if your preferences deviate from heterosexual norms. I convinced myself for years I was invisible, that there was something fundamentally broken about me that repelled those I had feelings for. All of this was nonsense of course; just as it's nonsense if you're lonely and reading this now.

On the other side, you have people who seek no romantic involvement with anyone, and are perfectly content with their living arrangements. Well, that is, until their parents or friends poke them about kids for the fiftieth time in a joking-but-not-joking fashion.

This is where I push back a bit on the idea that Valentines Day is meaningless. I mean, *it is* to Clara and I; we spent ours playing Minecraft and getting over a flu. But it sure isn't meaningless to those watching from the wings, or for those who'd rather be left alone. Valentine's day is being, to use the words of David Bowie, *under pressure*. Or maybe that's just the hypertension that comes from too much chocolate. Savoury > sweet anyway.

What was the point of this post? I don't remember. Let's just all agree Valentine's Day *sucks*.
