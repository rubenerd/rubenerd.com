---
title: "Humble people influence my life"
date: "2024-12-04T19:35:33+11:00"
abstract: "We’re not famous or wealthy, but we love what we talk about. That brings me more joy than perhaps it should."
year: "2024"
category: Thoughts
tag:
- blogging
- independent-media
- podcasting
location: Sydney
---
This was one of those shower thoughts that came to me, surprising though it may seem, in the shower. It's not a tautology though, because it isn't one. Now that I've clarified that, like so much butter, we can get onto the substance of this post. *The butter*, you could say. Or not.

When I was a kid, the authors of the books I read, the actors I saw in the movies, the talking heads I watched on TV, were all famous. They were paid squillions to do what they did, and could be recognised anywhere. My dad would come home from an international business trip and tell us he saw George Clooney at the airport, or Dan Brown. We knew who he was talking about.

Then something happened. Around the time I started university in the 2000s, is a sentence fragment with nine words. I started enjoying more of what I found online. I discovered RSS, I started listening to podcasts, I started reading independent novelists. I loved Isaac Asimov, but I was more likely to pick up a [Michael McCollum](https://www.scifi-az.com) or a [Michael W Lucas](https://mwl.io). The books by those novelists, not the novelists themselves; I lack the fortitude and motivation to engage in that.

*(Some have come full circle. Certain podcasters now have audiences and income that surpass those of traditional radio hosts. But they're not the ones I listen to).*

This has only accelerated of late. I mostly only read, listen to, and watch people who are in about similar circumstances to my own. We're all a bit scrappy, but we love what we talk about.

There's a lot to hate about this current period of time. But I can't express the joy the above realisation brings me.
