---
title: "8-Bit Software Fun: Starting with a Commodore"
date: "2024-02-18T15:36:33+11:00"
year: "2024"
category: Software
tag:
- 8-bit-software-fun
- retrocomputing
location: Sydney
---
In today's installment of [8-bit Software Fun](https://rubenerd.com/tag/8-bit-software-fun/ "View all posts in the 8-bit Software Fun series"), we're starting from absolute first principles on a Commodore machine. Most online guides and videos assume a modicum of familiarity when talking about this kit, but what if you were someone like me who grew up in the 32-bit era and are starting from scratch?

I'm using VICE for this post, because I've yet to get a proper capture device. This might change in the future.


### Your first Commodore boot screen

Picture the scene: you've bought yourself an 8-bit Commodore machine and hooked it up to a TV with composite or S-Video, or a modern HDMI machine with a RetroTINK. Now you're starting at the splash screen wondering what to do next.

This is the 40-column mode on the Commodore 128, my favourite 8-bit machine of all time. But it would be very similar on other Commodores:

<figure><p><img src="https://rubenerd.com/files/2024/8-bit-c128-load.png" alt="Screenshot showing the READY prompt." style="width:384px; image-rendering: pixelated;" /></p></figure>

Already this demonstrates a couple of differences between 8-bit and modern computers. You know how motherboards have a UEFI or BIOS? 8-bit machines tended to have a ROM containing an entire, functional OS for the system. And your primary interface to the machine was invariably a BASIC interpreter, in which you'd do everything from read disks, write code, and launch programs. Even the first IBM PC 5150 acted this way.

This was my first 8-bit *culture shock*, having grown up on MS-DOS. In my head I expected to land on a `C:\>` or `$` prompt, but instead I was thrown to BASIC, with code executed as soon as I typed them and press RETURN.

Here's the mandated line every new programmer has to run:

<figure><p><img src="https://rubenerd.com/files/2024/8-bit-c128-hello-world.png" alt='PRINT "HELLO, WORLD!"' style="width:384px; image-rendering: pixelated;" /></p></figure>

But don't be fooled, this isn't a simple shell. Commodore BASIC is also an editor, kinda! If you prepend your BASIC commands with line numbers and press RETURN, they're tokenised in memory instead of being executed as they are in direct mode. Write a couple of these, and you've got a program. Schveet.

Here's that same line we wrote above, with the `?` shortcut for `PRINT`, a `GOTO` statement, and line numbers.

<figure><p><img src="https://rubenerd.com/files/2024/8-bit-c128-program.png" alt='10 ?PRINT "THE BIRD IS THE WORD!", then 20 GOTO 10, then RUN' style="width:384px; image-rendering: pixelated;" /></p></figure>

Typing `RUN` and hitting RETURN, we get the most important musical line in history repeated in all its glory:

<figure><p><img src="https://rubenerd.com/files/2024/8-bit-vic20-bird.png" alt="THE BIRD IS THE WORD (repeated across the screen)" style="width:500px; image-rendering: pixelated;" /></p></figure>

Wait a minute, we switched over to a VIC-20 there for a second... almost like I did it on purpose! I forgot the exclamation mark though, which does break continuity somewhat.

Anyway, this will continue indefinitely until you reset or power cycle the machine. You can also press the RUN/STOP key, which is mapped to ESC if you're running in VICE as I am here.

<figure><p><img src="https://rubenerd.com/files/2024/8-bit-c128-break.png" alt="BREAK IN 10, then READY." style="width:384px; image-rendering: pixelated;" /></p></figure>

Now if you were like me, my next question was how could you write longer programs. You can't scroll up and down with the cursor keys as you'd expect in a modern editor. Are you limited to 24 lines?

Well, remember when I said prepending line numbers lets BASIC know to store the line rather than execute it? These lines are retained even if you clear the screen. This means you can write lines wherever you want, and even out of order!

Here's another quick program using the same `GOTO` line. Note that when I run the `LIST` command, the lines that were stored in memory are printed in execution order:

<figure><p><img src="https://rubenerd.com/files/2024/8-bit-c128-order.png
" alt='20 GOTO 10, then 10 ? FRE(0), then LIST. This results in 10 PRINT FRE(0), then 20 GOTO 10.' style="width:384px; image-rendering: pixelated;" /></p></figure>

Typing `RUN` and `RETURN` this time, and we get told more times than is useful how much memory is free for BASIC:

<figure><p><img src="https://rubenerd.com/files/2024/8-bit-c128-run-fre.png" alt='58089 repeated across the screen' style="width:384px; image-rendering: pixelated;" /></p></figure>

Ah, 8-bit machines and bank switching! We can press RUN\STOP again to terminate the program.

To clear the screen, press `SHIFT` and `CLR/HOME`, or type the `PRINT CHR$(147)` control code. Note that your program will still be retained in memory even if you do this.

<figure><p><img src="https://rubenerd.com/files/2024/8-bit-c128-clear.png" alt='BREAK IN 10, then READY, then PRINT CHR$(147)' style="width:384px; image-rendering: pixelated;" /></p></figure>

If you want to delete your program as well, use the `NEW` command. Note below that we've cleared the screen, run `NEW`, and then run `LIST`. No program lines are shown:

<figure><p><img src="https://rubenerd.com/files/2024/8-bit-c128-new.png" alt="READY, then NEW, then READY, then LIST, then a blank line, then READY." style="width:384px; image-rendering: pixelated;" /></p></figure>

There are a few more tricks, but those were the essentials I needed to start applying my BASIC knowledge from other machines to Commodores. If you treat the editor more as a scratch pad, it feels natural pretty quickly.

Commodore's BASIC was famously limited on the VIC-20 and C64, with most of their graphics and sound capabilities squirreled away behind PEEK and POKE memory commands. Other Commodore machines came with better versions of BASIC, including my beloved Plus/4 and C128 that include structured programming and multimedia commands. There were also extension carts and wedges that expanded BASIC into something more useful, which we'll cover in a future post.

For now, that's it for part one! In the next installment we'll look at storing, retrieving, and running programs on cassettes and disks, and loading software from cartridges.
