---
title: "Trash Theory discusses Tears for Fears"
date: "2024-11-11T10:30:11+11:00"
year: "2024"
category: Media
tag:
- 1980s
- music
- music-monday
location: Sydney
---
Today's *[Music Monday](https://rubenerd.com/tag/music-monday/)* is less a specific song, and more an exploration of a band that was a huge part of my childhood. *Mad World! Sewing the Seeds of Love! Everybody Wants to Rule the World! Shout (Let it all out)!*

<figure><p><a target="_BLANK" href="https://www.youtube.com/watch?v=Ke3xktAJgyg" title="Play Tears For Fears - Dancing To Their Own Trauma New British Canon"><img src="https://rubenerd.com/files/2024/yt-Ke3xktAJgyg@1x.jpg" srcset="https://rubenerd.com/files/2024/yt-Ke3xktAJgyg@1x.jpg 1x, https://rubenerd.com/files/2024/yt-Ke3xktAJgyg@2x.jpg 2x" alt="Play Tears For Fears - Dancing To Their Own Trauma New British Canon" style="width:500px;height:281px;" /></a></p></figure>

If you've never seen Trash Theory before, the channel explores the rise of popular bands, with a specific interest towards the 1980s. They're shockingly well researched and produced, with video footage and brief audio clips letting you hear a band's influences. I know a video is good when Clara and I watch it and have to keep pausing it to say *huh, I never knew that!*

From the episode notes:

> Tears For Fears didn’t quite fit in. Not style conscious enough to be New Romantic but too Smash Hits to be the next Joy Division. Not quite a synth-pop band, more a pop band that used synthesisers. Before U2 took over, Roland Orzabal and Curt Smith were briefly the biggest band in the world.

