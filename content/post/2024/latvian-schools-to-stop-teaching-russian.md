---
title: "Latvian schools to stop teaching Russian"
date: "2024-04-24T08:33:44+10:00"
year: "2024"
category: Thoughts
tag:
- europe
- latvia
- ukraine
location: Sydney
---
[Elsa Court reported for The Kyiv Independent](https://kyivindependent.com/latvia-russian-language-teaching/)\:

> Children in Latvia will no longer learn Russian as a foreign language in schools from 2026, but instead will be required to learn a language of the European Union or the European Economic Area, Latvia's Education Ministry announced on April 23.
>
> The ministry said that after the start of the full-scale invasion of Ukraine, schools had received feedback from parents that they did not want their children to learn the Russian language. 

Even if Putin is allowed to win his Ukranian invasion, the Russian soft power he's thrown away is immesurable.
