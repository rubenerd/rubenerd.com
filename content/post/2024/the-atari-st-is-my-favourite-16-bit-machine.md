---
title: "The Atari ST is my favourite 16-bit machine"
date: "2024-11-09T14:19:17+11:00"
thumb: "https://rubenerd.com/files/2024/atari-1040stf@1x.jpg"
year: "2024"
category: Hardware
tag:
- atari
- atari-st
- history
- retrocomputing
location: Sydney
---
The Atari ST family of machines are my favourite of the 16-bit era. Introduced between 1985 and 1993, they weren't the most technically advanced available, nor did they achieve significant marketshare against incumbents like Apple, Commodore, or the ever-expanding PC clone market. But they had *so much* home computer history soldered into their DNA that I can't help read and write about them.

In 1982, Jack Tramiel and his team at Commodore were riding high on the success of the legendary Commodore 64; by some metrics still the best selling computer of all time. Barely two years later, Tramiel had resigned from the company he'd founded, and bought the consumer division assets of his former arch-rival Atari. Along with key former C64 engineers like Shiraz Shivji, he staged a comeback to the computer market with a new line of budget-focused machines. These would become the Atari ST.

<a href="https://commons.wikimedia.org/wiki/File:Atari_1040STf.jpg">This photo by Bill Bertram</a> shows the machine with its colour monitor option and mouse. How gorgeous is this mid-1980s design!?

<figure><p><img src="https://rubenerd.com/files/2024/atari-1040stf@1x.jpg" alt="Photo of the colour monitor and 1040STF by Bill Bertram." srcset="https://rubenerd.com/files/2024/atari-1040stf@2x.jpg 2x" style="width:500px; height:340px;" /></p></figure>

The history around this time is fascinating. I [wrote back in May](https://rubenerd.com/when-tramiel-left-commodore-for-atari/ "When Tramiel left Commodore for Atari") about how the Amiga had its genesis with an ex-Atari engineer, and how many ex-Commodore engineers ended up at Atari once Tramiel jumped ship. It's fun to think of an alternative history where the Commodore ST competed with an Atari Amiga instead.

The ST sported a high-resolution monochrome mode for text and productivity tasks, and lower-resolution modes with up to 16 colours that made use of a different monitor. It was initially built around the same 16-bit 68k Motorola CPU as the Amiga and Macintosh, just as many of the 8-bit Commodores, Apples, and Ataris had used variants of the 6502. In marketing that echoed the C64, the ST undercut the Amiga by hundreds of dollars, and the Apple IIe by more than a thousand. STs never had the cornucopia of specialised ICs like the Amiga, though a blitter added to the Mega and later machines improved gaming performance and reduced CPU load.

While a decent, affordable game machine for the time, it was the fateful inclusion of MIDI-in and MIDI-out ports that gave the computer a cult following, especially among audio producers and musicians in Europe. I was surprised to read just how many artists I recognised on the Wikipedia page, including Mike Oldfield, Fatboy Slim, and the Pet Shop Boys! The Amiga had more colours and graphics capabilities, but the Atari excelled in this niche.

Along with the machine's pedigree, the ST is interesting to me for a few other reasons. I love the 45-degree design language carried through in the vents and function keys, and the all-in-one form factor bares a striking resemblance to the C128 and earlier 8-bit era machines. The fact the 520 ST had barely half a year of development, and beat *both* the Macintosh and Amiga to market with a GUI is incredible to me. The fact former Commodore engineers were largely responsible for its development also tickles me.

<figure><p><img src="https://rubenerd.com/files/2024/tos.png" alt="Screenshot from TOS, via Hatari" style="width:500px;" /></p></figure>

But the reborn Atari Corporation had some help. The first screenshots I ever saw from an ST looked oddly familiar as a fan of Digital Research, and for good reason: the ST was released with a ported version of DR's GEM desktop, with the machine's OS based on GEMDOS. GEM was available on the PC, but a lawsuit with Apple resulted in the desktop being hobbled with two static file windows. Atari avoided these troubles with their version of GEM, somehow.

Modern reviewers of the ST series praise the machine's audio interfaces and design, though are generally underwhelmed with TOS and GEM when compared to the Amiga. An expanded MultiTOS was released for the final 32-bit machines in the line, but the interface and design remained broadly the same. I find it charming and understated, though I'll admit I'm using an emulator through a rose-tinted monitor.

While the ST was initially a sales success, it struggled against an ocean of PC clones that were becoming more capable and dropping in price faster than a bespoke manufacturer could keep up with. An attempt to modernise the machine culminated in the now-unobtainium Atari Falcon, though Atari threw in the towel soon after to focus on their Jaguar game machine in 1993. The rest, as they say, is `HISTORY.PRG`.

<figure><p><img src="https://rubenerd.com/files/2024/hcm-amiga-atari@1x.jpg" alt="Photo of the Amiga 500 alongside an Atari 1040 at the Home Computer Museum in 2023, by Prolete." srcset="https://rubenerd.com/files/2024/hcm-amiga-atari@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

*Photo of the Amiga 500 alongside an Atari 1040 at the Home Computer Museum in 2023, by [Prolete](https://commons.wikimedia.org/wiki/File:HomeComputerMuseum,_August_2023_16.jpg).*
