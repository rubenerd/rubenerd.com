---
title: "The best blog posts are genuine"
date: "2024-03-17T09:25:08+11:00"
year: "2024"
category: Internet
tag:
- weblog
- writing
location: Sydney
---
I've reached the point in my web life where I'll read something, ruminate on it, decide it's interesting and that I want to comment. Then I'll go back to the source to quote it, and I have absolutely *no idea* where I read it, let alone who wrote it. It happens at least once a week, and it's proof I need a better mobile note-taking system.

I *suspect* this observation came from Om Malik, but I can't find it on his blog or social media accounts, so maybe it wasn't. But paraphrased, it went something like this:

> Good blog posts are short, sharp, witty, and interesting.

These are good, and certainly entertaining. But the *best* are ones written with interest, passion, and enthusiasm. I'll take something that's rambling, hastily written, and genuine, over something shallow but smart.

This is what differentiates blogs from newspapers, journals, and academic writing. This is supposed to be a space where anyone and everyone can express themselves. I'd hate for someone to be put off sharing something because they don't meet a hypothetical bar of wit.

*It clearly hasn't stopped me!*

I read a lot for my job, and you can tell when someone's heart isn't in it, when they're phoning it in, or if it's a means to an end (say, for clout or engagement). It's either a rare skill I posess, or more likely people can't feign enthusiasm for long.
