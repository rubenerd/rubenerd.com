---
title: "The CP/M Card and Gold Card for the Apple II"
date: "2024-06-21T10:09:21+10:00"
year: "2024"
category: Hardware
tag:
- apple
- apple-ii
- apple-iie
- cpm
- retrocomputing
location: Sydney
---
This year I fell down the rabbit hole of researching [Z80 cards for the Apple II](https://rubenerd.com/the-spaoe-z80-apple-ii-card/), including the [SPAOE clone](https://rubenerd.com/the-spaoe-z80-apple-ii-card/) of Microsoft's Softcard. These cards added a Zilog Z80 CPU to your 6502-based Apple II, permitting the operation of CP/M.

Another example was *The CP/M Card* by Advanced Logic Systems. [VintageWare explains](http://roger.geek.nz/apple2/cpm.html)\:

> ALS's The CP/M CARD (that's its name!) was one of the fancier ones. The Z80 on the card runs at 6 MHz, while the cheaper ones normally run at 2 or 4 MHz. It also has its own 64 KB of RAM. This means that it can use the Apple II memory for text display, buffers, I/O drivers and parts of CP/M and still have 61 KB left for running CP/M programs.

Digital Research also released an AppliCard-style card called the *CP/M Gold Card*, which [Santo Nucifora](https://vintagecomputer.ca/digital-research-cpm-gold-card/) was lucky enough to find and buy. I... I think I might have my new grail card!

