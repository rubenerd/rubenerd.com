---
title: "Updating an old Ansible role with multiple OS support"
date: "2024-12-15T08:42:51+11:00"
abstract: "Use import_tasks now instead of include if you need playbooks for each OS."
year: "2024"
category: Software
tag:
- ansible
- troubleshooting
location: Sydney
---
I had a series of roles in my old Ansible library that were configured, a little something, like this:

    ./roles/
    └── bootstrap/
        └── tasks/
            ├── debian.yml
            ├── freebsd.yml
            ├── illumos.yml
            ├── main.yml
            └── netbsd.yml

The `tasks` folder contains playbooks for the most common OSs I use, which were automatically selected by `main.yml`. That way, I could add a role to whatever new playbook I wanted, and it would work irrespective of the target server OS.

The `main.yml` file consisted only of includes to reference these OSs:

    - include: debian.yml
      when: ansible_os_family == 'Linux'
         
    - include: freebsd.yml
      when: ansible_os_family == 'FreeBSD'
       
    [...]

Alas, some of these older roles no longer worked, with Ansible reporting the following errors:

    ERROR! this task 'include' has extra params, which is only allowed
    in the following modules: [...] import_tasks [...]

Sure enough, according to the [Ansible docs](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_reuse_roles.html)\:

    - name: Install the correct web server for Debian
      import_tasks: debian.yml
      when: ansible_facts['os_family']|lower == 'debian'

I updated my `main.yml` with that syntax, and It Worked.&trade; Now to stop messing with [pyinfra](https://pyinfra.com) and fix this Ansible stuff in time for Christmas *cough*.


