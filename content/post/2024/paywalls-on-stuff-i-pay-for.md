---
title: "Paywalls on stuff I pay for"
date: "2024-05-19T09:25:35+10:00"
year: "2024"
category: Media
tag:
- authentication
- journalism
- paywalls
location: Sydney
---
I pay for half a dozen different newspapers. It's important to me.

They invariably want you to download and use their phone apps, but I rather include their RSS feeds in with everything else. My ideal would be to see an article from [James](https://jamesg.blog/), then [Wouter](https://brainbaking.com/), then [Michał](https://michal.sapka.me/blog/), then something from a [newspaper](https://kyivindependent.com/). But while web feeds are common across them, they behave differently.

Paywalls are... tolerable on the desktop. I configure Firefox with plugins to periodically purge cookies for privacy reasons, but I use the *Manage Exceptions* section to include URLs for newspapers. It was becoming tedious logging into each one, every single time I wanted to read an article.

Alas, this is still an unsolved problem on the phone. I'll be reading RSS on the go, tap on a news article, and it asks me to login. I could do the username, next, password, next, 2FA if it has it, next... or I could just not read it.

This is a failing on my part, but if there's too much friction for me to do something thesedays, I err on the side of not doing it. It means I end up paying for things I don't read or use, which sucks.

I'm sure there's a meta point here about paywalls, journalism, and the broken state of modern web auth. But I just want to read stuff!
