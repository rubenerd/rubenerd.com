---
title: "Super Pascal, and does a dual Pi1541 drive exist?"
date: "2024-02-16T16:52:24+11:00"
thumb: ""
year: "2024"
category: Thoughts
tag:
- retrocomputers
location: Sydney
---
I only just discovered there were versions of Pascal for the Commodore 64! I loved <em>loved</em> <strong><em>loved</em></strong> Pascal in school, even if it was the dodgy short-lived Microsoft QPascal for DOS iteration of it. I even uploaded a [crappy program](https://codeberg.org/rubenerd/lunchbox/src/branch/trunk/PROBLEM3.PAS "PROBLEM3.PAS from some primary school homework") I wrote as a kid into my lunchbox.

*[Super Pascal](https://www.c64-wiki.com/wiki/Super_Pascal)* requires two 1541 disk drives; one for the development environment, and one for save data. This makes sense; unlike Commodore BASIC in ROM, this has to exist elsewhere. 

This reminded me that while my datasette and Commodore 128's 1571 drive are perfectly functional, the [stepper motor on my 1541 drive is still borked](https://rubenerd.com/troubleshooting-a-commodore-1541-disk-drive/ "Troubleshooting a Commodore 1541 disk drive") despite my best efforts to revive it. The rails are well lubricated, and it has the correct 12V going to the motor. I fear it might be a dead MOS 325572-01, which isn't an affordable fix. *But I digress.*

It got me thinking I should finally invest in a [Pi1541](https://cbm-pi1541.firebaseapp.com/), the realtime emulator by Steve White. But then I realised I'd need two, right? Unless I can somehow run the program disk image from the SD2IEC, but from what I've read and watched about Super Pascal, this doesn't work.

This leads me to wonder (always a dangerous proposition), does anyone have a dual Pi1541? Say, a single external unit that shares a single data cable and power lead, and does the daisy chaining internally?

<figure><p><img src="https://rubenerd.com/files/2024/commodore-8050@1x.jpg" alt="" srcset="https://rubenerd.com/files/2024/commodore-8050@2x.jpg 2x" style="width:500px; height:333px;" /></p></figure>

<p style="font-style:italic;"><a href="https://commons.wikimedia.org/wiki/File:Commodore_CBM_2001_%26_8050.jpg">Photo by Jeff Keyzer on Wikimedia Commons</a></p>

I'm imagining some weird dual-Raspberry Pi contraption in a 3D printed case to resemble the 8050 in my dreams... albeit saddled with the IEC interface that my non-PET/CBM machines could use.

I suppose worst case, I could build two hats and use them on two Pis, then solder an IEC cable between the two in lieu of two of the connectors. Then set one permanently as #8, and the other as #9. Then learn how to use a modelling tool of some sort, create a scaled-down model 8050, and get it 3D printed. *Easy!*

I'll add it to the pile of other projects. Wait Ruben, didn't you *literally* just write a post saying you were trying to [cut down](https://rubenerd.com/more-wood-behind-fewer-tasks/ "More wood behind fewer tasks") on these!?
