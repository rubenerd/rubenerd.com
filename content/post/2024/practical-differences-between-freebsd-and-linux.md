---
title: "Practical differences between FreeBSD and Linux"
date: "2024-05-26T08:51:07+11:00"
abstract: "A comparison from my experience running both on servers and desktops."
year: "2024"
category: Software
tag:
- bsd
- freebsd
- linux
location: Sydney
---
[Michał](https://d-s.sh/) emailed earlier this year saying he's been reading a lot about BSD lately, but wanted to know the practical differences between it and Linux.

The Internet is replete with SEO-optimised comparison sites that offer superficial summaries of LLM-generated data laundered from sites like Wikipedia, so I thought I'd address the comparison here with my own experiences running both in production.

All of these are completely factual, with absolutely no subjective invocations of personal experience, jokes, or silliness. Yes, of course. Much of this also applies to NetBSD.


### Distribution

* FreeBSD is built, distributed, and updated as a cohesive system, rather than a collection of parts. You can feasibly install it by extracting a tarball onto a new partition. You can also build the kernel and entire userland with a few commands, and perform security audits with reproducible and verifiable builds. Ask [Michael Dexter](https://callfortesting.org/) how far he got trying to do the latter on Ubuntu, for example.

* FreeBSD is upgraded by rebuilding, or using the binary `freebsd-update(8)` tool. Unlike Linux package managers, this doesn't affect installed packages because...

* FreeBSD's ports system is installed and updated in a separate directory structure from the base system, meaning you can blow away installed ports without affecting the base if you mess something up. I find this so convenient, I run NetBSD's cross-platform [pkgsrc](https://pkgsrc.se) on some Linux servers I administer.

* FreeBSD has more permissive licencing, which makes its operation more flexible. The GPL makes distributing OpenZFS [difficult on Linux](https://blog.hansenpartnership.com/are-gplv2-and-cddl-incompatible/#comment-102719) (unless you believe Canonical, oh "snap"?), but FreeBSD can work with the CDDL without issue.

* Linux distros are broadly either long-term support (LTS) or rolling releases. Major FreeBSD versions are supported for five years, and are distributed under RELEASE, with [STABLE and CURRENT branches](https://docs.freebsd.org/en/books/handbook/cutting-edge/#current-stable) for development. Their official names are always IN CAPITALS, for emphasis.

* Linux has traditional package managers, Flatpaks, Snaps, AppImages, and so on. FreeBSD used to have a `pkg_*` toolchain for binary packages, but `pkg(8)` is probably closer to what you're used to on Linux. Otherwise you can build from the ports tree.

* Linux has better fanart than FreeBSD, though OpenBSD's is even better. I'll ask Clara if she'd be up for making some.


### Workloads

* FreeBSD and Linux differ in how they're better than Windows Server, but both are a tall glass of cool water (cool glass of tall water) by comparison. No really, I'd take `bash(1)` and `systemd` over PowerShell and Windows patches at 03:00 *any day*. Ask me how I know!

* Linux has wider software support, owing to its larger install and developer base. FreeBSD has the [Linuxulator](https://wiki.freebsd.org/Linuxulator), but I do keep Alpine Linux and Fedora rattling around at home for specific software. I think the trade-off is more than worth it for the system's mature tooling, stability, and mascot.

* FreeBSD can lag behind Linux in certain desktop drivers, and it has narrower hardware support (official Nvidia drivers always work well, but Wi-Fi and AMD Radeon drivers are spottier). FreeBSD operators tend to be more <span style="text-decoration:line-through">attractive, modest, intelligent</span> selective with the hardware they use.

* FreeBSD comes with a suite of tools that either differ from Linux, or don't have direct equivalents, like [DTrace](https://docs.freebsd.org/en/books/handbook/dtrace/), [Jails](https://docs.freebsd.org/en/books/handbook/jails/), the [bhyve](https://docs.freebsd.org/en/books/handbook/virtualization/#virtualization-host-bhyve) hypervisor, and [BSD Games](https://forums.freebsd.org/threads/is-games-txz-essential-for-base-system.40245/). Others may be available, but they have better integrations on FreeBSD, such as Capsicum and ZFS. 

* This [ZFS integration](https://rubenerd.com/allan-jude-discusses-zfs-on-level1linux/) is worth its own point. There's a misconception its only useful for storing data with integrity. It's also used in tools like [Poudrire](https://docs.freebsd.org/en/books/handbook/ports/#ports-poudriere) to build new ports, as a base for new jails, to permit rolling back base updates, to send/recieve backups, and to tune specific datasets for specific workloads. It handles volume management, formatting, and other tasks. Linux's closest equivalent is btrfs.

* Linux is overseen by Tux, and FreeBSD has the BSD Daemon and [Groff the BSD goat](https://twitter.com/GroffBSDGoat), whom I've had the pleasure of meeting a few times. He has an adorable hat.


### Development

* *Linux* is shorter than *FreeBSD* and requires fewer invocations of the shift key, but looks less cool. This is likely why the FSF insists on calling the Linux kernel with other tooling *GNU/Linux*, in spite of popular usage to the contrary, and the fact it's often [not even true](https://rubenerd.com/a-bsd-pserson-trying-alpine-linux/). I prefer to call my game desktop install *KDE/ZFS/Proton/Linux*.

* FreeBSD is more "Unix-like" (especially Solaris of late), which may make migrations and experience from old UNIX systems easier. Linux is actively moving away from this, which you either see as "reinventing Unix, poorly", or removing the shakles of compatibility for further growth.

* Even Linux engineer I speak with admit FreeBSD isn't as susceptible to *chasing the shiny*. Linux churns through frameworks, ABIs, and tooling like they're going out of fashion (or when a commercial outfit baits-and-switches their community, as always happens). FreeBSD is more conservative with what it introduces, and supports them for longer. With its smaller developer base, it doesn't have a choice.

* There's a wider debate about the influence large companies have on Linux, which is used to [push changes](https://suckless.org/sucks/systemd/) that other distributions must integrate lest they lose broader Linux ecosystem compatibility. Then again, FreeBSD has large users like Netflix and Sony that I'm sure also wield influence.


### The bottom of the post

I've [pointed out the similarities](https://rubenerd.com/tutorial-sites-treating-freebsd-like-a-linux-distro/) between the systems to encourage Linux people to give FreeBSD a try, but I hope this explains more of the differences. I happen to think FreeBSD is better for most things, but as with any tool, you should evaluate it based on your use case and requirements. That won't satiate those looking for a flamewar, but it's fact.

The [Handbook](https://docs.freebsd.org/en/books/handbook/) and [FreeBSD Foundation](https://freebsdfoundation.org/past-issues/freebsd-vs-linux/) have more resources, and [Klara Systems](https://klarasystems.com/articles/easily-migrate-from-linux-to-freebsd/) have an excellent series of articles that go into more technical detail about differences and migration paths.

My attitude is that if you're interested, give it a try! There's a lot to like, and even if you come away from it thinking it's not for you, the only thing you've lost are your Linux blinkers. You don't need to justify or make excuses for curiosity.
