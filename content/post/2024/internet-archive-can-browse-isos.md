---
title: "The Internet Archive can browse ISOs"
date: "2024-01-03T10:22:54+11:00"
year: "2024"
category: Internet
tag:
- archives
- internet-archive
- isos
- retrocomputing
location: Sydney
---
This was yet another thing I learned from VOGONS this week! The Internet Archive's `view_archive.php` function can be used to inspect ISOs, the same way you'd look at any archive.

[asdf53 mentioned](https://www.vogons.org/viewtopic.php?p=1113516#p1113516 "asdf's post on a VOGONS thread about drive benchmarks") an old disk benchmark was available on an old ISO compilation, and linked to it:

> The only thing that worked for me was SiSoft Sandra 98 (somewhat hard to find. Found it on this cd, direct download from [inside the ISO here](https://ia601403.us.archive.org/view_archive.php?archive=/6/items/pcinside_spezial_10/PCM_SPEZIAL.iso).

I've been asked many, many times over the years where I sourced drivers or docs for specific old hardware. Knowing I can do this will make linking to these much easier.

Which reminds me, [you can donate](https://archive.org/donate?origin=rubenerd.com "Internet Archive donation page") to them :).

