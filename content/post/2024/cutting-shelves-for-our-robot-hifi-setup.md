---
title: "(Finally!) cutting shelves for our robot Hi-Fi"
date: "2024-08-08T08:22:05+10:00"
thumb: "https://rubenerd.com/files/2024/robot-shelves@1x.jpg"
year: "2024"
category: Thoughts
tag:
- hi-fi
location: Sydney
---
For Christmas when I was 8 years old, my dad built me a wooden chest of drawers in the shape of a robot! He had a workshop in the garage at the time, and built most of our furniture. He ended up in storage when we started moving internationally, but I was happy to get him back when we moved back to Australia. The robot, I mean.

A silly idea I had a few years ago was to turn Mr Robot from a chest of drawers to a rack for Clara's and my burgeoning Hi-Fi system. The width of the drawers was within *5 mm* of a standard audio/server [rack unit](https://en.wikipedia.org/wiki/Rack_unit "Rack unit on Wikipedia"), almost as though my old man knew what my interests would be years later.

<figure><p><img src="https://rubenerd.com/files/2024/robot-old-shelves@1x.jpg" alt="Mr Robot with his various Hi-Fi components and plastic lid 'shelves'" srcset="https://rubenerd.com/files/2024/robot-old-shelves@2x.jpg 2x" style="width:420px; height:560px;" /></p></figure>

There was just one problem: he was completely hollow. The drawers sat on tracks inside the robot's belly, so their removal left no ledge upon which components could sit. While I contemplated what to do, I rested a couple of plastic tub lids on those tracks as an interim shelf. That temporary fix, as they often are, lasted... four years.

Well with a house move pending, we suddenly needed those lids back! So what to do?

One option was to buy a metal server rack shelf, but alas he was *just* too narrow. No regular bookshelf from IKEA or the like had those exact dimensions, so getting shelves from there wasn't an option. I put it in the *too hard* basket, until finally I decided we needed to actually do something about it.

Eventually Clara and I found ourselves at [Hammerbarn](https://www.bluey.tv/hammerbarn-for-real-life/), and we realised they could cut wood on the spot. Every Australian either knew this already, or don't live in an apartment where the use of large powertools isn't practical. But it was news to me.

I picked out a two narrow sheets of board, and had them cut it a few times to the exact size. They worked! (It also made carrying them home on public transport much easier than a couple of giant, long boards).

<figure><p><img src="https://rubenerd.com/files/2024/robot-shelves@1x.jpg" alt="His new shelves, cut to size!" srcset="https://rubenerd.com/files/2024/robot-shelves@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

Each "shelf" is actually two separate pieces, upon which the rear and front feet sit. This allows for more air circulation, and meant I could easily move the rear shelf inwards or outwards to fit cables.

Eventually I'd love to repaint him, and maybe cut a larger circular hole in the back for easier access. But right now this works extremely well, and I'm frustrated it took me this long to get around to installing these. I'm sure there's a broader lesson to be learned.
