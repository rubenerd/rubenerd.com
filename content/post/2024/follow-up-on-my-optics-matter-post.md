---
title: "Follow up on my “optics matter” post"
date: "2024-10-16T08:42:57+11:00"
year: "2024"
category: Ethics
tag:
- psychology
location: Sydney
---
Last Wednesday I wrote about [how optics matter](https://rubenerd.com/optics-matter/), especially within open source communities. I mentioned how a certain WordPress owner had conceded any technical or legal points he thought he was making with his conduct, which was troubling and sad to see.

I've had some interesting and thoughtful feedback, beyond the regular trolls who unintentionally prove my point. One in particular was from someone who wished to remain anonymous, so I've also lightly paraphrased their words:

> I work at a large IT firm, and am diagnosed `$REDACTED`. My problem has been that I'm told my "words have impact" and that "optics matter", but I struggle to meet the social expectations you describe. Sometimes my words land, other times they don't. It makes navigating these situations extremely difficult.

They went on to describe a situation at their workplace in which they made a comment others didn't appreciate, and drew parallels between that and the backlash against the head of WordPress.

I can see where they might think that, but the actions they described later in their email proved they're in a very different situation. They were advised they'd make a mistake, they owned it, apologised to the affected parties, and learned from the experience. That puts them head and shoulders above this other person... and dare I say, even above most "neurotypical" people I have to interact with on a regular basis! I have nothing but respect for them, and told them as much.

This is exactly what I'm talking about when I say *optics matter*: this person may have felt as though they behaved the same way, but they handled the situation appropriately after the fact. Being [polite](https://rubenerd.com/if-in-doubt-be-polite/), mature, and responsible are the best ways to deal with an unintentional situation like this.

As [Xandra Granade put it](https://wandering.shop/@xgranade/113307964728517722) recently:

> I'm neurodiverse, and have hurt people in ways I'm pretty deeply ashamed of. You know what I've tried to do (imperfectly)? Apologize, do what I can to heal the harm, learn what I can to prevent harm next time, and stop making excuses.

Social norms can be tricky. I think growing up in countries outside my birth and family culture made my hyper-aware of this, in both productive and unhelpful ways. But to be clear, we all make *faux pas* and run afoul (🐔) of expectations. It's how we deal with them that define our character.

To the person who sent me that comment, and if you see yourself in their words as well: you're one of the good ones. Sincerely.
