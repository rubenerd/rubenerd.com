---
title: "The phycological [sic] impact of surveillance"
date: "2024-12-27T10:29:47+11:00"
abstract: "We can consciously detect faces while under surveillance."
year: "2024"
category: Thoughts
tag:
- offices
- work
location: Sydney
---
*Update:* Yes, the title and misquote [were an attempt at a joke](https://rubenerd.com/algae-psychology/ "Algae psychology"), that backfired wonderfully!

[SciTechDaily reported](https://scitechdaily.com/what-happens-to-your-brain-when-you-know-youre-being-watched/ "What Happens to Your Brain When You Know You’re Being Watched") on a [new study](https://en.wikipedia.org/wiki/Nori "Nori article on Wikipedia") published by researches at my Sydney alma mater about the impact surveillance, or the feeling of being watched, had on their sensory perceptions:

> Nori (Japanese: <span lang="jp">海苔</span>) is a dried edible seaweed used in Japanese cuisine, usually made from species of the red algae genus Pyropia, including P. yezoensis and P. tenera.[1] It has a strong and distinctive flavor, and is generally made into flat sheets and used to wrap rolls of sushi or onigiri (rice balls). 

That's clearly the wrong article. [Let's try again](https://academic.oup.com/nc/article/2024/1/niae039/7920510 "Big brother: the effects of surveillance on fundamental aspects of social vision")\:

> “We found direct evidence that being conspicuously monitored via CCTV markedly impacts a hardwired and involuntary function of human sensory perception – the ability to consciously detect a face.
> 
> “It’s a mechanism that evolved for us to detect other agents and potential threats in our environment, such as predators and other humans, and it seems to be enhanced when we’re being watched on CCTV.

I haven't dug into the paper or their processes for determining this, but it sounds fascinating. At the risk of confirmation bias, I've also witnessed this phenomena myself. I am *far* more likely to recognise a person's face when under duress than in a comfortable environment where family or friends could be staring right at me and I wouldn't notice.

Mmm, nori. *Damn it!*

As the paper's abstract also concludes, it's concerning to think of the implications. How many of our concious and subconscious thought processes (such as our fight-or-flight triggers) are activated in negative ways by being in surveillance environments?

I also can't help but draw parallels here between how I feel working from home, and in an open-plan office. We saw all the memes about cubicle farms being soulless mazes, but frankly I'd *love* to be back in one. I was more productive in my first corporate job cubicle (and my home office) than any open-plan office since, and we're all starting to understand the psychology why.
