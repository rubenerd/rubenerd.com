---
title: "Wake up, it's a beautful day: the logistics of life"
date: "2024-08-30T04:38:13+10:00"
year: "2024"
category: Thoughts
tag:
- life
location: Sydney
---
I only started noticing this recently, and it made me chuckle.

Stuff just works when we're young... well, hopefully. You get out of bed, and you go to school or uni. My main concerns at the time were forcing myself to eat breakfast because I thought I needed to (I no longer do), and making my morning bus or train. In the words of Sting and Shaggy, I'd *clean my teeth and wash my face; act like I'm a member of the human race*. But that's about it.

Things are a bit different in my late thirties! I've been nursing a sore back for the last week&mdash;a result of twisting myself to look inside a small cupboard at a property Clara and I were inspecting&mdash;so I have to apply a head rub. I have a morning and evening skin care routine, because I was getting tired of my face feeling stiff in Australian winter. I have a special mouthwash I have to use on odd days, so that's something I have to track. I have to clean my glasses and my phone with alcohol wipes. I now make coffee at home most days to save money, so I'll pre-prep a lot of that before going to bed. 

Allan Jude of FreeBSD and ZFS fame famously said at an AsiaBSDCon one year that so many of our homelabs are basically prod now, and it's true! Clara's backups might not have run the night before, so I need to check that. I might need to source additional drives, or do another quick and dirty manual dedup for small batches of files. The dust filters might need cleaning. NextCloud or Jellyfin (ne. Plex) might need restarting. And wait, hold on, why is the BMC light blinking, did we have another ECC correction, or is it something else?

My sister and I had a lot of life admin when we were teenagers, on account of our mum's illness and our dad being a business traveller. But a lot of that was solved with money: it's easy to cook when you can buy takeout, and you have nice appliances that can wash, dry, and steam iron your clothes. Saving for a house deposit has meant we've stretched our older appliances beyond the point when some of them need servicing. There's all the work involved in maintaining a career, savings, and juggling mental health breaks like holidays. When was the last time we saw this friend, wow that long, we'd better book that in. And so on.

I'm *significantly* less stressed now than I was when I was a teenager. Here comes the proverbial posterior prognostication: *but...* there's definitely more preparation, planning, and logistics involved with living as one gets older. I'll take it over the alternative, though :).
