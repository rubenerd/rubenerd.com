---
title: "Some things you might not know about me"
date: "2024-03-23T15:25:00+11:00"
year: "2024"
category: Thoughts
tag:
- childhood
- commodore
- lotus-organiser
- personal
- pets
- retrocomputers
location: Sydney
---
* My first paid job was voice acting. My sister and I auditioned for the Discovery Kids programme on the Discovery Channel, and both got the gig. We did promos and those "coming up next" bumpers.

* I've been to Auckland, Colombo, Dallas, Darwin, and Dubai, but never left the airport. All of them were scheduled, save for Sri Lanka. Our Emirates flight to Frankfurt landed there out of the blue, and sat on the tarmac for more than an hour. We were never told why.

* In Singapore I was often confused for being English, and online people assume I'm American. ’Allo, y'all.

* I have crippling social anxiety, and I work in pre-sales engineering. How does that work? I still don't quite know. Maybe its a form of low-key <abbr title="cognitive behavioral therapy">CBT</abbr>.

* I grew up with dogs, whom I loved, but I secretly preferred cats. We had this running joke that whenever we'd visit family friends, their pet cats would only ever want to interact with me. Which was wonderful!

* *Lotus Organiser* is my favourite ever computer program, including games. It's a *Filofax*, but for the computer! Maybe don't read too much into that. I may be learning a language in secret to try and reproduce it for a specific architecture and system.

* I have above-average olfactory senses. In Singapore I once noticed a gas leak before an entire office of people did. I'd way prefer great vision or hearing though, above-average smell is bad in a *lot* of contexts!

* I don't have a driving licence. I briefly had L's, but I wasn't comfortable that my reflexes were good enough. Cars are death machines in the hands of someone *competent*, let alone me. If I were a billionaire, I'd get a creme *Auburn Speedster* just to display its stunning art deco lines in my huge apartment. How would we get it up there? I dunno, I'm a billionaire, I'd pay Jeeves to figure it out.

* My obsession with 8-bit machines started in 2004. I was on the high school ten-pin bowling team, and my name on the scoreboard alternated between *C64* and *C16*. That's probably the least surprising item on this list.

