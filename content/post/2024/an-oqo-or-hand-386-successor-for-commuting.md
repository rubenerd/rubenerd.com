---
title: "An OQO or Hand386 successor for commuting?"
date: "2024-09-29T16:34:59+10:00"
thumb: "https://rubenerd.com/files/2024/OQO_Model_e2@1x.jpg"
year: "2024"
category: Hardware
tag:
- handhelds
- oqo
- retrocomputing
location: Sydney
---
You know those dreams where you wake up and a wave of disappointment washes over you upon realising the thing you experienced wasn't real? I had such a dream last night where I had a handheld SPARC machine in a fetching shade of Sun purple, and a clicky thumb keyboard for data entry. Alas, I woke up to a world where tablets killed such things, and smartphones are merely slabs of glass. Oh well, *se a vida é*.

How did I end up dreaming of something so specific? And... more than once? I suppose it comes down to being a bit weird, but also as a direct result of our new living arrangements.

In short, with our recent move well underway, Clara and I are faced with having to commute again. We got used to living so close to the city, but now we're back to filling time for train trips every other day. I used to quite enjoy the forced relaxation that came with sitting in a comfy seat and reading, watching an anime, or listening to an album while I gaze at the scenery; and I intend to take full advantage of that again.

Sometimes though, I'd love to be able to hack on something! Inspiration for a bit of code, or a retrocomputer idea, can strike at any time. And it's here as I lumber along on this train where I've reached a bit of an impasse.

A laptop isn't practical to use if I'm standing, and they're just bulky enough if I'm in a train seat. I've never been able to grok tablets despite my best efforts; I feel constrained by touchscreen keyboards and limited OSs. Smartphones are portable, but otherwise have the same shortcomings as tablets. Also, I don't need notifications filling my screen while I'm trying to relax or unwind.

What I'd love already existed, but alas no more. Or... do they?

<figure><p><img src="https://rubenerd.com/files/2024/OQO_Model_e2@1x.jpg" alt="The OQO Model E2, showing the sliding screen and thumb keyboard." srcset="https://rubenerd.com/files/2024/OQO_Model_e2@2x.jpg 2x" style="width:400px; height:341px" /></p></figure>

My ideal commuting device is something akin to the original [OQO](https://en.wikipedia.org/wiki/OQO "Wikipedia article on the OQO") from the 2000s, which was a little PC with a monitor that slid out to reveal a thumb keyboard. These weren't scaled-up PDAs, they were scaled-down computers. As such, they ran real desktop x86 operating systems and software, and could connect with desktop peripherals. This made them impractical for the kinds of things we'd use a smartphone for now, but they were the perfect form factor for a handheld PC on the go.

<figure><p><img src="https://rubenerd.com/files/2024/hand386@1x.jpg" alt="The Hand 386" srcset="https://rubenerd.com/files/2024/hand386@2x.jpg 2x" style="width:400px; height:400px;" /></p></figure>

Funnily enough, we go back further in time for a more modern device. A few years ago, the Hand386 burst onto the retrocomputer scene, seemingly fuelled by the sucess of the unusual Book 8088. Like the OQO, this tiny machine didn't have a hinge or folding mechanism, instead placing a rigid keyboard underneath a fixed display. Only this time it didn't have a contemporary CPU, but an honest to goodness 80386! This retrocomputer fan is still kicking himself that he didn't get one before they too ceased production.

I don't blame companies for not making these devices; I suspect the target market is too niche, and their use case too unusual. But I'd still love to get one. Not a hinged PDA, or an upscaled Android phone, an actual computer in a slab form factor I can use on the train. Christian Kersting has already [hinted at one](https://uni64.com/shop/index.php?item_id=7470) that could be used to do some Commodore 64 retrocomputing which is exciting, and definitely something I'll be exploring.

If anyone knows of other such devices, I'm all ears.
