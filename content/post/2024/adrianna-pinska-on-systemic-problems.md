---
title: "Adrianna Pińska on systemic problems"
date: "2024-07-31T07:48:47+10:00"
year: "2024"
category: Software
tag:
- issues
- luddites
location: Sydney
---
I saw this [reposted on Mastodon](https://hachyderm.io/@confluency/112563385686089192) from back in May, and it's excellent:

> This Recall thing is a prime example of how bad we are at understanding when something is a systemic problem.
>
> It doesn't matter if *you* disable it. It doesn't matter if *you* install Linux. It doesn't matter if *you* set your computer on fire and move to a Luddite commune.
> 
> If you have *ever* sent sensitive data, no matter how securely, to another person who now has this shit enabled, and they find your data and look at it, your data is compromised, and there's nothing you can do about it.

It reminds me of the debate around [carbon footprints](https://en.wikipedia.org/wiki/Carbon_footprint#Shifting_responsibility_from_corporations_to_individuals). Framing it as an issue that people can just avoid running Windows ignores the forest for the trees (and I say as someone who doesn't run Windows). I was [flamed hard](https://rubenerd.com/people-who-need-to-run-windows/) in April this year for suggesting a related issue.

As an aside, Adam Conover did a [great episode with Brian Merchant](https://www.youtube.com/watch?v=wJzHmw3Ei-g&pp=ygUVYWRhbSBjb25vdmVyIGx1ZGRpdGVz) about the Luddites, if you're like me and thought the term was derogatory.
