---
title: "I miss my paper theme"
date: "2024-10-28T08:44:51+11:00"
year: "2024"
category: Internet
tag:
- design
- weblog
location: Sydney
---
I switched to my current *Times New Rubenerd* theme earlier this year, because I wanted something new, and I secretly liked serif fonts. They render my thoughts the way a newspaper or book would, which is fun.

But I'll admit I [kinda miss my paper theme](https://web.archive.org/web/20230409040218/https://rubenerd.com/), which itself was based on my pseudo-Wikipedia theme from 2010-ish. I always loved how 11 to 12-point sans serif fonts rendered on \*nix desktops and macOS, and the shadows and paper motifs helped visually separate posts without needing a tonne of white space.

That's the biggest problem with themes you write yourself: you're [never satisfied 📺](https://www.youtube.com/watch?v=z9Lc080VPso "Michael Franks: Never Satisfied"). Or maybe that's just me.

A lack of discoverability was the biggest issue with that old theme, as many of you pointed out. I'm wondering if I can add some more context to posts in that one, like links to metadata and archives.

Right now I'm updating that theme with the latest Hugo syntax, particularly around pagination. We'll see.
