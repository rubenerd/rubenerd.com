---
title: "Music Monday: Otonablue cover by Liz"
date: "2024-06-24T16:15:21+10:00"
year: "2024"
category: Media
tag:
- erbloodflame
- hololive
- hololive-en
- jpop
- music
- music-monday
location: Sydney
---
Welcome to *[Music Monday](https://rubenerd.com/tag/music-monday/)*, the blog post series where I talk about a specific type of media on some indeterminate day of the week.

Hey, [remember Elizabeth Rose Bloodflame](https://rubenerd.com/erbloodflame-hololive-debut/) from the latest Hololive English generation? She did a cover of that legendary Atarashii Gakko! track from their 2020 album *Ichijikikoku*, and I've had it on repeat all afternoon!

<figure><p><a target=_BLANK href="https://www.youtube.com/watch?v=zZXW_iXIrss" title="Play 【COVER】OTONABLUE (Elizabeth Rose Bloodflame)"><img src="https://rubenerd.com/files/2024/yt-zZXW_iXIrss@1x.jpg" srcset="https://rubenerd.com/files/2024/yt-zZXW_iXIrss@1x.jpg 1x, https://rubenerd.com/files/2024/yt-zZXW_iXIrss@2x.jpg 2x" alt="Play 【COVER】OTONABLUE (Elizabeth Rose Bloodflame)" style="width:500px;height:281px;" /></a></p></figure>

