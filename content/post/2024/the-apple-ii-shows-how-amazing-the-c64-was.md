---
title: "The Apple II shows how amazing the C64 was"
date: "2024-06-29T08:24:38+10:00"
thumb: "https://rubenerd.com/files/2024/appleii-64c@1x.jpg"
year: "2024"
category: Hardware
tag:
- apple
- apple-iie
- commodore
- commodore-64c
- retrocomputers
location: Sydney
---
Last year I bought an [Apple //e Platinum](http://retro.rubenerd.com/apple.htm#apple-iie-platinum "The Apple //e Platinum on my retro site") and [FloppyEmu](https://rubenerd.com/ordering-my-first-3d-printed-part/ "Ordering my first nylon 3D printed part") to relive some early childhood nostalgia. I set it up on Clara's and my weird assortment of tables we call our computer nook, and have proceeded to use it more than any other 8-bit machine. It's where I hack on BASIC the most, try old software, and explore retro and modern add-on cards. Sometimes it just runs *FISHIES* while we're working, to give us something cute to look at.

I cannot overstate how much *fun* this machine is. It's an all-in-one box of retro goodness, from its expansion slots and thocky keyboard, to its specific artefact colour palette and beeps. My cheeks have hurt from the amount of smiling this machine has caused, and I can't wait to pay this house deposit so I can slowly build it up with more cards and capabilities. I almost went for a IIGS instead, but I'm glad I have something with a case that harkens back to that original Apple ][ Woz and his team put together. It's a living piece of computer history, back when they were designed to be expanded, fixed, and tinkered with.

My love for this machine beyond simple nostalgia was more of a surprise than I was expecting, because I'd always considered myself a "Commodore guy". I still do. The Commodore 64, C16, and Plus/4 were what got me into 8-bit machines when I was in high school in the 2000s, and I've since been given a C128 and bought a German VC-20. My "bookcase of joy" is basically Clara's and my shrine to Hololive and Commodore kit; two fandoms separated by almost four decades.

These Commodore machines are all special and unique in their own ways, but everyone knows the C64 is the standout. And the more I use the Apple //e, the more I've come to appreciate how groundbreaking it was for the time.

<figure><p><img src="https://rubenerd.com/files/2024/appleii-64c@1x.jpg" alt="My Commodore 64C and Apple IIe" srcset="https://rubenerd.com/files/2024/appleii-64c@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

The C64 came out five years after the Apple II, though they were being updated in tandem throughout the 1980s. My Apple //e was manufactured in 1986, one year before my slim Commodore 64C.

They're similar machines in many respects. Both use 8-bit, 6502-derived CPUs and system architectures. They can generate composite video output. These later iterations were introduced while their respective companies were spruiking their next-generation Amiga and Macintosh machines, yet people still kept buying them.

But there's really no contest in other respects. The Apple //e's artefact colour doesn't hold a candle to the C64's VIC-II hardware sprites and palette, and its rudimentary sound system is a far cry from the C64's legendary SID (Commodore's vertical integration was arguably both a blessing and a curse for the company, because while it let them build custom chips that knocked the socks off anyone else, Apple could use commodity parts and outsource improvements).

This is where the value proposition comes in. Cards like the Mockingbird could be bought to bring the Apple II line closer to the capabilities of the C64, and I'm keen to try one of the modern reproductions one day. But Commodore were giving it to you already with your C64, and for less than *half* the price. It's incredible to think about now.

Given I use my retrocomputers in exceedingly boring ways, I get more daily use out of the Apple //e's 80-column card than the C64's superior colour and graphics. When I get around to buying and installing one of those Z80 Softcards or AppliCards, I suspect I'll live in this machine even more. Yes, CP/M fanboys exist, and apparently I am one now.

But to be clear, if I were a shopper in the 1980s, I almost certainly would have bought a C64, or even a C128, over an Apple II. To use Apple's own advertising from the early 1990s, you simply got more for less. I can absolutely see why these machines imprinted on so many people, especially if you were a child at the time. Commodore's sound and graphics were spectacular.
