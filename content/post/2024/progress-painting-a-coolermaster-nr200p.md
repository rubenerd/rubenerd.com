---
title: "Priming a CoolerMaster NR200P case for painting"
date: "2024-05-09T09:19:29+10:00"
year: "2024"
category: Hardware
tag:
- computer-cases
- colour
- painting
- projects
location: Sydney
---
I've mentioned I'm moving my FreeBSD desktop out of a Fractal Ridge case, because its CPU cooling ceiling is too low for what I need. Rather than going out and buying a new case, I thought it'd be fun to paint our spare NR200P an "aged beige" to match our retrocomputers!

Armed with [some tips from JaysTwoCents](https://www.youtube.com/watch?v=zY8R7XFkda4 "How to paint a computer case. The Cheap and EASY way!") and a friend who does metal CNC'ing and painting, Clara and I went to our [local sausage sizzler](https://en.wikipedia.org/wiki/Bunnings#Sausage_sizzles) to buy some primer, a base coat, clear coat, and a gentle-grit sanding block to fix paint runs. I also got a cheap clothes rack to hang the parts from when painting, and some blue painters tarp so the apartment balcony wouldn't be covered in stuff.

<figure><p><img src="https://rubenerd.com/files/2024/painting-before@1x.jpg" alt="Parts from the NR200P before painting." srcset="https://rubenerd.com/files/2024/painting-before@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

The first step was to clean the case with some IPA, microfibre cloths, and compressed air. This was especially important, because this case had been used for a computer previously, and was quite dusty. The great thing about the NR200P was that all the parts of the case I wanted to paint simply popped off the frame, so I wasn't having to contend with painting an entire chassis. This includes the white mesh on the plastic top, which pops off by bending its metal tabs.

Next, I hung the panels on the clothes rack outside, and over the course of a few days built up thin layers of a dedicated self-etching primer, letting it dry overnight between coats. Jay was right, it's much easier to apply thin, even coats when the parts are upright. I lightly sanded the plastic top before adding primer, but I didn't bother with the metal panels.

<figure><p><img src="https://rubenerd.com/files/2024/painting-case-hanging@1x.jpg" alt="Photo showing the panels hanging on a cheap clothes rack, with a couple of thin primer coats." srcset="https://rubenerd.com/files/2024/painting-case-hanging@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

This deliberate, slow painting was the first deviation from how I've painted cases before, and it made a huge difference. Previously I used paint that was advertised as containing primer, and sprayed far too much each time. This lead to the paint taking more than a week to cure, and it ran in enough places that I had to sand back and redo large sections. I still ended up having a couple of runs here, but the layer was so thin that gravity took care of them by the time they dried.

It's been raining continuously in Sydney for weeks, so adding primer took a while! But I'm really pleased with the results so far. None of the original colour is showing through, and it's nice and even.

<figure><p><img src="https://rubenerd.com/files/2024/nr200p-primer@1x.jpg" alt="" srcset="https://rubenerd.com/files/2024/nr200p-primer@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

The next step is to choose a beige colour and begin painting. I had an old can of my Heirloom White from my last paint attempt, but it was too pale compared to the slightly yellowed parts I have on the desk. I'm leaning towards Chiffon Cream after [seeing this Vogons thread](https://www.vogons.org/viewtopic.php?t=66199 "Best off the shelf spray paint for beige computer cases").

Part two soon, weather permitting!
