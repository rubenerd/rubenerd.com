---
title: "Grounds first in the Clever Coffee Dripper"
date: "2024-12-26T08:55:10+11:00"
abstract: "James Hoffman recomennds hot water first, but I find it makes the coffee taste thinner, at least at my skill level."
year: "2024"
thumb: "https://rubenerd.com/files/2024/clever-grounds-first@2x.jpg"
category: Thoughts
tag:
- coffee
- food
location: Sydney
---
I defer to the expertise of [James](https://jamesg.blog/ "James’ Coffee Blog") and [James](https://www.youtube.com/channel/UCMb0O2CdPBNi-QqPk5T3gsQ "James Hoffman’s YouTube Channel") when they say they approach coffee in a certain way. But I think I might be taking a different tack from how *Hoffey Coffee* brews with the [Clever Coffee Dripper](https://cleverbrewing.coffee/products/clever-dripper), the steep and release brewer from Taiwan for which I now harbour more than a slight obsession.

I [talked about the Clever Coffee Dripper](https://rubenerd.com/trying-the-clever-coffee-dripper/") at the start of the month, and mentioned this step recommended by James:

> Poured 250 g of boiling water into the brewer, then tipped in <span style="white-space:nowrap">16 g</span> of coffee. This was discovered by [Workshop Coffee](https://piped.video/channel/UCNvZVxsHV0KTEASMxpmVIhg) to drastically improve drawdown time, without affecting flavour!

It's true: when you put hot water in first, *then* the coffee grounds on top, it cuts the time it takes for the water to be released by the brewer. Meaning the sooner you can have tasty coffee first thing in the morning, and everyone's happy.

But it's that second part about not impacting flavour I'm not sure I agree with. I think putting hot water in first *does* affect taste in a slightly negative way, at least at my current skill level.

When I put grounds in first, as I would on a Hario V60, the hot water being poured on top agitates the bed of grounds and helps it to mix. By the time I've measured out the exact weight of water required, I give the slurry a gentle stir to break up any dry clumps. When I brew, I get a nice, flat bed of grounds and almost no sediment on the filter paper whatsoever:

<figure><p><img src="https://rubenerd.com/files/2024/clever-grounds-first@1x.jpg" alt="Clever Coffee Dripper, with grounds first, showing a lack of grounds on the paper" srcset="https://rubenerd.com/files/2024/clever-grounds-first@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

When I pour water in first, and *then* the grounds, the grounds naturally want to float on the surface of the water. It takes a bit more effort to mix this, and in the process, some of the coffee on the surface of the water naturally accumulates on the edge, where it sticks to the paper. It's wet and sticky, and in my experience difficult to extricate without mild scraping. By that point though, I feel like I've agitated the coffee too much. The gentle swirl I do at the `02:00` minute mark also doesn't help that much.

I haven't done a blind test, so take these with a dropper of saline solution. Here comes the proverbial posterior prognostication: *but...*, I've noticed coffee made by pouring water in first, and then the grounds, to be slightly thinner in taste. I suspect its down to the grounds stuck to the edge not participating in the brew.

Longer drawdown times aren't that much of a factor for me. My morning routine is to let it draw down while I'm setting up the laptop on the balcony, or packing my bag for the commute to work. If I were standing there waiting, I expect a shorter time would be preferable.

There are a bunch more variables to tweak here. It could be my gentle stirring is still too aggressive, even if I mix it with clockwise and counter-clockwise motions to cancel out any centrifugal force. Maybe I still need to dial in my grind settings slightly. Maybe I need to somehow be even more gentle when pouring the ground beans into the water.

All I know is, right now, with my current technique and kit, I think I make tastier coffee by pouring the water on top of the ground beans, not the other way. Give it a try if you have one of these coffee brewers.
