---
title: "Some wood for Firefox"
date: "2024-03-18T11:02:34+11:00"
year: "2024"
category: Software
tag:
- colour
- firefox
- design
- pointless
location: Sydney
---
There's something so delightful/silly/pointless about using a wood theme in a browser. The material is making a comeback in so much interior design and architecutre, why not lend a bit of texture to your browser too?

<figure><p><img src="https://rubenerd.com/files/2024/firefox-wood.png" alt="" style="width:500px; height:213px;" /></p></figure>

The one I'm currently using is [HORIZONTAL wood](https://addons.mozilla.org/en-US/firefox/addon/horizontal-wood/ "HORIZONTAL wood on the Firefox Add-Ons site") by bloochiz12, which is likely not their real name. It's not real wood either, so it all works out. Where was I going with this?

I used a stylised wood theme in the SeaMonkey suite for years, but I can't remember what it was called. There's something fun about writing email on a wooden desk, or at least an analogue of one.
