---
title: "Sakura-themed PC parts"
date: "2024-05-04T12:24:31+10:00"
thumb: "https://rubenerd.com/files/2024/sakura-card@1x.jpg"
year: "2024"
category: Anime
tag:
- advertising
- design
- gpus
location: Sydney
---
I mentioned in my post about the otherwise-excellent [Peerless Assassin CPU cooler](https://rubenerd.com/peerless-assassin-120-versus-120-se-cooler/) that it has an awful name. Because it does!

Alas, it's not unique in this regard. So many modern PC parts lack things like taste, and marketing usually saddles them with names like *BALLISTICS* and *COLLATERAL DAMAGE*. I've almost avoided buying certain components so I don't have to spell them out on part lists.

This is why I'm always so surprised (and a bit relieved) when a manufacturer releases something that *isn't* like this. I'll bet half the reason reference graphics cards are so popular is down to the fact they don't look look like a gun, or have that awful marketing. It's also another benefit to buying workstation or server-class hardware; you'd be thrown out of a boardroom if your medical VM cluster proposal was powered by *MORTAR SHELL* components. 

<figure><p><img src="https://rubenerd.com/files/2024/sakura-card@1x.jpg" alt="Promo image of the Sakura-themed 4060, showing a waifu on the side." srcset="https://rubenerd.com/files/2024/sakura-card@2x.jpg 2x" style="width:500px;" /></p></figure>

Another example of hardware not named for war or injuries is mainland Chinese manufacturer Yeston, and their anime-themed *Sakura* cards. People *en masse* on forums&mdash;with profile pictures of guns and RGB setups&mdash;say they're cringe, without a trace of irony or self-awareness! Honestly, give me cutesy designs like this over an edgelord any day of the week.

I suppose the real lesson here is *to each their own*. Pardon me while I wipe the blood off my keyboard so I can upgrade to something with flowers instead. *WAIFU GUNSHOT WOUND... VENGEANCE.*
