---
title: "My current favourite terminal colour schemes"
date: "2024-01-18T08:58:40+11:00"
year: "2024"
category: Software
tag:
- colour
- design
location: Sydney
---
It's been a while since I've shared these! I spend a disproportionate amount of my life staring into either terminals or text editors, so I like them to be nice. I also tend to use different themes for different times of day.

My primary scheme is still [Pencil Light](https://github.com/mattly/iterm-colors-pencil), which Matthew Lyon adapted from iA Writer. It's bright, contrasty, and cheerful for daytime writing and development, but not overly garish.

<figure><p><img src="https://rubenerd.com/files/2024/theme-pencil-light.png" alt="" style="width:500px; height:200px" /></p></figure>

When it's later at night, or I have a bad headache, I like to switch to something with a bit less contrast. This was [Solarized Dark](https://ethanschoonover.com/solarized/) for the longest time, but recently I've switched to [Nord](https://www.nordtheme.com/). *Soothing* is the best word I could use to describe it.

<figure><p><img src="https://rubenerd.com/files/2024/theme-nord.png" alt="" style="width:500px; height:200px;" /></p></figure>

My favourite Vim theme is currently [Toast](https://github.com/jsit/toast.vim), despite my current abstention from refined carbohydrates. It's billed as medium-contrast, but I still find it easier to read than most.

<figure><p><img src="https://rubenerd.com/files/2024/theme-toast.png" alt="" style="width:500px; height:200px;" /></p></figure>

And for my Emacs installs, I still love the [Spacemacs theme](https://melpa.org/#/spacemacs-theme). I especially like the subtle backgrounds in some syntax, which I've shown with this cobbled-together Perl script. I only run vanilla Emacs, but Spacemacs does have some great goodies.

<figure><p><img src="https://rubenerd.com/files/2024/theme-spacemacs.png" alt="" style="width:500px; height:200px;" /></p></figure>

Both Toast and Spacemacs have dark themes too if need, though thesedays I tend to prefer to work in a warmly-lit room with light backgrounds.

I used to change colour schemes constantly, but these have all served me well for a while now. One day I'll do a colour theory course and codify the teal-forward theme I develop in while I'm dreaming, but not today. Don't read too much into that.
