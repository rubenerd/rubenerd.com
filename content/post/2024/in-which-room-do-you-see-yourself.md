---
title: "In which room do you see yourself?"
date: "2024-12-24T10:01:49+11:00"
abstract: "Are you a Plant Room person, or an RGB Room person?"
thumb: "https://rubenerd.com/files/2024/ikea-buying-guide@2x.jpg"
year: "2024"
category: Media
tag:
- colour
- design
location: Sydney
---
Famed Swedish furniture store IKEA had this buying guide on their website landing page:

<figure><p><img src="https://rubenerd.com/files/2024/ikea-buying-guide@1x.jpg" alt="Screenshot showing a darkened room with RGB lights, and another room bathed in natural light and plants." srcset="https://rubenerd.com/files/2024/ikea-buying-guide@2x.jpg 2x" style="width:500px; height:320px;" /></p></figure>

[I mentioned on Mastodon](https://bsd.network/@rubenerd/113700132310104425), but I'd far prefer to be in the Plant Room, not the RGB Room. Is there a Plant Room for computators?

As an aside, is a phrase with three words. I'm glad that gamers are keeping the DIY computer scene alive after the rest of the industry was commodotised. But meh on RGB. Why can't I get a computer case that has plants in it, rather than pulsing lights to appease Boy Racers? *Plants*, damn it!

I suppose wooden Fractal cases get halfway there... if they weren't the size of empty bar fridges.
