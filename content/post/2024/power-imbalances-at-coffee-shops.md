---
title: "Power imbalances at coffee shops"
date: "2024-09-25T22:18:27+11:00"
year: "2024"
category: Thoughts
tag:
- coffee-shops
- psychology
location: Sydney
---
We've had [crowd dynamics](https://rubenerd.com/crowd-dynamics-at-coffee-shops/) at coffee shops, and [social norms](https://rubenerd.com/social-norms-at-coffee-shops/). What about power imbalances?

The title of this post may suggest a coffee shop has sufficiency high electrical requirements to warrant having three phases delivered, one or more of which might be unbalanced. This can cause all manner of problems, though in some ways they'd be easier to solve than other such issues with a similar name.

I was sitting at a coffee shop yesterday&mdash;surprising nobody&mdash;when an older gentleman and a young woman sat at the table next to me. I surmised they must have come from an important business meeting, based on their formal attire and conversation. The man was either her manager, or someone senior in the business. I've seen them here more than a few times, so they must work around here, or come often for meetings.

After discussing whether or not they'd succeeded in convincing the client of something, the older gentleman proceeded to tell some of the more inane stories I'd ever heard, interspersed with some racist jokes (she was a Chinese Australian, like Clara). *Boomer energy*, as the kids say. It was clear he thought he was imparting some serious wisdom, with the delivery of a sage, comedic genius. The younger woman laughed and nodded at the appropriate times, though he never let her get a word in during his breathless monologue.

As I prepared to leave myself, the man rose to pay the bill, and I caught the eye of his colleague. Her eyes widened, and she beamed at me before slumping into her chair and pinching the bridge of her nose. I shook my head in silent sympathy, with an embellished shrug that I hope communicated "some people, amirite!?"

I hope she's not dealing with that all the time; or if she is, that she has an exit. The worst is when you're dealing with someone who wields more influence, control, or rank in a situation, and you have to grit your teeth and take it. Privilege&mdash;earned or otherwise&mdash;is when you can walk away from such a situation.
