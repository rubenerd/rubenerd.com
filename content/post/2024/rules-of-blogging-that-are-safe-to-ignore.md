---
title: "Rules of blogging, some of which can be ignored"
date: "2024-05-23T08:48:13+10:00"
year: "2024"
category: Internet
tag:
- blogging
location: Sydney
---
Back when blogging was something regular people did, there were self-anointed experts who wrote guidelines on how to write, coached as "rules". They were often quite funny, because they'd invariably contradict the rules put down by other such experts!

There were a few broad themes though, and I've broken all of them over the years.

* Be as **specific with dates as possible**. Don't say "earlier this month", say "on the 9th of May". I used to do this, but I don't think people care.

* **Limit yourself to only writing about one primary topic**. I've written before that it's been fun being drawn into a site with one topic, and then being exposed to other stuff. Also, write what *you* want to write.

* **A blog isn't a blog without** comments or other `${FEATURES}`. Meh. Again, write how you want to. Some of my favourite blogs are static HTML pages linked together by date. I make *one* exception to this, described below.

* **Be consistent**. Absolutely not! If you want to write once a day, or once a year, it doesn't matter. The saddest thing I see is someone I otherwise used to love reading or watching committing to a schedule, and phoning in stuff to meet that schedule. I read a lot of technical blogs and material for my job, and it's *painfully* clear when someone's heart isn't in it.

* **Don't ramble**. This is a stylistic choice. If you like rambling posts with digressions and pointless references, go for it. It clearly hasn't stopped me.

* **Avoid use of the exclamation mark**. Whoops!

* **Only link if you have something positive to say**. That's probably a healthy way to live one's life more broadly, but sometimes linking to people you're critiquing *is* warranted. In the worst case, you can use `rel="nofollow"` if you don't want to advantage them in search metrics.

* Rules on keeping “[content](https://rubenerd.com/patrick-willems-discusses-content/ "Patrick H Willems discusses “content”")” fresh, expanding your engagement, writing headings with keywords, and **other SEO hacks**. These used to be a great way to churn out stuff to generate money, but LLMs have you beat here. It's not worth writing now unless it's something you *want to do*, which perhaps is a silver lining to the modern web.

On the other hand, *you have different fingers.* This still holds true:

* **Don't plagiarise words**. Whether you're copying someone else's text without attribution directly, or laundering it via an LLM, you're being a dick. You're also potentially breaking the law, or at least someone's licence.

And advice I never saw, but I personally think is important:

* **Performance**. Do what you can to run on something that runs smoothly, and don't overload your site with crap. My rule of thumb is: if you're not sure if your site needs something, it probably doesn't.

* That said, **have an RSS or Atom feed**! Your CMS likely supports it already. A web feed turns your blog into something that can more easily interact with the independent web, and improves accessibility.

* Have an idea of **who your target reader is**. My target for the longest time was my mentor and friend [Jim Kloss](https://rubenerd.com/a-letter-to-jim-kloss-a-year-on/ "A letter to Jim Kloss, a year on"); someone ethical and friendly with a technical background, but whom might not be aware of the absolute latest trends in the industry.

* **Ignore anyone who tells you how to write**. Yes, that includes me. See it as a guide, but chart your own course. That's the whole point.
