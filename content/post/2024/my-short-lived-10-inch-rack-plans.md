---
title: "My short-lived 10-inch rack plans"
date: "2024-10-11T20:03:42+11:00"
thumb: "https://rubenerd.com/files/2024/10-inch-2u-mini-itx-case@1x.jpg"
year: "2024"
category: Hardware
tag:
- homelab
- servers
location: Sydney
---
I've talked a lot recently about my research into [homelab server racks](https://rubenerd.com/researching-home-lab-racks/) and [clusters](https://rubenerd.com/building-a-homelab-cluster/). But I was also reminded today that 10-inch racks exist. If my primary school mathematics taught me anything, it's that a 10-inch rack is a full 9 inches narrower than a standard 19-inch server rack.

*(I still can't conceptualise most archaic units, though 12-inch records and 15-inch monitors do help me visualise how big a 10-inch rack is. Not that big, in other words)!*

10-inch racks are generally used for comms cupboards and the like. Their smaller size means you can more easily mount them to walls, weather proof them for outdoor installations, and locate them in cramped places. Hey, like an apartment.

This [8-port Amdex patch panel](https://www.wiltronics.com.au/product/63007/8-port-amdex-patch-panel-cat6/ "Wiltronics: 8 Port Amdex Patch Panel Cat6") gives you a sense of just how narrow a 10-inch rack is:

<figure><p><img src="https://rubenerd.com/files/2024/amdex-panel-cat6-8-port@1x.jpg" alt="Photo of the small Amdex 8-port patch panel, with little extra room on either side giving an indication of how small this is." srcset="https://rubenerd.com/files/2024/amdex-panel-cat6-8-port@2x.jpg 2x" style="width:423px; height:100px;" /></p></figure>

Alas, while they're optimal for perhaps a cute cluster of single-board computers [or 1L tiny PCs](https://www.etsy.com/au/listing/1410413576/10-inch-rack-mount-for-lenovo "HiveTechSolutions: 10 Inch Rack mount for Lenovo ThinkCentre M Series Tiny PCs"), there's a dearth of general-purpose server cases. [MyElectronics](https://www.myelectronics.nl/us/10-inch-2u-mini-itx-case.html "MyElectronics: 10 inch 2U Mini-ITX 10-inch-2u-mini-itx-casecase") sell a cute 10-inch chassis, though at most it supports two SSDs:

<figure><p><img src="https://rubenerd.com/files/2024/10-inch-2u-mini-itx-case@1x.jpg" alt="Photo of the short, squat MyElectronics 10-inch case." srcset="https://rubenerd.com/files/2024/10-inch-2u-mini-itx-case@2x.jpg 2x" style="width:500px; height:167px;" /></p></figure>

My dream setup would be a 10-inch rack with a 2 or 3U server chassis holding 8 NVMe or equivalent sleds, and the money to replace all our dense spinning rust with them. I guess that'll have to wait for another day.

In the meantime, is a phrase with three words. This is why I'd *love* to get into 3D modelling and printing one day. Provided you designed it properly, I could see something similar to an HPE Microserver layout in a 10-inch rack-mountable form factor with four or eight regular drives.
