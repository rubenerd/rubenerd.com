---
title: "Happy August everyone"
date: "2024-08-01T09:45:59+10:00"
year: "2024"
category: Thoughts
tag:
- numbers
- pointless-milestone
location: Sydney
---
We're `perl -e 'print 8/12' ==> 0.666666666666667` of the way through the year, depending on your level of precision.

This has felt like a long year, and an exceedingly short one. Again, depending on your level of precision.

*Update:* [kivikakk](https://kivikakk.ee/) pointed out this would only apply at the *end* of the month, not the start. Whoops.
