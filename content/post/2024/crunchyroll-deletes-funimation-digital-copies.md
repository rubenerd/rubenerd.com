---
title: "Crunchyroll reneging on Funimation Digital Copies"
date: "2024-02-14T08:46:07+11:00"
year: "2024"
category: Anime
tag:
- streaming
location: Sydney
---
I've talked a lot here about how I've given up on music streaming services, though the same mostly applies for video. Streaming platforms give artists a pittance, they revoke even purchased material on a whim, and you can't have local copies. They're a bad deal for everyone but the distributor, and I'd rather spend my money elsewhere.

The latest example of this is Funimation, the streaming platform that [promised in their FAQ](https://www.funimation.com/digitalcopy/) that online *Digital Copies* of their optical discs would be available *forever*. The validity of such a claim is already dubious; up there with *unlimited*, *100% uptime*, and *Rubenerd Certified*. But even by the fast-and-loose standards of modern advertising and customer contracts, this was destined to fail.

And it did! Funimation was recently brought under the same corporate umbrella as Crunchyroll, which have <a href="https://help.crunchyroll.com/hc/en-us/articles/22843839604500-Funimation-End-of-Services" title="Crunchyroll FAQs" rel="nofollow">no intention</a> of honouring these customer agreements:

> Please note that Crunchyroll does not currently support Funimation Digital copies, which means that access to previously available digital copies will not be supported.

Also note the use of the term *currently*, a popular tactic by service providers to imply they *may* support something at some indeterminate point. Without a concrete timeline or commitment in writing, it's fluff.

This is yet another cautionary tale about purchasing music and shows from online services. Any site that doesn't let you immediately download and archive without DRM is a [rental](https://mastodon.social/@herrbischoff/111906006115026490 "Marcel Bischoff responding on Mastodon"), irrespective of how much you pay, or what assurances you're given. Your access to what you bought is completely at the discretion of a business who's incentives and motivations are not aligned with yours.

It's high time these streaming platforms be held to account for this sort of behaviour. Isn't there a financial term for entering into a business transaction with terms you have no intention (or capacity) to honour?

