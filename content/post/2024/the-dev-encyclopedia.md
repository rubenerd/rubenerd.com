---
title: "The Dev Encyclopedia"
date: "2024-08-21T14:03:35+10:00"
year: "2024"
category: Internet
tag:
- development
- documentation
- perl
location: Sydney
---
I saw [The Dev Encyclopedia](https://devpedia.pages.dev/) making the rounds this morning:

> Find out what that Sr. Developer is talking about.

Sounds like fun!

I knew almost all of these, save for some of the machine language stuff. I was disappointed it was devoid of æ and [camels](https://www.perl.org/), though.
