---
title: "The 1983 JVC A-K100 stereo amplifier"
date: "2024-07-24T17:08:15+10:00"
thumb: "https://rubenerd.com/files/2024/jvc-amp@1x.jpg"
year: "2024"
category: Hardware
tag:
- 1980s
- hi-fi
- music
- retro
location: Sydney
---
Clara and I are now the proud owners of this cute **JVC A-K100**, pictured in the middle of our robot media cabinet below. We bought it for $70 [sic!] from a gentleman in Melbourne, and we love it!

<figure><p><img src="https://rubenerd.com/files/2024/jvc-amp@1x.jpg" alt="" srcset="https://rubenerd.com/files/2024/jvc-amp@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

The JVC A-K100 is a simple stereo amplifier manufactured in Japan in 1983. The branding mentions its *High Fidelity Gm Circuit* in several places, which [Peter Vis describes](https://www.petervis.com/gallery/Vintage%20Advertisements/jvc-high-fidelity-components/jvc-dynamic-super-a.html) as a noise-reduction system. Cool!

It has:

* 25 W output per channel into 8 Ω stereo speakers

* Four stereo inputs, including a single tape loop

* An LED panel to display power level

* Sliders for bass, treble, and L/R balance

It also ships with a traditional volume knob instead of a large slider or buttons, which became popular later in the decade with companies *including JVC.* We briefly had a TEAC amplifier which had a similarly-wonderful early 1980s aesthetic, but the volume buttons were a PITA.

Online reviews on Hi-Fi Engine [praise the audio quality](https://www.hifiengine.com/manual_library/jvc/a-k100.shtml) of the A-K100, especially considering its small size. I only cosplay as an audiophile, but it sounds *wonderful* driving our Mission bookshelf speakers. We also have our Pioneer graphic equaliser connected via the tape loop, which when paired with this amp, makes older tapes, worn records, and badly-mastered modern CDs sparkle.

Being from the early 1980s, it's also delightfully tactile. The power button has a satisfying *thonk*, the input selection buttons are more fun than an input dial, and the sliders feel substantial. That one stereo Rotel every JB Hi-Fi store sells in Australia has satisfying controls, but it's ten times the price, and doesn't feature a glorious 1980s LED panel.

<figure><p><img src="https://rubenerd.com/files/2024/jvc_a-k100b_stereo_integrated_amplifier@1x.jpg" alt="" srcset="https://rubenerd.com/files/2024/jvc_a-k100b_stereo_integrated_amplifier@2x.jpg 2x" style="width:500px;" /></p></figure>

What may come as a surprise is that the JVC replaces a hulking Marantz which, by all accounts, was far superior. Their sound, build quality, and history have earned Marantz amps a cult following, whereas I doubt no coffee table books will be written about JVC receivers.

Unfortunately, that power and size came at a cost. The Marantz was twice the size, and despite a recent service, still got hot enough to fry an egg. I thought that was normal for such amplifiers&mdash;all my dad's amps growing up were gigantic space heaters&mdash;but this JVC only gets slightly warm to the touch after hours of use. This is great for Australian summer, but also for power use and longevity by reducing thermal stress.

I was able to sell the Marantz for *far* more than I spent on this JVC, so we've come out ahead, and with something better suited to our small bookshelf speakers and listening habits.
