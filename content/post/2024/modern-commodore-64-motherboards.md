---
title: "Modern Commodore 64 motherboards"
date: "2024-01-10T08:32:48+11:00"
year: "2024"
category: Hardware
tag:
- aldi-commodore-64
- commodore
- commodore-64
- commodore-64c
- retrocomputing
location: Sydney
---
A year ago I talked about [wanting to build a VIC-20](https://rubenerd.com/things-id-love-to-build-this-year/) from modern parts. I ended up finding and restoring a beautfiul [shortboard VC-20](https://rubenerd.com/restoring-my-1983-commodore-vc-20/ "Restoring my 1983 “Aldi” Commodore VC-20") instead, then found an ["Aldi" C64 case and keyboard](https://rubenerd.com/my-new-aldi-commodore-64/ "My “Aldi” Commodore 64 case") that now needs a motherboard. *Whoops!*

The good news is, the Commodore 64 is such an iconic and treasured machine that we're spoiled for choice when it comes to reproduction boards. As I discussed in my [authentic retrocomputing](https://rubenerd.com/what-counts-as-authentic-retrocomputing/ "What counts as authentic retrocomputing") post, there's a gradient depending on how original you want to go.


### Faithful reproductions

* [Refurbished boards](https://www.ami64.com/product-page/fully-refurbished-c64-motherboard-with-sid-chip-options "Fully Refurbished C64 Motherboard with SID Chip Options"). I've linked to *Ami64* because I trust them, but there are many other options here. I include it for completeness, despite not being a modern board.

* [bwhack's 250407](https://www.pcbway.com/project/shareproject/The_C64_250407_replica.html). This is the most faithful reproduction of an early C64 motherboard I've seen. The Gerber files and a bill of materials are [also available on GitHub](https://github.com/bwack/C64-250407-Replica-KiCad). Open source hardware is awesome!

* [The "SixtyClone" by Bob's Bits](https://www.tindie.com/products/bobsbits/sixtyclone-commodore-64-replica-pcbs/). These are some gorgeous reproductions of each major C64 generation, and come with optional component kits and clear, silk-screened instructions. These seem to be the most popular boards when you read or watch people building modern C64s.

* [DIY Chris 250407](https://diychris.com/product/commodore-64-250407-replica/). Another 250407 revision board, this one with additional PCB colours, an RF bypass circuit for clearer pictures, native GAL support, and a reset jumper among other improvements. You can also order some of the components you'll need.

* [ICS64S by CBM Stuff](https://www.cbmstuff.com/index.php?route=product/product&product_id=85). Another beautiful reproduction board with several quality-of-life improvements including an internal IEC header for an SD2IEC slot, and a Kernal ROM switcher. Surface-mount components also come pre-soldered.


* [C64 Reloaded MK2 by Individual Computers](https://icomp.de/shop-icomp/en/produkt-details/product/c64-reloaded-mk2.html). This board replicates the C64 in function, though it differs in design. The standout feature for me are the zero-insertion force (ZIF) sockets, which would make testing new parts much easier.


### Hybrid or modern reimplementations

* [EVO64](https://retrospective.shop/evo64-rev1/). A reimagining of the original C64 with soupued-up audio, video, and other improvements. This is probably the *best* Commodore 64 you could build today.

* [The Ultimate64 Elite](https://ultimate64.com/). This is an FPGA implementation with a complete motherboard and modern IO that can fit into an existing C64 case. It even has Wi-Fi.


### More?

If you know of another to add to this list, let me know!
