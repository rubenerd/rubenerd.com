---
title: "Ben Sidran, On the Cool Side"
date: "2024-08-26T23:41:23+10:00"
thumb: "https://rubenerd.com/files/2024/yt--gzXSei6JjI@1x.jpg"
year: "2024"
category: Media
tag:
- ben-sidran
- jazz
- music
- music-monday
location: Sydney
---
This week's *[Music Monday](https://rubenerd.com/tag/music-monday/)* is the titular song from Ben Sidran's 1985 album *On the Cool Side*. It was Ben's first album to make use of electronic instruments, or so his [Wikipedia article](https://en.wikipedia.org/wiki/On_the_Cool_Side) says. I'd know, because I wrote it back in the day!

This is one of those songs that really helps me when I'm in a blue funk, or struggling mentally with things. The lyrics, delivery, and musical style always put a spring back into my step, especially after a rough day.

> For example, take me, standing right here today.
>
> I would not be here with you, if I believed the things that The People say!
>
> Keep it on the cool side ♫

<figure><p><a target="_BLANK" href="https://www.youtube.com/watch?v=-gzXSei6JjI" title="Play Ben Sidran - On the Cool Side"><img src="https://rubenerd.com/files/2024/yt--gzXSei6JjI@1x.jpg" srcset="https://rubenerd.com/files/2024/yt--gzXSei6JjI@1x.jpg 1x, https://rubenerd.com/files/2024/yt--gzXSei6JjI@2x.jpg 2x" alt="Play Ben Sidran - On the Cool Side" style="width:500px;height:281px;" /></a></p></figure>

