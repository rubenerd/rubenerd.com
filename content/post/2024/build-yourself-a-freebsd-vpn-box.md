---
title: "Build yourself a little FreeBSD jail services box"
date: "2024-01-20T09:36:08+11:00"
year: "2024"
category: Software
tag:
- bsd
- freebsd
- jails
location: Sydney
---
This is a short and silly post, and one that should probably have just gone on Mastodon. But if you're the kind of person who reads a blog like this, you owe it to yourself to run a little services box somewhere.

My recent consolidation of a few different VMs into the one with jails and a Wireguard VPN has made everything so much easier. Clara's overseas at the moment, so I was able to transfer our Minecraft server to a jail and deliver it securely to her. I wanted to trial a few file backup tools, so I spun up jails for Seafile and Syncthing. I even have a jail with different OpenZFS settings to trial some database optimisations; a topic for another post, but there are already some surprising findings.

It's amazing how much stuff you can create when you make your life easy. Get yourself a FreeBSD services box with jails and/or bhyve. It reduced the barrier to entry for *so much stuff*.
