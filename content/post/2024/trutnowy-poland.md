---
title: "Trutnowy, Poland"
date: "2024-02-27T08:21:13+11:00"
year: "2024"
category: Thoughts
tag:
- architecture
- europe
- poland
- wikipedia
location: Sydney
---
I love clicking on Wikipedia's *[Random Article](https://en.wikipedia.org/wiki/Special:Random)* link, and seeing where it takes me. Today we arrived in Poland, and the northern village of [Trutnowy](https://en.wikipedia.org/wiki/Trutnowy "Trutnowy article on Wikipedia")\:

> Trutnowy is a village in the administrative district of Gmina Cedry Wielkie, within Gdańsk County, Pomeranian Voivodeship, in northern Poland. It lies approximately 3 kilometres (2 mi) west of Cedry Wielkie, 13 km (8 mi) east of Pruszcz Gdański, and 19 km (12 mi) south-east of the regional capital Gdańsk.

I looked it up on [OpenStreetMap](https://www.openstreetmap.org/relation/12565025 "Relation: Trutnowy"), and it's really not that far from the Baltic Sea. The nearest major city is [Gdańsk](https://en.wikipedia.org/wiki/Gda%C5%84sk "Gdańsk article on Wikipedia") to the north-west, with which I'm well familiar thanks to photos of its stunning urban architecture.

<figure><p><img src="https://rubenerd.com/files/2024/trutnowy@1x.jpg" alt="Photo by Polimerek" srcset="https://rubenerd.com/files/2024/trutnowy@2x.jpg 2x" style="width:500px; height:351px;" /></p></figure>

Speaking of which! The article includes a photo of this beautiful old house, taken by [Polimerek on Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Podcieniowy_dom_we_wsi_Trutnowy_2010.jpg). I'm wondering if Clara and I could try building a simplified version of it on our Minecraft server.

Damn it, now I want to go back to [Alchemy](https://www.alchemy-restaurant.com/ "Alchemy restaurant in Sydney").
