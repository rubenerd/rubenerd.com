---
title: "The science (or not) behind smartphones and anxiety"
date: "2024-04-01T09:51:35+11:00"
year: "2024"
category: Thoughts
tag:
- anxiety
- mental-health
- phones
location: Sydney
---
This is a correction on my part regarding a train of thought I started last year.

For some background, I've noticed that as I [use my smartphone less](https://rubenerd.com/time-spent-looking-at-my-phone/ "Time spent looking at my phone"), uninstall certain social media applications, turned off notifications, and become more deliberate about when I let it into my life, I noticed huge improvements in my anxiety. I've found my resting heart rate is lower, I can concentrate more easily, and my general mood is better.

Mikan Oshidari [recently wrote about a similar journey](https://www.japantimes.co.jp/commentary/2024/03/18/japan/smartphone-addiction-japan-flip-phones/ "How switching from a smart to a flip phone saved me") for the Japan Times, emphasis added:

> Gradually, I became more and more dependent on my phone. I would look at it whenever I could, checking social media constantly, worrying that I had **missed something important** posted on my feeds. I hated having even the slightest bit of free time, so whenever I had nothing to do, I would **browse absentmindedly**, endlessly scrolling, whether I was on the train, in the bath, or on the toilet. Even during class. But that was normal, everyone was doing it.

This FOMO is real, especially when it comes to world events. Getting back into RSS helped here too, because I can check the news in my feed reader when I want, not when a fucking algorithm shoves something at me. The absentmindedness of it is also key: you're not getting anything out of your phone when you use it like this.

> Switching to a “dumbphone” cured my addiction and I have felt so much happier since. I have regained the mental space to enjoy my free time, as well as my ability to concentrate. Of course, it is time-consuming to have to use a computer or tablet when I need the internet, but I don’t mind. **I have learned to value my time more**. [...] So much so that I decided to write a book (in Japanese) about my experience. 

Related to this is the idea that slack time is something to be "filled". I've been told similar things by older family friends who quit smoking: you begin to associate downtime with that destructive activity. Appreciate your time, and you disarm one of these triggers.

Problem solved then, right? I've read enough of these experiences, and had them myself, to think these anecdotal stores are symptomatic of a wider issue, and that everyone could stand to benefit from a more deliberate approach to smartphone use.

But here comes the twist. In a Nature review of Jonathan Haidt's latest book, Candice L. Odgers points out that the [science doesn't support a link](https://www.nature.com/articles/d41586-024-00902-2 "The great rewiring: is social media really behind an epidemic of teenage mental illness?") between teen anxiety and social media use:

> These are not just our data or my opinion. Several meta-analyses and systematic reviews converge on the same message. An analysis done in 72 countries shows no consistent or measurable associations between well-being and the roll-out of social media globally. Moreover, findings from the Adolescent Brain Cognitive Development study, the largest long-term study of adolescent brain development in the United States, has found no evidence of drastic changes associated with digital-technology use. Haidt, a social psychologist at New York University, is a gifted storyteller, but his tale is currently one searching for evidence.

In other words, our phones reinforce and amplify mental health issues in unproductive and dangerous ways, rather than being the cause. Candice cites the scientific and medical literature for her conclusions here, so I defer to her expertise.

I'm not a teenager, nor did I grow up on social media. But this has still been a learning experience. While people like Mikan and I predisposed to such anxiety have seen benefit reducing our use of these slabs of lithium and glass, there's also more going on here. Part of me isn't entirely surprised.
