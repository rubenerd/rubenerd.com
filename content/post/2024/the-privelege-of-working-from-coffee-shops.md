---
title: "The privelege of working from coffee shops"
date: "2024-06-14T08:20:09+10:00"
year: "2024"
category: Thoughts
tag:
- coffee
- finances
- life
location: Sydney
---
Earlier in the week I talked about [why coffee shops were magic](https://rubenerd.com/why-coffee-shops-are-magic/), at least to me. I said the majority of posts I've written on this site for nearly twenty years were drafted and/or published while sitting at a handful of these places, because they're the most enjoyable, mentally stimulating places to be. When home or family life was tough, coffee shops were a valuable [third place](https://en.wikipedia.org/wiki/Third_place).

But this comes with a steep caveat, and it's something a few of you pointed out. Drinking at a coffee shop is *expensive*, especially if you do it every day, or every other day.

Clara and maintain detailed budget spreadsheets, and I can see there are weeks where I've spent AU $35 (about 22 €) just on coffee shops. Yikes. That's upwards of $140 a month, or $1,820 a year. We spend less than that in reality, but it's still stark to consider what it could be, and what opportunity cost that represents.

We save for international holidays, and occasionally splurge on something silly like a retrocomputer or weeb stuff. But we don't have debt, and save/invest about half of what we earn (how else are you supposed to get a mortgage in this economy!?). That's as much a function of the incomes we're lucky to have though... and the fact I've been reliably told we live "boring" lifestyles. Personally, sitting with Clara on a Friday night playing Minecraft and sharing a pot of tea is just about the most wonderful thing ever.

None of this is to say a coffee habit isn't expensive or an indulgence, but I guess it comes down to priorities. I see them more as a coworking spaces or a desk rental that serves a nice drink, but that doesn't change the price.

Anyway, thanks to those of you who pointed this out. The fact I hadn't even considered the cost in that earlier post is a luxury I've definitely taken for granted.
