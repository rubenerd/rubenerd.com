---
title: "What are they doing to keep you?"
date: "2024-06-08T07:19:53+10:00"
year: "2024"
category: Ethics
tag:
- enshittification
location: Sydney
---
This post was originally going to discuss a specific software feature that has everyone up in arms, but I realised it was a broader point.

People always want to talk about an entity losing someone. A politician losing the youth vote. A software vendor losing the confidence of their customers. A website losing their contributors through enshittification.

Often times, these losses are attributed to a specific decision, feature, or change, but in my experience it's usually the last in a line of updates that finally pushes people over the edge.

Detractors are quick to throw up their arms and claim people are shortsighted or wrong. Maybe they are, maybe they aren't. But the follow-up question is rarely asked: what did the entity do to keep people?

Years ago I remember talking with a friend who's parents were going through a divorce. During one of the family counseling sessions&mdash;I'm sure they were a bunch of fun&mdash;the psychologist didn't ask the abusive partner what they'd done to push their family away, but what they'd been doing to keep them around. She said her parent gaped, and couldn't provide an answer.

I think it's a useful lens. What *are* they doing to keep you around? Loyalty? Good service? The network effect? Vendor lock-in? 

If you don't have a good answer, that's also an answer... for both parties.
