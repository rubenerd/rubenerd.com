---
title: "Michał Sapka’s new home"
date: "2024-10-28T08:59:58+11:00"
year: "2024"
category: Internet
tag:
- friends
- writing
location: Sydney
---
Prolific writer, emacs aficionado, and friend Michał has consolidated many of his previous disparate thoughts into one site over on [CrysSite](https://crys.site/), with its own [RSS feed](https://crys.site/index.xml). This is the more rational thing to do, as opposed to me [sharding off](https://rubenerd.com/ruben-coffee-is-now-live/) something else for no good reason!

I especially like the visual representation of his site structure on the sidebar. It's clear and easy to follow.

I've [updated my blogroll](https://rubenerd.com/blogroll.opml), but thought you should all know.

Also, happy (belated) birthday!
