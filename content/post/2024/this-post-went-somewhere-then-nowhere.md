---
title: "This post went somewhere, then nowhere"
date: "2024-04-19T17:56:06+11:00"
year: "2024"
category: Thoughts
tag:
- pointless
location: Sydney
---
This was one of those posts I started writing, but in the process of putting text to console, I changed my mind about part of it, and figured out something much more interesting. Or at least, interesting to me.

**Text to console** is my way of saying **pen to paper**. I say it because I'm technically **typing** this, and because **writing** anything for more than a few sentences makes my hands cramp like someone with cramped hands. In fact, exactly like someone with cramped hands, because it cramps my hands. Even the one that isn't writing, somehow. Maybe its pity cramps. In my hands.

What I meant to say, I'll think of a better line regarding text and writing in the future. Maybe. Hey, anything is possible. Except for things that aren't, like dancing on Saturn wearing a cheese hat. Currently.

If you'll stop interrupting me, the original intent of this post was to think about currents in cheese. Ooh, like a cheese board. A... *shacuterie* board? How do you even spell that? I was only made aware of their existence recently, though they can hold far more than a simple collection of solidified dairy products. With currents. Which I could definitely go for now. Go for? Gopher? Gemini?

If there was a point to this post, I've lost it. It's gone. Kaput. It's not even just a resting parrot, it probably never existed in the first place. Are you here for five minutes, or the full half hour?

I'll admit by this point I usually bin posts like this and start from scratch; it's why I have all my published posts on a public Git repository, but I dare not ever commit my drafts folder. It would be admitting to people that this regular cadence of posts I produce aren't inspired words of brevity and wit, but are generally cut down versions of much longer posts of rambling nonsense. 

If you're surprised by *any* of this, you're kind to say so.

I've also invested at least five minutes writing whatever this is here. Damn it, **typing**. I literally spent a paragraph attempting to define the difference for no reason other than the fact I thought it was erroneous to refer to typing as writing, and I'm not even sure I necessarily believe that. It was a joke, which has now become the entire post.

I apologise. Go read Brain Braking today. Damn it, [Brain BAKING](https://brainbaking.com/). Named for its writer, Sir Wouter Baking. Baker Street is named for him too, as is his brain. Most of that wasn't true, but it should be. Why? Because this sentence has six words. *Shacuterie.*
