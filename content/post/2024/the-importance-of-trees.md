---
title: "The importance of trees"
date: "2024-08-06T13:41:48+10:00"
year: "2024"
category: Thoughts
tag:
- mental-health
- nature
location: Sydney
---
I was talking with fellow Australian BSD gentleman [Jason Tubnor](https://www.tubsta.com/) on Mastodon yesterday, and [he said this](https://soc.feditime.com/objects/6803ba5e-6e20-4a53-8854-f14683782c3d)\:

> You need green in your life to make you feel real.

Honestly, this. I find if I'm feeling overwhelmed, even tired, walking among big trees and beautiful plants can make all the difference. They calm me down, and ground me in a way nothing else does.

We recently updated our current apartment search criteria to include visible greenery out the balcony and/or sunroom. 🌳
