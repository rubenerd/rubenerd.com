---
title: "Getting an ATI 9600 XT AGP card"
date: "2024-01-04T07:48:52+11:00"
year: "2024"
category: Hardware
tag:
- ati
- dell-dimension-4100
- graphics-cards
- retrocomputing
location: Sydney
---
I didn't realise I had so many Windows 98-era gamers among you here, after I posted my [recent AGP question](https://rubenerd.com/researching-buying-my-first-agp-gpu/ "Researching buying my first AGP GPU"). I suppose I shouldn't have been surprised!

Based on overwhelming feedback, I managed to find an [ATI 9600 XT AGP card](https://www.techpowerup.com/gpu-specs/radeon-9600-xt.c30 "TechPowerUP"). Based on the budget SKU RV360 processor, it had midrange performance but stood out for its energy efficiency. It was among the last cards that didn't need an external power connector, which may prove necessary for the small(ish) PSU in my Dell Dimension 4100. It should also offer a nice bump from the Rage 128 Pro.

The seller is in Croatia (*bok!*), so it might be a while before the kangaroos are delivering it here. But I thought I'd post about it first to thank everyone for your $0.02.

Another contender was the Nvidia FX 5500; a much-maligned card at the time, but has found a niche among retro enthusiasts today for its broad compatibility and price. Phil on the other side of the continent [did a great video about this 📺](https://www.youtube.com/watch?v=rc4vivgEriU "GeForce FX 5500 for Windows 98 Retro PC Gaming") back in 2020.

I also have to send a shoutout to TechPowerUp for their detailed specification pages for this older hardware, especially for this generation of ATI cards.
