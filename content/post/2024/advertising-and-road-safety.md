---
title: "Advertising and road safety"
date: "2024-01-16T08:29:54+11:00"
year: "2024"
category: Thoughts
tag:
- advertising
- safety
- travel
location: Sydney
---
Speaking of cars, I overheard a great conversation at this coffee shop this morning from a motorbike group; paraphrased:

> I was riding around Gordon. Did you see the massive billboards on the overpass? It's a bloody massive TV screen. It literally has moving images and video.
> 
> Why do they put video on these things? To. Draw. Your. Attention! Which means you're not looking at the damned road!

I've been seeing more of these billboards around Sydney recently, but I was miffed over the visual pollution they pose. I hadn't even considered they were a safety risk.

Motorbike riders have to be keenly aware of safety in a way car drivers don't, because the crumple zone is their jacket. I'd take any safety concerns they have seriously.
