---
title: "Things you’ll hopefully never need to use"
date: "2024-12-13T17:29:38+11:00"
abstract: "A collection including remote replicas, emergency phones, and your wallet at mediocre coffee shops."
year: "2024"
category: Thoughts
tag:
- lists
- pointless
location: Sydney
---
* The emergency phone button in a lift, train, bus, aircraft, blimp, or spaceship capsule.

* That ZFS send/receive target at a remote location, because your primary array blew up in a power surge, because your UPS isn't what it was cracked up to be. Or maybe, it was *cracked too much*. Wait, what?

* Universal healthcare and/or insurance.

* Your remote HaST, DRBD, or WAL replica, as per above.

* An acoustic screwdriver and/or allen key. This was the first move where I assembled flatpack furniture using electric screwdrivers. *Oh wow*, my hands aren't cramping up this time!

* A streaming music service playlist. Or at least, use it while you can before they swap or disable half the tracks, and not pay the artists.

* Public rest rooms, basically anywhere outside Japan, or select areas of Singapore.

* The purchase button on a website selling cryptocurrency scams, or the accept button on some genAI guff.

* The spare tyre and/or air pump for your car and/or unicycle. That'd be tragic losing a tyre on a unicycle, it'd be nothing but a seat on a post.

* Your wallet at a mediocre coffee shop.

* A soup spoon to scoop ice cream, peanut butter, or other solid comestibles for which it wasn't designed, because it's the last clean spoon and the rest are in the dishwasher.

* An ancient DSA OpenSSH key, because the server you're remotely troubleshooting hasn't been updated in 20 years, yet is somehow still operating and suddenly your responsibility.
