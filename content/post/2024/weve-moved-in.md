---
title: "We’ve moved in!"
date: "2024-11-22T13:04:58+11:00"
abstract: "We bought a house, got financing done, replaced stuff, and now we’re in! AAAAAAH!"
year: "2024"
category: Thoughts
tag:
- life
- personal
location: Sydney
---
**AAAAAAAA!**

A year of house hunting, applying for finance pre-approval from a cooperative, legal adventures that threw a massive and unexpected spanner in the works (maybe a topic for another time), interstate travel, putting in offers, anxiety attacks, weekend after weekend of inspections, having an offer accepted, negotiating, conveyancing, government paperwork, sorting out the mortgage, waiting for settlement, getting the keys, having electrical work tested, levelling the floor, replacing the ancient carpet with wood/hybrid, having the walls painted, having the unit cleaned, booking leave from work, reserving the loading bays in the old and new apartment buildings, having utilities connected, packing, sorting out council rates, more packing, moving fragiles by hand, did we mention packing, and getting the movers in. Now we're in this apartment full of boxes, flat-pack furniture, cables, and a blinking fibre modem.

It doesn't seem real; it's so... anticlimactic?

As a Reddit critic of my blog once said, there's "nothing interesting, special, or especially noteworthy" about this. And he's right; people do this around the world, all the time. I've lived in more cities and homes than I can count, and have the range of packing boxes and bruises to prove it. Then I have my close Ukranian friends who had to leave their homes *under duress*; something I couldn't imagine.

📦 📦 📦

[As I said in August](https://rubenerd.com/house-inspections/)\:

> Apologies for the lack of posts over the last few days; Clara and I have been on our feet all day every day doing house inspections [&hellip;] I suspect in a few months we’ll be looking back at this and having a good laugh about it! Maybe I’ll revisit this post in December.

I'd laugh, but I think I'm too tired!

All I can say is, we now have a base, that's ours! Here's hoping we can stay here for a nice long while :').

With apologies to all of you, I know this has been a silly and self-absorbed series of posts. I know my writing has suffered this year while I've been distracted with all this. Thanks for your support! We'll be back to our regularly-scheduled bizarre mix of BSD, retrocomputers, technical communication, Japan, Singapore, and Australia soon. *Shiok, mate!*
