---
title: "Group assignments and expectations"
date: "2024-11-12T08:28:23+11:00"
year: "2024"
category: Thoughts
tag:
- life
- studies
location: Sydney
---
Did you go to university, polytechnic, or a similar institution? If so, do you remember... *group assignments?* No two words combined strike as much fear in the minds of undergraduates in my experience, other than perhaps *pop quiz*.

I worked with some standout people during some of my group assignments, including [Vadim](https://jamiejakov.lv) and Clara. But the majority of group assignments I did for computer science and IT were like pulling teeth. They were *terrible*, and not in a good way like diet Dr Pepper.

*(I asked one of the attendants for diet Dr Pepper on a United flight from San Francisco to Sydney a few years ago. I can still hear her mid-western accent booming back at me: "...but why!?").*

There were many reasons why people like me hated group assignments, but generally it boiled down to work allocation. I somehow always became the group lead, which roughly translates to "you do all the work"... some of you I'm sure are nodding your heads recalling a similar experience. Occasionally I'd be matched with someone who was willing to split half the work with me in a wider group, but it wasn't unusual for me to be carrying several concurrent assignments for three to eight people. 

*(The few times I let someone else manage the project, they downscaled our A2 diagrams to A4 to save a few dollars at the print shop, rendering weeks of work illegible. For another assignment, the person simply didn't submit it in time, and when they did, it was missing half the sections. We barely scraped a pass out of both, and I don't think we even deserved that).*

Now you could say this was a result of a lack of managerial experience, which is probably true. Group assignments are as much preparation for the real world as they are tools of educational oppression. All I knew at the time was that I'd allocate work to people, I'd follow up five times, get printouts from Wikipedia or nothing at all, the deadline would approach, and I'd give up.

Cynicism aside, my views of such projects changed dramatically after a fateful assignment with someone doing their masters. During one afternoon preparing a report, he mentioned that group assignments weren't frustrating because other people were lazy or uncommitted, but that there was a dichotomy of expectations. There were those of us that wanted at least a distinction, grouped with people who chanted *P's Get Degrees!* It wasn't that they were palming work onto us (well, most of the time), it was that they thought our efforts were unnecessary. They got their degree either way.

That *was* preparation for the real world. There are those who don't see the point of putting in the extra effort for something, because they don't value it, or they have something else going on. That's probably fine, provided people are upfront with their expectations from the start. In a professional setting, it's one of the key indicators for whether I'll want to work with someone.
