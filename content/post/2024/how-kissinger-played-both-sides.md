---
title: "How Kissinger played both sides"
date: "2024-05-06T07:51:29+10:00"
year: "2024"
category: Thoughts
tag:
- politics
location: Sydney
---
The *[Behind the Bastards](https://www.iheart.com/podcast/105-behind-the-bastards-29236323/)* podcast is one of my guilty pleasures. Hearing Robert and his guests absolutely *eviscerate* terrible people with meticulous writing and humour is balm to a troubled soul.

With the [welcome passing of Henry Kissinger last year](https://rubenerd.com/henry-kissinger/), the team released all six episodes of their Kissinger series as a two-parter ([one](https://www.iheart.com/podcast/105-behind-the-bastards-29236323/episode/czm-rewind-kissinger-parts-1-3-138228148/), [two](https://www.iheart.com/podcast/105-behind-the-bastards-29236323/episode/czm-rewind-kissinger-parts-4-6-138701479/)). There is so much going on, but I think is probably the most important point to understand why he was so dangerous:

> Part of what made Kissinger remarkable was his ability to rope conservatives in line for mass murder, while also charming the liberal establishment of the east coast. Nixon's chief of staff later recalled:
> 
> > We knew Henry is the hawk of hawks in the Oval Office. But in the evenings, a magical transformation took place. Touching glasses at a party with his liberal friends, the belligerent Kissinger would suddenly become a dove, and the press, beguiled by Henry's charm and humour, bought it. They just couldn't believe the intellectual smiling, humorous, Henry the K, was a hawk like "that bastard Nixon".

This is easily what made him one of the most terrifying people of the last century. Christopher Hitchens' book on the man is on my bedside table book pile, though I suspect I may need to read it during the day.
