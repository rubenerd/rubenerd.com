---
title: "The Who: Back to serif again"
date: "2024-12-09T08:48:51+11:00"
abstract: "I’ve changed my theme back to a serif font after feedback from all of you. Also, The Who."
year: "2024"
category: Media
tag:
- fonts
- music
- music-monday
location: Sydney
---
After writing the title for this post, I can't get that [Who song](https://piped.video/watch?v=_NzLs-xSss0 "Won’t Get Fooled Again") out of my head. *Back to Serif... again?* Hey look, another inadvertent [Music Monday](https://rubenerd.com/tag/music-monday/), courtesy of a train of thought that made no sense.

What was I talking about again?

I posted this [almost a month ago](https://rubenerd.com/quality-of-life-changes-week-45-2024/ "Quality of Life Changes, Week 45 2024") to the day:

> The serif fonts have been retired. It was fun, but they weren’t as legible on non-HiDPI/Retina displays. You might need to clear your cache or press SHIFT when refreshing to see any change.

I was surprised by the number of you who also expressed disappointment from this (I was too). [Torb](https://www.torb.no) finally convinced me to give it another shot.

Right now I'm back on *Georgia*, but I'll do some research into fallbacks and what's available on \*nix and Windows. Torb suggested I use `@media` queries in CSS to optimise for resolutions and viewports too, which is a great idea. The *Liberation* and *Nimbus* families are my favourite on \*nix, though their serif fonts are more *[Times New Roman](https://rubenerd.com/is-it-safe-to-like-times-new-roman-yet/ "Is it safe to like Times New Roman again?")* analogues than Georgia. *Mad Who organ riff on repeat.*

Remember *Bookman Old Style?* I did all my high school assignments in that. I loved it for its serif flourishes, and for taking up more space on a page. If you tell a student there's a font size and page length requirement, they're going to figure out ways around it. I remember Ms Gravina giving me full marks once on an English project, and commenting that she respected my chutzpah.

This post was a bit all-over-the-place, but at least it was rendered in serif fonts again. Assuming your RSS reader also uses serif fonts, or you're viewing my page having updated your cache. If not, this post was even sillier.
