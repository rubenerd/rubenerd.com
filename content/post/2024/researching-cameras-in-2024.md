---
title: "Researching cameras in 2024"
date: "2024-07-15T07:31:52+10:00"
thumb: "https://rubenerd.com/files/2024/fuji-x-s20@1x.jpg"
year: "2024"
category: Hardware
tag:
- photography
location: Sydney
---
I'm but a middling photographer, but I *love* the hobby, and it's one of the highlights of any trip. I'm also getting more into shooting video, which none of my aging kit does well. A new camera is *well* down the list of priorities right now, but a guy can dream.

This is my wish list:

* **APS-C or Micro Four Thirds**. These sensor sizes are within the sweet spot for performance, cost, size, and weight of the whole camera, including glass. Heavy, bulky cameras get left at home, which defeats the entire point of having them.

* **4K/50 or 60**. This would already be a *vast* improvement, but 10-bit would also be nice for editing.

* **Digital viewfinder**. I never got used to looking at a rear LCD, and it's much more ergonomic (to me) composing through a viewfinder.

* **In-body image stabilisation (IBIS)**. I shake, which is fine with a fast lens for taking stills, but not video. Help here is appreciated.

* **Decent lens range**. Ideally a sharp *f*/1.8 35mm prime, an *f*/2.8 pancake for long days, and a cheep and cheerful 18-55mm for everything else I care about. Availability of third-party glass would be a bonus.

* **USB-C** for data transfer and charging. I don't want to bring extra bulky stuff anymore.

Here's what I've been able to find.


### Sony α6700

I've never owned Sony kit before, but the α6700 seems to tick all the boxes and then some. The body weighs ~500g, it has 5-axis IBIS, does oversampled 4K/120, and it has additional dials compared to previous Alpha hybrid cameras (their sparse interfaces put me off before).

Reviwers have said the 26MP sensor and viewfinder are average, and that apparently nobody likes the styling. I kinda dig it, but I guess I'm weird.

<figure><p><img src="https://rubenerd.com/files/2024/sony-a6700@1x.jpg" alt="Sony a6700" srcset="https://rubenerd.com/files/2024/sony-a6700@2x.jpg 2x" style="width:500px; height:330px;" /></p></figure>


### Fujifilm X-S20

This camera is always mentioned in comparisons with the Sony, and it's easy to see why. For the same body weight it has all the features of the Sony, a viewfinder in the normal place, and that gorgeous Fujifilm design.

My only worry is the menu system. I've used Clara's Fujifilm kit before, and found the UI frustrating. Maybe it's improved though; I'll check in store.

<figure><p><img src="https://rubenerd.com/files/2024/fuji-x-s20@1x.jpg" alt="Fujifilm X-S20" srcset="https://rubenerd.com/files/2024/fuji-x-s20@2x.jpg 2x" style="width:500px; height:380px;" /></p></figure>


### Panasonic Lumix GH5-II, GH6, GH7

The Lumix GH line has been a favorite for mirrorless hybrid and video camears, and reviewers seem to love these latest iterations. They address the autofocusing issues of the earlier GH5, and would even be compatible with my small collection of Micro Four Thirds glass.

They're a bit bulkier, and more than 200g heavier than the Sony and Fujifilm though, which gives me pause.

<figure><p><img src="https://rubenerd.com/files/2024/lumix-gh7@1x.jpg" alt="Lumix GH7" srcset="https://rubenerd.com/files/2024/lumix-gh7@2x.jpg 2x" style="width:500px; height:425px;" /></p></figure>


### Other contenders

* The **Canon EOS R7** is more expensive than other cameras in this list, but has those legendary ergonomics and menu system. It's bulkier and 100g heavier than the Sony or Fujifilm though, and only gets 4K/60 with a crop. I'd consider if it were cheaper.

* The **OM System OM-1 II** would be my *dream* camera, given how my Olympus OM-D E-M10 II was the most fun, wonderful camera I've ever owned. But it's *far* more expensive for the same features I care about, which unfortunately I can't justify.

I'll post an update up in a... year, when I might be able to afford one. By which point I'm sure there will be other models to consider.
