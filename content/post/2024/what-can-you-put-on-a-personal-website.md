---
title: "What can you put on a personal website?"
date: "2024-02-17T11:26:21+11:00"
year: "2024"
category: Internet
tag:
- indieweb
- weblog
location: Sydney
---
Back in November last year, [@cutestgoth asked](https://twitter.com/cutestgoth/status/1727512834097959322):

> it feels like there are no websites anymore. there used to be so many websites you could go on. where did all the websites go

This generated a slew of soul searching posts, including [this fun one from Jason](https://www.fromjason.xyz/p/notebook/where-have-all-the-websites-gone/ "Where have all the websites gone?"). Point was, people did used to have websites. Now we have profiles.

Despite the best efforts of certain parties, it's almost as easy now to launch a simple, barebones personal site as it was thirty years ago. But people have been conditioned to think of the web in terms of engagement and value, not joy or interest. The fact people willingly call themselves *[content creators](https://rubenerd.com/stop-calling-people-content-creators/ "Stop calling people content creators")* now is proof.

But I digress. This is why I loved seeing [James' question](https://indieweb.social/@capjamesg/111943795422772259 "Post from Indieweb.Social") on Mastodon this morning:

> I commonly ask myself: what can I do next with my personal website? I'd love to make a list of things that people can do with their sites when they have a free moment. Comment and share some cool things you have done on your website!

Like me, James maintains an active blog. A blog is but one approach to maintaining a personal site; I love the format because I was avid journal writer as a kid. But it's by no means the only thing you can do.

The question kinda throws me back to GeoCities in the late 1990s. Kids around me at the time said they loved going to my silly site to see my Star Trek trivia, maps of places I'd walked, short stores, and lists of my favourite games. But when I'd ask why they didn't have their own site I could link back to, they'd say they had "nothing to say". It's the same thing I hear today from people who refuse to start a blog, but can spend hours writing posts on Facebook. It's as though people have convinced themselves they're not worthy of their own site, which is nonsense.

For want of a better phrase, my favourite sites back in the day were a bit like scrapbooks. They didn't see a website as a means to an end, or a way to establish clout for impressions, they were slapdash, fascinating collections of *things* tied together with a main menu of some sort.

For example:

* Lists of stuff, like one's camera gear, or watches, or powder compacts (my late mum's obsession).

* Maps of places one has visited, or loved, or aspire to travel to, or has bought coffee from, or craft beer, or anything.

* Reviews of books, anime, movies, shows, maybe even just a star rating or a thumbs up/down.

* Fan fiction! Remember that?

* Galleries of photos, or fan art, or colour palettes, or music album covers one has listened to. Maybe what you'd otherwise put on a photo sharing site, but you could tie them back to your maps, or to reviews, or anything!

* Emails people have received, a bit like a "letter to the editor". I used to get a kick out of fanmail and questions being published on a page like that. Or maybe going as far as having a guestbook?

* Guides or processes of how to build models, or kits, or personal projects.

* Scripts to run small games, or solve specific problems, or learning how to code.

Clara and I maintain our own personal retro-themed website on [Sasara.moe](http://www.sasara.moe/), and I codified my own list on the [Omake outline page](https://rubenerd.com/omake.xml) here. Both of these have been as much to write and curate as the blog of late, and they have the benefit of not being time-bound.


