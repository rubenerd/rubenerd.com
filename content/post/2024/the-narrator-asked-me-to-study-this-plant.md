---
title: "The Narrator asked me to study this plant"
date: "2024-01-09T07:31:29+11:00"
thumb: "https://rubenerd.com/files/2024/stanley-plant@1x.jpg"
year: "2024"
category: Software
tag:
- games
- stanley-parable
location: Sydney
---
<figure><p><img src="https://rubenerd.com/files/2024/stanley-plant@1x.jpg" alt="Screenshot from the office in The Stanley Parable during an Adventure Line journey. A fern sits in a planter by some shaded windows." srcset="https://rubenerd.com/files/2024/stanley-plant@2x.jpg 2x" style="width:500px; height:313px;" /></p></figure>

From *[The Stanley Parable](https://www.stanleyparable.com/)*, one of the greatest games of all time. It'll be easy with it as my background for a while.
