---
title: "What does your fork website say you do?"
date: "2024-05-13T09:57:22+10:00"
year: "2024"
category: Internet
tag:
- advertising
- design
- language
- open-source
location: Sydney
---
The trend among companies with open-source projects of late is to relicence them under their own bespoke terms, usually with licences that are incompatible with others. Elastic and HashiCorp are the most famous examples, though they're hardly unique. *ElashiCorp? HashiTic?* This is why I don't work in marketing.

Whether this is a reasonable reaction to people not paying to preserve the viability of such tools, or whether it constitutes a cynical bait and switch, is up to you. I'm not informed enough to comment, but based on terms like "rug pull" being thrown around, the court of public opinion has spoken.

This has lead to a series of forks for popular and established tools, all of which are keen to establish credibility with their new sites. They'll put the licences front and centre, along with variations of social compacts, manifestos, pledges, and guarantees. They may incorporate specifically as non-profits, or seek to establish themselves under the umbrella of entites like the Linux Foundation or Software in the Public Interest.

The fascinating part of this for me is seeing the sites themselves. Take a look at the first words in the banner for [OpenTofu](https://opentofu.org/), a slick new site for the excellent Terraform fork:

> The open source infrastructure as code tool.
> 
> Previously named OpenTF, OpenTofu is a fork of Terraform that is open-source, community-driven, and managed by the Linux Foundation.

It's clear what they want to prioritise with this message. It's followed by a list of features:

> Truly open-source   
> Community-driven   
> Impartial   
> Layered and modular   
> Backwards compatible

Beneath this are sections on how to contribute to the project, and a FAQ with six questions outlining the business and technical differences between it and Terraform. Here's the first one:

> OpenTofu is a Terraform fork, created as an initiative of Gruntwork, Spacelift, Harness, Baygle, Env0, Scalr, and others, in response to HashiCorp’s switch from an open-source license to the BUSL. The initiative has many supporters, all of whom are listed here.

As an aside, I added one additional stakeholder in that sentence. Can you guess which one? Sorry, I know it's silly, but I couldn't resist.

Back to the topic! There's just one small omission on that landing page, and it's not a cute logo (the best of us have these). I spoiled it in the title of this post.

What... does OpenTofu actually *do?*

How does it work? Who and what does it target (beyond existing Terraform users)? What are its technical features? How does it integrate? What's it compatible with? Are their paid support packages? How do you contribute?

The only mention is a passing comment about it being an *infrastructure as code* tool, but that's vague enough to be unhelpful. An engineer coming to this page without prior knowledge wouldn't have a clue. Compare this to HashiCorp's Terraform project landing page, and it at least spells out the intended use of the tool (among 23 uses of the word *infrastructure*, because *infrastructure* is *infrastructure* for *infrastructure*, です)!

This is what I see as one of the biggest challenges when it comes to forks like this: they define themselves (either primarily or entirely) as an alternative to something else. Spelling this out is probably necessary for attracting disgruntled people from the other camp, but your tool and project deserve more detail to appeal to new people as well. In the words of Merlin Mann, there's a person born every minute who hasn't seen *The Flintstones*.

As for that stakeholder I added? Think round foods, but misspelled for easier trademarking. I reckon there's a market for a bagel-as-a-service platform that fashions recipes with AI or something.
