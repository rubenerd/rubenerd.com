---
title: "A Perl guy learning Python3"
date: "2024-12-10T15:10:50+11:00"
abstract: "Things I’ve learned, and that have tripped me up."
year: "2024"
category: Software
tag:
- perl
- python
- programming
- ruby
location: Sydney
---
I've said it here before, and I'll say it again: I love Perl. I've used so many languages over the years, but writing Perl code feels natural. It feels like an extension of my brain. It's expressiveness, data structures, and near limitless flexibility meant I reached for it whenever I had a problem to solve (it's probably why I had fun with Ruby too, but that's a separate discussion).

I'm also keenly aware that I live in the Real World&trade;, and that much (if not most) of the tooling I use is written in Python (and Go). I'd dabbled in it before, but this is the first time I've committed to building some personal and work projects in it to get a feel for what makes it tick.

A few things that have surprised me:

* Variables can't be more local in scope than a function, which is terrifying. The pushback here is that *The Python Way* prefers concise functions anyway, so this shouldn't be an issue. I like being able to scope to a block in Perl though (or in basically every other language).

* The OOP features are nice. I got used to Moose, and I tend to write procedural stuff with data structures in lieu of OOP, but it's there and simple if I need it.

* `dict()` isn't a complete replacement for a Perl `%hash`, but it gets the job done. Quick string manipulation on the other hand: Python is still far inferor. And speaking of which...

* Strings are immutable. That threw me for a loop the first time.

* I'm used to `Text::Template`, but I'll admit Jinja is quite nice.

* It's true, the examples in the Python documentation really do reference Monty Python all the time. This is delightful, and I'm all for it. `#lifeofbrian`

* I kept hearing that *There's More Than One Way To Do It* in Perl, but that it wasn't the case with Python. Until I started looking at file systems, and there are dozens of ways to do things.

* Python 3.15 will be [moving to UTF-8](https://peps.python.org/pep-0686/ "PEP 686 – Make UTF-8 mode default") by default, which is awesome. Perl should do the same.

* `pip`, `easy_install`, whatever, it all sucks. Package management in Perl and Ruby weren't fun either, but I guess it's the devil you know. I cheated and used pkgsrc like I do for Perl and TeX, not that you read that.

* I miss sigals. You didn't need syntax highlighting when you could literally tell at a glance the difference between a `$scalar`, `@array`, `%hash`, and `&method`, or even `@scope` in Ruby.

* When typing `quit` in interactive mode, Python tells me that I need to type `quit()`. This is an interesting philosophical difference; Perl would just quit because it understands and acts upon intent, rather than pretending not to.

* Nevermind, Python 3.13 doesn't do that anymore!

* Meaningful whitespace doesn't bother me. I always indented my code correctly in Perl, so all Python does is remove boilerplate and reduce the amount of vertical scrolling I have to do. Dare I say, I think it looks beautiful.

It's been a lot of fun! I moved away from professional development years ago, but there's an itch that only writing code can scratch. I should probably blog about some of it.
