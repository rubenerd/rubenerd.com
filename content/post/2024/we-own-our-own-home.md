---
title: "We own our own home!"
date: "2024-10-20T09:59:44+11:00"
year: "2024"
category: Thoughts
tag:
- housing
- personal
location: Sydney
---
This time yesterday, Clara, my sister, my brother-in-law, and I were sitting on the floor of our new apartment in northern Sydney. The conveyancer and bank settled on Friday, Clara and I picked up the keys, and we all walked in together. It was a massive relief, and didn't seem quite real. We own a home now!? **AAAAAAAA!** 🏡


### The adventures

In true IT worker fashion though, *there was some fun!* Our bank was having a partial outage when settlement was supposed to occur, [because of course they were](https://en.wikipedia.org/wiki/Murphy's_law "Wikipedia article on Murphy's Law"). The conveyancer diligently filed all her paperwork, then we waited patiently while I can only imagine a stressed banker frantically flipped the bits required to make the transaction. It all worked out in the end, though we definitely double and triple-checked everything was correct. I half-expected to see one of those armoured cars drive up with massive sacks of cash instead, because an FTP server somewhere couldn't reconcile the numbers automatically.

I also don't think Clara and I really appreciated what we were getting into when we reached our agreed-upon savings goals last year. It wasn't just looking for a house, it was getting a feeling for different suburbs, endless lines of inspections, maintaining complicated spreadsheets of requirements and wants, weighing up what we could sacrifice to meet a specific location and price, realising everything we saw didn't quite tick the boxes, then doing it all over again the next weekend. Then the next weekend.

Is it an older apartment without too many expensive amenities and strata fees? Does it have a view of trees? Does it have at least two bedrooms? Is the front door set back from the rest of the unit so Asian and Asian-adjacent people like Clara and I can have a "shoe nook"? Does it have a balcony for morning coffee? Is it in liveable condition now, so we could take our time renovating if need be? Is it in a walkable suburb with amenities, coffee shops, and a train station? Is it a nice layout (aka, not a modern box)? Does it have reverse-cycle air conditioning already for Sydney winters and summers, or would strata be fine with a fitout? Is it double brick, or otherwise well insulated?

Meanwhile, we were applying for loan pre-approvals, doing some basic packing in the hopes we'd be moving soon, researching areas and processes, selling/consolidating some of our investment accounts, and uncovering some financial fraud which threw a spanner in the works and necessitated financial consultants and solicitors (a long story)! It took up all of our mental CPU cycles, which more than a few of you noticed given the drop off in quantity of posts here of late.


### Looking interstate

I mentioned to a few friends and readers, but we also looked interstate. One of our [recent trips to Melbourne](https://rubenerd.com/some-melbourne-coffee-reviews/ "Some Melbourne coffee reviews") was to look at apartments, and we also planned on going up to Brissie and Perth as well. I've lived in MEL and BNE, and they definitely come with pros and cons compared to $ydney. Huh, that's weird, I didn't mean to substitute a dollar sign there.

We discounted Brisbane almost immediately on price, but if you want inner-city living in Australia, Melbourne is a buyer's market right now, and there was some *gorgeous*, well-maintained places. Melbourne is great, and it would have been a lot cheaper, but we realised coming back to Sydney that we had too many roots and family connections here. Public transport in Sydney is also way better, [light-years ahead](https://www.timeout.com/melbourne/news/oh-dear-melbourne-has-ranked-below-sydney-for-public-transport-accessibility-according-to-new-research-071224) of anywhere else in Australia, and the development roadmap here will only see this gap widen in the coming years. I haven't talked about [the new Metro](https://www.intelligenttransport.com/transport-articles/91304/sydney-metro-a-game-changer-for-passengers/) much here, but it's Singapore-class transport in Australia which I never thought I'd see.

Truthfully though, this was all window dressing. The real reason was my sister and I spent [our whole childhoods moving interstate and overseas](https://rubenerd.com/unpacking-singapore-day-one/), the last of which was fairly traumatic for family reasons, so I wasn't in a hurry to repeat this. Clara was stressing out too, so we [decided to try Sydney](https://rubenerd.com/house-inspections/ "House inspections") if we could sling it.


### Back to Sydney

By comparison to *Marvelous Melbourne*, Sydney is decidedly *not* a buyer's market right now. It's the [second-most expensive city in the world](https://www.sbs.com.au/news/article/sydney-housing-worlds-second-most-expensive/e4sueurk6) after Hong Kong by some measures, which is so many levels of wild. The good news is apartments are far more reasonable than detached homes, provided again you keep a close eye on strata fees. I'm happy to write a longer post about it if people are interested.

*(It's also an easier job market for Clara's and my specific rolls to apply in, but that's also probably a separate post).*

It was a relief, and much less pressure, to be looking again at homes in the city where we already lived. There wasn't the mad rush to cram in as many inspections as possible into a weekend or two, and we could spread out and check more areas. Eventually we narrowed our search to one suburb near where we are now that was leafy, had lots of Asian culture and food, a large transport interchange, and a decent range of older apartment stock. Yes if you live in Sydney you probably guessed it. No, I'm not going to confirm it!


### Finding the place

After months&mdash;years&mdash;of planning and looking, the place we ended up with practically fell into our laps. What is it they say, luck favours the prepared? Whether that's true or not, we couldn't believe it happened here.

On the first day of inspections in our chosen suburb, we looked at a place that was already under offer, meaning a prospective buyer had paid a holding deposit. We thought we'd use it to get a feel for the area and building, but we were gutted by how much we loved it. Every apartment we looked at afterwards was measured against this first one. It was the only one that ticked 95% of our boxes; every other unit barely scraped by with 70%.

We were set to plan *another* weekend of apartment inspections, but out of the blue one afternoon the agent called informing us that the investors had pulled out of that first apartment, and that it was ours at the reasonable listed price if we wanted. An hour later, and I was on the phone with a conveyancer recommended by a colleague, and by the end of the week we had paid the deposit, got bank approval, and signed the contracts.


### Moving in

The place is ours now&mdash;well, okay, with a mortgage&mdash;so the next steps all involve cleaning, painting, replacing the floor, and so on. It's all a bit overwhelming, but also feels more like we're going through the motions, rather than the open-ended dance of inspections, offers, waiting, waiting, and more waiting.

Finally having some stability in our lives feels so foreign, and dare I even say, undeserved? But it's sorted now, and I can't express the level of relief. Fingers crossed we make a nice little nest out of this place for years. With special thanks to my colleagues Eric and David for all their help, my sister and brother-in-law for joining us on so many of our inspections, and for Clara for being that rock we all deserve to have in our lives.

As I've also said previously, I also apologise if you've sent comments without a reply. My head has been somewhere else for a while. Specifically, spreadsheets and 50+ email threads with conveyancers. I am *so* looking forward to thinking about BSD, travel, and basically anything else again!

