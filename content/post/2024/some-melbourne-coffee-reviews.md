---
title: "Some Melbourne coffee reviews"
date: "2024-03-31T09:46:26+11:00"
year: "2024"
category: Thoughts
tag:
- australia
- aviation
- coffee
- melbourne
- reviews
location: Melbourne
---
Clara and I didn't go out of our way to find great coffee around Melbourne, but we kept stumbling across it! Here were a small selection of our favourites, in no specific order.


### Miyama

We found this [Japanese-inspired coffee shop](https://miyamamelbourne.com.au/) while looking for something else on the top floor of Melbourne Central, and decided to check it out.

They use AllPress coffee which is good but not special, but the way they used it was. They advertised a *Magic* coffee which was a double-shot espresso with a specific ratio of steamed milk for that "magic ratio". I don't usually go for milk in my coffee, but this was exceptional.

<figure><p><img src="https://rubenerd.com/files/2024/melbourne-miyama@1x.jpg" alt="View of the entrance to Miyama." srcset="https://rubenerd.com/files/2024/melbourne-miyama@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>


### Hikari

This [tiny coffee shop](https://www.hikarilife.com.au/coffee "Hikari") did a range of espresso and batch brews, but I decided to try their Kenyan cold brew from the Gondo PB factory of the New Kiriti Farmers Cooperative Society. It was the best coffee I tried in Melbourne, and in the top three ever in Australia. *WOW*.

They also had a wall of trinkets from other coffee shops, including Blue Bottle that Clara and I went to in Japan and San Francisco!

<figure><p><img src="https://rubenerd.com/files/2024/melbourne-hikari@1x.jpg" alt="View of the counter at Hikari." srcset="https://rubenerd.com/files/2024/melbourne-hikari@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>


### Axil

[Axil](https://axilcoffee.com.au/) are a coffee roaster and small chain of shops around Melbourne that do some beautiful batch brews. By far my favourite was this Colombia Sebastian Gomez, which was rich, fruity, and incredibly mellow. I was sorely tempted to order a second one, had I not already had four shots of coffee already that day, including a Guatemalan right before (cough).

We also ordered some chocolate bread which was sublime. It's great when we're on holidays and getting thrice the exercise we usually do; it means we can indulge in stuff we usually avoid.

<figure><p><img src="https://rubenerd.com/files/2024/melbourne-axil@1x.jpg" alt="Yummy Axil coffee in a small flask next to a coffee cup." srcset="https://rubenerd.com/files/2024/melbourne-axil@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>


### Honourary Mentions

* [The Terrace](https://www.rbg.vic.gov.au/melbourne-gardens/functions-venues/the-terrace/) in the Melbourne Royal Botanic Gardens did a decent iced long black, but it was *wonderful* together with the gorgeous scenery.

* [Soul Cafe](https://www.mercurewelcome.com.au/dining/soul-cafe) down the street from our hotel. My heart went out to the service staff who had to cop it from tourists angry that public holiday surcharges were a thing. They were friendly, and their coffee was great!

* [Virgin Australia lounge](https://www.virgin.com/about-virgin/latest/take-a-peek-at-virgin-australias-new-melbourne-lounge) at Melbourne Airport (I get access via work). Coffee was a bit better than I was expecting, but the real draw is tarmac views for a couple of aeroplane nerds!

<figure><p><img src="https://rubenerd.com/files/2024/melbourne-lounge@1x.jpg" alt="View out some of the windows of the Virgin lounge of a 737-800." srcset="https://rubenerd.com/files/2024/melbourne-lounge@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>
