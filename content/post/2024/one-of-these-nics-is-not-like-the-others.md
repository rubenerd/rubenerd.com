---
title: "One of these NICs is not like the others"
date: "2024-04-04T21:28:43+11:00"
year: "2024"
category: Hardware
tag:
- networking
- silly
- shopping
location: Sydney
---
<figure><p><img src="https://rubenerd.com/files/2024/ebay-nics.jpg" alt="A screenshot from eBay showing three 3Com Etherlink PCI cards, priced $67.54, $1546.40 (wow!), and $19.95." style="width:500px; height:538px;" /></p></figure>
