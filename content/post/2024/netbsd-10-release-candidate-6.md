---
title: "NetBSD 10 Release Candidate 6"
date: "2024-03-22T08:51:31+11:00"
year: "2024"
category: Software
tag:
- bsd
- netbsd
location: Sydney
---
A sixth Release Candidate of my other favourite OS was made available on the 12th of March. [From the NetBSD blog](https://blog.netbsd.org/tnf/entry/netbsd_10_0_rc6_available)\:

> RC6 fixes a few issues with the new named/bind imported for RC5 plus several minor issues.
>
> If you want to test 10.0 RC6 please check the [installation notes](https://cdn.netbsd.org/pub/NetBSD/NetBSD-10.0_RC6/amd64/INSTALL.html) for your architecture and download the preferred [install image](https://cdn.netbsd.org/pub/NetBSD/NetBSD-10.0_RC6/images/) from the CDN or if you are using an ARM based device from the netbsd-10 builds from the [bootable ARM images](https://armbsd.org/) page.
> 
> If you have any issues with installation or run into issues with the system during use, please contact us on one of the [mailing lists](https://mail-index.netbsd.org/) or file a [problem report](https://www.netbsd.org/cgi-bin/sendpr.cgi?gndb=netbsd). 

This is old news for the NetBSD community, but I mentioned that I'd been [testing RC5](https://rubenerd.com/running-netbsd-10-rc5/) last month, so I felt like I should pass this on.
