---
title: "Gorgeous replica Australian Olympic outfits"
date: "2024-07-26T10:23:22+10:00"
thumb: "https://rubenerd.com/files/2024/olympic-sign@1x.jpg"
year: "2024"
category: Thoughts
tag:
- australia
- colour
- design
- swimming
location: Sydney
---
The Olympics are now underway in Paris, just on the off chance you didn't notice. I'm only really interested in table tennis, gymnastics, and badminton&mdash;you can take the boy out of Singapore, but you can't take Singapore out of the boy&mdash;but it's impossible to walk around in Australia without coverage of aquatics plastered everywhere. I think it's traditionally the country's strongest set of events.

This poster by Speedo at our local shopping centre made Clara and I do a double-take:

<figure><p><img src="https://rubenerd.com/files/2024/olympic-sign@1x.jpg" alt="Photo from a local shopping centre showing swimmers in official Olympic garb." srcset="https://rubenerd.com/files/2024/olympic-sign@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

The design of [these outfits](https://speedo.com.au/product/womens-aus-replica-printed-v-back/8_00429217809.html) are *so cool!* They have the national green/gold colours with Australian Aboriginal motifs. I want a print of this I can hang up:

> Designed with former Olympic Boxer Paul Flemming's 'Walking Together' first nation art, this piece represents the journey for the athletes along the way to the Olympics.

We checked out the Speedo website though, and of course, the female version is amazing, and the male version... isn't. Is there some unwritten rule somewhere that men, even mildly dysphoric ones such as myself (cough) are allergic to colour!?

<figure><p><img src="https://rubenerd.com/files/2024/olympic-designs@1x.jpg" alt="Shopping photos of the gorgeous female outfit, and a bland male one. Of course." srcset="https://rubenerd.com/files/2024/olympic-designs@2x.jpg 2x" style="width:500px; height:343px;" /></p></figure>

Actually now that I screenshot that, they're available for women, girls, and boys&hellip; but there isn't even one for men? *Phooey.*

As an aside, I can't wait to get a new gym membership sorted out when we move. Although where we're going, it'll need to be heated (cough). I've coughed a lot on this post, maybe I should get that (cough) checked out.
