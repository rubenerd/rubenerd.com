---
title: "The PA in Buzzy Explores the Airport"
date: "2024-03-16T07:23:24+11:00"
thumb: "https://rubenerd.com/files/2024/buzzy-airport.png"
year: "2024"
category: Software
tag:
- 1990s
- aviation
- humongous-entertainment
- games
- nostalgia
- scummvm
location: Sydney
---
*[Buzzy Explores the Airport](https://archive.org/details/AIRPORT95 "Archive.org's page to download Buzzy Explores the Airport")* is still such a fun game after all these years. You arrive at this virtual airport where you can wander around, see the luggage being sorted, board an aeroplane, and click everything in sight. It even runs on [ScummVM](https://www.scummvm.org/) on modern hardware.

<figure><p><img src="https://rubenerd.com/files/2024/buzzy-airport.png" alt="View inside the airport terminal in Buzzy Explores the Airport" style="width:318px; height:239px;" /></p></figure>

The game is full of easter eggs, but my favourites involve the terminal announcements that I didn't understand as a kid:

> Would a Mr D.B. Cooper please come to the Lost and Found? We found your parachute.
>
> Paging Mr Waldo. Mr Waldo, where are you?

That's what makes an enduring and epic game for kids: something that the parents and grown ups can get a chuckle from too.


