---
title: "The uConsole, as recommended by you!"
date: "2024-09-30T23:33:07+10:00"
thumb: "https://rubenerd.com/files/2024/uconsole@1x.jpg"
year: "2024"
category: Hardware
tag:
- commuting
- handhelds
location: Sydney
---
Can I just say how awesome you all are? Despite my current bout of social anxiety meaning I barely have the mental capacity to reply to my ever-increasing backlog of email comments and Mastodon posts, you all pull through and solve a problem for me within a day of writing!

On Sunday I talked about my desire for a [modern OQO or Hand386 portable machine](https://rubenerd.com/an-oqo-or-hand-386-successor-for-commuting/) I could use while standing on the train. I remembered the joy of sitting on the Singapore MRT as a kid with my Palm III or VAIO subnotebook, but wanted something a bit beefier and easier to use with one hand.

David Crawford, [Michał Sapka](https://michal.sapka.me/), Rebecca, and one of my regular anonymous Ukranian contributors all emailed to suggest I check out the [Clockwork uConsole](https://www.clockworkpi.com/uconsole):

<figure><p><img src="https://rubenerd.com/files/2024/uconsole@1x.jpg" alt="The uConsole, showing its flat form factor, small display, and clicky keyboard." srcset="https://rubenerd.com/files/2024/uconsole@2x.jpg 2x" style="width:415px; height:300px;" /></p></figure>

If the name sounds familiar, this is the same team that developed the cute [DevTerm](https://www.clockworkpi.com/devterm), a machine I was thinking about when I wrote that original post, but discounted for being *slightly* too big.

Let's run through the requirements I had in that post, and some more I thought of after I hit upload:

* Does it have a thumb keyboard? *Check.*
* Does it run a real OS, not a phone? *Check.*
* Is it a solid block, not a hinged laptop? *Check.*
* Is it user-serviceable? *Looks like.*
* Could I use with one hand? *Potentially.*
* Is it reasonably priced? *Check.*
* Is it... aesthetic? *Check!*

The default SKUs use varing versions of ARM64 Cortex chips, which should be decent for compatibility with the BSDs and Penguins, but there's also a provision for a RISC-V board as well. Epic!

Keep the suggestions coming in, I appreciate it. But I think I have a contender.
