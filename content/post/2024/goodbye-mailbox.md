---
title: "Goodbye, mailbox"
date: "2024-05-10T08:55:07+10:00"
year: "2024"
category: Hardware
tag:
- australia
- post
location: Sydney
---
I was getting my 10,000 steps one evening a few days ago, like a gentleman, when I noticed one of our familiar red Australia Post mailboxes had disappeared:

<figure><p><img src="https://rubenerd.com/files/2024/goodbye-mailbox@1x.jpg" alt="" srcset="https://rubenerd.com/files/2024/goodbye-mailbox@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

There are a few possibilities here.

I've noticed the parking spots in front of this mailbox are popular with SUV and American light truck drivers, which I've since been told are technically referred to as *Wankpanzers*. They've already done damage elsewhere in the suburb with their high bumpers and insecure drivers, so maybe a motorist drove their contraption into the mailbox and wrecked it.

Another possibility is that the building being renovated next door will be encroaching on the space, or needed the area clear for construction vehicles, or they needed access to a utility buried underneath. Note the paint marks on the tiles to the left and right of where the box once stood.

Otherwise, the only thing I can think of is that fewer people are sending letters, and the construction was an opportunity to remove the box for good. That's a bit sad, but probably unsurprising.
