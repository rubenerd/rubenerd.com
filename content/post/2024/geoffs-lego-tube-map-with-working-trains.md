---
title: "Geoff’s LEGO Tube map, with working trains"
date: "2024-08-05T21:08:58+10:00"
thumb: "https://rubenerd.com/files/2024/yt-9WCkRFYnHfM@1x.jpg"
year: "2024"
category: Thoughts
tag:
- london
- lego
- trains
- uk
- video
location: Sydney
---
This was *so* wonderful. My cheeks still hurt from smiling too much.

<figure><p><a target="_BLANK" href="https://www.youtube.com/watch?v=9WCkRFYnHfM" title="Play We Built a Giant LEGO TUBE MAP With Working Trains!"><img src="https://rubenerd.com/files/2024/yt-9WCkRFYnHfM@1x.jpg" srcset="https://rubenerd.com/files/2024/yt-9WCkRFYnHfM@1x.jpg 1x, https://rubenerd.com/files/2024/yt-9WCkRFYnHfM@2x.jpg 2x" alt="Play We Built a Giant LEGO TUBE MAP With Working Trains!" style="width:500px;height:281px;" /></a></p></figure>

