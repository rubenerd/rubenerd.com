---
title: "Blocked when using a VPN"
date: "2024-12-24T09:10:54+11:00"
abstract: "I always get blocked when using my own VPN, but not on commercial ones. HMM!"
year: "2024"
category: Internet
tag:
- security
- vpns
location: Sydney
---
I roll my own VPN servers for myself and my family because it's (relatively) easy, I retain more control, and I'm wary of the usual suspects for `$REASONS`.

But when you do, oh boy, be prepared for stuff to break! I managed to land on Reddit for a specific topic, but they immediately told on themselves that their crappy "network security" couldn't distinguish between me and a nefarious actor:

<figure><p><img src="https://rubenerd.com/files/2024/reddit-ban@1x.jpg" alt="A cutesy Reddit mascot telling me that I've been “been blocked by network security.”" srcset="https://rubenerd.com/files/2024/reddit-ban@2x.jpg 2x" style="width:420px; height:389px;" /></p></figure>

I could go to the effort of filing a ticket, or re-opening my deleted Reddit account to log in and/or get my aforementioned API token. Or I could close my browser tab and browse elsewhere. *So long, and thanks for all the fish!*

This is unfortunately all too common. The number of CAPTCHAs I have to fill out absolutely skyrockets when using my entirely pedestrian WireGuard or OpenVPN connections, across all manner of providers and IP ranges. Despite what those VPN advertisers may suggest, your use of a VPNs is trivial for web server software to detect.

This also leads me to suspect that these network security vendors must maintain lists of ranges in use by large VPN providers. I know people who use these services who don't encounter these problems. It's... interesting to think about the implications, especially if one is using a commercial VPN provider under the illusion that it offers total privacy. You may be more identifiable than you think.

As I've also mentioned before, it suggests any workarounds for geoblocking (a popular feature touted by commercial VPN providers) is permitted entirely at the discretion of the streaming company you're accessing. It'd be trivial for them to implement VPN detection and blocking, just as Reddit does here. The fact they don't suggests they're well aware of it, but are looking the other way... for now.
