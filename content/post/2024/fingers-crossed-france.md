---
title: "Fingers crossed, France"
date: "2024-07-04T08:27:15+10:00"
year: "2024"
category: Thoughts
tag:
- europe
- news
- politics
location: Sydney
---
🤞

**Update:** As I said on the [UK post](https://rubenerd.com/uk-elections-2024/), **wow!** *Vive la France!* 🇫🇷
