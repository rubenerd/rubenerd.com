---
title: "We just handed back the keys"
date: "2024-11-28T11:47:59+11:00"
abstract: "We’re officially not renters anymore!"
year: "2024"
category: Thoughts
tag:
- personal
location: Sydney
---
We're officially not renters anymore! 🔑

**Related:**

* [We've moved in!](https://rubenerd.com/weve-moved-in/)
* [We own our own home!](https://rubenerd.com/we-own-our-own-home/)
* [Goodbye to an old spreadsheet](https://rubenerd.com/goodbye-to-an-old-spreadsheet)
* [House inspections](https://rubenerd.com/house-inspections/)

**Unrelated:**

* [If you like piña colada~](https://piped.video/watch?v=YGAeI5KODLA "Rupert Holmes - Escape")
