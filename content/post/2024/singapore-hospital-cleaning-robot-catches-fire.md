---
title: "Singaporean hospital cleaning robot catches fire"
date: "2024-07-09T12:17:51+10:00"
year: "2024"
category: Hardware
tag:
- safety
- singapore
location: Sydney
---
[Channel NewsAsia](https://www.channelnewsasia.com/singapore/singapore-general-hospital-fire-cleaning-robot-charging-patients-transferred-no-injuries-4466456)\:

> SINGAPORE: Patients in a Singapore General Hospital (SGH) ward were evacuated on Monday night (Jul 8) after a fire broke out.
> 
> Preliminary investigations found that a cleaning robot caught fire while charging, setting off the fire alarm in ward 76 at about 7pm. 
>
> As a precaution, patients in ward 76 were moved to other ward areas due to the smoke. No injuries were sustained and most of the patients returned to the ward by 9pm.

Massive lithium batteries are scary. I'm [surprised](https://rubenerd.com/an-apartment-tower-of-lithium/ "An apartment tower of lithium") that such fires aren't a more frequent occurance.
