---
title: "Ron Gilbert on enjoyment/engagement"
date: "2024-01-12T08:19:51+11:00"
year: "2024"
category: Software
tag:
- design
- engagement
- enshittification
- mastodon
location: Sydney
---
[Via Mastodon](https://mastodon.gamedev.place/@grumpygamer/111721208984705790)\:

> There is no capturable metric for enjoyment. What products/games can capture is engagement and that is then misinterpreted as enjoyment. There are a lot of products I engage with because I want want they produce, but I am unhappy.

Cory Doctorow's *enshittification* principle renders this even more pernicious: these services *may* have been enjoyable for people at one point, but then the network effect is then used to wring it out when the priorities of the company move away from serving people.

Companies seem unable to discern enjoyment and engagement, or willingly conflate them. Even leaving aside how corrosive the idea of *engagement* is to the web, the problem is you sacrifice other metrics when you only optimise for one.

*(As an aside, working at a small cloud infrastructure company, our metric for success is how **little** time people spend on the portal. It's shocking how this changes everything from design decisions to priorities)!*
