---
title: "House inspections"
date: "2024-08-18T09:30:16+10:00"
year: "2024"
category: Thoughts
tag:
- life
- personal
location: Sydney
---
Apologies for the lack of posts over the last few days; Clara and I have been on our feet all day every day doing house inspections! Well, apartments specifically. It's a full-time job.

We've narrowed down to a few suburbs, streests, and places which feels unreal. Among the smarmy and slippery estate agents, we've also hit it off with a couple who buck the trend and have been helpful.

Everything feels like it's in flux right now, and there's a small part of us that feels like we've bitten off more than we can chew trying to change too many things at once. It also feels a bit unusual doing this in my late thirties, when others during inspections are in their early twenties with their parents, or those much older and looking for investments.

I suspect in a few months we'll be looking back at this and having a good laugh about it! Maybe I'll revisit this post in December.
