---
title: "SATA as a legacy connector"
date: "2024-09-02T13:32:58+10:00"
year: "2024"
category: Hardware
tag:
- storage
location: Sydney
---
Leviathan42 posed an [interesting thought experiment](https://forum.level1techs.com/t/rip-sata/215662) on the Level1Tech forums about the future of SATA:

> Noticed lately that consumer motherboards are omitting SATA ports in favor of more M.2 ports. Are we ready to classify SATA ports as legacy devices yet? wonder when we will see U.2/3 and M.2/3 become stand on consumer boards

I've noted the same trend. Classes of boards that previously had 8 SATA connectors are now lucky to ship with half as many. SATA Express ports failed to get any traction, eSATA hasn't been a thing for years, and more boards are shipping with multiple M.2 ports instead.

<figure><p><img src="https://rubenerd.com/files/2024/Serial-ATA-Logo.svg" alt="Serial ATA logo." style="width:240px;" /></p></figure>

I always read the *SATA* logo as *ASTA*... but I digress.

This move away from SATA is inevitable in some ways. SATA hard drives are still more cost-effective per gigabyte than solid state storage, but people are being convinced they don't need bulk storage in home in lieu of perpetual cloud subscriptions. M.2 (et.al.) SSDs are already comparable in price to SATA drives at the low end. SSDs that slot directly to boards require no cable mess, can achieve higher throughput, and take up less physical space.

External media was the only other class of device that required SATA, such as optical drives. Those have long since been considered obsolete, or in the case of Blu-ray media, relatively niche. Nobody outside nostalgic fools like me are still playing CDs or burning M-Discs. And even then, a USB 2.0 drive would offer sufficient transfer speeds, either in an external caddy, or with a SATA to USB adaptor on an internal drive.

SATA has been a dead-end in the data centre and workstations for a while in lieu of SAS-connected SSDs, hard drives, and LTO. Part of me expected that tech to eventually enter the home, but it seems the data centre is moving to direct PCIe, and gamers are using M.2/U.2 instead.

I'm not sure I'd be ready to write off SATA just yet though. For bulk storage at a relatively low cost, there's no competition. You can also get M.2 to SATA breakout boards, which both fascinate and terrify me. But again, where bulk storage is more important over speed, it could work.

I can still remember my first Athlon machine that used SATA instead of parallel IDE ribbon cables. I still have my IDE-to-SATA adaptors I bought so I could keep using my existing optical and hard drives. It still feels like a "new" tech to me for this reason, though it really isn't!
