---
title: "Researching buying my first AGP GPU"
date: "2024-01-01T09:23:00+11:00"
year: "2024"
category: Hardware
tag:
- dell
- dell-dimension-4100
- retrocomputing
location: Sydney
---
I haven't talked about it on the blog here, but I bought a [Dell Dimension 4100](http://www.sasara.moe/retro/#dell-dimension-4100 "Clara's and my retro-inspired web server") earlier this year, ironically for the same reason as the [Apple IIe Platinum](https://rubenerd.com/remembering-the-apple-iie-platinum-and-finally-finding-one/ "Remembering the Apple //e Platinum, and finally finding one!"). My high school was full of these beige towers, which despite being as dull as dishwater were still exciting for a kid like me to use.

I'll save a proper post for that, but for now I realised it's the first tower I've ever owned with AGP. I only ever had PCI, PCI-X, and now PCIe motherboards on the PC and Mac, so I missed out on this entire generation of graphics cards. And until today, I knew next to nothing about them.

Just as [VESA Local Bus](https://rubenerd.com/between-isa-and-pci-we-had-vlb/ "Between ISA and PCI, PCs had EISA and VLB") did with ISA, the Accelerated Graphics Port was a superset of PCI designed to address bandwidth limitations... *of bandwidth*. Well, that was redundant. They came in several generations, speed multipliers, and voltages which were broadly denoted by notches in the connector. [PlayTool has a useful table](https://www.playtool.com/pages/agpcompat/agp.html "AGP compatibility for sticklers") showing the differences.

This Dell Dimension 4100 has a 1.5 V, 4× AGP port, which places it somewhere in the middle of the lifespan of the connector. I'll admit I was excited to see it when I opened the machine! AGP was the thing my *friends* had while I struggled with integrated graphics and PCI.

The machine shipped with a Dell OEM ATI Rage 128 Pro card, which [according to PhilsComputerLab](https://www.vogons.org/viewtopic.php?f=46&t=48925 "ATI Rage 128 Pro - Better than expected!") performs competitively with my PCI cards in other machines. Dell even [still has drivers](https://www.dell.com/support/home/en-au/drivers/driversdetails?driverid=r23105 "ATI 16MB Rage 128 Pro, v. 6.34.3CD22, A06") for it! But I kinda want to see what this machine is capable of, and upgrade it to something that can handle DirectX 7 games like the original *Train Simulator* and *Midtown Madness* on something above potato settings.

*(As an aside, if you get stuck installing drivers for this card, you must install the Dell ones. Standard ATI drivers will complain a compatible card doesn't exist. That's probably worth a separate post too).*

Reading suggests that later AGP cards advertised as 4×/8× are compatible, though they'll likely be bottlenecked by the 800 MHz Pentium III, and the slower slot. Anything faster is likely pointless. Or is it?
 
Do I have any AGP card experts here? I'm leaning towards a 9000-series Radeon, because they can still be had for (somewhat) reasonable prices. But I'm all ears; or I should probably say eyes.
