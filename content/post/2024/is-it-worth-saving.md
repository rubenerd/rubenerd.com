---
title: "Is stuff online worth saving?"
date: "2024-12-16T17:24:21+11:00"
abstract: "I’ll admit, the question took me back a bit. But I’d say yes, it is."
year: "2024"
category: Internet
tag:
- archiving
location: Sydney
---
Related to my post about [self-hosted bookmarking](https://rubenerd.com/bookmarking-services/) tools (thanks to everyone for the suggestions!), I've been exporting my bookmarks from various sites so I can eventually aggregate them into once place. It's a lot of work, and it might mostly be for naught.

I expected some of the links to not resolve, but the results were far more grim than I ever could have imagined. I was a fairly obsessive bookmarker from about 2005, but barely one bookmark per month survived to the present day. Almost everything either redirects elsewhere, fails to resolve, or has been taken over by a spammer. It makes me wish I'd archived some of them.

It lead me to have a conversation with a colleague, who laughed and said "well, is it really *worth* saving in the first place?" in response.

I'll admit, the question took me back.

At various times I've defined myself as a *digital archivist* or *unproductive hoarder*, depending on how full my OpenZFS pools are, and the budget I have to buy more drives and/or controllers at a given time. My philosophy in life has always been "if you liked it, keep it". The Web is an ephemeral place, and we're learned recently even the Internet Archive is not immune from overzealous lawyers and bad-faith actors. I have backup copies of entire blogs, podcasts, YouTube channels, and books that no longer exist anywhere else, and where copyright would preclude me from resurfacing any of it.

That's also applied to my own stuff here. I wouldn't say much of my blog is great, and the first dozen or so years were absolutely *dreadful!* I was a kid, cut me some slack. But I can't bear to get rid of any of it. I'll admit there's a bit of arrogance on my part assuming that any of this stuff is worth anything, let alone worth the space and time it takes to store, backup, and deliver. But I like to think it is.

I suppose it comes down to what the purpose of such archiving is. At first, a post here is to explore and share something I'm interested in with you. Over time, that purposes morphs into something that provides a bit of a retrospective, or even nostalgia. Like newspapers, blogs change from timely to a time capsule. Extrapolate that over an entire online society, and I'd say that's even more important. I think?

So in answer to my colleague, and the title of this post, I do think it's worth saving. In my case, the question is less whether its worth doing, and the mechanics of how best to do it. Well, that and the budget!
