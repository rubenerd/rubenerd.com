---
title: "Asking for feedback before a resolution"
date: "2024-10-15T16:29:44+11:00"
year: "2024"
category: Thoughts
tag:
- feedback
- finance
location: Sydney
---
Every recent online or phone-based interaction I've had with a financial company&mdash;or almost any company now that I think about it&mdash;immediately triggers a prompt for feedback. Here's an example from today:

> Hi there,
> 
> At `$BRAND`, your experience is important to us. We’d like to get your feedback on your recent interaction with one of our representatives on 15 October 2024.
> 
> Your responses will provide us with valuable insight on what we’re doing well and what can be done to improve your experience.

This sort of feedback clearly works, or they wouldn't bother asking for it. At least, that's the theory I'm going with; maybe they're operating under a false assumption from an external agency they've hired to solicit feedback from their customers.

Leaving aside what I think about the value and accuracy of such surveys in the first place, one thing I can say with absolute confidence is they're pointless to ask *before a resolution has been reached!* This financial company asked me for feedback after my *first* interaction, for what I know is going to be a long and arduous issue.

I say such feedback is pointless for three reasons:

* It's hard to gauge the effectiveness of an interaction with an agent or company while the issue is unresolved because... *it's unresolved!* The process might be progressing well at the time we're being asked, but it could end poorly. Conversely, the interaction might be negative now, but they make good with an effective resolution. Praise or criticism is premature.

* Asking someone midway through an issue may distort your data. People generally don't reach out to support lines for the fun of it, and you risk someone conflating their frustration at a situation with an interaction with a specific agent. That doesn't help anyone.

* Three comes after two.

If you're going to ask for feedback from someone, I implore you: please ask us *after* the issue is resolved! You'll get more valuable, actionable feedback.
