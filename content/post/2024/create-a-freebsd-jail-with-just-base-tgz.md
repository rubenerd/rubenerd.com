---
title: "Create a FreeBSD jail with just base.tgz"
date: "2024-06-10T09:35:40+10:00"
year: "2024"
category: Software
tag:
- bsd
- freebsd
- jails
location: Sydney
---
This isn't so much a tutorial (because you should [read the Handbook](https://docs.freebsd.org/en/books/handbook/jails/) for such guidance instead) and more a case of *oh, that's neat*.

I've mentioned you can create a FreeBSD jail using `bsdinstall(8)`, which you might have used to install FreeBSD in the first place. This lets you choose multiple distribution sets, and enable/disable certain services.

For example, if your jails are in the same place as mine:

    holo# zfs create srv/jail/pavoliareine
    holo# bsdinstall jail /srv/jail/pavoliareine

If you only need base though, you can just extract a tarball instead you downloaded from a [FreeBSD mirror](https://docs.freebsd.org/en/books/handbook/mirrors/)\:

    holo# zfs create srv/jail/pavoliareine
    holo# tar -xvf base.tgz -C /srv/jail/pavoliareine
 
Wait, that's it? *Yes!*

Okay, you may still need to configure it, like setting up `resolv.conf` and other files in `/etc` if you intend it to be network-facing. But other configuration you would have set up in `/etc/jail.conf` will be applied automatically.

When I talked about the [practical differences between FreeBSD and Linux](https://rubenerd.com/practical-differences-between-freebsd-and-linux/), this is one of the key ones that makes my life easier. I use a similar approach for `chroot` environments on NetBSD too, which I use/abuse for similar deployments to jails; that probably warrants its own post.

