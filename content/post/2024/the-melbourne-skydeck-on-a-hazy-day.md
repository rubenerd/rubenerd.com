---
title: "The Melbourne SkyDeck on a hazy day"
date: "2024-03-30T09:59:07+11:00"
thumb: "https://rubenerd.com/files/2024/melbourne-skydeck-day-1@1x.jpg"
year: "2024"
category: Travel
tag:
- melbourne
- observation-decks
- photos
location: Melbourne
---
Clara and I went to the Melbourne SkyDeck atop the Eureka Tower a few nights ago. It was a lot of fun! So we decided to see what it was like during the day. In short: *very hazy* from a bit of fog and bushfire smoke in the distance. It was eerie, like we were looking at the city through a filter. Which I suppose we were.

<figure><p><img src="https://rubenerd.com/files/2024/melbourne-skydeck-day-1@1x.jpg" alt="First city view when you step out of the lift, with a part of the observation area on the right." srcset="https://rubenerd.com/files/2024/melbourne-skydeck-day-1@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

We were a bit tired from wandering all day again, so this time we decided to produce some overpriced comestibles and sit at the little *Bar 88* area and watch the world go by. Clara got some video of the trams, trains, and boats floating by from a distance which was pretty cool. The trams and trains weren't floating, the boats were. The former two were... locomotioning?

<figure><p><img src="https://rubenerd.com/files/2024/melbourne-skydeck-day-2@1x.jpg" alt="A view of one of the corners of the observation area, with the small bar tables and stools overlooking the city below." srcset="https://rubenerd.com/files/2024/melbourne-skydeck-day-2@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

I was surprised that the reflections from the glass were just as bad during the day than at night. Every observation deck Clara and I have been silly enough to go to have this problem, save for those with open-air areas. I'm not sure how much they can do about it, but the material scientist who can make non-reflective glass will have their work coveted by transport workers and observation deck fans alike!

<figure><p><img src="https://rubenerd.com/files/2024/melbourne-skydeck-day-3@1x.jpg" alt="View of the eastern side of the CBD." srcset="https://rubenerd.com/files/2024/melbourne-skydeck-day-3@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

Maybe it was the combination of haze, tinting, and reflections, but some of the photos we took almost don't look real; they almost look like something from SimCity 4. I originally corrected for colour and white balance when importing these, but I decided to leave them as-is.

<figure><p><img src="https://rubenerd.com/files/2024/melbourne-skydeck-day-4@1x.jpg" alt="View towards the middle of the CBD." srcset="https://rubenerd.com/files/2024/melbourne-skydeck-day-4@2x.jpg 2x" style="width:500px; height:375px;" /><br /><img src="https://rubenerd.com/files/2024/melbourne-skydeck-day-6@1x.jpg" alt="View of the west." srcset="https://rubenerd.com/files/2024/melbourne-skydeck-day-6@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

But I think my favourite of this bunch was the view of Flinders Street Station, which practically looked like a toy model! You can just make out a tram in the far right:

<figure><p><img src="https://rubenerd.com/files/2024/melbourne-skydeck-day-5@1x.jpg" alt="A scale model view of the Flinders Street Station!" srcset="https://rubenerd.com/files/2024/melbourne-skydeck-day-5@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

Clara and I agreed, we think this is more spectacular at night. But it was still fun coming up again and having a snack :).

