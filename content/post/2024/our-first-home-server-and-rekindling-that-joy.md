---
title: "Our first home server, and rekindling that joy"
date: "2024-08-14T07:46:35+10:00"
year: "2024"
category: Hardware
tag:
- 2000s
- family
- mandrake-linux
- nostalgia
location: Sydney
---
We moved a *lot* when I was a kid, which was both a blessing and a curse. It was always a pain for my parents to have found the perfect set of furniture, only to have to shoehorn it into a new house or apartment with a new layout. But it also gave us an opportunity to try different things.

In our second Singapore apartment, we had a dedicated study for the first time since we lived in Melbourne. Until that point, we had computers scattered around the house with our various printers, scanners, hard drives, serial dial-up modems, optical drives for ripping CDs, and later that Motorola Surfboard cable box everyone had. I had an idea to collect all of this into the study, with a dedicated computer to which they could be connected and used. My dad agreed, and gave me a bit of money to hit [Sim Lim Square]() to see what I could build.

Little did we know at the time, but I'd stumbled upon the idea of a home server, and the beginnings of a homelab! Or a "home hub" as the press briefly called them at the time.

It's really only gamers and homelab people who build their own computers today, but in the early 2000s it was still common to source parts to build something affordable. I'd built my own desktops, but the calculus this time was completely different. I realised the machine probably didn't need discrete graphics or stellar performance, but it *did* need a lot of PCI slots for all the peripherals we'd want to connect. It felt weird buying a machine without thinking I'd ever need a discrete GPU, or a sound card, or what people would consider a "good" CPU. This computer had a very different purpose.

Eventually I settled on an AMD Turion CPU and a basic Gigabyte board with onboard graphics support. The case was a generic mid-tower beige box with plenty of PCI slots and 5.25-inch drive bays. I brought the parts home, built it into this box, and spent the weekend configuring all our peripherals into it with Windows 2000. I even eventually added dual Zip drives, to make copying easier.

It worked great! If anyone in the family wanted to print a photo, or some paper documents, or scan something, or backup some digital camera photos, or connect to the web to do homework, or rip a CD, there was *one* place to do it. Other computers in the house were much better, but this one was indispensable.

Later that year, something else happened. My friend Boyd (hi, if you ever read this!) had got a beta release of Mac OS X installed on his PowerMac G4, and was excited to show me how he could connect to it from the school's library using PuTTY. With one command he was back home and could check his torrents. I mean, his legitimate enterprises. Star Trek Enterprise.

I'd configured our "home server" with TigerVNC, so I could be lazy and fix problems from my bedroom if they came up, but this was infinitely cooler. Naturally, I wanted to know if I could do something similar. I'd also started wondering if I could use it more as a print and file *server*, so anyone in the house could access it from their own machine.

I'd been experimenting with a boxed set of Red Hat Linux, but it was Mandrake that had really tickled my fancy (I still have the discs)! I added another old hard drive, and spent another weekend installing it and reading its excellent printed manual to set up services for printer, scanning, and file sharing. With only enough networking knowledge to be dangerous, I even got it routing the internet from the connected cable box.

I didn't know about things like ECC memory, and the cavalier attitude I took to file system integrity makes me squirm now. But pardon the French, this box was ducking *awesome!* 🦆

I'd grown up on DOS and Windows, but for the first time I felt like we had a server in the house. Linux felt *strange*, but I was able to pick up how it worked much faster than I expected. The basic shell commands and KDE GUI would come in handy when I'd install NetBSD on my iBook G3 a few years later, and later FreeBSD, and even Solaris at uni.

It was legitimately exciting finding out all the cool things I could do with this box. It was the first machine I ever got a LAMP stack going on, so we could have a family wiki! It ran Folding@Home with a cron job overnight, because we knew this was the one machine that would always be turned on. Like Boyd, I was soon able to SSH from school, which I used to take notes and study alongside various bits of mischief. I felt like I was living in the future when my mum pinged me from home saying she couldn't print something, so I SSH'd to the box during lunch and kicked the service.

I'd talk endless technobabble about all the cool stuff this machine did while we'd be eating dinner, and my parents would patiently nod and smile, not knowing a word I said. I kinda liked playing games, but *wow* this box was infinitely cooler than anything I'd used before. Windows was familiar, but \*nix!? What *couldn't* it do!?

I don't know what ever happened to this box. I suspect it was binned during the *Great Electronics Purge* when we moved to Malaysia many years later, which is a bit sad. It makes me think I need to rebuild it; I still have its rough specs burned into my memory, and those parts weren't exactly desirable. And as mentioned, I still have the original Mandrake discs!

I owe a lot of my professional and personal development to that lowly box. I'm remembering all the optimism and joy I felt using it as I finish this post; something I definitely need to reconnect with.
