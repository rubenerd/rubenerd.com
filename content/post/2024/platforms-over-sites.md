---
title: "Sites over platforms over sites"
date: "2024-08-12T07:10:05+10:00"
year: "2024"
category: Internet
tag:
- indieweb
- mastodon
- social-media
location: Sydney
---
Online search has become so bad thanks to spam and generative AI, it's made people look fondly at *Reddit* again, of all places! Fans were abandoning their Digg clone a year ago for its [draconian API changes](https://www.theverge.com/2023/6/8/23754780/reddit-api-updates-changes-news-announcements), and in February given their [worrying IPO](https://edition.cnn.com/2024/02/23/tech/reddit-ipo-filing-business-plan/index.html)\:

> Now, Reddit — which is not yet profitable — says it seeks to grow its business through advertising, more e-commerce offerings and by licensing its data to other companies to train their artificial intelligence models.

Note the journalist said "its data", not "its contrubuters' data". That's the fundamental issue with platforms writ large: your contributions are theirs; either through asserted ownership, or a perpetual licence term. People are learning that the hard way from Reddit to Stack Overflow.

I guess a lot can happen in a year. But regardless of whether Reddit and sites like it are desireable or not, the ultimate question boils down to the same thing: *what's the replacement?*

I think that for most people, the replacement they envisige is another platform. When Digg fell apart after v4, people flocked to Reddit. Friendster users went to MySpace, then to Facebook. Even Twitter users transferred to Mastodon, which usually meant the [Mastodon Social](https://mastodon.social) instance. That latter example interests me, because it actually demonstrates a potential solution to a longstanding issue we've had with replacing platforms.

Much has been written about the *network effect* of platforms, in which the switching cost is kept high by hampering interoperability, and because it's where your friends are. Platforms leverage this to their advantange, until a sufficiently compelling competitor along, and their user base has had enough.

But *convenience* is just as important a factor. A platform is appealing because it delivers everything under one roof. IndieWeb fans like me wax lyrical about the glory days of niche web forums (or BBSs), but there's no denying there's utility in having one URL, login, and interface, alongside a handy aggregated view of all your communities.

People flocked to these platforms not just because their freinds were there, but because they were convenient. I could follow #hashtags or subreddits for retro computers, bush walkers, and Hololive. It also reduced the friction for entering new communities, because you could carry your established profile with you, rather than being "the n00b" every time in a forum of veterans measuring their worth based on signup date.

Mastodon, and the wider Fediverse, offer an alternative that flips this problem on its head. Like email and web feeds, I don't need to be on the same server or platform to communicate with someone on Mastodon. But because we've agreed on a technical protocol and a (relatively) consistent interface, the network effect is neutralised, and we have that convenience and change for serendipity previously only available on centralised platforms.

It gives me a bit of hope for the web again.
