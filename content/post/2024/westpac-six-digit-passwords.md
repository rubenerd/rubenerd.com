---
title: "Westpac’s six digit passwords"
date: "2024-09-11T19:11:57+10:00"
year: "2024"
category: Internet
tag:
- authentication
- security
location: Sydney
---
[Kieran Murphy on Mastodon](https://aus.social/@km/113118102136643216)\:

> In 2024, Westpac (one of the largest banks in Australia) has decided that online banking needs a password more secure than a 4-6 digit numerical PIN. Welcome to the 21st Century, Westpac. You're only 24 years late.

That's a relief! I had a Westpac account briefly when I moved back to Australia in the early 2010s, but moved off them when I learned there was only a six digit PIN safeguarding my account. My primary bank is a cooperative (and formerly a credit union), and they were able to have better login auth than a company *thousands* of times the size!

I remember my DBS account in Singapore using a Java applet and a six-digit PIN for its login form back in... 1998.
