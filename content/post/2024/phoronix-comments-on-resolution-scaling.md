---
title: "Resolution scaling discussion on Phoronix"
date: "2024-02-25T14:55:28+11:00"
year: "2024"
category: Internet
tag:
- comments
- displays
- forums
- kde
- pc-screen-syndrome
location: Sydney
---
Phoronix posted an [article on the upcoming KDE Plasma 6](https://www.phoronix.com/news/KDE-Plasma-6.0-Next-Week "KDE Plasma 6.0 Ready For Release Next Week, Plasma 6.1 Seeing Early Feature Work") yesterday, and the comments were nothing if not predictable.

We start with this observation by [hv_139](https://www.phoronix.com/forums/forum/phoronix/latest-phoronix-articles/1445503-kde-plasma-6-0-ready-for-release-next-week-plasma-6-1-seeing-early-feature-work?p=1445537)\:

> Just one example: One year ago they released support for fractional scaling of wayland and it got reverted one day after the release again, because it broke wayland. Turned out that nobody ever tested it. But for a GNOME user it might be enough, because GNOME hides its fractional scaling controls and GTK doesn't support wayland fractional scaling yet.

I've been bitten by this before too. It's a bit grim, considering macOS has been able to do it for more than a decade, and even Windows mostly can now.

[user1 responds](https://www.phoronix.com/forums/forum/phoronix/latest-phoronix-articles/1445503-kde-plasma-6-0-ready-for-release-next-week-plasma-6-1-seeing-early-feature-work?p=1445538)\:

> Well, I don't use fractional scaling, because I have a 1080p monitor.

**Rule &numero; 1:** It's not a problem if *they're* unaffected.

[They double down](https://www.phoronix.com/forums/forum/phoronix/latest-phoronix-articles/1445503-kde-plasma-6-0-ready-for-release-next-week-plasma-6-1-seeing-early-feature-work?p=1445545)\:

> I don't know what kind of a rock you live under, but 1080p monitor is not "decade old hardware". It's still the most widely used resolution and 1080p monitors are still being manufactured.

**Rule &numero; 2:** It's not a problem if *most* are unaffected.

[Then they go for the trifecta](https://www.phoronix.com/forums/forum/phoronix/latest-phoronix-articles/1445503-kde-plasma-6-0-ready-for-release-next-week-plasma-6-1-seeing-early-feature-work?p=1445580)\:

> And? 4K monitors also came out around 20 years ago. That doesn't change the fact that 1080p monitors are still perfectly fine for many people. You don't seem to realise [because of course they said that].

**Rule &numero; 3:** It's not a problem if... other things work.

The mental gymnastics people will go through to avoid acknowledging an issue is staggering, and far less entertaining than the real thing. Alas, it's widespread on technical forums. 
