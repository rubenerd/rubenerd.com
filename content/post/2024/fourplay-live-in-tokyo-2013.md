---
title: "Fourplay live in Tōkyō 2013"
date: "2024-10-22T11:53:21+11:00"
thumb: "https://rubenerd.com/files/2024/yt-PJi0PhJVk7k@1x.jpg"
year: "2024"
category: Media
tag:
- jazz
- music
- music-monday
location: Sydney
---
It's [Music Monday](https://rubenerd.com/tag/music-monday/) time! Each and every Monday... wait, it's Tuesday. I was going to claim it might still be Monday in the US, but this video was recorded in Japan, which is basically in our timezone. Where was I going with this?

Fourplay are incredible, as was this concert performed in 2013. Rest in peace, Chuck ♡.

<figure><p><a target=_BLANK href="https://www.youtube.com/watch?v=PJi0PhJVk7k" title="Play Fourplay - Live in Tokyo (2013)"><img src="https://rubenerd.com/files/2024/yt-PJi0PhJVk7k@1x.jpg" srcset="https://rubenerd.com/files/2024/yt-PJi0PhJVk7k@1x.jpg 1x, https://rubenerd.com/files/2024/yt-PJi0PhJVk7k@2x.jpg 2x" alt="Play Fourplay - Live in Tokyo (2013)" style="width:500px;height:281px;" /></a></p></figure>

