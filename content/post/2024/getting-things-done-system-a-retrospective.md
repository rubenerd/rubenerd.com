---
title: "Getting Things Done: A personal retrospective"
date: "2024-05-20T09:01:52+10:00"
year: "2024"
category: Thoughts
tag:
- personal
- task-management
location: Sydney
---
I started university and my IT career in the mid-2000s. This was the era of the iPod, ZFS launching in FreeBSD, Suzumiya Haruhi taking over the world, and getting CD-ROMs from your school with software and PDFs for your classes. Wait, does your laptop not have a CD-ROM drive? Wow, it must be *old!*

Back then, David Allen's *Getting Things Done* was the new hotness for personal productivity. I learned of the system via Merlin Mann, a gentleman who's had a profound effect on my work and mannerisms. I literally bought the book before most of my textbooks, and implemented a form of its system for my studies, work, and personal tasks.

I credit David and Merlin for getting me through the toughest time of my life, back when I was juggling a *lot* of things. I feel stressed at times now, but it was nothing compared to studying full time, working, and being a palliative carer at home.

<figure><p><img src="https://rubenerd.com/files/2024/gtd@1x.jpg" alt="The Getting Things Done book" srcset="https://rubenerd.com/files/2024/gtd@2x.jpg 2x" style="width:320px; height:478px;" /></p></figure>

There's a lot to like about GTD, but **contexts** were the most powerful and consequential attribute of the system for me. A typical `#TODO` list may contain a list of tasks, which you might categorise into projects. I had projects for each course, for each major work deliverable, grocery shopping, manga to read, and so on.

If you imagine projects as single columns of tasks on a spreadsheet, contexts are the rows. They cut across multiple projects, and let you filter tasks depending on where you are, what you're doing, the time of day, whatever works for you.

Have access to Wi-Fi? Here are some tasks from multiple projects filtered on whether you can do them. At home in the evening? There's the *plant watering* task under the life project.

It might sound weird, but I likened contexts to the legendary XTreeGold's file tagging feature, similar to what macOS introduced with Spotlight many years later. Tagging files let you search and perform specific actions across them regardless of which folder (aka *project*) they're in. In storage and networking terms, they're logical views of a physical set of projects.

Fast forward to today, and I realised I don't use the system anymore. GTD is one of those ways of thinking you really have to jump all into or it doesn't work, and lately I found it easier to just generate static lists. Part of this was down to predominately only getting tasks from two sources now, and the nature of those tasks being more atomic with fewer dependencies. With ubiquitous connectivity, contexts like Wi-Fi aren't that useful anymore either.

So what did I replace it with? I have two systems that are largely independent of each other. I went back to **tasks listed under projects** in plain text files. At work, we use one of the multitude of mediocre web-based task managers that attempt to be an all-encompassing kanban system, but end up just being an over-engineered UI atop... **tasks listed under projects**. But it doesn't need syncing, and allows tasks to be assigned to people and tracked, so there's that. 

And yet, the *volume* of stuff I have to do on a daily basis has been slowly ticking up again for a while. My role in my current job has me tackling things from multiple places and people, often times within minutes of each other. And yet, I struggle to map the GTD system atop this new reality.

Which is really what resonated from [Cal Newport's article about GTD](https://www.newyorker.com/tech/annals-of-technology/the-rise-and-fall-of-getting-things-done "The Rise and Fall of Getting Things Done") for The New Yorker in 2020:

> The knowledge sector’s insistence that productivity is a personal issue seems to have created a so-called “tragedy of the commons” scenario, in which individuals making reasonable decisions for themselves insure a negative group outcome. An office worker’s life is dramatically easier, in the moment, if she can send messages that demand immediate responses from her colleagues, or disseminate requests and tasks to others in an ad-hoc manner. But the cumulative effect of such constant, unstructured communication is cognitively harmful: on the receiving end, the deluge of information and demands makes work unmanageable. There’s little that any one individual can do to fix the problem. [...]

> In this context, the shortcomings of personal-productivity systems like G.T.D. become clear. They don’t directly address the fundamental problem: the insidiously haphazard way that work unfolds at the organizational level. They only help individuals cope with its effects.

It's that line about productivity being a "personal issue" that stuck with me. There's the idea of implementing a system that works for you, and the unfortunate corollary that if you're not, it's your fault for not being productive. It's the same thing when you see wealthy people blaming the working poor on the basis they must be bad at budgeting.

To be clear, David and Merlin weren't suggesting this. But to tackle this issue wholistically (I never thought I'd use *that* word!), we also need to acknowledge where the tasks are coming from, and why. The world's most efficient, streamlined task management system still can't help with burnout, fatigue, or frustration if the volume and type of tasks aren't themselves productive or useful. That was a lot of *or's*, maybe I should start a rowing club for people with massive `#TODO` lists.

I'm currently reviewing implementing another system to help with this growing list of stuff I'm expected to do everyday, while making sure I also raise these issues. Whether that will be GTD or something else, I'm not sure yet.
