---
title: "Quick break, 2024"
date: "2024-01-28T21:45:12+11:00"
year: "2024"
category: Thoughts
tag:
- personal
- weblog
location: Sydney
---
Sometimes blogging is a great distraction, but it's getting tougher with all the family things and some more bad news.

See you again soon. Peace. ✌️
