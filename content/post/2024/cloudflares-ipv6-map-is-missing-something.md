---
title: "Cloudflare’s IPv6 map is missing something"
date: "2024-03-03T07:58:28+11:00"
year: "2024"
category: Media
tag:
- geography
- ipv6
- maps
- networking
location: Sydney
---
Or should I say, it's [missing *many* somethings](https://radar.cloudflare.com/reports/ipv6):

<figure><p><img src="https://rubenerd.com/files/2024/cloudflare-ipv6.png" alt="Map of the world with many glaring omissions." style="width:500px; height:300px;" /></p></figure>

I can already see I'm in trouble here, because while Singapore is still around, the ground under my feet in Australia has vanished. Even Germany is gone, which throws half my genome into question.

Beyond those that I can see, we're missing Papua New Guinea, Nigeria, the Netherlands, Denmark, Argentina, most of North and Central America, India, Bangladesh, Japan, and a few others. Sweden may have gone, but at least they can still hang out in Åland.

What amazes me is that New Zealand didn't get left out [for once](https://en.wikipedia.org/wiki/Omission_of_New_Zealand_from_maps "Omission of New Zealand from maps"). I don't think sheep can swim, though jars of the world's [best peanut butter](https://picspb.com/ "Pic’s Peanut Butter") might.
