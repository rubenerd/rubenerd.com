---
title: "Stymied by legacy measurements in the morning"
date: "2024-12-08T08:37:15+11:00"
abstract: "5/8ths!? What is this, the Spanish Inquisition!?"
year: "2024"
category: Hardware
tag:
- australia
- measurements
- retrocomputing
location: Sydney
---
We're getting a delivery from a Swedish furniture manufacturer this morning, so I was doing some measurements and calculations to confirm where they'd go in the apartment. One cabinet is going to be a place to display some of our retrocomputers, and I wanted to make sure they'd fit.

I was looking at the Computer History Museum to figure out dimensions, because people almost never post them in their technical specifications or retrospectives for some reason (even me, whoops)! The [Apple IIe Platinum](https://www.computerhistory.org/collections/catalog/102633586) was listed as the following:

> 4 1/2 x 15 x 18 in.

I'd heard about the Inch before; it's an archaic measure of length used to figure out how many Cubits are in a Bushel of Furlongs. It's so *weird* to my metric mind to refer to measurements as "half" of something, but I guess you have to when you don't have millimetres. My parents who grew up during [metrication in Australia](https://en.wikipedia.org/wiki/Metrication_in_Australia "Wikipedia article on Australia's move to the Metric system") in the 1970s likewise used to say how quickly their mind flipped to decimals as well, not least because it made using calculators much easier.

Anyway, it was easy in a pinch to add to Clara's and my spreadsheet by changing to a decimal and converting:

> 4.5 * 2.54

But then I looked up the [Commodore Plus/4](https://www.computerhistory.org/collections/catalog/102633586)\:

> 3 x 12 5/8 x 8

I... wait, *5/8ths?!* What is this, the Spanish Inquisition!?

After the coffee had kicked in I remembered that you can also just put fractions into a spreadsheet, and most of of my frustration evaporated: all 45/87ths of it. I still would have preferred a simple decimal though.
