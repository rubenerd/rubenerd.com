---
title: "Editing Dynmap YAML directly"
date: "2024-04-07T06:22:11+10:00"
thumb: "https://rubenerd.com/files/2024/dynmap-2024@1x.jpg"
year: "2024"
category: Thoughts
tag:
- games
- minecraft
- plugins
location: Sydney
---
[Dynmap](https://www.spigotmc.org/resources/dynmap%C2%AE.274/) is one of those plugins that feels *integral* to Minecraft once you start using it. I've joked with Clara that I spend as much time cataloguing, mapping, and exploring our Dynmap than I do playing the host game itself, which perhaps says more about me than I care to admit.

This is our first, and still current world that we started during COVID lockdowns in 2020. Seasoned Minecraft players could probably immediately spot the original four 1.16 maps, and the new 1.17+ areas:

<figure><p><a href="https://rubenerd.com/files/2024/dynmap-2024@original.png"><img src="https://rubenerd.com/files/2024/dynmap-2024@1x.jpg" alt="Screenshot showing Dynmap running at a coffee shop via our VPN." srcset="https://rubenerd.com/files/2024/dynmap-2024@2x.jpg 2x" style="width:500px; height:312px;" /></a></p></figure>

Glowing reviews aside, tracking all the pins and lines we added was one area that was becoming tedious. I'd worked out a system where I'd first transcribe the attributes and coordinates into a spreadsheet, then *copy pasta* the resulting line into the Minecraft chat window to place it. That way, I had a complete record of everything we added in the event we'd have to recreate it in the future.

Unbeknownst to me until embarrassingly recently, the pins are stored by default in a YAML file you can backup. It also means you can edit it! Here's a pin and its associated set from our current map:

    sets:
        portals:
            hide: false
            circles: {
                }
            deficon: portal
            areas: {
                }
            label: Nether Shinkansen
            markers:
                java_lava_shinkansen_portal:
                    world: world
                    markup: false
                    x: -269.0
                    icon: portal
                    y: 64.0
                    z: 244.0
                    label: Java Lava Shinkansen Portal

This has all the attributes I was storing in the spreadsheet, and in editable form. It means I can use this as the source of truth, and place new markers by merely walking to where I want it in the game, rather than transcribing coordinates every single time.

Using the `/dynmap` commands directly in Minecraft is still probably safer and easier for most people, or maybe your only option if you're on a shared server without an "operator" account. But if you're like me and wanting to bulk-update stuff, editing this file is another option. You'll also need to restart the server for it to take effect.

It *should* go without saying that you'd want to **backup the file first**, and double-check your YAML even if you're seasoned. I can't tell you how many times I've updated an Ansible playbook only to have it fail halfway because I inadvertently forgot to dot a T or cross an I. No, wait.

Thanks again to Michael Primm for this awesome plugin.
