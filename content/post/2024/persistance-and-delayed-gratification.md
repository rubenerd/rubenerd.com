---
title: "Persistence and delayed gratification"
date: "2024-06-28T09:27:11+10:00"
year: "2024"
category: Thoughts
tag:
- finances
- psychology
location: Sydney
---
Pop psychology is full of contradictory information:

* We're told attempting the same thing more than once, then expecting different results, is the definition of insanity. Yet we're also told to try, and try again, to secure a better outcome.

* We're told to save and plan for the future, because you don't know what's coming. Yet we're also told it's important to live in the present moment.

These positions aren't *entirely* mutually exclusive, but I can understand why people would look at these, throw up their hands, and say *...what the!?*

A friend of mine&mdash;who's since become a psychologist&mdash;used to say that persistence and the ability to delay gratification are the two biggest superpowers people can have. Persistence grants you resiliency against what life throws at you, and lets you push for what you need and want. Delayed gratification lets you avoid entire classes of personal or financial problems.

I guess moderation is the key to everything. You don't want to persist with things that are clearly dead ends, or push you into the sunk cost trap. Likewise, you don't want to delay gratification for so long that life passes you by, and you only realise it looking bleary eyed at a spreadsheet years later.

Yes, there's subtext here! But I think the broader points are still worth thinking about.
