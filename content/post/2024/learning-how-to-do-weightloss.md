---
title: "Learning how to do weightloss"
date: "2024-01-07T15:37:17+11:00"
year: "2024"
category: Thoughts
tag:
- health
- hiking
- walking
location: Sydney
---
*I should preface this post by saying that everyone's health is different, and you <strong>must not</strong> take the ramblings of a well-meaning person who isn't a doctor or nutritionist seriously.*

This is a bit of a different topic here to what I normally discuss, but a few of you noted my comment about being on a "lower-carb" diet in a previous post. There isn't anything really new or interesting here, but carry on reading if you're not a sucker for punishment.

I grew up never needing to think about my metabolism. I was a skinny kid who was lucky to have been born with genes that let me eat what I wanted, in whatever quantity I wanted, and with variable exercise. Granted, my mum instilled a love of walking and hiking in my sister and I that resulted in me never getting a licence. Why bother when I can always just walk to public transport?

But walking isn't the most strenuous of activities, and I otherwise haven't been as active as I probably should have. My gymnastics and cross country days are long over, and I haven't touched a badminton or squash racquet for at least a decade.

This finally came to a head during Covid lockdowns in 2020, where for the first time I spent the majority of my day not moving at all. This became normal even after we pretended everything was back to normal, and while I'm back to walking most places again, I've found myself becoming winded and tired more easily. Whereas I was able to take 30,000+ steps a day in Kyōto and New York in my late 20s, I was struggling to hit 10,000 around Sydney.

My belly was where this showed the most. I managed to put on more than ten kilos, which for a lanky guy like me was *steep*. My belt sizes didn't change that much, but I found myself being embarrassed wearing certain shirts. I felt like a slob.

I finally started doing something about it with Clara in December last year. Giving up sweets was easy; I'd avoided most sugar for a while. But I was advised that I was eating far too many refined carbohydrates for my activity level, and to try cutting these down for a period of time.

Clara and I have been exercising every day, including taking at least 10,000 steps. Yes it's an arbitrary, pointless number that doesn't factor in how *strenuous* those steps are, but seeing the pedometer display anything less than that number has pulled us off the couch when we'd have otherwise not done anything. It's felt like an accomplishment seeing a week go by, then two with numbers exceeding that number.

Reducing carbs has been more difficult. Some were easy, like leaving off the croutons from our regular home salads (which are awesome), having a lettuce leaf in lieu of a burger bun, or having a few nuts instead of a muesli bar. Cauliflower rice has been interesting, though not as good as the real thing. A few corn chips, or a small bowl of oats are now seen as a treat, not a base for a whole meal. It's normal now to feel full, not bloated.

*(I even tried one of those protein shake things, and proceeded to not eat for two days. I can't recommend it, but I sure lost weight).*

Still, I don't think we had to change *all that much*... and the results have been ridiculous. I've already found I can exercise and walk more easily, and I've lost more than 5 kilos. It's comical to me now that all I needed was regular exercise and eating better. *WHO WOULD HAVE THOUGHT!?* I'm also starting to *feel* better about myself, even if the purported mental health benefits feel oversold.

As I mentioned on Mastodon, the trick for me is to keep an overengineered spreadsheet of what I've been eating, my step count, what exercise I've done, and my weight every day. This has been important to validate what I'm doing, and has been weirdly motivating. I now have a month of data showing that a small, cheeky treat in a given day makes no discernible difference, but a Christmas party with alcohol and heavy canapés sure do.

I still need to get more exercise, but I feel like this is a start. It's as though I've taken 3 MacBook Pros off my stomach already.
