---
title: "What I look for in blogs"
date: "2024-11-16T08:02:18+11:00"
year: "2024"
category: Internet
tag:
- blogging
location: Sydney
---
I read a *lot* of blogs. My [public blogroll](https://rubenerd.com/blogroll.opml) is woefully out of date; I probably have close to a third more feeds in my reader than what I have published there. Independent writers are my favourite, because I get enough curated, sanitised corporate media in my `$DAYJOB` and other places. 

I realise though that while I've mentioned how to *start* blogging, I've never described what makes me reach for the subscribe button when I chance upon a new one. I realise this is quite a large oversight, so here are my rules of thumb, fingers, and other body parts.

**Do they have an RSS/Atom feed?**
: <p>This is less a philosophical stance, and more nuts-and-bolts concern. I read blogs in an aggregator. If it’s hard to read a blog, I probably won’t. Same goes for radio shows without a podcast feed, or pasta without a fork.</p>

**Is it written by a single person?**
: <p>I’m not automatically adverse to reading a publication with multiple writers, but I tend to prefer individual writers expressing themselves in their own space.</p>

**Is their heart in it?**
: <p>I come across a lot of sites where it’s clear the writer is doing it for clout, influence, SEO, whatever. It’s hard to feign enthusiasm and joy, though it doesn’t stop people trying. Likewise, I don’t care if English isn't their first language, or that they get grammar and spelling wrong. I’m there to read their ideas and thoughts.</p>

**Do they have a range of interests?**
: <p>I love learning about <em>random</em> new things. One of the best avenues is to be introduced by someone with whom you otherwise have another common interest.</p>

**Do they have good character?**
: <p>I don’t care if someone’s a respected subject-matter expert or an industry icon if they flaunt being a dick, or their ethics and morals violate my own. I’ve decided my limited time is worth more.</p>

**Does their site respect us?**
: <p>Whimsy and creativity in a site design are wonderful, but as long as a site avoids autoplaying videos, popups, JavaScript bloat, tracking, and other dark patterns, I’ll enjoy reading it. Clearly design chops (or lack thereof) haven’t stopped me.</p>

