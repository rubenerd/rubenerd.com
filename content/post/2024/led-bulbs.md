---
title: "LED bulbs"
date: "2024-06-11T08:15:45+10:00"
thumb: ""
year: "2024"
category: Hardware
tag:
- energy
- environment
location: Sydney
---
LED "bulbs" are great. They're drastically more power efficient, run cooler in hot climates, and offer a wider spectrum of light than those horrible corkscrew compact fluorescent lamps we all ran for a few years there.

I also distinctly remember LEDs being touted as more reliable than other bulbs, such as halogens and incandescents. But my experience doesn't mirror this at all. In the three small apartments Clara and have lived in, we've had to change an LED bulb at least every few months, if not more frequently. By contrast, my dad once quipped they almost never had to change incandescents in the houses we lived in.

The plural of *anecdote* isn't *data*, and perhaps in aggregate they're more reliable. But five to seven bulbs... a year?

Now you could say the more expensive LED bulbs we're buying are still no good, that the apartments we live in don't have the appropriate driver circuits, and that the claims of longevity referred to the tech itself, not in its specific applications. Being technically accurate is the best kind of accurate, after all. But these ignore the practical reality how these bulbs are bought and used, and the claims people bought them for.

Oh well, maybe I've just had a breathtakingly bad string of luck. A string of bad luck. A luck of string... pardon, the bulb above the island kitchen just blew.
