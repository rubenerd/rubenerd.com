---
title: "Phil Gerbyshak’s happiness boosters"
date: "2024-02-22T15:58:02+11:00"
year: "2024"
category: Thoughts
tag:
- mental-health
- philosophy
location: Sydney
---
One of Phil's latest newsletters enumerated *happiness boosters*, which I'm all for. This one stuck out for me:

> **Three Cheers for the Day:** Each night, think of 3-5 cool things that happened. It could be anything, from a tasty coffee to nailing a work task.

I can't tell you how much this has helped me. Wait, yes I can, I'm doing it now. It's helped me a lot!

I've gone as far as maintaing a text file of small accomplishments. Sometimes I can add large projects to them, other times the tasks are as simple as "unstuck kitchen drawer".
