---
title: "Building a replacement 386/486 CMOS battery"
date: "2024-12-23T11:13:17+11:00"
abstract: "If your motherboard has an external battery header, they’re easy to build and work great."
thumb: "https://rubenerd.com/files/2024/goodbye-varta@2x.jpg"
year: "2024"
category: Hardware
tag:
- am386
- guides
- retrocomputing
- v4s471
location: Sydney
---
Motherboards made from the late-1990s to today generally have a CR2032 cell to maintain the system realtime clock and CMOS settings when the machine isn't operating. When these wear out, you can pop them out with a flathead screwdriver, and replace them with a new coin cell without much fuss.

Prior to this, a soldered NiCD or NiMH barrel battery was the most common RTC battery in PC compatibles, of which Varta made the majority. These recharged from the board, which was convenient when these boards were new and in regular use. Unfortunately, time has proven a harsh mistress, with most of them becoming prone to leaking and damaging boards with corrosive electrolyte. The [Bad Idea Barta Board](https://texelec.com/product/bad-barta/) is a tongue-in-cheek reference to their destructive power.

I'm lucky that my [Am386 motherboard](https://rubenerd.com/tag/am386/ "View all posts in my Am386 motherboard series") either didn't ship with a Varta barrel of death, or it was removed long before it came into my possession in the early 2000s. My [new 486 motherboard](https://rubenerd.com/an-mv4-v4s471-472p-486-motherboard/ "An MV4-V4S471/472P 486 motherboard from 1995") shipped with a corroded barrel, though fortunately the damage was minimal and cosmetic. Not all my boards have been this lucky.

<figure><p><img src="https://rubenerd.com/files/2024/bad-varta@1x.jpg" alt="Photo showing a disgusting, crusty barrel battery having been removed from a 486 motherboard." srcset="https://rubenerd.com/files/2024/bad-varta@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

The good news is, these motherboards are fully functional without them, meaning you can validate they work first. But you'll quickly want to replace it to save yourself reconfiguring the BIOS every time you power up the machine, especially if you're using an older board without XT-IDE autoconfiguring your cylinders, heads, and sectors for you.


### Building a replacement

You've got a few options to replace the Barrel of Doom. One is to buy a CR2032 coin cell holder, and solder it to the board with a schottky diode to prevent it being charged. The [Barrel Battery Blaster](https://github.com/scrapcomputing/VerticalBarrelBatteryBlaster) has gerber files you can fab and build. I've done this once before, but it's fiddly for a novice solderer.

*(ML2032 cells are rechargable versions of the CR2032 which may negate the need for the diode, but I've never used or seen one. I assume they'd be safe with motherboard recharging circuitry, but I'd want to do more research first before trying on a board I care about).*

If the motherboard has an external battery header, life is much easier. You generally find these near or around where the original Barrel of Doom stood, and are usually 1×4 pins wide. Check [The Retro Web](https://theretroweb.com/) for a copy of your motherboard's manual, which should document the pinout. Otherwise, you can use a multimeter to check for continuity between the ground plane of the motherboard and one of the pins. The positive terminal is usually connected with a trace to the positive side of where the Varta battery stood. It's always been pin 1 in my experience, but you should still test.

My 486's manual described the pinout as this, which I double checked and confirmed was the case:

1. Positive
2. Empty/keyed
3. Ground
4. Ground

This leads to the question: what cells to put in this? One option is to use a CR2032 with wires and shrink tubing, which I know early ThinkPads did. Another is to use a [Tadrian 3.6 V Lithium](https://au.rs-online.com/web/p/c-batteries/5268475) cell, much like those used in original external battery packs. Both of these should provide sufficient voltage, though I've found CR2032s drain quickly on these older boards.


### Settling on AAs

I elected to use the 3× AA battery approach, along with a [battery holder](https://www.jaycar.com.au/3-x-aa-side-by-side-flat-battery-holder/p/PH9274) and a [crimpable 2.54 mm pitch header](https://www.jaycar.com.au/4-pin-0-1in-header-with-crimp-pins-2-54mm-pitch/p/HM3404). My local electronics store had all these in stock today, which saved me waiting weeks for shipping. I only buy rechargable AAs on principle, so three of these rated at 1.28 V achieve a similar combined voltage to those above cells, and the Barrel of Doom.

The advantage of this approach is you can swap out batteries easily, and use regular 1.5 V alkalines if you have an older board with higher voltage requirements... though you'll want to make sure you locate it away from your board.

<figure><p><img src="https://rubenerd.com/files/2024/goodbye-varta@1x.jpg" alt="Photo showing the components described above." srcset="https://rubenerd.com/files/2024/goodbye-varta@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

Armed with a good pair of pliars, I was able to feed the exposed leads of the battery holder into the crimpable header, and crimp them down until they were tight. These were then pushed into the header block until they clicked, and subjected to a light "pull" test to make sure everything was secure. I could have used a CD-ROM audio cable too, but I like how clean this turned out.

Once assembled, add your batteries and use a multimeter on the metal contacts to confirm everything works.  As mentioned, these rechargables are rated for a lower voltage than standard alkalines, meaning they added up to 3.85 V DC instead of 4.5 V. This works fine on my target boards, though earlier boards with higher voltage requirements may need more juice.

<figure><p><img src="https://rubenerd.com/files/2024/goodbye-varta-voltage@1x.jpg" alt="Photo showing the multimeter reading 3.85 V on the pins coming off my new battery pack" srcset="https://rubenerd.com/files/2024/goodbye-varta-voltage@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

I prefer using this method when I can. It's approachable with basic tools, and doesn't require any mods to the original motherboard. If you get stuck, this [widely-circulated document on Vogons](https://www.vogons.org/viewtopic.php?p=1102819#p1102819) has a lot more information.

As for why I chose `$foo` instead of `$bar`, or didn't do something else, you are welcome to build yours that way instead :).
