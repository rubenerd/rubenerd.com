---
title: "pgdoc.link is wildly useful"
date: "2024-10-31T08:40:01+11:00"
year: "2024"
category: Software
tag:
- databases
- documentation
- postgres
location: Sydney
---
My "home page" is a single private [Omake file](https://rubenerd.com/xmlns-omake/) of links to commonly accessed sites and documentation, so I can quickly access what I need and sync it easily across browsers and devices. I don't update it that often, so when I do, it's usually for something really useful.

This is one such occasion. I've had two dozen links to various Postgres docs, but [depesz](https://www.depesz.com/2024/10/29/new-way-to-search-postgresql-documentation/) has just made navigating them all *much* easier with his new site [pgdoc.link](https://pgdoc.link/)\:

> This site is providing helpful shortcuts to PostgreSQL documentation. It has set of keywords that were extracted from documentation, so that when you search for known keyword - you will get redirected directly to appropriate page or even part of page of documentation.

I just tried it with `pg_database_size`, and it immediately took me to the appropriate [System Administration Functions](https://www.postgresql.org/docs/17/functions-admin.html) page of the PostgreSQL 17 docs. Friggen *awesome!*

I've also just added it to the new Postgres card under BSD/illumos on my [public Omake](https://rubenerd.com/omake.xml). I should probably merge a bunch more links into it too.

As an aside, a few of you reminded me recently that I need to post my consolidation journey from a bunch of disparate personal and family MySQL-adjacent and SQLite3 DBs to Postgres a few months ago. I didn't think there was anything especially unusual or "edge casey" about it, but it seems many of you are going through a similar process.
