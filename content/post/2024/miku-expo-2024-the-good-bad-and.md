---
title: "Miku Expo 2024: The Good, Bad, and…"
date: "2024-04-10T16:16:27+10:00"
thumb: "https://rubenerd.com/files/2024/GKx_O4lboAAgV1z.jpg"
year: "2024"
category: Anime
tag:
- art
- hatsune-miku
- music
location: Sydney
---
[Miku Expo 2024](https://mikuexpo.com/na2024/) has launched in North America, with sell out concerts featuring everyone's favourite virtual idol.

On the good side, we have hatahiro's art for the event which is *fscking spectacular!* She's clearly a manga-inspired character, but the Gotham-style landscape and the subtle colouring and shading on her outfit are such a fun nod to American comic books. I would buy a poster or wall scroll of this in a heartbeat, and I'm not even in North America!

<figure><p><img src="https://rubenerd.com/files/2024/GKx_O4lboAAgV1z.jpg" alt="Epic fanart of Hatsune Miku with her trademark teal twin tails spinning behind her against a backdrop of an American-comic style skyline." style="width:500px" /></p></figure>

Unfortunately, that seems to be where the good ends. I've been reading comments from friends on social media that the hologram projection system that previous concerts used was instead [replaced with a large TV](https://www.ign.com/articles/hatsune-miku-fans-reeling-virtual-pop-star-on-screen-not-as-hologram "Hatsune Miku Fans Left Reeling as Virtual Pop Star Appears on Screen, Not as a Hologram") at the inaugural concert in Vancouver. People were incredulously comparing it to their lounge setups, and were frustrated at how it much it shattered the illusion.

It does seem odd that it would be such a downgrade from past years and events. The 3D projection system failing would be the best-case scenario, requiring they invoke a backup at short notice. But given the lack of communication explaining this (Japanese organisers tend to be *very* apologetic about such things), we're left to assume they didn't think it was a big deal, or that it was planned that way in the first place. And even if the primary projection failed, why not have backup projectors? The whole thing is weird no matter how you spin your twin tails.

As my friend \_BADCATBAD posted on her private Schnitter account, it's unfortunately not surprising given [Crunchyroll's involvement](https://anitrendz.net/news/2023/09/12/miku-expo-2024-partners-with-crunchyroll-on-10th-anniversary-north-american-tour-next-april-may/ "Miku Expo 2024 Partners with Crunchyroll on 10th Anniversary North American Tour Next April and May"). The company has developed a reputation for having what I dub the *Reverse Midas Touch*; last year's Crunchyroll Expo in Melbourne was a schmidtshow, and their wholesale [reneging on commitments](https://rubenerd.com/crunchyroll-deletes-funimation-digital-copies/ "Crunchyroll reneging on Funimation Digital Copies") for Funimation customers was shocking, if unsurprising. They're the anime world's poster child for [enshittification](https://en.wikipedia.org/wiki/Enshittification "Wikipedia article on Enshittification"), and it's spilling into the real world too. [LOVE IS WAR](https://www.youtube.com/watch?v=SmX5cnQ1YAg), but they're winning.

It still sounds like it was a fun event overall, though Miku wears something else... thank you, I'm here all day. That said, I'd still be wary of buying, subscribing, or attending anything with that orange Crunchyroll logo on it. It sounds like a recipe for disappointment at best, and a failure to deliver what's promised at worst. 
