---
title: "“How can I link to your website blog?”"
date: "2024-02-27T08:11:15+11:00"
year: "2024"
category: Internet
tag:
- spam
location: Sydney
---
I read this email subject line, and heard "[left-side brain 📺](https://www.youtube.com/watch?v=_2qJy5r-WAY "Train, Hey Soul Sister")" when I read "website blog". I still can't believe I saw Train with my sister back in the day. But I digress.

I get these sort of spam emails more than daily, with the requisite replies saying they're "just following up" when they don't get a response. The bulk of my blocked senders list now consist of these sort of people, which in my weaker moments I've been tempted to sell to marketers themselves.

This was the latest one, from someone who was even too lazy to mail merge my name properly:

> Hello rubenerd.com
> 
> I recently had the pleasure of exploring your website and I'm interested in the possibility of collaborating with you to publish an article that includes a link to one of our websites
> 
> I'd love to discuss the details, including the remuneration for your assistance. Your insights and guidance in this matter would be greatly appreciated.
> 
> Looking forward to your response.
> 
> Best regards.

I decided to reply for once, just to see where this takes me! Probably right to another marketing list, knowing my luck.

> Hello fangosports.com,
> 
> The pleasure is all mine. As per my FAQ here, I charge $8,888 a character for paid posts, which I'm sure you've read by exploring my website:
>
>> https://rubenerd.com/omake.xml
>
> How long were you expecting the published article to be? I require upfront payment. I'll send an invoice and my payment details once you confirm. 
>
> Best regards,   
> rubenerd.com

Let's see what they come back with. If nothing, then I'll respond asking that I'm "just following up".

