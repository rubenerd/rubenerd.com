---
title: "Wishing I Was Lucky, Wet Wet Wet"
date: "2024-04-12T10:19:26+10:00"
year: "2024"
category: Media
tag:
- lyrics
- quotes
location: Sydney
---
> He would buy the best;   
> But never something new.

