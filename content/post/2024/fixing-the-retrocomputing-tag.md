---
title: "Fixing the retrocomputing tag"
date: "2024-01-19T08:05:07+11:00"
year: "2024"
category: Internet
tag:
- metadata
- weblog
location: Sydney
---
Have you checked out my [tag archive](https://rubenerd.com/tag/) recently? I wouldn't; it's a gigantic mess accumulated from nearly two decades of unstructured rambling.

I've got a project this year to clean them up, which involves dropping ones that are only used on single posts, correcting spelling miatakes [sic], and merging similar ones. One major disadvantage of static-site generation over a tool like WordPress is that it'll accept whatever metadata you type without validation (though maybe that's also something you could fix).

As an example, my retrocomputing tag had at least five variations:

* retro-computing
* retrocomputers
* retrocomputing
* retrocompputing
* retrocomuting

These have all been consolidated into [retrocomputing](https://rubenerd.com/tag/retrocomputing/). Really it should go into its own category at this stage; but then, my categories are basically useless thesedays.

Next step: reassign things that are in [retro](https://rubenerd.com/tag/retro/) if they're related to computers. Maybe this would be a task for next time I'm [feeling scatterbrained](https://rubenerd.com/coping-with-distractions/ "Coping with distractions, or something else?").
