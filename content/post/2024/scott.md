---
title: "Scott"
date: "2024-10-19T09:49:14+11:00"
thumb: "https://rubenerd.com/files/2024/scott@1x.jpg"
year: "2024"
category: Thoughts
tag:
- pointless
location: Sydney
---
I wonder who Scott is? Did they leave their mug behind?

<figure><p><img src="https://rubenerd.com/files/2024/scott@1x.jpg" alt="An abandoned mug on top of a cupboard with the name SCOTT printed on the side" srcset="https://rubenerd.com/files/2024/scott@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>
