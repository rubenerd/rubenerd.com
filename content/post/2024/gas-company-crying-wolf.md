---
title: "A residential gas company crying wolf"
date: "2024-04-25T15:51:15+11:00"
year: "2024"
category: Thoughts
tag:
- business
- spam
location: Sydney
---
For nearly two years, an Australian billing company (pardon, an "energy retailer"), has been sending threatening letters about cutting off our non-existent gas supply.

This is the most recent letter we've received:

> **DISCONNECTION WARNING NOTICE**
>
> TO THE UNREGISTERED ENERGY USER  
>
> **Important Information about your Gas Account**
>
> <span style="color:red; font-weight:bold;">URGENT - PLEASE READ</span>   
> <span style="color:red; font-weight:bold;">DO NOT DISREGARD THIS LETTER</span>
>
> You have received this disconnection warning notice because your gas is being supplied by $RETAILER but you have not contacted us to set up an account.
>
> We may disconnect your Gas supply after $DATE.

Next month, we'll receive another one. And it'll unceremoniously meet the same shredder the last fifteen or so have.

Just in case you missed it in the first sentence of this post, I said *non-existent* gas supply! Somehow our address ended up in their system, and we've been receiving threats ever since.

It's always three things with me, and as many of these strike me as weird and/or funny about this.

First, they *must* have it in their system that they've sent more than a dozen letters to this specific address without the desired outcome or resolution (namely, the signing up of a new retail contract for gas delivery). Why, after all this time, hasn't this been escalated to a human for review? Has an AI chatbot been running their system this whole time, or was it written by one!?

Second, it's clear their playbook simply can't handle this edge case. They're happy to send ever-escalating messages, but eventually when the outcome they want isn't realised, it resets and goes back to quietly encouraging us to sign up for a gas plan. Lather, rinse, repeat, with an electric hot water heater.

And third, these aren't cheap letters. Each envelope contains a ludicrously thick retail "contact" for a gas supply, along with another stapled booklet of terms of conditions thick enough with legalese to be as obtuse to the layperson as possible. This is by design; these contracts are designed to indemnify them, not to protect you. But the upshot is this one letter weighs as much as ten others, and likely comes with a price tag to match. And they're happy to keep sending it, every month, for almost two years.

I could call them up, wait in their support queue for hours, be told my call is important to them, then talk to an underpaid, abused, and overworked member of their call centre staff to explain they can't deliver gas to an address that *doesn't have the requisite piping*, to power appliances that *don't run on gas*. But that sounds like a lot of work for something that isn't my problem or responsibility.

The other side of me is almost tempted to sign up for a contract, then lodge an ever-escalating series of angry letters complaining that gas isn't appearing at my premises. I want my carcinogenic micro-particles and soot, damn it! Then see them attempt to explain why they can't deliver gas to a place they've been sending threatening letters to for almost two years, and to people they were happy to sign a legally-binding contract with.

But then I remember I have better things to do, and the reassuring sound of the shredder fills the room. Time to cook some food on our induction stove, me thinks. 

