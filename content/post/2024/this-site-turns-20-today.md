---
title: "This site turns 20 today"
date: "2024-12-21T19:43:55+11:00"
abstract: "I started this blog in 2004, and now the optometrist says I need bifocals!?"
thumb: "https://rubenerd.com/files/2024/rubi-tree@2x.jpg"
year: "2024"
category: Internet
tag:
- milestones
- personal
- weblog
location: Sydney
---
It's official, my blog is now older than some people attending university today! And driving! And drinking outside the US! Have I mentioned I'm in my late thirties and feeling old? I went for an eye checkup today, and the word *bifocals* was mentioned for the first time. **NO SIR, I REJECT YOUR MEDICAL ADVICE!**

Where was I going with this?

I'll be Frank (or Janice, or whomever), I haven’t blogged much over the last few days, because I’ve been mentally logjammed trying to figure out an appropriate way to celebrate. **Twenty years of blogging, what!?** It feels like a massive milestone worthy of more than I have the mental space for right now. But maybe a rambling post in lieu of something substantial will suffice.

Let's take a trip down memory lane first. Knowing my luck, it'll be on a power cord. Get it, because tripping and... hey, shut up. My [first blog post here](https://rubenerd.com/the-first-post/) was on the 21st of December 2004. I'd finished my high school exams, and had time to kill over the Christmas holiday break. I've found this time of year the most rewarding and fun for personal projects and learning, because the world around me seems to take a break from interruptions. I call it the Xmas Buzz, and it's wonderful. *But I digress.*

My late mum instilled in me a daily journaling habit, from which I have volumes spanning from when I was 6 to the present day. I'd also written under pseudonyms on other sites before (as I'm sure we all did), and written a bunch of novels that hopefully never see the light of day. But this site, with that awful childhood concatenation of *Ruben* and *Nerd*, was the first time writing under my own name, on my own domain, on a site I controlled.

*(I also started experimenting with this newfangled thing called [podcasting](https://rubenerd.com/show/), which I ended up doing in lieu of writing for a year or two. I still think it's a lot of fun, but perhaps the less said about it the better).*

I didn't have a plan to speak of, but on the first post I said:

> I don't know, but the idea of keeping a record of my life, however dull it is, might be fun to look at in years gone by! [&hellip;] I thought that maybe one percent of it, or maybe two, might be useful to someone, especially with respect to some of the tech problems I've had and solved over the years. So here it is.

I suppose the site is a success by those vague metrics. I think, maybe?

**BIFOCALS!?** Look at this teenager who wrote that first post, do you think **HE** needs bifocals!?

<figure><p><img src="https://rubenerd.com/files/2024/57864144_595f2f8ba2_o@1x.jpg" alt="It’s me! Back when I had hair!" srcset="https://rubenerd.com/files/2024/57864144_595f2f8ba2_o@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

More to the point: how do you summarise twenty years of a site? Twenty years of a life spent exploring and thinking and doing things, some of which ended up on a personal journal that others can read? Five percent of which you might even be vaguely proud of, even if you [struggle to say so](https://rubenerd.com/talking-up-oneself-via-an-actor/ "Talking up oneself, via an actor")?

The insufferable, technical side of me wants to enumerate the dozens of CMSs I've run, from a Perl CGI script I hacked up one evening, to stints with Movable Type and WordPress, then to static site generators like Hugo running on FreeBSD and... see, I'm hopeless, I can't control myself. I could talk about the topics I covered, or go through the [giant mess of tags](https://rubenerd.com/tag/), or the [yearly archives](https://rubenerd.com/year/). I think we both have better things to do.

Instead though, I'm going to pontificate on something with the potential for much more cringe: how this has felt to write.

For most of the history of this site, is eight words. I write not because I'm especially good at it, but because I enjoy it. Setting up on the balcony in the morning with a cup of coffee&mdash;like a gentleman&mdash;and looking at that blank text editor is just about the greatest feeling in the world, even if really I was only posting for myself. In the words of my coffee grinder, that was *fine*.

In the last few years though, is six more words. I now have enough of you reading that I had to bump up the specs on my server this year, *twice*. The amount of email I receive is overwhelming, in a great yet terrifying way. It felt so gradual, and yet so sudden. There are people out there, whom when given a choice between billions of different sites, decide to make *mine* a part of their day. It beggars belief, and yet, here you are. Way to go making me feel nervous.

I guess I'm also coming to terms now with the fact that this silly teenager's site is now the single longest running project I've ever maintained. Part of me thinks I should have much more to show for 1,333,446 words over 9,500 posts, yet I wouldn't have wanted to change a thing. Well, almost.

<figure><p><img src="https://rubenerd.com/files/2024/rubi-tree@1x.jpg" alt="Rubi in front of a Christmas tree at night." srcset="https://rubenerd.com/files/2024/rubi-tree@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

There *are* two things I wanted to launch here to coincide with this anniversary, but I decided to postpone them for when I'm in a healthier headspace. Vim `:set spell` insists that's a typo, but I swear I saw that term unhyphenated before. Un-hyphenated? *DAMN IT.*

In the meantime, I can't think of anything profound or pithy to say. I just wanted to thank you all for reading, listening, and sending me your thoughts, whenever it was you joined me here. Who knows, maybe in another twenty I'll still be posting, rocking some of the **sexiest bifocals** you've ever seen. I don't think either of us needed to read that.

Cheers `^_^/`.
