---
title: "A beautiful early afternoon"
date: "2024-04-24T20:25:36+10:00"
thumb: "https://rubenerd.com/files/2024/beautiful-day@1x.jpg"
year: "2024"
category: Thoughts
tag:
- sydney
location: Sydney
---
Clara and I went for a lunch break walk yesterday. The leaves and sky were gorgeous.

People use the phrase *touch grass* as a bit of an insult thesedays, but honestly, it's probably one of the best things you can do.

<figure><p><img src="https://rubenerd.com/files/2024/beautiful-day@1x.jpg" alt="A suburban street in Sydney with some autumn leaves." srcset="https://rubenerd.com/files/2024/beautiful-day@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>
