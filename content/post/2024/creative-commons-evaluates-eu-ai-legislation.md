---
title: "Creative Commons evaluates EU AI legislation"
date: "2024-06-08T23:18:37+10:00"
year: "2024"
category: Ethics
tag:
- creative-commons
- generative-ai
- law
location: Sydney
---
Long-time reader Damon shared this [this briefing](https://creativecommons.org/2023/10/23/cc-and-communia-statement-on-transparency-in-the-eu-ai-act/) Creative Commons put out in October last year, and it's a doozy.

> Like the rest of the world, CC has been watching generative AI and trying to understand the many complex issues raised by these amazing new tools.

Amazing!

> AI developers should not be expected to literally list out every item in the training content.

I... I beg your pardon? Why should *scale* let someone get away with copyright and licence laundering? If attribution isn't feasible, that's on *you*.

Oh sorry officer, I took money from so many investors I lost track of them! I can't be expected to remember *all* their accounts!

This is a disappointing perspective from an organisation that's done so much digital good over the years. So much so that I'm reconsidering my use of their licences.
