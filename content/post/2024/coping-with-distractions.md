---
title: "Coping with distractions, or something else?"
date: "2024-01-13T09:36:23+11:00"
year: "2024"
category: Thoughts
tag:
- psychology
- work
location: Sydney
---
I've been struggling a *lot* with distractions over the last couple of years. I'll have something I need to do, or even *want* to do, and there's this invisible mental barrier I can't seem to punch through. It doesn't even seem to be strongly correlated with what I'm doing, where I am, or my energy levels.

I've been at this coffee shop for an hour this morning, and it's been almost impossible to focus on anything. My eyes dart from the macOS dock, to an email on my phone, to an awful song playing on their speakers, to wondering if what I'm saving for is useful, to updating packages. I'll do each in turn for 10-30 seconds before darting to the next one. It leaves me feeling unproductive, unfulfilled, and frustrated!

But here's the thing. I was here yesterday, with more to do, and didn't have this problem whatsoever. I was able to sit here and write dozens of pages of a compliance document without a second thought. I did probably the equivalent of a week's work in a few hours.

I've been blaming open-plan offices and always-online chat rooms for this in a work setting, but I think there's something more going on. While constant interruptions, discussions, and distractions are certainly ruinous for productivity and focus, I think it only exacerbates something else.

Talking with friends who have ADD and related mental challenges, I think it's unlikely I have it. These tend to be life-long afflictions, and I only seem to have noticed it fairly recently. Maybe it's another way burnout manifests? But then, how does one explain the variability?

One thing is for sure, it's taken an hour to write this post! An ancient Avril Lavigne song came on the radio, which reminded me of high school, which took me back to this one weird interaction involving pretzels. There was that great German restaurant in Singapore called Werner's Oven, and they... wait, didn't they have that issue with their Wi-Fi modem that time? We need a new Wi-Fi access point at home, and probably a new pfSense box, maybe one of those cute passively-cooled boxes that, mmm, passively cooled, I need to book the aircon servicing people because the filter needs changing. Is that how you spell Avril Lavigne? Why did pkgsrc error out on that LaTeX package, did I... oh, my tethering dropped off, DAMN IT I keep forgetting to switch over to the new phone with all my profiles and, did I remember to charge the other one? Which dock is it connected to again? Something about tables.
