---
title: "Farewell to AnandTech"
date: "2024-08-31T09:45:53+10:00"
year: "2024"
category: Hardware
tag:
- goodbye
- jornalism
- writing
location: Sydney
---
[Ryan Smith](https://www.anandtech.com/show/21542/end-of-the-road-an-anandtech-farewell)\:

> It is with great sadness that I find myself penning the hardest news post I’ve ever needed to write here at AnandTech. After over 27 years of covering the wide – and wild – word of computing hardware, today is AnandTech’s final day of publication.

This definitely got to me this morning.

AnandTech has been one of *the* resources the technically-minded have gone to get news and analysis. Anand Lal Shimpi's early posts about Socket 7 informed the [first computer I built](http://retro.rubenerd.com/mmx.htm) in 1998 when I was a kid, and I've read them ever since. Their site was among the first RSS feeds I ever subscribed to, along with Tom's Hardware. More recently, I knew that when the YouTube or social media drama mill was hyperfixated on something, I could go to AnandTech to get a considered perspective.

Thank you to the whole team, I wish you all nothing but success. ♡
