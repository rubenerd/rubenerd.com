---
title: "Euromaidan, and Alexei Navalny"
date: "2024-02-23T12:17:24+11:00"
year: "2024"
category: Thoughts
tag:
- europe
- news
- peace
- russia
- ukraine
location: Sydney
---
This week marked the one decade anniversary of the end of the Euromaidan protests in Ukraine, and the Revolution of Dignity that ended the corrupt Yanucovych regime. Having lost his puppet, Putin began his invasion of the country in Crimea, which he accelerated in 2022 with his lie of a "special military operation".

We've also had the news recently that Alexei Navalny died while in custody in Russia. If you believe the official narrative, you probably bought the lie of WMDs in Iraq too.

These are dark times for my Eastern European friends. It's also not much better in other parts of the world right now. I wish I had something profound or useful to say, but I'm a loss for words.

Here's a novel idea for aggressor politicians everywhere: *stop killing people.* I know, what a fucking concept.
