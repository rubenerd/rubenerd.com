---
title: "Australian embassy reopening in Ukraine"
date: "2024-12-21T06:38:05+11:00"
abstract: "The ambassador to Ukraine, Paul Lehmann, and the deputy head of mission will return to Kyiv in January."
year: "2024"
category: Travel
tag:
- news
- ukraine
location: Sydney
---
[Dominic Giannini and Tess Ikonomou](https://www.standard.net.au/story/8851010/australia-denounces-russia-ahead-of-kyiv-embassy-return/)\:

> Australia will reopen its embassy in Kyiv and give cash to a recovery fund as it doubles down on support for Ukraine "at a critical time in this conflict".
> 
> Foreign Minister Penny Wong toured Ukraine and met with Ukraine's Prime Minister Denys Shmyhal, Foreign Minister Andrii Sybiha and Energy Minister Herman Halushchenko.

I've said here before, but Clara and I can't wait to go there for ourselves. I've been interested in Ukraine since seeing my first Antonov in Singapore as a kid. The people, food, art, culture, engineering, everything. 💙💛 
