---
title: "Whatever happened to Akai?"
date: "2024-05-05T09:17:38+10:00"
year: "2024"
category: Hardware
tag:
- business
- hi-fi
- japan
- music
- nostalgia
location: Sydney
---
My father was obsessed with Hi-Fi stuff, though I only started getting really stuck into it during COVID in 2020. Our house was decked out with Pioneer, Sony, Marantz, and Onkyo; I'd heard the names Kenwood, Akai, and TEAC; and I coveted anything made by Matsushita (like Technics and Panasonic). I have a whole separate post pending about Panasonic's portable music range, and how I loved their industrial design even more than Sony's Walkman range.

*GO KANSAI!* But I digress.

There was a time as a kid that I thought Akai was the same as Aiwa, that made the personal stereo I had hooked up to my iMac in my bedroom. I knew them vaguely for their mixing consoles and digital samplers that I saw at my first job, but didn't give them a second thought in the home. I don't think my dad had any Akai kit, though I'm sure I'm forgetting a bunch of the random boxes he had in that massive Hi-Fi cabinet. I don't even remember who made his AC-3 decoder; I assume it would have been Pioneer?

As another digression, Sydneysiders probably know Akai best for the massive faded logo on the side of that abandoned building in St Leonards. I haven't been there recently, but I wouldn't be surprised if it were still standing.

<figure><p><img src="https://rubenerd.com/files/2024/akai-r2r@1x.jpg" alt="Silver Akai reel to reel machine, bu Erkaha on Wikimedia Commons." srcset="https://rubenerd.com/files/2024/akai-r2r@2x.jpg 2x" style="width:420px; height:401px;" /></p></figure>

*Photo of an Akai GX-630D reel to reel machine, by [Erkaha on Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Tape_recorder_GX-6300.jpg).*

*Anyway!* Like so many Japanese conglomerates, Akai started in the early 20th century, but reached their industrial and sales peak in the 1980s. They made some gorgeous reel to reel machines, alongside a staple of amplifiers, musical instruments, and VCRs. Their 80s kit adopted that flat, angular black box design most Hi-Fi gear switched to, which I'll admit while not as beautiful as the silver stuff of the 1970s, it tickles my nostalgia centres having grown up with it. Their designs give me TEAC vibes.

But whatever happened to them? According to [Wikipedia](https://en.wikipedia.org/wiki/Akai "Wikipedia article on Aiwa"), they were subsequently embroiled in an accounting scandal that had them bought out by a company in HK, which now slaps the brand on everything but the kitchen sink. Akai got RCA'd, which is a shame.

If I were a billionaire, I'd buy up the IP and branding for a bunch of these old Hi-Fi and computer companies, set up R&D labs, and build bespoke hardware to sell under them. *Quality Nostalgia Holdings* I'd call it! What I'd lack in profitability, I'd make up for in volume. As in, how loud they'd be. That's probably the only good pun I've achieved here this year.
