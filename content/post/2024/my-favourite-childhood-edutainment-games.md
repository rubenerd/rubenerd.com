---
title: "My favourite childhood edutainment games"
date: "2024-07-28T08:53:13+10:00"
thumb: "https://rubenerd.com/files/2024/buzzy-airport@1x.png"
year: "2024"
category: Software
tag:
- games
- humongous-entertainment
- inventorlabs
- nostalgia
location: Sydney
---
[Wouter Groeneveld](https://brainbaking.com/post/2024/07/edutainment-games-of-my-childhood/) ([web feed](https://brainbaking.com/index.xml)) and [Luk Weyens](https://pensiveibex.com/blog/2024/7/edutainment-games/) ([web feed](https://pensiveibex.com/index.xml)) recently shared some of the edutainment games from their 1990s childhoods, so being of a similar vintage I thought I'd take a look too. Edutainment titles aspired to be the "hidden vegetables" of games, where kids would have fun at the same time as learning something... *maybe!*

In no particular order, we have:


### Buzzy Explores the Airport (1995)

<figure><p><img src="https://rubenerd.com/files/2024/buzzy-airport@1x.png" alt="Screenshot of the opening screen of Buzzy Explores the Airport, showing Buzzy!" srcset="https://rubenerd.com/files/2024/buzzy-airport@2x.png 2x" style="width:640px; height:480px;" /></p></figure>

*Buzzy the Knowledge Bug* was a tour guide for a series of exploration games based around the farm, the rainforest, and the airport. You could wander around the buildings, go into aircraft, and click on anything to hear those legendary *Humongous Entertainment* sound effects. Listen closely, and you'll hear the PA asking about a "small purple car", a reference to their *Putt Putt* games.

Similar to Microsoft's *Explorapedia*, the idea was you'd click on things that interested you, which would bring up an information screen. There were also minigames themed around the world, such as egg catching for the farm, and bag sorting for the airport. I wasted more than an hour playing these again this morning in the process of writing this post!

You can run all the HE games on modern hardware today using ScummVM. They're incredibly charming. Which thanks to Wouter, I've remembered I need to fire up *Freddi Fish* too; those kelp seeds won't find themselves.


### Mavis Beacon Teaches Typing 9.0 (1998)

<figure><p><img src="https://rubenerd.com/files/2024/mavis-beacon@1x.png" alt="Screenshot of a new install of Mavis Beacon." srcset="https://rubenerd.com/files/2024/mavis-beacon@2x.png 2x" style="width:640px; height:480px;" /></p></figure>

Mavis was a staple of every touch typist's desktop in the 1990s. Her lessons guided you through typing various key combinations for accuracy and speed, building up what you know as you reach certain milestones. It also had minigames! 

I was never a great touch typist, and lost a lot of muscle memory after an injury that took several years to recover from. I installed it in 86Box so I could take this screenshot, maybe I should give it a try again.

Honourable mentions in this class go to *Typing Tutor* for the Apple II, *Kid Keys* for DOS, and *Slam Dunk Typing* for MacOS/Windows 95.


### Word Rescue and Math Rescue (1992)

<figure><p><img src="https://rubenerd.com/files/2024/wordrescue@1x.png" alt="Screenshot of the first level of Word Rescue." srcset="https://rubenerd.com/files/2024/wordrescue@2x.png 2x" style="width:640px; height:400px;" /></p></figure>

Apogee were probably more famous for their *Duke Nukem* game franchise, but they were prolific shareware publishers. They released a series of edutainment titles including *Math Rescue*, and my personal favourite, *Word Rescue*.

They were simple side-scroller platformers in which you'd pick up letters to form words, or numbers to perform basic arithmetic. It wasn't as buttery smooth as my beloved *Commander Keen*, or even *Super Mario*, but they had the veneer of education! I have their PC speaker sound effects etched into my soul.


### InventorLabs Transportation

<figure><p><img src="https://rubenerd.com/files/2024/inventorlabs@1x.png" alt="Screenshot of the Wright Brothers' workshop in InventorLabs Transportation." srcset="https://rubenerd.com/files/2024/inventorlabs@2x.png 2x" style="width:640px; height:480px;" /></p></figure>

We end this list with not only my favourite Edutainment game, but probably one of my favourite games of all time. There were two *InventorLabs* titles, one for transport, the other for technology. They ran on Windows, but they were also the first games I ever ran on my blueberry iMac DV.

In *InventorLabs*, you'd be walked through the workshop of a famous inventor, and learn the fundamentals of how their inventions, devices, and discoveries worked. You'd then be given the task of assembling something using what you've learned. Should the wing on your Wright Flyer be short and wide, or long and narrow? How curved should it be? Have you considered weight?

The animations and graphics haven't stood up as well as others on this page, but it's still a lot of fun. I got through several school science classes thanks almost entirely to this software, which is the biggest praise one could give tiles in this genre.


### Conclusion

I might need to do a part two, I keep thinking of more! But these were my favourites.

I'm also relieved I can relive these relatively easy with emulators. People growing up on sealed smartphones with app stores and DRM might not find it as easy.
