---
title: "My fun experience at a Scorptec store (no, really)!"
date: "2024-05-13T08:27:07+10:00"
thumb: "https://rubenerd.com/files/2024/scorptec@1x.jpg"
year: "2024"
category: Hardware
tag:
- australia
- shopping
location: Sydney
---
Scorptec are a computer parts retailer in Australia, with most of their branches in New South Wales. Clara and I braved the absurdly wet weather Sydney has been drenched in for the last fortnight and went to their Macquarie Park branch over the weekend.

Like most vendors in this space here, you walk into the store and sit at a table while the handsome member of staff walks backstage to retrieve what you need from their massive warehouse. I was after a [Peerless Assassin CPU cooler](https://rubenerd.com/peerless-assassin-120-versus-120-se-cooler/ "Peerless Assassin 120 versus 120 SE cooler"), which was just as awkward a name to ask for as I expected. There was a bit of confusion as to where the box was, but they offered me free shipping if I didn't want to wait. Eventually one of his colleagues found the specific box, and we left with the model I was after.

<figure><p><img src="https://rubenerd.com/files/2024/scorptec@1x.jpg" alt="Blurry view of the Scorptec store with a lot of dreary drizzle!" srcset="https://rubenerd.com/files/2024/scorptec@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

Growing up in Singapore, I was used to wandering down to Sim Lim Square, Sim Lim Tower, or the pre-gutted Funan Centre whenever I needed computer parts, cables, blank media, or other gear. KL also had Plaza Imbi, where you were also able to source second-hand kit for very reasonable prices. I was also lucky to experience Fry's in Santa Clara when I worked there for a few months, just before they disappeared.

Not being able to wander the shelves does lose a bit of the magic, but it was still fun just being amongst it all again. Ordering stuff online and having it delivered is certainly easy, but you miss hearing the chatter of other people talking about topics you know about, and taking a look at the display cases.

I wonder what else we've lost with online retail?
