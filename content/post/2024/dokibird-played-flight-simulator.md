---
title: "Dokibird played Flight Simulator"
date: "2024-04-17T08:56:55+10:00"
thumb: "https://rubenerd.com/files/2024/yt-SbCfH5v3xDs@1x.jpg"
year: "2024"
category: Media
tag:
- cessna
- dokibird
- flight-simulator
- video
location: Sydney
---
I haven't watched it through yet, but a vtuber playing one of my favourite games of all time!? It was a salve to a troubled mind.

<figure><p><a target=_BLANK href="https://www.youtube.com/watch?v=SbCfH5v3xDs" title="Play 【FLIGHT SIMULATOR】I have a flying license remember?【Dokibird】"><img src="https://rubenerd.com/files/2024/yt-SbCfH5v3xDs@1x.jpg" srcset="https://rubenerd.com/files/2024/yt-SbCfH5v3xDs@1x.jpg 1x, https://rubenerd.com/files/2024/yt-SbCfH5v3xDs@2x.jpg 2x" alt="Play 【FLIGHT SIMULATOR】I have a flying license remember?【Dokibird】" style="width:500px;height:281px;" /></a></p></figure>

The downside to mostly following streamers from Hololive is that getting permissions can be trickier for a large agency (at least, from what I've been told by a couple of semi-insiders). When you're indie (once more!), you have much more freedom.

This post had (a lot of) brackets.
