---
title: "Qantas engine failure this afternoon"
date: "2024-11-08T16:51:47+11:00"
year: "2024"
category: Thoughts
tag:
- australia
- aviation
- news
location: Sydney
---
Social media was flooded with pictures of a smoky grassfire at Sydney Airport this afternoon, after an aircraft bound for Brisbane experienced an engine failure on take-off. Fun!

[Qantas released a statement](https://www.qantasnewsroom.com.au/featured/qantas-statement-on-qf520/ "Qantas statement on QF520")\:

> Qantas engineers have conducted a preliminary inspection of the engine and confirmed it was a contained engine failure.
> 
> While customers would have heard a loud bang, there was not an explosion.

More news on the [ABC](https://www.abc.net.au/news/2024-11-08/live-qantas-plane-emergency-landing-sydney-airport-grassfire/104578088 "Sydney Airport emergency landing live: Engine failure on Qantas plane mid take-off") and [SBS](https://www.sbs.com.au/news/article/qantas-passengers-on-board-qf520-speak-after-emergency-landing/4upgu2fv9 "'Disturbing': Qantas passengers speak after engine failure forced emergency landing").
