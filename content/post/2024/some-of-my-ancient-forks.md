---
title: "Some of my ancient forks"
date: "2024-12-22T08:05:02+11:00"
abstract: "“This branch is 232760 commits behind”!"
year: "2024"
category: Software
tag:
- git
location: Sydney
---
I was cleaning out my *Gith Ub* account after their latest volley of spam, and I had some impressively outdated forks:

> This branch is 5050 commits behind textmate/textmate:master.

> This branch is 47232 commits behind Homebrew/legacy-homebrew:master.

> This branch is 232760 commits behind Homebrew/homebrew-cask:master.

I've had pull requests accepted, but I guess I was also keeping these around for... some reason? It almost seemed a shame to get rid of them.

I'm continuing to move my stuff over to [Codeberg](https://codeberg.org/rubenerd). I believe in self-hosting what you can, but my repos are a form of remote backup for me.

