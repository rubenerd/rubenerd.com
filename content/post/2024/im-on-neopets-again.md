---
title: "I’m on Neopets again"
date: "2024-04-13T08:34:22+10:00"
thumb: "https://rubenerd.com/files/2024/neopets@1x.jpg"
year: "2024"
category: Internet
tag:
- games
- neopets
- nostalgia
- the-sims
location: Sydney
---
I resisted making another account for two decades, but the [time finally came](https://www.neopets.com/userlookup.phtml?user=md_dynamo). I couldn't believe how much was the same as I remember. *The Tiki Tack Tombola!*

There's plenty of blame to go around here, but the bulk of it goes to my sister. She has the [second-oldest active account](https://www.neopets.com/userlookup.phtml?user=elke) on the site, with a Neopet [who's 25](https://www.neopets.com/petlookup.phtml?pet=Amigo). I made an account around the same time, but had it frozen in 1999 when I left it logged in at school, and kids did what kids do. I'll admit, childhood Ruben was shattered.

<figure><p><img src="https://rubenerd.com/files/2024/neopets@1x.jpg" alt="Screenshot of my Neopets profile" srcset="https://rubenerd.com/files/2024/neopets@2x.jpg 2x" style="width:500px;" /></p></figure>

Pecanio is a reincarnated, caffeinated version of my original two Flotsams, with a pecan-forward version of the name my first had, and the colour of my second. His stats are also all unremarkable save for his *heavy* defence, because of course I was going to choose that. And he has a cute hat! *But enough about me*.

I'm [MD_Dynamo](https://www.neopets.com/userlookup.phtml?user=md_dynamo) if you're still around there too. It's a [Sims 1 House Party](https://www.youtube.com/watch?v=YQV7_9QerhE "Disco 3") reference, another thing from our childhood. *Eva talc nadey hey hee hidey ho. Ah becha nadey M.D. Dynamo... yeah*. What's the [RFC5646](https://datatracker.ietf.org/doc/html/rfc5646) code for Simmish?

