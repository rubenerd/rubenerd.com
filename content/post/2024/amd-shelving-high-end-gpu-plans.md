---
title: "AMD shelving high-end GPU plans"
date: "2024-10-13T09:17:25+10:00"
year: "2024"
category: Hardware
tag:
- amd
- business
- gpus
- news
location: Sydney
---
Graphics cards have been in a weird place for years now. They've bucked the trend in price and performance of the rest of the market, and have been integral to the current hype train sweeping the industry. Most surprisingly, a lot of this has had very little to do with *graphics* at all, which is wild to me.

We don't need to rewind the clock very far before we were living through *Blockchain Tulip Mania*, with cards being scalped for twice, sometimes three times their recommended retail price. Cryptocurrencies needed the massive parallel processing of these cards, and manufacturers were happy to supply them. The current *GenAI Tulip Mania* has substituted wasteful mathematics for inefficient slop, but the lack of a viable business model, and the underlying hardware and resource requirements, have largely remained the same. Give us GPUs to make nonsense, *consarnit!*

It's in that environment that we find ourselves [reading that interview last month](https://www.tomshardware.com/pc-components/gpus/amd-deprioritizing-flagship-gaming-gpus-jack-hyunh-talks-new-strategy-for-gaming-market) with Paul Halcon of Tom's Hardware fame, and Jack Huynh from AMD. I've been mulling over the implications of AMD conceding the high-end GPU market for weeks now, and I think I see a silver lining.

As mentioned at the start, the *graphics* in graphics cards have seemed to take a back seat for a while now... well, unless you consider stolen art with six fingers, I suppose. Game reviewers talk big about what GPU manufacturers are bringing to the table, but the reality is the industry mostly treats games as an incidental side effect of their hardware. Why fret over graphics cards for gamers when you can sell higher-margin SKUs of your cards to enterprise customers? And to be clear, I mean **MUCH** higher-margin.

This is where I'm actually bullish, and dare I say *optimistic* about AMD's plans here. It's refreshing to hear a company talk about optimising cards for the rest of us. Sure, a halo card from Team Green is the best graphics card for games and guff, but how many people can afford them, *especially* in the current economic climate?

This is an opportunity for AMD. Their graphics are already the *de facto* standard for consoles, portable gaming devices, and their Linux desktop gaming drivers are so much easier to deal with. Sure, these markets are far smaller than AAA gamers on Windows, or data centres who want to train vast datasets for the sheer thrill of burning investor money without any prospect of a return.

But here's the thing. One thing I've seen *time and time again* in this industry is how far good will can take you; and how easy it is to lose. People are once more feeling abandoned and disconnected from this current wave of GPU development, and are despondent about its future. If AMD play their cards right with price, features, price, and price&mdash;did I mention price?&mdash;they could take a meaningful and sustainable chunk of the market back. That will benefit everyone.

I'll be driving my RTX 3070 until it falls apart, despite how frustrating it now is to use given I now play games on Fedora instead of Windows. But fingers crossed my next card will be an AMD.
