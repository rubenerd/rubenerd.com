---
title: "Figuring out water"
date: "2024-11-03T09:25:16+11:00"
year: "2024"
category: Thoughts
tag:
- health
- personal
location: Sydney
---
*Hydration check* is one of the more endearing and fun things I see fans post to vtubers and streamers; and you know its effective based on how often you'll hear a *glug* shortly after. It's easy to get tunnel vision and be so absorbed in what you're doing that you forget the fundamentals of being a human. Before I met Clara, I was known to hack on things well into the night without remembering to eat, drink, or do other things.

Truth is, none of us probably get enough water, or as climate-change deniers write, `H20`. But how much I actually need is something that has been frustrating to pin down. Some will claim you need to drink 8-10× your body weight every hour, while others say the need for water is overblown. My local GP takes the attitude that "you're probably fine, but you could also probably drink more of it".

I've taken an interest in it again recently on account of a couple of things:

* I'd been waking up absolutely *parched*. I've kept a fresh glass of water by the bed since before I started school, but irrespective of how much of it I drink, I wake up feeling like I should have had more. Sometimes I get up multiple times to refill it. It seems weird that I drink as much as I can, and I still wake up with a dehydration headache!

* Alcohol has been another. I don't drink much, but it was getting to the point where I couldn't have a single glass of [Pilsner Urquell](https://www.pilsnerurquell.com/) without feeling *crushingly* hungover the next day.

I wasn't sure if water was related, but it seemed like it could be? So I did what every self-respecting computer engineer did: I built a database table to track my water intake! Then realised this was a bit of a pain, and made a spreadsheet instead.

The scientific or medical value of this data was questionable given I immediately fell into the [Hawthorne Effect](https://rationalwiki.org/wiki/Hawthorne_effect). I noticed I was drinking a *lot* more water once I started tracking it, and more regularly. That's probably a good thing, though a proper study would demand I drink the same amount as before as a control before tweaking it as a variable. But I didn't want to maintain the water intake I had before, given how rubbish it made me feel.

Just like those vtubers, I also noticed it was useful to have prompts. Washing my hands? Have a drink. Sitting back at my desk? Have a drink. Having a coffee, tea, or any food? Have a drink. Having a drink? Have a drink. 🧊

In news that shocks absolutely nobody, I found that drinking more water during the day lead to better sleep and waking up without feeling parched. In some cases I woke up with the glass by the bed still mostly full, and I *still* felt fine. Likewise, I was able to have a beer or a whiskey without issue (though I still abstain from wine for its unique ability to make me feel absolutely terrible).

This could well be confirmation bias, and I don't understand the specific mechanics involved here, but I have a theory. It could be that I was dehydrated during the day, and it was exacerbated while I slept for hours unable to drink. Pre-loading water during the day meant I had enough in reserve when I slept, so I woke up feeling like a glass of water would be nice, not critical to avoiding a headache. I've definitely learned that I need to drink more water after coffee and tea as well.

*Your mileage may vary*, but... hydration check!
