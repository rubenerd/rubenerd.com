---
title: "But chatbots are better than $FOO!"
date: "2024-07-13T08:48:43+10:00"
year: "2024"
category: Ethics
tag:
- generative-ai
- spam
location: Sydney
---
I really am done thinking about the tragicomedy of generative AI, and am already wondering [what comes after this latest bubble](https://rubenerd.com/where-does-the-industry-go-after-ai/ "Cautious optimism for the industry after AI pops"). Here comes the proverbial posterior prognostication: *but...*, there's one final perspective on all this tech I want to call out.

Ask chatbots the same question three times, and you'll get as many answers. Sometimes they can approximate the correct answer, or lead you in the right direction (albeit at steep environmental and ethical cost, but who cares, amirite?), but their reliability is constrained by mathematics (sorry, [Malcolm Turnbull](https://www.newscientist.com/article/2140747-laws-of-mathematics-dont-apply-here-says-australian-pm/ "New Scientist: Laws of mathematics don’t apply here, says Australian PM")).

Presented with this reality, people have been invoking whataboutism to defend the technology. You see, chatbots might be inconsistent, but this other `$THING` is worse. You can substitute `$THING` with whatever bugbear you possess, from the quality of Wikipedia, to journalism, to democracy. I don't buy it.

*(I briefly had links and names to famous people in the industry for those claims, but I decided to remove them. I don't mean to call out specific people, because their positions are widely held. I also don't want to cop the inevitable abuse)!*

First, it presupposes that massive quantities of AI-generated slop will address these issues. How else can you defend something that demonstrably hampers finding facts online? If not, defenders have to admit that chatbots are a necessary abstraction over the problem to which they're contributing, like a coal-fired air purifier. I dub these positions *technical tarpits*, because they're messy and inescapable.

But even if these chatbots weren't Ouroboros incarnate, it's a non-sequitur to claim other things are worse. Sure, Wikipedia admins are obnoxious and guided by self-defeating rules regarding notability (ask me how I know!), but they're better than a North Korean censorship board. It's frustrating when politicians avoid answering questions like this, and I don't accept such a distraction here either.

The core issue here is that chatbots, and generative AI more broadly, lack an audit trail. Or more specifically, they *can't* have an audit trail. Wikipedia has nonsense, but you can check the logs and see who put it there, what was changed, and maybe even why. Chatbots can only be black boxes, which even leaving aside ethical and legal concerns (it's depressing how often I have to write that), it should be terrifying for those concerned with objective truth.

The lack of maturity surrounding discussion of this tech isn't surprising, though I wish the industry had learned from the previous bubbles. All together now: *[we won't get fooled again](https://www.youtube.com/watch?v=_NzLs-xSss0)!*

Time to invoke my generative AI filters once more. Can someone tell me when it's safe to read stuff again?
