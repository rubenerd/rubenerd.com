---
title: "Exporing OpenRailwayMap"
date: "2024-03-13T09:10:33+11:00"
year: "2024"
category: Travel
tag:
- japan
- kansai
- hankyu
- maps
- mrt
- public-transport
- singapore
- trains
location: Sydney
---
Danny Harmon on [Distant Signal 📺](https://www.youtube.com/user/distantsignal) clued me into [OpenRailwayMap](https://openrailwaymap.org/), a OpenStreetMap-style site but for global railways. It has freight lines, passenger lines, branch lines, HSTs, light rail, and metro systems, all on the one map you can zoom into and explore.

Here's a view of the most hectic part of the world when it comes to rail. Look at all that glorious red; I hope we start to see that in India soon too.

<figure><p><img src="https://rubenerd.com/files/2024/world-openrailmap@2x.png" alt="OpenRailwayMap showing Europe and Asia's extensive rail systems" style="width:500px; height:375px;" /></p></figure>

Here's a closeup of central Singapore, showing the above and below-ground MRT lines. One day we'll have that orange KTMB line on the Malaysian side connected too.

<figure><p><img src="https://rubenerd.com/files/2024/singapore-openrailmap@2x.png" alt="OpenRailwayMap showing Singapore's rail system" style="width:500px; height:375px;" /></p></figure>

And what global tour would be complete without seeing my favourite system in the whole world, the maroon Hankyū in the Kansai region! Okay, the JR Central and Keihan pictured are also pretty great.

<figure><p><img src="https://rubenerd.com/files/2024/kansai-openrailmap@2x.png" alt="OpenRailwayMap showing a portion of the systems in Japan's Kansai region" style="width:500px; height:375px;" /></p></figure>
