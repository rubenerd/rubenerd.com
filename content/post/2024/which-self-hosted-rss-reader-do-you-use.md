---
title: "Which self-hosted RSS reader do you use?"
date: "2024-10-30T15:54:59+11:00"
year: "2024"
category: Software
tag:
- rss
- web-feeds
location: Sydney
---
I'm running [another Mastodon poll](https://bsd.network/@rubenerd/113394614272945405)\:

> Messing with XML::LibXML has been fine the last 5+ years, but I think it's time to upgrade to something proper.
> 
> What do people recommend?
> 
> Preferences are for Postgres, and bare metal/VM install over a container. PHP/Python fine, would rather not deal with Node.

Feel free to vote and leave a comment.

I used KDE Akregator and Thunderbird/SeaMonkey for years, but I created an account on The Old Reader recently to try remote reading. It's been great being able to read on the phone, and seeing those articles marked as read when I get back to the desktop.

Full transparency: I'm probably going with Miniflux, because its [opinionated](https://miniflux.app/opinionated.html) in largely the same ways I am. But I'm open to trying alternatives too.

