---
title: "Explaing how or why something is"
date: "2024-04-29T15:13:36+10:00"
year: "2024"
category: Thoughts
tag:
- comments
location: Sydney
---
I've been on the Web long enough to know that responses to a technical opinion fall into one of two camps:

1. How something got to be the way it is.

2. Why (or the corollary: why should it still be that way).

My experience is that people who don't understand the difference tend to answer the first way, and those who think bigger picture think the second way.

Neither are more *correct* than the other, because they tackle different aspects of a problem or system. But friction arises in online chats when groups from either camp discuss something, and invariably talk past each other.

Take smartphones. Someone might express frustration at a confusing piece of UI, like iOS's mix of conference call icons. How would these two groups approach it?

A *how* person will explain that touch screens have finite space, what the icons represent, and maybe reference a user-interface guideline. In other words, how the device works, how it presents those choices, and the technical constraints, even if they acknowledge the outcome is suboptimal.

A *why* person won't consider that a sufficient response, because it doesn't explain the motivations or decisions. Why does the user-interface guideline permit such behavior? Why do these technical limitations exist? Why does it have to be this way going forward?

To a *why* person, a *how* response sounds like [tacit acceptance](https://www.collinsdictionary.com/dictionary/english/tacit-acceptance). To a *how* person, a *why* response sounds like [hand-waving](https://en.wikipedia.org/wiki/Hand-waving). Sparks fly, impassioned responses ensue, and nothing productive is gained.

I was a *how* person for most of my life, but now lean towards *why* in my thirties. A mix of both is probably most useful though, which is something I've decided to work on.
