---
title: "Visiting Patterson Lakes for first time since 1992"
date: "2024-03-28T09:26:43+11:00"
thumb: "https://rubenerd.com/files/2024/patterson-lakes-bream@1x.jpg"
year: "2024"
category: Travel
tag:
- apple-ii
- melbourne
- nostalgia
- patterson-lakes
location: Melbourne
---
Today I got to share a bit of my childhood with Clara!

I was born in Sydney, but my family moved to Melbourne before my first birthday, so I remember none of it. My parents intended to live in Melbourne permanently, so they bought a house for the cost of a sandwich in a new development well south of the city called Patterson Lakes. A few years later, and we'd sold up and moved to Singapore. Live comes fast sometimes.

Technically I *did* briefly come back here with friends in 2007 when I was studying in Adelaide, but I didn't have the opportunity to really explore. So for my birthday yesterday we got the Frankston train from Flinders Street Station down to Carrum, then walked down the road to check it out.

<figure><p><img src="https://rubenerd.com/files/2024/patterson-lakes-carrum@1x.jpg" alt="Photo from above the station platform showing the stunning blue sky and turqouise water." srcset="https://rubenerd.com/files/2024/patterson-lakes-carrum@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

Firstly, the new Carrum station is stunning! It's elevated above the ground as part of the state government's level crossing removal project, so you get to see a view of the bay that would otherwise be hidden behind houses. The colour of the water and sky on this brisk morning were something else. *Australia!*

And of course, Clara got a silly photo of me beneath a sign pointing to the main event! You can take a few different buses, but we chose to walk and get some exercise after an especially heavy breakfast of pancakes (cough).

<figure><p><img src="https://rubenerd.com/files/2024/patterson-lakes-sign@1x.jpg" alt="An awkward gentleman posing below a direction sign for Patterson Lakes." srcset="https://rubenerd.com/files/2024/patterson-lakes-sign@2x.jpg 2x" style="width:375px;" /></p></figure>

Crossing the bridge into Patterson Lakes immediately sent a rush of nostalgia through my... nostalgia bones. The suburb was built around a series of artificial and natural lakes that are connected to Port Phillip with a small canal. It's funny to think than two decades later I'd be [studying in *Mawson* Lakes](https://rubenerd.com/eerie-mawson-photos/ "2009: Eerie afternoon in Mawson Lakes photos") in Adelaide built around a similar concept.

<figure><p><img src="https://rubenerd.com/files/2024/patterson-lakes-lake@1x.jpg" alt="View of one of the Patterson... Lakes from the bridge into the suburb." srcset="https://rubenerd.com/files/2024/patterson-lakes-lake@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

The beautiful weather suddenly started to turn, because this is Melbourne! The joke among Australians is that if you don't like Victorian weather, just wait an hour because it'll change. We decided to brave it and walk down my childhood streets first.

All the streets around this area were named for fish. This first street branching off *Snapper Point Drive* was named for Bream, which my childhood self struggled to say for some reason. *Brim?* The trees had also grown since last time I'd been here; I remember as a kid being able to touch the tops of saplings with my hands, and now they're well established. We grew up together, isn't that nice?

<figure><p><img src="https://rubenerd.com/files/2024/patterson-lakes-bream@1x.jpg" alt="Sign on a suburban street saying Bream Bay" srcset="https://rubenerd.com/files/2024/patterson-lakes-bream@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

I knew that a few things had changed about the suburb already from looking at Street View, including the soulless replacement of the *Tucker Bag* supermarket for another Safeway, then a Woolworths. The front of the Community Centre had also been repainted from a fetching light pink to a dreary grey, and a weird addition added to the front. I remember "phys ed", school assemblies, and special events taking place in "The Community Centre", and being very nervous when it was my turn to present!

<figure><p><img src="https://rubenerd.com/files/2024/patterson-lakes-cc@1x.jpg" alt="View of the refurbished Community Centre." srcset="https://rubenerd.com/files/2024/patterson-lakes-cc@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

And what nostalgia trip would be complete without a walk down to my first school? I went to Patterson Lakes Primary right in the throws of the conservative state premier Jeff Kennett's reign in the 1990s, so half of us were in what the teachers charitably referred to as "portables", but were really shipping containers with windows. I'm glad to see some more modern buildings among the older brick ones. Investing in education!? What a concept!

<figure><p><img src="https://rubenerd.com/files/2024/patterson-lakes-school@1x.jpg" alt="An awkward gentleman outside his old primary school." srcset="https://rubenerd.com/files/2024/patterson-lakes-school@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

In addition to our house here being the first place where I used a computer, this school was also where I used my first Apple II! The school "lab" had a bunch of Apple IIe and IIgs machines that were donated by Apple under the principle of hooking them young. They were already on their way out by the time I was in school, but I guess schools weren't about to throw away stuff that worked. And it only took me [three decades](https://rubenerd.com/remembering-the-apple-iie-platinum-and-finally-finding-one/ "Remembering the Apple //e Platinum, and finally finding one!") to get one of my own!

<figure><p><img src="https://rubenerd.com/files/2024/patterson-lakes-marina@1x.jpg" alt="View of the giant Patterson Lakes marina building." srcset="https://rubenerd.com/files/2024/patterson-lakes-marina@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

Coming here made me wonder how my sister's and my lives would have been different if we didn't move overseas. At the time I liked being here, but that was because I didn't know anything else. I can't stand suburbia now; I'd rather be closer to town, public transport, and amenities than live out in far flung nowhere. I though that was me being weird until I learned about modern urbanism, and realised a *lot* of people feel the same way.

Still, it's oddly comforting to know this place still exists. I wonder how it'll look in another two decades when I come back again?
