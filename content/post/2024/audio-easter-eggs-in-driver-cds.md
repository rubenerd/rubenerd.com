---
title: "Audio easter eggs in driver CDs"
date: "2024-02-15T10:37:51+11:00"
year: "2024"
category: Media
tag:
- cds
- creative
- easter-eggs
- music
location: Sydney
---
The original *Yellow Book* standard included a provision for CDs to hold computer data and traditional PCM audio on the same disc. The industry seemed to have coalesced on the term *Mixed Mode CD*, though the original standard didn't call them that. [Wikipedia](https://en.wikipedia.org/wiki/Mixed_Mode_CD "Wikipedia article on Mixed Mode CDs") also describes them as *enhanced CDs*.

I remember games like Midtown Madness and Age of Empires II had hidden audio tracks on their game CDs, which I discovered when iTunes offered to "rip" these game CDs when launched. Naturally I did, and I still have them in my music library today! Some of those tracks were, to use the Zoomer terminology, *bangers*.

What I only learned shockingly recently was that some of the driver CDs I've been carrying around for three decades also have these hidden tracks. Creative Labs included hidden audio on most of their Sound Blaster discs, presumably so you could use them to test your audio setup and their CD deck application. Or maybe they were just meant as a surprise, who knows.

<figure><p><img src="https://rubenerd.com/files/2024/sb16-cover@1x.jpg" alt="" srcset="https://rubenerd.com/files/2024/sb16-cover@2x.jpg 2x" style="width:500px; height:203px;" /></p></figure>

You can see this for yourself by downloading a Sound Blaster driver CD with BIN/CUE file extensions from the Internet Archive, such as [this one from 1995](https://archive.org/download/sb-16_202206 "Sound Blaster 16 Driver CD"). Then you can run them through Heikki Hannikainen's bchunk(1) tool with the `-w` option:

    $ bchunk -w drivers.bin drivers.cue output

This shows us the original audio tracks of the ripped disc, and writes them out to a data ISO file, and WAV audio files:

    Reading the CUE file:
       
    Track  1: MODE1/2352    01 00:00:00
    Track  2: AUDIO         01 10:20:41
    Track  3: AUDIO         01 14:40:73
    Track  4: AUDIO         01 16:38:01
    Track  5: AUDIO         01 18:59:31
    Track  6: AUDIO         01 20:47:73
    Track  7: AUDIO         01 22:26:68
    Track  8: AUDIO         01 24:05:43
    Track  9: AUDIO         01 25:16:63
    Track 10: AUDIO         01 26:09:13
       
     1: out01.iso   90/90   MB  [********************] 100 %
     2: out02.wav   43/43   MB  [********************] 100 %
     3: out03.wav   19/19   MB  [********************] 100 %
     4: out04.wav   23/23   MB  [********************] 100 %
     5: out05.wav   18/18   MB  [********************] 100 %
     6: out06.wav   16/16   MB  [********************] 100 %
     7: out07.wav   16/16   MB  [********************] 100 %
     8: out08.wav   11/11   MB  [********************] 100 %
     9: out09.wav    8/8    MB  [********************] 100 %
    10: out10.wav   32/32   MB  [********************] 100 %

I then ran this through [lame](https://lame.sourceforge.io/ "lame audio encoder homepage") and added cover art with [Kid3](https://kid3.kde.org/ "The Kid3 audio tagger").

    $ find . -name "*wav" -exec lame --preset cbr 320 "{}" \;

Most of the tracks are classical, and sound rather compressed. But they have that early 1990s electronic synth quality to them I love, *almost* as though they were produced with an OPL chip.

I love having this disc in my local music library! It's very nostalgic and silly, which is absolutely my current energy.
