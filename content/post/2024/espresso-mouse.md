---
title: "Espresso mouse"
date: "2024-10-31T14:31:24+11:00"
thumb: "https://rubenerd.com/files/2024/espresso-mouse@1x.jpg"
year: "2024"
category: Hardware
tag:
- coffee
- mouses
location: Sydney
---
<figure><p><img src="https://rubenerd.com/files/2024/espresso-mouse@1x.jpg" alt="Photo of a new espresso next to my new Logitech mouse" srcset="https://rubenerd.com/files/2024/espresso-mouse@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

Thank you, that is all :).
