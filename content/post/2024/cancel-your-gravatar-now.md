---
title: "Cancel your Gravatar, now!"
date: "2024-08-04T10:57:41+10:00"
year: "2024"
category: Internet
tag:
- blockchain
- environment
- scams
location: Sydney
---
Gravatar announced some ["decentralised" identidy management](https://web.archive.org/web/20240801005212/https://blog.gravatar.com/2024/07/30/decentralized-identity-management/), and it's as grim as you think it is:

> Imagine having complete control over your personal identity information without relying on a centralized authority. Well, that’s absolutely possible with decentralized identity management – an innovative system that allows individuals to own, control, and manage their identity data.

> So, how does it work?

Something something AI?

> Decentralized identity management uses cutting-edge technologies like blockchain

Oh, we're back to boiling the planet the old-fashioned way! Talk about being a [day late and a dollar short](https://mastodon.gamedev.place/@beeoproblem/112877404703682428).

> Blockchain technology is fundamental to decentralized identity systems

No it isn't. *Fundamental* means it can't exist without it. Why are they so intent on lying?

> Ethereum’s smart contract functionality lets you create complex identity management systems that operate autonomously and transparently.

Advocating for this tech in 2024 should be grounds for immediate dismissal. I don't know how you could possibly see the years of evidence against this stuff and *still* think it's a good idea. Either this is [further](https://www.404media.co/tumblr-and-wordpress-to-sell-users-data-to-train-ai-tools/ "404 Media: Tumblr and WordPress to Sell Users’ Data to Train AI Tools") proof Automattic have lost the plot, or they're being pushed to spruik scams by a third party. I don't care either way, to be honest.

To disable your Gravatar:

1. Log into Gravatar.com

2. Click *Avatars* from the left sidebar. Hover each of your avatar images to get the "..." menu, and choose *Delete*. I’m not confident this deletes them either, but I’ll bet you have a better shot getting rid of them than having them GC’d after your account is gone.

3. Click your email address in the top-right, and choose *Disable My Gravatar*.

I will say, at least it was a bit of a blast from the past seeing some of these old avatars. I remember when I used to have lots of hair; it was great.

<figure><p><img src="https://rubenerd.com/files/2024/gravatars@1x.jpg" alt="My various gravatars." srcset="https://rubenerd.com/files/2024/gravatars@2x.jpg 2x" style="width:334px; height:220px;" /></p></figure>
