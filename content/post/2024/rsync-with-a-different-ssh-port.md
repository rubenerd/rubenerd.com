---
title: "rsync with a different SSH port"
date: "2024-07-23T08:12:11+10:00"
year: "2024"
category: Software
tag:
- things-you-already-know-unless-you-dont
- rsync
location: Sydney
---
In today's installment of [things you already know, unless you don't](https://rubenerd.com/tag/things-you-already-know-unless-you-dont/), you can specify a custom SSH port with `rsync(1)` using:

    rsync -e 'ssh -p $PORT' $SOURCE $TARGET

This is handy for one-off transfers where the host doesn't warrant an inclusion in your `~/.ssh/config` file.

And no, before anyone asks, I definitely didn't just create this post to remind myself how to do this. That'd be silly. I also definitely didn't put it on the one-liners card on the [Omake](https://rubenerd.com/omake.xml) either.
