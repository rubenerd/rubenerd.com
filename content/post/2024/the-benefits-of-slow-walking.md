---
title: "The benefits of slow walking"
date: "2024-10-27T20:57:52+11:00"
year: "2024"
category: Thoughts
tag:
- mental-health
- hiking
- walking
location: Sydney
---
I had to get my ten thousand steps in today, but I'm coming off a bad cold and was feeling melancholic, weak, and lethargic. So I started walking in one direction down the street, at a steady and gentle pace. Walking outdoors does wonders for mental health, and I swear it helps with ailments too.

I didn't listen to a podcast or any music; I put the personal phone in my pocket, and left the work beeper and headphones on my desk at home.

I was walking, watching where I was going, but otherwise not really paying attention to anything.

There were so many chirping Australian birds!

As I approached a few intersections, I'd see the green man come on indicating I could cross. Rather than run to make the crossing in time, I kept the same slow walking pace I had been. I arrived at the crossings just in time for them to go red, so I'd stand there and wait. I wasn't impatient or frustrated, it just felt like something that was happening. I had no agency over the situation, nor could I impact it any way, so I just let it play out. I felt *zero* impatience.

I got home after an hour, and it was probably the best I've felt in weeks.

I'm always rushing somewhere. To work, to the shops, to the train, to nowhere in particular. I slow my gate somewhat when I'm with Clara (on account of her much shorter legs!), but we still power walk. It's probably good for exercise, and does get us where we're going quickly.

*(A crush I had as a kid once joked once they'd tried to run after me to say hi back in the day, but they couldn't catch up! There's a metaphor in there somewhere).*

I'm a transport nerd, so to me it felt like the difference between taking a commercial flight, and a cruise… of sorts. I wouldn't say I reached any level of zen, nor would I pretend I was meditating, achieving mindfulness, or being “at one” with my surroundings. I just found the act of deliberately walking slowly to be one of the nicest things I've done for myself in a long time. I was letting the proverbial idle mental CPU cycles be just that. I suspect this was easier on account of my head being full of cotton wool with whatever cold this is I have, but the outcome was still pleasant and calming in a way that's profoundly difficult to describe.

Find a park if you can, and give some slow walking a try. Recommended :).

