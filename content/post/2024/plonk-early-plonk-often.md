---
title: "Plonk early, plonk often"
date: "2024-11-10T09:50:01+11:00"
year: "2024"
category: Thoughts
tag:
- feedback
- social-media
location: Sydney
---
I've struggled mentally with the idea of blocking people. I internalised the oft-repeated idea that I was being closed minded each time I reached for the big red button, and that I could be the "bigger person" somehow (whatever that means).

I got around this before by claiming I blocked people for mental health. I'd say I *wanted* to help and change the mind of a cryptobro or genAI  slopper who's pinged me, but that it was exhausting. That's only partly true; I also blocked them because such topics self-select for unpleasant people who have no intention of engaging in an honest discussion. They're (mas)*debaters*; the kind of people who say they *destroy* arguments for Internet points. I could wrestle with them in the mud, but only they would enjoy it.

I've now given up the pretence of mental health. I block *defensively*, and encourage others to do so too. Our finite time and attention are 
worth more, and are better served.

As one example, after writing 9,000+ posts here over two decades, I started being noticed by sites like HN, and getting more inbound links from social media. People were sharing and discussing what I'd written, and not always positively. But&mdash;and I cannot overstate this&mdash;a shocking number of people don't understand the difference between constructive feedback, and being an arsehole (this even assumes feedback is constructive too, when plenty isn't). Once you've explained the basics of being a human a dozen times in a morning, you rapidly lose interest. *Plonk.*

Recently jwz linked to his 2021 post about [blocking people](https://www.jwz.org/blog/2022/01/on-blocking-and-the-coinsplaining-cryptobros/), which I now think is the most important thing he's ever written. He talks about the defensive block, in which you block someone in advance if you see them saying something ridiculous. He's also since addressed concerns about [living in an echo chamber](https://www.jwz.org/blog/2024/11/echo-chamber/). Turns out you can have differing opinions of food in a restaurant without requiring an open sewer alongside for balance.

But I think [his recent post about the DNA Lounge](https://www.dnalounge.com/backstage/log/2024/11/06.html) said it best:

> One of the things that I can do is to keep fighting to provide a welcoming place for our various communities to gather and feel safe. 

Your posts are either in the service of making the world better, or they're not. If you're in the latter camp, you get a *plonk*.
