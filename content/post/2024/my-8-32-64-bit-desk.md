---
title: "My 8, 32, and 64-bit desk!"
date: "2024-05-27T08:12:18+10:00"
thumb: "https://rubenerd.com/files/2024/desk2024@1x.jpg"
year: "2024"
category: Hardware
tag:
- apple-ii
- dell-dimension-4100
- retrocomputing
location: Sydney
---
Clara and I are looking at moving soon, so we'd let the apartment slide a bit of late. Which makes no sense; why wouldn't you want to live somewhere nice in the interim? After more than a weekend of shuffling cables and tables, I have my three desktops set up within reach!

The 8-bit Apple and Commodore machines are connected via an upscaler which I can toggle, and the 32-bit PCs and SPARCStation are on the KVM under the desk. The main monitor is for the FreeBSD/Alpine Linux tower, and the MacBook Air in clamshell mode on a different input. Clara has space behind the large monitor for her desktop and Wacom setup.

<figure><p><img src="https://rubenerd.com/files/2024/desk2024@1x.jpg" alt="Photo showing the desktop layout with the aforementioned computers arranged in a way that's accessible." srcset="https://rubenerd.com/files/2024/desk2024@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

Our hope is our apartment we hope to buy will have two bedrooms, or potentially three if we can swing it. In the meantime, our coffee table doubles as a dining table, and Clara and I have everything on these two other "desks". It's a squeeze, but it'll do for now.

Honestly, this is all I ever wanted as a kid. While we chase home ownership, work, and all those other life obligations, it's worth remembering. As I type this, *The Oregon Trail* is loading on the Apple //e (my American friends insist I try it), *Train Simulator* is installing on the Pentium III, Clara is playing that unpacking game opposite, and I'm writing this post.

This year has been hard again, but living in this moment, right now, it doesn't seem real. Someone pinch me.
