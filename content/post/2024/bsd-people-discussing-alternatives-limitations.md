---
title: "BSD people discussing alternatives, limitations"
date: "2024-08-11T09:24:29+10:00"
year: "2024"
category: Software
tag:
- bsd
- freebsd
- netbsd
location: Sydney
---
I haven't formally quantified this, so don't take my anecdotal experience here at face value. Here comes the proverbial posterior prognostication: *but...*, I do tend to find BSD people to be more candid and transparent when it comes to limitations with specific platforms and tools, going as far as to offer alternatives when they don't think something will fit.

When you deal with commercial vendors and large Linux distros for most of your *Day Job*, it's refreshing.
