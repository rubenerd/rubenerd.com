---
title: "An assertiveness tip: state the outcome"
date: "2024-05-17T08:37:40+10:00"
year: "2024"
category: Thoughts
tag:
- pointless-anime-reference
- sociology
location: Sydney
---
Being assertive doesn't come naturally to me iRL. I've been getting better at recently, but I still let people walk over me more than they should in social settings.

Some of the best advice I've received mirrors what this advice columnist wrote [with regards to a wedding](https://www.theguardian.com/lifeandstyle/article/2024/may/17/im-getting-married-but-my-father-isnt-invited-how-can-i-ensure-he-doesnt-crash-the-wedding), emphasis added:

> The most you can do to try to influence their behaviour is to be clear about outcomes. “If he comes, he won’t be let in”; “If you don’t come, I’ll be very sad that he was able to keep you away.” Not “here’s why”, not “please see my side of things”. **Just: “If this happens, this is what I will do.”**

You risk being obstinate yourself if you're immovable in all contexts, especially where the person you're talking with has no control over the situation. Nobody is helped by you walking into a bank and saying "I'm not leaving here until you give me a dakimakura of Mashu from Fate/Grand Order". That example might be slightly too specific, though she does look assertive here.

<figure><p><img src="https://rubenerd.com/files/2024/mashu-assertive@1x.jpg" alt="Mashu and Fou" srcset="https://rubenerd.com/files/2024/mashu-assertive@2x.jpg 2x" style="width:335px; height:475px;" /></p></figure>

*But I digress*. Sometimes a discussion or situation warrants a line in the sand like this. Like an extended family member who's being unreasonable. Ask me how I know! Though I'll bet you know what it's like too.

You don't need to be aggressive or rude. State clearly, and unambiguously, what the outcome will be if a negative course of action is followed. It clears the air, and steers the conversation in a productive direction. 

