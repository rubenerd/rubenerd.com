---
title: "A return to computing innocence"
date: "2024-07-02T11:26:13+10:00"
year: "2024"
category: Software
tag:
- cpm
- dos
- retrocomputing
location: Sydney
---
[Emil on Mastodon](https://helo.fuse.pl/@emil/statuses/01J1QG7CC843JN2YDKNN3FW445) forwarded me an article by Adrian Kosmaczewski in *De Programmatica Ipsum*, titled [A Requiem For DOS](https://deprogrammaticaipsum.com/a-requiem-for-dos/). *Dziękuję!*

This passage especially hit me like a proverbial megagramme of 8-bit bricks, in light of what I've been discussing here of late:

> There is, nowadays, a collective bout of nostalgia happening, this article notwithstanding. I have already talked in a previous issue of this magazine about the wonders that an old DOS system, coupled with a rusty copy of WordStar, did to the creativity of George R.R. Martin. Imagine this: just you and your words, in a laconic white-over-black screen, without any distractions (because there cannot be any, as a matter of fact.) The length and breadth of his creation have now an easy explanation. I consider this return to innocence a required vacation for our brains, a way to escape an ever-growing complexity, or simply a mechanism to appreciate the goodness of our current computing world.

I've been wrestling with the idea that there's more going on with my interest in retrocomputing than mere nostalgia, or even a fascination with electronic history. It it escapism? A way to rekindle that joy I had as a kid tinkering with computers, having had some of wrung out and extinguished? Is it a sense of understanding at a more fundamental level how something works? Is it, like sci-fi, a critique of the present?

Last year I wrote about using [classic computers for distraction-free writing](https://rubenerd.com/using-an-old-computer-for-new-writing/ "Using an old computer for new writing"), ironically in a way Emil points out is lucky given that my language only uses a subset of Latin which these machine support.

But I can't shake the feeling there's more to it. That it's a reckoning somehow. I think Adrian has come the closest to explaining what that is.
