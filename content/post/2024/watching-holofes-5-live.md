---
title: "Watching Capture the Moment, Holofes 5"
date: "2024-03-18T08:35:15+11:00"
year: "2024"
category: Anime
tag:
- ceres-fauna
- hololive
- music
- pavolia-reine
- hoshimachi-suisei
- watson-amelia
location: Sydney
---
Today's *[Music Monday](https://rubenerd.com/tag/music-monday/)* is all about the latest Hololive vtuber concert *Capture the Moment*.

I'll admit, it was bittersweet. Clara and I had won tickets in the limited lottery, which we were going to head to Japan for, then head over to Taipei for AsiaBSDCon. But bad life things happened, and we had to stay put.

<figure><p><img src="https://rubenerd.com/files/2024/holofes5@1x.jpg" alt="Montage of Reine, Fauna, and Watson" srcset="https://rubenerd.com/files/2024/holofes5@2x.jpg 2x" style="width:500px; height:320px;" /></p></figure>

That's not to say it wasn't fun watching again. My Hololive Oshi [Pavolia Reine](https://www.youtube.com/channel/UChgTyjG-pdNvxxhdsXfHQ5Q) from Hololive Indonesia owned the stage again with confidence and flair, which was such a relief to see after her recent fan troubles. My favourite English vtuber [Ceres Fauna](https://www.youtube.com/channel/UCO_aKKYxn4tvrqPjcTzZ6EQ) was as chill and beautiful as last year with her downbeat citypop-style performance; there's a reason I watch her Minecraft streams when stressing at work. And [Amelia Watson](https://www.youtube.com/channel/UCyl1z3jo3XHR1riLFKG5UAg), Clara's and my first vtuber sang Japanese with such confidence, but with a bit of that cheeky detective still showing through. *Little hats!* 💙💚💛

On the Japanese side, Suisei performed a medley with so many pitch and speed changes it was hard to keep up! She's the original diva of Hololive; I'd go see her live even if she wasn't in her vtuber persona. Subaru, Mio, and Polka doing the high-energy opening to the 2007 anime *Toradora* was also, to use the technical language, nostalgic as absolute fuck.

You pay for access, which we streamed to the TV; hence the quality of the "screenshots". Hopefully when we finally make it back over there we can get the Blu-Ray.
