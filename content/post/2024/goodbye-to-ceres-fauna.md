---
title: "Goodbye to Ceres Fauna 🌿 💚"
date: "2024-12-03T08:29:54+11:00"
abstract: "Ceres Fauna announced her graduation. I’ll miss her dearly."
year: "2024"
category: Anime
tag:
- bossa-nova
- ceres-fauna
- hololive
- music
location: Sydney
---
It hasn't been a great year for Hololive fans. The trailblazing [Amelia Watson graduated](https://rubenerd.com/goodbye-amelia-watson/) in all but name back in September, with Sakamata Chloe following suit as an "affiliate". Minato Aqua left permanently in August, and Yozora Mel was dismissed. Now Ceres Fauna of Council/Promise has announced she's leaving as well, citing a disagreement with management.

It's hard not to worry there's something wider going on at Cover Corp right now, whether it's the pressure from going public or something else. People in the real world leave jobs all the time to pursue new opportunities and to reignite that creative spark, but the number of departures raises eyebrows. Above all else, I hope all the talents are being treated well, and those who chose to leave did so of their own volition.

Which leads us to Fauna. It's hard to overstate just how much she did for Clara and I over the last few years. When Council [burst onto the scene in 2021](https://rubenerd.com/ceres-faunas-debut-stream/ "Ceres Fauna’s debut stream #faunline"), Fauna and Kronii became my instant favourites. Her calm, yet cheeky demeanour, beautiful voice, and creativity were balms to a troubled soul.

<figure><p><img src="https://rubenerd.com/files/2024/ceres-fauna@2x.png" alt="Ceres Fauna with her 3D debut design." srcset="https://rubenerd.com/files/2024/ceres-fauna@2x.png 2x" style="width:384px; height:627px;" /></p></figure>

Fauna was my introduction to ASMR. She brought a chill vibe to everything from *Hitman* and *The Witcher* to *Euro Truck Simulator!* Her designs in Minecraft were nothing short of breathtaking, from her cute [Hobbiton-inspired home](https://rubenerd.com/the-ceresfauna-built-a-minecraft-kitchen/ "The @ceresfauna built a Minecraft kitchen") to the utterly ridiculous, multi-year World Tree build on *Minecraft Mondays*. The way she'd inhale sharply and mutter *ooooh!* when confronted with something difficult has entered Clara's and my family folklore!

Fauna had an effortless, natural rapport with everyone she collaborated with, especially with Pavolia Reine of Hololive Indonesia with whom she shared those hats. The [3D holiday stream in 2021](https://rubenerd.com/hololive-englishs-second-holiday-stream/ "Hololive English’s second holiday stream"), and her beautiful bossa nova performance during the Hololive *[Our Bright Parade](https://rubenerd.com/hololive-our-bright-parade/)* have yet to be beaten in my eyes:

> Bossa nova has been my favourite music since first hearing Jobim, so hearing her singing her Let Me Stay Here original among the falling leaves in lieu of flashing lights felt like worlds colliding in the comfiest way possible. It was fun looking out at the sea of green glowsticks swaying as if in a gentle breeze. I could have listened all day. 🌿

She will be gone, but not forgotten. Here's hoping she can turn a new (Lemon)Leaf on her own terms again. Pretend you didn't read that, Dooby3D.

<figure><p><img src="https://rubenerd.com/files/2024/sapling-watson@1x.jpg" alt="A Fauna sapling next to our Amelia Watson pillow" srcset="https://rubenerd.com/files/2024/sapling-watson@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>
