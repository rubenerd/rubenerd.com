---
title: "Xmas 2024 season officially starting"
date: "2024-10-26T07:56:37+11:00"
thumb: "https://rubenerd.com/files/2024/xmas2024prep-01@1x.jpg"
year: "2024"
category: Thoughts
tag:
- holidays
location: Sydney
---
<figure><p><img src="https://rubenerd.com/files/2024/xmas2024prep-01@1x.jpg" alt="Photo showing undecorated trees in a shopping centre hallway." srcset="https://rubenerd.com/files/2024/xmas2024prep-01@2x.jpg 2x" style="width:500px; height:375px;" /><br /><img src="https://rubenerd.com/files/2024/xmas2024prep-02@1x.jpg" alt="View of a shopping centre atrium with an Xmas village under construction" srcset="https://rubenerd.com/files/2024/xmas2024prep-02@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>
