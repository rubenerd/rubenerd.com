---
title: "Crowd dynamics at coffee shops"
date: "2024-02-25T10:03:16+11:00"
year: "2024"
category: Thoughts
tag:
- business
- coffee
location: Sydney
---
Coffee shops are an interesting study in crowd dynamics. Either that, or it's a place where I've had coffee, and it causes me to notice silly things.

I'll walk past a coffee shop at 09:00 on a Saturday, and it's so packed there's a queue of people waiting, only for it to be empty when I walk past an hour later. On Sunday at the same time, there's an old man sitting in the corner shouting at the young women walking past for the length of their skirts, and that's it.

*(Also, you're in Chatswood mate. If you don't like East Asian fashion in Sydney, maybe Bondi would be a better fit for you. Although, that has a beach, so you'd likely be offended there too. I know I am; I can't stand sand. It gets in everything).*

Another weird phenomena is how people arrive and leave in waves. I've been at this coffee shop for an hour, and it's gone from having empty tables, to being so full that one almost can't think over the din, to being almost empty again. Damn it, the creepy guy is still there. Maybe he'll get the message if I raise my eyebrows at him a few more times.

I don't even notice the coffee shop is filling up or emptying out until after the fact too, which is weird. It's not like a gradient, it goes FULL, then EMPTY. Clearly coffee helps with concentration and distractions, or the Matrix's random-number generator is glitching again.

But it gets stranger. These waves of people aren't predictable; or at least, in the timescales I'm working from. The time of day, day of the week, holidays, specials, number of people already there, Wi-Fi signal, none of it correlates whatsoever with the number of people choosing to sit here. Or does it, but I haven't cracked it yet?

This is a idle curiosity for me, though I'm sure retail and hospitality staff would love nothing more than to understand it so they can tailor and optimise their businesses. What was it John Wanamaker was credited as saying? *Half the money I spend on advertising is wasted; the trouble is I don’t know which half.*

