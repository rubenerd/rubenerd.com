---
title: "We’ve had high fidelity music for a while now"
date: "2024-06-22T23:46:08+11:00"
year: "2024"
category: Media
tag:
- music
- nostalgia
location: Sydney
---
Riding off the back of my (controversial, le sigh!) post earlier this year about the [compact audio cassette](https://rubenerd.com/is-it-worth-collecting-cassettes-in-2024/ "Is it worth collecting cassettes in 2024?"), today I thought I'd talk about a broader observation I've made recently.

I had a few *intense* interests as a kid that weren't related to computers or electronics, many of which had to do with the early 20th century. I relished reading books and watching documentaries about art deco design and architecture, ocean liners, and the music of the Roaring Twenties. It was an era of unbridled experimentation and optimism (though we also know how it turned out). 

These documentaries would&mdash;and still do&mdash;set the scene with flickery black and white video, played to that unmistakable swing and big band music from the time. Think brass sections and flappers topped with a blast from a liner's horn as it pulls into New York.

But it wasn't just the music itself, it was how it sounds in a modern context. Having been recorded on early analogue equipment with much lower fidelity than what we have today, it sounds tinny, compressed, and... *unmistakable*. You can't help but be transported to that period of time when you hear it, regardless of whether its jazz, classical, or a radio programme. 🎺

It's taken on such an identity of its own, artists behind modern shows and music set in that time will attempt to compress their audio in the same way. It *doesn't sound right* otherwise.

This is a fascinating example of where technology, culture, and society converge. A strictly technical constraint is not only used to date something, but is **integral to the experience**.

It's funny to think about how people at the time were hearing music live the same way we do now. It's the same as kids thinking the world was monochrome before the advent of colour photography (... or maybe that was just me).

People had a similar experience in the home environment. Audiophiles, and those cosplaying as one like me, wax lyrical about the warmer, richer sound that LPs have and develop over time. This is another *feature not a bug* that has become a part of the experience, and is why FLAC people arguing about audio quality don't ever seem to get through to analogue fans. We know the quality isn't as good; that's part of the whole point.

Our Sony DVD/CD changer supports MP3 CDs, so I put an old CD-R on that I'd cobbled together in the late 1990s. It instantly teleported me to MSN Messenger, Beanie Babies, Visual Basic 5.0, and Sailor Moon. The sound quality was muffled, but it kinda had to be. Because *that's what it sounded like*.

But this is an interesting inflexion point for another reason. Some time in the 1990s, digital mastering and distribution got good enough that a CD from that time sounds as good as one released today, at least technically. Granted songs are EQ'd louder now than they were before, and more people are listing over compromised streaming services, but it's possible for a layperson to get a track from 1999 and have it sound as good as something that came out in 2023. Shows like *Cold Case* now date time periods based on what the song *is*, not what it sounds like. That's a huge shift.

It makes me wonder if/when we'll get to that point with video.
