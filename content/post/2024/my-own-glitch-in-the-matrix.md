---
title: "My own silly intercom glitch in the Matrix"
date: "2024-05-07T08:18:00+10:00"
year: "2024"
category: Hardware
tag:
- glitch-in-the-matrix
- pointless
location: Sydney
---
I love the phrase *glitch in the Matrix*. It describes the phenomena of observing something that didn't quite generate correctly in the vast simulation in which we find ourselves. It's all a bit of fun, though perhaps slightly more interesting if we're to believe [people like Nick Bostrom](https://academic.oup.com/pq/article-abstract/53/211/243/1610975 "Are We Living in a Computer Simulation?").

I've had the experience a few times recently, all of which occurred in our own apartment block. Whoever implemented this apartment block in Blender either didn't do their homework, or didn't export it properly!

Every apartment in the building recently had their intercoms replaced. The previous ones used these cute little projectors to shine a black and white image onto a large, concave piece of plastic. The newer ones use regular LCDs, which funnily enough are less clear and bright than the units they replaced. I suspect they swapped them out when they started failing *en masse*, and replacement parts were becoming expensive and difficult to source.

Last Thursday I saw a stack of Panasonic intercom boxes stacked up in an infrequently-used hallway. I took a peek in a couple of the top boxes, and sure enough, the old projection units were carefully packed back inside them, complete with the original styrofoam inserts. They were *incredibly* musty in that specific way old electronics and aged plastic smell, but I was still tempted to ask reception if I could take one to experiment with!

Over the weekend, I was sad to see the boxes were all gone. *Oh well* I thought, no tinkering. We're about to move anyway, so we don't need the extra junk.

But today, on the way back from my morning coffee&mdash;I know you'll find me doing that hard to believe&mdash;the boxes had all returned, in exactly the same position. And I haven't been able to stop thinking about it.

Let's tackle the boxes first. Who keeps the original boxes for *hundreds* of intercoms for an entire building, presumably installed when the complex was built in the 1990s? Where would they have kept them for nearly three decades? The styrofoam inserts would have prevented the boxes being collapsed to store them in less space, so they would have had to be stored as-is.

Then there's the packing. Why would you take the time to put all these old units back into their original boxes, hundreds of times? Surely they were long out of warranty and financially near worthless, and many of them were broken or otherwise non-functional, hence why they were replaced. If they're going to ewaste, or even harvested for parts, why would you bother repacking them like this? Even if you were doing it out of boredom, or because a clueless manager told you to, surely doing this drastically increases their total volume for later transport.

But even *those* weren't my primary concerns. No, the biggest issue was the fact the boxes were restacked *identically* to how they were before. Okay, maybe not *identically*, but damned near close enough that I couldn't tell the difference.

One reason I chose that specific box to look into was its position. It was on the top left corner, and rotated 90 degrees from the other boxes in that layer. This allowed me to access the top flap and pull it down easily, revealing the contents. I also remember it being a bit water damaged. That box was in the same position again.

Other standouts for me were the fact some boxes must have slid off the pile, because they were lying on the ground on their sides. All these boxes were now in the same positions again, on the ground, on their sides.

Perhaps most surprising was a streak of water damage I remember that covered some of the boxes. In the reassembled pile, this damage lined up *perfectly*, as though they'd been restacked with an eye to achieving this.

I've worked in a factory before, and am aware of the existence of forklifts and pallets that can lift, shift, and move large quantities of stuff provided they're secured correctly. I couldn't see any pallet, tray, or even a simple sheet of cardboard underneath these boxes. The volume of them combined would have been far too heavy for one person to lift, and too unwieldy and precarious for several people to lift without them all tumbling down.

Somehow, these stack of intercoms were haphazardly stacked in a pile, then moved, then moved back *exactly* the same way. Again, to the point where ones that fell down fell in the same places, in the same way, and water damage was kept consistent in their positions.

**HOW!?**

It's most likely the pile didn't move, and that I must have misremembered where they were stacked the second time I went down that hallway. It's a *narrow* hallway though, and I was practically stepping over some of them to pass. Maybe I just had tunnel vision, but it seems like a huge and obvious thing to miss, to the point where I was actively disappointed they were gone.

OR! And here me out here. There was a *glitch in the Matrix*. Someone, either by mistake or to mess with me, placed those boxes into the apartment building model, removed them, then put them back. Maybe their asset folder didn't have the right permissions, or the model itself is mildly corrupted. The use of a model also explains why they were reboxed, because it's easy to just redo that a few hundred times in a loop than it would be to do it by hand.

I'm glad to have cleared that up. Maybe. Pity I can't call the designers on one of those intercoms.
