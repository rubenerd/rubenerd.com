---
title: "Paul Reinheimer on security docs"
date: "2024-11-29T14:06:26+11:00"
abstract: "The more I learn about cryptography, the more I think Alice and Bob should probably just talk in person."
year: "2024"
category: Software
tag:
- documentation
- security
- silly
location: Sydney
---
[Via The Mastodons](https://phpc.social/@preinheimer/113511397292118016)\:

> The more I learn about cryptography, the more I think Alice and Bob should probably just talk in person.
