---
title: "Ignoring email for a bit"
date: "2024-04-22T07:09:45+10:00"
year: "2024"
category: Internet
tag:
- comments
location: Sydney
---
The fallout from suggesting to (certain) Linux fans that [some still need to run Windows](https://rubenerd.com/people-who-need-to-run-windows/ "A thread about people who need to run Windows") continues. I'm ignoring social media for a bit, but I'm still getting regular angry email.

As I said in my [follow-up](https://rubenerd.com/lessons-from-my-linux-fun-on-mastodon/ "Lessons from my Linux fun on Mastodon"), I'm done. My apologies to those of you I've yet to reply to, but I'm going to be ignoring my public email for a bit.

Thanks to [Kevin](https://kevquirk.com/linux-elitism-again) and [Roy](http://news.tuxmachines.org/n/2024/04/15/Mastodon_Tale.shtml) for their posts about this.

