---
title: "Some Olympic events I’d win"
date: "2024-08-13T18:32:37+10:00"
year: "2024"
category: Thoughts
tag:
- pointless
- sport
location: Sydney
---
<dl>

<dt style="font-weight:bold">👟 The 100 metre heart race</dt>
<dd><p>It's a race track, but the sidelines are filled with <a href="https://bsd.network/@rubenerd/112925268055292702">crying infants</a>, unscheduled client meetings, and endless clutter! Watch that anxiety heart rate soar!</p></dd>

<dt style="font-weight:bold">🍃 Competitive sneezing</dt>
<dd><p>My current independently-verified record is 29 consecutive sneezes. I reckon I could beat that, and everyone else.</p></dd>

<dt style="font-weight:bold">🧇 Waffle</dt>
<dd><p>The event in which competitors sit around a table, and attempt to fill the air with incessant, haughty banter that manage to fill hours of time, without saying anything of substance, purpose, or meaning whatsoever. It reminds me of the time where&hellip;</p></dd>

<dt style="font-weight:bold">💥 The faceplanting relay</dt>
<dd><p>Clara and I have the uncanny ability to trip and fall on the same thing, in the same way, while holding hands. It's hilarious and tragic.</p></dd>

<dt style="font-weight:bold">🤺 Package manager fencing</dt>
<dd><p>You need to upgrade to the latest Stable PHP on... Debian! No, now it's... FreeBSD! <em>Hai... yah!</em> What's the syntax!? What do all the systems call it!? Okay then, bootstrap pkgsrc! <em>On guard!</em></p></dd>

<dt style="font-weight:bold">🩳 Synchronised swimming&hellip; style</dt>
<dd><p>Clara and I would look <em>awesome</em> in matching suits by the pool. Okay, maybe not me. And we wouldn't be judged on athletic prowess. Wait, how does this work?</p></dd>

</dl>
