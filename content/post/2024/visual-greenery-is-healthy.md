---
title: "Visual greenery is healthy"
date: "2024-07-05T08:04:37+10:00"
year: "2024"
category: Thoughts
tag:
- environment
- health
location: Sydney
---
I've long known personally that I feel better when house plants and outdoor greenery are in sight, to the point where I'll rearrange entire rooms to have my desk and/or chair pointing in the direction of it, and will keep a little planter on the balcony table when I'm outside having my morning coffee. 🌿

But it's always good to have validaton, as [this study published](https://www.nature.com/articles/s44220-024-00227-z "Long-term exposure to residential greenness and decreased risk of depression and anxiety Nature Mental Health") this year in Nature Mental Health investigated (paywalled):

> This national study highlights that long-term exposure to residential greenness was linked to a decreased risk of incident depression and anxiety. Reduced air pollution was a significant mediator linking green environments to depression and anxiety.

This is also another way reducing car depenency would be beneficial to urban environments. With less space dedicated to cars, there could be more road-side greenery which helps everything from mental health, to reducing the urban heat-island effect, to reducing localised pollution.

Singapore was always great at urban greenery. Australian coastal cities are generally pretty good, though some suburbs do it better than others.
