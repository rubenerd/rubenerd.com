---
title: "Thinking aloud about companies in the Fediverse"
date: "2024-03-23T19:59:48+11:00"
year: "2024"
category: Thoughts
tag:
- mastodon
- open-web
- social-networks
- standards
location: Sydney
---
Facebook's Twitter clone Threads has joined the Fediverse; a sentence so dense with random words it barely makes sense even to me. I almost envy those of you who just read French.

More specifically, Threads has implemented [ActivityPub](https://www.w3.org/TR/activitypub/), the W3C's decentralised social network protocol. This means people giving Zuck even more time can view, reply to, and share posts from other networks that implement ActivityPub, such as [Micro.blog](https://micro.blog/), [PeerTube](https://peertube.tv/), [Pixelfed](https://pixelfed.org/), and [Mastodon](https://joinmastodon.org/). Hey, more random words!

This has currently caused a cry of considerable cantankerous consternation, especially among those on Mastodon I respect. Some of those words were unfair, but I loved the alliteration. But I'm not so sure.

First, we have to understand the context and environment in which this is taking place. Mastodon was first created in 2016, but really took off after some *[kiasu](https://en.wikipedia.org/wiki/Kiasu "Wikipedia article on Kiasus")* took over Twitter in 2022, the only social network I ever grokked. I maintain my ancient account for my few remaining friends there, and to follow Hololive vtubers like my beloved [Pavolia Reine](https://twitter.com/pavoliareine) and [Ceres Fauna](https://twitter.com/ceresfauna)... Cover corp, please start your own Mastodon instance! But I made the switch to Mastodon for most of my daily microblogging, and it's fantastic. Superficially, it's as though I'm using Alternate Reality Twitter, where the platform continued to evolve rather than *[enshittify](https://en.wikipedia.org/wiki/Enshittification "Copy Doctorow's term on Wikipedia")*.

But there's more to it than that. The Mastodon developers are non-profit, and Mastodon itself represents a fundamentally different operating model for social networks. Anyone can host an ActivityPub-compatible site; [my Mastodon account](https://bsd.network/@Rubenerd) is on the [BSD Network](https://bsd.network/), run tirelessly by a few lovely people in the OpenBSD space. The common protocol means we can all communicate across disparate sites and servers. It's beautiful, and gets us back to the architecture the Web was supposed to have from the start. Open protocols, distributed servers, accessible software, and people having a choice where they want to go.

<figure><p><img src="https://rubenerd.com/files/2024/chase-floor@1x.jpg" alt="A view inside a shopping centre atrium with a new floor level." srcset="https://rubenerd.com/files/2024/chase-floor@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

*I wanted this photo I took of some shopping centre renevations in Sydney to be a metaphor for "building back from the ground up"... but it didn't really work. I put the effort into uploading it though, so let's pretend it's profound.*

It's this sense of camaraderie and hard-won independence from commercial interests that my friends now feel is under threat by Facebook injecting Threads into the same space. Facebook is the poster child of walled off social networks; they made their fortune performing all manner of shady things with people's data, some of which got them in [legal trouble](https://www.bbc.com/news/technology-54722362 "BBC: Facebook sued over Cambridge Analytica data scandal"). I argue that if TikTok is to get the chop, they should be the next in line if one is being ethically and legally consistent.

But being angry Threads has implemented ActivityPub is also to misunderstand how the open web works. Much like the open source licences we all use, we don't get to pick and choose which use cases we find distasteful, and which ones we like. Anyone is free to implement an open protocol, because it's an open protocol. As soon as we demand a site leave a network, we're no better than the walled garden pushing away a user who makes life difficult for an advertiser, or a CDN deleting a customer because they hosted legal material a payment processor didn't like.

*(That's not to say that individual servers don't have the right to moderate and de-federate from anyone they choose, including Threads. That's also the whole point of an open protocol. I could block whomever I wanted accessing my RSS feeds, but I can't tell Sky News not to publish RSS feeds).*

I'm also on the fence about what effect this will have long term. There are enough examples from history of *embrace, extend, extinguish* to be concerned that Threads have ulterior motives here. These commercial social networks also seem to attract the absolute dregs when it comes to spammers, trolls, and scammers. But I actually think the Mastodon CEO and Founder Eugen Rochko [put it best in July last year](https://blog.joinmastodon.org/2023/07/what-to-know-about-threads/)\:

> The fact that large platforms are adopting ActivityPub is not only validation of the movement towards decentralized social media, but a path forward for people locked into these platforms to switch to better providers. Which in turn, puts pressure on such platforms to provide better, less exploitative services. This is a clear victory for our cause, hopefully one of many to come.

I think the idea of Facebook using ActivityPub to make it easier to migrate off their service is a *bit* of wishful thinking, but it's not inconceivable that a Threads user today could become a Mastodon user tomorrow. We'll know Facebook are taking the position seriously when they support account migrations, though how that would be implemented is an open question.

I dunno, I agree with people worried about Threads here, and I'm more than likely to block people from it who ping me. But there's a small part of me who's relieved that we're even having this conversation in 2024. The 2010s were a dark period for the open web, and we're still in the throws of it in many ways. The fact a commercial entity even feels like they have to pay lip service to us is a breath of fresh air. Blue sky thinking, one could say. I'm not sorry.

Practically speaking, I also relishe the idea of chatting with some of my family and distant friends who are all over Instagram (and therefore Threads) whom I haven't talked with in years. Ironically, they may soon be more accessible to me on Mastodon than people on my seventeen-year old Twitter account.
