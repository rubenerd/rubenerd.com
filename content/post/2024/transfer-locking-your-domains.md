---
title: "Domain transfer locks for your domains"
date: "2024-10-27T11:56:22+11:00"
year: "2024"
category: Internet
tag:
- digital-hygeine
- security
location: Sydney
---
You know that feeling when you've inadvertently skirted something potentially dangerous? Dodging the proverbial bullet, as they say?

I was transferring the last of Clara's domains over to our shared registrar, when I noticed they didn't enable transfer locking by default. I then realised that *none* of our domains had it enabled. Some of them had been with this registrar for *years*.

What is it they say, the [cobbler's child walks barefoot](https://en.wiktionary.org/wiki/the_shoemaker%27s_children_go_barefoot)?

Suffice to say, is a phrase with three words. I've fixed that little personal *whoopsie doodle*, and took the time to remove a couple of old devices I'd left accumulate in the multi-factor auth screen. One of them was a phone I hadn't used since 2018. Cough.

If you have a spare few moments this weekend, and have some domains you maintain for yourself or family, maybe take a quick peek and make sure everything is configured correctly. Best to find out now, not when you get a transfer authorisation request.
