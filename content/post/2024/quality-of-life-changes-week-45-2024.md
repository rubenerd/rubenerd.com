---
title: "Quality of life changes, week 45 2024"
date: "2024-11-10T10:56:41+11:00"
year: "2024"
category: Internet
tag:
- weblog
location: Sydney
---
In no particular order:

* The serif fonts have been retired. It was fun, but they weren't as legible on non-HiDPI/Retina displays. You might need to clear your cache or press `SHIFT` when refreshing to see any change.

* I've added [Ninomae Ina'nis](https://www.youtube.com/@NinomaeInanis/streams) and [Ceres Fauna](https://www.youtube.com/@CeresFauna/streams) to my fan section on the sidebar. They're talented, comfy streamers who have done wonders for my anxiety over the last few years. Ina in particular reminds me a *lot* of Clara, which I'm sure is no coincidence.

* Hugo was finally updated to the latest version, and the themes fixed with their latest breaking changes to pagination. I *think* we're all good again, but ping me if you see anything silly.

* More HTML has been simplified, and redundant attributes removed. I'm still using [Dublin Core attributes](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/), because they do the job, and OpenGraph et.al are redundant.

* I offloaded the `dns-prefetch` elements to HTML headers.

* The [Omake](https://rubenerd.com/omake.xml), [Blogroll](https://rubenerd.com/blogroll.opml), and [Coffee](http://ruben.coffee/) pages were tweaked a bit. I'm still not happy with how the silly background images render on mobile, but at least they're more consistent.

