---
title: "Adrian Black’s Plexus P/20 UNIX machine"
date: "2024-07-19T07:59:55+10:00"
thumb: "https://rubenerd.com/files/2024/yt-lBprWU9cHXs@1x.jpg"
year: "2024"
category: Thoughts
tag:
- adrians-digital-basement
- retrocomputing
- unix
- video
location: Sydney
---
[Adrian's Digital Basement](https://adriansbasement.com/) is one of my favourite video channels. I got hooked on his early Commodore 64 and Apple II repair videos, the former of which were instrumental in getting my own Commodore hardware working. I consider him, [Jan Beta](https://janbeta.net/), and [Noel Llopis](https://noelsretrolab.com/) as my *Retrocomputing Treo* (heh), and often refer to their videos for instructions and advice as much as entertainment.

Recently Adrian went through the process of getting a Plexus P/20 working, an early-1980s UNIX tower with 8 RS232 interfaces. The motherboard and power supply needed servicing, though shockingly the MFM hard drive booted into UNIX! It was so much fun seeing his journey from "does this machine boot?" to "whoa, here are some commands I recognise!"

I was following along each week, but recenly he uploaded a supercut of all his Plexus P/20 videos. I can't recommend it highly enough to anyone interested in UNIX history.

<figure><p><a target="_BLANK" href="https://www.youtube.com/watch?v=lBprWU9cHXs" title="Play The Plexus P/20: A Fantastic Journey (Supercut)"><img src="https://rubenerd.com/files/2024/yt-lBprWU9cHXs@1x.jpg" srcset="https://rubenerd.com/files/2024/yt-lBprWU9cHXs@1x.jpg 1x, https://rubenerd.com/files/2024/yt-lBprWU9cHXs@2x.jpg 2x" alt="Play The Plexus P/20: A Fantastic Journey (Supercut)" style="width:500px;height:281px;" /></a></p></figure>
