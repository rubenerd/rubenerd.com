---
title: "Jan Beta is my YouTube channel of the year"
date: "2024-01-11T10:24:02+11:00"
thumb: "https://rubenerd.com/files/2024/janbeta@1x.jpg"
year: "2024"
category: Media
tag:
- jan-beta
- retrocomputing
- video
location: Sydney
---
I mention a *lot* of independent bloggers and videographers here, but in the paraphrased words of Highlander, *there can be only one… favourite for 2023*.

It was a close race, yet picking the winner took almost no time. My favourite YouTube channel for 2023 goes to [Jan Beta 📺](https://www.youtube.com/@JanBeta), everyone’s favourite German retrocomputator, restorer, and computer historian.

I'll start by saying Jan's enthusiasm and warm smile never cease to put one on mine. I’ll admit to having put one of his long-form repair videos (again!) just for a comforting voice while I do some of the more rote tasks of my job. The downside to this is I'm often distracted by the far more interesting things he’s doing, and have to tear myself away.

<figure><p><img src="https://rubenerd.com/files/2024/janbeta@1x.jpg" alt="" srcset="https://rubenerd.com/files/2024/janbeta@2x.jpg 2x" style="width:500px; height:281px;" /></p></figure>

Jan, Adrian Black, and Noel Llopis have also helped me in my own 8-bit electronic adventures. Through watching his videos over the last few years, he's given me the skills, knowledge, confidence, and advice to repair my own 8-bit machines, something I wouldn’t have dreamed of doing even a few years ago. My [Plus/4](http://www.sasara.moe/retro/#commodore-plus4) machine is operational today because of his repair videos on the C16; though it's my work-in-progress [Aldi 64](http://www.sasara.moe/retro/#aldi-commodore-64) that's earned the hostname *Jan!*

I’ve also learned of some awesome games, electronic music, [modern 8-bit hardware](https://rubenerd.com/jan-beta-reviews-the-foenix-f256k-retrocoputer/ "The Foenix F256k retrocomputer"), and diagnostic tools from seeing him test and play on various machines. As I said above, his enthusiasm is infectious, whether it be for an old computer, some Hi-Fi gear, a beautiful 1970s calculator, or all the eye-catching trinkets spread around his workshop.

Clara and I watch a lot of YouTube, but Jan’s videos are among the few we rush to watch as soon as a new episode appears. He has a way of making you feel like you're sitting in his workshop with him, like a friend. This is a skill precious few have.

[Jan has a Patreon](https://www.patreon.com/janbeta/posts) which I joined last year, which also gives you access to the discord server I lurk in (social anxiety sucks)! It’s a warm community of retro fans looking out for each other and sharing tips; exactly the kind of people you’d expect him to attract.

I know Jan has mentioned not having the best of times mentally over the last year, so I wanted to publicly say what a joy his channel has been, and to thank him for what he does. I didn’t learn German from my father alas, so I’ll have to leave this with a simple *danke schön!* One day the timezones may align to catch a live stream :).

