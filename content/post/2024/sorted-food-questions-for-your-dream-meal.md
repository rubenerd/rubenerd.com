---
title: "Sorted Food questions for your dream meal"
date: "2024-07-27T09:56:57+10:00"
year: "2024"
category: Thoughts
tag:
- food
- pointless
location: Sydney
---
The Sorted gents have a fun show format where they ask cryptic questions to determine how to cook someone's dream meal. It's all quite silly and tenuous, but that's the whole point! Ben was the [latest to be interviewed](https://www.youtube.com/watch?v=rVsYXBbuJmA), so I thought it'd be fun to answer the same questions.

* <span style="font-weight:bold;">Moist or Dry?</span> Us: Moist.

* <span style="font-weight:bold;">Plastic or Bamboo?</span> Us: Bamboo.

* <span style="font-weight:bold;">Soy or Worcestershire?</span> Us: Soy. 

* <span style="font-weight:bold;">Cabbage or Steak?</span> Ben: Stake. Me: **Cabbage!**

* <span style="font-weight:bold;">Laces or Slip Ons?</span> Us: Slip Ons.

* <span style="font-weight:bold;">Rubbing or Grinding?</span> Us: Grinding.

* <span style="font-weight:bold;">Glasses or Blindfolds?</span> Us: Glasses.

* <span style="font-weight:bold;">Jazz or Smash Metal?</span> Us: Smooth jazz!

* <span style="font-weight:bold;">Red or Green?</span> Ben: Red. Me: **Green.**

* <span style="font-weight:bold;">Vimto or Ribena?</span> Ben: Ribena. Me: **Vimto.**

* <span style="font-weight:bold;">Flour or Flower?</span> Ben: Flower. Me: **Flour.**

* <span style="font-weight:bold;">Barry or Janice?</span> Us: Barry.

* <span style="font-weight:bold;">Burnt or Boiled?</span> Us: Boiled.

We have more in common than I thought.
