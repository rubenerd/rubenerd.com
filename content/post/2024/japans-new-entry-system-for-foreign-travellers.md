---
title: "Japan’s new entry system for foreign travellers"
date: "2024-09-05T08:52:24+10:00"
year: "2024"
category: Travel
tag:
- japan
location: Sydney
---
Oona McGee reported in SoraNews24 about a [new travel authorisation system](https://soranews24.com/2024/08/29/japan-set-to-introduce-new-entry-system-for-foreign-tourists/) for tourists and travellers entering Japan from the 71 countries that have Visa waivers:

> The new system is said to run in a similar way to ESTA (Electronic System for Travel Authorisation) in the U.S., which was introduced as an anti-terrorism measure. Just as ESTA determines the eligibility of visitors to travel to the United States under the Visa Waiver Program, the Japanese version, which the government has tentatively named JESTA, will also screen visitors prior to entry, using a similar online system.

The ESTA programme in the US always felt weird. The questions were clearly targeted at sussing out undesirable people from friendly countries, but those would be the exact actors who wouldn't be truthful on such a form in the first place. I suppose it introduces a paper trail, but has anyone done a study confirming its effectiveness?

I guess it doesn't really matter in a practical sense. It won't change Clara's or my travel plans, and it'll just be another step to be carried out before we head off. We were already used to the process of declaring our Covid vaccines and plans during our last trip. It's just a bit sad there's a bit more friction now.

