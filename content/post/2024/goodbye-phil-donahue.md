---
title: "Goodbye, Phil Donahue"
date: "2024-08-20T10:07:36+10:00"
year: "2024"
category: Thoughts
tag:
- goodbye
- philosophy
location: Sydney
---
I'm sure I speak for a lot of people of my generation, but Phil Donahue's American daytime talk show was mandatory viewing when I'd be home from school, either because I was sick, or later when I was staying back to care for my mum after her chemo days.

He sparked many a conversation between us, often times that would stretch into days. His show was the lens through which I learned so much about my mum's philosophy and morals, which in large part formed my own. He also offered a glimpse into the US beyond what we learned from scripted TV and the evening news.

His kind, principled approach to discussion will be missed.
