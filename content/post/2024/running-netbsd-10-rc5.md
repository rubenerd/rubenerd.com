---
title: "Running NetBSD 10 Release Candidate 5!"
date: "2024-03-07T15:49:33+11:00"
year: "2024"
category: Software
tag:
- bsd
- fluxbox
- netbsd
- pkgsrc
- qemu
location: Sydney
---
The word *exciting* is overused thesedays, thanks to [unimaginative marketing](https://rubenerd.com/this-press-release-is-excited/ "This press release is excited!") executives and chatbots. But [NetBSD 10.0 is exciting](http://netbsd.org/releases/formal-10/NetBSD-10.0.html "NetBSD 10.0"). I remember talking with some of the devs about it at AsiaBSDCon in 2019.

I spun up some arm64 and amd64 QEMU builds on my work machine while sitting at a coffee shop, like a gentleman. I built my usual [cgd(8)](https://www.netbsd.org/docs/guide/en/chap-cgd.html "Guide on the cryptographic device driver") encrypted volumes, installed [pkgsrc](https://www.netbsd.org/docs/pkgsrc/index.html "The pkgsrc guide"), and ran my essential (cough) packages. Don't forget to install [compat9](https://pkgsrc.se/emulators/compat90 "compat9 package in pkgsrc") if you intend to run software built for NetBSD 9.0.

<figure><p><img src="https://rubenerd.com/files/2024/netbsd-10-rc5@1x.jpg" alt="" srcset="https://rubenerd.com/files/2024/netbsd-10-rc5@2x.jpg 2x" style="width:500px; height:291px;" /></p></figure>

Impressions thus far are very, very good! The maintainers weren't kidding about speed improvements either. In a world where major software and OS releases come with as much trepidation on my part as interest, it's wonderful to simply be pleasantly surprised.

I don't think I've mentioned this before, but in addition to some personal servers and retrocomputers, I've always had a NetBSD VM on work and personal laptops, and this latest one is no exception. I use it for testing, but also when I need a more distraction-free environment for writing (he says as he blatantly runs PySol). It absolutely *screams* on arm; faster than macOS has felt in a long time.

NetBSD with [Fluxbox](https://pkgsrc.se/wm/fluxbox "Fluxbox in pkgsrc") remains my other favourite OS combination, and I don't blog about anywhere near enough. I plan to change this.

My thanks to everyone in the NetBSD Project. I share my sadness at the loss of Ryo Shimizu, and also pay my respects to his memory and family. He made a lasting impression on all our lives. 🧡
