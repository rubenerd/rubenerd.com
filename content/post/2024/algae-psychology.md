---
title: "Algae psychology"
date: "2024-12-27T17:32:28+11:00"
abstract: "Phycology is the study of algae. Or, my attempt at a bad joke."
year: "2024"
category: Thoughts
tag:
- pointless
location: Sydney
---
My post about the [Minecraft Xmas Easter Egg](https://rubenerd.com/xmas-minecraft-chests/) made me want to try embedding one of my own. [In the last post](https://rubenerd.com/psychological-impact-of-surveillance/ "
The phycological impact of surveillance"), I:

* Linked to the "wrong" article. I quoted the Wikipedia article about Nori, or Japanese seaweed/algae.

* Wrote the title of the post as *phsycological* instead of *psychological*. *Phycology* refers to the study of algae.

* Get it? **GET IT!?**

I thought that was so clever, we'd all be able to revel in its sheer clever cleverness. Except it wasn't clever at all, because I misspelled *phycology*. **DAMN IT.**

I set up the pins, chose the perfect bowling ball, and threw it down the wrong lane. I built a DaVinci helicopter with upside-down rotors, and drilled myself into the ground. I made a beautiful jaffel or kaya toast with inverted bread and filling, and set the toaster on fire. I put trousers on my head, a shirt on my legs, and gloves on each foot. I ran `freebsd-update` with the `-r` release flag, and chose an *earlier* version. I installed a beautiful new hybrid floor with the noise-dampening pads on top. I launched my new car, train, and/or light aircraft backwards into a brick wall.

This is the kind of backfiring joke you'll only read right here at *Rubenerd*. Or other places.

To everyone who **(a)** still got my bad attempt at a joke, and **(2)** sent a correction, thank you! I'm surprised, happy, and a little scared how closely some of you read my stuff. I'd better be careful.
