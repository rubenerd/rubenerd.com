---
title: "Xmas Minecraft chests"
date: "2024-12-26T17:50:39+11:00"
abstract: "This Easter Egg (wait, wrong season) has become a highlight each year."
thumb: "https://rubenerd.com/files/2024/minecraft-xmas@2x.jpg"
year: "2024"
category: Software
tag:
- games
- minecraft
- xmas
location: Sydney
---
<figure><p><img src="https://rubenerd.com/files/2024/minecraft-xmas@1x.jpg" alt="Screenshot of Xmas chests in Minecraft" srcset="https://rubenerd.com/files/2024/minecraft-xmas@2x.jpg 2x" style="width:500px; height:281px;" /></p></figure>

This has become a highlight of the season each year. I was going to say it's an *Easter Egg*, but that's another time of year.

I almost don't want to log out, because after Xmas they return to regular chests. Oh well :').
