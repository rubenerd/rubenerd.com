---
title: "Doing an impromtu coffee taste testing"
date: "2024-11-14T20:27:07+11:00"
year: "2024"
category: Thoughts
tag:
- coffee
location: Sydney
---
It might shock some of you to know I spend time in coffee shops. [Apothecary Coffee](https://www.apothecarycoffee.com.au/) is my favourite on Sydney's North Shore, tucked away next to the Zenith building in Chatswood. They do great espresso, but I'm a particular fan of their pour-overs. This is opposed to a pour-on, which I've thankfully only performed at this coffee shop twice. Unfortunately, I was wearing khaki chinos both times.

I was enjoying the morning brew today, like a gentleman, when one of the baristas came to me asking if I wanted to help them dial in their new cold brew machine. It sounded like fun, and I wasn't about to pass up free coffee.

She gave me two small paper cups and asked me to choose which was my favourite. Both were made with the same beans, in the same machine, for the same duration of time. The only difference was the grind setting (*grs* written on the side refers to grind setting, not gram). I ignored the writing on the side until after tasting.

<figure><p><img src="https://rubenerd.com/files/2024/tasting@1x.jpg" alt="Two espresso-sized cups of coffee on a table, with 90 and 80 written on them respectively." srcset="https://rubenerd.com/files/2024/tasting@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

Both tasted great, without a trace of bitterness or astringency. But I noticed once was slightly weaker, almost watery. The other was stronger and fruitier, but still without any bitterness. I turned the cups back around, and realised the tastier was on the coarser grind setting. The barista agreed!

It made sense, sort of. You don't want to use coffee with too fine a grind for filter or immersion brews, or it clogs the filter and adds bitterness (likewise, you don't want too fine a brew for espresso, or you get channeling and pressure issues). But I still would have thought a finer grind would have made it stronger, especially in a cold brew setup where the greater surface area would have more contact with water for longer. This shook a few assumptions.

It's also interesting how much easier it is to tell subtle details in coffee when doing a comparison tasting. I can see why people review coffee and other drinks like this; and why it's so much fun!
