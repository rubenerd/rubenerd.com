---
title: "Disable Firefox 128’s latest adware “feature”"
date: "2024-07-15T10:29:13+10:00"
year: "2024"
category: Software
tag:
- firefox
- privacy
location: Sydney
---
If you're one of the few remaining people running Firefox, you've had a new feature enabled without your knowledge or consent. To disable:

1. Go to **Firefox Preferences**

2. Click **Privacy &amp; Security** in the sidebar, or do a search

3. Locate the new heading **Website Advertising Preferences**

4. Uncheck **Allow websites to perform-privacy preserving ad measurement**

If this really were a privacy-focused feature, it'd be opt in. We all know why it wasn't.

Get your act together Mozilla! You could stand for something here, and you'd have people rallying to your cause. Now people like me **only** use it because the alternative is slightly worse. It's the iOS of desktop browsers.
