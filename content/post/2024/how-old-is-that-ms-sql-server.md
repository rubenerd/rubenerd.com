---
title: "“How old is that MS SQL Server?”"
date: "2024-06-18T15:53:36+10:00"
year: "2024"
category: Software
tag:
- databases
- security
location: Sydney
---
[Richard Speed in The Register](https://www.theregister.com/2024/06/17/outdated_sql_server/?td=rt-3a)\:

> According to chief strategy officer [of Lansweeper] Roel Decneut, the biz scanned just over a million instances of SQL Server and found that 19.8 percent were now unsupported by Microsoft. Twelve percent were running SQL Server 2014, which is due to drop out of extended support on July 9 – meaning the proportion will be 32 percent early next month.

My favourite comment, by Philip Storry:

> I think see the problem...
>
> When they asked "How much [to upgrade]?", you told them how much it would cost to do it.
>
> You should have responded with "No idea. How much would it cost us if we couldn't take any orders for two weeks last month?"
