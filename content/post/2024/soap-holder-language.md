---
title: "Soap holder language"
date: "2024-10-08T08:31:39+11:00"
year: "2024"
category: Hardware
tag:
- language
- pointless
location: Sydney
---
Did you know that [this utilitarian soap dish](https://www.bunnings.com.au/caroma-chrome-cosmo-soap-holder_p4820135) has the following standout feature?

> Smooth clean lines to suit modern bathrooms

I do like smooth lines. Jaggered lines probably mean I've cracked a tile, the repair for which in a rental would be marked up ten times and taken from my bond.

> Contemporary style doesn’t have to mean edges and angles with the `$BRAND`. This soap holder offers a stylish design with smooth, clean lines at a surprisingly economical price, `$BRAND` create the perfect finishing touch for any bathroom.

I've been imbuied with contemporary style just by reading this! Though I thought it was supposed to be modern.

What's most incredible though is its Tardis-inspired size:

> Volume 0L

I'd say I do get a bit of fun out of fluffy language early in the morning, but in a bathroom setting that implies mould which likely isn't great.
