---
title: "IPv6 addresses with port numbers"
date: "2024-04-25T08:46:34+11:00"
year: "2024"
category: Internet
tag:
- ipv6
- networking
location: Sydney
---
Have you ever wondered how IPv6 even managed to make *port numbers* awkward? [Poul-Henning Kamp](https://varnish-cache.org/docs/7.4/phk/ipv6suckage.html "IPv6 Suckage"), of Varnish and FreeBSD fame:

> Colon was chosen [for IPv6] because it was already used in MAC/ethernet addresses and did no damage there and it is not a troublesome metacharacter in shells. No worries.
>
> [Need port numbers?] No worries, says the power that controls what URLs look like, we will just stick the port number after the IP# with a colon:
>
> http://192.168.0.1:8080/…

Spot the issue? The IPv6 designers didn't.

> That obviously does not work with IPv6, so RFC3986 comes around and says “darn, we didn’t think of that” and puts the IPV6 address in […] giving us:
>
>    http://[1080::8:800:200C:417A]:8080/
>
> Remember that “harmless in shells” detail? Yeah, sorry about that.

IPv6 would be funny if it weren't so <a title="The Register: How legacy IPv6 addresses can spoil your network privacy" href="https://arxiv.org/abs/2203.08946">tragic</a>. I know I've harped on about this a few times, but the idea that a committee of intelligent people thought encoding MAC addresses into an IPv6 address was in *any way* a good idea is absolutely breathtaking.
