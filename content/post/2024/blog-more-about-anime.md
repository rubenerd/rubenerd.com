---
title: "Vietnam, and “blog more about anime!”"
date: "2024-07-07T09:44:01+10:00"
thumb: "https://rubenerd.com/files/2024/theapothecarydiaries@1x.jpg"
year: "2024"
category: Anime
tag:
- apothecary-diaries
- southeast-asia
location: Sydney
---
My random [Blue Archive figure review](https://rubenerd.com/our-shiroko-blue-archive-fig-arrived/ "Sunaookami Shiroko by Good Smile Company") from earlier this week generated a ton of feedback, including several people from Vietnam. This shouldn't surprise me, Southeast Asia has a *lot* of anime fans.

*(Among my biggest regrets living in Singapore was not travelling around Vietnam when I had the chance. I got to travel around Indonesia, Malaysia, Thailand, and briefly Cambodia, but then I got to year 12 and spent more time studying. I'm sure I would have gained more life experience exploring Vietnam and Laos than I would have from any textbook, but that's a train of thought for another time).*

I used to blog about anime here *a lot* as well, and even had a separate anime blog that we all used to do. I don't watch as much of it anymore; the *Fate* movies were probably my most recent. But I'm keen to get back into it and write again. Clara and I are looking to pick up the *Apothecary Diaries*, and see what the furore was about with *SPYxFAMILY* and *Bocchi the Rock!* (I've read the manga).

If any of you have any recommendations for a couple of lapsed weebs, do let me know.

<figure><p><img src="https://rubenerd.com/files/2024/theapothecarydiaries@1x.jpg" alt="Screenshot from the Apothecary Diaries" srcset="https://rubenerd.com/files/2024/theapothecarydiaries@2x.jpg 2x" style="width:500px;" /></p></figure>
