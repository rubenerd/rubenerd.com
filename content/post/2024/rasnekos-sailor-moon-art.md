---
title: "Rasneko’s Sailor Mercury fanart"
date: "2024-06-06T07:56:21+10:00"
year: "2024"
category: Anime
tag:
- 1990s-anime
- nostalgia
- sailor-mercury
- sailor-moon
location: Sydney
---
My personal phone has had a retrocomputer or Hololive wallpaper for the past few years. But by chance I saw this [stunning art](https://www.pixiv.net/en/artworks/98617780) of everyone's favourite character from the series, and it was phone-sized!

<figure><p><img src="https://rubenerd.com/files/2024/phone-rasneko@1x.jpg" alt="Phone wallpaper showing Rasneko's Sailor Mercury art" srcset="https://rubenerd.com/files/2024/phone-rasneko@2x.jpg 2x" style="width:320px; height:693px;" /></p></figure>

They've done the [full lineup](https://www.pixiv.net/en/artworks/100602067) of the original characters which I'd buy as a print in a heartbeat. I wonder if they do comissions?
