---
title: "Unhelpful sites for moving"
date: "2024-10-21T15:51:42+11:00"
year: "2024"
category: Thoughts
tag:
- moving
location: Sydney
---
Moving house in Australia comes with so many wonderful challenges! Here are just a couple.

Our new apartment has gas, which we want disconnected. Here's what Jemena, the gas distributor, [says we need to do](https://www.jemena.com.au/gas/existing-connections/abolishmentdisconnection/)\:

> Customers who do not want to have the gas permanently removed can submit a request to their energy retailer to have the gas disconnected.

We... don't have an "energy retailer". We're just moving in. That's the whole point! Do they want us to sign up with a retailer, then immediately cancel? Because the alternative is to receive [ever-escalating letters](https://rubenerd.com/gas-company-crying-wolf/) from a gas company complaining that we need to register for a contract.

Likewise, this is what the Australian Government's [Energy Made Easy](https://www.energymadeeasy.gov.au/article/finding-your-nmi) site says under the heading "finding your NMI":

> NMI stands for National Metering Identifier. It’s a unique number for the electricity connection at your address.
>
> Here is a step-by-step guide to help you correctly use your NMI when comparing energy plans on our website.
>
> Get your latest energy bill [&hellip;]

Again, people, we don't have an energy bill. We're just moving in! The power company is asking for the NMI! How can I look up a bill I haven't received yet?

[Joseph Heller](https://en.wikipedia.org/wiki/Catch-22 "Wikipedia article on Catch-22") is alive and well, and running comms at Australian billing companies.
