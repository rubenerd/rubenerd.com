---
title: "My answer for OSnews about NetBSD"
date: "2024-11-06T13:47:56+11:00"
year: "2024"
category: Software
tag:
- bsd
- netbsd
location: Sydney
---
Thom Holwerda asked if we use NetBSD for anything. [I answered](https://www.osnews.com/story/141078/netbsd-the-portable-lightweight-and-robust-unix-like-operating-system/#comment-10444666)\!

🌲 🌲 🌲

NetBSD is wonderful! I first started using it back in the early 2000s when I was in school. My ageing iBook G3 refused to boot Yellow Dog Linux, but NetBSD/macppc installed without any issues. Starting from the ground up with networking, X11, and package management taught me more about \*nix than a year of university.

I’ve since moved to FreeBSD for most personal workloads for its tooling, but I still hold a soft spot for it (I also use pkgsrc everywhere too, but that’s a separate discussion), and use it wherever I can:

First, NetBSD works great on laptops, even better than FreeBSD in my experience. I run NetBSD/amd64 on a tiny Japanese Panasonic Let’s Note laptop for a distraction-free writing machine, to the point where I just use tmux as a poor man’s window manager (though I can fire up Xorg when I need to return to the real world).

Second, as its reputation suggests, it runs everywhere. I’m a retrocomputing tragic, so I have it on everything from a Sun SPARCStation 5, to the Pentium 1 machine I built as a kid. It’s ludicrously fun swapping from BeOS, DOS, or OS/2 into NetBSD. Also really helps for transferring files, doing partition backups etc.

And finally, I also just run it for all the basic server tasks we all do. I’ve joked sometimes that NetBSD is “boring” after coming home from $DAYJOB running whatever Rube Goldberg stack the Linux world has dreamed up that week, but honestly it’s the highest praise I could imagine. I love tinkering with hypervisors, and NVMM has been a lot of fun. NPF is also a balm to a troubled soul.

I’ve also had the privilege of meeting many of the engineers behind NetBSD and pkgsrc at events like AsiaBSDCon and Linux.conf.au, and they were some of the kindest, most humble, intelligent people I’ve ever talked with. They’re the exact opposite of hype and bluster, which I suppose shows through in their OS.

Thanks Thom for asking, and giving us the opportunity to talk. NetBSD gets vanishingly little coverage even among the BSDs, let alone OSs in general, so it’s really appreciated :). I’ve also been a reader for years, but this finally convinced me to create an account here. Also thanks to @torb@hachyderm.io for convincing me to comment.

