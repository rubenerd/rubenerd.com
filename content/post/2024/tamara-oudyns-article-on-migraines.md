---
title: "Tamara Oudyn’s article on migraines"
date: "2024-03-26T11:37:28+11:00"
year: "2024"
category: Thoughts
tag:
- health
location: Melbourne
---
Tamara Oudyn works for the ABC, and she [just wrote an article on migraines](https://www.abc.net.au/news/2024-03-26/migraine-cause-treatment-research-australia-ndis-disability/103553100 "ABC: Blind Spot: An estimated 4.9 million Australians battle through the debilitating pain of migraines.") that comes the closest I've ever read to describing how it feels:

> People who know me well can always tell if I’m getting a migraine while I’m reading the news — my face is a dead giveaway. The spark leaves my eyes, and my forehead and brows start to sink while I peer out at the camera through a descending fog of pain that sits, then tightens like a vice across the top half of my face. My blinking gets squinty, and the words become hard to find – which isn’t ideal for someone in my line of work.
> 
> Before long, I’ll be hunkered down in bed in the dark and quiet, counting my breaths and waiting for my meds to kick in.
> 
> People who get them can experience severe head pain, nausea, vomiting, dizziness, hypersensitivity to light and sound, blurry vision, muscle weakness and cognitive impairment.

I'm very lucky that I don't get migraines as often as I used to. Much of my teens and early twenties was spent *exactly* as Tamara describes, though thesedays I only get them every month or so (I say "only", but I wish I didn't get them full stop).

Migraines can get right in the proverbial bin.
