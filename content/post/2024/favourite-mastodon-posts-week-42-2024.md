---
title: "Favourite Mastodon posts, week 42 2024"
date: "2024-10-18T09:08:03+11:00"
year: "2024"
category: Thoughts
tag:
- mastodon
location: Sydney
---
As an aside, what a great week number.

[Owen Nelson](https://mastodon.social/@onelson/113297934990776703)\:

> I don't understand why books are so heavy when you put them in boxes.

[Heliograph](https://mastodon.au/@Heliograph/113297851894812816)\:

> If you don't have the skill to audit your LLM output don't use it

[Josh Simmons](https://josh.tel/@josh/113313594346365617)\:

> My least favorite part of being an executive, of a small org, with ADHD is how much of a tax all the shitty sales pitches are on my attention.

[Mostly Harmless](https://thecanadian.social/@MostlyHarmless/113312842000137849)\:

> Money may not buy happiness, but poverty sure as heck can’t buy anything.

[Ben Werdmuller](https://werd.social/@ben/113324727599298494)\:

> Everything is a lot all of the time.

[JP Mens](https://mastodon.social/@jpmens/113315097806745549)\:

> I often read "..., 2nd ed". Wasn't aware there where two Unix editors.


