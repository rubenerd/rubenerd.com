---
title: "Deshrouding my ZOTAC RTX 3070 Twin Edge?"
date: "2024-04-09T08:16:13+11:00"
year: "2024"
category: Hardware
tag:
- cooling
- games
- gpus
location: Sydney
---
I picked up the *ZOTAC RTX 3070 Twin Edge* graphics card right at the peak of blockchain scams, back when we were stuck at home and few cards were in stock. I paid *way* too much for it in retrospect, but the joy and entertainment it's provided over these last couple of years are also worth something.

Alas, the card is *loud*. All the usual YouTube reviewers claimed the fans on the Twin Edge 3070 and 3060 Ti were "fine", but they're only able to maintain temperatures by being as noisy as my PowerMac G5 of yore. I've throttled back graphics settings so I can hear Clara on the other side of the table, but it's a steep downgrade from what I paid for.

<figure><p><img src="https://rubenerd.com/files/2024/zotac3070@1x.jpg" alt="Press image of the Zotac 3070 Twin Edge" srcset="https://rubenerd.com/files/2024/zotac3070@2x.jpg 2x" style="width:500px; height:336px;" /></p></figure>

I evaluated selling it, but it's middling hardware from an old generation, so I likely wouldn't get much. And while I have plans to eventually replace it with a Radeon or Arc card to make life easier on Linux, it's simply not in the budget for this year. That leaves me with a more invasive option.

Deshrouding involves taking the manufacturer fans and plastic styling off the card, and attaching larger, more efficient fans. This can improve performance by wicking away more heat and reducing noise.

I did a bit of digging, and I'm thinking this:

1. Get two [Noctua A12x25-PWM](https://noctua.at/en/nf-a12x25-pwm) or [Arctic P12 Max PWM](https://www.arctic.de/en/P12/ACFAN00118A) fans to replace the tinny ZOTAC fans. Noctuas have been a popular choice since those official Noctua-edition cards came out, though I've also been extremely impressed with Arctic fans as a budget option.

2. Get a [decent quality](https://www.ebay.com.au/itm/224601642636) mini GPU to CPU 4-pin PWM cable to connect the fans to the card. You want to use connectors on the card not the motherboard, so they can use the card's sensors to adjust speed.

3. Use some cable ties to "attach" them to the card to see if the fans make any meaningful difference in thermals, performance, and noise.

4. If the solution is validated, get [this frame by mzluzifer](https://www.thingiverse.com/thing:5031055) 3D printed, tip them for their design, then use it as a more permanent solution.

5. For bonus points, perhaps look at what it would take to remove the heatsink itself and re-paste it with something a bit better, like my favourite [Arctic MX-4](https://www.arctic.de/en/MX-4/ACTCP00007B).

I assumed this would be a one-way operation that would void my now non-existent warranty and reduce its resell value, if it had any to begin with. But I think this is the most cost effective way to go for a steep quality of life improvement.
