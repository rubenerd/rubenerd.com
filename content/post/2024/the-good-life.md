---
title: "The good life ☕️"
date: "2024-09-15T07:27:37+10:00"
year: "2024"
category: Thoughts
tag:
- health
- personal
location: Sydney
---
So many things are up in the air right now, but I'm sitting out on the balcony on a cool morning with a fresh brewed coffee, my Ansible scripts updated my personal servers without issue, a sulphur-crested cockatoo came to say hello, and the letter *J* on my keyboard has stopped acting up.

It doesn't get much better than this. Which is worth reminding myself sometimes.
