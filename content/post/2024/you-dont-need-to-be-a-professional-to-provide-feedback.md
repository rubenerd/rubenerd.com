---
title: "You don’t need to be a professional to provide feedback"
date: "2024-02-20T17:06:38+11:00"
year: "2024"
category: Thoughts
tag:
- feedback
- urbanism
location: Sydney
---
Recently I had a discussion with a friend who's art was stolen by a generative AI user, and regurgitated onto online stores. She said it hurt, but that she wasn't "qualified to comment", given her lack of IT education and experience. I respected her caution; can you imagine if everyone admitted their ignorance? Many politicians, journalists, and managers would be out a job!

But I disagree with her premise for a simple reason. An artist is a *stakeholder* (can you tell I've been writing tenders and docs at work again)? Stakeholders are integral to the success of any system, and their feedback is vital to evaluating its performance, impact, and areas for improvement. A layperson's comments provide badly needed context, and serve as a reality check. I know from my own experience that IT engineers and talking heads are easily blinkered by complexity and the shiny, and can miss the forest for the trees. We all know of examples.

In a similar vain, if you're at all into the modern urbanist movement, you may have also seen [The Nth Review's critique](https://www.youtube.com/watch?v=bUs0ecnbOdo "Urban planning YouTube has a HUGE problem") of YouTubers not providing actionable advocacy advice. This isn't true, but he also fell into this professional comment trap:

> The problem is, few of these creators [<a href="https://rubenerd.com/stop-calling-people-content-creators/" title="Stop calling people content creators">sigh</a> –ed] aren't actually urban or traffic planning professionals, except maybe Dave from City Beautiful or Ray at CityNerd. These other creators, including NotJustBikes and myself, are enthusiasts, and fans. And we may know some key lingo, but we're not in the bureaucratic murk fighting battles day to day.

He's right to an extent. The specifics are beyond the scope of this post, but people need the experience and expertise to successfully advocate for change, and its important to get one's hands dirty. I've learned a lot over the last year or so about how to engage with my local and state government in Australia.

But again, I think he sells himself short in the same way my friend did. *Expertise isn't a prerequisite to being a stakeholder*. You don't need to be a statistician to have opinions on generative AI, nor do you need to be a traffic planner to resent being hit by a land yacht on wheels. Someone may lack sufficient understanding to recommend prescriptive steps to resolve an issue. But if a system affects you, you have a right to report on how it works (or doesn't) for you. This is crucial.

I think this caution comes from a good place. We see how unfettered, unchecked ignorance has lead to bad actors wielding disproportionate amounts of power, whether they be corrupt politicians or online trolls. I still remember our former prime minister commenting that "the laws of mathematics don't trump the laws of Australia" when discussing cryptography. *And he even had previous industry experience!*

Here comes the proverbial posterior prognostication: *but...* someone with good faith concerns are just as valid as the engineer like me who's building the system. A robust team and process can accept this advice and weigh it appropriately.
