---
title: "Is it worth collecting cassettes in 2024?"
date: "2024-01-26T23:26:12+11:00"
year: "2024"
category: Media
tag:
- cassettes
- hi-fi
- music
location: Sydney
---
The *too long, didn't read* is: if you want, but maybe not modern tapes! Wow that's a terrible setup for a post; pretend I *recorded* something profound here instead. On Metal, with Dolby C.

This transition from a general-interest blog of technology, development, and servers into a (mostly) retrocomputing site surprised even me. But I haven't talked that much about Hi-Fi gear, a related field that sucked me in during Covid lockdowns. Today we're exploring the compact cassette, the format you either loved to hate, or hated to love, or loved, or... what other combination am I missing?

<figure><p><img src="https://rubenerd.com/files/2024/tape-deck@1x.jpg" alt="Photo of Clara's and my inherited Yamaha tape deck." srcset="https://rubenerd.com/files/2024/tape-deck@2x.jpg 2x" style="width:500px; height:333px;" /></p></figure>


### A potted history of my own cassette adventures

Cassettes were well on the way out by the time I was old enough to understand music and want to record, collect, and buy it. I missed out on the tape culture that peaked in the 1980s, and I only picked up my first Walkman recently.

My little bedroom Aiwa microsystem had a cassette deck in the late 1990s, and I bought tapes from our local *Best Denki* in Singapore to record mix tapes, as well as songs and audio programmes off the radio. I was oddly interested in collecting news reports, as though I'd want to listen back to them for a bit of history when I was older. I don't know where they ended up; I suspected they were lost in that fateful household move to KL alongside my Zaft and Starfleet uniforms. Shucks.

It sounds silly, but I was captivated by the spinning supply and takeup reels inside their plastic shells. I knew how hard drive and floppy disks worked, but here was a mechanical storage medium *I could watch!* That physical aspect is something we'll touch on again shortly.

I still have a few shrink-wrapped tapes from this time, alongside a mix tape from my late mum. She was a calligrapher, but even her *print* writing was nicer than mine. 

<figure><p><img src="https://rubenerd.com/files/2024/tape-formulations@1x.jpg" alt="Photo of a selection of shrink-wrapped Metal, Chrome, and Ferric tapes, alongside a mix tape my late mum made." srcset="https://rubenerd.com/files/2024/tape-formulations@2x.jpg 2x" style="width:500px; height:333px;" /></p></figure>

### A digital distraction

I only used tapes for a few years, but they had a lasting and specific impression on how I listen to music to this day.

When we got our first CD burner, I was most interested in creating *mix CDs*. The process felt similar to making a mix tape on a dual tape deck: you'd place the source disk into the computer's second optical drive, then use a tool like Adaptec's authoring software to transfer your chosen tracks onto the CD-R. It was slow enough to feel like real time too, and you'd have to swap source CDs before you could transcode the next track.

The big innovation here came with the MP3, which let us rip CDs to the internal drive and burn whatever we wanted in one go. I did this to fill discs for a Creative MP3 CD player, my first portable system. I wasn't that familiar with MP3 bitrates, just like how I didn't understand why some cassettes I bought and recorded on sounded way better than others. All I knew was I had a CD full of my favourite MP3s. My first FireWire iPod let me store even more.

I briefly used streaming services, but to this day I still maintain this music collection, albeit with most of the tracks ripped at much higher quality. I've also long created my own virtual mix CDs, complete with custom ID3 tags and cover art! It's why I stopped using "scrobbling" services, because I didn't want to pollute their databases with albums that otherwise don't exist.

Anyway, the point here was that while I made a few mixes and recordings on tapes, their lasting impact on me was the idea that I *could* create an album of my own. Some of my earliest spreadsheets involved careful timings to see how I could arrange tracks. It wasn't sufficient to merely calculate total tracks for a 60 or 90 minute tape, you had to make sure you didn't inadvertently split one across two sides!


### *Rewinding* back to tapes

This takes us to the present, and the resurgent interest in legacy audio formats like LPs, MiniDiscs, and cassette tapes. The *technically correct* crowd bemoan this development, owing to the lower resolution, bitrate, sampling size, dynamic range, and/or other quality metrics compared to, say, FLAC. [I use 320 kbps MP3](https://rubenerd.com/choosing-audio-codes/ "Choosing audio file codecs"); *don't tell them!*

They have a point. Modern file formats are superior to these analogue or compressed digital formats in every metric. They take up little physical space beyond a USB key, they're higher fidelity, can be bought and downloaded instantly, and offer the best possible music experience.

<figure><p><img src="https://rubenerd.com/files/2024/tape-with-irys@1x.jpg" alt="Photo of a small Al Stewart tape above an IRyS LP, which dwarfs it in size" srcset="https://rubenerd.com/files/2024/tape-with-irys@2x.jpg 2x" style="width:500px; height:333px;" /></p></figure>

Likewise, LPs and 45s can sound exquisite on good-quality hardware, and have a richness and warmth that I have to tweak my graphic equaliser to get from a CD, especially early masters. Paul Simon's *Graceland* on CD just doesn't sound right otherwise, though unlike at German restaurants, I appreciate the lack of crackling. It's an experience seeing a vinyl folder with its huge cover art and liner notes, then taking out and holding the sound waves in your hand, and cuing it up yourself on a turntable. You either get this, or you don't.

Cassettes share many of the same qualities. Like records and CDs, you're holding a physical object in your hand which you've picked out to play. It makes music feel tangible and real in a way an audio file can't. It also scratches a technical itch learning about all the noise reduction algorithms, tape formulations, bias types, and decks. As I said above, I loved watching the reels spin as a kid, and I still do.

Cassettes also come with a cultural element missing from other formats. They were the *rip, mix, and burn* before the MP3. The also ushered in portable music thanks to the original Walkman. Generations of people grew up listening to them on their commute, whether by car, bus, train, or foot. The mix tapes I have from my mum are treasures.


Clara and I have a small set of drawers for our cassette collection, and a deck and Walkman to play them on. It's fun! Not to get all Malcolm Gladwell on you, but *turns out* tapes don't get stuck or come out of their shells if you take care of them, and both play and record them on good quality kit. I made an anisong compilation tape last year on our Yamaha deck with Dolby C on a high-bias Type II Chrome tape, and it sparkles like the eyes of a shoujo character! (I've yet to use my Sony Metal tape yet, because I feel like it needs to be something extra special).


### Here comes the proverbial posterior prognostication

*But...* it's worth remembering a few things about tapes in a modern context. Cassettes could have excellent sound quality if you used the right carts, but Type IV Metal and even Type II Chromes are starting to command ridiculous prices. I have a couple of Chrome and Cobalt (fancy Type I Ferrics) albums which sound surprisingly clear and rich for pre-recorded tapes, but they're the exception:

<figure><p><img src="https://rubenerd.com/files/2024/tape-chrome@1x.jpg" alt="Photo of a pre-recorded Chrome and Cobalt tapes." srcset="https://rubenerd.com/files/2024/tape-chrome@2x.jpg 2x" style="width:500px; height:333px;" /></p></figure>

Likewise, modern cassettes are&mdash;sad to say&mdash;rubbish. Due to modern mechanisms being crap, the impossibility of licensing Dolby noise reduction, lack of modern knowledge of sound mastering for analogue tape, and the continued manufacturer of Type I Ferric tapes, modern pre-recorded cassettes sound thin, muffled, and noisy. I made it one song into BTS's *Butter* before I couldn't hear it over the sound of my laughter.

But that's not really the point of these tapes. As mentioned, these are physical anchors to the digital world; something tangible you can buy to support an act, and display. But unlike modern LPs with their premium, heavier vinyl, these new tapes still sound crap. They're probably best left on the shelf, or bought for their cases and J-cards and re-recorded on better kit.


### In conclusion, finally!

So are they worth collecting today? It depends what you want out of your music. I know plenty of people older than me who said they couldn't wait to rid themselves of their tapes. Others hold onto ones they listened to while they were in school, or commuted with in the 1980s and 90s. People like me enjoy them for the reasons stated above, though I'd probably lean towards the LP or CD if given the choice.

There's this attitude in technical circles that not employing the ordained way of doing something makes you ignorant, stupid, or deluded. Cassettes evoke this visceral reaction; you'll be told tapes are crappy, outdated, unreliable, and that you're a hipster who's only interested in them because you saw one in a *Guardians of the Galaxy* movie.

Who cares? You don't need to justify your hobbies or interests. Like retrocomputing, if you want to learn about them, that's your choice. Tapes were a cultural revolution, and there's a fascinating history behind their development and technology. I still tinker with them today for retrocomputer storage!

Music is art to be enjoyed: it's up to you how you want to experience it. Now where's my 7-inch reel to reel?
