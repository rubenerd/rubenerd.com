---
title: "Performance issues, and posts from Thursday"
date: "2024-07-26T09:17:10+10:00"
year: "2024"
category: Internet
tag:
- freebsd
- troubleshooting
- weblog
location: Sydney
---
I spent some time fixing the [performance issues](https://rubenerd.com/performance-issues-from-europe/) some of you reported earlier this week; hopefully things look better now. It's all actually configured and running correctly now! My plan is to eventually replace a bunch of stuff with the imitible Poul-Henning Kamp's Varnish, but one step at a time.

Funny story, I realised that I stood up my latest cloud VM using a *very* old copy of my personal Ansible roles library which, lets just say, did some horrible things. The mark that you've improved as an engineer (or an artist!) is whether you're embarrased by your past work. The way I did networking back then was... well, let's just leave it at that.

In the process, I noticed posts from Thursday only just posted. Whoops! It's not like I've literally been blogging for two decades or anything.

Special thanks to Rebecca H., [kivikakk](https://kivikakk.ee/), and E.D. who helped me with some of the troubleshooting from their homes in Europe. My only regret now is that I badly want to visit Wales and Estonia, *on top* of Latvia, Poland, Armenia, and Ukraine. Clara and I are specifically budgeting for a house price range so we can travel more again. I hope you're all happy with yourselves.

**Update:** We have confirmation from the most beautiful coastline in Europe too, with special thanks [Misko Kranjec](https://mastodon.social/@miskolin/112851560075572462) :).
