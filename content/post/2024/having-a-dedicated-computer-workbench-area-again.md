---
title: "Having a dedicated computer workbench again"
date: "2024-03-20T08:24:25+11:00"
year: "2024"
category: Hardware
tag:
- homelab
- retrocomputers
- tinkering
location: Sydney
---
I hated our massive family house in Kuala Lumpur when I was a teenager. The serviced apartment we had before was amazing; it was [close to public transport](https://myrapid.com.my/bus-train/rapid-kl/lrt/), amenities, and things to do. We could see much of [KLCC](https://commons.wikimedia.org/wiki/File:Petronas_Twin_Towers._View_from_KL_Tower._2019-11-30_20-50-32.jpg) from the lounge! By comparison, that house in far-flung suburbia may as well have been on the moon, with even trips to coffee shops being out of the question unless you booked a cab.

Here comes the proverbial posterior prognostication: *BUT...* I will say that having a bedroom the size of our entire current apartment in Sydney did offer one advantage: I had space for a dedicated workbench.

Computer workbenches are great, because you don't need to partially disassemble an existing setup to test or fix other kit. You can have a spare monitor, keyboard, tools, and charging cables ready for any project. More sophisticated people might also have an oscilloscope, multimeter, soldering iron, and a model of the *Enterprise D* (cough).

I recently talked about my ridiculous [retrocomputer KVM setup](https://rubenerd.com/finally-got-my-ultimate-retro-kvm-setup-working/ "Finally got my ultimate retro KVM setup working!"), which thanks to a hornet's nest of adaptors, upscalers, converters, and cables, let me attach everything into one keyboard, mouse, and monitor. This let me consolidate two previously discrete setups into one, which freed up space for... get this:

<figure><p><img src="https://rubenerd.com/files/2024/workbench@1x.jpg" alt="" srcset="https://rubenerd.com/files/2024/workbench@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

It might not look like much, but this has been months in the making! This patch of Clara's and my shared desk in this tiny apartment previously hosted my RetroTINK machines, which are now connected to the bigger monitor on the other side of the table with the Apple //e and my vintage PCs.

This baby computer workbench has a monitor and keyboard ready for connecting to what I need, along with powered USB-C and Micro-USB cables, and a box of tools on the floor alongside. It means I now have a setup ready to go when I need to configure a Raspberry Pi for a Pi1541 disk drive, or install FreeBSD onto a Vision Five board, or troubleshoot why my tiny Toshiba Libretto 70CT decided that powering up its internal LCD is optional.

Before I had this setup, I'd have to turn off my primary desktop, unplug the monitor and peripherals, and connect it all to what I was testing. Then when I was done, plug it all back in again, turn it back on, and navigate the awkward Dell LCD menu to get the right input back.

I've realised as I get older that if I don't make something easy, or at least lower the level of friction involved, I likely won't do it... even if it's a hobby. My shelves are bursting with projects I haven't started or finished, but hopefully this new setup will make these easier.

Of course there's lots to do here still; I need an Ethernet cable from the switch on the other side of the desk, but I currently don't have one long enough. Maybe I can use  some of those Velcro ties we use at the data centre to bundle all these hidden cables together. And finally, I'd love to get one of those tall, skinny Muji chests for organising my bottles of cleaning solution, compressed air, and lithium grease at the bottom, and things like soldering irons, wire crimpers, and spare parts in the top.

