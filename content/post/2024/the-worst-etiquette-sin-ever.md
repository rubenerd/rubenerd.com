---
title: "The worst etiquette sin ever"
date: "2024-10-11T10:37:59+11:00"
year: "2024"
category: Ethics
tag:
- phsychology
location: Sydney
---
[William Hanson, via Wired](https://www.youtube.com/watch?v=bdyyin_9izI)\:

> If you do not say please, thank you, and sorry; as a human being walking on this Earth; then you should be put into Etiquette Room 101, and the key should be thrown away.
>
> Those are the absolute basic fundamental things of being a human being. 
