---
title: "Premature scale optimisation"
date: "2024-07-29T09:08:56+10:00"
year: "2024"
category: Software
tag:
- architecture
- complexity
- design
location: Sydney
---
The Tailscale people have been a refreshing voice of candour on a lot of online infrastructure, such as [their position on IPv6](https://tailscale.com/kb/1134/ipv6-faq). *Candour* sounds like a budget German airline. Wait, that's <a href="https://en.wikipedia.org/wiki/Condor_(airline)">Condor</a>. Didn't they get bought by Thomas Cook? Wait, they're <a href="https://en.wikipedia.org/wiki/Condor_(airline)#2010%E2%80%932020:_Rise_and_fall_of_Thomas_Cook_Group_Airlines">independent</a> again? Huh, the more you know.

If you'll stop interrupting me, Avery Pennarun [wrote another great post](https://tailscale.com/blog/new-internet) on what I've come to call *premature scale optimisation:*

> I read a post recently where someone bragged about using kubernetes to scale all the way up to 500,000 page views per month. But that’s 0.2 requests per second. I could serve that from my phone, on battery power, and it would spend most of its time asleep.
>
> In modern computing, we tolerate long builds, and then docker builds, and uploading to container stores, and multi-minute deploy times before the program runs, and even longer times before the log output gets uploaded to somewhere you can see it, all because we’ve been tricked into this idea that everything has to scale. People get excited about deploying to the latest upstart container hosting service because it only takes tens of seconds to roll out, instead of minutes. But on my slow computer in the 1990s, I could run a perl or python program that started in milliseconds and served way more than 0.2 requests per second, and printed logs to stderr right away so I could edit-run-debug over and over again, multiple times per minute.

I wear a solution architect hat for half my job, and I hear daily how clients and leads are planning to build, upgrade, replace, expand, or migrate workloads. It surprises me just how often people have convinced themselves that they "need" these complicated tools.

I consider it a similar fallacy developers fall into with premature optimisation. It comes from a good place, but the opportunity costs and added complexity have to be weighed against the expected returns. Just as the working poor voting for wealthy tax cuts aren't temporarily embarrassed millionaires, we're not (all) temporary embarrassed sysadmins.

And those complexities are real. Technical writer [Michael W. Lucas](https://mwl.io/) (hey, go [support](https://mwl.io/about/patronize-me) his work!) hinted at [one aspect back in June](https://io.mwl.io/@mwl/112558865356267567), though there are plenty of others:

> That's the problem with Docker. It's easy to get stuff up and running, but when it breaks you have to dive deep and learn what's inside the container.
> 
> I keep intending to write something about how containerization moves the learning period from "before deployment" to "during outages," and that's bad.

It reminds me of a comment Marco Arment of Tumblr fame made on his old *[Build and Analyse](https://buildanalyze.fireside.fm/)* show with Dan Benjamin: you probably just need a couple of VPSs. I guess it's hard to sell the next shiny thing with that approach though.
