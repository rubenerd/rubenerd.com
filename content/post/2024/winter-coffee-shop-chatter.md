---
title: "Winter coffee shop chatter"
date: "2024-05-24T08:17:30+10:00"
year: "2024"
category: Thoughts
tag:
- coffee-shop-chatter
- weather
location: Sydney
---
I overheard a conversation this morning with logic I can't fault:

> **Person 1:** I don't like winter.
>
> **Person A:** You don't like winter?
>
> **Person 1:** Nah, I don't like winter.
>
> **Person A:** Why don't you like winter?
>
> **Person 1:** It's too cold.
