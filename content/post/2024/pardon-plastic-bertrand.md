---
title: "Pardon, Plastic Bertrand"
date: "2024-03-25T07:15:29+11:00"
year: "2024"
category: Media
tag:
- beligum
- music
- music-monday
location: Sydney
---
Today's *[Music Monday](https://rubenerd.com/tag/music-monday/)* is a presentation of one of the greatest songs of all time, and an apology!

*Ça plane pour moi* was one of the songs of my childhood, and I still consider it one of the greatest of all time. When I decided to add a nonsensical *Capacitive Duractance* section on my sidebar, it dawned on me to include before almost anything else.

<figure><p><a target="_BLANK" href="https://www.youtube.com/watch?v=8soQkubMk1g" title="Play Plastic Bertrand - Ça plane pour moi (Official Audio)"><img src="https://rubenerd.com/files/2024/yt-8soQkubMk1g@1x.jpg" srcset="https://rubenerd.com/files/2024/yt-8soQkubMk1g@1x.jpg 1x, https://rubenerd.com/files/2024/yt-8soQkubMk1g@2x.jpg 2x" alt="Play Plastic Bertrand - Ça plane pour moi (Official Audio)" style="width:500px;height:281px;" /></a></p></figure>

But I'll admit that I'm a hopeless Anglo when it comes to its origins, even despite my fascination and interest in the Benelux countries. I assumed Monsieur Bertrand was French, but he's Belgian. **Zut alors!**

I will atone for this by buying some Duvel and waffles this month to have with Clara's and my regular Sole Meunière analogue, which we make with basa.

*Ça plane pour moi! Ça plane pour moi! Ça plane pour moi moi moi moi moi! Ça plane pour moi!* ✈️ 🇧🇪
