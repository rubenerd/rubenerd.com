---
title: "How YouTube treats MH370"
date: "2024-03-19T08:09:48+11:00"
year: "2024"
category: Ethics
tag:
- aviation
- malaysia
- video
- youtube
location: Sydney
---
Mentour Pilot recently [posted a video about MH370 📺](https://www.youtube.com/watch?v=Y5K9HBiJpuk), the Malaysia Airlines flight from Kuala Lumpur to Beijing that vanished over the Indian Ocean in 2014. Petter's discussion of the flight was empathetic, articulated clearly, and well researched. I recommend it to anyone interested in learning about the facts.

I say this for two reasons. First, Petter has a great channel, for which I've been a Patreon for many years. But second, the event has become disaster porn for most people, where details and facts aren't as important as speculation, clickbait, and lies for milking engagement and ad revenue.

Take the thumbnails for these two videos about the disaster, which have garnered millions of views. Spot any issues?

<figure><p><img src="https://rubenerd.com/files/2024/mh370-garbage@1x.jpg" alt="A terrible AI photo, and a stolen photo of a 737" srcset="https://rubenerd.com/files/2024/mh370-garbage@2x.jpg 2x" style="width:500px;" /></p></figure>

The first video is an AI-generated mess. We have a DC-9 nose cone, A320-ish wing fences, and a 737 windshield and vertical stabiliser. Impressively, *none* of these are even remotely correct; the missing plane is a wide-bodied Boeing 777 with a passenger capacity twice that of these short-range jets. The second video uses a stolen image of a 737 from the AP, which also isn't the correct aircraft make, model, or livery.

Not pictured here are their video titles, descriptions, and graphics which refer to it as *Malaysian Airlines*. It's impressive how you can get that wrong when the correct spelling is literally staring at them in their own thumbnails.

Other independent *content creators* (I use the term deliberately in this context) fare slightly better, but fail in other ways. Here are some more examples:

<figure><p><img src="https://rubenerd.com/files/2024/mh370-clickbait@1x.jpg" alt="A channel asking if the plane was deliberately downed by a pilot, and another rubbish one with the pilot saying aliens, because mocking victims is hilarious!" srcset="https://rubenerd.com/files/2024/mh370-clickbait@2x.jpg 2x" style="width:500px;" /></p></figure>

The first shamelessly invokes [Betteridge's Law of Headlines](https://en.wikipedia.org/wiki/Betteridge%27s_law_of_headlines "Wikipedia article on Betteridge's law of headlines") for clickbait, because it wouldn't end in a question mark if they had evidence to back up their assertion. The second is, to use the Zoomer term, pure cringe. *We don't know* what happened with either pilot. If one or both were victims, I'm sure their families really appreciate these characterisations.

What gets me is that [even mainstream outlets](https://rubenerd.com/erwin-knolls-law-of-media-accuracy/ "Media accuracy on topics with which you’re familiar") get *basic* details wrong. The Sun alleged that routine aircraft operations are "bombshells", which is on-brand for their fish and chip wrappers, but preposterous. American TV station CNN also gets the make, model, and livery of the aircraft wrong.

<figure><p><img src="https://rubenerd.com/files/2024/mh370-suncnn@1x.jpg" alt="Some wanker from the Sun, and CNN showing a Malaysia 737" srcset="https://rubenerd.com/files/2024/mh370-suncnn@2x.jpg 2x" style="width:500px;" /></p></figure>

Not to be outdone, Yahoo and CNN used *the same photo* of the wrong aircraft in their thumbnails. Did all these companies do a stock photo search for "Malaysia Airlines" without bothering to check if it was correct?

<figure><p><img src="https://rubenerd.com/files/2024/mh370-same@1x.jpg" alt="Two thumbnails showing a Boeing 737." srcset="https://rubenerd.com/files/2024/mh370-same@2x.jpg 2x" style="width:500px;" /></p></figure>

And finally, we have this incredible AI-generated rubbish with a reaction that so perfectly summarises all these thumbnails. I'd say this isn't Malaysia Airlines, or the right plane, but I'm not even sure this is a plane at all. The wings are tiny, it's missing cabin windows, there's a chunk absent from the vertical stabiliser, the livery makes no sense, the forward right door has a superfluous frame that looks like its about to fall off, and the cockpit looks like its from a piston-powered... DC-7? Is that the level of care and attention we're supposed to expect?

<figure><p><img src="https://rubenerd.com/files/2024/mh370-spark@1x.jpg" alt="I... I just can't. Where do you begin writing accessible alt text for this?" srcset="https://rubenerd.com/files/2024/mh370-spark@2x.jpg 2x" style="width:500px;" /></p></figure>

This only covers the thumbnails. I haven't even touched on the videos themselves, because if I did, it'd be all I'd write about for weeks. There is *so much* misinformation&mdash;pardon, *lies*&mdash;surrounding this story. People getting basic details about the aircraft wrong. Endless speculation and  dull conspiracy theories with no evidence. Unfair besmirching of an entire airline and country, just as we saw during the 737 MAX crashes. I take it personally, and I only flew on them a few times, and lived in Malaysia briefly! It's grim.


### So they got details wrong, so what?

It's always three things with me.

The first, and by far the most important, comes down to basic respect. It would have taken these *content creators* and media outlets mere minutes to verify the aircraft in their thumbnails was correct. For those AI images, they could have spent just as long looking for accurate Creative Commons photos. Here, it took me [less than ten seconds](https://commons.wikimedia.org/w/index.php?search=malaysia+airlines+777). It's sloppy, lazy journalism, and is not how you respect the people affected by this tragedy.

It's also another example of what outcome you can expect from misaligned incentives. [As Hbomberguy discussed 📺](https://youtu.be/yDp3cB5fHXQ?feature=shared&t=3846) in the content mill section of his video on plagiarism, these videos don't come from a place of interest, or a desire to tell important stories. They're churned out at scale for clicks, engagement, and ad revenue. It also continues to floor me that certain techbros insist that AI will somehow improve journalism, when evidence like this exists.

Most worryingly for me personally, it has implications for the web at large, and our collective understanding of history. If you'll forgive the self-quote from my [post about the RMS Lusitania](https://rubenerd.com/photos-of-the-lusitania-online/ "Photos of the RMS Lusitania online"):

> It's important to get your facts right when you're writing a post (or producing a podcast or video). This experience demonstrates how trivial it is to spread misinformation about historical photos and events; mistakes that percolate and wash across the web like sea foam to be reposted elsewhere. It’s a little bit scary to extrapolate that out to every historical event and topic being shared online today, with each generation of mistake or error compounding our misunderstanding of the world.

I think we can even see this play out in these thumbnails. Did a CNN journalist see the CBS video, assume it was accurate, and go with the same incorrect aircraft? Did the Yahoo employee recognise the stock image and use the same one? Was the AI that created those horrifying images fed erroneous information too? Before you know it, these images you may otherwise have considered harmless are now on the public record of the event. Most people aren't going to read technical documents about the incident, their understanding comes entirely from these videos, and the subsequent echo chamber comments under them.

The Wall Street Journal proclaims the *Truth Dies in Darkness*. I'd say in the 21st century that *Rubbish Buries Reality*.

Again, I encourage you to check out [Mentour Pilot's MH370 video 📺](https://www.youtube.com/watch?v=Y5K9HBiJpuk) if you're interested in learning more.  Let's reward ethical people who do a good job.
