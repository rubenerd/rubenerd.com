---
title: "Assuming a component is the whole system"
date: "2024-03-04T20:50:41+11:00"
thumb: "https://rubenerd.com/files/2024/shinkansen-osaka@1x.jpg"
year: "2024"
category: Software
tag:
- australia
- hong-kong
- japan
- osaka
- public-transport
- sydney
- systems
- trains
location: Sydney
---
A few years ago I'd come back from a trip with Clara to Hong Kong and Ōsaka, like a gentleman. It was my first trip to both Hong Kong and Japan, and still consider it one of the most special trips we've ever taken. But I digress.

I tapped my Opal card at the Sydney airport upon our return, and noted how much *slower* it felt for the turnstiles to open compared to the MTR, Ōsaka Metro, JR, and Hankyu. The MTR felt as quick as the Singapore MRT, and Japanese card readers felt instant. Sydney's turnstiles take a solid second to register, requiring you to wait and hold up the people behind you. It doesn't seem like much in isolation, but you *really* notice the difference in a crush of people in peak hour.

I noted this on Twitter at the time, and immediately had a few people mansplain that that the Sydney Opal system shares the same technology as the Hong Kong MTR. They concluded I was deluded, lying, or not especially bright for suggesting there was any difference.

<figure><p><img src="https://rubenerd.com/files/2024/mtr-2017@1x.jpg" alt="Sign pointing in the directon of Hong Kong at the airport station." srcset="https://rubenerd.com/files/2024/mtr-2017@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

These responses were&hellip; interesting for a few reasons. It was clear these armchair mass transit experts had no experience using these systems whatsoever, had likely done a quick Wikipedia search, and had confidently based a conclusion on the most superficial of readings. Their patronising tone, and their disregard of a lived experience were nothing new either, but that's the web for you.

You've likely already spot the issue. They assumed (there's that word!) that one could extrapolate the function of an entire system based on one constituent component. The fact the Sydney Opal card, and the Hong Kong Octopus card use the same NFC hardware means they must operate the same, right? It's the same reason computers with QWERTY keyboards all have the same CPU, OS, and wallpaper of Mashu Kyrilight from FGO.

<figure><p><img src="https://rubenerd.com/files/2024/shinkansen-osaka@1x.jpg" alt="Concourse view of the Shinkansen ticket area at Shin Osaka station" srcset="https://rubenerd.com/files/2024/shinkansen-osaka@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

The truth is much messier. Card readers are but the human interface to a vast and unfathomably complex system of interconnected parts that have to sing and dance to the same tune. There's authentication, payment processors, databases, and procedures for calculating distances, prices, fares, concessions, and refunds. Most transit systems don't have the benefit of being built in isolation, and usually have messy integrations with legacy platforms and other transit agencies.

In the case of card readers, Sydney Trains clearly has a component (or several) that isn't responsive enough to let the card reader operate as quickly as those on the MTR, or those aforementioned Japanese readers. Either that, or those latter systems have a degree of optimisation we simply don't. The outcome is the same either way; cards built from the same components, from the same manufacturer, diverge in responsiveness when operating under different conditions and systems. It almost seems silly pointing it out.

But *here's the thing*. I don't even necessarily blame narrow-minded people on social media for such comments. Most people, myself included, don't appreciate just how much complexity underpins everything we do in our world. My desktop is plugged into a wall, with electrons flowing from a generator somewhere. I just ate food that was grown somewhere. I messaged my sister&mdash;who's in Tōkyō funnily enough&mdash;and she was able to reply in a few moments thanks to networks somewhere.

It's all so *simple*, but it isn't.

