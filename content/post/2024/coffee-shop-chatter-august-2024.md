---
title: "Coffee shop chatter, August 2024"
date: "2024-08-14T08:12:58+10:00"
year: "2024"
category: Thoughts
tag:
- coffee-shop-chatter
location: Sydney
---
I haven't been to a coffee shop for a while, on account of Clara and I scrimping for an expensive move and buying a house. It was *so much fun* treating myself this morning, and writing a post about [our first server](https://rubenerd.com/our-first-home-server-and-rekindling-that-joy/).

But I digress. Here's some early morning Sydney coffee shop chatter:

* Mate, I didn't see it anywhere. It's like it wasn't even there. It's so confusing. Literally nothing! I know right!?

* You can't mend that because it's broken, but how can you work around it?

* How was [indesiperable]? Oh, that good huh? Yeah I've always wanted to go there. I bet they have amazing supermarkets [... I think that's what they said!?]

* [Someone with one of those accents where they whistle the letter S so loudly that it permeats the entire space]. SSSSSSSSSometimes I wonder if the SSSSSSSSSSSSSSystsem can SSSSSSSSSupport...

* Do you do a chai latte? No, not a coffee, a chai. Like, as a latte. Oh, it doesn't have coffee? Okay I'll have a chai then! But not the latte. Oh okay, yeah, a chai latte.

* It's on Signal mate, they can't do it!
