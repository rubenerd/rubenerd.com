---
title: "We must not be done with kindness"
date: "2024-11-25T08:05:59+11:00"
abstract: "A post by Paul Kafasis after the US election emplores us to not be done with joy."
year: "2024"
category: Thoughts
tag:
- kindness
location: Sydney
---
Paul Kafasis <a href="https://onefoottsunami.com/2024/11/20/where-we-go-from-here-2024/" title="Where We Go From Here (2024)">concluded a post</a> about the recent elections in the US with this, but I think it broadly applies:

> Even as cruelty is ascendent, we must not be done with kindness. Let us also not be done with joy. Instead, we should locate humor wherever we can and revel in it. The darkest times still contain something to laugh about. We’re going to continue to find it right here, together.

Kindness is one of the great multipliers: it costs very little, and compounds. That doesn't mean we should be reflexively kind to those who wish us harm; instead we should create the environment where they can't thrive.
