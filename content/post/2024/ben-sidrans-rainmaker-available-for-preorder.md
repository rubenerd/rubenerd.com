---
title: "Ben Sidran’s Rainmaker available for preorder"
date: "2024-04-22T07:51:49+10:00"
thumb: "https://rubenerd.com/files/2024/a0794211635_16@1x.jpg"
year: "2024"
category: Media
tag:
- ben-sidran
- jazz
- music
- music-monday
location: Sydney
---
Today's *[Music Monday](https://rubenerd.com/tag/music-monday)* is an announcement! I just got pinged from his newsletter that Ben Sidran's latest album *Rainmaker* is [available for preorder](https://bensidran.bandcamp.com/album/rainmaker "Ben's album on Bandcamp").

<figure><p><img src="https://rubenerd.com/files/2024/a0794211635_16@1x.jpg" alt="Cover for Ben Sidran's Rainmaker" srcset="https://rubenerd.com/files/2024/a0794211635_16@2x.jpg 2x" style="width:375px; height:375px;" /></p></figure>

I've been listening to Ben since before I could walk. His influences span across jazz, fusion, and bebop, to dabblings in electronica and blues. Like Cory Wong, he has the uncanny ability to bring in some of the best musical talent around him as well.

You can [preorder](https://bensidran.bandcamp.com/album/rainmaker "Ben's album on Bandcamp") digital tracks, or you can also get a CD.


