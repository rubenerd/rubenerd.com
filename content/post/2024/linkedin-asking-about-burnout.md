---
title: "LinkedIn asking about burnout"
date: "2024-03-30T09:41:21+11:00"
year: "2024"
category: Thoughts
tag:
- work
location: Melbourne
---
I got this email from LinkedIn Australia; maybe you got something similar:

> My name is $PERSON and I'm a Senior Editor at LinkedIn News Australia. I often get in touch with professionals who can add informed perspectives on news and trends.
>
> Burnout is increasingly an issue among workers today. The fast-paced nature of the workplace, coupled with the expectation of constant availability, can make employees feel overwhelmed and stressed. What strategies can they adopt to protect themselves against burnout? 
> 
> We would love to hear your thoughts on this topic. Please share your tips in a comment on this article.

I'm of three minds about this, because it's *always* three things with me!

LinkedIn talking about burnout feels like those horse-racing advertisements asking you to *bet responsibly*. I clicked on it to see what others had submitted, and was immediately asked to enable notifications, which is beautifully poetic. Their entire platform is predicated on feather preening and constant communication. Forgive me if this sounds cynical, but I always have my doubts when a business models conflicts with a desired social or ethical outcome; the former invariably wins.

Based on the few times I've been on LinkedIn, I can also take a guess at the sort of answers people might provide. On the one side you'd have those who've been suckered into what documentarian Dan Olson jokingly calls *the grindset*. You have to put in the hard yards, discard your family, and destroy your health in the pursuit of excellence, damn it! Then there are the humble braggers who answer that weakness question in job interviews with *well, I just work too hard.*

*(In case anyone reading this believes that nonsense, no, you don't have to do that. In fact, you'll be more productive taking your personal, health, and family life more seriously).*

But I suppose if we ever want to broach the topic among professional circles, or at least those cosplaying as such, this would be one of the places to reach them. Acknowledging the issue is the first step, right?
