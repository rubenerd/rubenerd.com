---
title: "Returning to the dual-browser life"
date: "2024-10-13T10:03:25+11:00"
year: "2024"
category: Software
tag:
- bsd
- browsers
- mozilla
location: Sydney
---
I mentioned the joys of [browser monocultures](https://rubenerd.com/an-october-2024-browser-monoculture-update/) again recently, which has lead to the inevitable question: what am I running if Gecko-based browsers have been relegated to the "couldn't be bothered to test/support/etc" camp once more?

I wish I had a better answer, but it's Chromium on my desktops, and Safari on my Macs. I *try* to see the positive and silver lining to situations if I can, and this time I'm telling myself that I'm keeping the likes of `$GOOG` in a sandboxed, separate browser of its own. I *suppose* there's utility in that. But it still kinda sucks.

<figure><p><img src="https://rubenerd.com/files/2024/browsers@2x.jpg" alt="KDE Plasma on my desktop showing Firefox and Chromium running concurrently." style="width:500px; height:150px;" /></p></figure>

For my younger readers, there was a period prior to the late 2000s when it was necessary to run multiple browsers. I used Mozilla Phoenix for all its modern features, and IE when I needed to access school and financial sites. Over time, the likes of Firebird, [Camino](https://rubenerd.com/goodbye-camino/), SeaMonkey, and Firefox began to be supported by major sites, because people started writing to *standards*, not browsers. Okay that's a simplification, but at least sites worked reliably across rendering engines.

Now we're back to those early days of split browsers. *But you can just run IE/Chrome and leave it at that*, I hear you cry. *Phooey*, I say!

As an aside, I've been running [Dillo+](https://github.com/crossbowerbt/dillo-plus) again sometimes, especially if I need fewer distractions, and I know the sites I need to access are written with graceful degradation. It's fast, and I've already tweaked a few of my own sites and projects to work better.
