---
title: "Quick review: cookies.txt for Firefox"
date: "2024-05-04T08:42:37+10:00"
year: "2024"
category: Software
tag:
- firefox
- plugins
- reviews
location: Sydney
---
I had to run `curl(1)` recently for some automated tests, which needed cookies for authentication. I could export these manually from a logged-in session in Firefox, but I wanted something easier.

**[cookies.txt](https://addons.mozilla.org/en-GB/firefox/addon/cookies-txt/)** puts an icon in your toolbar, and gives you the option to export all cookies, or just ones for the current site you're on. The resulting file can be passed to curl, or any other compatible shell tool.

Thanks, Lennon Hill!
