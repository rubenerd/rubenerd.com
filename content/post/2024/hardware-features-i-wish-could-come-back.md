---
title: "Hardware features I wish could come back"
date: "2024-09-03T16:00:57+10:00"
year: "2024"
category: Hardware
tag:
- accessibility
- displays
- features
- phones
location: Sydney
---
* **Smart flip phones with hardware keyboards**. Not a flip phone with a creased OLED, I'm talking something like a Sony CLIÉ with a screen and keyboard on discrete slabs.

* **Headphone jacks**. I know phone manufacturers switched to Bluetooth so they could sell you another consumable with irreplaceable batteries. But some of us love our wired headphones, and don't want yet another dongle to lose.

* **5:4 or 4:3 displays**. I almost never use the edges of widescreen displays. I'd love a HiDPI-grade equivalent of my old 1280 × 1024 display, so my eyes don't have to dart around as much, and I get some desk space back.

* **Translucent hardware cases**. Let's have some whimsy and a bit of fun again.
