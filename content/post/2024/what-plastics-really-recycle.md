---
title: "What plastics really recycle: myths"
date: "2024-04-17T08:15:43+10:00"
year: "2024"
category: Hardware
tag:
- economics
- environment
- health
location: Sydney
---
*(I wanted to take a break from writing about tech today, so here's something random that's been on my mind! Or I suppose, in my mind, given how microplastics are now a thing. Thanks for reading).*

You know when you're a little kid and full of a million questions? My parents kept a magnet on the fridge that said *we're your parents, that's why* in response to why I had to go to bed, brush my teeth, and eat vegetables. Though ideally not at the same time.

The two questions I vividly remember stumping them on&mdash;or at least, stumping them on how to explain&mdash;were how to divide by zero, and where plastic came from. I knew that wood grew on trees, and that metal somehow was dug out of the ground, but plastic? Was there a plastic plant? Are there chunks of it under a layer of peat? *Turns out*, explaining petrochemical extraction, distillation, and processing to a child were more involved than discussing how trees are grown (though that's also far more complicated at scale than you'd think too).

Going to school in the 1990s, I was taught about how crude oil had saved whales from extinction, and that synthetic plastic had prevented widespread habitat destruction, because we didn't need massive plantations to farm natural polymers. Later we learned that plastic also describes a *property*, and the differences between thermosetting and thermoplastic materials. Don't get them confused, or you'll end up with something charred or melted!

We learned about air pollution, and the atmospheric implications of pumping our carbon sinks into the air at industrial scales for a century. *But climate change is a hoax, yo!* However, plastic itself took a back seat in these environmental discussions, because it had an out that coal and gas didn't. I'll bet you got told the same thing.

*Plastic could be recycled!*

Right? I mean, right?

It's hard not to overemphasise just how much recycling was pushed as a solution to plastic during this time. It was up to us, as citizens of the modern world, to sort, clean, and dispose of our recyclable materials thoughtfully. The upside was obvious: we were shown all the cool things that were being made out of our recycled containers we were forced to buy because they stopped selling things made of other material. And I'm sure it burned less diesel driving around PET bottles over glass. And hey, if it ends up in landfill, we're just sequestering carbon, right? (Uh oh, Slashdot is leaking out).

Recently, some important changes have happened. Reductions in the use of polystyrene takeout containers and plastic straws was a good thing, and sparked a wider discussion on disposable plastic use. Six-packs aren't packaged with plastic rings anymore, which fish can become ensnared in. Some of this were recyclable, but it's better not to make the crap in the first place.

But these superficial benefits hid something else. Raw synthetic plastic is still cheaper to produce than recycled, at least in our current regulatory environment. Though even if it wasn't, plastic polymers don't have an infinite recycling capacity unless you physically [process it back](https://www.uow.edu.au/the-stand/2022/the-truth-about-australias-plasticproblem.php) into oil and gas, which is even more of a waste. The upshot of this is we're pumping orders of magnitude more plastic into the environment than we can capture, let alone hope to recycle. Everything from our oceans to [bloodstreams](https://www.sciencedirect.com/science/article/pii/S0160412022001258 "ScienceDirect: Discovery and quantification of plastic particle pollution in human blood") are full of the stuff, and we're only making more of it.

Back in 2022, [REDcycle](https://en.wikipedia.org/wiki/REDcycle) admitted that they had been dumping soft plastic in Australia in lieu of recycling it, and suspended operations. Groceries wrapped in needless plastic had been proudly stamped with the *REDcycle* branding, encouraging shoppers to return their used plastic to the store where it'd be recycled. Clara and I did it! It wasn't a scam in the sense that they knowingly took plastic they had no intention of recycling, but it's clear they grossly overestimated the scale, cost, and complexity in doing it. That's the problem of plastics writ large.

Except, is it? [As CBC News reported](https://www.cbc.ca/documentaries/the-passionate-eye/recycling-was-a-lie-a-big-lie-to-sell-more-plastic-industry-experts-say-1.5735618)\:

> In the '80s, the industry was at the centre of an environmental backlash. Fearing an outright ban on plastics, manufacturers looked for ways to get ahead of the problem. They looked at recycling as a way to improve the image of their product and started labeling plastics with the now ubiquitous chasing-arrows symbol with a number inside. According to Ronald Liesemer, an industry veteran who was tasked with overseeing the new initiative, "Making recycling work was a way to keep their products in the marketplace." 

Clara and I have been very deliberate about the plastic we bring into our home now, to the point where we'll pay significantly more for less food because it doesn't come wrapped in redundant rubbish. We do the whole thing of buying refills instead of newer containers, even though it's often messy or a massive pain. We've swapped hand soap for bars, and stopped buying certain foods altogether.

But not everyone has the privilege of doing this. It's also not something that can be solved with personal responsibility, as we've often been lead to believe. This has to come from the top.


