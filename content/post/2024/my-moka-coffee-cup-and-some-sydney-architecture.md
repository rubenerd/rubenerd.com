---
title: "My Moka coffee cup, and some Sydney architecture"
date: "2024-03-01T11:10:30+11:00"
year: "2024"
category: Hardware
tag:
- architecture
- coffee
- mugs
- reviews
- sydney
location: Sydney
---
*(I've backdated this post to the 1st, because I forgot to publish. The jokes are also a bit weird; clearly this was written too early in the morning, even by my standards).*

In 2022 I started reviewing my [coffee mugs](https://rubenerd.com/tag/mugs/) for a bit of fun. So far I've covered my [Hahndorf Inn mug](https://rubenerd.com/my-hahndorf-inn-hotel-mug/) and [Apothecary mug](https://rubenerd.com/my-apothecary-coffee-mug/). We're mixing things up a bit today by reviewing my current takeaway. As in, the beverage is naturally mixed when I walk around carrying this.

For the longest time I used a *Keep Cup* sold by my beloved [Boatshed](https://www.teagardensboatshed.com/) in Teagardens, a few hours drive north of Sydney. It was nostalgic and worked reasonably well for years, but the lid assembly and rubber sleeves were both a bit of a pain to clean. It also wasn't especially rigid, which made it prone to spilling if I squeezed it when being jostled or bumped on the [morning train](https://en.wikipedia.org/wiki/North_Shore_%26_Western_Line "Wikipedia article on my local train line"). Black coffee and white shirts don't mix, especially when you're off to a client site or meeting.

When the lid finally cracked, it was time to source a replacement. Though I typically prefer drinking coffee out of these things. To think I offer these jokes for *free*, it's a travesty.

I fell down my usual rabbithole of over-research normally reserved for computer and hi-fi equipment, before coming across this handsome but unassuming vessel in a supermarket. It's called a *Moka*, and I'm a convert. Here we are in the office breakroom.

<figure><p><img src="https://rubenerd.com/files/2024/moka-cup@1x.jpg" alt="A tall beige takeaway cup on a table" srcset="https://rubenerd.com/files/2024/moka-cup@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

It's *very* sturdy, so I don't accidentally squeeze it and spray beverage everywhere when people bump me on the train. Well, unless they want that. The ridges on the side prevent the sides getting too hot, without the need for a sleeve, which saves another thing to wash.

The [distributor's website](https://urbanethos.co/products/moka-reusable-coffee-cup), and the sides of the cup itself, claim it's manufactured from repurposed coffee grounds and husks. I'll  be honest (as opposed to normal?), I'm not prepared to take that at face value without seeing some evidence, which I haven't been able to find. But it'd be cool if true; this cup will already save a tonne of plastic entering landfill, but every bit helps.

But the best part of all, especially for a nerd like me? It's as though one of Australia's most important architects [Harry Seidler](https://en.wikipedia.org/wiki/Harry_Seidler) is around with us again and designing coffee cups. Tell me the colour and shape doesn't look like one of his buildings, like that 1970s round structure in the podium area of the MLC Centre in Sydney! Thanks to [Wpcpey](https://commons.wikimedia.org/wiki/File:MLC_Centre_Podium_2017.jpg) for this photo.

<figure><p><img src="https://rubenerd.com/files/2024/mlc-centre-podium@1x.jpg" alt="Photo of the podium area of the MLC Centre tower by Wpcpey" srcset="https://rubenerd.com/files/2024/mlc-centre-podium@2x.jpg 2x" style="width:500px; height:333px;" /></p></figure>

I wonder what coffee Harry drank? I assume he must have, given how prolific he was, and the fact he was born in Vienna. *One day* I'll get to have a Sachertorte and a coffee there. I don't even like cake, but I make exceptions.

Anyway, my new Moka takeaway cup. It's pretty great.

