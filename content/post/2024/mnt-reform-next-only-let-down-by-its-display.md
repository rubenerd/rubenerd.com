---
title: "MNT Reform Next only let down by its display"
date: "2024-12-29T11:32:48+11:00"
abstract: "Sadly, we're still stuck at 1080p like its 2010."
year: "2024"
category: Hardware
tag:
- accessibility
- open-hardware
- pc-screen-syndrome
location: Sydney
---
I love that open, considered hardware like the MNT Reform exists, and the [MNT Reform Next](https://www.crowdsupply.com/mnt/mnt-reform-next) is set to continue this trend. The keyboard, reasonable-sized trackpad, and repairable construction all look *boss*.

Unfortunately, it still posesses a visual Achilles' heel:

> 12.5" matte IPS 1920x1080 eDP panel

It's such a shame! That pixel density is too low for clean 2× HiDPI/Retina&trade; and too small to run at full resolution. It's another case of *[PC Screen Syndrome](https://rubenerd.com/tag/pc-screen-syndrome/)*, and I can only assume it's down to economies of scale; though [Framework](https://frame.work/) ships them now, Apple has for [more than twelve years](https://rubenerd.com/now-obsolete-macbooks-still-have-better-screens/ "Now obsolete MacBooks still have better screens"), and even your *phone* sports more.

But there's good news: the [MNT Pocket Reform](https://shop.mntre.com/products/mnt-pocket-reform) *does* offer sufficient density. If you want a tiny, hackable machine with legible fonts, crisp images, and no awkward visual compromises that come from fractional scaling, this still seems to be the way to go. I can't *wait* to get my hands on one.
