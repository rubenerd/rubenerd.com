---
title: "XFN versus OPML blogrolls"
date: "2024-05-27T12:28:30+10:00"
year: "2024"
category: Internet
tag:
- fediverse
- indieweb
- semantic-web
- xfn
- xml
location: Sydney
---
I've been thinking about blogrolls again, and whether I want to re-implement mine. 🥯

For the last few years I've had a public-facing OPML blogroll again. OPML is the *de facto* format blog readers use to import and export subscription lists, and has attributes for RSS feeds, web links, and tags. Because it's a native outliner format, you can have nested elements to fudge categories as well (though support for that in RSS readers is patchy).

But lately I've been wondering if this is the best approach, for a couple of reasons.

First, OPML is a bit clunky. I'm not proud of the XSLT I wrote to [present mine nicely](https://rubenerd.com/blogroll.opml), and writing JavaScript to parse a plain text file seems ridiculous. I ended up moving my [Omake](https://rubenerd.com/omake.xml) to my own format for reasons I go into on its [namespace page](https://rubenerd.com/xmlns-omake/#comparing-omake-to-opml). They're more academic concerns though.

The lack of semantic data is the larger issue for me. An OPML outline element has no concept of what a link is, beyond differentiating between an `xmlUrl` and an `htmlUrl`. Is the site owned by a friend? Someone you admire? A relative? A project to which you contribute? A podcast?

We'd have called these links and codified relationships your **social graph** back in the day, and there were a few different schemas for expressing them. The easiest, and I think most widely-deployed, was the [XHTML Friends Network](http://www.gmpg.org/xfn/join), or XFN.

As the name suggests, is a phrase with four words. XFN [codified relationships](http://www.gmpg.org/xfn/1) within the existing HTML `rel` attribute, such as `friend`, `met`, `muse`, and `me`. All have prescriptive definitions, and expectations regarding symmetry (a `sweetheart` is expected to be, but a `crush` might not be). You can see how much you can express with just a little bit of metadata, and how a web of friends and family could be easily built.

Open social graph tech largely withered on the vine with the promulgation of walled gardens, but the IndieWeb and Fediverse have brought them back into focus. This is fantastic! But it leads me to think whether my links would be more functional and useful as expressed on a plain HTML page with XFN data, not locked away in an OPML file no rational person would likely import wholesale anyway.

I'm not sure yet, I'll have to think about it.
