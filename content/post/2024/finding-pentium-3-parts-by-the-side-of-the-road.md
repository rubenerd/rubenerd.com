---
title: "Finding Pentium 3 parts by the side of the road!"
date: "2024-07-01T08:17:45+10:00"
year: "2024"
category: Hardware
tag:
- retrocomputers
location: Sydney
---
Clara and I were briskly walking down the street for our ten thousand steps yesterday, when we chanced upon a dischevelled beige computer tower half buried in the mud. Being a retrocomputing tragic, I proceeded to load it into a large shopping bag without a second thought. This lead me to notice a bag of parts sitting underneath the case, which presumably contained all the parts from the machine. Huzzah!

We took the haul back to the apartment, and spent the evening cleaning and taking notes while we caught up on the [Sorted Washed Up live show](https://www.sortedfood.com/live). It was fitting, because this poor machine also looked as though it'd been washed ashore in England somewhere, right down to the rust holes and dents from being smashed against rocks.

The midtower case chassis was too far gone, but the bezel was near flawless, save for a bit of yellowing. I'm collecting these for a nerdy *Das Blinkin Lights* art project, so I unscrewed it and gave it a quick wash in the shower to dislodge the dirt. The rest of the parts were in much better shape; I hit them with some compressed air and IPA, then dried them out with a hair dryer.

<figure><p><img src="https://rubenerd.com/files/2024/saved-p3@1x.jpg" alt="Photo showing all the parts listed below." srcset="https://rubenerd.com/files/2024/saved-p3@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

The machine was based around a [GA-6PMM rev 3.2 motherboard](https://www.gigabyte.com/au/Motherboard/GA-6PMM-rev-32/sp#sp), which Gigabyte still have the manual for! It's a socket 370 for the Pentium III, a VIA chipset, PC-133 memory, and an AGP 4x slot. The user manual lists it as coming out in February 2001.
The other parts include:

* **512 MiB PC-133 DIMM**, unknown brand
* **Samsung DVD-ROM**, 12× DVD-Master SD-612
* **LG CD writer**, 52× CD-R/RW GCE-8525
* **Panasonic floppy drive**, JU-256A888PC, with broken bezel
* **TP-Link PCI NIC**, TF-3239DL, Realtek RTL8139D IC
* **Conexant RS56/SP-PCI fax modem**, model I101 Rev:2X
* **ATAPI and floppy cables**, 80 and 40-pin

It was common for good machines at the time to have a DVD-ROM and a CD writer, before combi drives came out that supported both. Having two optical drives was a boon for making mix CDs, and also if you were playing multi-disc games that could load off both drives. *Titanic: Adventure out of Time* would check both drives for discs when the game loaded, which negated the need for tedious swapping.

Anyway, check out this 2000-era AGP goodness! I used Gigabyte boards for all my teenage builds back in the day&mdash;albeit for Athlon&mdash;so the layout, sockets, and colours are incredibly nostalgic. It even has that weird [AMR slot](https://en.wikipedia.org/wiki/Audio/modem_riser), which I literally never used. Did... anyone?

<figure><p><img src="https://rubenerd.com/files/2024/saved-p3-board@1x.jpg" alt="Close-up view of the board." srcset="https://rubenerd.com/files/2024/saved-p3-board@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

I haven't had a chance to test any of this yet, but I'd be thrilled if even one component worked. If the motherboard is functional, I'm tempted to swap out the [proprietary Dell board](http://retro.rubenerd.com/x86.htm#dell-dimension-4100) with its limited BIOS and weird power connector with it, given the specs are close to identical.

