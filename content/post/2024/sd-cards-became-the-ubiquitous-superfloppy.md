---
title: "SD cards became the ubiquitous superfloppy"
date: "2024-02-24T09:32:16+11:00"
year: "2024"
category: Hardware
tag:
- cds
- iomega
- sd-cards
- storage
location: Sydney
---
Those of you my age or older may remember the Iomega Zip drive. In a sea of competitors from SyQuest, Imation, and Castlewood, Iomega's little *drive that could* was the closest to reach ubiquity, at least in the West (my Japanese friends in Singapore all used those Magneto Optical disks which were super cool in their own way). There was a period in the late 1990s where they were optional add-ons to everything from PowerMacs to laptops.

The term *superfloppy* was popular among the tech press for describing such devices, in part because regular 3.5-inch floppy disks were also still in widespread use. It was a bit of a chicken and egg situation: people used 3.5-inch floppies because they were ubiquitous, and they were ubiquitous because people kept using them. But while they offered huge benefits over the *literal* floppy disks they replaced, they were frustratingly limited by the mid to late 1990s. My homework assignments rarely fit on one, and I was often helping my old man use a tool like WinZip to split projects across multiple disks to pass to clients.

I remember two technologies were credited for the downfall of the *superfloppy*, both of which you may still read in retrospectives:

* CD writers, which offered higher-capacity media at a lower price than proprietary storage disks.

* Machines with newfangled USB ports, which could take advantage of USB keys and card readers without mechanical drives at all.

I always thought the first was overstated. Sure, a blank CD (or DVD) had capacity in excess of a *superfloppy*, and economies of scale meant they could be stamped out *en masse* for less than a complicated *superfloppy* cartridge. But their operation was completely different.

Floppies were CRUD devices (create, read, update, destroy). CDs were mostly WORM devices (write once, read many). CD-Rs required time-consuming authoring with specific software, and couldn't be modified. Multi-session CD-Rs could have data appended, and CD-RWs could be erased and re-written, but neither offered the flexibility of a floppy, outside specific use cases like data backups. DVD-RAM discs offered a more CRUD experience, but they were slow, error prone, expensive, and not as compatible. But boy did I try!

USB keys were more compelling. The data they contained *could* be CRUD'd. While their initial capacities didn't match Zip disks, and they didn't have the same cost advantage of CD-Rs per unit, economies of scale and advances in silicon density soon brought these down. When we could assume a destination machine had a USB port, and that it's OS could handle live attached storage, the world changed. USB keys remain a popular sneakernet choice today, with capacities well in excess of even Iomega's professional Bernoulli, Jaz, Rev, or Peerless disks; three of which even used hard drive platters.

<figure><p><img src="https://rubenerd.com/files/2024/superfloppy@1x.jpg" alt="A small collection of various disks and cards." srcset="https://rubenerd.com/files/2024/superfloppy@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

But I think SD cards deserve the modern moniker of *superfloppy*. Yes I know they're *cards* with no circular platters or plastic; but tell that to the designers of the [SD card logo](https://en.wikipedia.org/wiki/SD_card#History "Wikipedia article on the SD card, with details on the origins of the SD logo")!

Like the Zip drive, SD cards and their tiny bretherin entered a crowded market of SmartMedia, Sony Memory Sticks, xD-Picture cards, and even MMC upon which it was based. Professional settings have moved from CompactFlash to XQD, but the SD card is the *de facto* standard from video cameras and routers, to embedded computers and emulators. Need a removable storage slot on your device or project? It's going to be an SD card.

Certain companies would have you believe wireless is the future, and that removable storage is a dead-end technology. Firstly, have these people ever *used* flaky tech like Bluetooth before? I kid, but there's clearly still growing demand for physical cards. While I'd have preferred MMC win out, I'm oddly relieved there's a still a storage format for people who live in the real world.

I don't think I've appreciated that enough. I lived my life on those Iomega disks as a kid. I would have been *thrilled* at the idea of having a little case full of high capacity, widely-supported cards I could format and carry around with me everywhere. Though I likely would have lost them; but that's a topic for another time.
