---
title: "Neil on the hope in Star Trek"
date: "2024-01-08T12:30:16+11:00"
year: "2024"
category: Media
tag:
- star-trek
- twitter
location: Sydney
---
[Via Schnitter](https://twitter.com/neilkid75/status/1742250669502005648)\:

> For me, something about the Vulcans provides a hopeful quality. If we met them, we’d be better for it. First Contact expresses this hope. “It unites humanity in a way no one ever thought possible. When they realize they're not alone in the universe…”

🖖
