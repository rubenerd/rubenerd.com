---
title: "Trying out the Kagi search engine"
date: "2024-04-08T15:10:09+10:00"
thumb: "https://rubenerd.com/files/2024/521303.jpg"
year: "2024"
category: Ethics
tag:
- generative-ai
- search-engines
location: Sydney
---
I just created an account, and got a welcome email. Cool!

> Hi there,
> 
> Thanks for trying Kagi!
> 
> To get the most out of your trial, consider:
>
> Kagi is a little different from other search engines. Get familiar with our unique features with this Quick Start.

I'm sold so far.

> Your Kagi subscription also includes unlimited access to AI tools like FastGPT and Universal Summarizer.

<figure><p><a href="https://frinkiac.com/caption/S08E11/"><img src="https://rubenerd.com/files/2024/520886.jpg" style="width:320px;" alt="Open your bag of ingredients... ugh." /><br /><img src="https://rubenerd.com/files/2024/521503.jpg" style="width:320px;" alt="Check for... GPT." /></a></p></figure>
