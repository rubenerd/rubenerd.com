---
title: "Buying a matte screen protector"
date: "2024-04-21T07:50:37+10:00"
year: "2024"
category: Hardware
tag:
- ergonomics
- phones
location: Sydney
---
I prefer matte displays over glossy, on both phones and laptops. Colours don't *pop* like they do behind glossy, which is probably why they've been disappearing on consumer-facing hardware. But matte displays are easier to calibrate, and diffuse light rather that reflect it, which makes them more useful in a wider range of areas. You know, like you'll find when using a phone or laptop.

But I guess I do need to remind myself that, surprising nobody, I'm a bit of a contrarian in this case. Most people either prefer glossy, or don't think twice about it. It's what their mobile device came with, and that's that.

Case in point, is a phrase with three words. I went to get a new glass screen protector for my phone yesterday at one of those repair shops, because they apply them better than I can with my shaky hands. I asked if they had any matte protectors in stock for my specific model, and the owner laughed saying that "of course he did" because "nobody buys these!"

Surprisingly though, I joked if I was the youngest person to buy matte protectors in general, but he said they were popular with parents to put on iPads for their kids. He had to tend to another customer, so I didn't get a chance to ask him why he thought that was.

This has been your annual report on the benefits of, and demand for, matte displays. Tune in next year to hear why a guy called Matt prefers them.

