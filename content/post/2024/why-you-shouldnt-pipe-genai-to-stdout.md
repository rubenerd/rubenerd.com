---
title: "Why you shouldn’t pipe genAI to STDOUT"
date: "2024-06-15T07:21:02+10:00"
year: "2024"
category: Internet
tag:
- funny
- generative-ai
- spam
location: Sydney
---
<figure><p><img src="https://rubenerd.com/files/2024/gpu-spam@1x.jpg" alt="A screenshot showing a prompt output from a summary tool" srcset="https://rubenerd.com/files/2024/gpu-spam@2x.jpg 2x" style="width:500px; height:300px;" /></p></figure>
