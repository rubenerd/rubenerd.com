---
title: "Not using PostgreSQL stored procedures?"
date: "2024-07-18T08:31:25+10:00"
year: "2024"
category: Software
tag:
- data
- databases
- postgres
location: Sydney
---
Raphael De Lio recently wrote about the feasibility of [using Postgres in lieu of Redis as a cache](https://medium.com/redis-with-raphael-de-lio/can-postgres-replace-redis-as-a-cache-f6cba13386dc). This bit stood out for me though:

> Truth is that most modern applications don’t rely on Stored Procedures anymore and many software developers advocate against them nowadays.
>
> Generally, the reason for that is because we want to avoid business logic leaking into our databases. Besides that, as the number of stored procedures grows, managing and understanding them can become cumbersome.

Is this true? Admittedly I haven't been a DBA for a number of years now, but I was always told it's better to offload procedures to the DB if you can, for performance and consistency.

The business logic angle is also interesting. We all had the MVC framework drilled into us, but there's still a tonne of meaningful processing and data collation one can *always* do faster in the DB. Such processing doesn't rely on understanding how the data is used; you're just returning an optimised dataset for a specific query or use case.

*(I've also always argued that business logic is inescapably baked into the structure of your tables and data as well, but I know others disagree).*

I'm of the opinion that stored procedures are an underappreciated and underutilised tool, but they scarcely warranted a mention in any of the development courses at either university I went to. Maybe that's part of the problem.
