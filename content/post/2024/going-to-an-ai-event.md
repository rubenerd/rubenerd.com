---
title: "Takeaways from an AI event"
date: "2024-06-17T23:51:15+10:00"
year: "2024"
category: Thoughts
tag:
- generative-ai
- spam
location: Sydney
---
I went to an AI industry event last week, and there were some interesting takeaways.

First, surprising none of you, we are well and truly in the AI hype cycle. Everything that has even a modicum of electronic processing and delivery is being called "AI", to the point where the term is meaningless. I mean, it was a vague umbrella term that data scientists and researchers never used anyway, but now that marketers have got a hold of it... my electric toothbrush could claim to have AI in it now.

Wait, let me get it right. *Clears throat*. We're witnessing an oral hygiene revolution. Introducing the **AI brush**. It uses Deep Learning Technology&trade; to sense when too much pressure is being applied and stops the brush spinning. Its advanced AI Timing&trade; can also report when you've brushed for a sufficient time, which it reports to an App&trade; on your phone! Your medical insurance provider can then be alerted to raise your premiums if you... wait, sorry, I wasn't supposed to read that bit.

There are a lot of compelling use cases for specific applications, especially when it comes to primary industry, medicine, and edge devices. Being able to identify and report on patterns that a human can act upon and validate is useful. Again, this is all stuff computers have been doing for years (albeit perhaps at a slower speed or with less processing power), but it's called AI now. The term is cringe, but the technology is impressive, and I should discuss it more.

Generative AI is still grim, but there are encouraging signs that people are waking up to it. One presenter talked about a tool that could write "content" for you based on a sentence, then breathlessly pivoted to an auto-summariser. A gentleman asked why we're wasting so much power generating then summarising material, when we could cut out the AI and just deliver the original summary. This is the closest the event got to discussing the environmental impact of these tools, which was a bit disappointing.

Another idea presented was the "AI Sandwich", where humans provide the input, and QA on the other side. For all the breathless talk about how generative AI was going to usurp professionals and change the world, this is probably the most realistic outcome of these tools given their output can't be trusted.

Privacy and security was another big pitch. Someone at the meeting asked about concerns regarding deep fakes, which another person in the audience answered, and I swear this is true, *trustless blockchain*. Those of us based in reality still had an interesting discussion, especially when it came to validating people and rebuilding trust.

But finally, and most interesting to me, was how data were discussed. A vendor argued that remote services can't be trusted, because you're giving up your valuable data to train someone else's model. This is encouraging; large, general purpose generative AI tools can only be built from data harvested without knowledge, permission, attribution, or compensation. Nobody in industry cared when it was individual artists or writers affected, but if it's a *business* issue!? Shut the floodgates!

Cynicism aside, I got an inkling from this event that people are starting to take these issues seriously, which is encouraging. Had this event happened a year or so ago, people would still probably be talking about how great ChatGPT is, without any introspection. It's a sign that while the marketing people are still buying their own hype about the metaverse, sorry blockchain, sorry AI, the rest of the industry is at least starting to think.
