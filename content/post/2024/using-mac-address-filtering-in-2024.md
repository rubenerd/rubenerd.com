---
title: "Using MAC address filtering in 2024"
date: "2024-12-05T11:18:19+11:00"
abstract: "You almost certainly shouldn’t do it. But its utility combined with WPA isn’t zero."
year: "2024"
category: Internet
tag:
- networking
- security
location: Sydney
---
Should you filter devices by MAC address on your Wi-Fi access point and/or network? **Almost certainly not**. But while I'll probably get in trouble for admitting it, Clara and I do. We have for years, and it works fine for us.

MAC address filtering is considered a Bad Idea&trade; for these reasons:

* They're transmitted in plaintext, so an attacker can sniff MAC addresses in use, send a disassociation request, and spoof it. This is feasible for literally anyone, whether they're a seasoned infosec professional, or the proverbial grandparent who clicked the wrong network by mistake in a crowded apartment building, and has been typing the wrong password into it over and over again.

* It sounds too much like Mac address filtering, which is only effective against AppleTalk and Bonjour. Thank you.

* WPA makes any additional MAC address filtering redundant, just like WEP did back in the day. Security is famously about avoiding layers.

* It's insufficient to just use MAC address filters, just as seatbelts don't negate the need to respect stop signs.

* Maintaining lists of permitted MAC addresses is time consuming, because everyone performs the same cost/benefit analysis against the same requirements and constraints.

Yes, I'm half joking. People who provide this advice have a point: MAC address filtering is the infosec equivalent of posting a *keep off the grass* sign. If it's complicated or frustrating to implement, it's not worth your time. And if it's your *only* form of network protection, you're in massive trouble.

Thing is, it isn't for us. Clara and I maintain a personal DB which includes all sorts of data, from budgets to media collections. One of the tables tracks our assets, including serial numbers, dates of purchase, warranty information, hostnames, ZFS pools, and... MAC addresses. This is feasible because our fleet remains fairly static; we're not buying new hardware every day. And wouldn't you know it, those MAC addresses get added automatically to our filters with a few lines of shell script. It was trivial to do, I implemented it and didn't think about it again.

Based on logs since we moved into this apartment a month ago, the following connections have been dropped based on...

* MAC address: 18
* WPA authentication failures: 0

... huh! I'd consider that a worthy return on minimal investment.

This is a great example of the [xkcd security problem](https://xkcd.com/538/). We build up this image in our heads of competent, malicious people being foiled (or not) by our stunning technical prowess. Someone sufficiently motivated to break into us will not be stopped by a MAC address filter; they'll have to contend with WPA. In the real world, the majority of unauthorised connection attempts are far more mundane.

Some people ignore *keep off the grass* signs, either out of defiance or convenience. But here's the thing: they're easy to install, and some people *do* respect them. I have numbers showing this to be the case with our digital equivilent.

In conclusion, you still probably shouldn't do it. WPA is fine. But as part of a range of measures to reasonably protect a home network? Sure, go for it. Just don't make the same mistake I just did and tell everyone.
