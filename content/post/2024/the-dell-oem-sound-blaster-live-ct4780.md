---
title: "The Dell OEM Sound Blaster Live! CT4780"
date: "2024-08-12T07:16:38+10:00"
thumb: "https://rubenerd.com/files/2024/sblive-oem-dell@1x.jpg"
year: "2024"
category: Hardware
tag: 
- dell-dimension-4100
- retrocomputers
- music
- music-monday
- sound-cards
location: Sydney
---
Today's *[Music Monday](https://rubenerd.com/tag/music-monday/)* is less about a specific song, and more about some weird hardware you may have been forced to figure out how to play music on back in the day. **TL;DR**, [try these drivers on Archive.org](https://archive.org/details/sound-blaster-sb0100-retail-disc).


### The Dell Dimension 4100

I bought a beigetastic [Dell Dimension 4100](http://retro.rubenerd.com/dell4100.htm) from eBay last year, and it's been a lot of fun. While I bought it to relive nostalgia from my high school computer labs, it's turned out to be the most capable, expandable retro PC I own. Just look at that retro beige goodness!

<figure><p><img src="https://rubenerd.com/files/2024/dell4100@1x.jpg" alt="Photo of the beige Dell Dimension 4100" srcset="https://rubenerd.com/files/2024/dell4100@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

But its generic design hid a more proprietary machine than I realised. The power supply pinout differs from ATX, requiring an adaptor or custom pin block to replace it. The [Intel D815EEA](https://theretroweb.com/motherboards/s/intel-d815eea-easton) motherboard was modified to remove the onboard sound, game port, and one of the SD-RAM slots.

Weirder still are its expansion cards. The provided ATI Rage 128 looks just like a regular ATI AGP card, but its Dell firmware means standard ATI Catalyst drivers don't work. As of 2024, Dell still [make these available](https://www.dell.com/support/home/en-au/product-support/product/dimension-4100/drivers) on their site though, which is a relief.

<figure><p><img src="https://rubenerd.com/files/2024/sblive-oem-dell@1x.jpg" alt="Photo of the unassuming Dell Sound Blaster Live! CT4780" srcset="https://rubenerd.com/files/2024/sblive-oem-dell@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

### Dell's strange sound cards

Which leads us to the curious case of the *Sound Blaster Live! CT4780*. From outward appearances, this looks like a standard Creative PCI sound card of the era, complete with gold *Sound Blaster* branding in the corner, a standard Creative model number, and the usual assortment of connectors:

But in the musical words of Eurythmics, *there's just one thing*. This is a stripped-down Dell OEM version of a *Sound Blaster Live! Value* card. Like the ATI Rage card, standard Creative drivers don't work, nor can Windows 98 detect it out of the box.

Finding drivers for this card was like extricating that needle from the proverbial haystack. The Dell driver site provided an installer that flat out didn't work, and *dozens* of Creative drivers and installers didn't either. Based on these [Dell](https://www.dell.com/community/en/conversations/desktops-general-locked-topics/soundblaster-live-value-digital-drivers-ct4780/647e4060f4ccf8a8dea2c64e "SoundBlaster Live Value Digital Drivers (CT4780)"), [Vogons](https://www.vogons.org/viewtopic.php?t=43244 "Vogons: Sound Blaster LIVE! Versions"), and [Reddit](https://www.reddit.com/r/retrobattlestations/comments/9d9h5j/help_looking_for_windows_98_se_drivers_for_a_sb/ "[HELP] Looking for windows 98 SE drivers for a SB Live Ct4780 sound card.") threads, I wasn't alone. The conventional wisdom seems to be *e-waste these cards and get better ones*.

Eventually I came across a [driver ISO on Archive.org](https://archive.org/details/sound-blaster-sb0100-retail-disc) for the Sound Blaster CT4780. I had low hopes for it working, given it was listed as a *retail* disc. But to my absolute **shock**, Windows 98 SE rebooted after installing the drivers, and I had sound. *Shiok!*

<figure><p><img src="https://rubenerd.com/files/2024/sb-4100.png" alt="Screenshot of my Dell Dimension 4100 with the Sound Blaster software and Winamp running" style="width:512px; height:384px;" /></p></figure>


### Conclusions

If you have one of these cards, first evaluate how much you value your time, and if you can make the problem go away with a better card for not much money. If you're a nostalgic fool like me who can't let retro hardware go by the wayside, keep trying drivers until one works. That ISO I linked to above worked for mine, but your mileage may vary.

I've noticed these sound cards come up on eBay fairly frequently, which must be incredibly disheartening if you accidentally buy them and can't get them working. My advice is to avoid them if you can. But if you get one by accident, try those drivers.
