---
title: "Being second with physical media"
date: "2024-08-22T07:20:44+10:00"
year: "2024"
category: Hardware
tag:
- business
- iomega
- retrocomputing
- tapes
location: Sydney
---
Here’s a shower thought! There's a real advantage coming second with physical media.

Coming to market before anyone else, often dubbed the *first-mover advantage*, gives you the opportunity to frame a product category, define parameters, patent key components, set the price, and reap the financial reward of having a captive audience... at least, until your competitor enters the market.

To use an aircraft analogy, they may have come to market first due to their own innovation and timing, like Boeing did with the 707 over the DC-8, or because their competitor was late, as Lockheed ended up being with the Tristar compared to the DC-10. The end result was the same.

But there’s a risk inherent in dealing with real-world constraints. Selling first requires an understanding of the market, which is tricky. Meanwhile, a competitor can merely compete by upping the capacity or performance of the incumbent, even if they don’t undercut them on price. Betamax tapes had their runtime based on what Sony thought was reasonable for home use, but JVC could simply offer a longer tape than Betamax with its VHS format.

That’s not to say a potential usurper has it easy in other respects. They may underplay their hand in responding to a competitor by not offering a sufficiently-compelling alternative. The Zip disk defined the mass market *superfloppy* format, at least in the West (Bernoulli and Jaz disks were professional devices, and MO disks were never as popular overseas as they were in Japan). Imation’s LS120 SuperDisk offered slightly more capacity, but it was too little, too late to move the market away from Iomega until both formats petered out with USB keys and CD writers.

*(The irony was Iomega originally pioneered the "floptical" system upon which the LS120 was based! That whole era of personal computer storage is fascinating; I might do a pointless deep-dive here one day).*

The history of disks, chips, tapes, and cartridges are filled with examples of companies getting this right and wrong.

