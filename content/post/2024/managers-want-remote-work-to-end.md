---
title: "Managers want remote work to end"
date: "2024-08-09T11:28:14+10:00"
year: "2024"
category: Ethics
tag:
- culture
- work
location: Sydney
---
[Elias Visontay wrote in The Guardian Australia](https://www.theguardian.com/australia-news/article/2024/aug/09/australian-employers-want-staff-in-the-office-but-for-many-workers-home-is-where-the-heart-is)\:

> The NSW executive director of Property Council, Katie Stevenson, called the move from Minns a “gamechanger”. 
> 
> “More workers means more life, more investment and more business for our cities,” she said

People were spending where they live, and will now be forced to shift spending to the CBD, where everything is more expensive. Will they be paid more to compensate? The answer may surprise you!

> The executive director of Business Sydney, Paul Nicolaou, said the move [to encourage working from offices] would increase foot traffic in the CBD.

In the words of moral philosopher Ariana Grande: *yes, and?* Why should workers be expected to subsidise a specific part of a city? If they want that, build more housing in those areas, like Melbourne has.

> Akshay Vij, an associate professor of business at the University of South Australia, has researched Australian workers’ attitudes to remote working flexibility.
>
> “There’s less organisation loyalty in places where there’s not a sense of team spirit,” he said. “Without camaraderie, workers don’t feel as tied to an organisation, which is a concern for management in terms of attrition and retention.

And yet, how do they explain successful companies with 100% remote workforces? I call shenanigans!

Managers need to understand that it's (a) culture, (2) flexibility, and (Γ) pay that move the needle. Some people, like me, prefer being in an office a couple of days a week. Others don't. How you respond to that reality determines how well you'll retain staff. It's as simple as that.
