---
title: "Why coffee shops are magic (to me)"
date: "2024-06-11T16:15:52+10:00"
year: "2024"
category: Thoughts
tag:
- coffee
- life
location: Sydney
---
Back when I was at uni in Adelaide, there was a gentleman in my dorm who *loved* hip hop. Posters of the music adorned his room, and you could always hear the muffled thumping of his speakers through the walls. One day I knocked on his door, and asked if he needed anything from the supermarket. To my surprise as the door swung open, he was buried in textbooks! I couldn't believe someone could study in that loud environment, but he said that specific beat "did something" to let him concentrate.

That's a bit like me with coffee shops. Since I was a teenager, they've been the primary place where I think and write. Most blog posts I've written here since 2004 have been from a handful of coffee shops in the place where I lived at the time, to the point where I've been tempted to track these locations in metadata.

It was borne of necessity at the start. Singaporean apartments are small, so it's *very* common across the city-state to see hoards of students studying and hanging out in a [third place](https://en.wikipedia.org/wiki/Third_place) where they can also get a drink or snack. This clearly imprinted on me.

I don't have any concrete explanations for why, but I have some theories.

Humans are pack animals, but I'm also an introvert. There's something reassuring about being surrounded by people with whom you otherwise don't need to engage. I can say hello to the barista, sit down, and rent a table for a couple of hours for the cost of my favourite drink. A common misconception about introverts is that we all enjoy being lonely. Some may, but I certainly don't.

That gets to why I find the environment comforting, but it still doesn't address productivity and creative output. I thought the CityNerd himself put it well [in October last year](https://rubenerd.com/citynerd-on-working-from-coffee-shops/ "CityNerd on working from coffee shops")\:

> If you follow me on The Apps, you probably know I really like working in coffee shops. I like the buzz and human activity. And something about being under the watchful eye of complete strangers, and imagining they’re judging me rather than completely ignoring me (which is much more likely), well it really helps keep me on task.

[Coffee Magazine](https://magazine.coffee/blog/1/5370/writers-who-did-their-best-work-in-cafes "Writers who did their best work in cafes") compiled a list in 2018 of writers who did their best work from coffee shops, including Ernest Hemingway:

> “The marble-topped tables, the smell of café cremes, the smell of early morning sweeping out and mopping and luck were all you needed.” – A Moveable Feast

I wouldn't compare myself to one of American's greatest writers! But I share in his sentiment. It's a mix of things: the background chatter, the smells, the sights, and a blank text editor. I can absolutely see why someone not attuned to such things would find them distracting and frustrating. But for me there's no better place to think, code, and write. As the English idiom goes, *to each their own*.
