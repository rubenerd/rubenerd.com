---
title: "Ganggajang, Sounds of Then"
date: "2024-09-30T09:50:51+10:00"
year: "2024"
category: Media
tag:
- australia
- music
- music-monday
location: Sydney
---
Today's *[Music Monday](https://rubenerd.com/tag/music-monday/)* is a song about music, which I'm uploading on a Monday. What are the odds!? Don't answer that.

I heard this absolute pinnacle of Australian 1980s music on the radio this morning, and consider it my patriotic duty to repost it. It was the first time I saw the actual music video too. Can something be *too* 80s?

<figure><p><a target=_BLANK href="https://www.youtube.com/watch?v=IfcAlBK2mVk" title="Play Ganggajang - Sounds of Then - Official Video 1985 - 4K Remastered"><img src="https://rubenerd.com/files/2024/yt-IfcAlBK2mVk@1x.jpg" srcset="https://rubenerd.com/files/2024/yt-IfcAlBK2mVk@1x.jpg 1x, https://rubenerd.com/files/2024/yt-IfcAlBK2mVk@2x.jpg 2x" alt="Play Ganggajang - Sounds of Then - Official Video 1985 - 4K Remastered" style="width:500px;height:281px;" /></a></p></figure>

