---
title: "Some Proton Saga Hot Wheels"
date: "2024-07-14T09:53:53+10:00"
thumb: "https://rubenerd.com/files/2024/saga-vending@1x.jpg"
year: "2024"
category: Travel
tag:
- cars
- malaysia
- nostalgia
- toys
location: Sydney
---
The [Proton Saga](https://en.wikipedia.org/wiki/Proton_Saga) is less a motorised, quad-wheeled vehicular conveyance as it is a cultural icon. Introduced as Malaysia's first domestically-produced vehicle in 1985, the Saga went on to power much of the country's taxi and private car fleets. The former meant I got to know them well when I lived there.

Fast forward to 2024, and Clara and I were wandering back from our local Aldi having bought ingredients for dinner. We passed a couple of new vending machines selling Hot Wheels of all things, when I did a double-take and looked at the display.

<figure><p><img src="https://rubenerd.com/files/2024/saga-vending-side@1x.jpg" alt="Side view of the vending machine as we walked past." srcset="https://rubenerd.com/files/2024/saga-vending-side@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

There, among the sports cars, tanks, and heavy machinery, was the original *Proton Saga!* It was even in the popular red colour!

<figure><p><img src="https://rubenerd.com/files/2024/saga-vending@1x.jpg" alt="Up-close photo of the Saga in its original packaging inside the vending machine." srcset="https://rubenerd.com/files/2024/saga-vending@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

As a reminder, I'm an urbanist who's not a fan of cars. But how could I pass up this specific one?! Don't answer that. Here it is with my latest Malaysian snack with [pandan](https://en.wikipedia.org/wiki/Pandanus_amaryllifolius), the world's single greatest flavour.

<figure><p><img src="https://rubenerd.com/files/2024/saga-pandan@1x.jpg" alt="The unpackaged Proton Saga next to a Malaysian snack with pandan." srcset="https://rubenerd.com/files/2024/saga-pandan@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>
