---
title: "What’s with the macOS Bin?"
date: "2024-03-25T07:33:34+11:00"
year: "2024"
category: Software
tag:
- apple
- bugs
- macos
location: Sydney
---
Apple introduced localisation for things like the *Trash* recently, so those of us in the Commonwealth now have a *Bin*. That's quite nice. But the functionality has also been taken to the curb. Ooh, I'm proud of that line.

Is it a true synonym though? My impression was that *trash* referred to what you put *into* something, not the recepticle itself. I don't put *bin* into something, I put *rubbish* into a *bin*. Does this mean Apple has been using the wrong term this whole time? Maybe someone from North America can straighten me out.

In the words of former Australian prime minister Tony Abbott, *synonym, that's my favourite flavour!* Okay that's a bit stolen from Robin Williams referring to Bush Jr. I miss Robin something fierce. What a beautiful human being. *But I digress.* I used to have a graphic in the 2000s for whenever I'd get distracted in a post; maybe I need to ask Clara to draw me something. But I digress... again.

Multiple Macs, across three versions of macOS now, have fundamental issues with a feature going back to the original System software in the 1980s. It either sits there emptying for days at a time, or complains that it can't be emptied because it's being emptied even though I haven't told it to empty. I thought it had something to do with unmounted shares, but it also happens on a clean restart.

<figure><p><img src="https://rubenerd.com/files/2024/macos-bin-borked.png" alt="Bin can't be opened right now because it's being used by another task, such as moving or copying an item or emptying the Bin. Try again when the current task is complete." /></p></figure>

For comparison, the Trash on KDE on my FreeBSD desktop has never not emptied. Hey, double negatives! I don't think I ever had that issue with the Recycle Bin on Windows either, or Gnome and Xfce running on Linux or NetBSD either, now that I think about it. It used to crash *constantly* when emptying in my Windows 7 VM at the time, but it's always emptied after a restart. Funny how Windows people said 7 was *the good one*, which I always thought was a weird way to pronounce *2000*.

I've started making a folder on the Desktop that I liked to on the Dock that I drag stuff I want to delete, and clear periodically from the Terminal.
