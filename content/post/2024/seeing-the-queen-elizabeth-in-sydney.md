---
title: "Seeing the Queen Elizabeth in Sydney!"
date: "2024-12-12T09:57:51+11:00"
abstract: "It was such a stunning day yesterday, I walked across the Harbour Bridge."
year: "2024"
category: Travel
tag:
- cunard
- ocean-liners
- sydney
location: Sydney
---
It was such a stunning day yesterday, I walked across the Harbour Bridge with a wrap for my lunch break. And hey look, a Cunard ship!

<figure><p><img src="https://rubenerd.com/files/2024/qe-sydney-1@1x.jpg" alt="" srcset="https://rubenerd.com/files/2024/qe-sydney-1@2x.jpg 2x" style="width:500px; height:375px;" /><br /><img src="https://rubenerd.com/files/2024/qe-sydney-2@1x.jpg" alt="" srcset="https://rubenerd.com/files/2024/qe-sydney-2@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>
