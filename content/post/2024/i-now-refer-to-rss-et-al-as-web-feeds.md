---
title: "I now refer to RSS et.al. as web feeds"
date: "2024-09-24T08:01:32+10:00"
thumb: "https://rubenerd.com/rss.svg"
year: "2024"
category: Internet
tag:
- rss
- web-feeds
location: Sydney
---
*Web feed* isn't a great term either, but it's a marginally better one:

* "Web feed" at least hints to its functionality. I had no idea what the orange XML rectangle meant when I first saw it in the early 2000s, and even then, I wondered why it didn't say "RSS". 

* Tragic XML fans like me aside, nobody knows or cares about the intricacies and differences between RDF/RSS 1.0, or Atom 0.3, or RSS 4.0 that a chatbot claimed Dave Winer invented.

* Feed your brain... with a web feed! 🧠

* Our aggregators are good enough to detect, parse, and present a range of feeds, regardless of their delivery mechanism. That's *awesome*.

I also think the [Mozilla feed icon](https://commons.wikimedia.org/wiki/File:Feed-icon.svg) is the closest we've come to a universal symbol for these technologies, so I'm using it again.

