---
title: "SSD cost versus spinning rust"
date: "2024-11-30T08:38:21+11:00"
abstract: "Hard drives are still more cost-effective than SSDs for bulk storage."
year: "2024"
category: Hardware
tag:
- storage
location: Sydney
---
Back in 2022 I talked about my [dream all-flash NAS setup](https://rubenerd.com/a-home-nas-of-the-future/ "A home NAS of the future"). I say *dream* because it literally was. Some people have cool dreams; I dream about storage. I can't recall if I also dreamed about OpenZFS, though I can't imagine wanting to run anything else.

Clara's and my homelab will need a storage upgrade on the sooner side, so I started looking into what was available. I've always looked to hard drives first, but this time I was interested to see whether it's finally cost effective to supplement hard drives with SSDs for bulk storage. SSDs on the desktop have been the default for years, but I'd love the space and noise savings in our little servers, not to mention the potential for improved random-read performance.

The first thing I saw was that hard disks are now available in *24 TB*. Here are some Western Digital UltraStar drives at various capacities at a local Australian retailer, and the rough price per reported TB:

* **24 TB:** $1149 ($47.88)
* **22 TB:** $979 ($44.50)
* **20 TB:** $839 ($41.95)
* **18 TB:** $789 ($43.83)
* **16 TB:** $649 ($40.56)
* **14 TB:** $549 ($39.21)
* **12 TB:** $479 ($39.92)

The price scales more linearly than I expected, with the 20 TB being the best value at the high-end.

Now let's check SSDs. Again, these are for bulk SATA storage:

* **Samsung 870 QVO 8 TB**: $899 ($112.38)
* **Samsung 870 QVO 4 TB**: $469 ($117.25)
* **Crucial MX500 4 TB**: $429 ($107.25)
* **Sandisk Ultra 4 TB:** $389 ($97.25)

Well there you have it: the promised inflection point where SSDs are cheaper per TB than hard drives still hasn't happened. At least, not at the scale where mere mortals like us buy them; I know at work we get bulk discounts for SAS and NVMe.

Our 8 TB drives are approaching a decade old, so we'll probably need to cycle them out soon. It'd be *awfully* tempted to swap them out for some SSDs, but I think we have other priorities for that money.
