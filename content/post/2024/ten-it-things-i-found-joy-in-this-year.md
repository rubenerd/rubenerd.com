---
title: "Ten things in tech I found joy in during 2024"
date: "2024-12-28T09:55:15+11:00"
abstract: "Includes Python 3, creative people online, modern hardware on retrocomputers, XML with coffee, social media, NetBSD and Alpine Linux, boutique hosting, repair approachability, and feedback."
year: "2024"
category: Hardware
tag:
- artists
- feedback
- html
- python
- retrocomputing
- vtubers
- xml
location: Sydney
---
Neil Brown posted about what [brought him joy in computers and the Internet this year](https://neilzone.co.uk/2024/10/10-things-i-find-joyful-about-computers-and-the-internet-in-2024/). I agree that it could be a [cathartic exercise](https://mastodon.neilzone.co.uk/@neil/113724657513032721) among the gloom and doom, so I thought I'd give it a shot as well.

As Neil qualifies, we are both lucky people, and these items should have "for us" appended. If you have different perspectives, that's also fine.


### 1. Learning Python 3 has been fun

Learning Python has been on my forever pile for years. Part of this was down to Python 2 not really clicking for me the way Perl and Ruby did. It didn't feel as flexible as the former, or as elegant as the latter.

Python 3 nails it for me. It's been *so much fun!* I build myself a `dict()` for what I want to process, write some simple code, and I have something functional I can use and refine.

Best of all, it's also reinvigorated my interest in learning more broadly, not just with scripting languages. I've *learned* to seize that whenever I find it.


### 2. The Web is full of creative people

This year saw Michael W. Lucas publish another in his long-running Mastery book series. I follow some incredible bloggers, digital artists, vtubers, craftspeople, PCB designers, and video editors. I read, listen to, watch, and explore their creativity, and I can't help but be inspired.

In spite of my industry's best efforts to smother their hope and talent with genAI smoke and slop, there are still people who pour their hearts and souls into making the world more beautiful and fun. This deserves celebration.

I need to tell more of them that their work means a lot to me.


### 3. Modern hardware on retrocomputers is awesome

It's no secret how much I love retrocomputers, but in recent years I've been *bowled over* by the ingenuity and creativity of people in the space, and the accessibility of fabs that can turn their gerber and CAD files into something tangible. Couple that with the ubiquitous RP2040 or ESP32, and it almost feels like nothing is out of reach.

My Apple IIe, Atari 1040 STe, Commodores, and my vintage PCs can all sing and dance to tunes their original designers could have only dreamed of. They have better storage, modern video output, more memory, and even Internet access!

My favourite category this year was consolidated expansion cards that solve multiple problems in ingenious ways, like the [PicoMEM](https://github.com/FreddyVRetro/ISA-PicoMEM). Too fscking cool.


### 4. XML and coffee, together at last

The death of XML has been predicted for years; and yet it's still here, in all its validated and YAML-free goodness. I keep finding uses for it, and word has since got around that I'm one of "the guys" to ask questions about it.

This year also marked my introduction of my [Omake format](https://rubenerd.com/omake.xml), which I've since extended to run my [coffee site](http://ruben.coffee/). I love that two things I appreciate melded together like that.


### 5. Social media has never been better

Neil mentioned this, and I thought it was including here too. My social media experience this year is the best it's ever been, surpassing even that of Twitter in 2007.

I've [been on Mastodon](https://bsd.network/@Rubenerd) since 2018, but this was the year it surpassed any other network for me. And for all its foibles and limitations, the people I talk with there have been wonderful. It feels more sustainable, and I'm not being sold crap with every second post. It's also possible for me to host my own server and engage with the network on my terms, just as I do with this blog. This is what the Web was supposed to be!

My social anxiety has got much worse this year for unrelated reasons, but at least I still have a healthy avenue for discussion.


### 6. NetBSD and Alpine Linux showed me the simple life

NetBSD/macppc was my first BSD operating system back in the day, as I've discussed here before. I ran it on my iBook G3 when both Yellow Dog Linux FreeBSD refused to install for some reason. I since reverted to running FreeBSD for most tasks, mostly for its tooling and ZFS integrations, for which it has *served* me well.

This year though saw NetBSD return into my life in a big way. It started as a small experiment, but it gradually grew into something that now runs on my personal laptops (for which it is perfect), and one of Clara's and my two homelab "servers" (aka, Supermicro workstation boards in Antec 300 cases). It's hard to quantify, but I feel in control with NetBSD. It doesn't ever surprise me. Install, build a few chroots, done.

I also finally tried Alpine Linux for my Penguin software requirements, and discovered a small OpenRC-based distribution that's easy to maintain. I'll still likely keep Debian kicking around for experiments and to maintain my systemd skills, because I live in the real world. But Alpine will run what I care about.


### 7. Boutique hosting is still a thing

Consolidation in the web hosting/cloud space felt like it was unstoppable, but this year has proved you can still find people and companies outside the big players who (a) give a shit about you and (2) offer a better service. They could be a small BSD hypervisor operator in Europe, or a dedicated host in southern Australia.

The health and viability of the Web will depend on making it a... Web again. I'm relieved that people are still showing us how it's done.


### 8. Repair feels more approachable

I grew up building my own computers, like I'm sure many of you did as well. In recent years that spark was partially extinguished thanks to the sale of sealed boxes, and phones becoming the primary computing platform.

This year I got stuck back into building homelab servers alongside retrocomputers. But just as importantly, I also fixed a bunch of them. There are more bloggers and video editors showing us how to do this than ever before, which has made the whole process feel accessible in a way it hadn't to me previously. Or at least, to the same extent.

I now get more satisfaction and joy bringing ewaste and retro hardware back to life than any new purchase. It feels like I'm *righting* something small in the world.


### 9. I rediscovered plain HTML

My first websites were written on the school's computers in Notepad. They were scrappy, and mine! We've learned a lot in the intervening years, but I've also felt like we lost something too.

My [retro site](http://retro.rubenerd.com/) started as a silly way to track retrocomputer parts, but has since taken on a life of its own. *Turns out*, it's fun writing plain HTML again! No frameworks, no JavaScript, no CSS. Just you, some markup, and some mildly-nostalgic output. Could I eventually have my own site here done like this? Who knows!

There's something so approachable and reassuring about getting your hands dirty and writing basic HTML.


### 10. My interactions with all of you

This was the year where the amount of feedback and comments I received in email and on social media exploded. My backlog is now so comical that I'm almost ready to declare feedback bankruptcy. But while some of it is the usual trolls and spammers, I still wake up almost every morning to something nice or constructive. Selfishly, that's helped me in ways I can't even describe.

Thank you.


