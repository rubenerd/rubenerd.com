---
title: "A History of Scotland, by Neil Oliver"
date: "2024-08-31T09:42:45+10:00"
thumb: "https://rubenerd.com/files/2024/history-of-scotland@1x.jpg"
year: "2024"
category: Thoughts
tag:
- europe
- family
- history
- scotland
location: Sydney
---
I picked up a copy of Neil Oliver's book on Scottish history last time I was in Melbourne, and I just got around to finishing it. It was decent, though I'm a little conflicted.

Nearly half my family comes from Scotland. My dad is German, and both branches of my late mum's family came from Scotland (with a branch hailing from Sussex in England). But I was born in Australia, and spent most of my childhood in Southeast Asia, so I know more about the Peranakan and colonial history of Singapore and Malaysia than I do my own ancestry. I've started to change this.

Neil's book mostly manages to condense thousands of years of Scottish history into one tome, from the formation of Scotland's unique geology, to Thatcher and the collapse of the nation's shipbuilding industry. It's dense; it took me reading the same chapter a few times in places to retain enough information, names, and events to understand the context of the next. It's also necessarily brief in sections, and there are omissions.

<figure><p><img src="https://rubenerd.com/files/2024/history-of-scotland@1x.jpg" alt="Cover of the book." srcset="https://rubenerd.com/files/2024/history-of-scotland@2x.jpg 2x" style="width:381px; height:585px;" /></p></figure>

The best passages weren't the endless procession of monarchs, wars over empire, or the geopolitical machinations that began with the Picts and Gaels, but of the struggles of common people. How the changing climate forced hunter gatherers to adapt to harsh new realities, or the misery and plight of iron workers in Glasgow's infamous slums. There's a palpable sense of indignation in the treatment of his people, and of the indifference of the politicians and industrialists who also benefited handsomely from the New World slave trade. Neil's patriotism didn't blind him to the harsh realities of life on the ground.

What also struck me was Neil's careful discussion of "Britishness" as a cultural and national identifier. I knew of the merging of the Scottish and English crowns, culminating in the formal Acts of Union that created Great Britian. But Scots I've spoken to over the years still regarded this as Scotland being subsumed by their southern neighbour; a final triumph of centuries of English monarchs thinking owning Scotland (Scotland-shire, as Neil describes) was their birthright. To read someone's view of being British as a positive, and acknowledging how Scotland benefited from being part of this larger empire, was a perspective I hadn't really encountered before.

Alas, this is where some of Neil's modern perspectives come in. I wasn't aware [he was a crank](https://en.wikipedia.org/wiki/Neil_Oliver#Political_views), especially when it came to Covid, "world government", and climate change. It's surreal to think about, given how impassioned a defence he gave to the downtrodden before. One could imagine a hypothetical grandson or daughter of Neil talking about the plight of people living under conservative Britain during the pandemic, and how their grandfather spouted rubbish about vaccines causing cancer, leading to hesitation and more Covid deaths. It's so *deeply* weird... but then, that's Scottish history for you.

I still left his book knowing more about one of my ancestral homes than before, and with an interest to learn more. To that end I'd consider it a success, but clearly I have more work to do. 🏴󠁧󠁢󠁳󠁣󠁴󠁿
