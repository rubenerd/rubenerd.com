---
title: "Seeing the 2024 Xmas trees around Sydney"
date: "2024-12-25T10:08:23+11:00"
abstract: "We got to wander around Martin Place and the QVB, which was a lot of fun."
thumb: "https://rubenerd.com/files/2024/xmas-martin-place@2x.jpg"
year: "2024"
category: Travel
tag:
- photos
- sydney
location: Sydney
---
Clara and I took a train into the CBD earlier this week to tour all the decorations. It was much cooler than the summer heatwave we'd had a week earlier which was lovely... though we still saw two women in bikini tops and massive Santa hats. Because Australian summer!

Here we are at [Martin Place](https://en.wikipedia.org/wiki/Martin_Place "Martin Place on Wikipedia"), not that far from the infamous fountain from *The Matrix*. Clara's Blue Bear Cat, fuzzy mango, and plushie Magni joined in:

<figure><p><img src="https://rubenerd.com/files/2024/xmas-martin-place@1x.jpg" alt="Photo of a large Christmas tree with lots of baubles in Martin Place, with Clara's aforementioned plushies." srcset="https://rubenerd.com/files/2024/xmas-martin-place@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

I love when stores integrate decorations into their façade, like this old one did not that further down the road:

<figure><p><img src="https://rubenerd.com/files/2024/xmas-stars@1x.jpg" alt="A building with Greek/Roman style columns and large Xmas stars in between." srcset="https://rubenerd.com/files/2024/xmas-stars@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

Moving down towards [Westfield Sydney](https://en.wikipedia.org/wiki/Westfield_Sydney "Wikipedia article on Westfield Sydney") at the [Pitt Street Mall](https://en.wikipedia.org/wiki/Pitt_Street_Mall "Wikipedia article on the Pitt Street Mall"), we saw these minimal trees with simple LEDs everywhere. This is an aesthetic they moved towards a few years ago. They look almost sinister when backlit like this, as though I'm about to venture somewhere Santa sends you if you haven't been good one year:

<figure><p><img src="https://rubenerd.com/files/2024/xmas-westfield@1x.jpg" alt="Dark, almost shaggy trees against a large skylight, with only LEDs and no other ornementation." srcset="https://rubenerd.com/files/2024/xmas-westfield@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

We didn't make to the [Strand Arcade](https://en.wikipedia.org/wiki/The_Strand_Arcade "Wikipedia article on The Strand Arcade") unfortunately, which usually has some amazing decorations that match its Victorian-era design. But we did go to the [Queen Victoria Building](https://en.wikipedia.org/wiki/Queen_Victoria_Building), with its tree that spans multiple floors. This was just the top! It was definitely one of the better ones they've had of late.

<figure><p><img src="https://rubenerd.com/files/2024/xmas-qvb@1x.jpg" alt="Tall Xmas tree at the top floor of the QVB." srcset="https://rubenerd.com/files/2024/xmas-qvb@2x.jpg 2x" style="width:420px; height:560px;" /></p></figure>

We had to head home soon after this, but we ended up going back for a Yomie's, where this little fellow with a *shockingly* handsome moustache greeted us!

<figure><p><img src="https://rubenerd.com/files/2024/xmas-yomies@1x.jpg" alt="Shopfront showing a small santa elf with an extremely promient moustache!" srcset="https://rubenerd.com/files/2024/xmas-yomies@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

Happy Holidays wherever you are, I hope you're well :).
