---
title: "Why some might not want to publish web feeds"
date: "2024-08-28T08:38:25+11:00"
year: "2024"
category: Internet
tag:
- indieweb
- rss
- web-feeds
location: Sydney
---
I regularly talk about why I love web feed formats like RSS an Atom, and how my words have been available via such feeds for two decades. But I don't think I've ever acknowledged why people might not want to publish them.

Theft is likely the biggest reason, unfortunately. Feeds are machine-readable formats, which are simpler and more predictable to use for a script kiddie than writing a web scraper, and can be updated easily. My words have appeared wholesale on dozens of knock-off sites over the years&mdash;even before generative AI tools normalised plagiarism&mdash;the most benign of which merely wrap them with ads. Publishers can opt to only include summaries, or include authorship info at the end of each post in an attempt to at least claw back attribution or a bit of control, but these can be easily detected and worked around.

Indie sites with large photo libraries also tend to not like publishing web feeds, because they can increase load and negate (albeit imperfect) hotlinking protection. A trick in the early web was to only deliver an image if the request header came from the same site, but this would break any images delivered over a web feed to a remote client. I used to do a few tricks like custom RSS image URLs with finite lifespans, but abandoned them when I realised they were easily negated and more trouble than they were worth.

Seeing the debate around Mastodon [including RSS by default](https://github.com/mastodon/mastodon/issues/22172) also highlighted privacy concerns. I happen to think web feeds are the *perfect* use case for this new era of social networks, but it does introduce challenges for moderation and privacy I hadn't considered.

And finally, XML is also seen as *passé* and old hat. I'm the first to admit I'm the only XML fanboy left in 2024, but if you're not enamoured with the format, including a web feed and the requisite XML parsing and authorising dependencies can seem like a lot of work for something you'd rather avoid. I'd suggest this wariness is misplaced, as all good CMS packages will give them to you out of the box. In the pantheon of XML formats (*cough* RDF), a basic RSS 2.0 and Atom feed is also easy enough to generate.

I see people get angry when a site doesn't have a web feed available, especially if it's a blog or similar site with regular posts. I don't think that attitude is helpful; a better approach is to say that having a web feed helps grow the independent web again. But I can see circumstances where people might want to opt-out.
