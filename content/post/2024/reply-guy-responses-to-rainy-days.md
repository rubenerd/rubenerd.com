---
title: "Reply Guy™ responses to being caught in the rain"
date: "2024-04-03T08:38:30+11:00"
year: "2024"
category: Thoughts
tag:
- replies
location: Sydney
---
These replies are `apropos(1)` of nothing in particular. Yes, that'll do!

* What do you expect, you don't live in the dessert [sic].

* The weather forecast clearly stated rain, are you a smooth brain?

* It's your own fault for not preparing, bringing an umbrella, losing your umbrella, using too small an umbrella, using an umbrella instead of a raincoat, using a raincoat instead of an umbrella, not performing the appropriate chant.

* Ummm, cloud seeding is a thing, cooker.

* We haven't had rain in years over here, stop being selfish.

* I just checked your city on $APP and it's not raining, so why are you lying?

* *Something incomprehensible linking the OP to conspiracy theories because they have a rainbow flag in their profile.*

* Rain, what's that?

* Why are you not thinking about the poor farmers and/or hydroelectric turbine  operators?

* It's raining where you are? Sucks to be you!

* You probably get home and shower or have a bath after, what's the difference?

* I've been in worse storms, stop complaining.

* Um, actually, that's a shower/storm, not rain.

* See!? Solar power is a folly!

* How sheltered [heh –ed] are you if this is your main worry?

* If you hate being in rain, why do some of your screenshots from Minecraft and Train Simulator show rain?

* Serves you right for bashing cars in urban areas.

* You do realise that some of us actually enjoy walking in the rain?
