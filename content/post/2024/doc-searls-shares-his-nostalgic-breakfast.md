---
title: "Doc Searls shares his nostalgic breakfast"
date: "2024-02-19T10:21:24+11:00"
year: "2024"
category: Thoughts
tag:
- doc-searls
- food
- nostalgia
- singapore
location: Sydney
---
Doc Searls recently [blogged about the breakfast](https://doc.searls.com/2024/02/17/moms-breakfast/ "Mom's breakfast") his mum used to make him, and what he still makes all these years later:

> [H]er most leveraged dish was the breakfast she made for us often when my sister and I were kids: soft-boiled eggs over toast broken into small pieces in a bowl. It’s still my basic breakfast, many decades later.

It looked great! It also struck me how similar it was to a local Singaporean breakfast I used to have. Strips of toast with boiled eggs are a staple of local kopitiams, complete with a strong coffee or *teh tarik*.

As I posted on his blog, food brings the world together :).
