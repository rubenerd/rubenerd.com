---
title: "We can’t be informed about everything"
date: "2024-11-25T15:29:49+11:00"
abstract: "The world is complex and unforgiving. We shouldn’t chide people for not knowing a small sliver of it."
year: "2024"
category: Ethics
tag:
- chinos
- music-monday
- philosophy
- skills
location: Sydney
---
I'm pretty good at a few things. You have a technical system you need described and documented? A complex application with a database, load balancers, and caching you need help installing, configuring, or pushing from dev to production? You need to migrate between providers, or from bare metal to a VPS fleet, or a VPS to a cloud, or Windows Server to Linux, or Linux to BSD? Do you have a document management system you need help administering, or questions about metadata or compliance? What about platform accessibility? Do you have concerns about drives, file systems, and/or data recovery? In the words of moral philosopher James Blunt, [I'll be your man](https://www.youtube.com/watch?v=IUkLOUNxP8A "James Blunt’s music video for I’ll Be Your Man").

*Hey, we got a [Music Monday](https://rubenerd.com/tag/music-monday/) out of this!*

On the flipside, do you need coffee advice? What about how to maximise your DOS conventional memory? How to tell an A350 from a B787, or an Alco from an EMD, or a Peppercorn from a Mallard?  What a good lens would be for a given situation? Where should you travel in Japan or Southeast Asia? What the best era of jazz was/is? (Ab)using XML to do cool stuff in 2024? Setting up a classic Hi-Fi system? What anime studio tugs at the heartstrings the most, or which anime fig manufacturer is best for detail? When was a map created? What retrocomputer should you start with? How you navigate anxiety or gender dysphoria? Oh boy, let me tell you!

Wait, maybe scratch those last two.

Point is, that might sound like I know a lot. But I don't. **I know shockingly little about how the world works.** And what I *do* know doesn't come close to approaching the professional or enthusiast levels that I exhibited above.

I plug my carefully-crafted computers into power sockets, but I don't know about home wiring or the intricate details of power transmission and generation. I'm typing this using my fingers, but I'm not an orthopedic surgeon. Tonight I'm going to make a salad, but I wouldn't know the first thing about how to grow capsicums. I know the detergent in my washing machine right now has surfactants, but how did they make it? What about the low-friction coating on the mouse I'm sliding around? How *did* they get that specific hue on our new wall paint? How does my bathing suit dry so quickly, compared to when I fall in the pool in my chinos? 💦

The modern world is designed to be infinitely complex, yet the people who inhabit it are increasingly unforgiving of any misunderstanding. Any problem that arises was clearly the result of *you* being a failure, or not taking the time to research something in obsessive detail. It's as though people *deserve* any and all negative outcomes. But that's an impossible bar to clear for everything you interact with, directly or otherwise. *That's why we have professionals*, and why they're so important, and why we need to be able to trust them. *That's a lot of italics*.

[Cory Doctorow brought this up last week](https://pluralistic.net/2024/11/21/policy-based-evidence/), and I jumped up and down on my chair, which I don't know how it was made beyond the use of Allen keys:

> The modern world is full of modern questions, the kinds of questions that require a high degree of expert knowledge to answer, but also the kinds of questions whose answers you'd better get right.

> You could do any of [this research]. You might even be able to [research] two or three of them. But you can't do all of them, and that list is just a small slice of all the highly technical questions that stand between you and misery or an early grave. Practically speaking, you aren't going to develop your own robust meatpacking hygiene standards, nor your own water treatment program, nor your own Boeing 737 MAX inspection protocol.

Yes Cory, this! Anyone can be an armchair `$something`. But to know everything that sustains your existence? That's a different kettle of fish. How would you even *get* a fish in a kettle? Or shoot one in a barrel? After I've fallen into a lake of fish? In chinos? **IN CHINOS, DAMN IT!**

`&intermission_music("We're experiencing technical difficulties")`

I bring this up for three reasons.

First, is the one that precedes the second.

Second, a younger reader lamented their lack of understanding about a technical topic in which they'd just started to pursue, and I wanted to assuage their concerns. Everyone *literally* has to start somewhere, and the fact you're willing to admit you have a lot to learn places you well ahead of people who think they know everything. It's cliché to bring up the [Dunning-Kruger Effect](https://rationalwiki.org/wiki/Dunning%E2%80%93Kruger_effect "Rational Wiki on the Dunning-Kruger effect") here, but if the shoe fits. The number of engineers who tell people they're stupid for not knowing `$X` because "it's so obvious", when it's clear they don't understand `$Y` that I'd consider basic knowledge is astounding.

But more broadly, is a phrase with three words. Wow Ruben, you're on a tear with sparkling, *sparkling* wit today. If you find yourself getting ready to chastise someone for not knowing something that's out of their wheelhouse, just remember that we're all muddling through this world as best we can. Chances are, they know something about an unrelated topic that you don't. I bet you don't know the best hybrid floors to use for NSW strata approval. Or maybe you do.

And yes, this is as much a reminder for me too. Otherwise you're not better than a grown man in chlorinated chinos.
