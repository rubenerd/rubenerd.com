---
title: "A selection of old things I remember"
date: "2024-11-14T14:56:15+11:00"
year: "2024"
category: Thoughts
tag:
- lists
location: Sydney
---
In no particular order:

* The parameters for `FORMAT` in DOS to prepare a 720 KiB double density floppy disk, instead of high density.

* When Funan Centre in Singapore was *The IT Mall*.

* How to answer a phone call that's a fax. "Hello, Ruben speaking..." *PSSSHHHHHHHHHH* aaah hang up and press the button!

* How to recover a PC bitten by the Chernobyl virus, which was a great gig for pocket money in school.

* Downgrading Vista machines to XP, which was a great gig for money in university.

* TransitLink cards, and those monthly CityRail passes.

* `pkg_add(8)` in FreeBSD and NetBSD.

* Being interested, excited, and/or keen for new software and hardware releases, rather than being filled with dread, trepidation, and/or worry.

* What it was like to go to school with Friendster, MySpace, and some PHP forum software we ran for our grade.

* Winamp, QuickTime Player, and Real Player being installed by various things. I miss one of them.

* When Borders Books was in the Hornsby Westfiend in Sydney, and Wheelock Place in Singapore.

* Landlines.

