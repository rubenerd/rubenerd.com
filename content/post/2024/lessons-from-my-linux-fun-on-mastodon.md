---
title: "Lessons from my Linux fun on Mastodon"
date: "2024-04-15T12:01:47+10:00"
year: "2024"
category: Internet
tag:
- bsd
- gnu
- replies
- social-media
- windows
location: Sydney
---
Last Saturday I made the mistake of [asking Linux people](https://bsd.network/@rubenerd/112264116393309434) not to tell those who need Windows to use Linux, and [answered](https://rubenerd.com/people-who-need-to-run-windows/) the most popular replies.

I thought it was innocuous, but at the time of writing, it has over a hundred (visible) comments, five hundred reposts, and a thousand likes. This is a typical evening for a Hololive vtuber, and a quiet day for a politician or musician. But by the standards of someone with social anxiety, and empathy for long-suffering Windows users? It was an *omnishambles*.

A few Australians got that reference. I wish I didn't.

I've been overwhelmed by the emails, direct messages, and comments supporting what I said. Thank you, especially to those of you I didn't get a chance to tell directly. But I do think I learned a few things from the experience.


### Generalisations

The first is that people *really* don't like generalisations. If I'd qualified "Linux people" with "some", I suspect this would have nipped a lot of this in the bud. A number of comments explained why they *personally* don't engage in that behaviour, and that my characterisation was unfair (or a less charitable term).

I can understand this to an extent. We get defensive when we think "ours" are under attack from "them". *Who's this guy with a RUN BSD icon, and where does he get off slagging our entire community?*

For a cohort famed for lacking social skills, it was akin to my throwing a match at a power keg. People should have been smart enough to know I wasn't *literally* referring to every Linux user, just as I'm not saying waffles suck if I profess a preference for pancakes. I'm sure I could look up the same logical fallacy site detractors referred me to for an example. 

That said, I will be more sensitive to how I word things in the future, and I apologise if I tarred you with the wrong brush. The next group won't be receiving the same apology.


### The GNU and Free Software movement

I'll admit now, I have newfound appreciation (for want of a better word) for the political ideology of GNU and Free Software fans. 

I've copped negative feedback before when mentioning my preference for permissive licences like BSD/ISC/MIT, and my sympathy for people stuck using commercial software. But this was the first time I was openly described as being a corporate shill, a defender of capitalism, and enforcing serfdom onto those who are scared of freedom. It was their ideology espoused in its pure, uncut form, and I'll admit it was a bit scary.

I get that they want to change the world, and they see the GPL as a means to achieve this. But I want to be crystal clear here: my experience over the last couple of days has evaporated **any** residual sympathy I had for their cause. This wasn't just an ineffectual waste of time, it was actively counterproductive to their goals.

I also acknowledge however that this vocal subset don't represent the views of everyone who supports Free Software, nor does everyone in that movement agree with the actions of these people hellbent on making their wider community look like prats. But my lesson here is to block them and move on. There's nothing to be gained from talking with them, and I have better uses of my time.


### Examples always help

I've learned this from writing technical documentation for almost a decade, but forgot it here. You can write a point, discuss a feature, or advocate for a position, but it will almost always fall on deaf ears without a concrete example.

Had I said that people had to use Windows, and mentioned some industrial and enterprise IT software and hardware, people with a dispensation towards skimming and knee jerk reactions may have been given pause. Or not, but it would have been worth a shot.


### Sometimes, people just want to be heard

The most interesting comments coming out from this debacle were people discussing their own use cases and histories when it came to both Windows and Linux. I learned that government tax software in the US and India doesn't run on Linux, that there are still million-dollar pieces of laboratory equipment that run Windows CE, and that accessibility and VR features still have a way to go on Linux.

Equally, I had as many people tell me that Linux works perfectly fine, and that people running Windows had all tricked themselves because their mail server and 80-year old grandma run GNOME just fine.

You also have the people who need to joke about everything. Linux won't solve your problem, but what about the Amiga? BSD? DOS? I've been guilty of doing that myself, though it does grate after you've read the same thing fifty times.

There's this urge to hitch yourself onto the latest train, and I guess I gave people the opportunity to discuss their experiences. Take them as comments, and move on.


### Your Mastodon admins are probably underappreciated

I'm lucky that this happened on Mastodon, and that there are small team of people I now know well who maintain and moderate it. They were top of this pseudo-DDoS from the moment it started, and saved me from seeing the worst takes from people.

I've had my own Mastodon server on the side for a while, but this is proof of where a small community and a caring team of people really help. If you were part of the exodus from Shnitter, or arrived on Mastodon of your own accord, thank your admins. Just because software is open source doesn't mean it's easy or cheap to maintain.


### Something pretending to be a conclusion

I should be ending on something pithy or inspired here, but it's almost the end of my lunch break. All I'll say is that I'm going to keep talking about this stuff, because it's important.

I'm also glad to have met a few wonderful people out of the exchange, and glad that at least it (briefly) sparked a wider discussion on the culture around this software. Michael Dexter and I [discussed this back in 2019](https://rubenerd.com/discouragement-in-it/ "Discouragement in IT"), and the issues are all still here.

Check out [Kestrel's moving article about kindness](https://tilde.green/~kestral/blog/2024-04-14.html) after this, it's excellent. 
