---
title: "VMware outsourcing their support"
date: "2024-04-27T08:42:39+10:00"
year: "2024"
category: Software
tag:
- business
- enshittification
- virtualisation
- vmware
location: Sydney
---
Poor VMware. I refer to them as the Brussels sprout of enterprise IT; constantly shifted around the plate between new owners, never seeming to land anywhere good. Their current owners Broadcom haven't exactly endeared themselves to the industry or their customers, as any quick web or social media search will uncover.

The latest chapter in this never-ending adventure came in the form of an email update regarding support contracts:

> Globally at Broadcom, we have solidified strategic partnerships with a select list of distributors by geography to deliver support on behalf of Broadcom.

Whenever an email starts with "Here at $COMPANY, we have...", you know it won't be good.

> This message is to officially notify you of this change and that, depending upon the products you purchased, your technical support provider for VMware by Broadcom products may change from the Broadcom Support Center to one of our Authorized Distributor's Technical Support Centers during the 2024 calendar year.

That's right, they're not even offering their own support anymore. There's also no mention of who these "select list of distributors" are, though later in the email they detail that we'll continue to receive support through the same portal as before.

A charitable read of this would be that they're being transparent about who's handing these support requests. Theoretically, you could imagine them palming off requests to their distributors behind that support portal, and not telling us about it. But the GDPR and CCPA would probably nip that in the bud.

We run VMware for some legacy workloads at work, or for those that require VMware for certification. Aka, things we can't migrate to our Xen-based platform. Our clients understand it's not us hiking their licencing costs and enshittifying their support, but it's still not a fun conversation to have several times a week. I'd rather talk about this cool new feature or integration their platform supports. You know, tech stuff.

Their support already wasn't great; we end up dealing with many questions ourselves because we're able to diagnose and resolve issues faster than their escalation process can handle. This isn't ideal for something you pay licencing for at least in part so you can receive that support in the first place. It's also a dramatic fall from grace for a business a decade ago that used to be halfway decent.

I feel for people stuck working there. If [Conway's Law](https://en.wikipedia.org/wiki/Conway's_law) is to be believed, this external upheaval must reflect some severe internal malaise. `#hugops` indeed.
