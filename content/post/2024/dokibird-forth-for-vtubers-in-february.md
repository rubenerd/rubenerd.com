---
title: "Dokibird forth for vtubers in February"
date: "2024-03-07T11:37:36+11:00"
year: "2024"
category: Anime
tag:
- ceres-fauna
- dokibird
- hololive
- vtubers
location: Sydney
---
Dokibird's [ascent in the charts](https://twitter.com/StreamHatchet/status/1765303903132745874) is such a wonderful achievement, and I couldn't be prouder for her!

For those who don't follow this sort of thing, Dokibird left her former streamer agency after they publicly debased themselves. She went back to her old independent persona, and has been kicking vArse ever since.

<figure><p><img src="https://rubenerd.com/files/2024/GH-d0daXgAAVpK-@1x.jpg" alt="Chart showing Dokibird number 4" srcset="https://rubenerd.com/files/2024/GH-d0daXgAAVpK-@2x.jpg 2x" style="width:500px; height:281px;" /></p></figure>

I'm mostly a Hololive fan, so I don't know that much about Niji. But while I started watching her stuff to support her transition, I've since doing it becuase she's genuinly creative and entertaining.

I wish all all the success in the world; she's fucking earned it. Which reminds me, I need to [preorder some merch](https://www.etsy.com/listing/1689591201/preorder-dokibird-goods "PREORDER | Dokibird goods").

<figure><p><img src="https://rubenerd.com/files/2024/dokibird-merch@1x.jpg" alt="You know you want these stickers and cards. Don't say I didn't warn you." srcset="https://rubenerd.com/files/2024/dokibird-merch@2x.jpg 2x" style="width:500px; height:500px;" /></p></figure>

Also, as a Sapling, seeing [Fauna](https://www.youtube.com/channel/UCO_aKKYxn4tvrqPjcTzZ6EQ "Fauna's YouTube channel") represent Hololive English in this chart is amazing. If you need some [comfy Minecraft](https://www.youtube.com/playlist?list=PLeLzFij5dykeHJ3hME0iov183V2a4r9xz "Her Minecraft playlist") while you work, nobody does it better. 🌿

