---
title: "Media accuracy on topics with which you’re familiar"
date: "2024-02-22T10:52:08+11:00"
year: "2024"
category: Thoughts
tag:
- facts
- journalism
location: Sydney
---
I've mentioned a few times how easy it is to pick apart inaccuracies in media coverage on a topic you know about, and the unease that sets in when you realise *every* topic likely has the same issues.

I'm by no means the first person to notice this! [Wikipedia quotes](https://en.wikipedia.org/wiki/Pear "Wikipedia article on pears") a portion of a speech by American author and filmmaker Michael Crichton:

> The pear is native to coastal and mildly temperate regions of the Old World, from Western Europe and North Africa east across Asia. It is a medium-sized tree, reaching 10–17 m (33–56 ft) tall, often with a tall, narrow crown; a few species are shrubby. 

That's clearly the wrong article. [Let's try again](https://en.wikipedia.org/wiki/Michael_Crichton#Speeches "Wikipedia article on Michael Crichton")\:

> Briefly stated, the Gell-Mann Amnesia effect is as follows. You open the newspaper to an article on some subject you know well. In Murray's case, physics. In mine, show business. You read the article and see the journalist has absolutely no understanding of either the facts or the issues. Often, the article is so wrong it actually presents the story backward—reversing cause and effect. I call these the "wet streets cause rain" stories.

That same page also references [this New York Times article from 1982](https://web.archive.org/web/20230425002436/https://www.nytimes.com/1982/02/27/us/required-reading-smith-on-lawyers.html), attributed to journalist Erwin Knoll:

> Everything you read in the newspapers is absolutely true except for the rare story of which you happen to have firsthand knowledge.
