---
title: "Happy Lemon is back in Sydney 🍋"
date: "2024-03-03T07:59:43+11:00"
year: "2024"
category: Thoughts
tag:
- food
- nostalgia
- sydney
- tea
location: Sydney
---
Clara's and my favourite bubble tea and waffle shop is back in Sydney after nearly a decade of absence!

<figure><p><img src="https://rubenerd.com/files/2024/happylemon@1x.jpg" alt="The new Happy Lemon branch in Sydney" srcset="https://rubenerd.com/files/2024/happylemon@2x.jpg 2x" style="width:500px; height:333px;" /></p></figure>

We had our first date at the now-closed *Happy Lemon* on George Street in the Sydney CBD. We talked from lunchtime until the owners had to kick us out at closing time. I'd met her at the anime club at our uni and been a friend for a few years, but it was that date that confirmed it all for me.

Pardon the cheesiness, but *rock salt cheese tea* is one of the drinks you can get, and they're such a treat. Even without sugar it works *so well*.

I always made sure to visit their Melbourne branches whenever I was down there on business, but now Clara and I can get it locally too.
