---
title: "A BSD person tries Alpine Linux"
date: "2024-04-26T08:23:08+10:00"
year: "2024"
category: Software
tag:
- alpine-linux
- bsd
- grilled-cheese-sandwiches
- linux
- openrc
location: Sydney
---
In February last year I wrote about [running a FreeBSD desktop](https://rubenerd.com/its-worth-running-a-freebsd-or-netbsd-desktop/), and concluded that sometimes you need to give yourself permission to tinker.

Well recently I've started tinkering with [Alpine Linux](https://www.alpinelinux.org/)! It's been recommended to me for years, so I'm finally getting around to checking it out. There's a lot to like if you come from BSD, which we'll dig into here.

<figure><p><a target="_BLANK" href="https://rubenerd.com/files/2024/alpine-ovm-boot.png"><img src="https://rubenerd.com/files/2024/alpine-ovm-boot.png" alt="Booting a new Alpine ISO on OrionVM" style="width:500px;" /></a></p></figure>


### A potted history

The Alpine website [describes it as](https://www.alpinelinux.org/about/)\:

> an independent, non-commercial, general purpose Linux distribution designed for power users who appreciate security, simplicity and resource efficiency.

Its small footprint and design decisions also make it more secure:

> All userland binaries are compiled as Position Independent Executables (PIE) with stack smashing protection. These proactive security features prevent exploitation of entire classes of zero-day and other vulnerabilities.

Natanael Copa discussed the genesis of the project back in 2005, making it older than I realised. Like the BSDs, it's found its way into embedded systems, routers, and mobile devices, as well as general purpose servers and desktops.

Alpine is also a popular base for use in Linux containers, owing to its compact size and limited dependencies. There are also [toolchains](https://github.com/alpinelinux/alpine-chroot-install/) for easily running it in a `chroot(8)`, which is interesting for someone who uses [NetBSD chroots(8)](https://man.netbsd.org/chroot.8) and [FreeBSD jails](https://docs.freebsd.org/en/books/handbook/jails/) extensively for testing and deployments.


### Installation

Alpine comes in a [few different versions](https://alpinelinux.org/downloads/), including builds for ARM, PPC64, x86, and x86_64.

I downloaded the Xen ISO image because I was booting it on a VM at work, before realising I misread *Dom0* as *DomU*. The former refers to a Xen hypervisor, not a guest. Either way, it booted and installed the same as a standard ISO.

The install process is about as simple as you could make it. You log into the live environment with `root` and no password, then execute `setup-alpine`.

You're asked basic questions for your keymap, networking, timezone, and root authentication. You can also inject an SSH key from the start, which is useful if you're deploying a fleet of VMs or servers with an orchestration tool after the fact, or you're deploying to a mediocre hosting provider that doesn't give you an OOB console.

You're also given the choice of a few different SSH servers and ntp clients, which let me choose my preferred OpenSSH and openntpd. It also correctly identified it was operating under Xen.

<figure><p><a target="_BLANK" href="https://rubenerd.com/files/2024/alpine-install.png"><img src="https://rubenerd.com/files/2024/alpine-install.png" alt="Screenshot of a portion of the Alpine Linux installer, showing the mirror selection, user config, and Xen installation." style="width:500px;" /></a></p></figure>

It can also configure an LVM, but I stuck with what Alpine calls standard `sys` partitions for now. This uses `ext4`.


### Post-install and exploration

Booting into Alpine for the first time, your given a hint as to why it's special: `dmesg(1)` informs you you're running [OpenRC](https://github.com/OpenRC/openrc)! It's portable, small, fast, efficient, transparent, and secure. It's also very familiar to a BSD person used to writing rc scripts. `/etc/rc.conf` and `crond(8)`!? **Yes!!!**

At the risk of embellishing my feelings about this, it is *such* a relief that there are Linux distros like Devuan, Gentoo, and Alpine using this. It's a breath of alpine air, and has legitimately made Linux fun again.

<figure><p><a target="_BLANK" href="https://rubenerd.com/files/2024/alpine-openrc.png"><img src="https://rubenerd.com/files/2024/alpine-openrc.png" alt="Screenshot showing dmesg output on first boot." style="width:500px;" /></a></p></figure>

Along with OpenRC, Alpine is bundled with [musl](https://wiki.alpinelinux.org/wiki/Musl), and runs busybox. Both are obviously more limited than GCC and the GNU coreutils, but they further contribute to the base system's smaller size and attack surface. [llvm](https://pkgs.alpinelinux.org/package/edge/main/x86_64/llvm-runtimes) is also available, as is the [MirBSD Korn shell](https://pkgs.alpinelinux.org/package/edge/main/x86_64/mksh), one of my two preferred interactive shells.

> Um, Ruben, I'd like to interject for a moment. What you are referring to as Linux, is in fact, GNU/Linux, or as I've recently taken to calling it, a GNU grilled cheese sandwich merely featuring Linux as the...

... nope!


### Packages

Speaking of installing packages, let's take a look at that. Alpine's default package manager is [apk](https://docs.alpinelinux.org/user-handbook/0.1a/Working/apk.html "Working with the Alpine Package Keeper"). As is normal on Linux, this handles updating the base system and all packages, because it makes no distinction. I'd be interested to see if I could also run an unprivileged copy of this as I like to do on the BSDs, but I haven't checked yet. There's also [pkgsrc](https://www.pkgsrc.org/#index2h3), so no biggie.

Configuration is in `/etc/apk/repositories`, where you can enable the community repo by uncommenting the second URL supplied by the installer. Alpine also has a `testing` repo, and you can add your own.

Usage is easy, though I've still been mistyping `apt install` instead of `apk add`, because old habits die hard. There's an official [web interface](https://pkgs.alpinelinux.org), and Alpine repos are on [pkgs.org](https://alpine.pkgs.org/).

A few packages later, and I had my "essentials" going, like I do on my [console-only laptop](https://rubenerd.com/my-freebsd-laptop-with-just-tmux/ "My FreeBSD laptop... without a GUI!?"):

<figure><p><a href="https://rubenerd.com/files/2024/alpine-stuff.png"><img src="https://rubenerd.com/files/2024/alpine-stuff.png" alt="Alpine running tmux, htop, zpool, and cmatrix." style="width:500px;" /></a></p></figure>

Perhaps the package I was most surprised about was zfs. It was literally two commands to install and load the kernel module (though obviously root on ZFS would be more involved). What that would look like after an upgrade I'd have to see, but thus far I'm impressed.

    # apk add zfs zfs-lts
    # modprobe zfs


### Conclusion

I've barely scratched the surface, but there's enough here for me to seriously consider a switch to it as my primary Linux distro for testing and servers. I love that `htop(1)` and `lsof(1)` only shows a small list of recognisable processes, that it uses OpenRC, that package management seems straight forward, and that it's so simple to configure. I've wondered what a modern, functional "Occam's Linux" would look like. This is it.

I'd be interested in seeing if [uutils](https://github.com/uutils/coreutils) runs if I need something more than busybox, but for a server I doubt it.

<figure><p><img src="https://rubenerd.com/files/2024/alpine-alpine.png" alt="Alpine running the Alpine mail client" style="width:500px;" /></p></figure>

*I heard you liked Alpine, so I etc...*
