---
title: "An update on painting my NR200P case beige"
date: "2024-07-03T20:16:27+10:00"
year: "2024"
category: Hardware
tag:
- colour
- nr200p
- retrocomputing
location: Sydney
---
Back in early May I started [painting a CoolerMaster NR200](https://rubenerd.com/progress-painting-a-coolermaster-nr200p/) a delightful shade of retrocomputer beige. I got up to adding the primer, posted some photos, then got distracted with urgent life things. I'm back, and painting again.

[This Vogons thread](https://www.vogons.org/viewtopic.php?t=66199) suggested *Rustoleum Chiffon Cream* as the colour, which fortunately our local hardware store had in stock. I built up a few thin coats on the external parts of the case over a period of days, letting it dry overnight between each coat because of the shockingly bad weather we've been having this winter.

One change I made was to place the pieces flat, instead of hanging them up for painting. It was comical just how little of the paint was making it onto the metal with the violent wind we've been having. The paint also ran a few times when I accidentally sprayed too thickly, which lead to some manual and power sanding to take it off; a topic for a separate post! I could control and direct the flow more easily with the panels laying flat.

This is what the pieces started to look like after a few coats:

<figure><p><img src="https://rubenerd.com/files/2024/paint-update@1x.jpg" alt="Pieces of the case drying in the closest thing to sun right now" srcset="https://rubenerd.com/files/2024/paint-update@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

I'll say that while the Chiffon Cream is closer in colour to my slightly-yellowed beige electronics than some other paints I've tried, though it's still a little on the bright side. If anything, it reminds me of the eggshell colour of the [Commodore VIC 20 breadbin case](http://retro.rubenerd.com/commodore.htm#commodore-vc-20), or a slab of white chocolate. I'm digging it.

The next step is to apply a few coats of satin clear, and to do some light buffing to remove some of the imperfections. I'm learning as I go, but the results so far are *significantly* better than the first time I attempted such a paint job on an old case. I'm tempted to take that old machine, sand it back, and redo it with the knowledge I've gleaned from this experience.
