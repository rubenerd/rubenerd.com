---
title: "My AeroPress has a new lease of life"
date: "2024-10-06T10:14:56+11:00"
year: "2024"
category: Hardware
tag:
- aeropress
- coffee
location: Sydney
---
I've waxed lyrical here about the AeroPress. I have all manner of kitchen gadgets to make coffee, including my beloved plastic Hario V60 I bought in Japan, but the AeroPress is my default. I can prep the exact amount of grounds and water I need the night before, so when I wake up I can boil, steep, and plunge a specialty roast that takes just as good as anything from most of our local coffee shops. It feels like a hack.

Alas, we recently had to replace my original one after almost a decade of faithful service. The rubber seal that affixes to the bottom of the pluger had been worn down from literally thousands of cups passing through it. This lead it to not forming a tight seal, meaning it didn't offer as much resistance. This lead to what Clara has referred to as some *coffee explosions*, and she wasn't that off the mark.

<figure><p><img src="https://rubenerd.com/files/2024/old-aeropress@1x.jpg" alt="" srcset="https://rubenerd.com/files/2024/old-aeropress@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

Unbeknownst to me though, you can replace just the rubber plunger seal itself. I found a local distributor that sells replacement parts, and sure enough, I can now bring my trusty original AeroPress back into service! This old guy has been around the world with me, was there for my first full-time job in Australia, and got me through Covid lockdowns. Not that I'm a sentimental fool for a few pieces of plastic, mind you.

The new AeroPress had only been used a dozen or so times, so I'll be giving it to my brother in law who's trying to get used to black coffee. As I tell everyone, a decent grinder, some lovely light to medium-roasted beans, and an AeroPress will take all that harsh bitterness out that so many people try to mask with additives, and leave you with something that's wonderful.
