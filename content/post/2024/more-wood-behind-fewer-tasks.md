---
title: "More wood behind fewer tasks"
date: "2024-02-16T14:50:23+11:00"
year: "2024"
category: Thoughts
tag:
- personal
- productivity
- task-management
location: Sydney
---
Alongside retrocomputing, this blog threatens to turn into one of those `#productivity` sites that were huge a decade ago. Oh well!

Recently I decided to take stock of every personal project and life thing I needed and wanted to do. I wrote a big, long text file and broke down everything into small chunks I could tackle. It's over 200 lines long, and covers everything from braces to Commodore 128 RAM upgrades.

I've really struggled lately with finishing anything in my personal life. I'll nibble at one thing before moving onto the next. I somehow&mdash;and for some reason&mdash;find it mentally easier to justify starting something new, instead of chipping away at an exiting thing. It's how I end up with an Apple //e Platinum when my C16 still needs a fixed reset line, or why my blog reader project has a great RSS parser and DB model, and no way to read in a list of feeds. After two years. It makes me feel like a failure, and guilty in a way that's hard to describe.

A funny and somewhat tragic side-effect of having a personal blog is that I can scroll back a few weeks, months, or years, and see all the half-finished projects and ideas. Many of you have asked over the years where I got up to on certain things too.

It was never a problem of being bored or lacking choices, it was the effect of not being able to *dedicate time to one thing*. It's just not how my brain works. It's not just how my brain works. It's brain not how my just works. 

I said words to this effect on Mastodon, and was surprised to read just how many others [share similar struggles](https://social.sdf.org/@signaleleven/111877602358420145). I've managed to project the idea that I have all these disparate hobbies with which I'm productive and have fun, but I assure you it's an illusion!

Taking stock of all I need and want to do has given me some clarity around this. I'm going to put more wood behind fewer arrows, to use that Google phrase from years past.
