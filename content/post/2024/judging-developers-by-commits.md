---
title: "Judging 9.5% of developers by commits"
date: "2024-11-28T08:24:48+11:00"
abstract: "Some fun new “research” from another person who doesn’t understand what development is."
year: "2024"
category: Ethics
tag:
- work
location: Sydney
---
[404Media provides us](https://www.404media.co/are-overemployed-ghost-engineers-making-six-figures-to-do-nothing/) an example of [Betteridge's Law](https://en.wikipedia.org/wiki/Betteridge%27s_law_of_headlines)\:

> Are Overemployed ‘Ghost Engineers’ Making Six Figures to Do Nothing?

The answer may surprise you!

The author quotes a Stanford researcher's... let's charitably refer to them as *findings*:

> “It is insane that ~9.5 percent of software engineers do almost nothing while collecting paychecks,” `$he` tweeted. “This unfairly burdens teams, limits creative disruption, wastes company resources, blocks jobs for others, and limits humanity’s progress. It has to stop.”

I added an item to that list. Can you spot which one? I'll give you a hint, it's not "limit humanity's progress".

How did he arrive at this conclusion? Was it by interviewing them and their colleagues about their roles and daily routines?
Seeing their task lists? Performance reviews?

>[&hellip;] tech companies have given his research team access to their internal code repositories (their internal, private Githubs, for example) and, for the last two years, he and his team have been running an algorithm against individual employees’ code.

Well, that's one metric. [Charles Goodhart](https://en.wikipedia.org/wiki/Goodhart%27s_law) would be proud. Where are the others?

> He said that this automated code review shows that nearly 10 percent of employees at the companies analyzed do essentially nothing, and are handsomely compensated for it.

This is what people are warning against when they say AI tools lack cognition and context. I could not have dreamed up a more ridiculous, clichéd misunderstanding of what developers do if I tried. Seriously, if you're a developer, how much of your time is actually spent writing and committing code? How much *work* is behind each of those changes? How many conversations? How much planning? Has this person ever sat in a room with stakeholders in, you know, the real world?

Tools like this would warn someone eating sushi that it's raw fish. *Ten percent of chefs in this Japanese food alley don't even cook! They're limiting progress!* He also thinks this can be solved with LLMs too, not that you needed me to tell you that.

If you have a manager who's started talking like this, (1) they don't respect you, (B) they don't understand your business, and (Γ) they lack the curiosity to address either. Run, don't walk to the emergency exits. Maybe push a few commits first though just to `fsck(8)` with him.

Thanks to [James Savage](https://social.axiixc.com/@axiixc) for posting this article today, I needed a laugh.
