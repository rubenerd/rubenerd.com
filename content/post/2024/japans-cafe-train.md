---
title: "Japan’s Shimatetsu Café Train"
date: "2024-07-20T09:02:32+10:00"
thumb: "https://rubenerd.com/files/2024/cafe-train@1x.jpg"
year: "2024"
category: Travel
tag:
- food
- japan
- nagasaki
location: Sydney
---
Clara and I watched the latest episode of [NHK's Japan Railway Journal](https://www.youtube.com/watch?v=XNISB_aiPhY), and... <span lang="jp">すごい</span>!!

<figure><p><img src="https://rubenerd.com/files/2024/cafe-train@1x.jpg" alt="Press photo from the offical site showing the Cafe Train." srcset="https://rubenerd.com/files/2024/cafe-train@2x.jpg 2x" style="width:500px; height:305px" /></p></figure>

[From the official site](https://www.shimatetsu-cafetrain.com/english/)\:

> “Shimatetsu Café Train” runs on a scenic railway route from Isahaya to Shimabara in Nagasaki Prefecture. It is a traveling café that allows you to enjoy delicious dishes and sweets from the Shimabara Peninsula. While you taste the delicacies, you can enjoy the scenic landscape, the idyllic countryside, the beautiful sea and spectacular Mount Unzen. We are sincerely looking forward to showing you the beauty of Nagasaki.

Sometimes I worry that Japan is too perfect for a shameless tourist like me. Trains, my favourite cuisine, trains, scenery, trains!?

It's been added to the list.
