---
title: "RSS readers that use titles from HTML?"
date: "2024-10-18T08:22:18+11:00"
year: "2024"
category: Internet
tag:
- atom
- rss
- troubleshooting
- weblog
location: Sydney
---
I put out a [request for comment](https://rubenerd.com/issue-with-feed-readers-not-showing-titles/ "Issue with feed readers not showing titles") on Wednesday regarding an odd issue Drew Jose reported. For some reason all my blog posts were only appearing with the title of the blog, not the post's actual title. This understandably makes reading and bookmarking posts frustrating.

I couldn't reproduce this behavior in [NetNewsWire](https://netnewswire.com/), [Thunderbird](https://support.mozilla.org/en-US/kb/how-subscribe-news-feeds-and-blogs), [Miniflux](https://miniflux.app/), [TheOldReader](https://theoldreader.com/), and [my own XML testbed](https://rubenerd.com/replacing-xpath-with-libxml-in-perl/)  so I asked Drew what client he was using. They did me one better, and showed me a screenshot of [Omnivore](https://omnivore.app/) with the offending behaviour:

<figure><p><img src="https://rubenerd.com/files/2024/drew-jose-reader@1x.jpg" alt="Screenshot from the Omnivore read later service." srcset="https://rubenerd.com/files/2024/drew-jose-reader@2x.jpg 2x" style="width:420px;" /></p></figure>

For those using screen readers or text-based browsers, the screenshot shows entries from  the lovely [Asherah Conner](https://kivikakk.ee), my favourite Nova Scotian [Gavin Anderegg](https://anderegg.ca/), and WP Engine's [Jason Cohen](https://longform.asmartbear.com/) appearing with correct titles. Underneath these is a recent post from this blog, but it only shows the title.

Why!? I had no idea. I spent more time looking at my RSS feed, and it seemed to work just fine. So I went to bed, dreaming of XML validators.


### Feedback from readers

I've received comments from Gabriel Garrido, [Nico Cartron](https://www.ncartron.org/), Rebecca, [Rene Kita](https://rkta.de), [Tom Jones](https://enoti.me/), [Diederick de Vries](https://thefoggiest.dev), and three of my regular anonymous readers who all reported no issues. As an aside, I was surprised at the diversity of readers people are using; the ecosystem is healthier than I thought.

I even received a message from none other than Brent Simmons of [NetNewsWire](https://netnewswire.com/) and [Inessential](https://inessential.com/) fame, which I'll shamelessly admit made my morning!

> I saw your note about an RSS issue — titles not appearing correctly. I took a look at your feed and it looks fine. Tested it in my feed reader and it worked using both the in-app parser and with Feedbin. And the [feed validator](https://validator.w3.org/feed/check.cgi?url=https%3A%2F%2Frubenerd.com%2Ffeed%2F) says it’s fine.

Everyone seemed to confirmed what I was seeing: my feed was completely valid.

So why was Drew having issues? Was it my slightly more exotic XML namespaces I have in my RSS feed? Was it confused by the mix of Dublin Core and standard RSS 2.0 elements? Did it not like that I was using XSLT to view the feed?


### An issue with HTML titles

Then a pattern started emerging. [Torb Lunde Miranda](https://torb.no), Dennis Blondell-Decker, [Hales](https://halestrom.net/darksleep/), [Bix Frankonis](https://slow.dog/), and Paul Freeman noted something about the site itself I hadn't noticed: Hugo wasn't rendering titles in the HTML for individual blog posts.

This is the HTML title for my homepage:

    <html lang="en-SG">
        <head>
            <title>Rubenerd</title>
    […]

And until yesterday, this was the title for each blog post:

    <html lang="en-SG">
        <head>
            <title>Rubenerd</title>
    […]

*Whoops!* The joys of writing your own theme. This was a [one-line fix](https://codeberg.org/rubenerd/rubenerd.com/commit/646b29a69bfb2a10539eb12b314e2f04dba95702), so now blog posts have a meaningful HTML `<title>`.

This lead me to wonder if this was related. Were RSS readers pulling this incomplete `<title>` from the HTML, and not using the provided RSS `<title>`? And if so... why would they?

I'm not certain, but I have some theories.


### How RSS deals with post data

RSS can be... fun when it comes to post encoding, as anyone will tell you who's tried to parse a range of them.

One of the challenges is how different CMSs encode posts. RSS has the `<description>` element, which can be plain text, HTML escaped like Radio UserLand of yore, or encoded as [XML CDATA](https://www.w3.org/TR/REC-xml/#sec-cdata-sect). Which one is in use? That's up to you to figure out. This ambiguity resulted in the more prescriptive `<content:encoded>` being [defined](http://web.resource.org/rss/1.0/modules/content/), and Atom being [specific](https://www.rfc-editor.org/rfc/rfc4287#page-14) with a MIME `type` attribute on its `<content>` element. This is a bit of a theme.

Another challenge is *how much* of a post is encoded. Sites like mine [deliver the entire post](https://rubenerd.com/feed/), but other authors prefer to only include a summary. Some RSS readers work around this by offering to pull the entire text of a post and present it in the same reader interface (I'm on the fence about the ethics of this, but that's a separate discussion).

And finally, as Dave Winer has reminded us many times on [Scripting News](http://scripting.com/), the `<title>` element is entirely optional in the various RSS and Atom specs. RSS readers treats an `<item>` without a `<title>` in different ways, but it wouldn't surprise me if some are loading the page referenced with `<link>` and deriving the title from this as a shortcut.

Through a combination of these, some RSS readers seem to hit a code path where they *always* ignore supplied `<title>` elements in an RSS `<item>`, and derive it from the HTML site instead. I'd say this is a bug: a local RSS `<title>` should always be preferred if supplied.


### Conclusion

Regardless of what caused this, posts from my silly blog here should now have titles in your RSS reader, bookmark tools, and "read later" services. I've fixed the bug in my theme, so agents that use the HTML `<title>` will get a meaningful title, not just the name of the blog.

Thanks to all of you for your feedback, advise, screenshots, and troubleshooting tips. I learned a lot from this.
