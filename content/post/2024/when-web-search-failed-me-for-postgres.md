---
title: "When web search failed me for Postgres"
date: "2024-07-11T08:24:34+10:00"
year: "2024"
category: Internet
tag:
- databases
- mysql
- postgres
- search-engines
location: Sydney
---
We've all witnessed the precipitous decline in search engine result quality of late, though I still seemed to have slightly better success than others. That string of good luck ended this morning. Wait, maybe I should say "middling" luck.

I knew earlier this year that someone had presented at a PostgreSQL event regarding a new compatibility layer for MySQL queries. I remembered they used a cheeky title like "MySQL is now PostgreSQL", or something similar. The idea was that you could use this patched version of Postgres with a plugin to talk with applications written against MySQL. I use both in production, so the idea was *incredibly* intriguing.

Do you think I could find it?

Every combination of keywords I used returned pages upon pages of superficial, chatbot-generated comparisons between the two databases, and nothing of value whatsoever. *MySQL versus Postgres: What you need to know. Did you know that a database is like a spreadsheet that…* 

This was the first search I'd done in a while where *every* result, on *every* page I tried, was slop. I knew quality had slid, but I didn't realise just how bad it was. I had a prepared joke about selecting from `DUAL`, because that's how [battling](https://en.wikipedia.org/wiki/Duel) these search engines feels... but that's an Oracle reference.

Eventually, I went through my own bookmarks and found the talk from the Postgres Conference 2024 by [Jonah Harris and Mason Sharp](https://nextgres.com/res/20240419-The-Future-of-MySQL-is-Postgres.pdf). *Phew!*
