---
title: "My own comment rules (and advice)"
date: "2024-04-30T07:39:30+11:00"
year: "2024"
category: Internet
tag:
- comments
location: Sydney
---
In my [unintentional series about comments](https://rubenerd.com/explaing-how-or-why-something-is/ "Explaining how or why something is"), today I want to address the other unproductive responses I get when I'm mentioned on sites like Hacker News run by [well-adjusted people](https://newrepublic.com/article/180487/balaji-srinivasan-network-state-plutocrat "The New Republic: The Tech Baron Seeking to “Ethnically Cleanse” San Francisco").

Nothing here is unique or special, to be clear! But I suspect following these guidelines will get you far in life in general, not just when emailing a small-time blogger in Australia.

1. **Comprehension:** Read the post. Try to understand *why* people do or think something, rather than projecting your views onto it. It is *obvious* when someone hasn't tried, or stopped reading after the title.

2. **Details:** Asking for clarification is fine; [some recipes call for it](https://en.wikipedia.org/wiki/Clarified_butter "Wikipedia article on clarified butter"). Being deliberately obtuse isn't.

3. **Kindness:** I don't suffer rude people. [If in doubt, be polite](https://rubenerd.com/if-in-doubt-be-polite/). Otherwise, it's redirected to `/dev/null`.

4. **Main Character Syndrome:&trade;** Not all posts are targeted specifically at you or your constraints, environments, experiences, histories, kinks (heavens), locales, occupations, positions, perspectives, priorities, requirements, reservations, skills, and/or toolchains. Wouldn't life be boring if it was?

5. **Entitlement:** You can make a request in good faith, but you can't demand anything from me. In the voice of the Doctor from Star Trek Voyager: *I'm a blogger, not a support line*.

6. **Interests:** You don't need to tell me if you don't understand or share in my hobbies and tastes. You don’t get far telling BSD people that nobody uses it, or shouting at a vtuber concert that weebs suck. All else aside, we already know.

7. **Humour:** Sometimes I say things in jest (ingest; indigestion?), or obviously embellish for dramatic effect. It's an Australian thing. No, I don't *literally* think `$TOOL` is worse than pestilence. Or... hmm.

8. **Proving negatives:** Asking why I *didn't* do something is, to quote Merlin Mann, like asking *why are you not a potted fern?* 

9. **Context:** Personal blogs are not the same as news outlets, sky writing, official documentation, or academic journals. While I hold myself to standards of honesty, personal blogs are opinionated, subjective, and a bit scrappy. I don't run double-blind tests on coffee or package managers.

10. **Seeing the forest for the trees:** Telling me it's *actually* GNU/Linux, when in my case it's [not even factually true](https://rubenerd.com/tag/alpine-linux/), contributes nothing to a broader discussion. Worse, it makes me want to engage with you *less*.

11. **Locale:** I love travelling in the United States, have worked there, and want to visit again. But not everyone who writes in English is American, or lives in the US. **I cannot overstress this!** When you say "spring", is it yours or mine? What does 12/2/12 mean? If you assume I'm American, what else do you think you've got wrong?

I apologise if these appear flippant, but I've got tired answering the same dozen questions over and over (and over!) again. If you have a blog or a sufficiently large social media profile, you know what it's like. 

To the majority who are interesting and civil, even if we disagree, thank you. Sincerely. You're the reason I still publish an email address and Mastodon profile, despite being tempted many times to remove them. There are more of you than I appreciate sometimes, and I have to remind myself of that.

