---
title: "Personal Wi-Fi hotspots at coffee shops"
date: "2024-08-19T15:48:20+10:00"
thumb: "https://rubenerd.com/files/2024/wifi-networks.png"
year: "2024"
category: Internet
tag:
- wifi
location: Sydney
---
I get this odd kick out of seeing hotspots. The punny ones, the ones with emojis, the ones with statements, and the ones where people didn't change the default name of their phone. Sometimes I'm seeing it via a macOS dropdown, other times via `wpa_supplicant(8)` on FreeBSD.

Especially at airport coffee shops, you see names in all sorts of languages and scripts. It's like a little global community, brought together temporarily and possibly never again.

<figure><p><img src="https://rubenerd.com/files/2024/wifi-networks.png" alt="List of hotspots showing up on macOS" style="width:346px; height:295px;" /></p></figure>
