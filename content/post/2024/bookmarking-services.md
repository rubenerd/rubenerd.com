---
title: "Self-hosting a bookmarking tool"
date: "2024-12-15T08:21:42+11:00"
abstract: "Anyone have suggestions before I bake one myself?"
year: "2024"
category: Internet
tag:
- bookmarks
location: Sydney
---
I was a massive fan and user of [del.icio.us](https://rubenerd.com/tag/delicious/ "View posts tagged with delicious") back in the day, then I [moved to Pinboard](https://rubenerd.com/moving-from-delicious-to-pinboard/ "Moving from del.ici.ous to Pinboard") when the renamed Delicious was Yahoo'd. It works great, but I'm self-hosting most other things, and this seems like a prime candidate.

Before I learn more Python and bake one myself (or do it in Perl), anyone have any they like? I don't need it to be a "read later" style tool, nor does it need page archiving. It's just a place to put bookmarks, tagged with... tags.

Right now I've seen people use plugins for Nextcloud, but I'm also not using that right now.


