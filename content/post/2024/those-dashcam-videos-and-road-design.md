---
title: "Those dashcam videos and road design"
date: "2024-12-13T09:20:52+11:00"
abstract: "If you design roads that encourage speeding, people will speed on them."
year: "2024"
category: Thoughts
tag:
- cars
- public-transport
- safety
location: Sydney
---
I'm sure every country has a channel like [Dash Cam Owners Australia](https://www.youtube.com/c/DashCamOwnersAustralia). As the name suggests, it features videos submitted by Australian drivers of events caught on their dashcams, from cars doing dangerous things, to Thomas the Tank Engine crossing an intersection.

It's tempting to write off such videos as clickbait, sensationalist, or even lowbrow, but I'd argue they're performing a vital public service. They remind motorists, cyclists, and pedestrians to be vigilant, lest a vehicle dash out in front of you without warning. Likewise, if you drive recklessly, there's a chance you&mdash;and your car licence plate&mdash;might end up featured. If that scares even one motorist away from merging without indicating, gunning it at a red light, or *[chucking a uey](https://en.wiktionary.org/wiki/chuck_a_uey "Wiktionary definition for chuck a uey")* at a busy intersection towards a crowd of pedestrians, the videos have paid for themselves.

The social utility of such media are worth exploring in more detail one day. But for the purposes of this post, dashcam videos are also an insight into a part of Australia I rarely see: the suburban landscape that makes up the majority of the country. Clara and I have always chosen to live in walkable neighbourhoods close to train stations and amenities, but most Australians commute by car to their detached home in suburbia somewhere. The Sydney Metro and train systems are decent, but the car rules the roost... so much so that politicians will pander to them with [rebates that make absolutely no sense](https://rubenerd.com/can-i-use-toll-rebates-for-public-transport/ "Can I use toll rebates for public transport?").

<figure><p><img src="https://rubenerd.com/files/2024/dashcam-australia@1x.jpg" alt="Screenshot from a recent dashcam video, showing a wide, straight road with turning lanes." srcset="https://rubenerd.com/files/2024/dashcam-australia@2x.jpg 2x" style="width:500px;" /></p></figure>

Watching these dashcam videos, I'm transfixed not by the impending sense that something is about to happen, but the **environment itself** in which these cars are driving. Between neighbourhoods are wide, straight roads with massive shoulders for turning, little in the way of vegetation, and plenty of shimmering, hot bitumen. They're not as impressive (for want of a better word) than the [stroads 📺](https://www.youtube.com/watch?v=ORzNZUeUHAM "NotJustBikes video on Stroads") of North America, but it's clear their design is optimised for car throughput above any other road user, whether they be on a motorbike, acoustic unicycle, or foot.

I'm *sure* this has an impact, both mentally and in terms of safety. I've walked across [and been stuck on](https://rubenerd.com/getting-stuck-in-the-middle-of-a-stroad/ "Getting stuck in the middle of a stroad") a few of these six and eight-lane roads, and it's a miserable experience. It's hot, loud, smelly, dusty, and the roads and cars aren't that great either. Wait, what?

But the other thing these massive roads do is *encourage speeding*. By design.

You know the saying that *fences make good neighbours?* It's a comment on the fact that setting clear boundaries can negate a lot of friction and problems between people. Sure, you could legislate what someone can and can't do on property that isn't theirs, but a fence makes it unambiguous where people's rights and responsibilities begin and end.

The key is, you have:

* Rules

* Designs that encourage *adherence* to said rules

Australian roads, like most in the world, do the former but not the latter. We spend millions in taxpayer money putting up speed limit signs, and paying traffic police to enforce them (I love when TV news used to report where speed cameras would be the following day too, because safety is for squares and "revenue raisers"). But if you still design the road like a drag-racing circuit... what do you expect people will do? All that's missing from them are chequered flags!

As Australian cities get denser and more populated, we're going to have to think harder about where our priorities are here, and whether we're really serious about safety and mobility... *especially* in light of how big personal vechiles are becoming thesedays. [In the words](https://www.newscientist.com/article/2140747-laws-of-mathematics-dont-apply-here-says-australian-pm/ "New Scientist: Laws of mathematics don’t apply here, says Australian PM") of former prime minister Malcolm Turnbull, "the laws of [physics] are very commendable, but the only law that applies in Australia is the law of Australia." Except, that isn't true at all!

Unicycles.

