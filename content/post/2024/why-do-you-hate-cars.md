---
title: "Why do you hate cars?"
date: "2024-05-04T07:57:34+10:00"
year: "2024"
category: Thoughts
tag:
- cars
- public-transport
location: Sydney
---
All my talk lately of urbanism and public transport has lead some emails concluding that I hate cars. I'm sympathetic to this potential mischaracterisation, especially when I've referred to them as smoke machines, death traps, and other accurate descriptors.

To clear the air first, *I'd take the train!* My dad was a car and motorbike enthusiast, especially of the Italian variety. I had posters and model Ferraris alongside the trains, aeroplanes, and rocket ships that every stereotypical boy had in the 1990s. When we moved to Singapore, some of my most treasured memories involved going with my dad up to Sepang to see the Grand Prix (I was a Williams fan, you'd never guess his favourite).

Even today on this blog, I still mention the stunning 1930s <a href="https://en.wikipedia.org/wiki/Auburn_Speedster#/media/File:Auburn_851_Speedster_(1935)_-_49118499802.jpg">Auburn Speedster 851</a>, and the retro-futurism of the second 1980s <a href="https://en.wikipedia.org/wiki/Aston_Martin_Lagonda">Aston Martin Lagonda</a>. I appreciate them as industrial works of art, just as I do the RMS *Lusitania*, or the lampshade iMac.

But this reverence actually belies my core issue with cars. Ask anyone what their dream vehicle would be, or the one they'd *prefer* to be driving, and they'll mention such luxury cars, sports cars, or luxury sports cars. Nobody is excited by the Toyota Corolla. These are utilitarian workhorses, not something to enjoy.

Which leads us to the unfortunate truth. Since the mid 20th century, cars became a requirement to live in much of the world. What proponents see as four-wheeled symbol of freedom, I see a mobility tax. You want to participate in society? Get a licence, pay for the car and insurance, and get used to refuelling it. What's free about that, in any sense of the word?

I've had people cheekily tell me that I must have be against horse-drawn carts, but that underplays the scope of the problem. Our urban environments have been fundamentally altered to accommodate these machines, whether it be massive new freeways that displace neighbourhoods, the encouragement of unsustainable fringe developments, the opportunity cost to more inclusive forms of transport, the huge maintenance liability with which they burden us, the demand such roads induce, and the millions they kill each year. On every metric, from environmental, social, health, and financial, this experiment has been a disaster.

So no, I don't hate cars specifically, I hate car dependency. Cities should be built around public transport, cycling, and walking first. The best time to have started advocating and building this was a century ago. The next best time is now.
