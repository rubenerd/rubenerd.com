---
title: "My FourMore photo project"
date: "2024-01-09T08:01:48+11:00"
thumb: "https://rubenerd.com/files/2024/fourmore@1x.jpg"
year: "2024"
category: Media
tag:
- photos
- projects
location: Sydney
---
I started something silly! Introducing *[FourMore](https://fourmore.tumblr.com/)*:

<figure><p><a href="https://fourmore.tumblr.com/"><img src="https://rubenerd.com/files/2024/fourmore@1x.jpg" alt="Screenshot from FourMore, showing collections of four things for each day." srcset="https://rubenerd.com/files/2024/fourmore@2x.jpg 2x" style="width:500px; height:333px;" /></p></a></figure>

It's four related things I find on my daily walk, every day, for no reason whatsoever. None of them are especially good photos, I just like collecting things.

I'm hosting it on Tumblr because it's easy to post from the phone, though I'll eventually bring it back in-house.
