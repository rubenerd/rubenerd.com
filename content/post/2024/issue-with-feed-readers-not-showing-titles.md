---
title: "RFC: issue with feed readers not showing titles"
date: "2024-10-16T08:27:33+11:00"
year: "2024"
category: Internet
tag:
- rss
- web-feeds
- weblog
location: Sydney
---
Drew Jose emailed me recently with a strange bug. Their feed reader doesn't show the title of posts correctly, instead just printing "Rubenerd" each time. This is, understandably, a bit frustrating for them.

I haven't messed with the RSS template code for a while now, though I did update [Hugo](https://gohugo.io/) recently. Do any of you have the same issue? I haven't been able to reproduce.

**Update:** I've [written a follow-up](https://rubenerd.com/follow-up-on-missing-rss-post-titles/ "RSS readers that use titles from HTML?").

