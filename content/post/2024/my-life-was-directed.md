---
title: "On life being directed"
date: "2024-11-05T22:09:22+11:00"
year: "2024"
category: Thoughts
tag:
- personal
- quotes
location: Sydney
---
I haven't been able to get [this quote by B. F. Skinner](https://en.wikiquote.org/wiki/B._F._Skinner) out of my head all week:

> I did not direct my life. I didn't design it. I never made decisions. Things always came up and made them for me. That's what life is.

I worked hard to get where I am. I studied hard, maintained hobbies that also turned into career, fell in love, and built a home (so to speak). By most measures of success, I'm living it.

There's a *bit* of satisfaction in knowing that hard worked paid off. But I don't feel entirely responsible... or even *mostly*. My genes, where I was born, how I was raised, and the opportunities I had were not of my doing. Whether nature or nurture, I wasn't responsible.

Likewise, it seems every *big* decision I've made has come by chance. There's cool hobby, want to explore it? Your family is moving again, want to experience a new place? A university is offering exactly what I'm after, would you like to study? The anime club has a wickedly smart and cute girl who happens to have a crush on you too, want a date? What about this job offer, do you accept? What about this home you inspected, want it? What about all these lovely people who read my ramblings and take the time to email me, want to hear from them?

Life is hard. But I also feel as though the successes I've had also just "came up". I guess in my longwinded way, I'm living proof that B. F. Skinner was right. It's not optimistic or pessimistic, it's facts.

*(I mistyped that last word as <strong>fax</strong>, which yes, I'm still one of the last people in the world who has such a machine. That decision was entirely of my own choosing, because I'm an insufferable retro fool. Did I say fool, I meant genius. Wait, no, that first one was right).*
