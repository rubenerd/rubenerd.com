---
title: "A thread about people who need to run Windows"
date: "2024-04-14T07:52:30+10:00"
year: "2024"
category: Software
tag:
- linux
- replies
location: Sydney
---
Hi! My name is Ruben Schade, and I'm a solution architect for an indie cloud company who mostly runs Linux, but tries to use FreeBSD and NetBSD where he can. This is my story. **LAW AND ORDER DUN DUN**


### The post

Yesterday I saw the news that Microsoft will be including ads in an upcoming Windows 11 update, which so perfectly encapsulates [all that's wrong with modern IT](https://en.wikipedia.org/wiki/Enshittification). I saw a few people post about it on Mastodon, which generated dozens of comments from Linux fans saying the real solution was to switch to their OS.

[Before I went to bed, I posted](https://bsd.network/@rubenerd/112264116393309434)\:

> Linux people, **please** understand this. Sometimes people need to run Windows. They’re allowed to complain about Windows ads, or tracking, or any other enshittification problems, without you saying “use Linux” every time.

Suffice to say, I woke up to a storm of certain Linux people&hellip; not understanding this. But a few themes emerged, which I thought I'd respond to in aggregate here instead.


### Replies that miss/ignore the point

* **I don't understand why someone would need to run Windows.** If I offered you $1,000, I'll bet you could think of some reasons. Don't take me up on that though, I'm trying to save for a house. *In this economy!?*

* **Using Windows makes you a cog in the capitalist machine.** This assumes agency. Also, [no](https://donotreply.cards/en/do-not-explain-capitalism-to-me).

* **I'm philosophically against Windows.** I might be too. It's irrelevant.

* **You don't have a right to complain if you use Windows.** Big [we should improve society somewhat](https://knowyourmeme.com/memes/we-should-improve-society-somewhat) vibes with this.

* **You deserve all the hate coming your way.** I've certainly learned my lesson.

* **It's GNU/Linux.** No comment. Wait, that's a comment. *Fsck.*

* **I don't understand why you're so angry/frustrated/etc.** Probably the most honest thing they could have said.


### Replies that address the point

* **Run within a VM.** This at least solves the technical issue of certain hardware or software not working on Linux. Running a VM isn't always feasible, but the point at least acknowledges that Windows is unfortunately still necessary.

* **Use Wine, or some other shim.** Again, not always tenable, and you're smart enough to know why. But the community is making incredible strides, even in gaming.

* **I love how many people are proving your point.** Self-awareness definitely seems lacking among a cohort of Linux people, even though we may see eye to eye on a lot of things. That does make me a bit sad.
