---
title: "Yerevan on my mind"
date: "2024-07-10T20:51:14+10:00"
year: "2024"
category: Travel
tag:
- armenia
- latvia
- urbanism
location: Sydney
---
If there were two specific places that have had an oversized influence on my life, they'd be Latvia and Armenia. Family friends I grew up with in Australia and Singapore; people I counted as close friends at university; the assignments I did in high school; even the calligraphy my late mum did... could often be traced to one (or both) of these places. Not bad for countries that have populations lower than Singapore, an island barely the size of an Australian farm! The suburb of Sydney in which I currently live also has a sizable Armenian population, and my favourite coffee shop has a regular Armenian meetup.

Armenia in particular has been in my thoughts recently on account of their geopolitical situation. The shocking and opportunistic ethnic cleansing of Armenians out of the formerly autonomous Nagorno-Karabakh region of Azerbaijan pushed this traditionally Russian-aligned country towards the West. The invasion of Ukraine sent many Russian professionals fleeing to Armenia, a couple of which I count among readers of my blog here (it's been made all the more confusing by neighbouring Georgia's bizarre recent moves, though that's a topic for another post).

But it's here that I must confess something. Here's my current FreeBSD workstation wallpaper:

<figure><p><img src="https://rubenerd.com/files/2024/yerevan@1x.jpg" alt="Photo of Yerevan on my FreeBSD desktop" srcset="https://rubenerd.com/files/2024/yerevan@2x.jpg 2x" style="width:500px;" /></p></figure>

This is view of The Cascade and surrounding park in Yerevan, taken by [Gerd Eichmann on Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Yerevan-Cascade-02-2019-gje.jpg). I was fascinated by the design of the structure, but the landscape and trees were also quite relaxing.

It sounds weird, but I tend to view wallpapers as aspirational. During the first wave of COVID when we were all stuck at home, I had [this one park bench in Ōsaka](https://rubenerd.com/the-best-blog-park-bench-in-the-world/ "The best blog park bench in the world") as my wallpaper. I see these pictures every day, and they give me something to work towards. Armenia is going to be painful to get to from Australia, but Clara and I are committed to heading that way eventually!

Which [gets us to a comment](https://sigin.fo/@antranigv/112742381192902988) from my friend and BSD extraordinaire [Antranig](https://antranigv.am)\:

> Some developers are adopting the (sorrynotsorry) shitty US-style single/double houses, while others are doing the right thing and renovating old soviet buildings and asking the municipality to put some kind of public transportation and (hey here's a wild idea) sidewalk around these buildings. I know who's gonna win in the end. This is not a taste issue. It's an economics issue.

Channels like *[Not Just Bikes](https://www.youtube.com/@NotJustBikes)* talk about cities having "good bones" when it comes to culture and good urbanism. This often, but not always, comes down to the city being developed before the advent of the car... and Yerevan is one of the oldest settlements in the world. If any city in the world had a shot at maintaining its history and growing with good urbanism, it'd be Yerevan.

Yerevan, at least all the photos I've been obsessed with of late, is gorgeous. A unique place like that shouldn't be drowned out by American/Australian-style detached housing that destroys environments, take in more in maintenance costs than they contribute to local economies, and leave people isolated. Fingers crossed the perpetrators of this development style get some pushback.
