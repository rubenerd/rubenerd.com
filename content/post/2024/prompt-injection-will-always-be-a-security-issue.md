---
title: "Generative AI is a security vulnerability"
date: "2024-08-22T10:27:53+10:00"
year: "2024"
category: Internet
tag:
- generative-ai
- security
location: Sydney
---
[Thomas Claburn, writing for The Register](https://www.theregister.com/2024/08/21/slack_ai_prompt_injection/)\:

> "Slack AI uses the conversation data already in Slack to create an intuitive and secure AI experience tailored to you and your organization," the messaging app provider explains in its documentation.
> 
> Except it's not that secure, as PromptArmor tells it. A prompt injection vulnerability in Slack AI makes it possible to fetch data from private Slack channels.

I [asked in May 2023](https://rubenerd.com/twitter-circles-feature-leaked-data-as-expected/) how long it would take for the likes of Slack to have a data leak. Who would have thought such a leak would be self-imposed?

None of this should be surprising, though I'm glad it's getting coverage. To be crystal clear: **there is no safe use for this tech with proprietary or confidential information**. Data leaks aren't merely an unavoidable or inevitable byproduct, they're intrinsic to how these tools operate.
