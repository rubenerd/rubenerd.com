---
title: "Making coffee at home as an economic signal"
date: "2024-08-25T09:03:32+10:00"
year: "2024"
category: Thoughts
tag:
- coffee-shops
- economics
location: Sydney
---
Australia is dealing with a cost of living crisis, as is much of the world. [Jonathan Barrett wrote an article](https://www.theguardian.com/australia-news/article/2024/aug/24/from-cars-to-coffee-machines-heres-how-australian-spending-habits-are-weathering-the-high-cost-of-living "From cars to coffee machines, here’s how Australian spending habits are weathering the high cost of living") about the last full-year reporting season:

> [S]pending patterns remain uneven, and at times counterintuitive, leading to a mixed corporate earnings season marked by subdued but not collapsing demand.

He mentions coffee machines as an example of how certain Australian retailers and manufacturers have bucked the trend:

> Shares in home appliance manufacturer and distributor Breville surged after recording a year of record sales, despite the broader weakness in consumer spending.
> 
> That uplift was underpinned by double-digit growth from its coffee category, with strong sales overseas, particularly in the US, as demand for espresso grows. Sales were also healthy in Australia.

He cites the trend of retailers "slashing pricing" and "tweaking their product ranges" to appeal to younger buyers, but I think there's a simpler explanation. Coffee machines are cheaper than cafes! [As Reuters reported last year](https://www.reuters.com/business/ground-down-australia-coffee-shops-an-early-inflation-casualty-2023-07-10/), demand for cafes in Australia has slumped hard:

> The A$10 billion ($6.6 billion) Australian cafe industry, the world's biggest outside Europe per capita, is shaping as an early, visible casualty of a perfect storm of rising utility bills, produce costs, wages and rents plus a slowdown in discretionary spending brought on by interest rate hikes, say economists and people in the industry.

I think coffee machines are useful economic indicators, because people buy them to supplement or replace more expensive spending elsewhere during tight periods.
