---
title: "Numbers that don’t have nine in them"
date: "2024-05-05T09:13:42+10:00"
year: "2024"
category: Thoughts
tag:
- numbers
- pointless
location: Sydney
---
In past posts I've listed numbers that [don't have seven](https://rubenerd.com/it-has-numbers-in-them/), then [did it again](https://rubenerd.com/it-also-has-numbers-in-it/), and then clarified numbers that [don't have eight](https://rubenerd.com/numbers-that-dont-have-8-in-them/). Today we're looking at numbers that don't have nine. I provide this service free of charge.

* 42
* Nine

Wait, damn it.
