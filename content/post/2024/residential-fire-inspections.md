---
title: "Residential fire safety and inspections"
date: "2024-03-08T09:13:28+11:00"
year: "2024"
category: Ethics
tag:
- economics
- housing
- safety
- standards
location: Sydney
---
Famed writer of Shit Rentals [@PurplePingers](https://twitter.com/purplepingers/status/1762020646018834694) wrote a post recently talking about fire safety in residential buildings, which caused a bit of a furore. Most people accepted the idea of electrical and fire inspections every two years; others eloquently articulated their opposition by calling people "stupid".

*Welcome to the Internet! Can I take your order?*

This is actually something in which I have experience. I've been one of the designated fire wardens in a couple of jobs, both in industrial and commercial office settings. Singapore's [SS5xx standards](https://www.scdf.gov.sg/firecode2023/firecode2023/standards-codes), and Australian Standard [AS1851-2012](https://www.standards.org.au/standards-catalogue/standard-details?designation=as-1851-2012) mandate six months to a year between inspections for fire suppression systems, alarms, hydrants, doors, smoke alarms, emergency lighting, warning signs, and other measures. It's critically important, especially when you're dealing with chemicals that could leak, catch fire, or explode.

Some of these fire monitoring systems are fascinating, especially if you're at all interested in process control and automation! One control room I briefly used to sit in looked like *Star Trek*, albeit with more mechanical buttons and fewer lens flares. But I digress.

<figure><p><img src="https://rubenerd.com/files/2024/sample-of-coc@1x.jpg" alt="" srcset="https://rubenerd.com/files/2024/sample-of-coc@2x.jpg 2x" style="width:362px;" /></p></figure>

*[Example fire safety conformity document](https://www.scdf.gov.sg/home/fire-safety/plans-and-consultations/regulated-fire-safety-products) published by the Singapore Civil Defence Force website.*

Residential settings introduce their own challenges. Apartment buildings will have their own fire systems similar to office and retail spaces. The strata in our building do quarterly reviews of smoke detectors and alarms, which is above and beyond what's legally required, but demonstrates a level of maturity missing even in some commercial settings. If and when they're needed for real, I have confidence in their operation.

Unfortunately, most people in Australia live in detached dwellings. Without sprinkler systems, lots of combustible material, and only a recommendation for functional smoke detectors, most people in houses are sitting ducks. Everyone has a friend, colleague, or family member who's either been affected by this, or knows someone who has. It's scary.

The problem is exacerbated for renters, whom by definition don't have the ability to modify their accommodation. Replacing dodgy wiring, hazardous insulation, and flammable fixtures are entirely at the discretion of the landlord who holds all the cards. There's no point demanding what you're legally entitled to if the landlord or agent can later evict you for someone who'll play ball (a fact routinely missed when armchair experts say "just sue them!").

Mandated fire, gas, and electrical safety inspections are a way to address this power imbalance, mitigate safety concerns, and codify responsibility. In [New South Wales](https://www.fairtrading.nsw.gov.au/housing-and-property/key-changes-to-smoke-alarm-requirements-for-rented-homes), smoke detector inspections and necessary repairs must be carried out annually. Regulations also require the completion of an [Annual Fire Safety Statement](https://cfspnsw.com.au/faqs-for-tenants/) by landlords, which may require inspections to ensure the safety of tenants. I'd consider it a necessary and important responsibility if I were a landlord, up there with making sure the places I rent out are free of black mould and have functional roofing.

Personally, I think even these policies are too lax. Granted, industrial settings require more frequent inspections owing to their more hazardous materials, but residential buildings are loaded with lithium batteries, natural gas piping, and furniture that release toxic gases when burned. We're far too *blasé* about this, until it's too late.

Some bright sparks (thank you) on the original thread suggested that even five years was "unwarranted", because anything more frequent was government overreach and would cost too many lives. Pardon, money, cost too much money. This irresponsible attitude would have you laughed out of any room of qualified fire fighters and inspectors.

Thanks again to @PurplePingers for continuing to advocate for marginalised people.
