---
title: "Midnight McCartney, John Pizzarelli"
date: "2024-11-04T08:23:50+11:00"
year: "2024"
category: Media
tag:
- jazz
- john-pizarelli
- music
- music-monday
- paul-mccartney
location: Sydney
---
Today's *[Music Monday](https://rubenerd.com/tag/music-monday/)* is actually on a Monday for once! Who would have thought.

Clara's and my rediscovered obsession with Paul McCartney continues unabated, to the point where I introduced her to John Pizarelli's 2015 album *[Midnight McCartney](https://www.youtube.com/watch?v=-3nXPs5yLxI)*. So the story goes, Paul approached John to do an album of his music, and he took him up on the offer.

John is one of my all-time favourite modern jazz performers. He's written plenty of music in his own right, but his gentle voice, guitar, and swinging band hit just the right chord with me. I tend to reach for his stuff when I'm feeling stressed, or want to unwind after a long day.

*Midnight McCartney* is a great album if you're into smooth jazz, but it's especially fun if you're familiar with Paul's work. My favourite has to be his take on *[Let ’Em In](https://www.youtube.com/watch?v=WyIILQeryyc)!*

<figure><p><a target=_BLANK href="https://www.youtube.com/watch?v=WyIILQeryyc" title="Play John Pizzarelli - Let 'Em In (Live)"><img src="https://rubenerd.com/files/2024/yt-WyIILQeryyc@1x.jpg" srcset="https://rubenerd.com/files/2024/yt-WyIILQeryyc@1x.jpg 1x, https://rubenerd.com/files/2024/yt-WyIILQeryyc@2x.jpg 2x" alt="Play John Pizzarelli - Let 'Em In (Live)" style="width:500px;height:281px;" /></a></p></figure>

If you want to learn more, I [wrote the Wikipedia article](https://en.wikipedia.org/wiki/Midnight_McCartney) :).
