---
title: "Importing worlds on a multiworld Minecraft server"
date: "2024-04-11T07:47:39+10:00"
year: "2024"
category: Software
tag:
- guides
- minecraft
location: Sydney
---
It's funny that I field almost as many sysadmin questions about Minecraft than I do BSDs thesedays. Even Minecraft *on* BSD! This post addresses the most common question after [running Minecraft on FreeBSD](https://rubenerd.com/how-we-run-a-minecraft-server/ "How we run Minecraft"), and [on NetBSD](https://rubenerd.com/netbsd-can-also-run-a-minecraft-server/ "NetBSD can also run a Minecraft server"). I'll assume here that you have a functional server, and know how it works.

Say you have a map running on your local Java Minecraft install, and you want to import it as another into a Java multiworld server running Paper (as I recommend), such as one running [MyWorlds](https://www.spigotmc.org/resources/myworlds.39594/) or [Multiverse](https://www.spigotmc.org/resources/multiverse-core.390/). How do you do this?

<figure><p><img src="https://rubenerd.com/files/2024/runbsd-minecraft@1x.jpg" alt="The pixelart section of our primary Minecraft world showing the RUNBSD sign!" srcset="https://rubenerd.com/files/2024/runbsd-minecraft@2x.jpg 2x" style="width:500px; height:333px;" /></p></figure>


### Locating your Minecraft data directory

First, access your saved local world in the appropriate location:

* Windows: `%appdata%\.minecraft`
* Mac: `~/Library/Application Support/minecraft/`
* Linux: `~/.minecraft/`

Within this folder, you'll have a folder called `world`. This contains your world data, along with subfolders for the associated Nether (`DIM-1`) and End (`DIM1`) worlds. These are generated when you first create the world, even if you've never accessed them:

* `./world/`
    * `./DIM-1/`
    * `./DIM1/`


### Importing without mutliworld, and some context

Importing is easy on a Minecraft server without multiworlds. You copy the `world` folder into the Minecraft server folder before starting the server, then run. If you're importing into an existing server, you'll want to backup and move the existing `world` folder elsewhere first before replacing it, unless you don't need it anymore.

You'll notice that after importing, the server has rearranged the worlds into their own folders in the server directory:

* `./world/`
* `./world_nether/`
* `./world_the_end/`

This works, but what if you use a multiworld plugin and want a custom folder name for this world?


### Importing with a multiworld plugin

It's important to note that the Minecraft server will only import `world` folders. If your imported world is called something like `resourceserver`, it won't import the Nether or End worlds automatically. This might not matter to you, but you'll end up teleporting to your server's default Nether or End, not the ones you expected.

The temptation is to rename `DIM-1` to `world_nether`, and `DIM1` to `world_the_end`, as several forum posters and AI-generated guides suggest. **This doesn't work**, because the other two worlds are missing metadata. Thanks GPT, you *monumental* waste of resources!

The solution I've found is to temporarily rename the imported world as `world`, let the server import it, then rename it to what you wanted. In more detail:

1. Stop the Minecraft server, and temporarily rename the existing server `world` folder to something else, like `world_backup`.

2. Copy your local imported `world` folder into the server, leaving the name of the folder set as `world`.

3. Start the Minecraft server, letting it rearrange the folders into `world`, `world_nether`, and `world_the_end`, then generate the required metadata for each.

4. Stop the Minecraft server again, then rename the folders to what you want, such as `resourceserver`, `resourceserver_nether`, and `resourceserver_the_end`.

5. Rename your original `world_backup` back to `world` again, assuming you want this to be the default.

6. Start the Minecraft server again, and import your world.


### Importing the world, and its attached Nether and End

If you use MyWorlds like I do, you can now import the worlds from the game with these:

    /myworlds load resourceserver
    /myworlds load resourceserver_nether
    /myworlds load resourceserver_the_end

Donezo.
