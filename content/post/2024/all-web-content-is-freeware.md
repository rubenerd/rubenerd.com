---
title: "All web “content” is freeware"
date: "2024-06-29T06:58:44+10:00"
year: "2024"
category: Ethics
tag:
- generative-ai
location: Sydney
---
Sean Endicott [quoted a CNBC interview](https://www.windowscentral.com/software-apps/ever-put-content-on-the-web-microsoft-says-that-its-okay-for-them-to-steal-it-because-its-freeware) with Microsoft's CEO of AI, and it's nothing if not entertaining!

> "With respect to content that is already on the open web, the social contract of that content since the 90s has been that it is fair use. Anyone can copy it, recreate with it, reproduce with it. That has been freeware, if you like. That's been the understanding," said Suleyman.

This is the same company behind [this famous letter in 1976](https://en.wikipedia.org/wiki/An_Open_Letter_to_Hobbyists). Perhaps that's why he bookended his claims with "since the 90s". But that means torrents of Windows are freeware! Maybe he should have run this by legal first.

Easy gotchas aside, this rambling, incoherent interview was a fascinating and *deeply* revealing perspective into how managers are thinking. It's dawning on them that they've lost the financial argument, because these models are unsustainable to anyone not [selling the shovels](https://quoteinvestigator.com/2021/04/20/gold-shovel/). Model decay has revealed the emperor has no clothes when it comes to tools "learning" or "being inspired" as artists are. The general public are beginning to equate "AI generated" with low effort and low quality, coining a [new term in the process](https://www.thestar.com.my/tech/tech-news/2024/06/21/slop-is-the-dubious-online-content-churned-out-by-ai "‘Slop’ is the dubious online content churned out by AI"). There are also indications that [peak AI](https://omny.fm/shows/better-offline/are-we-at-peak-ai "Better Offline: Are We At Peak AI?") may already soon be upon us, given they're rapidly exhausting their supply of organic material to train against.

Backed into an ethical, financial, mathematical, and legal corner, generative AI vendors are now resorting to arguing everything is fair game, because it always was. Don't blame us, the [Torment Nexus](https://knowyourmeme.com/memes/torment-nexus "Know Your Meme: The Torment Nexus") is established practice!

This gives me the opportunity to address a point a lot of tech pundits are now making: a chatbot is no different from a search engine. The "social contract" here is that search engines could crawl our pages, create indexes, data mine them based on secret algorithms, and present processed results based on a query. A generative AI chatbox is no different, right?

Except, and I know this may come as a shock: *search engines link to their sources!* Chatbots don't. That's what makes their "hallucinations" so dangerous; there's no audit trail. Search engines deliver traffic, generative AI tools train against data to avoid doing that. The "social contract" here is completely upside down, as though a tool was asked to generate an image of it.

This is the surest sign to me that we're in a bubble again: the talking heads are beginning to believe their own nonsense. That's another form of model decay, now that I think about it.
