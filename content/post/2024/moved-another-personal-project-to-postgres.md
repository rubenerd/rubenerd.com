---
title: "We moved our home DB to PostgreSQL"
date: "2024-09-22T20:48:14+10:00"
year: "2024"
category: Software
tag:
- bsd
- databases
- freebsd
- homelab
- postgresql
location: Sydney
---
This is one of those unhelpful status updates with no detail whatsoever! But I successfully spent part of yesterday moving Clara's and my internal DB on our FreeBSD homelab box to Postgres. Everything in our homelab (and home life) revolve around a single database, from helper scripts to budgets. It's a mess, but it's *our* mess, and it's something that's been oddly fun to see grow over the years. I tend to start with schemas or data structures before writing any program; it's just how my mind works.

`KiribenDB` started its life briefly as a SQLite3 database, before moving to MySQL in the late 2010s. There's nothing exotic or interesting about it, save for it being fully BCNF'd, which I still maintain sounds like an American rail company. This made it easy to export, change a few suble lines of syntax, and import as Postgres. No indexes, no fancy data types, just your run-of-the-mill stuff.

I can't say I've seen any difference in functionality or performance since porting our Perl and other scripts to talk Postgres, which I'm taking as good news. It also means one fewer database I'm running at home, because our web feed reader, Minecraft Dynmap server, and Nextcloud also use Postgres DBs. *Huzzah!*

Back in January I talked about the process and idea behind [dual-stacking MariaDB and PostgreSQL](https://rubenerd.com/mariadb-versus-postgres/). I've since moved back to MySQL over MariaDB on servers that still need it, for reasons I may also get into one day. But the path to slowly moving to Postgres is now well underway!
