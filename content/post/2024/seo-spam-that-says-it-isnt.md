---
title: "SEO spam that says it isn’t"
date: "2024-04-12T11:39:17+10:00"
year: "2024"
category: Internet
tag:
- spam
location: Sydney
---
This put a smile on my face this morning:

> Note: - Though this is not an automated email, (i.e., to ensure that we do not contact you again for this matter), please send a blank mail to it with NO as Subject.

Not automated! This post wasn't written by me as I drink coffee either! If you believe this, I have a bridge to sell and all that.

This reminds me of that spam that assures you they've been scanned by an anti-virus. Literally it'd be the easiest line to add if you were going to send viruses.
