---
title: "Questions about modding my Blueberry iMac DV"
date: "2024-03-12T12:20:17+11:00"
year: "2024"
category: Hardware
tag:
- apple
- imac
- imac-dv
- retrocomputers
location: Sydney
---
Below is my beloved Blueberry iMac DV, albeit taken at a weird angle and on the floor because I forgot how *difficult* it is to photograph! It's basically a giant reflective sphere that messes with focus and colour balance unless you have a really good quality diffused light source, *which I don't!* It's a hard life.

<figure><p><img src="https://rubenerd.com/files/2024/imac-dv@1x.jpg" alt="Bad photo of a wonderful machine." srcset="https://rubenerd.com/files/2024/imac-dv@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

My parents bought this for me for Christmas in 2000 after I couldn't stop raving about the Macs I was using at school. Its bright colours and friendly OS embodied so much of the optimism and happiness I had during that period of my life, to the point where it still makes me smile looking at it today.

This was a machine of firsts for me. It was the first computer I cut video on, thanks to iMovie and its FireWire ports. It ran the very first Mac OS X betas. My iBook G3 was the first computer I booted a BSD on (NetBSD), but this iMac was the first to specifically boot PowerPC FreeBSD. I started my iTunes library on it that I still maintain today. It was also where I played the most SimCity 3000 and Sims, thanks to Aspyr.

### Zzzzzap

Unfortunately, the poor thing hasn't worked for more than a decade. The CRT was the first component to go, with the picture becoming blurry from the centre before eventually fizzling out. I was able to use an external display for a few more years, until a house move from KL to Singapore in the late 2000s jostled it enough to damage the logic board (2000s Apple parlance for motherboard). There are no signs of life when turning it on now.

I want to do something about it, but I'm a bit unsure how to proceed.

First, I definitely want to get as much of the original hardware working again. This will almost certainly involve pulling it apart and testing as many parts as I can individually.  The good news is replacement logic boards don't seem to be too difficult to find, and I've got plenty of RAM sticks to try.

I'm not confident enough with electronics to touch the power supply, so that might be something I source a replacement for if I detect any anomalous voltages. It's nearing twenty-five years old anyway, so it might be due (and no, I haven't plugged it into mains in at least four years, so I'm not about to electrocute myself on a cap).

Another option is to gut the machine entirely, and put a more modern PowerPC Mac Mini board inside, then route the cables out the side. This would be a bit of a kludge, but it would let it run classic Mac software in that familiar shell again.


### Replacing the display

The CRT is another issue. As much as I love retrocomputers, CRTs give me bad headaches after a few minutes of use now, from expensive Trinitrons to old phosphor units. I can keep the CRT disconnected and use an external monitor with it like I did before, but that seems a bit sad.

Alternatively, a sillier side of me wants to replace it with an LCD panel. This would make the machine significantly lighter and easier to move with, and would mean I could actually use it. The challenge is that the bezel is curved to fit the CRT, which means I'd need to fill the gap between it and the flat LCD somehow. I've seen some people print 3D gap fillers with varying degrees of success, but they seem to be highly dependent on the panel you source. As much as it pains me to say, some of these conversions look pretty terrible.

I think I've already overthought this, so step zero is just to check if I can get any part of it working again to start. If and when this is done, I can evaluate how to proceed. If you have any ideas or suggestions, I'm all ears.



