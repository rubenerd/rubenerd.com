---
title: "Windows Server asking weird questions"
date: "2024-04-04T08:11:56+11:00"
year: "2024"
category: Software
tag:
- networking
- security
- windows
location: Sydney
---
I had a dream a few nights ago in which an ice cream vendor asked me which SAS backplane I needed. Before I had time to answer, he'd already connected the cone to a 2U server somehow, and the ice cream starting shorting things and blowing up circuits, which I then had to explain to my boss who was The Dude from *The Big Lebowski*.

Was this related to this post? Almost certainly not. But maybe a bit? Because I got this question when deploying a new Windows Server for a client recently:

<figure><p><img src="https://rubenerd.com/files/2024/windows-server-appropriate@1x.png" alt="Windows Server asking: Do you want to allow your PC to be discoverable by other PCs and devices on this network? We recommend allowing this on your home and work networks, but not public ones." srcset="https://rubenerd.com/files/2024/windows-server-appropriate@2x.png 2x" style="width:500px; height:250px;" /></p></figure>

I... huh? You want to plug ice cream into what? 🎳

I've recently started teasing out *excuses* from *reasons*. The *excuse* here would be that this came from desktop Windows, and that a hobbyist community of home servers exist. But is that an appropriate *reason* to have it ask about home networks by default, especially when it's far more likely the server is going to be running in a corporate environment running IIS, Active Directory, or RDS? I'd say no.

I feel like this must be a [boiled frog](https://en.wikipedia.org/wiki/Boiling_frog#As_metaphor "The boiling frog metaphor on Wikipedia") situation. If you live your life running Windows Server, you probably dismiss these questions without a second thought. I certainly feel this in desktop Windows when I have to boot back into it.
