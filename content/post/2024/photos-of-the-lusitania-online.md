---
title: "Photos of the RMS Lusitania online"
date: "2024-02-28T20:07:36+11:00"
year: "2024"
category: Media
tag:
- architecture
- engineering
- ocean-liners
- photos
location: Sydney
---
I've been on a bit of an ocean liner kick again of late, as you've probably noticed from recent posts. They were engineering and design marvels, brought people closer together, and shaped world events with their size, speed, and splendour.

Alas, researching such vessels on the web isn't always *smooth sailing*. **AAAH!**  The enduring notoriety and mystery surrounding the *Titanic*, and to a lesser extent the *Lusitania*, have made them prime candidates for online *churnalism*, where accuracy is less important than generating something on which to serve ads.
 
A victim of these articles, or perhaps a promulgator themselves, is *Vintage Everyday*. Let's take a look at some examples from a couple of their historical articles, though I don't mean to just pick on them here.

In their <a rel="nofollow" href="https://www.vintag.es/2019/09/the-Lusitania-exterior-and-interior.html">2019 article</a> on the liner, they share some photos from an incredible collection published by the Southern Methodist University's DeGolyer Library [on Flickr](https://www.flickr.com/photos/smu_cul_digitalcollections/albums/72157647351186054/). I warn you, don't go there unless you want to lose yourself for a while.

Here's an image they captioned "First class promenade on the boat deck". This photo also appears in a <a rel="nofollow" href="https://www.vintag.es/2021/03/rms-lusitania-interior.html">2021 repost</a>, this time without a caption.

<figure><p><img src="https://rubenerd.com/files/2024/not-the-lusitania@1x.jpg" alt="Photo of the RMS Mauretania, showing its large cowl ventilators." srcset="https://rubenerd.com/files/2024/not-the-lusitania@2x.jpg 2x" style="width:416px; height:500px;" /></p></figure>

This is not the *Lusitania*, but her sister ship *Mauretania*. Unlike White Star's *Olympic* and *Titanic*, Cunard's ocean greyhounds were built by different ship yards, and had more noticeable differences in their external design and interior fitout. *Mauretania* used gigantic cowl vents, and had rectangular windows on her boat deck. *Lusitania* used canister-like vents, and had rounded windows on her boat deck.

Contrast this with another photo from the same page, and the differences become obvious:

<figure><p><img src="https://rubenerd.com/files/2024/lusitania-boat-deck@1x.jpg" alt="Photo of the Lusitania, showing her rounded windows and canister-like vents." srcset="https://rubenerd.com/files/2024/lusitania-boat-deck@2x.jpg 2x" style="width:375px; height:306px;" /></p></figure>

I grant *Vintage Everyday* a pass here, because the mistake came from the DeGolyer Library themselves. Their [original photo](https://www.flickr.com/photos/smu_cul_digitalcollections/8891973886/in/album-72157647351186054/) erroneously refers to the vessel as the *Lusitania*, which a couple of people commented on. You'd think an academic institution would have their steel ducks in a row, so to speak.

This next photo is a bit more odd. In their 2021 repost, this lower-resolution photo of *Lusitania's* bridge was inserted:

<figure><p><img src="https://rubenerd.com/files/2024/not-the-lusitania-bridge@1x.jpg" alt="Photo of the RMS Olympic's flat bridge." srcset="https://rubenerd.com/files/2024/not-the-lusitania-bridge@2x.jpg" style="width:400px; height:261px;" /></p></figure>

People who know the *Titanic* will immediately recognise this as her aforementioned sister ship *Olympic*, and not the *Lusitania!* Incidently, you can quickly tell this isn't *Titanic* because the forward A Deck promenade doesn't have glass windows. The bridge wings (that small box with two square windows below the second funnel here) are also flush with the superstructure.

There are almost too many differences between the *Lusitania* and the *Olympic* to mention. But the most obvious are *Olympic's* flat bridge, rectangular forward windows, and the lack of Cunard's signature black reinforcement bands on her funnels. *Lucy* and *Maury* had beautiful curved bridges and rounded superstructures, and didn't have a lower well aft of their black forecastle decks (*Lusitania* was built with a white forecastle which looked rather fetching, but was painted black in Cunard style after her sea trials).

Again, we turn to [another image](https://www.flickr.com/photos/smu_cul_digitalcollections/8891352783/in/album-72157647351186054/ "View of the bridge from navigation deck") from the original DeGolyer Library for comparison. While they're both ships with funnels and white superstructures, they're clearly very different scales and designs.

<figure><p><img src="https://rubenerd.com/files/2024/lusitania-bridge@1x.jpg" alt="Photo of the forward deck on the Lusitania, with her smaller, rounded bridge and superstructure in the background." srcset="https://rubenerd.com/files/2024/lusitania-bridge@2x.jpg 2x" style="width:420px; height:348px;" /></p></figure>

The photo didn't come from the DeGolyer Library collection this time, so this was a mistake by *Vintage Everyday*. Whether they were careless, or just lacked sufficient knowledge of the subject matter isn't clear. We all make mistakes, so I won't pass judgement.

What I *will* say is that it's important to get your facts right when you're writing a post (or producing a podcast or video). This experience demonstrates how trivial it is to spread misinformation about historical photos and events; mistakes that percolate and wash across the web like sea foam to be reposted elsewhere. It's a little bit scary to extrapolate that out to every historical event and topic being shared online today, with each generation of mistake or error compounding our misunderstanding of the world.

This post was already getting too long, so I didn't include the most widespread and surprising example of this I've seen! I'll save it for next week.
