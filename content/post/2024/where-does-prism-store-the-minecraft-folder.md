---
title: "Where Prism stores the Minecraft folder"
date: "2024-10-21T08:01:12+11:00"
year: "2024"
category: Software
tag:
- games
- guides
- minecraft
location: Sydney
---
If you use a launcher like [Prism](https://prismlauncher.org/) or [PolyMC](https://polymc.org/) instead of the offical Minecraft launcher, where does it store the `Minecraft` folder? I ask, because I was going bananas trying to figure out how to import a local world.

When you launch Prism, click **Folders** in the toolbar, and choose **Launcher Root**, you're given a directory tree that looks like this:

    .
    ├── PolyMC_nomigrate.txt
    ├── accounts.json
    ├── assets
    ├── catpacks
    ├── icons
    ├── iconthemes
    ├── instances
    ├── libraries
    ├── logs
    ├── meta
    ├── metacache
    ├── polymc.cfg
    ├── prismlauncher.cfg
    ├── themes
    └── translations

Within the `instances` folder, you'll have your Minecraft install. Or so all the forum posts and guides tell us:

    .
    ├── 1.21.1
    │   ├── instance.cfg
    │   └── mmc-pack.json
    └── instgroups.json

But... *where's the Minecraft folder?* Turns out, it's a hidden directory under the appropriate instances folder:

    .
    ├── 1.21.1
    │   ├── .minecraft
    │   │   ├── coremods
    │   │   ├── downloads
    │   │   ├── icon.png
    │   │   ├── logs
    │   │   ├── mods
    │   │   ├── resourcepacks
    │   │   ├── saves
    │   │   └── server-resource-packs
    │   ├── instance.cfg
    │   └── mmc-pack.json
    └── instgroups.json

Now you can import your worlds to the `saves` folder, as you would using the vanilla Minecraft installer.
