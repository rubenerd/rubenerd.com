---
title: "Pieces of 8: BMCs"
date: "2024-02-21T18:52:48+11:00"
year: "2024"
category: Hardware
tag:
- pieces-of-8
- pointless
- servers
location: Sydney
---
Baseband management controllers are what separate desktops (and even some billing themselves as workstations) from proper servers.

Surprisingly, BMCs can also stand for other things, including:

* Brisbane Melbourne Canberra
* Big, mighty cranium
* Bedok MacPherson… Canberra
* Brandishing my cutlass
* Brewed morning coffee
* Bánh mì chicken
* Belgians make chocolate
* Borrowed mandolin case

Thank you.
