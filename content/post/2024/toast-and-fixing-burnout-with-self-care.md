---
title: "Toast, and fixing burnout with self-care"
date: "2024-02-14T11:00:05+11:00"
year: "2024"
category: Thoughts
tag:
- burnout
- health
- mental-health
- work
location: Sydney
---
This podcast by the Scientific American was titled *[You Can't Fix Burnout With Self-Care](https://www.scientificamerican.com/podcast/episode/you-cant-fix-burnout-with-self-care/)*. I left the episode with almost more questions than answers, surprisingly enough.

One of the co-authors Shayla Love concludes:

> After thinking about it, I’ve decided that as far as frameworks go for understanding our frustrations, exhaustion and malaise, I can think of worse ones. And that’s because burnout, as it was originally conceived, is a social problem. It’s not that you are tired of parenting or volunteering for no reason. It’s probably because you’re not getting enough help, you don’t feel like you’re making an impact and you’re not in control of your schedule or what you’re doing. You can’t take on the job of feeling better all by yourself. We need help, and we need to help each other.

That last line broadly matches the sentiment of one of her guests, Anthony Montgomery from Northumbria University:

> The most important part of burnout is that it’s about yourself, but it’s also about others.

They're right, of course. The painful irony of burnout is that so many suffer from it, but they do so alone. Only by tackling our circumstances, and working together with the people in our professional and personal lives, can we hope to make a meaningful and lasting difference. *We're all in this together*, to borrow a cliché.

But this is where my own lived experience differs slightly from what these clearly far more qualified people prescribe. In fact, the way I treated my last major burnout was *specifically* self-care. I took a break from work, got a hotel in the mountains (well, what passes as mountains in Australia), and communed with nature for a fortnight. I worried at the time that I was attempting to escape my problems, but I did came back more motivated to change things. Some of the behaviours I adopted after this have helped me a lot with where I stand today. The race is long, as the *Sunscreen Song* says, but in the end, it's only with yourself.

A dear friend I've talked with fairly recently went through their own burnout, only their fortnight was a year. They raised the point that some see burnout like toast; namely, you can't *unburn* it, or turn it back into bread. But you can scrape off some off the char, and make something nice out of it. They were just trying to figure out what that looked like for them.

Sometimes I do walk into work or family commitments feeling a bit like toast, so clearly there's more work to do. Maybe I need to take more of that collaborative approach to addressing the problem, as Shayla and Anthony describe.
