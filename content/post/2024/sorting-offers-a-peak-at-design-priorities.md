---
title: "Sorting offers a peek at design priorities"
date: "2024-04-19T13:00:25+10:00"
year: "2024"
category: Software
tag:
- algorithms
- design
location: Sydney
---
*Sorting* is one of those classic Dunning-Kruger topics in information science: you *think* it's easy, until you start uni and learn how to implement it. Sorting algorithms vary in complexity, efficiency, and speed, and are optimised for different data sets, structures, and systems. One algorithm or process that's optimal for one setting won't be for another. There is no universal sorting tool, just as there isn't a universal cipher, or compression algorithm, or coffee bean.

I don't bubble around with implementing sorting algorithms anymore, but I'm wise to their effects. I think the way a system sorts something offers an interesting insight into the priorities, design choices, and assumptions made by its developers.

Performance is one obvious metric, but another I love to check is how a system or application sorts numbers. Or more specifically, how it *treats* numbers. Take this hypothetical set of integers:

* 12
* 2
* 128
* 7

If you or I were asked to sort this in ascending order, we'd quickly return this:

* 2
* 7
* 12
* 128

But a computer is just as likely to do this:

* 12
* 128
* 2
* 7

This doesn't make any rational or logical sense on first glance, until you realise the number is being sorted in *alphabetical* or *lexicographical* order, not *numeric*. 1 comes before 2, which comes before 7, and then the next character in each number is processed. It's one of those weird things that makes logical sense, and still seems absurd.

<figure><p><img src="https://rubenerd.com/files/2024/sorted-files-mac.png" alt="A screenshot showing a list of files sorted in numeric order, not alphabetic" style="width:500px; height:220px;" /></p></figure>

With the exception of the Mac OS Finder, most file system explorers and shell tools have traditionally sorted numbered files the same way as all other characters. This keeps sorting simple, because you can apply the same filtering across everything. But it can be an endless source of frustration; how many times have you had a folder open that puts folder 2 before 20, then 3 before 30?

*(It's part of the reason people like me have long preferred ISO 8601 dates formatted with `YYYY-MM-DD`. It's not only to remove ambiguity with American dates, it's so a computer will sort files and folders with timestamps correctly, regardless of system).*

Another great example is the English article *the*. Most traditional lists undergoing sorting (like indexes) will rearrange words to place *the* at the end, or ignore them completely. I remember being pleasantly surprised when my first iPod sorted artists like this:

* The Beatles
* Chicago

While my Linux music player at the time sorted them like this:

* Chicago
* The Beatles

This is a great example of the tension that exists between software design and accessibility. Well-designed software will accommodate human ideas like this, even though it's more complicated to implement. People writing badly-designed software will only offer excuses.
