---
title: "Singaporeans delaying or giving up on kids"
date: "2024-04-21T08:29:58+10:00"
year: "2024"
category: Thoughts
tag:
- economics
- society
location: Sydney
---
*This was a draft post from November 2023 I just found again.*

[Jewel Stolarchuk wrote in The Independent](https://theindependent.sg/40-of-young-singaporeans-do-not-expect-the-next-generation-to-have-children-new-survey/), a Singaporean news website:

> SINGAPORE: With statistics showing that Singaporeans have begun to delay the age at which they have children, with the situation worsening over the past ten years, a new survey has found that a whopping 40 per cent of young Singaporeans do not expect the next generation to have children.

I haven't checked this specific survey, but if the report is anything to go by, it's part of a broader global trend. Places like Australia have shored up a sharp drop in birthrate with immigration, but that also feels like it's delaying the inevitable.

The reason I bring this up though is that I find the responses just as interesting:

> Experts have suggested that implementing measures to alleviate the financial burden associated with raising children and reducing the pressure of educational competition could help create a more family-friendly environment in Singapore. Doing so would help citizens feel better equipped to embrace parenthood.

It's always three things with me, *and this is no exception!*

The first is that it's telling people think it's a problem in the first place. We've built our economic systems to be growth dependent, meaning we need an ever-expanding population to make and buy things to merely break even. If we acknowledge Earth is a closed system with finite resources, and that economists can't (in fact) bend the laws of physics, this can't continue indefinitely.

But even if we punt this problem into the future like we always do, it does seem strange that the state is so dependent on growth, yet the cost for those children must be borne by individuals. When humans lived in small groups, it was a collective effort bringing up and looking after the children. There was an understanding that they were literally the future of our families, tribes, clans, or collectives.

Modern society is expensive, complicated, and stressful. Throwing subsidies at people may address one of these, maybe a bit of two, but I doubt they sway many people who otherwise don't want kids to consider it. There are far larger, structural issues that governments everywhere are seemingly incapable or unwilling to address. It fascinates me that they talk big about this being a problem, but dance around any real solution like so many Bluey opening jingles.

Wait, I had a third thing here too, but I can't remember what it was. If I had a kid, I could ask her to remind me. Let's pretend it was me reminding everyone to watch Bluey. It's great.
