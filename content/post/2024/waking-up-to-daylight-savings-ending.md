---
title: "Waking up to Daylight Savings ending"
date: "2024-04-07T05:16:35+10:00"
year: "2024"
category: Thoughts
tag:
- personal
- timezones
location: Sydney
---
Daylight Savings (do we capitalise that?) ended across south-east Australia early this morning, so it's now 05:16 and I'm wide awake. Specifically, awake enough to know you won't be getting back to sleep.

I tended to find that I'd go to sleep earlier and wake up earlier during school holidays back in the day, and I guess it's still my natural inclination now. 

It might be nice to sit out on the balcony with a coffee and watch the sunrise. I'm already seeing some light streaks of blue behind the clouds which are stunning. Late risers miss out!
