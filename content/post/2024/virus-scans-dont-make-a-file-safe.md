---
title: "Virus scans don’t automatically render a file safe"
date: "2024-08-19T15:07:08+10:00"
year: "2024"
category: Thoughts
tag:
- privacy
- security
location: Sydney
---
I saw this in the footer of someone's technical forum post:

> All files have been analysed for malware with VirusTotal, and have shown a positive output, being completely safe to install.

I have thoughts!

* English wasn't the person's primary language, so I empathise and understand logically why they said "positive output". But testing "positive" for something you don't want isn't good. You'd be better saying a file tested "negative" for viruses on a virus scan, or "passed" the test if that's confusing. As an aside, isn't English fun!?

* Saying an executable was "analysed" as "safe" with a virus scanner is meaningless, because anyone can make the claim! If anything, I'm *less* inclined to trust an email or service claiming to be virus free, because it's precisely what a malicious actor would claim.

* A virus scanner can't be used to prove a file is "completely" safe. The tool may exploit an attack vector unknown to the virus scanner, or behave in a novel way that it hasn't been trained to detect. At best, one can claim it passed a test at a specific point in time and/or with a specific version of the engine.

* Even if the file is deemed safe, it may still leak, harvest, or sell personal information without your explicit consent, or be released by a company with a poor track record of security and privacy, or interact with other components in your system that can result in unexpected or undesirable behaviour. These are (generally) outside the scope and remit of a service like VirusTotal that scan individual files, or even more sophisticated threat analysis tools.

I know, I'm preaching to the choir here! But it's important that these claims are challenged. Blanket statements like this give laypeople a misplaced sense of trust, which is dangerous and counterproductive.

*(As an aside, we also have to consider that such public advice will soon be fodder to be stolen and regurgitated by a generative AI to confuse and mislead millions more people, without providing citations for people to verify. Fun)!*
