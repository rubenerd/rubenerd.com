---
title: "Fixing pagination in Hugo 0.135.0"
date: "2024-10-04T10:41:20+10:00"
year: "2024"
category: Software
tag:
- gohugo
- troubleshooting
location: Sydney
---
The depracated syntax for pagination was removed in Hugo 0.135.0. I had this line in my theme:

	{{ .NextPage.Permalink }}

It generated this error:

	ERROR deprecated: .Page.NextPage was deprecated in Hugo v0.123.0 and \
    will be removed in Hugo 0.136.0. Use .Page.Next instead.

So I replaced it with this, as instructed:

	{{ .Page.Next.Permalink }}

This also didn't work:

	execute of template failed: [...] <.Page.Next.Permalink>: \
    nil pointer evaluating page.Page.Permalink
	
[Checking the docs](https://gohugo.io/methods/page/next/), the permalink syntax is:

    {{ .RelPermalink }}

Changed it to that:

	{{ .Page.Next.RelPermalink }}

This also didn't work:

	execute of template failed: [...] <.Page.Next.Permalink>: \
    nil pointer evaluating page.Page.Permalink

The solution? I was using `{{ if .Page.Next }}` to check if there was a new page. Instead, use `with`:

	{{ with .Page.Next }}
        {{ .Permalink }}{{ .Title }}
    {{ end }}

[Hugo](https://gohugo.io/) is the best static site generator, and I definitely need to contribute. But I'm also looking forward to some API and syntax stability.
