---
title: "Can I control a system failure?"
date: "2024-01-02T10:45:46+11:00"
year: "2024"
category: Thoughts
tag:
- systems
location: Sydney
---
I have to interact with one specific bank that, when things are working, is fine. I can log in, do what I need to do, and leave. I get notifications when I need them, scheduled transactions go through, I can download CSVs of my billing periods, *the whole shebang*. When I'm a standard customer, doing standard things, in a standard way, there aren't problems. Standard.

You can probably see where this is going.

The problems set in when I want to deviate even *slightly* from what's normal, standard, or expected. Suddenly the façade of this well-oiled machine collapses. It seizes up, lubricant between joints starts smoking, and you hear metal grinding sounds. When this happens, I know I have a long afternoon of phone calls and follow up explaining why things are a certain way. At times even the poor customer service agent is laughing along with me at the absurdity of the situation as they attempt to escalate to someone with access they don't.

As a counter example, take Aussie Broadband. Their web UI is okay, but I know that if something gets borked, or I don't get a IPoE DHCP lease, I can log in and reset the port on their side without having to call someone. It's the difference between a five minute fix, and not having Internet for a day.

A large part of engineering robust systems is evaluating how well they handle failure or edge cases. It's easy to say a system works well when it... works. It's a different kettle of fish when it doesn't.

But as an end-user, I'm just as interested in whether systems fail in ways *where I still have control*. Even just having visibility can be the difference between being angry at a situation, and at least accepting it while the gears on the backend are re-oiled.

