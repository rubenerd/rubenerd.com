---
title: "Happy Trans Day of Visibility"
date: "2024-04-01T09:11:39+11:00"
year: "2024"
category: Thoughts
tag:
- human-rights
- love
location: Sydney
---
I'll admit, it feels a bit weird saying *happy* in relation to a day that shouldn't need to exist. This isn't an event like Mothers Day where we lavish attention on people we think deserve it, it's so trans people are *seen*.

Still, I hope my trans friends had a nice day yesterday, and that it does get easier. We can all help by making them feel loved and respected. 🏳️‍⚧️

