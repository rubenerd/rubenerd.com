---
title: "There is no attribute HERF"
date: "2024-11-11T13:07:35+11:00"
year: "2024"
category: Internet
tag:
- html
- pointless
location: Sydney
---
I was fixing a page at work&mdash;like a gentleman&mdash;but I couldn't figure out why a simple link wasn't being respected. I must have read over the output of this script at least a dozen times, if not more.

As always, the [W3C Validator](https://validator.w3.org/#validate_by_input "The W3C Validator with direct input") came to the rescue:

> Line 36, Column 308: there is no attribute "`HERF`"

Heh, `HERF`. No wonder.

As an aside, I might have a new name for my next sci-fi book I'll write and never publish. Captain Herf, of the RMS *Validator*, at your service. He had an illustrious career, but was always two letters away from true *grate*-ness.
