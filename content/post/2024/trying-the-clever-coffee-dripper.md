---
title: "Trying the Clever Coffee Dripper"
date: "2024-12-02T08:38:30+11:00"
abstract: "It's a forgiving full-immersion brewer that makes tasty coffee!"
thumb: "https://rubenerd.com/files/2024/clever-brewer@2x.jpg"
year: "2024"
category: Thoughts
tag:
- coffee
- food
- reviewers
location: Sydney
---
This morning was Clara's and my first time using our new Baratza Encore ESP grinder, and the [Clever Coffee Dripper](https://cleverbrewing.coffee/products/clever-dripper), a Taiwanese take on a filter or pourover-style coffee maker. It's too much fun!

While superficially resembling a Hario V60, Bee House, or the funnel of a Chemex, the Clever is a full-immersion brewer, meaning the water is only released after steeping. This is achieved with a nifty valve mechanism that remains closed while you prepare, and opens when placed atop a mug or carafe. This way, all the water&mdash;save for the bypass behind the filter&mdash;is used in the brew. *Clever!*

<figure><p><img src="https://rubenerd.com/files/2024/clever-dripper-press@1x.jpg" alt="Press photo of the Clever Dripper" srcset="https://rubenerd.com/files/2024/clever-dripper-press@2x.jpg 2x" style="width:500px; height:355px;" /></p></figure>

We broadly used [James Hoffman's technique](https://piped.video/watch?v=RpOdennxP24 "The Ultimate Clever Dripper Technique")\:

1. Set our new coffee grinder to 22, which the guide recommended for use in the Hario V60. I think we'll go a bit finer next time.

2. Folded and rinsed the filter paper, placed it in the brewer, set on the scales, and tared.

3. Poured 250 g of boiling water into the brewer, then tipped in 16 g of coffee. This was discovered by [Workshop Coffee](https://piped.video/channel/UCNvZVxsHV0KTEASMxpmVIhg) to drastically improve drawdown time, without affecting flavour!

4. Gave the delicious slurry a light stir to make sure there weren't any clumps of dry coffee.

5. Timed it steeping for 2 minutes.

6. Gave the brewer a gentle shake, which immediately sent the grounds that had float to the top back down.

7. Waited another 30 seconds.

8. Lifted the brewer up and onto Clara's mug. Within about 30 seconds, all the coffee had drawn down.

The resulting coffee was sweet, mild, and tasty, even with our imperfect technique. I definitely think we'll try grinding finer next time, stirring the slurry a bit more at the start to prevent clumping, and folding the paper better. You can see in the photo that we ended up with coffee on the sides, when it should all be in the bed.

Still, even on our first attempt, and with a less than ideal distribution, it still tasted *great!* Dare I say, I might even prefer it to the AeroPress.

<figure><p><img src="https://rubenerd.com/files/2024/clever-brewer@1x.jpg" alt="Photo of our home coffee setup, with the Clever Coffee Dripper in the centre." srcset="https://rubenerd.com/files/2024/clever-brewer@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

It's definitely a more involved process than an AeroPress or French press for brewing. Making sure the paper is folded correctly and rinsed is something you don't need to worry about when dealing with a simple disc of AeroPress paper. But not needing a precise water pouring technique first thing in the morning is appreciated, and the results speak for themselves. It's a very forgiving piece of kit, especially for rank amateurs like us. I might still enjoy a V60 in the afternoon, but we'll be reaching for this in the morning. I'm going to keep improving our technique, and see how much more flavour we can extract from it.

I love that in a world of coffee machines that can cost as much as a used car, simple brewers like this exist that can make something delicious and fun for shockingly little money. It makes the world of good coffee approachable, both in technique and price.
