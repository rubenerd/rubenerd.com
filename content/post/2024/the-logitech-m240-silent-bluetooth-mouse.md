---
title: "The Logitech M240 Silent Bluetooth Mouse"
date: "2024-10-17T09:57:44+11:00"
year: "2024"
category: Hardware
tag:
- mouses
- reviews
location: Sydney
---
This is a quick review for a device bought under duress. Unfortunately my MacBook Air trackpad went on the fritz yesterday, and I don't have the time right now to make an appointment to fix it. Minor things like buying a home, and all that.

I went to our local electrical retailer of choice, and picked up this cute little Logitech mouse for $29 to tide me over:

<figure><p><img src="https://rubenerd.com/files/2024/tide@1x.jpg" alt="A bottle of Tide" srcset="https://rubenerd.com/files/2024/tide@2x.jpg 2x" style="width:320px; height:320px;" /></p></figure>


No, not *that* kind of tide me over. That's a detergent. I think? They don't sell it here.

Let's try this again. I went to our local electrical retailer of choice, and picked up this cute little **Logitech mouse** for $29 to tide me over:

<figure><p><img src="https://rubenerd.com/files/2024/tide@1x.jpg" alt="A bottle of Tide" srcset="https://rubenerd.com/files/2024/tide@2x.jpg 2x" style="width:320px; height:320px;" /></p></figure>

Okay, that first one was a gag, this one was an accidental paste. No damn it, **detergent**, not paste. What was I talking about? Who am I? *What even is this!?*


### And now for something, completely different

This is a quick review for a device bought under duress. Unfortunately my MacBook Air trackpad went on the fritz yesterday, and I don't have the time right now to make an appointment to fix it. Minor things like buying a home, and all that.

I went to our local electrical retailer of choice, and picked up this cute little Logitech mouse for $29 to tide me over:

<figure><p><img src="https://rubenerd.com/files/2024/logitech-silent@1x.jpg" alt="" srcset="https://rubenerd.com/files/2024/logitech-silent@2x.jpg 2x" style="width:320px; height:268px;" /></p></figure>

Friggen *finally*. I swear.

I haven't owned a regular mouse for years. I've been a trackball person, and my personal machines all have TrackPoints or equivilent. So this was a new experience.

It's fine. The scrollwheel is one of those old-school clunky ones, but does the job. It tracks across wooden coffee shop and shiny boardroom tables without issues. True to its name, the buttons are whispher quiet, but still have a satisfying *click*. I quite enjoy that.

I hadn't used a Bluetooth device&mdash;deiberately, and by choice&mdash;since a Logitech MX mouse more than a decade ago, and the connectivity was so flaky I swore I'd never touch another one again. But fingers crossed this also looks to be working fine. I turn on the mouse, open the MacBook, and it connects without issue.

<figure><p><img src="https://rubenerd.com/files/2024/tide@1x.jpg" alt="A bottle of Tide" srcset="https://rubenerd.com/files/2024/tide@2x.jpg 2x" style="width:320px; height:320px;" /></p></figure>

**Damn it!**
