---
title: "Assad is out"
date: "2024-12-10T14:59:12+11:00"
abstract: "Wow."
year: "2024"
category: Thoughts
tag:
- news
location: Sydney
---
[France24](https://www.france24.com/en/middle-east/20241208-in-pictures-syrians-celebrate-end-of-half-a-century-of-assad-family-rule)\:

> Jubilant Syrians poured into the streets in celebration on Sunday after a lightning rebel offensive reached the capital Damascus, putting an end to the Assad family’s 50 years of iron-fisted rule over the Middle Eastern nation scarred by war.

[ABC News Australia](https://www.abc.net.au/news/2024-12-10/australian-syrian-community-sydney-react-to-assad-regime-ousted/104702460)\:

> Said Ajlouni from the Melbourne-based Australian Syrian Association told the ABC he felt "relief" that the "brutal dictatorship" was over.
> 
> "We feel very happy, very pleased. I was shocked and surprised at the speed it happened," he said of the rebel groups' campaign.
>
> "Here, though, in Sydney, there are a lot of people from Syria and Aleppo who live in the same area to me. Wherever Syrian people go, they contribute and love life."

[Ammar Azzouz in The Guardian](https://www.theguardian.com/commentisfree/2024/dec/09/bashar-al-assad-syria-fall-afraid-hope)\:

> I couldn’t stop crying over all the pain, psychological and physical, that we have been exposed to. For our entire lives, we have lived in fear; it is liquid in our blood. And when we spoke up, we were killed, imprisoned or exiled. Homs is seeing a new history today.

[Channel News Asia](https://www.channelnewsasia.com/world/syria-explore-ousted-bashar-al-assad-damasucus-home-hayat-tahrir-al-sham-rebels-4795396)\:

> On Sunday (Dec 8), video circulating online showed crowds peeking into the bedrooms in the Assad residence, which was previously off-limits to ordinary citizens.
>
> They could be seen snatching clothes, plates and whatever belongings they could find including a Louis Vuitton cardboard shopping bag.
>
> Jubilant men, women and children wandered the home and its sprawling garden in a daze, the rooms stripped bare except for some furniture and a portrait of Assad discarded on the floor.

[Ben Royce](https://mastodon.social/@benroyce/113620717280008700)\:

> Can Mastodon do me a favor? Can y'all give #Syria and #Syrians a few days to rejoice after overthrowing a mass murdering sadistically torturing vile #Assad regime that has brutalized them for 54 years and after 13 years of a vicious #SyrianCivilWar?
> 
> This is a #Syrian celebration. Let them have it. Join them in it.
