---
title: "Performance issues from Europe"
date: "2024-07-22T08:40:01+10:00"
year: "2024"
category: Internet
tag:
- troubleshooting
- weblog
location: Sydney
---
I've discussed with Clara that an official *Rubenerd Blog* meetup would have to be in Warsaw, because Eastern/Central (cough!) Europe is where at least half of you send comments. It's humbling and awesome to think about actually, considering the furthest *east* I've ever been is Plzeň for a beverage. With apologies to the German side of my family, it's where my [favourite beer](https://www.pilsnerurquell.com/) comes from. Polish drops are also excellent, as is your food. But I digress.

Unfortunately, a few of you have emailed me to say the performance from Europe has been atrocious in the last couple of weeks. I fired up a VPN to Amsterdam to give it a try, and... wow. What's Latvian for "this sucks?"

I'll be honest, is a phrase with three words. The performance from Australia hasn't been super great either. Whatever I did when I consolidated a bunch of old servers and sites into this one VM, I dun goofed.

I've never used a CDN, partly because this blog has always been a scrappy place to experiment, and also because the world doesn't need another site that goes down when CloudFlare does. This also means it's on to me to figure out.

Life is a bit of a mess at the moment&mdash;in good and bad ways!&mdash;so I might not have time to properly dig into for a week or two. But I'll run some tests, and redeploy if necessary.

I suspect it's probably something silly like checksum offloading, or my RAM disk not being configured properly, or not getting the blocksize right for ZFS. If it's a network issue, that might take more time to fix.

Thanks for your patience.
