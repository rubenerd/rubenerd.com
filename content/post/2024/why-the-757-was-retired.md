---
title: "Ideas on why the 757 was retired"
date: "2024-03-07T08:19:45+11:00"
thumb: "https://rubenerd.com/files/2024/united-sfo-commonsabhay@1x.jpg"
year: "2024"
category: Travel
tag:
- aviation
- business
- boeing-757
location: Sydney
---
The Boeing 757 is a gorgeous plane, one of my all-time favourite conventional aircraft designs alongside the Lockheed L-1011 TriStar. I had a giant model of one hanging from my bedroom ceiling as a kid, complete with its *Boeing* blue and red livery. When I used to carry a sketchbook at school and doodle my own skylines, ocean liners, and aircraft designs, many of mine ended up resembling the 757. The planes, not the ocean liners. I wish I still had those.

Boeing abruptly retired the aircraft in 2004, with the closest "replacement" being the larger 737 variants which couldn't match its versatility, performance, comfort, or range. Or plug doors. Or control systems. I love the cute 737s designs of yore, but the MAX has demonstrated this tired design is well past its prime. Unlike the 757, sadly enough.

<figure><p><img src="https://rubenerd.com/files/2024/united-sfo-commonsabhay@1x.jpg" alt="Photo of a United 757 by commonsabhay" srcset="https://rubenerd.com/files/2024/united-sfo-commonsabhay@2x.jpg 2x" style="width:500px;" /></p></figure>

*(Photo of a United 757 in San Francisco in 2023 by [commonsabhay](https://commons.wikimedia.org/wiki/File:United-757-sfo.jpg) on Wikimedia Commons).*

I had long assumed Boeing retired the 757 due to a few factors:

* Orders drying up; arguably the primary reason

* The growth of low-cost carries that favoured smaller jets

* Competition from widebodies with similar capacity, which I keep reading most people prefer

* Bad market conditions and timing

* Airlines opting to standardise on 737s and A320s; an irony, given the 757 was originally sold with the idea that airlines would want to use the common type rating between it at the 767

Many aviation enthusiasts like me speculate that a re-engined 757 would sell well today, owing to the proliferation of point-to-point routes which the aircraft's longer range would have been perfect for. Also, because modern 737s are a bit rubbish. Delta and several European airlines still fly the aircraft today, two decades after the airframe ceased production.

But Petter from the Mentour Pilot and Mentour Now channels [offered an additional explanation 📺](https://www.youtube.com/watch?v=wVWQzw3pLWI "What was the real reason Boeing killed the 757?") I hadn't considered, the gravity of which really came home when he showed this slide:

<figure><p><img src="https://rubenerd.com/files/2024/mentour-now-757@1x.jpg" alt="" srcset="https://rubenerd.com/files/2024/mentour-now-757@2x.jpg 2x" style="width:500px;" /></p></figure>

Wow! For roughly the same passenger capacity, the A321 is significantly lighter empty than the closest equivalent 757 variant. It seems obvious with hindsight; the additional range and capabilities of the 757 didn't come from nowhere. The airframe had to be larger to carry more fuel, and the engines had to be thirstier to have that additional power.

I guess it always comes down to the numbers.

