---
title: "A selection of Wi-Fi hotspot names"
date: "2024-05-21T07:30:34+10:00"
year: "2024"
category: Internet
tag:
- networking
- wifi
location: Sydney
---
These have all come from sitting at coffee shops over the last week:

* Dont connect to me
* Grab A Cookie
* Ordinary van (someone is a Simpsons fan)
* Pretty Fly for a WiFi (always a good one)
* Qantas free WiFi (hmmmm)

We've been working through various *Fate/Stay Night* characters in each apartment we've shared. The only issue currently is that we're using *Tohsaka*, who's unquestionably the best character. Perhaps we should have saved her for the place we end up buying.
