---
title: "The Indie Wiki Buddy browser extension"
date: "2024-06-20T08:20:39+10:00"
year: "2024"
category: Internet
tag:
- indieweb
- minecraft
- wikis
location: Sydney
---
Browse any wikis today, and there's a decent chance it's hosted by Fandom (ne. Wikia, Wikicities) which is, in word, not great. Wait, that's two words. I wrote a post about how to [declutter it using uBlock Origin](https://rubenerd.com/decluttering-fandom-com-with-ublock-origin/), but the real answer is to direct your traffic elsewhere.

In true IndieWeb spirit, there's a [Firefox plugin for that](https://getindie.wiki/)!

> Indie Wiki Buddy is a browser extension that helps you easily find and support independent wikis.
> 
> When you visit a wiki on Fandom or Fextralife, this extension will notify or automatically redirect you to quality independent wikis when they're available.
>
> Indie Wiki Buddy supports over 300 quality, independent wikis across 17 languages. And you're in full control -- you can choose your experience wiki-by-wiki!

It's worked great for the [Minecraft Wiki](https://minecraft.wiki/) so far.

