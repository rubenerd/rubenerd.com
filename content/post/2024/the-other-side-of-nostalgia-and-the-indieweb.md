---
title: "The other side of nostalgia, and the IndieWeb?"
date: "2024-06-24T08:25:30+10:00"
year: "2024"
category: Internet
tag:
- comments
- indieweb
- nostalgia
location: Sydney
---
Electronic nostalgia and history are important to me. There was something magical about the 1980s and early 1990s when it came to home and mini computers, and I revel in teaching my old dogs new tricks. I assumed this was harmless fun, but I'm seeing more pushback against the idea, or at least nostalgia in general.

I was scrolling through a podcast directory recently, like a gentleman, when I chanced upon a sociology series that discussed the "problems with nostalgia". Their angle, at least based on the description, was that modern companies are playing on nostalgia to release new devices, movies, foods, and so on. But they're overpriced, crappy, or otherwise uninspired. I suppose there's big business in giving people modern analogues to their nostalgia, whether it respects the source material or not.

I've read others make the point that feeling nostalgia in a political or social sense can hamper progress, by making people yearn for a time that didn't exist, or is witnessed through rose-tinted glasses. My Scottish friend Simon blames Brexit on this phenomena, and it's easy to see why.

🌲 🌲 🌲

These are all economic and social factors related to nostalgia. But what about a more nuts and bolts concern? 

One angle I didn't consider came via Mastodon. The person voiced frustration that people publishing indie, nostalgic websites that are "non profit" are elitist, because they have the financial freedom to do so that others don't (this is paraphrased from memory, and Mastodon's search function is deliberately terrible. But this was the thrust).

I empathise with their position. They've clearly struggled and built a viable living from commercialisation of a website, and therefore resents the perceived characterisation of their efforts by people like me who post retro and IndieWeb sites without that commercialisation. There's the implication that these sites fetishise a time that only existed for people who could afford to live in it.

There's a lot going on here, but I'd start by saying I have my own lived experience when it comes to this tech too.

I grew up as a kid writing HTML3 webpages, which is part of the reason I rebuilt [my own silly retro site](http://retro.rubenerd.com/) recently. Finding I had an aptitude for this was a life-changing event: it got me a few odd jobs, helped me cope when family life was rough, and it was also a huge boon for my self-esteem and outlook on life. I suspect every artist and engineer has a similar childhood story with their own creative outlet.

I come to the retro and IndieWeb communities looking to rekindle some of that awe and sense of joy that have subsequently been eroded after being in the industry for a decade. Having a retro or IndieWeb site is a rejection of overly-complicated frameworks which push people out, the steady grind of [enshittification](https://en.wikipedia.org/wiki/Enshittification), and the race-to-the-bottom attitude that comes from treating everyone's works as ["content"](https://www.youtube.com/watch?v=hAtbFwzZp6Y "Everything Is Content Now").

I take the point that I can do this because I have other income streams, but I'd counter than these activities serve multiple purposes for different people. They're a hobby, a coping mechanism, and a form of protest against everything from plagiarised AI slop generators, to social networks that would have us believe our creativity is merely gristle to be chewed though so heads they win, tails we lose. No thanks, I'll write in my own space.

Retro and IndieWeb writers aren't the enemies of people who want an independent space upon which they can make a living. I'd argue we're on their side, because we're pushing for a future where there's a viable alternative to the [Torment Nexus](https://knowyourmeme.com/memes/torment-nexus).

But maybe I misunderstand the issue.
