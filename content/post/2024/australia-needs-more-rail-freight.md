---
title: "Australia needs more rail freight"
date: "2024-07-15T16:02:42+10:00"
thumb: ""
year: "2024"
category: Thoughts
tag:
-
location: Sydney
---
I have a tattered old photo book of Australian locomotives titled *Australian Locomotives*. Thanks Ruben, that was helpful. This is the part of the opening from chapter one:

> Australia's vast distances presented early European settlers with formidable transport problems. The railways offered a solution [...and] helped develop the country.

The people Europeans displaced may have something to say about that, but it's true that railways were instrumental in building out the country into what it is today. Without the rails, entire towns and cities would never have been established, supply chains for food and raw materials wouldn't have been possible, and nerds like me would have nothing but blank books where galleries of the majestic [3801](https://en.wikipedia.org/wiki/3801) and [R 761](https://en.wikipedia.org/wiki/Victorian_Railways_R_class) would otherwise be.

Alas, Australia's freight rail ain't what it used to be. According to [this recent Guardian article](https://www.theguardian.com/australia-news/article/2024/jul/15/freight-operators-will-buy-new-diesel-trains-if-governments-dont-back-green-alternatives-industry-says), less than 2% of the freight between Sydney and Melbourne&mdash;Australia's two largest cities by far&mdash;are handled by rail. Given the distances and volume of traffic involved, that's ridiculous.

What's been interesting however is the push by the rail industry to adopt electrification. Transport-related emissions are set to grow sharply in the coming decades, and constitute a larger percentage of the country's overall emissions. As the article states, the rail industry wants to meet its own emissions targets, and not be seen as holding up the rest of the country.

In isolation, I can see why this would be a desirable prospect. Burning diesel fuel, even efficiently, is still a dirty process that generates noxious particulate matter that's especially dangerous to our respiratory systems. I still remember the fanfare around diesel cars, and why I thought [they were a terrible idea](https://rubenerd.com/being-dead-wrong-about-diesel-cars/). I'm glad European cities like Paris are realising this too, after flirting for decades with *Madame NO<sub>x</sub>*.

But here's the thing. Diesel electric locomotives aren't primarily competing (or failing to compete) with electrics with pantographs or third rails; they're competing with road transport. And all those road-wearing trucks are diesel powered.

<figure><p><img src="https://rubenerd.com/files/2024/SCT011_SCT004@1x.jpg" alt="Beautiful landscape photo of an SCT locomotive." srcset="https://rubenerd.com/files/2024/SCT011_SCT004@2x.jpg 2x" style="width:500px; height:333px;" /></p></figure>

*(Beautiful Australian landscape photo of an SCT locomotive [by DBZ2313](https://commons.wikimedia.org/wiki/File:SCT011_SCT004.jpg) on Wikimedia Commons).*

Clara and I are amazed when we're railfanning over a video stream at just how many hundreds of cars can be carried by just a few locomotives. The low coefficient of friction between the rails and wheels make the mode of transport so efficient, it's almost criminal we don't use this tech everywhere.

I take the point that electrification would be excellent. Australia's energy mix isn't great right now, but it's much easier to swap an electric locomotive's "fuel" source than one with an engine (something the *well actually* crowd ignore when they say electric vehicles powered by fossil fuel plants are no cleaner).

But wholistically, a fleet of new, more efficient diesel electric locomotives, and the motivation to use them over road transport, would make *such* a difference to Australia's overall carbon footprint (not to mention road maintenance and other positive side-effects). Diesel electrics could even be fitted with pantographs for use in areas that do receive electrification, provided disparate state systems can operate the same way.

I'm currently fascinated with the idea of AC traction, which several railway companies in the US have started deploying. As [this article in Trains](https://www.trains.com/trn/railroads/history/8-diesel-locomotive-breakthroughs/) summaries, AC traction can improve traction and torque by up to 20% over equivalent DC motors. Let's get some of those!
