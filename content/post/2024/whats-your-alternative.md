---
title: "What’s your alternative?"
date: "2024-06-30T10:02:15+10:00"
year: "2024"
category: Ethics
tag:
- feedback
location: Sydney
---
There's a popular rhetorical tactic when discussing contentious issues online that goes, a little something, like this:

> **Person 1:** I don't like drinking tainted water.
>
> **Person B:** Okay, well have your plans for a water treatment facility on my desk by tomorrow morning.

*(I read a variation of this somewhere, but I spent a solid 20 minutes trying to find it and I can't. If you're the originator of this, do let me know. <strong>UPDATE:</strong> Thanks to <a href="https://mastodon.sdf.org/@juni">juni</a> for pointing me to the source of <a href="https://www.jwz.org/blog/2024/06/mozillas-original-sin/#comment-250354">this comment by boonq</a>).*

The position here is that one can't argue against something without offering a viable, vetted, and validated alternative. While excellent for alliteration, it's not as rational a position as it sounds.

I think people who put forward "what's your alternative?" arguments come from a better place than someone who falls for the [we should improve society somewhat](https://knowyourmeme.com/memes/we-should-improve-society-somewhat) trap, because at least they concede the issue being discussed. In their mind, they're taking the next logical step and addressing how to resolve the issue.

The problem here is threefold:

* The respondent isn't the one addressing the issue, they're expecting the affected person to. It's unreasonable to expect someone stubbing their toe to have formed a detailed plan for  rearranging their furniture before shouting out in pain.

* Stakeholders are still impacted by a system they don't understand, can't control, or lack agency over. As I wrote in February, you [don't need to be a professional to provide feedback](https://rubenerd.com/you-dont-need-to-be-a-professional-to-provide-feedback/).

* The respondent asking for a solution from a frustrated person lacks basic social intelligence. A spouse doesn't need to be chided for not wearing steel-toed boots, they first need to be sympathetic to the pain they're experiencing. This is basic *How to Win Friends and Influence People* territory, in addition to being a pleasant human being that others will want to work with in the future to solve problems.

I get it; serial complainers can also be tedious. One could say I get BAUD of them. **AAAAAAAAAA!** But this reflexive dismissal of someone's point if they haven't provided a detailed alternative is, at best, counterproductive.

