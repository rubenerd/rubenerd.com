---
title: "Comments that didn’t age well"
date: "2024-04-20T08:18:35+10:00"
abstract: "Get a grip guys. The worst flu season in recent years killed 80,000 worldwide."
year: "2024"
category: Thoughts
tag:
- comments
- covid
- health
location: Sydney
---
A person with the handle "steveccc" left this big brain comment on a [Sydney Morning Herald Covid article in 2020](https://www.smh.com.au/national/potential-fifth-australian-case-of-coronavirus-detected-with-more-highly-likely-20200126-p53uvg.html#comments)\:

> Get a grip guys. The worst flu season in recent years killed 80,000 worldwide.

The sentiment was absurd even at the time. Anyone who'd been paying attention knew we were dealing with something severe. 

His stats were also wrong. [This NBC report](https://www.nbcnews.com/health/health-news/doctors-speak-bluntly-about-record-80-000-flu-deaths-n914246) talked about 80,000 deaths in the US, not "worldwide". An [American NIH report](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6815659/) cited a WHO estimate of 250,000 deaths at minimum from the 2017 flu season, plus additional victims from secondary complications.

Funnily enough (for want of a better phrase), the flu I had earlier this year hit my far worse than COVID, and we were well up to date with our vaccines for both. It's proof we have to take all this seriously!
