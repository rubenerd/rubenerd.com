---
title: "Saving whimsy and the 418 status code"
date: "2024-11-18T16:00:35+11:00"
year: "2024"
category: Internet
tag:
- whimsy
location: Sydney
---
I tongue-in-cheek referenced the 418 status code in my ultimate [multi-function device post](). I'm relieved that past attempts to [buzzkill it](https://web.archive.org/web/20170810220743/https://github.com/nodejs/node/issues/14644#issue-248218948) out of existence were met with such [enthusiastic and ongoing repudiation](https://save418.com/).

[@romellem put it best](https://web.archive.org/web/20170810220743/https://github.com/nodejs/node/issues/14644#issuecomment-321649109)\:

> I for one like fun little easter eggs that you find throughout a programming career. To me, it shows that everything that goes on to make a computer actually do work is still made by humans, and keeping small slices of that human element is nice (in my opinion).

I also lamented the sanitisation of the web in my recent post about [what I look for in blogs](https://rubenerd.com/what-i-look-for-in-blogs/). Let's bring quirky back!
