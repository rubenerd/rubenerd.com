---
title: "Emil on old computers for new writing"
date: "2024-03-21T16:59:26+11:00"
year: "2024"
category: Hardware
tag:
- internationalisation
- language
- retrocomputers
- writing
location: Sydney
---
Back in December I talked about [using old computers for new writing](https://rubenerd.com/using-an-old-computer-for-new-writing/ "Using an old computer for new writing"). Provided one is careful to back up their work on hardware that might be old and flaky, it can be a fun and distraction-free way to write, along with being a bit of a novelty.

Emil Oppeln-Bronikowski [posted on Mastodon](https://helo.fuse.pl/@emil/statuses/01HRYFQBE818MJ2ACW263A0BYV)\:

> damn you, anglo bastards, living in US-ASCII world, being able to write blog posts on cute 8bits. I ńęęd my ćhąrąćtęrś.

It was said tongue-in-cheek, but he raises another massive blindspot on my part.

My assumption was that *of course you could write on these machines!* I might dedicate a post to specific practical concerns like the Apple \]\['s lack of lowercase characters, or the Commodore 64's 40 columns, or the hardware you'd need to interface with modern machines to extricate your writing when done. There are a bunch of assumptions and assumed knowledge there that I realise my original post didn't touch on at all.

But missing vital letters and accents from your character set would be another massive limitation, to say nothing of alphabets or writing systems that aren't supported *at all*. Even the Japanese Commodore VIC-1001 only supported Katakana (another machine I'd *dearly* love to have one day, because I'm a massive Showa electronics nerd).

Once again this highlights the ridiculous and *entirely* unearned benefit I have being an English speaker and writer in electronics. 23 Latin characters and a bunch of weird punctuation can write virtually all my words.
