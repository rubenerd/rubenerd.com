---
title: "ruben.coffee is now live"
date: "2024-10-26T10:26:25+11:00"
year: "2024"
category: Internet
tag:
- coffee
- xml
location: Sydney
---
My new coffee page now lives at [ruben.coffee](http://ruben.coffee/)! It's going to be my evolving collection of hardware, shops, links, travel, recipes, wish lists, roasters, and anything else I can think of. It started on Clara's and my wiki, but has now evolved into its own thing using my [Omake format](https://rubenerd.com/xmlns-omake/), because why not?

Okay, there are a lot of reasons why not. Unlike drinking coffee, which is always appropriate. Except when it's not.

I wanted to thank [James](https://jamesg.blog/) for inspiration, and [Wesley](https://www.wezm.net/v2/) for his kind words. Because remember the golden rule of coffee and XML: if they don't work, you're not using enough.
