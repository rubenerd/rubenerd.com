---
title: "My Tom n Toms coffee mug"
date: "2024-05-21T09:40:10+10:00"
year: "2024"
category: Hardware
tag:
- coffee
- mugs
location: Sydney
---
I haven't talked about a mug in a while! So far I've shared my [Apothecary Coffee mug](https://rubenerd.com/my-apothecary-coffee-mug/), a [Hahndorf Inn Hotel mug](https://rubenerd.com/my-hahndorf-inn-hotel-mug/), and a [Moka takeaway cup](https://rubenerd.com/my-moka-coffee-cup-and-some-sydney-architecture/). Today's is another mug that would be in school now if it were a person. What a weird way to talk about crockery.

<figure><p><img src="https://rubenerd.com/files/2024/mug-tom-n-toms@1x.jpg" alt="Photo of a Tom N Toms coffee mug on our balcony." srcset="https://rubenerd.com/files/2024/mug-tom-n-toms@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

*Tom n Toms* is a Korean coffee chain that was briefly open in Sydney in the early to mid 2010s. They did the usual coffee fare, but also had treats like cheese pretzels and honey butter bread which were *amazing*. I think they're still open in Singapore, but it's been a while since I've been to one.

Clara and I applied for our first shared apartment rental together at their store on Bathurst Street, and did uni assignments together at their George Street store. They were cosy, comfortable, and certainly did better coffee than the Starbucks down the street.

I'm not sure how long they sold mugs, but we picked up one on special when they announced they were closing. It's even made in Korea according to the base! Despite the name containing *coffee*, it's mostly a tea cup.
