---
title: "Tom Nicholas on the 2024 UK elections"
date: "2024-07-05T10:31:50+10:00"
year: "2024"
category: Thoughts
tag:
- europe
- politics
- united-kingdom
location: Sydney
---
[From his latest video](https://www.youtube.com/watch?v=vry5deT8lc0)\:

> After fourteen years in power [views of David Cameron, Theresa May, Liz Lettuce, and Rishi Sunak], more than 100 billion GBP to cuts in public services, one Brexit referendum, countless corrupt COVID PPE deals, and a self-inflicted economic crisis, the consequences of Conservative rule are plain for all to see.

**Update:** Wow, *what* a landslide!
