---
title: "Elizabeth Rose Bloodflame’s Hololive debut! 🇬🇧"
date: "2024-06-23T11:47:31+10:00"
thumb: "https://rubenerd.com/files/2024/liz-soz@1x.jpg"
year: "2024"
category: Media
tag:
- hololive
- erbloodflame
- music
location: Sydney
---
<figure><p><img src="https://rubenerd.com/files/2024/liz-soz@1x.jpg" alt="Screenshot from Liz's debut stream, showing her dislikes including tea." srcset="https://rubenerd.com/files/2024/liz-soz@2x.jpg 2x" style="width:500px;" /></p></figure>

> My dislikes? Heh heh heh. HEH HEH HEH. Hmmm. Haha. HAHAHA! Ahhh. Um&hellip; yeah.
> 
> I'm not a big fan of tea. HAH!
> 
> Soz, innit.

I don't want to embellish or overemphasise what a *balls* month this has been, but this latest Hololive EN generation debuted over the weekend, and it was balm to a troubled soul! Specifically [Elizabeth Rose Bloodflame](https://www.youtube.com/@holoen_erbloodflame), who was the vinegar to our fish and chips! A *cheeky chicken!* **Herbs**, with an H!

A criticism of Hololive's EN branch lately has been the North America-centric nature of their talents. I don't think is entirely deserved, though Yagoo has [assured us this is changing](https://www.youtube.com/watch?v=-CYSz5lu3g0). I'd also held out hope they'd finally get some more international vtubers joining their ranks. Europe was always going to be a challenge, given the lack of overlap between Japan and the west coast of North America.

<figure><p><img src="https://rubenerd.com/files/2024/liz-bbc@1x.jpg" alt="Screenshot from Liz's debut stream, showing a BBC test screen." srcset="https://rubenerd.com/files/2024/liz-bbc@2x.jpg 2x" style="width:500px;" /></p></figure>

But, and I *cannot* overstress this enough, Liz was *SMASHING*, despite it being `04:30` in the UK. Wow, what a singing voice! Even her graphics including references to the BBC were on point. I haven't been this excited for a vtuber in a while. My thoughts are so scattered having just finished watching her debut, I can't get them out fast enough! *Fizzy pop!* And her *impressions!*

Her artists and riggers also did an incredible job; she has one of my favourite character designs of all of HoloEN already. She's as though *Fate* and *Fire Emblem* did a collaboration, then asked us if we wanted crisps. **AAAAH!**

I hope [Bae](https://www.youtube.com/channel/UCgmPnx-EEeOrZSg5Tiw7ZRQ) and Liz can do a collab stream at some point. In the meantime, gov’, [check out her debut](https://www.youtube.com/watch?v=EDzEeX8thOo), and her [first cover](https://www.youtube.com/watch?v=zZXW_iXIrss "【COVER】OTONABLUE (Elizabeth Rose Bloodflame)")!

<figure><p><img src="https://rubenerd.com/files/2024/liz-standing@1x.jpg" alt="Liz standing at the end of the stream." srcset="https://rubenerd.com/files/2024/liz-standing@2x.jpg 2x" style="width:500px;" /></p></figure>

