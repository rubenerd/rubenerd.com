---
title: "Movable Type and the Rise of WordPress"
date: "2024-03-14T08:02:29+11:00"
year: "2024"
category: Internet
tag:
- 2000s
- movable-type
- nostalgia
- weblog
- wordpress
location: Sydney
---
Jason Lefkowitz [wrote a post in late February](https://octodon.social/@jalefkowit/112005522820467704 "Jason's post on Mastodon about Movable Type and WordPress") explaining a big reason behind WordPress’s rise in 2004:

> Movable Type was commercial software; there was a free personal version, and a relatively expensive pro version. This didn't get in their way for a long time, because the terms of who qualified for the free personal version were generous. But when they released version 3.0 in 2004, they tweaked who qualified for which license in such a way as to make it look like lots of high-traffic bloggers were suddenly going to have to pay for a pro license.

> As you might imagine, the entire blog world lost its collective shit.

He explains that WordPress even today can't match Movable Type's usability or feature set, but that WordPress was open source and benefitted from this exodus. WordPress's install base is made more impressive when you realise it didn't have first-mover advantage.

I ran [Movable Type 4 briefly](https://rubenerd.com/i_just_finished_installing_movable_type_4/ "I just finished installing Movable Type 4"), and had a Vox blog, but I never got around to importing my posts into either. I remember enjoying it; it married static site generation with server-side software which was much easier for me when I was hopping between KL and Adelaide for uni. I was sad when the [Melody project](https://rubenerd.com/melody-mt-offshoot/) couldn't get traction.

WordPress embodied the scrappy *worse is better* ethos, but Movable Type was simply better. Now that WordPress is beginning its own journey into *[enshittification](https://en.wikipedia.org/wiki/Enshittification "Wikipedia article on Cory Doctorow's idea of enshittification")* with the [Gutenberg Editor](https://rubenerd.com/dave-asks-about-wordpress-block-editor/ "Dave asks about the WordPress block editor") and its [hosted pivot to AI](https://www.404media.co/tumblr-and-wordpress-to-sell-users-data-to-train-ai-tools/ "Tumblr and WordPress to Sell Users’ Data to Train AI Tools"), it needs meaningful competition now more than ever.

Hey Six Apart! Wanna step up? お願いします?
