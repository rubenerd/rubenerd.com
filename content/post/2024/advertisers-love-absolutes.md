---
title: "Advertisers love absolutes!"
date: "2024-11-04T13:24:49+11:00"
year: "2024"
category: Internet
tag:
- advertising
- telcos
location: Sydney
---
I watch a lot of YouTube and listen to a lot of podcasts, some of which have embedded ads. Embedded ads are ads that are embedded. Gee, thanks Ruben! These are ads that the presenter delivers, in lieu of injected ads that frankly sound terrifying.

I've said *ad* so many times the word has lost all meaning. Ad ad ad. Here's an ad! What's an ad? Ad!?

An ad, whatever that is, on a well-known travel channel started with this:

> [You can call me ad](https://www.youtube.com/watch?v=uq-gYOrU8bA "You Can Call Me Al, by Paul Simon").

Sorry, that was a typo. Let's try that again:

> 🎷 🎷 🎷

It's been one of those days.

> `$TELCO` will work, wherever you are.

People with mobile phone operating experience may raise an eyebrow at such an assertion, but these sorts of claims aren't unusual. Over the years I've heard that my telco:

* has "unbridled" coverage, because packets are delivered by horse 

* operates because "Australia is why", which is indecipherable

* would "never fail" me, despite repeatedly failing me

A star-adjacent telco in Singapore famously ran ads with a dog who, upon eating someone's phone, would start ringing everywhere he went. I'd get heavy metal poisoning if I tried that, and I'd be indecipherable anyway. Still, it made more sense than anything the big T has said in Australia of late.

But back to the original ad. They then said:

> `$TELCO` is available in 150 countries.

Wait, hold on... that's not all of them. What if I go somewhere that isn't in that list? Whatever happened to working "wherever you are?"

Okay, it *probably* covers the countries you and I are probably thinking of and would travel to. Likewise, they were probably using a bit of creative licence when claiming it would work "wherever you are". But why is it on us to make excuses for bad advertising? I mean, [I am good at it](https://rubenerd.com/an-advanced-case-of-in-their-defence/ "An advanced case of “In Their Defence”"), but there's a difference between hyping yourself up a bit, and making falsifiable claims that are easily disproved. I'm tired of it always being *caveat emptor* and not... caveat *business?*
