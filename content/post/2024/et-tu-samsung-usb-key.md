---
title: "Et tu, Samsung USB key?"
date: "2024-04-05T12:02:25+11:00"
thumb: "https://rubenerd.com/files/2024/samsung-usb@1x.jpg"
year: "2024"
category: Hardware
tag:
- sd-cards
- storage
- usb-c
- usb-keys
location: Sydney
---
I've had a good run with memory cards and USB keys, given their reputation for flakiness relative to other storage tech. It *worries* me how many MicroSD cards are running boot volumes on single-board computers and routers around the world, quietly grinding down their wear-levelling capabilities to dust. But thus far, it hasn't affected me.

Well, until today. My unbroken, multi-decade streak came to and end, thanks to this USB 3.1 256 GiB Samsung USB key:

<figure><p><img src="https://rubenerd.com/files/2024/samsung-usb@1x.jpg" alt="A photo of the offending USB key." srcset="https://rubenerd.com/files/2024/samsung-usb@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

I've been able to perform data recovery for friends who have busted USB keys and memory cards, but that was because I could still see the device in [dmesg(8)](https://man.freebsd.org/cgi/man.cgi?query=dmesg), and work backwards from there. This key shows no signs of life whatsoever, on macOS, FreeBSD, or Fedora. The key used to get a bit warm when connected, but it remains cool to the touch now.

USB-C is such a compromised connector, with multiple incompatible signals requiring specific cables that lack visual disambiguation, and their susceptibility to issues from the tiniest flecks of dust. This particular key was perfectly clean though, and was never transport without its cap which I somehow didn't lose. Maybe it was zapped or something.

It's a minor pain, but I have all the data elsewhere. I'm also lucky that I use full-drive encryption, so I can take this to e-waste without too much worry. 


