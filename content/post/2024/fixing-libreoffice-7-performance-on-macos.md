---
title: "Mitigating LibreOffice 7 performance issues on MacOS"
date: "2024-01-25T19:39:26+11:00"
year: "2024"
category: Software
tag:
- libreoffice
- spreadsheets
- troubleshooting
location: Sydney
---
Last February I noticed LibreOffice 7 had [switched to the Skia rendering engine](https://rubenerd.com/libreoffice-730-skia-stability-issues-on-macos/ "LibreOffice 7.3.x Skia stability issues on macOS"), which coincided with a steep increase in stability issues.

I've since moved from an Intel MacBook Pro to an M1 MacBook Air, and while the stability issues have been resolved, performance is still *extremely* poor. I spend most of my day among LibreOffice Calc sheets, and it can't keep up with Lotus 1-2-3 97 on my Pentium 1. I avoid Excel when I can, but I'll admit it's buttery smooth and fast on the same hardware.

I went to LibreOffice &rarr; Preferences &rarr; View, and saw *Use Skia for all rendering* was selected. I cllicked *Copy skia.log* and noticed something interesting:

    RenderMethod: raster
    Compiler: Clang

*Raster* means its not hardware accelerated at all. No wonder my 10,000+ line spreadsheets chugged when scrolling.

When I unticked *Use Skia for all rendering* and restarted, the difference in performance was night and day. It's still not as good as Excel on the same hardware, but the benefit is it's not Excel on the same hardware.
