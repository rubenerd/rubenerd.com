---
title: "Getting back into Australian espresso culture"
date: "2024-11-09T08:24:34+11:00"
thumb: "https://rubenerd.com/files/2024/espresso-table@2x.jpg"
year: "2024"
category: Thoughts
tag:
- australia
- coffee
- sydney
location: Sydney
---
I love filter and immersion brewing at home. I've talked about how much I *love* the AeroPress as a daily driver, and the Hario V60 is a fun treat when I'm feeling a bit fancier. Some nice single-origin beans from a local roaster, fresh water, and those cheap coffee machines make some incredible brews, and they can taste markedly different depending on preparation and technique.

*(One of my treasured memories working briefly in San Francisco was having Philz Coffee with some of my colleagues. Instead of every other coffee shop with which I was familiar, the store had rows upon rows of filter coffee machines, with staff studiously blooming the coffee and pouring water by hand. The coffee was generally pretty good, but it was the experience of choosing a specific blend, then watching them make it, that was the most fun).*

Outside our little kitchen coffee setup, the vast majority of Australian coffee shops serve espresso drinks. Whether it's a chain, a bog standard office tower lobby café, or a speciality shop with coffee sacks on the walls, they'll invariably have a large espresso machine. The sounds of steam pressure being released are as much a part of the Australian coffee shop experience as the small glass cabinet of pastries, and overheard conversations in Japanese, Armenian, Lebanese, whatever native tongue the patrons have that day. It's fucking wonderful.

I suspect this preference towards espresso came down to the strong Italian influence on coffee culture here in the mid-20th century which, among other things, has been credited with limiting the appeal of what my British friends call *Starbees*, and local Australians *Floor Sweepings*. I like Starbucks for reasons other than their drinks&mdash;if you can believe it&mdash;but that'd be a topic for another time. It does mean that if you get a coffee, it's invariably using espresso as a base.

As I'm sure many of you did too, I grew up having milk and sugar in my coffee. I had very early mornings from my mid-teens for family reasons, and the coffee shop downstairs in Singapore jolted me out of my stupor with remarkable success. Back then, I wasn't drinking it for the taste as much as the psychoactive effect!

Over time though I gave up on the sweetener and milk, and started drinking Americanos and kopi-o kosong, depending on where I was having it. The former was a mellow treat, the latter could still knock your socks off. When I moved back to Australia, I substituted these for the long black, which is similar in principle if not necessarily in its preparation.

<figure><p><img src="https://rubenerd.com/files/2024/espresso-table@1x.jpg" alt="An espresso on a coffee shop table in the early afternoon." srcset="https://rubenerd.com/files/2024/espresso-table@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

Maybe it's the influence of more coffee bloggers and channels I've been looking at lately, but I started getting back into straight espresso at coffee shops in lieu of these watered-down versions. I figured it's easier and cheaper to make immersion and filter coffee at home than espresso, so it made sense to outsource this to people with the skills, equipment, and experience when I go out.

The results have been wonderful so far! I didn't used to like espresso for being bitter, but I think this came down to getting coffee at places that used beans optimised for milk and water. If you have a great local coffee shop, espressos are the drinks where they shine and show their skills. And *wow*, a great espresso is sublime!

10/10, would recommend. ☕️
