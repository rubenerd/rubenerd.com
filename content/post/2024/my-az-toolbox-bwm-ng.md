---
title: "My A-Z toolbox: bwm-ng"
date: "2024-09-26T08:52:06+10:00"
year: "2024"
category: Software
tag:
- az-toolbox
- bsd
- freebsd
- illumos
- linux
- netbsd
- networking
location: Sydney
---
This is the second post in my [A-Z Toolbox](https://rubenerd.com/tag/az-toolbox) series, in which I'm listing tools I use down the alphabet for no logical reason.

The letter B has the excellent FreeBSD [Bhyve](https://bhyve.org/) hypervisor, and the [bzip2](https://sourceware.org/bzip2/) compression utility. But **[bwm-ng](https://www.gropp.org/)** takes the proverbial cake for a a small tool that I've found indispensible more times than I can count.

The *Bandwidth Monitor Next-Generation*&mdash;cue Star Trek theme music&mdash;is a live network bandwidth monitor that runs on everything I care about, so FreeBSD, NetBSD, illumous, and various Penguins.

Here it is running on a relatively idle server:

    bwm-ng v0.6.3 (probing every 0.500s), press 'h' for help
    input: getifaddrs type: rate
    \   iface             Rx              Tx           Total
    ========================================================
        igb0:      0.12 KB/s       0.49 KB/s       0.61 KB/s
         lo0:      0.00 KB/s       0.00 KB/s       0.00 KB/s
    --------------------------------------------------------
       total:      0.12 KB/s       0.49 KB/s       0.61 KB/s

And here it is on a busy FreeBSD jail running Samba:

    bwm-ng v0.6.3 (probing every 0.500s), press 'h' for help
    input: getifaddrs type: rate
    \   iface             Rx              Tx           Total
    ========================================================
        igb0:  33495.43 KB/s     232.99 KB/s   33728.41 KB/s
         lo0:      0.00 KB/s       0.00 KB/s       0.00 KB/s
    --------------------------------------------------------
       total:  33495.43 KB/s     232.99 KB/s   33728.41 KB/s

I've used it to troubleshoot paravirtualised drivers, and a crude bandwidth monitor for file transfer tools that don't have an easy progress bar or activity indicator. At least with `bwm-ng`, I can see that it's doing *something*.

With special thanks to Volker Gropp for writing this awesome tool, and to [Tim Bishop](https://www.freshports.org/net-mgmt/bwm-ng/) and [Paolo Vincenzo Olivo](https://pkgsrc.se/net/bwm-ng) for maintaining the packages on FreeBSD and NetBSD pkgsrc.
