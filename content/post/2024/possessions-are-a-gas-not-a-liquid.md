---
title: "Possessions are a gas, not a liquid"
date: "2024-01-21T13:35:07+11:00"
year: "2024"
category: Thoughts
tag:
- decluttering
- mess
location: Sydney
---
This is a well-worn theme here, and something I've probably pointed out in dozens of related and semi-related posts. But it's worth calling out again.

I've spent much of this weekend consolidating, collapsing, repurposing, listing for sale, giving away, recycling, donating, and throwing away *stuff*. Last night the kitchen floor and our computer nook were more garbage bag than tile, and I probably filled the entire building's cardboard and clothing recycling bins with just our stuff.

And yet, after a solid day of doing this, the apartment looks... [exactly the same](https://bsd.network/@rubenerd/111791034307234234 "My post on Mastodon about it"). Okay, not *exactly* the same. But close enough to being the same that someone who visited recently wouldn't be able to tell the difference.

I love the physical and mental weight that's lifted when I get rid of *stuff*, but it's also oddly demoralising when you spend an entire day off doing all this organising with little noticeable impact!

*Stuff* expands to fill whatever container you put it in, whether it's a bookshelf, cupboard, drawer, or desk. I suppose it's all the more reason to keep trucking on... and to at least attempt to not accumulate more of it in the first place.

I definitely did better last year at being more deliberate with what I buy, and keeping up the idea that I have to get rid of something to bring in something else. That silly monitor stand yesterday displaced another book I'd finished and put in the shared library downstairs. But these steps merely maintain equilibrium; the next step is to reduce.
