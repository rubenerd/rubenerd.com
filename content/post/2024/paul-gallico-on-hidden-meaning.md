---
title: "Paul Gallico on hidden meaning"
date: "2024-05-23T10:46:35+10:00"
year: "2024"
category: Thoughts
tag:
- language
location: Sydney
---
[Via Wikiquote](https://en.wikiquote.org/wiki/Paul_Gallico)\:

> No one can be as calculatedly rude as the British, which amazes Americans, who do not understand studied insult and can only offer abuse as a substitute. 

Australians and Kiwis speak English in a similar way. I've been in video calls where an arrogant person thinks they're being praised, and a surface reading of a written transcript would suggest this is the case. But in reality, the Scot and Aussie on the bridge are having a go at them.

It's interesting to think about in terms of English being a *lingua franca*. The surface-level meaning of individual words and phrases varies surprisingly little between places, but there are entire layers of extra meaning depending on context, tone, and the person speaking.

I find Eastern Europeans learning English as a second language are able to pick up on these subtleties better than most, interestingly enough. A Ukrainian or Latvian engineer praising someone in the West for their technical prowess is not someone to be messed with!

Like all broad trends though, there are exceptions to the rule. I've worked with American engineers who can quote Monty Python backwards, and dig into people with incredible wit and subtlety. Likewise, the tone in [my post about gitignore](https://rubenerd.com/git-ignores-gitignore-with-gitignore-in-gitignore/) was missed by two English developers, who emailed to let me know I was dim. However, they *did* pick up on my intent when I thanked them for bestowing their incredible development talent upon me.
