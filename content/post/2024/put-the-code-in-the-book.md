---
title: "Put the code in the book"
date: "2024-02-18T08:33:58+11:00"
thumb: ""
year: "2024"
category: Software
tag:
- books
- education
- programming
location: Sydney
---
[Rich Loveland](https://logicgrimoire.wordpress.com/2023/12/05/an-extreme-antipattern-for-programming-books/) dropped some hard facts on his blog last December:

> An extreme antipattern for a programming book is “download the author’s idiosyncratic library code from some random URL and figure out how to load it on your system”. No, all code used in the book should be printed in the book itself, so the reader can type it in. The URLs always break. The libraries never load properly. A book should never depend on a URL. If you have a small idiosyncratic library, put it in an appendix. If you have a large idiosyncratic library, rewrite your code to be simpler.

