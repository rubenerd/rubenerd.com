---
title: "Quick review of the Pi1541 from Shareware Plus"
date: "2024-03-03T09:15:54+11:00"
thumb: "https://rubenerd.com/files/2024/pi1541.jpg"
year: "2024"
category: Hardware
tag:
- commodore
- retrocomputing
- storage
location: Sydney
---
The [Pi1541](https://cbm-pi1541.firebaseapp.com/ "Pi1541 project page") is a cycle-exact emulator for the venerable Commodore 1541 disk drive. It uses a Raspberry Pi with a custom hat to load and save data to SD cards, which makes it compatible with more software than even my beloved SD2IEC units.

My proper review, usage notes, photos, and screenshots for [this specific version from Shareware Plus](https://sharewareplus.blogspot.com/2019/02/arriving-soon-only-2-left-pi1541.html "Pi1541 Commodore 1541 disk drive emulator C64") is in the works, including testing it on a C128, 64C, Plus/4, and C16. In the meantime, I've left this review on the seller's site: 

> Bought this kit from Tim last month, and am very happy! The build quality is the best of any kit I've ever bought, the instructions are easy to follow, and Tim had it shipped to me in Australia from the UK faster than some domestic purchases.
> 
> If you've been on the fence about building a Pi1541, do yourself a favour and get this one when Tim has stock. Getting into 8-bit machines has been so rewarding, and this has made it even easier and more fun.

Below is the store's photo of the device. This really is the best one you can get; the injection-moulded case is even more polished in person. Not that you should polish a person without their permission. "Homer, did you put your head in the *Shino Ballo?*" "Uhhhh... no".

<figure><p><img src="https://rubenerd.com/files/2024/pi1541.jpg" alt="Photo of the Pi1541, showing the drive and cables." style="width:319px; height:239px;" /></p></figure>
