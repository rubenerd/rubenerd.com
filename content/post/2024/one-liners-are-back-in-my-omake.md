---
title: "One-liners are back on my Omake page"
date: "2024-01-13T09:19:59+11:00"
year: "2024"
category: Software
tag:
- omake
- scripting
location: Sydney
---
I've re-added the **One-liners** section on my [Omake page](https://rubenerd.com/omake.xml), after a few people asked where they went, incluing Clara (cough). I didn't realise people found them useful.

Originally I was going to rewrite these as individual scripts for my [Lunchbox](https://codeberg.org/rubenerd/lunchbox/), but it does make it easier seeing them inline if you need a quick reminder.

I actually have each of these under their own function in a massive Swiss Army Knife script, which sits in my home directory for quick invocation. I've been trying to offload a bunch of these from aliases in my `.kshrc`, which I think was starting to become a bit unwieldy.

Use <a title="Portable OpenBSD ksh" href="https://github.com/ibara/oksh">oksh(1)</a>! But I digress.

There are a bunch more of these scripts I need to add, inclding more to the Lunchbox too. One day I should just sit down and smash them all out. 
