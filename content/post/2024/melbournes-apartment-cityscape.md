---
title: "Melbourne’s apartment cityscape"
date: "2024-04-03T10:25:42+11:00"
thumb: "https://rubenerd.com/files/2024/melbourne-apartments-1@1x.jpg"
year: "2024"
category: Travel
tag:
- australia
- melbourne
- urbanism
location: Sydney
---
We're coming to the end of my Melbourne travel posts, which may be a feature depending on what you originally came to this weird blog for! This post talks less about a specific experience, and more a general observation having spent a week wandering around.

I've been to Melbourne on business dozens of times, but 2006-07 was the last time I came down here seriously for a holiday to explore. The one thing struck me more than anything coming back was just how much the CBD has transformed. Southbank is practically unrecognisable, and large swaths of what locals call the *Hoddle Grid* have completely changed their purpose.

<figure><p><img src="https://rubenerd.com/files/2024/melbourne-cbd@1x.jpg" alt="View down one of the main classical shopping streets." srcset="https://rubenerd.com/files/2024/melbourne-cbd@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

The *CBD* is how those of us in the Commonwealth refer to what Americans call *downtown*. It stands for *Central Business District*, and belies the perceived purpose of these city centres. With suburbia sprawling in all directions by the mid 20th century, the CBD was where business, shopping, and entertainment happened. It was where you'd commute to and from every day.

It's important to remember this wasn't always the case. Downtowns used to be a mix of everything, because they were designed to be convenient for people on foot to access what they needed. It's why it's easy to tell the old capitals of Europe, and awesome places like New York City, from more modern cities either built or greatly expanded after the widespread adoption of the quad-wheeled motorised conveyance, better known as&mdash;say it with me now&mdash;the ca... rcinogenic automobile.

Australia didn't escape rapid suburbanisation. The state capitals here have some of the most impressive (cough) sprawl in the world, and governments have been happy to continue rubber-stamping new peripheral developments utterly detached from their host city. Australia does public transport better than much of North America, but most of these suburbs are still car-dependent.

<figure><p><img src="https://rubenerd.com/files/2024/melbourne-apartments-1@1x.jpg" alt="View of the new Melted Cheese building in Southbank." srcset="https://rubenerd.com/files/2024/melbourne-apartments-1@2x.jpg 2x" style="width:375px; height:500px;" /></p></figure>

Then something started changing in the early 2000s. Modest apartment blocks designed for working-class people and retirees in the inner rings of suburbia began to give way to larger, taller buildings. And I mean *much* taller. CBDs in every major capital began building residential towers that met, and in some cases even exceeded, commercial office spaces.

Melbourne in particular has seen a transformation of its CBD from a near total commercial space into a mixed-use area where huge residential blocks grow out of shopping centres with late-night restaurants. The areas around Swantston Street were busy even late on a Sunday night, which was so energising and fun to be around. They're the *anti-*suburbia.

There are economic factors here that are well beyond the scope of this post. But it also has to be said that these buildings wouldn't be going up, and at such a frantic clip, if there wasn't demand for this kind of housing. Which I think is interesting. It speaks to a sociological shift among Australians away from thinking apartments are for people who can't afford houses, into homes that are desirable and attractive. Or at least, their *locations* make them so.

<figure><p><img src="https://rubenerd.com/files/2024/melbourne-apartments-2@1x.jpg" alt="A bunch of new apartment buildings near the Victoria Markets." srcset="https://rubenerd.com/files/2024/melbourne-apartments-2@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

Melbourne's CBD has apartments *everywhere*. Not only that, they're by far the tallest buildings in the city. There are entire neighbourhoods of them now that didn't exist when I came here in 2006, and plenty more under construction. It wouldn't surprise me to see another half a dozen appear by the time I come back.
