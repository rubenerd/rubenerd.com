---
title: "29th of February"
date: "2024-02-29T10:41:55+11:00"
year: "2024"
category: Internet
tag:
- dates
- pointless-milestone
location: Sydney
---
Posts on this date come only every four years. *Smashing!* I went through the archives to see what I've posted on past leap year days:

* **2024:** [29th of February](https://rubenerd.com/29th-of-february/); how meta

* **2020:** *Nothing*

* **2016:** [Rubenerd Show 326: The 29th of February episode](https://rubenerd.com/show326/)

* **2012:** *Nothing*

* **2008:** [Is embedded spam getting worse?](https://rubenerd.com/is-embedded-spam-getting-worse/)

* **2004:** *I only started in December*

This cadence means I can't post anything in 2028.
