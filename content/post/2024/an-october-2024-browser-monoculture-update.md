---
title: "An October 2024 browser monoculture update"
date: "2024-10-01T23:23:47+10:00"
year: "2024"
category: Software
tag:
- browsers
- monoculture
location: Sydney
---
Wait, we're in *October* of this year now? Where does the time go.

[I wrote about browser monocultures back in 2021](https://rubenerd.com/chromium-monoculture-marches-on/)\:

> I rasied the impending Chromium monoculture [back in August last year](https://rubenerd.com/firefoxs-situation-reminds-me-of-openssl/). I raised the issue about why monocultures are bad, why it is the same thing we dealt with during the dark IE days, and what we need to do to address it again.
> 
> The feedback was mostly sympathetic, though I still had a few people claiming Chrome is better placed than IE (which I already addressed), and that the number of Chrome-specific sites are too insignificant to matter. As my old man says, I wish I shared their optimism!

Well, we're long overdue for another status update. What's it like using Firefox/Waterfox as your primary browser in 2024?

**Short answer:** Not great.

**Slightly-longer answer:** Is a phrase with three words.

**Longer answer:** I use about twelve sites daily for my current job, and I have about fifteen I use for most of what I call *life admin*. Seven of these no longer work in Firefox. That's a success rate for my primary browser of 85%. Wow.

I was surprised by the number of people claiming browser monocultures weren't a thing in 2021. They're still apparently not a thing, but they're *even more* not a thing than they were before. Wait, I'm confusing myself, which one am I?
