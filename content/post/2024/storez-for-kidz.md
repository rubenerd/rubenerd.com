---
title: "Storez for kidz"
date: "2024-11-08T07:28:56+11:00"
thumb: "https://rubenerd.com/files/2024/shoe-store-for-kids@1x.jpg"
year: "2024"
category: Thoughts
tag:
- advertising
location: Sydney
---
Here's something that's occupied a portion of mental CPU cycles for decades at this point. Take this shop for kids shoes in our local shopping centre:

<figure><p><img src="https://rubenerd.com/files/2024/shoe-store-for-kids@1x.jpg" alt="A closed store for kids, called Shoes and Sox" srcset="https://rubenerd.com/files/2024/shoe-store-for-kids@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

Why can't stores for kids... spell?

You see this everywhere. The legendary *Toys R Us*. *Kidz Cutz*, which was a terrifyingly-named hairdresser.

I know, they're trying to be cutesy and give the impression of immaturity. But don't they know their clientele are learning how to spell? I wonder how many young schoolkids flunked a test because they recalled a place to which parents regularly took them.

Maybe it's a subtle lesson for not believing everything they read on signs. Some people may need a refresher on that.
