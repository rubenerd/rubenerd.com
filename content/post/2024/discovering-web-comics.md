---
title: "Discovering web comics"
date: "2024-11-13T08:24:00+11:00"
year: "2024"
category: Media
tag:
- comics
- web-comics
location: Sydney
---
I loved reading syndicated comic strips when I was a kid. I had all the *Garfield Fat Cat Three Pack* volumes, and oodles of *Far Side* and *FoxTrot*. It was also so much fun as a kid seeing them adopt Xmas decorations at that time of year.

And yet, I never really made the transition to web comics; [Matchstick Cats](https://intoyourhead.ie/category/comics/) from my friend Neil notwithstanding. I only just discovered [BitSmack](https://bitsmack.com/), and [Joshua O’Brien](https://mastodon.social/@joshuaobrien/113468562471977600) recommended [Saturday Morning Breakfast Cereal](https://www.smbc-comics.com/). Then there's the imitible [Oatmeal](https://theoatmeal.com/comics).

Let me know if you have any recommendations for someone who's interests slant technical and either silly or surreal :).

(... please no xkcd though).
