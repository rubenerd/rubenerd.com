---
title: "Hololive Suisei and Marine collab art"
date: "2024-12-28T09:11:44+11:00"
abstract: "The key visual from their BE@RBRICK collaboration in October was awesome!"
year: "2024"
category: Anime
tag:
- art
- hololive
- hoshimachi-suisei
- music
location: Sydney
---
Hololive divas Hoshimachi Suisei and Houshou Marine [had a collab](https://hololivepro.com/news_en/20241022-01-124/ "Collab limited edition BE@RBRICK Hoshimachi Suisei ver. and Houshou Marine ver. to be released!") with BE@RBRICK in October that I only just saw. The art is amazing!

<figure><p><img src="https://rubenerd.com/files/2024/suiseimarine@1x.jpg" alt="Official art of the characters with their BE@RBRICK designs." srcset="https://rubenerd.com/files/2024/suiseimarine@2x.jpg 2x" style="width:400px; height:566px;" /></p></figure>

It looks like the BE@RBRICKs themselves are [still available for sale](https://shop.hololivepro.com/en/search?q=bearbrick) if that's your thing. And no, I wasn't paid for this post. Maybe I was bribed by art though.
