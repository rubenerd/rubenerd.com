---
title: "Cautious optimism for the industry after AI pops"
date: "2024-07-10T20:42:30+10:00"
year: "2024"
category: Software
tag:
- ai
- business
location: Sydney
---
You know that English saying that history doesn't repeat, but that it rhymes? "AI" is now that rhyme. Like Big Data, the Metaverse, VR, and Blockchain, AI has reached stratospheric levels of hype and investment without the scruples, mathematics, or business models needed to sustain it. It will therefore deliver the same outcome.

This naturally leads me to wonder what comes next.

Part of the reason AI was such a godsend for the industry was that it let them soak the excess of the previous bubble. Blockchain speculation lead to huge investments in graphics hardware and APIs that could be broadly repurposed for training AI models. Before that, we had huge investments in Big Data and Machine Learning that created the data centres and massive clusters that could host the latest smoke and mirror machines sold as the solution to everything.

This time may be trickier. How do you repurpose a data centre full of expensive equipment that can only operate by burning investment cash by the proverbial truckload?

AI will burst like all previous bubbles, and it will bring down a lot of people with it; innocent and otherwise. We'll eventually view AI as another tool like compression algorithms, cryptographic ciphers, and hat racks. Evolutionary, not revolutionary.

The fallout will be ugly. But it will also be an opportunity. Dare I say, I'm a bit excited for it.

Tech can be wonderful, but weaning it off the need to be fuelled by hype and bluster will be as complicated and necessary as our transition from fossil fuels. But from that will come immeasurable good. It'll free up opportunity costs, so to speak, to tackle real problems again.

There is *incredible*, life changing, useful, fun, creative, productive, and wonderful innovation out there waiting to be explored, researched, and funded, that simply can't right now because people can't slap blockchain on it. Wait, AI. Or whatever. The time where they become viable may be sooner than we all think.

Maybe then, the public will start to like tech again.
