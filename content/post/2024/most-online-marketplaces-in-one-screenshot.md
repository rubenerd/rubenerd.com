---
title: "Most online marketplaces, in one screenshot"
date: "2024-03-05T07:39:33+11:00"
thumb: "https://rubenerd.com/files/2024/auction-spam@1x.jpg"
year: "2024"
category: Internet
tag:
- cory-doctorow
- enshittification
- spam
- stores
location: Sydney
---
I'm *down* for Cory Doctorow's description of modern web services having entered a state of *[enshittification](https://en.wiktionary.org/wiki/enshittification "Wiktionary definition of Enshittification")*. He's discussed one prominent merchant example named for a South American rainforest, but I've noticed it among plenty of other stores.

Specifically, spend any time on a marketplace that allows people to post listings, and you'll eventually encounter something like this. It's the inevitable endgame of the site format, whether its a general auction site like eBay, or a site for independent arts and crafts.

<figure><p><img src="https://rubenerd.com/files/2024/auction-spam@1x.jpg" alt="A wall of search results for the same motherboard." srcset="https://rubenerd.com/files/2024/auction-spam@2x.jpg 2x" style="width:500px; height:333px;" /></p></figure>

For my visually impaired friends, this is broadly the same listing for a rubbish motherboard reposted *dozens* of times, all within the same price. I'm using PicClick to visualise eBay listings, but the experience is very similar on the official site.

It's kinda fun seeing what nonsense the sellers have overlaid on the same stock photo to make their listings stand out. Some put Australian flags across it, or attempt to highlight their listing with a coloured border. Some exclaim *their* dropshipped item is on sale, or has free shipping, or is based in Australia, or will deliver with Express Post. These claims are lies of course; I've had someone proudly state their item was in Sydney, only for it to take a month to arrive with a China Post franking label dated after I purchased the item.

But back to the images being largely the same. Surely we could use this to our advantage? If a site was designed to serve *people*, it'd be easy to implement a search filter to hide listings with similar images. I've had saved searches go from thousands to dozens of items when I've crafted long and tedious queries to filter out specific dropshipped goods, only for it to be outdated in a week.

I suppose dropshippers would attempt to work around this by manipulating the images enough to avoid triggering the filter. I'd still consider that a win however, because it would represent an additional hurdle to spamming.

