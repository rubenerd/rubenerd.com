---
title: "What we can learn from _why"
date: "2024-12-06T15:47:09+11:00"
abstract: "He taught countless people the joy of programming."
year: "2024"
category: Software
tag:
- programming
- ruby
location: Sydney
---
I loved this [retrospective](https://github.com/readme/featured/why-the-lucky-stiff "What we can learn from _why, the long lost open source developer") by Klint Finley:

> Perhaps most importantly, he taught countless people the joy of programming. \_why showed veteran coders and n00bs alike a curious, adventurous, and creative side of programming. He demonstrated that code could be more than just a form of technical problem solving: it could be a form of self-expression and of art.

I first learned programming from Borland's Learn to Program books, the Perl Camel Book, and [\_Why's Poignant Guide to Ruby](https://poignant.guide/book/). All were invaluable and fun, but it's the latter from which I can recite entire passages. Chunky bacon!

I hope wherever he is, he's happy and doing well. He touched a lot of lives. ♡
