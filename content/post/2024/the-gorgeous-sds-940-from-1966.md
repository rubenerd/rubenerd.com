---
title: "The gorgeous SDS 940 from 1966"
date: "2024-09-29T17:37:03+10:00"
thumb: "https://rubenerd.com/files/2024/102649032p-03-01@1x.jpg"
year: "2024"
category: Hardware
tag:
- history
- retrocomputers
location: Sydney
---
I realised my blog here has reached 940 pages. With ten posts per page, that's a lot of posts. But who's counting posts? Not me, I'm only counting pages.

As I read that number on my site footer however, I couldn't shake the feeling that this number held significance for some reason. [Wikipedia jogged my memory](https://en.wikipedia.org/wiki/SDS_940)\:

> The SDS 940 was Scientific Data Systems' (SDS) first machine designed to directly support time-sharing. The 940 was based on the SDS 930's 24-bit CPU, with additional circuitry to provide protected memory and virtual memory.

I did an assignment about this machine at uni! I think it's one of the few interface panels that give my beloved DEC a run for their money:

<figure><p><img src="https://rubenerd.com/files/2024/102649032p-03-01@2x.jpg" alt="Panel for the SDS-940" style="width:300px;" /></p></figure>

With thanks to [Mark Richards at the Computer History Museum](https://www.computerhistory.org/revolution/mainframe-computers/7/181/730) for sharing this stunning machine. One day I'll make an analogue of it with a Raspberry Pi and some Das Blinkin Lights, just you wait and see!
