---
title: "A simple 86box shell script menu"
date: "2024-08-06T09:49:08+10:00"
year: "2024"
category: Software
tag:
- 86box
- retrocomputing
- scripts
location: Sydney
---
I love [86Box](https://86box.net/) for emulating classic PC hardware, but it doesn't come with a VM manager by default. Instead, the software will always boot the last VM configuration you set. Programs exist to give you a more VirtualBox-like experience, where you can select, configure, and boot a range of VMs.

But what if you're silly, and want to launch it from a script? Easy! 86Box can be invoked with the `--config` option, where you can pass it config files. Here's the simplest menu I could think of:

    #!/usr/bin/env sh
       
    LAUNCHER="86Box --config"
        
    printf "1. IBM 5060, CP/M-86\n"
    printf "2. Commodore PC 30 III, MS-DOS 3.2\n"
    printf "X. Exit\n"
      
    while :
    do
        IFS= read -r CHOICE
           
        case $CHOICE in
            1)
                $LAUNCHER ./86box-xt-cpm86.cfg
                exit
                ;;
            2)
                $LAUNCHER ./86box-pc30-dos32.cfg
                exit
                ;;
            B|b)
                printf "The bird is the word\n"
                ;;
            X|x)
                exit
                ;;
        esac
    done

This is basically the same script I use for all my Xen, Bhyve, and QEMU guests. It presents a menu where I can choose which VM I want to launch, and asks again if I typo an answer. I have more VMs than this, but you get the idea. And before you ask, yes, the *Surfin Bird* reference is mandatory.

Another option would be to simply build the menu dynamically each time based on the available configs matching `*cfg` in a chosen folder, but I like that I can give these pretty names, and would let me do more prep for individual VMs if I wanted.

Then you just need some config files. Invoking 86Box without any options creates/uses a file called `86Box.cfg`, which you can later rename and use with the aforementioned `--config` option. *Done!*
