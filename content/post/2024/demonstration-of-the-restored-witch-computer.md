---
title: "Demonstration of the restored WITCH computer"
date: "2024-03-20T14:58:30+11:00"
year: "2024"
category: Hardware
tag:
- history
- retrocomputing
- video
location: Sydney
---
[Nico Cartron](https://www.ncartron.org/) noted my post on Monday about the [women behind Bletchley Park](https://rubenerd.com/the-women-of-newnham-and-bletchley-park/), and shared a video he took back in 2013:

<figure><p><a target=_BLANK href="https://www.youtube.com/watch?v=XDHWWvEk6eI" title="Play The WITCH Computer at The National Museum of Computing - Betchley Park, UK #TNMOC"><img src="https://rubenerd.com/files/2024/yt-XDHWWvEk6eI@1x.jpg" srcset="https://rubenerd.com/files/2024/yt-XDHWWvEk6eI@1x.jpg 1x, https://rubenerd.com/files/2024/yt-XDHWWvEk6eI@2x.jpg 2x" alt="Play The WITCH Computer at The National Museum of Computing - Betchley Park, UK #TNMOC" style="width:500px;height:281px;" /></a></p></figure>

This is a demonstration of the Wolverhampton Instrument for Teaching Computing from Harwell (WITCH) computer at the Bletchley Park museum. Seeing those valves, and *hearing* all those relays is incredible.

I often talk about loving 8-bit computers because the frequencies and signals involved can be visualised by mere mortals with amateur equipment. Here's a computer you can visualise  as it operates, with just your eyes and/or ears!

