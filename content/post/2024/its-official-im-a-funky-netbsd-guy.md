---
title: "It’s official: I’m a funky NetBSD guy"
date: "2024-10-06T17:17:21+11:00"
year: "2024"
category: Software
tag:
- bsd
- netbsd
location: Sydney
---
I've redacted the source now, but it's still pretty funny:

> dumbfunk low level netbsd admin guy with a silly fedora and a shitty blog thinks he is [sic] hotshot

Recognition was a long-time coming, but I'm flattered! 🧡

You can view related posts with the [NetBSD](https://rubenerd.com/tag/netbsd/) and [BSD](https://rubenerd.com/tag/bsd/) tags. Give it a try if you've only ever used Linux or other BSDs; there's a lot to like.

*<strong>Update</strong>: A few of you have expressed concern. Don't worry, it isn't half as bad as 90% of the comments I get when I'm featured on Hacker News!*
