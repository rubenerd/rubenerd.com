---
title: "Keeping Hi-Fi gear cool"
date: "2024-07-09T08:49:10+10:00"
year: "2024"
category: Hardware
tag:
- hi-fi
- music
location: Sydney
---
Clara and I recently downsized our Hi-Fi setup, in part so we'd have less to pack when we move, but also because the Marantz amplifier we inherited was huge, heavy, and needlessly powerful for our bookshelf speakers in a small apartment. We never had the volume nob set beyond 15%, and when we've gone beyond that by accident, our ears (and the ears of our neighbours) exploded.

The other reason came down to heat. This amp, while packed with features and with excellent sound quality, gets *hot*. Again, this was partly down to the device being way overpowered for what we need, which is a waste of power. But it also came down to the semi-enclosed cupboard it has to run in.

When I was growing up, my dad brute-forced a solution by mounted a large diameter, low-speed fan to the back of the cabinet containing all the Hi-Fi gear. Our current shelving arrangement prevents this, because it sits against the wall.

<figure><p><img src="https://rubenerd.com/files/2024/jvc@1x.jpg" alt="1983 pamphlet" srcset="https://rubenerd.com/files/2024/jvc@2x.jpg 2x" style="width:500px; height:162px;" /></p></figure>

Our new amp is much smaller, cooler, and lower-powered than the one it replaced, and will be the topic for another post (it's also more retro)! But it still gets a bit warm, which lead me to wonder if it were possible to keep it cool with an active device of some sort. If this were a server rack, I'd fit a 3U rack mounted fan array into it.

*Turns out*, there are specialised devices to do this. The AC Infinity "Aircom" series mounts a set of large, quiet fans in a case that sits atop hot Hi-Fi gear and electronics. They have temperature sensors to let them respond with the appropriate fan speed for the amount of heat,  and can be set to draw fresh air into them, or pull hot air out. This unit has a fancy LED panel, though I'd be fine with their budget models that are adorned with little more than a power button.

<figure><p><img src="https://rubenerd.com/files/2024/aircom@1x.jpg" alt="Photo of an Aircom active cooling device, showing the front air intakes/exhaust." srcset="https://rubenerd.com/files/2024/aircom@2x.jpg 2x" style="width:500px; height:120px;" /></p></figure>

I suppose though this leads to the question of necessity. Keeping Hi-Fi gear cool would theoretically be great for their longevity, by reducing the thermal stresses to which components are subjected. Here comes the proverbial posterior prognostication: *but...* presumably Hi-Fi gear is also designed with tolerances to handle these thermal loads. An amplifier getting warm is an expected side-effect of its operation, as is anything with a transformer.

We tend to run older kit, because we love that 1980s and early 1990s aesthetic (components were also just... better at that time; modern turntables and tape decks are kind of rubbish, but again, a topic for another post). Keeping these older components operating probably does require a bit more TLC, because over time they develop issues.

Provided these active cooling devices are as quiet as they claim, I'm tempted to get one when funds are available to try. At the very least, they'd get more airflow and circulation where the components are forced to sit at the moment, which could only be a good thing.

