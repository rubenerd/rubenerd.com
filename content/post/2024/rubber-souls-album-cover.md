---
title: "Rubber Soul’s album cover"
date: "2024-03-27T09:50:07+11:00"
thumb: "https://rubenerd.com/files/2024/rubber-soul@1x.jpg"
year: "2024"
category: Media
tag:
- art
- music
- the-beatles
location: Melbourne
---
Notice anything?

<figure><p><img src="https://rubenerd.com/files/2024/rubber-soul@1x.jpg" alt="Rubber Soul album cover" srcset="https://rubenerd.com/files/2024/rubber-soul@2x.jpg 2x" style="width:420px; height:420px;" /></p></figure>

**Their name isn't on the cover!**

I was *15* when I realised they're called *The Beatles* because it has the word *Beat* in it. It also took me [reading Wikipedia](https://en.wikipedia.org/wiki/Rubber_Soul "Rubber Soul article on Wikipedia") to realise it was their first album that doesn't mention their name anywhere on the front.

I'm still coming to terms with this. I must have seen this cover a trillion times over my life.
