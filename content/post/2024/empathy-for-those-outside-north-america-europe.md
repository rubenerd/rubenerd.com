---
title: "Empathy for those outside North America and Europe"
date: "2024-02-26T08:32:14+11:00"
year: "2024"
category: Media
tag:
- europe
- geography
- north-america
- public-transport
- trains
location: Sydney
---
Every now and then, something reminds me that in the minds of many Westerners, there's North America and Europe, and precious little else.

Yesterday it was a meme video showing a stadium of Taylor Swift fans [emptying into trains in Sydney](https://twitter.com/fictillius/status/1761263111691202655 "Clip shared by Evan"). Many of the comments expressed surprise that a "European" double-deck train could move so many people so efficiently after a concert. They knew the video didn't depict North America, so they naturally assumed it had to be Eu... rope.

> It had to be Eu... it had to be you. I wandered around, and finally found, somebody who... could make be be true. Could make me be blue. Or even be glad, just to be sad, thinking of you.

Hey, this turned into a [Music Monday](https://rubenerd.com/tag/music-monday/)! This is the legendary Harry Connick Jr, an artist I missed perfomring here last year, damn it. But I digress.

<figure><p><a target=_BLANK href="https://www.youtube.com/watch?v=_UnQOfPwZfs" title="Play Harry Connick Jr. - It Had to Be You"><img src="https://rubenerd.com/files/2024/yt-_UnQOfPwZfs@1x.jpg" srcset="https://rubenerd.com/files/2024/yt-_UnQOfPwZfs@1x.jpg 1x, https://rubenerd.com/files/2024/yt-_UnQOfPwZfs@2x.jpg 2x" alt="Play Harry Connick Jr. - It Had to Be You" style="width:500px;height:281px;" /></a></p></figure>

This geographic assumption is interesting, if not surprising.

First, it ignores that *other countries have trains*. Their trains are almost always better than ours too; I'm looking at you, Japan. Even cities in countries certain Westerners would consider beneath them have better public transport than most of North America and Australia. *I would know!* The idea of "exceptionalism" blinkers people to this idea completely, to the point where it's not even a consideration.

It's this closed-minded attitude that explains so much of how people view the world. Or I suppose in this case, how they *don't* view the world. If other places don't even cross your mind, of course you're going to view global challenges with disdain or apathy. It's the same reason why you'll hear a podcaster say "in the summer" without concern. 

This gets us to Australia and New Zealand. We're in a unique position here; on the one hand, we receive media attention disproportionate to our population, just as Europe and North America do. But on the other hand, *you have different fingers* (I promised myself I'd never make that joke here). Pictures of our places are assumed to be elsewhere, because if a country is well off and has a lot of white people, naturally it has to be in one of two places.

This doesn't apply to my regular American readers and listeners; my silly writing tends to self-select for people who are keenly aware the rest of the world exists. But the furore around this post demonstrates there's still a sizable population who either don't know, aren't curious, or don't care.

*(The irony is, many Americans have intimate experience with what this is like at a domestic level. The world, and much American media, treats the US as two coasts sandwiching an otherwise empty landmass. Maybe that shared experience of being ignored hints to how we can address this).*

Anyway, a big shout out to those of you who live elsewhere on this beautiful blue orb of ours. Let's keep reminding people we exist.
