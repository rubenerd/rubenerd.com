---
title: "PwC: The report is coming, bro!"
date: "2024-02-11T11:08:08+11:00"
year: "2024"
category: Thoughts
tag:
- finance
- news
location: Sydney
---
[Henry Belot](https://www.theguardian.com/australia-news/live/2024/feb/09/australia-news-live-earthquake-melbourne-dunkley-anthony-albanese-peter-dutton-penny-wong-quake-magnitude-cost-of-living?page=with:block-65c55b028f08b8fa87cc8a2b#block-65c55b028f08b8fa87cc8a2b) reported something that's just too good:

> The Australian Tax Office has still not received a report by the law firm Linklaters, which was cited by PwC global’s executive last year to clear its international partners of any wrongdoing.

We did nothing wrong! We have a report that proves it! Oh it's coming, don't worry! Can you imagine if an employee tried this stunt on their boss?

Weirdly enough, I also have a report saying PwC owes my [favourite cancer research charity](https://www.curecancer.com.au/) and I one billion dollars. It's 100% legit and absolutely, definitely exists. Trust me! To whom should I send bank details?
