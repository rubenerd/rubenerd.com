---
title: "Room elephants"
date: "2024-06-27T07:04:19+10:00"
year: "2024"
category: Thoughts
tag:
- elephants
- quotes
location: Sydney
---
[@JustFrank@mas.to](https://mas.to/@JustFrank/112679661839808255)\:

> Let's talk about the elephant in the room. I don't know who brought it in here but I'm not cleaning that up.
