---
title: "Wouter Groeneveld on retrotech"
date: "2024-11-03T10:10:09+11:00"
year: "2024"
category: Hardware
tag:
- retrocomputing
- life
location: Sydney
---
Wouter recently [fixed his 486 machine](https://brainbaking.com/post/2024/10/replacing-a-baby-at-motherboard/ "Replacing a Baby AT Motherboard ") with a new motherboard, and made this observation:

> The result: it works! Well… Something else stopped working correctly. Of course it did. Repairing old hardware is like a yin yang thing: something is fixed, something else breaks. To ensure that ever-lasting balance—all for the greater good.

I can't overstate just how true this is. Get that VLB graphics card working? That might conflict with your ISA SCSI controller. Add more than 4 MiB of RAM, or some more cache? Sorry, now this BIOS setting does something funky. Add a 387 co-processor? Now this OS you had installed before doesn't boot. It's a balancing act figuring out what you want out of your machine, like a glorious puzzle of frustration and triumph.

As I put on the [advice page](http://retro.rubenerd.com/advice.htm) on my Retro site:

> Be willing to accept that much, if not most, of your hardware won't be operable at any one time. Old hardware breaks, and fixing things is a journey.

It's probably true of modern tech too, especially in light of how complicated we insist on making things for dubious or ill-defined benefits.
