---
title: "Exploring FreeBSD service(8) basics"
date: "2024-01-05T23:39:13+11:00"
year: "2024"
category: Software
tag:
- bsd
- freebsd
- netbsd
location: Sydney
---
A *service* in BSD land is a rc.d script that can be invoked on boot, or by the root operator after booting.

These are table stakes for any online guide, but I often see people making the same mistakes, or not doing things in the most efficient way. I think this might be due to Linux people *porting* their guides, without understanding BSD specifics. I thought it was worth taking a quick look.


### Starting a service

You start a service by executing its rc.d script. Like NetBSD, FreeBSD's default location is here:

    # /etc/rc.d/openntpd

In addition, services you install via ports may reside under `local`:

    # /usr/local/etc/rc.d/openntpd

*(Thanks to Felix J. Ogris and @Azunyan for correcting a typo there).*

A shortcut to these is the `service(8)` command, which Douglas Barton first wrote for FreeBSD 7.3, and was ported to NetBSD 7.0 by Adrian Steinmann. This should look familiar to those of you from penguin-land: 

    # service openntpd start

But here's where you might run into your first problem.

    Cannot 'start' openntpd. Set openntpd_enable to 
    YES in /etc/rc.conf or use 'onestart' instead of 'start'.

OSs like Debian will tend to enable and start services as soon as you install them. This is great for instant gratification, but the BSDs are more cautious. Personally, the last thing I'd want for a network-facing service is to have it enabled before I've configured it, or locked it down.

As it suggests, you can run `onestart` instead of start if you want to launch the service immediately. But for a persistent service, you'll want to enable it.


### Manually enabling a service

You enable a service by including a line in FreeBSD and NetBSD's `/etc/rc.conf` file. On FreeBSD:

    openntpd_enable="YES"

And on NetBSD:

    openntpd=YES

Another location you can write files to enable services is in `/etc/rc.conf.d/`. Like all configuration directories, this can be useful if you want to keep your base `rc.conf` file the same across a fleet, and drop in specifics into a seperate location.

There are even more places, which can be invoked at different times. Refer to [rc.local(8)](https://man.freebsd.org/cgi/man.cgi?rc.local(8)) for details.

But how to write to these files? Online guides will tend to suggest you write these lines manually, or do something like this:

    # echo openntpd_enable="YES" >> /etc/rc.conf
    # echo openntpd=YES >> /etc/rc.conf

Or if they're a [bit more clued-in](https://www.in-ulm.de/~mascheck/various/echo+printf/ "Sven Mascheck's page about echo and printf")\:

    # printf "%s\n" openntpd_enable="YES" >> /etc/rc.conf

This *usually* works, but there are better ways.


### Smarter ways of enabling services

Starting with FreeBSD 9.2, we have Devin Teske's `sysrc(8)` tool. This updates your `rc.conf` in an idempotent fashion. This means you don't end up with duplicate and/or contradictory lines if you accidentally add config twice.

    # sysrc openntpd_enable="YES"

But it can also configure much more, and can even append config to existing lines. This is where it really shines:

    # sysrc update_motd="NO"
    # sysrc jail_start+="lb minecraft hatsunemiku"

Keen-eyed readers may have just spot an issue though. While you typically enable and start a service with the same name, they don't always match. Did you know you enable/disable `motd(8)` with `update_motd` instead? Another example is MariaDB:

    sysrc mysql_enable="YES"
    service mysql-server start

It's important to note that `sysrc(8)`, `echo(1)`, and `printf(1)` will dutifully write whatever config you request, regardless of their validity. There's a joke in there somewhere about writing tenders; ask me how I know.


### An even smarter way to enable services

In this case, it's usually better to enable services with the surprisingly-named `enable` command:

    # service openntpd enable
    # service openntpd start

This requires the rc.d script to have been written with the appropriate configuration, though I've yet to come across something from pkgsrc or FreeBSD ports that *doesn't* have such config.

NetBSD rc.d scripts don't have an `enable` function like this, at least from the ones I've tried. I'm continuing to grow my personal NetBSD knowledge, so if there's an equivilent tool for it, let me know!


### Conclusion

Could this entire post have just been that last code block? Probably! But I enjoy reading when other people follow a train of thought and explore something, rather than just doing a couple lines wrapped in boilerplate to sell ads.

Happy BSD daemon’ing. 😈


### Related links

* [FreeBSD rc.local(8) manpage](https://man.freebsd.org/cgi/man.cgi?rc.local(8))
* [NetBSD handbook: The rc.d System](https://www.netbsd.org/docs/guide/en/chap-rc.html)
* [NetBSD service(8) manpage](https://man.netbsd.org/service.8)
* [Practical rc.d scripting in FreeBSD](https://docs.freebsd.org/en/articles/rc-scripting/)
