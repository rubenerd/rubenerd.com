---
title: "Explaining after the fact"
date: "2024-11-08T07:41:34+11:00"
year: "2024"
category: Thoughts
tag:
- journalism
location: Sydney
---
It happens after every big political and sports event. Pundits are publishing their analyses, and they already have tidy, convenient explanations for the outcome.

> `$FOO` **clearly** failed because of X. `$BAR` **clearly** succeeded because of Y.

Thing is, if the outcome had been different, they'd offer the exact same explanations.

> `$FOO` **clearly** succeeded because of X. `$BAR` **clearly** failed because of Y.

So many biographies I've read do just this. If someone grew up poor, the writer will use it to explain why their subject was thrifty, or spent to ruinous excess. They have facts and events, and they draw whatever connecting line they want. There's no interrogation, no curiosity, just expediency. I'd call it a shallow dive, but they didn't even left the board.

If these pundits published their reasons as predictions a week earlier, then analysed them after the fact, I might believe them more. Sure, hindsight is 20/20, but you don't get to say "I told you so"... when you didn't! Well you *can*, but it's transparently silly, like a clear umbrella topped with an inflatable fish. I'm not sure where I was going with that metaphor, something something Monty Python?
