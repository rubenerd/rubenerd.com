---
title: "To be seen working"
date: "2024-07-27T16:37:34+10:00"
year: "2024"
category: Thoughts
tag:
- work
location: Sydney
---
This is my Codeberg public activity graph. There are many like it, such as those green ones on GitHub, but this one is mine:

<figure><p><img src="https://rubenerd.com/files/2024/codeberg-graph.png" style="width:640px;" /></p></figure>

What does it tell you? Does it say I'm prolific, or productive, or professional? The truth is, it says basically nothing. It's a pretty graph that mildly guilts people away from taking breaks, but that's about it.

Public activity graphs are a prime example of [Goodhart's Law](https://en.wikipedia.org/wiki/Goodhart%27s_law): when a measure becomes a target, it ceases to be a good measure. When you prioritise the quantity and regularly of contributions over quality, you'll have people contribute to make the colour of a square go darker. Throw [hackerthon shirts](https://rubenerd.com/hacktoberfest-needs-to-stop/ "Hacktoberfest needs to stop") into the mix, and you get spam that would make a generative AI blush.

🌲 🌲 🌲

I realised this is a variation on the *it's insufficient to work, you have to be seen working* advice many of us received when we entered the workforce. It comes from a good place; your manager, or clients, can't acknowledge contributions for which they have no knowledge. But for a surprising number of people, the tentpole word *insufficient* in the advice is ignored or forgotten. To them, performative work is a *replacement* for real work, not an addition.

What do I mean by performative work? Well, say your colleague communicates that they're doing something, without actually doing it. There's *productivity porn*, where you build a complicated task management system in lieu of... completing tasks! And there's issuing multiple, unsquished pull requests and commits to a repo, purely for the sake of *making number go up*.

I'm realising that *performative work* is one of my major bug bears. It's frustrating when I have to deal with it in others, and I'm disappointed when I catch myself doing it. Let's get to doing real work!

Wait&hellip; is this blog real work? Don't answer that.
