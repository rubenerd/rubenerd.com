---
title: "📚 RMS Lusitania: A History in Picture Postcards"
date: "2024-04-06T16:46:49+11:00"
thumb: "https://rubenerd.com/files/2024/lusitania-book@1x.jpg"
year: "2024"
category: Thoughts
tag:
- books
- lusitania
- ocean-liners
location: Sydney
---
I haven't done a book review here in years. Today we're looking at this beautiful collection collated and written by historian Eric Sauder in 2015 that arrived from the UK:

<figure><p><img src="https://rubenerd.com/files/2024/lusitania-book@1x.jpg" alt="Cover of the aforementioned book, with the Lusitania on the cover." srcset="https://rubenerd.com/files/2024/lusitania-book@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

Does the slotted balcony table and plant look at all like I'm sitting at her first class *Verandah Café?* Probably not, but it was worth a shot.

As the title suggests, the book weaves the story of Cunard's 1905 liner RMS *Lusitania* from contemporary postcards and their handwritten notes. It's a fascinating concept, and more touching than I was expecting.

Like *Titanic*, Cunard's first ocean greyhound is best known for her tragic demise at the hands of a German U-boat.  If, like me, your interest stretches back to her conception and design, there isn't much out there relative to her sister ship *Mauretania*, or any of the other great liners from the time period. This is a shame, because as Eric himself says in the forward:

> The public's interest in Lusitania continued even after her loss but for different reasons [to Mauretania]. She ultimately became a rallying cry for troops on the front lines and propaganda posters, her achievements and success forgotten.
>
> Although not an exhaustive history of this extraordinary liner, this book will hopefully pique the interest of some readers to find out more about this magnificent vessel.

The book is divided into several sections, from her construction and sea trials, to her operational service, interiors, and her eventual sinking. Each card is presented with a detailed caption adding additional information and context, and occasionally the written notes sent from passengers to their loved ones.

<figure><p><img src="https://rubenerd.com/files/2024/lusitania-book-inside@1x.jpg" alt="A page showing a photo of the first class library" srcset="https://rubenerd.com/files/2024/lusitania-book-inside@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

This one came from passenger Percy Seccombe before *Lustania's* last voyage. He did not survive.

> Dear Mary,   
> I all most forgot to send you a line. Here is one, now take care of it, for it is valuable. Be good.   
> Piles of love.   
> P.

Perhaps my favourite of the designs included a colourised photo of a gentleman standing on the boat deck as he peered into the distance, with a poem written below that tugs at the heartstrings:

> A big ship is sailing for the dear old shores,   
> And the youth is bound for home;   
> He longs for the one glace of the land he adores,   
> As the ship speeds o'er the foam.   
> Though rich, that old picture he wears next to his heart   
> Just the same as in days of yore,   
> Then he vows with that mascot he never will part,   
> These fond words recalling once more.

I'd been leaning towards ebooks over the last decade, but I'm so glad I bought a hardcopy of this one. Photobooks are never as good digitally, and this one was wonderful to sit and read on the couch over a cup of tea. I tend to donate or pay forward physical books when I finish reading, but this is going on the shelf.

I had to order the book from eBay, because nobody in Australia stocked it that I could find. But I thoroughly recommend it. 🚢


### Details

* **Title**: RMS Lusitania, A History in Picture Postcards
* **Author**: Eric Sauder
* **Publisher**: The History Press, 2015
* **ISBN**: 0750962801 / 978-0750962803
