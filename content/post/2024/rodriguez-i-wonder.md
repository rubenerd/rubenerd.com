---
title: "Rodriguez, I Wonder"
date: "2024-04-08T11:54:24+10:00"
thumb: "https://rubenerd.com/files/2024/yt-gVWmKG_eBPo@1x.jpg"
year: "2024"
category: Media
tag:
- folk
- music
- music-monday
- rodriguez
location: Sydney
---
It's [Music Monday](https://rubenerd.com/tag/music-monday/) time! Each and every *etc*, I *something* here for *stuff*.

Today we have one of Rodriguez's best songs from his seminal 1970 album *Cold Fact*. It still blows me away that this talented American singer/songwriter was huge in South Africa and Australia, but relatively obscure in his home country.

<figure><p><a target="_BLANK" href="https://www.youtube.com/watch?v=gVWmKG_eBPo" title="Play I Wonder"><img src="https://rubenerd.com/files/2024/yt-gVWmKG_eBPo@1x.jpg" srcset="https://rubenerd.com/files/2024/yt-gVWmKG_eBPo@1x.jpg 1x, https://rubenerd.com/files/2024/yt-gVWmKG_eBPo@2x.jpg 2x" alt="Play I Wonder" style="width:500px;height:281px;" /></a></p></figure>


