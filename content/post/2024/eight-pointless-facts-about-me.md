---
title: "Eight pointless facts about me"
date: "2024-09-07T19:13:21+10:00"
year: "2024"
category: Thoughts
tag:
- bsd
- fun-facts
- personal
- pointless
location: Sydney
---
Back by popular demand! Wait, don't interrogate that.

* NetBSD's pkgsrc has more packages than my cupboard.


* I've inexplicably lived in two Australian suburbs called `$Foo` Lakes. But not in the lakes themselves.

* My eyes are slightly green, but mostly brown. I'd look at them closely, but, you know, I'm looking through them.

* Clara is amazing, and I feel very lucky. 🧀

* Most of my personal OpenZFS datasets and FreeBSD jails are named for Hololive characters, which has made homelab sysadmin work needlessly difficult. Totally worth it.

* I can't stand jeans or shorts, unless I'm swimming. And even then, jeans just get waterlogged.

* My three favourite coffee shops of all time no longer exist. But on the positive side, one of the worst ones I've ever been to is gone as well.

* Bad coffee makes me sneeze.
