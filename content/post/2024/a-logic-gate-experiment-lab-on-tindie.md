---
title: "A logic gate experiment lab on Tindie"
date: "2024-07-23T16:58:55+10:00"
thumb: "https://rubenerd.com/files/2024/logicboard@1x.jpg"
year: "2024"
category: Hardware
tag:
- indie-hardware
- logic
location: Sydney
---
I saw [this project by MH-EDU-Electronics](https://www.tindie.com/products/mh-edu/logicboard-stem-logic-gates-experimentation-lab/) appear on my feed, and it looks like fun!

> Introducing LogicBoard, the ultimate experimentation tool for anyone interested in learning about logic gates and digital electronics! With its 3 inputs, multiple logic gates, and 3 output LEDs, LogicBoard is the perfect platform to experiment with and understand the principles of digital logic.

You connect the provided DuPont wires to each of the "gate" header pairs represented by the icons, and you can see the output on the provided LEDs. The outputs can also be daisy-chained, so you could build yourself a flip-flop circuit!

<figure><p><img src="https://rubenerd.com/files/2024/logicboard@1x.jpg" alt="Photo of the compact LogicBoard showing the headers for each gate." srcset="https://rubenerd.com/files/2024/logicboard@2x.jpg 2x" style="width:500px; height:333px;" /></p></figure>

I know how all this works, and I'm *still* tempted to get one just for fun. I wish it came in a kit form as well, then you could teach basic through-hole soldering as well as how digital logic works.
