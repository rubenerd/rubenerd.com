---
title: "George Fame: I Love the Life I Live"
date: "2024-09-09T12:35:02+10:00"
thumb: "https://rubenerd.com/files/2024/yt-BL9kttZyXjA@1x.jpg"
year: "2024"
category: Media
tag:
- ben-sidran
- georgie-fame
- jazz
- music
- music-monday
location: Sydney
---
Today's *[Music Monday](https://rubenerd.com/tag/music-monday/)* introduses one of the all-time greats to the site. In the words of my beloved late uncle dave, *George Fame could swing!*

<figure><p><a target="_BLANK" href="https://www.youtube.com/watch?v=BL9kttZyXjA" title="Play Georgie Fame - I Love the Life I Live"><img src="https://rubenerd.com/files/2024/yt-BL9kttZyXjA@1x.jpg" srcset="https://rubenerd.com/files/2024/yt-BL9kttZyXjA@1x.jpg 1x, https://rubenerd.com/files/2024/yt-BL9kttZyXjA@2x.jpg 2x" alt="Play Georgie Fame - I Love the Life I Live" style="width:500px;height:281px;" /></a></p></figure>

Keen-eyed viewers may recognise the cover as being a similar style to Ben Sidran's *Cool Paradise*, released around the same Go Jazz label. They also toured together, and appeared on each others concert albums. There's something so magical about that late 80s mix of electronic synth and jazz/big band I can't get enough of.
