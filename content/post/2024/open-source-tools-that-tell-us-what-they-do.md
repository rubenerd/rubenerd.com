---
title: "Open source tools that tell us… what they do!"
date: "2024-11-20T07:42:11+11:00"
abstract: "Obtuse language is really starting to get to me. At least most open source projects don’t fall for it."
year: "2024"
category: Internet
tag:
- spam
location: Sydney
---
It's time to play another game! We haven't done one of these for a while.

I was shopping for a new laundry appliance last week&mdash;like a gentleman&mdash;when I saw this incredible public answer posted on a seller's website. Your challenge, should you wish to accept, is to determine **what the person asked** based on this answer:

> Hello Annie, We appreciate your product inquiry. The information you're seeking is readily accessible on our website, specifically listed under the `$REDACTED` section. To assist you further, I've included an excerpt from our website below. Additionally, we offer a user manual that contains valuable instructions on operating and maintaining your new machine. You can download it before your delivery to ensure a smooth experience. Country of Origin: Germany I trust that this information will prove helpful as you shop with us at `$RETAILER`. Our Sydney office operates around the clock 24/7, and you can reach us at `$NUMBER`.

I did not modify that answer in any way, save for some `$REDACTIONS`. It was one paragraph of uninterrupted text, with more than a hundred words, to answer a single question. So what was the question?

If you guessed *how long is the water intake hose*, you're correct! Though maybe not, it was about where the device was made. Still, it was incredible to me that a *one word answer* warranted that much waffle. And not the [good kind](https://en.wikipedia.org/wiki/Stroopwafel "Stroopwafel on Wikipedia") like I hope Clara and I will get to experience in The Netherlands one day.

The hollow, mouth-mealy language has the hallmarks of a chatbot, with the answer tacked in the middle without adequate punctuation. I'm seeing this more and more: someone outsources their boilerplate, then attempts to shoehorn additional information they think is necessary. Except, they don't maintain the same language or tone the chatbot is using, so it goes from being condescendingly milquetoast to stilted, then back to milquetoast. It makes sense they wouldn't put in any effort, *they're using a chatbot!* But it's still funny.

What does this have to do with open source projects though... laundering of licences using an LLM aside?

As mentioned in my recent post about [what I look for in a blog](https://rubenerd.com/what-i-look-for-in-blogs/), I've become attuned&mdash;maybe more accurately *allergic*&mdash;to corporate PR speak and descriptions when it comes to hardware, software, and services I need to do my job. More and more I'm reading that a tool doesn't "do something", it's a "disruptive, synergistic solution". The corporate world has always spoken like this, but it definitely feels like it's getting worse.

(Most) open source is *so refreshing* for not doing this! Unless they're chasing VC funding, I can go to a repo page, or view a one-line description in a package manager, and I know immediately what something is, what it does, and what it's written in. In the washing machine analogy above, I can issue a `pkgin pkg-descr` and be told *Germany* instead of *ensuring a smooth experience*.

The answer I need on these PR-laden sites is often buried there somewhere, but I have to go digging for it. If I can't tell what you, your company, or your project does after reading a wall of text, that's a problem. And honestly, I'll look elsewhere if I can.
