---
title: "An advanced case of “In Their Defence”"
date: "2024-09-18T12:49:45+10:00"
year: "2024"
category: Ethics
tag:
- safety
location: Sydney
---
A friend of mine once joked that I was great at making excuses for other people. I'm starting to realise it's not an asset.

Case in point, I was walking across a zebra crossing yesterday&mdash;like a gentleman&mdash;when a Sydney Buses driver came barreling down the street and nearly mowed me down. I instinctively leaped out of the way, and he instinctively blared his horn and threw his hands in the air out of frustration that someone dared use a pedestrian crossing while he was in the midst of drifting his bus like a Subaru.

I've talked on Mastodon and Twitter before about how Sydney Buses drivers are among the worst I've ever experienced as a pedestrian and passenger; *and I've lived in Kuala Lumpur!* There are some true gems among their staff, but they have a serious issue with bad apples. Something about the tangled web of streets in Chatswood, and the hilly areas around Earlwood, causes them to lose all sense of spacial awareness and safety. I'm shocked there aren't more fatalities given how they conduct themselves, and how fast and loose they play with road rules.

And yet, once the shock subsided, do you know what my first thoughts were? *Sydney Buses drivers are probably criminally underpaid. I'll bet they have to deal with some truly terrible passengers, unreasonable work hours, and all manner of other issues that a cushy white-collar worker like me doesn't.*

Now, a bit of empathy is fine. Few people are overtly and deliberately malicious, despite what you or I may think sometimes. I think we're all trying our best most of the time, with whatever hands we've been dealt, and within what I like to call the Three E's: ethics, education, and experience.

In this case, maybe the guy had just been threatened by a passenger. Maybe he'd just got some terrible news regarding a member of his family, or a close friend. Maybe he was bitten on the crotch by an alligator that managed to climb aboard at Wolli Creek. Who knows?

The weird thing is, that's often where my brain stops thinking about a situation or event. My instinct is to feel for the person who wronged me. I can always think of an excuse that explains an action, or absolves the actor of said action. Whenever a member of retail staff is curt with me, or a public transport driver looks cranky, or a support engineer is more blunt than I'm used to, my brain *automatically* starts thinking about power imbalances, workers' rights, and the plight of those doing it tougher than me. Because in an ideal world, we'd all be happy, fulfilled, and respected.

Here comes the proverbial posterior prognostication: *but...* that's not always good enough, is it? That bus driver may have been having the worst day ever, but that doesn't excuse *breaking the law*, or putting the lives of innocent people at risk. A line has to be drawn between coming up with excuses for people, and taking responsibility.

A person who’s been emailing me for a while has said this mental gymnastics is common among a cohort of more neurodivergent people. Specifically, it's a symptom of one's desire to be fair, which ends up going too far in the other direction. I'm not sure that's how my brain works, but it's an interesting idea.
