---
title: "Researching home lab racks"
date: "2024-08-27T08:05:08+10:00"
thumb: "https://rubenerd.com/files/2024/startech-rack.jpg"
year: "2024"
category: Hardware
tag:
- homelab
- racks
location: Sydney
---
Recently I've been looking more seriously at cases [like the SilverStone CS382](https://rubenerd.com/the-new-silverstone-cs382-homelab-case/) for Clara's and my homelab boxes. Currently our FreeBSD Supermicro board lives in an old Antec 300, which is great for airflow, but replacing drives is a pain. Our NetBSD install still lives in an HP MicroServer Gen 8, which is starting to show its age. And the less said about my poor Debian Xen cluster, and our smattering of single-board computers, the better.

This lead me down the rabbithole of thinking... what about rackmount server cases for them? And then... what about a *rack* to put them in? Before long, I had a rack diagram created in LibreOffice Calc, and was planning everything from the patch panels, to how much space a smaller UPS would take, to whether Token Ring MAUs can be mounted backwards or whether I'd need a brush panel.

But I'm getting ahead of myself. Why would someone want to bring a noisy, big box from work back home?

<figure><p><img src="https://rubenerd.com/files/2024/startech-rack.jpg" alt="A StarTech rack." style="width:420px;" /></p></figure>

There are many reasons, most of which boil down to it looking cool and generally being awesome. Yes, despite all the negative things going on in the tech space right now, I still ultimately love all the engineering and hardware that powers this stuff. A server rack at home the size of a bar fridge isn't a necessary rite of passage, but it sure makes you *feel* more like a nerd, which is important. Well okay, it isn't, but it should be.

There are practical considerations too. Homelab server racks consolidate all your comms, cables, and computers into one box. When you've got wires going all over a house, it's so much nicer having them all terminate in the one place; especially if you can also locate your cable modem and services there too.

A full 42 RU data centre rack might be ridiculous (... or, would it be?), but various "half racks" exist with sizes ranging from 18 to 27 RU. A brand new rack can go for silly money, but they pop up on the second-hand market fairly frequently for peanuts, usually during office cleanouts. Install one of these at home, put all your stuff into them, done.

But this leads us to the elephant in the room, if it spent the entire time trumpeting. Rack mount server hardware, as delivered by the big OEMs and vendors, are *extremely* loud. They're designed for high static pressure to keep the internals cool in a dense data centre setting, with little to no concern for the hapless engineers like me who had to service them. This might be fine if you can locate your homelab rack in a basement, or another room away from living areas, but apartment dwellers might not have that luxury.

The key here seems to be to go big. Larger 3 and 4 U rackable server chassis (chasii?) have physically larger fans which can draw through more air with less noise. I've been giving SilverStone a lot of free advertising here lately, but even some of their 2U boxes have support for 80 mm fans that you could source from the likes of Noctua or Arctic. Use PWM on these fans and control their power curves, and you'll negate much of the issue.

I'm a while away from entertaining having one of these things; we haven't even got the move sorted out yet! But a nerd can dream. Specifically about homelab server racks.
