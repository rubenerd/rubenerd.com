---
title: "Dan Hon’s resources for managing replies"
date: "2024-02-19T14:02:07+11:00"
year: "2024"
category: Thoughts
tag:
- comments
location: Sydney
---
His [image gallery of responses](https://www.flickr.com/photos/danhon/albums/72177720314761105/with/53527566001 "Stickers to manage replies by") are wonderful:

1. Do not reply to deny my lived experience.

2. Do not reply unless you have direct experience.

3. This is an observation.

4. Do not reply with a software suggestion.

5. Do not reply to tell me to use open source software.

6. Do not reply to teach me about capitalism or enshittification. I know.

7. Do not reply to tell me you don't have this problem.

8. Do not reply to mansplain my post back to me.

9. I'm just complaining, not asking for help.

10. Do not reply to tell me why a thing I like is bad.

The visceral, gleeful response to these stickers being shared around speaks to how common these replies have become. I know from experience that I can expect a wall of such comments whenever I appear on the Orange Peanut Gallery or similar.

I'd classify these into two groups: **lacking empathy**, and **missing context**.

A few years ago I was in an office building with a failing lift that crashed so violently into the basement, I ended up with swollen ankles. It was terrifying, and it hurt! I was assured that what I felt was impossible, because safety elevators are a thing. Why did people feel the need to post this? Was it pride that they watched the same documentary about how lifts worked?

An example of missing context is macOS. I'll post some frustration about a bug Apple hasn't fixed, and I'll receive comments that they don't have the same problem, or that I should use Linux distro X. It doesn't factor in than I *did* have the problem, and that I can't run this program for my job on desktop Linux. I've called these replies *unhelpful helpful* comments before, because the solutions people offer aren't relevant or actionable.

I generally get friendly comments, in a low-enough volume that I’m able to factor in intent when reading them. Someone telling me to try Linux when I post about Windows 3.1 isn’t being obtuse, they just haven’t read many of my other posts about retrocomputing. But I can see why they'd grate if you were receiving hundreds of them.

I'd only add one to Dan's list: *do not reply asking why I did/didn't do something.* The former is both a lack of empathy and context, and the latter is proving a negative, which is nigh impossible.
