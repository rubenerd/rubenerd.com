---
title: "Bootstrapping CP/M-86 on an IBM 5160 PC/XT"
date: "2024-06-12T15:58:29+10:00"
thumb: "https://rubenerd.com/files/2024/ibm5160@1x.jpg"
year: "2024"
category: Software
tag:
- cpm
- cpm-86
- ibm-xt
- retrocomputers
location: Sydney
---
We knew this was coming! After my [5160 XT retrospective](https://rubenerd.com/the-ibm-5160-xt/), and my post about [bootstrapping PC DOS 3.3](https://rubenerd.com/bootstrapping-dos-330-on-an-ibm-5160-pcxt/), we're now going to look at CP/M-86, the version of Gary Kildall's OS ported from the Z80 to the IBM PC. The history of how MS-DOS came to rip off and supplant the world's first successful microcomputer OS is worth its own post, but let's get it running for now.

As before, I'll be using the excellent [86Box](https://86box.net/) because I don't have an XT. Refer to that post for how I configured it to run as a 5060 XT with an MFM hard drive, and how to use the **IBM Advanced Diagnostics** disk to perform a low-level format.

<figure><p><img src="https://rubenerd.com/files/2024/ibm5160@1x.jpg" alt="Photo of an IBM 5150" srcset="https://rubenerd.com/files/2024/ibm5160@2x.jpg 2x" style="width:500px; height:333px;" /></p></figure>

*Photo by <a href="https://commons.wikimedia.org/wiki/File:IBM_PC-IMG_7271_(transparent).png">Rama &amp; Musée Bolo</a> on Wikimedia Commons.*


### Booting CP/M

CP/M was one of the three OSs IBM offered for the PC, though most people chose MS-DOS. I'm using *IBM CP/M-86 for the IBM Personal Computer Version 1.1*. This release introduced hard drive support, albeit with a 1024 logical cylinder (8 MiB) limit.

Once you've acquired a disk image, attach it to the first floppy drive of 86Box, and boot. You'll see `Loading CPM.SYS`, followed by a list of detected hardware, then the Console Command Processor (CCP) with the `A>` prompt.

<figure><p><img src="https://rubenerd.com/files/2024/xt-cpm-first@1x.png" alt="Screenshot showing CP/M first booted, with the output of the DIR command on the A drive." srcset="https://rubenerd.com/files/2024/xt-cpm-first@2x.png 2x" style="width:640px; height:200px;" /></p></figure>

This post isn't meant to be an exhaustive introduction to CP/M; I'll go into more detail in a future post. The key here is that it's showing `1` hard disk drive. If you don't see a drive here, you need to low-level format it using the **IBM Hard Disk Maintenance** tool described in my [PC DOS 3.3 post](https://rubenerd.com/bootstrapping-dos-330-on-an-ibm-5160-pcxt/), or the disk image you've attached isn't supported.


### Formatting the hard drive

Partitioning is done with the CP/M transient command `A>HDMAINT`, similar to how you'd invoke `FDISK` on DOS. *Transient* commands refer to external executables, as opposed to *built-in* commands provided by the CCP. Press `F1` to begin partitioning:

<figure><p><img src="https://rubenerd.com/files/2024/xt-cpm-hdmaint-2@1x.png" alt="Screenshot of the Main Menu of Hard Disk Maintenance" srcset="https://rubenerd.com/files/2024/xt-cpm-hdmaint-2@2x.png 2x" style="width:640px; height:200px;" /></p></figure>

Here we can see our formatted drive has no cylinders assigned, so we can press `F6` to create a new CP/M partition:

<figure><p><img src="https://rubenerd.com/files/2024/xt-cpm-hdmaint-3@1x.png" alt="Screenshot of Hard Disk Maintenance, showing 000 of 305 cylinders assigned." srcset="https://rubenerd.com/files/2024/xt-cpm-hdmaint-3@2x.png 2x" style="width:640px; height:200px;" /></p></figure>

For this exercise we're using as much of the disk as we can, so press `F10` to accept the starting cylinder of `000`.

<figure><p><img src="https://rubenerd.com/files/2024/xt-cpm-hdmaint-4@1x.png" alt="Screenshot of Hard Disk Maintenance, prompting for the starting cylinder. The default is 000." srcset="https://rubenerd.com/files/2024/xt-cpm-hdmaint-4@2x.png 2x" style="width:640px; height:200px;" /></p></figure>

We also don't want to go above `8192K` for CP/M-86, so press `F10` to accept the default partition size:

<figure><p><img src="https://rubenerd.com/files/2024/xt-cpm-hdmaint-5@1x.png" alt="Screenshot of Hard Disk Maintenance, prompting for the partition size. The default is 243, or 8192K." srcset="https://rubenerd.com/files/2024/xt-cpm-hdmaint-5@2x.png 2x" style="width:640px; height:200px;" /></p></figure>

This summary screen provides some interesting options. You can see the CP/M directory limit, and the option for writeback verification, which is neat. For this VM I left everything as standard, and pressed `F10`:

<figure><p><img src="https://rubenerd.com/files/2024/xt-cpm-hdmaint-6@1x.png" alt="Screenshot of Hard Disk Maintenance, showing 1024 directory entries of out a possible 2048, and file writeback verification." srcset="https://rubenerd.com/files/2024/xt-cpm-hdmaint-6@2x.png 2x" style="width:640px; height:200px;" /></p></figure>

Now it's time to reboot! Keep the CP/M-86 boot disk in the virtual drive, and press `F9`:

<figure><p><img src="https://rubenerd.com/files/2024/xt-cpm-hdmaint-7@1x.png" alt="Screenshot of Hard Disk Maintenance, instructing us to insert a system diskette and to reload the OS." srcset="https://rubenerd.com/files/2024/xt-cpm-hdmaint-7@2x.png 2x" style="width:640px; height:200px;" /></p></figure>


### Copying required system files

Next, we need to copy the CP/M system files to our new partition, so `HDMAINT` will recognise them and set the partition active.

This is where we need the `PIP` transient command! This lets us copy files, though it supports a bunch of different endpoints too. The syntax is famously a bit weird:

    A>PIP C:=A:*.*

Here we're copying the contents of A &rarr; C, even though it reads backwards from what we're used to. It makes more sense if you read it like a programmer coding a variable: C is being *assigned* what's in A.

<figure><p><img src="https://rubenerd.com/files/2024/cpm-xt-pip@1x.png" alt="Screenshot of CCP, showing the files copied across from the A drive to C." srcset="https://rubenerd.com/files/2024/cpm-xt-pip@2x.png 2x" style="width:640px; height:200px;" /></p></figure>


### Setting the partition active

We're almost done! Invoke `A>HDMAINT` again. Note we now have a few more options now. Press `F1` again:

<figure><p><img src="https://rubenerd.com/files/2024/cpm-xt-active-1@1x.png" alt="Screenshot of Hard Disk Maintenance Main Menu, with additional options for verifying CP/M options, and displaying/changing CP/M values." srcset="https://rubenerd.com/files/2024/cpm-xt-active-1@2x.png 2x" style="width:640px; height:200px;" /></p></figure>

We want to change our partition to be bootable, so press `F4`:

<figure><p><img src="https://rubenerd.com/files/2024/cpm-xt-active-2@1x.png" alt="Screenshot of Hard Disk Maintenance, with the option to delete, change, or create CP/M partitions." srcset="https://rubenerd.com/files/2024/cpm-xt-active-2@2x.png 2x" style="width:640px; height:200px;" /></p></figure>

Press `F2` to change the bootable partition:

<figure><p><img src="https://rubenerd.com/files/2024/cpm-xt-active-3@1x.png" alt="Screenshot of Hard Disk Maintenance, with explanation that CP/M needs CPM.SYS or CCPM.SYS in the partition before it can boot. There's an option to change the bootable partition." srcset="https://rubenerd.com/files/2024/cpm-xt-active-3@2x.png 2x" style="width:640px; height:200px;" /></p></figure>

You'll notice the word `(bootable)` now appears in the top right! Press `F10` to dismiss, then `F9` to exit.

<figure><p><img src="https://rubenerd.com/files/2024/cpm-xt-active-4@1x.png" alt="Screenshot of Hard Disk Maintenance, with the word bootable in the corner." srcset="https://rubenerd.com/files/2024/cpm-xt-active-4@2x.png 2x" style="width:640px; height:200px;" /></p></figure>


### Reboot and test

Here comes the moment of truth! Eject the CP/M-86 boot disk, and reboot the 86Box VM. If all goes well, you'll have the same CPP screen as before, but note the `C>` volume identifier indicating we've now booted onto our MFM hard drive:

<figure><p><img src="https://rubenerd.com/files/2024/cpm-xt-loaded@1x.png" alt="Screenshot of CP/M, now booted from C!" srcset="https://rubenerd.com/files/2024/cpm-xt-loaded@2x.png 2x" style="width:640px; height:200px;" /></p></figure>


### Conclusion

I weirdly like CP/M, because I'm weird, and because I have the potential of running it on my Apple //e and Commodore 128 too. I'm chuffed I have a way to run this unusual port on my current machines. The next step is to try Concurrent CP/M, and DR-DOS!
