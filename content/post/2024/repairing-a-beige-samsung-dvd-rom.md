---
title: "Repairing a Samsung DVD-Master from 2001"
date: "2024-07-25T08:08:22+10:00"
thumb: "https://rubenerd.com/files/2024/sd612-windows@1x.png"
year: "2024"
category: Hardware
tag:
- retrocomputers
location: Sydney
---
Clara and I found some [Pentium III-era parts on the kerb](https://rubenerd.com/finding-pentium-3-parts-by-the-side-of-the-road/ "Finding Pentium 3 parts by the side of the road!") earlier this month, and I'm finally getting around to [cataloguing](http://retro.rubenerd.com/parts.htm "Parts page on Ruben's Retro Corner"), cleaning, testing, and hopefully fixing what I can.

On the bench today is a *Samsung 12x DVD-Master* IDE drive, model `SD-612`. The barcode sticker says it was manufactured in Korea back in March 2001, which makes it older than many of my readers. That's terrifying, why did I have to say that.

<figure><p><img src="https://rubenerd.com/files/2024/samsung-dvd-dell@1x.jpg" alt="The DVD drive sitting atop the Dell." srcset="https://rubenerd.com/files/2024/samsung-dvd-dell@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>


### Initial test and failure

As with much of my old hardware, I decided to use my 2000 [Dell Dimension 4100](http://retro.rubenerd.com/dell4100.htm) tower to test. This has become my favourite retro test bench on account of its side panel being the easiest to open! I connected the drive to the primary internal IDE interface, set the jumper to *Cable Select*, and tried.

First signs looked good. The BIOS reported the correct device:

    SAMSUNG DVD-ROM SD-612F

Windows was also able to see the drive:

<figure><p><img src="https://rubenerd.com/files/2024/sd612-windows@1x.png" alt="Screenshot of the drive showing up in System Properties." srcset="https://rubenerd.com/files/2024/sd612-windows@2x.png 2x" style="width:640px; height:240px;" /></p></figure>

I pressed the eject button, and was happy to see the tray open and close without issue. If anything, it sounded *way* smoother than the scratchy modern 9.5mm BD burner in my FreeBSD Bhyve/jail tower. I loaded a disc, and heard some encouraging whirring sounds.

Unfortunately, Windows couldn't mount any disc I tried. The first was a DVD-RW, so I tried a pre-recorded DVD, and a few CDs. No dice; it would spin momentarily, then give up.

<figure><p><img src="https://rubenerd.com/files/2024/sd612-fail@1x.png" alt="Screenshot showing 'Please insert a disk into drive D:', despite a disc being present." srcset="https://rubenerd.com/files/2024/sd612-fail@2x.png 2x" style="width:640px; height:240px;" /></p></figure>

Just in case it was Windows, I tried both my Red Hat 6 and NetBSD 8 x86 CDs, but neither booted. Alas, this looked like a hardware issue, unless classic Red Hat was in cahoots with an orange Daemon.


### Cracking open the drive

I'm not an electrical engineer, but given the drive was literally found half-buried in dirt during a thunderstorm, I figured it couldn't hurt seeing if it was damaged inside (aren't we all sometimes)?

Like all drives of this vintage, there's a specific order for disassembly:

1. Turn the computer on and eject the tray. This lets you remove the bezel without trying to pry it open.

2. Close the drive, shut down the machine, and unplug.

3. Unscrew the bottom metal plate, exposing the drive's PCB.

4. *Gently* push the plastic tabs for the front bezel inwards, letting you pop it out. Take your time here, these are easy to snap off if you're not careful.

5. Lift the plastic drive mechanism and tray assembly out of the drive shell.

<figure><p><img src="https://rubenerd.com/files/2024/samsung-lens@1x.jpg" alt="Photo showing the lens and tray of the disassembled drive." srcset="https://rubenerd.com/files/2024/samsung-lens@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>


### Inspection and cleaning

I was surprised how clean(ish) the inside of the drive was, save for some dust and slight corrosion at the front behind the drive bezel. It didn't *look* like any major ingress of water or silt had happened, which was encouraging.

My first step with any malfunctioning magnetic drive is to clean the read/write head, so I figured I'd do the same thing here with its laser lens (pictured above). It looked fine peering under the spindle frame, but I dabbed it with a cotton swab sprayed with some IPA, and was shocked at the amount of fluff and gunk that came off! No wonder it couldn't see any disc.

Besides this, everything looked in working order. I cleaned off and added a drop of new lithium grease to the moving metal parts, and used some compressed air to remove some of the dust.


### Huzzah!

I reassembled the drive, connected it to the Dell again, booted, and tried another disc. It... worked. First time!

<figure><p><img src="https://rubenerd.com/files/2024/sd612-msts@1x.png" alt="Screenshot showing Microsoft Train Simulator loaded in the drive." srcset="https://rubenerd.com/files/2024/sd612-msts@2x.png 2x" style="width:640px; height:240px;" /></p></figure>

There you go. If you have an old drive that turns on and ejects just fine, but can't detect discs, give the laser lens a clean. I don't think I've ever had to do that for an old optical drive before.
