---
title: "25 years on from Y2K"
date: "2024-12-29T21:37:20+11:00"
abstract: "It was fine. But it nearly wasn’t. And most retrospectives don’t get that."
thumb: "https://rubenerd.com/files/uploads/screenie.qemu.powermenu.11111.png"
year: "2024"
category: Software
tag:
- history
- y2k
location: Sydney
---
I've been reading retrospectives talking about how Y2K happened *a quarter of a century* ago. Denoting periods of time by fractions of another period of time always seemed odd to me, especially when it's clearly designed to render something more impressive than it actually is!

That's a bit of a theme when it comes to Y2K, though not for the reasons the pundits thought.

Let's start with some context. It's hard not to overemphasise just how dire the situation felt towards the end of the 1990s. Our global, interconnected society had come to rely on electronic computers to an extent never before seen in human history. We'd all had a taste of what IT outages meant leading up to 2000, affecting everything from booking systems to supermarket checkouts. The idea of extrapolating that out to *everything*&hellip; it rang alarm bells. And leading the charge ringing said bells were two groups of people.

First, you had **IT professionals** warning that the issue had to be taken seriously. The public&mdash;and most critically, the bean counters&mdash;weren't going to care that years were stored as two integers to save processing, memory, and storage capacity. People had to be made aware of the potential for issues so fixes and testing could be prioritised and performed. And even then, the potential for undefined behavior when millions of disparate systems flipped over and interacted meant we couldn't be sure we were out of the woods until the clocks rolled over.

<figure><p><img src="https://rubenerd.com/files/uploads/screenie.qemu.powermenu.11111.png" alt="Screenshot of Brown Bag Software PowerMenu, showing the date as the year 111" style="height:220px; width:500px; image-rendering: pixelated;" /></p></figure>

The aforementioned retrospectives regularly miss this fact. Our fears at 11:59 were still well founded, even in spite of our best collective efforts. Simulations were encouraging, but just that.

Some of these professionals&mdash;or at least, a disingenuous subset who were cosplaying as such&mdash;may have also been&hellip; enthusiastic with how this was pitched. There were plenty of dubious solutions offered, from placebo software packages to overpriced consultations. Of course the chance to make a quick buck couldn't be squandered once the public was nervous. It's the American way! Well, Singapore in this case, but it was American software.

I was only a kid at the time, but I helped our school's IT team inspect and validate every machine on campus, and we quickly determined at least two packages that had been purchased&mdash;at great expense&mdash;were snake oil. A family friend asked me to help at his office too, and I was *shocked* at what was being used in a building with hundreds of staff. Stories for another time, perhaps.

Oh right, we were talking about people ringing bells. The second group somehow got themselves into a lather that far surpassed anyone else: **the press**. I can still remember the TV news reports dramatising exploding nuclear reactors, planes dropping from the sky, and entire countries going dark. It was frankly embarrassing and irresponsible journalism, and a harbinger of how the next chapter was to be reported. It was my first ever experience with [Knoll's Law of Media Accuracy](https://rubenerd.com/erwin-knolls-law-of-media-accuracy/ "Media accuracy on topics with which you’re familiar"), though I didn't know it at the time.

The reactions afterwards were equally telling. Most of us in IT heaved a sigh of relief when Armageddon was successfully averted; at least, for the most part. There were spot fires which, again, retrospectives tend to gloss over. The IT teams at some businesses hadn't got through to management in time, and others had coasted on legacy software without knowing they were vulnerable. It would take years in some cases for problems to manifest. Either way, it's difficult to get communities of people to agree to do something, let alone the planet, so we were all rather chuffed.

The press, on the other hand, had a field day. News reports spread as quickly as those images of dropping planes. The risk posed by Y2K had been overblown, nothing had happened, those boffins warning us of gloom and doom were charlatans, and so on. It wasn't down to us doing our jobs as an industry, it was that we were full of crap. Then the world moved on.

**Y2K! It was fine. But it nearly wasn't.**

I wonder how we'll handle 2038?
