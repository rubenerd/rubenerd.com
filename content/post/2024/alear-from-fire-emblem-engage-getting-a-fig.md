---
title: "Alear from Fire Emblem Engage getting a fig"
date: "2024-02-13T23:01:54+11:00"
year: "2024"
category: Anime
tag:
- fire-emblem
- mika-pikazo
location: Sydney
---
From our favourite artist [Mika Pikazo herself](https://x.com/MikaPikaZo/status/1752260914299216299)\:

> Alear will be a figure from "Fire Emblem Engage". I drew her congratulatory illustration and comments, the reproduction and power is amazing...!

I posted about [her incredible game art last year](https://rubenerd.com/mika-pikazos-fire-emblem-engage-designs/ "Mika Pikazo’s Fire Emblem Engage designs"). I think the sculptors did an amazing job, right down to her billowing hear and the detail on her sleeves.

It's a dangerous time for someone trying to get rid of stuff.

<figure><p><img src="https://rubenerd.com/files/2024/alear-fig@1x.jpg" alt="" srcset="https://rubenerd.com/files/2024/alear-fig@2x.jpg 2x" style="width:500px;" /><br /><img src="https://rubenerd.com/files/2024/GFFFhlVbgAAntgQ@1x.jpg" alt="" srcset="https://rubenerd.com/files/2024/GFFFhlVbgAAntgQ@2x.jpg 2x" style="width:500px;" /></p></figure>
