---
title: "The gorgeous Streamline Hotel"
date: "2024-12-16T09:40:34+11:00"
abstract: "The colors, the Art Deco font, the rounded shapes, aaaa!"
thumb: "https://rubenerd.com/files/2024/streamline-hotel-Dough4872@2x.jpg"
year: "2024"
category: Thoughts
tag:
- architecture
- colour
- united-states
location: Sydney
---
<figure><p><img src="https://rubenerd.com/files/2024/streamline-hotel-Dough4872@1x.jpg" alt="Photo of the Daytona Hotel, with teal/green accents, round windows, and a stunning Art Deco font" srcset="https://rubenerd.com/files/2024/streamline-hotel-Dough4872@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

Photo by [Dough4872 on Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Streamline_Hotel_July_2018.jpg). This is a hotel on Daytona Beach in Florida in the US, and I absolutely love everything about it. The colors, the Art Deco font, the rounded shapes. I might have my next desktop wallpaper for [after the holidays](https://rubenerd.com/an-xmas-wallpaper-for-2024/).
