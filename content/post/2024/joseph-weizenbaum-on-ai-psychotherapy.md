---
title: "Joseph Weizenbaum on AI psychotherapy"
date: "2024-04-24T20:17:45+10:00"
year: "2024"
category: Ethics
tag:
- ai
- mental-health
- psychology
location: Sydney
---
The inventor of ELIZA [didn't mince words in 1982](https://www.bostonreview.net/articles/computer-science-and-civil-courage/ "Article in the Boston Review: Computer Science and Civil Courage"), and they still hold true in the current hype cycle:

> I would put all projects that propose to substitute a computer system for a human function that involves interpersonal respect, understanding, and love in the same category. I therefore reject Dr. Kenneth Mark Colby’s proposal that computers be installed as psychotherapists, not on the grounds that such a project might be technically infeasible, but on the grounds that it is immoral.

Also reminds me of [that IBM slide from 1979](https://twitter.com/MIT_CSAIL/status/1484933879710371846)\:

> A computer can never be held accountable. Therefore a computer must never make a management decision.
