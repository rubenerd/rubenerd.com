---
title: "Wandering around the Melbourne Botanic Gardens"
date: "2024-03-27T09:28:40+11:00"
thumb: "https://rubenerd.com/files/2024/melbourne-botanic-flower@1x.jpg"
year: "2024"
category: Travel
tag:
- gardens
- nature
location: Melbourne
---
The Melbourne Botanic Gardens are Melbourne's Botanic Gardens just south of the Melbourne CBD, hence the name. Sometimes I worry the stuff I write here is *too* clever... but then I raise something completely pointless like this.

<figure><p><img src="https://rubenerd.com/files/2024/melbourne-botanic-pond@1x.jpg" alt="A view out at one of the ponds." srcset="https://rubenerd.com/files/2024/melbourne-botanic-pond@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

Clara hadn't been before, and I'd never seen it late in the afternoon; it's such a different vibe wandering around as the sun was setting. A selection of her Prince Cats also made the trip, so we could try matching their fur with some pretty specimans.

<figure><p><img src="https://rubenerd.com/files/2024/melbourne-botanic-cats@1x.jpg" alt="An assortment of Prince Cats with a fetching pink flower" srcset="https://rubenerd.com/files/2024/melbourne-botanic-cats@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

My favourite area in the Melbourne Botanic Gardens that are Botanic Gardens in Melbourne remains the Fern Gulley, with its meandering little path surrounded by trees. I came here myself last year on a business trip, and found it a welcome respite. We found it so relaxing, I forgot to take a photo!

<figure><p><img src="https://rubenerd.com/files/2024/melbourne-botanic-flower@1x.jpg" alt="I love taking pictures of light through flowers." srcset="https://rubenerd.com/files/2024/melbourne-botanic-flower@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

I liked this shot of an older couple looking out at Melbourne from their bench, complete with lens flare! How this view must have changed over the years. Maybe that was something they were talking about.

<figure><p><img src="https://rubenerd.com/files/2024/melbourne-botanic-1@1x.jpg" alt="A view out at one of the open areas, with the CBD in the background." srcset="https://rubenerd.com/files/2024/melbourne-botanic-1@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

The Singapore Botanic Gardens remain my favourite, but Melbourne's comes pretty close too. Another lovely way to spend an afternoon.
