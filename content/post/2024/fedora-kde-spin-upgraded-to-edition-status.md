---
title: "Fedora KDE spin upgraded to Edition status"
date: "2024-11-11T10:11:42+11:00"
year: "2024"
category: Software
tag:
- fedora
- kde
- linux
location: Sydney
---
This is great news, via the [Fedora Pagure](https://pagure.io/Fedora-Council/tickets/issue/504 "Fedora Council: Request to upgrade Fedora KDE Desktop Spin to Edition status under the Personal Systems WG")\:

> As discussed at Flock, the Fedora KDE SIG and the newly forming Fedora Personal Systems Working Group that will oversee the SIG are requesting that the Fedora KDE Plasma Desktop spin be upgraded to Edition status for Fedora Linux 42.
>
> **amoloney:** This request can now be considered APPROVED (+9, 0, 0).

Workstation is the most polished and well-tested release of Fedora, which includes the Gnome desktop. Fedora offer spins with various alternative desktops, such as KDE and Xfce. The teams have done a great job making these work, though they've sometimes had a few rough edges that have made them difficult to recommend to people who want a "just works" distribution.

Starting with version 42, KDE will now be offered as a Desktop alongside the Gnome-based distribution. In my opinion, KDE offers the most complete, functional \*nix desktop in 2024, especially if you're coming from macOS or Windows.

I've [been running Fedora](https://rubenerd.com/tag/fedora/ "View all posts tagged with Fedora") as my primary Linux desktop since version 11 in 2009. When I fire up Linux to run a specific program or game that doesn't run on the BSDs, I just want something that works. I don't talk about it often because it fits this bill without fuss. Well, [codecs aside](https://rpmfusion.org/Howto/Multimedia "RPM Fusion repo's Multimedia FAQs")!

I'm looking forward to running a KDE Workstation release in Fedora 42.
