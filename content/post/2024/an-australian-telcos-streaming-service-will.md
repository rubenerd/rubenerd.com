---
title: "An Australian telco’s streaming service will..."
date: "2024-05-19T11:30:34+10:00"
year: "2024"
category: Media
tag:
- business
- streaming
location: Sydney
---
*I'm gonna stop you right there*. Wow, that was a blast from the mid-2010s, and I haven't even played any of the Fallout games. It's always three reasons for me, so here's why I didn't finish the title of this post:

1. I'll bet you already know what comes next, despite having little to no context about the story whatsoever. If *that* doesn't tell you the state of modern distribution, I don't know what would.

2. It's a way to ask a hypothetical first, which will set the stage for the subsequent lines of the post.

3. It's been a bit of a theme here of late.

Here's the hypothetical!

Say you're a customer of a shiny new streaming service that offers you a catalogue of music, movies, or television programming for a flat monthly fee. Say they also offer certain media you can pay for, which adds it to your personal catalogue you can listen to or watch whenever you want. To distinguish them from your streaming material, they may use terms like "buy" to refer to the transaction.

Do you *own* any of what you buy here?

My ramblings and topics tend to appeal to technically-minded people, so I'll bet you all just shouted **no** at you screens. Which, sadly, is true. You don't "own" it in a whole host of ways, but these two are the most important:

* You don't own the **copyright**. That's been the case since people first bought Edison cylinders. You instead have been granted permission to play it.

* You don't own any **files**. What you have is a licence and/or permission to play a file through a proprietary platform or web portal. You can't *download* the file, at least officially.

The issue with point two is clear. You are forever tied to the whims and commercial viability of the streaming service. If they lose the right to distribute something, or go out of business, you're up a certain creek without a paddle... or the media you "bought". It's made the more ridiculous when you consider music companies weren't breaking into people's homes to revoke their LPs or CDs. Welp, I shouldn't give them ideas!

This goes beyond digital restrictions management (DRM) previously used to restrict the playback of material under dubious ethical and legal terms. Streaming services restrict, obfuscate, or abstract the file itself, meaning you have little to no recourse when they're intentionally revoked, or otherwise made unavailable. It sucks.

I've talked about this before in the case of [anime streaming services](https://rubenerd.com/crunchyroll-deletes-funimation-digital-copies/), but it's a far wider issue. Josh Taylor wrote about Australian telco Telstra, and their plan to [shutter their streaming service](https://www.theguardian.com/media/article/2024/may/14/my-whole-library-is-wiped-out-what-it-means-to-own-movies-and-tv-in-the-age-of-streaming-services)\: 

> What rights do you have to the digital movies, TV shows and music you buy online?
>
> That question was on the minds of Telstra TV Box Office customers this month after the company announced it would shut down the service in June. Customers were told that unless they moved over to another service, Fetch, they would no longer be able to access the films and TV shows they had bought.

Bluntly, technical people weren't the target market for this service; people my dad's age who lost access to analogue TV transmissions were. But that's not the point: these services used language like "buy" specifically to imply a level of ownership that, at *best*, was misleading.

It's almost the perfect racket. Get non-technical people to pay extra for permanent access to a file&mdash;which is what terms like "buy" suggest&mdash;when you have no intention or capacity to honour it. Then when you lose interest in your business, or if it's no longer viable, cut people off and have them "buy" it again somewhere else.

Telstra at least offered a way to transfer material somewhere else. But how long will *they* be around for? And what if you don't want to give money to the parent company of that other service?

I do empathise with the idea of *caveat emptor* in this case. Enough streaming services have reneged on their commitments that it should be public knowledge not to trust anything "bought" on them. But even if this were the case, these services still mislead their customers into buying things under false pretences.

It's another example of how modern digital distribution is built to serve platforms, not people. Which is... I was trying to think of a negative adjective that begins with **p** for some alliteration, but I could only think of blue things. Precarious? Pernicious? Putrefactive?
