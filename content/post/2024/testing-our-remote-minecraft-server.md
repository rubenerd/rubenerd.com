---
title: "Testing our remote FreeBSD Minecraft server"
date: "2024-03-31T09:20:50+11:00"
year: "2024"
category: Software
tag:
- bsd
- freebsd
- minecraft
location: Melbourne
---
The last few times Clara and I have been on trips I've copied our Minecraft server to one of our laptops, and shared the address with the other machine. It works, but we always run tight on memory. I also need excuses to deploy and tinker with new things like... someone who doesn't need excuses to deploy and tinker with new things. 

<figure><p><img src="https://rubenerd.com/files/2024/minecraft-remote@1x.jpg" alt="" srcset="https://rubenerd.com/files/2024/minecraft-remote@2x.jpg 2x" style="width:500px;" /></p></figure>

So we decided to run a remote box instead this time. The stack is basically:

1. A [FreeBSD](https://www.freebsd.org/) VM on [OrionVM](https://www.orionvm.com/) (where I work), though any provider that offers a current template would work. This would be my first *production* version 14 box!

2. A [Wireguard setup](https://klarasystems.com/articles/simple-and-secure-vpn-in-freebsd/) for the laptops to connect to. I don't have strong preferences either way for VPN configuration; it was more a test to see how it worked. First impressions are pretty good, but I'll need to test it further to write about it.

3. A jail for the MariaDB backend for [Dynmap](https://github.com/webbukkit/dynmap) assets. You don't need this unless you're running the Dynmap plugin, and have a big enough map to justify going from static file storage. I think almost anyone can benefit from a proper database.

4. A jail for [OpenJDK](https://www.freshports.org/java/openjdk17-jre/ "OpenJDK 17 runtime on Freshports") and the Minecraft server itself. We use the excellent optimised [Paper](https://papermc.io/) server, [MyWorlds](https://www.spigotmc.org/resources/myworlds.39594/) to import and create portals between multiple worlds, and [Sodium](https://www.curseforge.com/minecraft/mc-mods/sodium) to optimise our local clients.

5. Phones to tether to.

Despite all the additional layers, and the remote server, this worked shockingly well! We sat at a [Japanese coffee shop](https://miyamamelbourne.com.au/ "Miyama Melbourne") yesterday afternoon to relax after a long day, and again [this morning](https://www.mercurewelcome.com.au/dining/soul-cafe "Soul Cafe"), and it ran without a hitch.
