---
title: "The SPAOE Z80 Apple II card"
date: "2024-05-28T08:07:53+10:00"
year: "2024"
category: Hardware
tag:
- apple-ii
- apple-iie
- cpm
- retrocomputing
- z80
location: Sydney
---
Ever since I [found out](https://rubenerd.com/z8o-cards-for-the-apple-ii/ "Z80 cards for the Apple //e") you could get a Z80 card for the Apple II, I've been researching every option I can find. They broadly come in two flavours: the Microsoft Softcard which shares the host Apple II's memory, the PCPI AppliCard which acts like a single-board computer... and more clones of each than you can throw a CP/M disk at.

The **SPAOE-Z80** is the one that's intrugied me the most. It's by far one of the more common Softcard clones you see on second-hand sites, yet there's scant information about it. eBay listings regularly misspell the name, or omit it entirely in lieu of calling it a *Z80 Card* or similar. There's an inconveniently-placed trace that sits underneath the sinkscreened name that either obsscures the **O** or **E**, which only adds to the confusion.

[MG's Apple II Site](http://apple2.guidero.us/doku.php/mg_notes/cpm/cpm_combos "Apple II CP/M Hardware and OSes") lists it as a "Generic Softcard clone", and links to a photo on the [Apple II Documentation Project](http://mirrors.apple2.org.za/Apple%20II%20Documentation%20Project/Interface%20Cards/Z80%20Cards/SPAOE%20Z80/)\:

<figure><p><img src="https://rubenerd.com/files/2024/spaoe@1x.jpg" alt="Photo of the SPAOE Z80 card." srcset="https://rubenerd.com/files/2024/spaoe@2x.jpg 2x" style="width:500px; height:223px;" /></p></figure>

It *looks* like a generic Microsoft Softcard clone, but is it? This [old newsgroup thread](https://groups.google.com/g/comp.sys.apple2/c/0LAGlnp11zg "MicroDrive/Turbo and Z80 CP/M card") from 2019 started by Bobbi hints that it is:

> The CP/M card is a from SPAOE, whoever they were! I think it is pretty much
a generic clone of the Microsoft Softcard. [...] Incidentally, my SPAOE Z80 Card looks identical in layout to the Pineapple one Anthony posted a pic of yesterday. Looks to be a complete clone of the real Softcard.
>
> I got DATASOFT CP/M 2.23B to boot (63K TPA)

Bobbi also [opened an issue](https://github.com/ProDOS-8/ProDOS8-Testing/issues/14 "ProDOS 2.5.0a3 and Apple2Pi v1.6 - No PiDrive!") on the **ProDOS8-Testing** repo with the card around the same time. [Olivier Dauby also mentions](https://forum.vcfed.org/index.php?threads/can-you-identify-this-card-i-found-in-an-apple-iie.80384/ "Can you identify this card I found in an Apple IIe ?") finding a SPAOE-Z80 card in their Apple //e, but only in the context of identifying an unrelated disk controller.

The photo above shows a first-party Zilog Z80A, though eBay archive site WorthPoint shows an [old listing](https://www.worthpoint.com/worthopedia/vintage-sharp-spaoe-z80-cpu-soft-card-4631464035 "Vintage Sharp Spaoe Z80 CPU Soft Card Apple Computer Accessory Ram Card") with Sharp silicon instead. I suspect swapping out Z80 suppliers was fairly common.

That's about all the information there is about this card. Archive.org [lists nothing](https://archive.org/search?query=SPAOE), and web searches only return space agencies. I suppose clone cards weren't unique enough to justify coverage; probably by design.
