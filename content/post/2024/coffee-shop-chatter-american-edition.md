---
title: "Coffee shop chatter, American edition"
date: "2024-04-29T07:07:58+10:00"
year: "2024"
category: Thoughts
tag:
- coffee-shop-chatter
- new-york
- ukraine
location: Sydney
---
There are some Americans at this Sydney coffee shop next to me.

* I order large here, because their coffee is small.

* Congress voted for Ukraine aid! Fucking *finally.*

* We've lived here for two years, and I'm still not used to their bacon. It's really good, but it's not bacon?

* No, I couldn't get Taylor Swift tickets down here either.

* Fancy seeing you here! This ain't Flushing, New York!

I'm thinking of getting that last one printed on a shirt.
