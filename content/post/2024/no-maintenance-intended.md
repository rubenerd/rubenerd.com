---
title: "No Maintenance Intended"
date: "2024-11-24T08:00:11+11:00"
abstract: "A new repo you can link to from your open source project"
year: "2024"
category: Software
tag:
- open-source
location: Sydney
---
I love that this exists. [By Potch](https://unmaintained.tech/ "Unmaintained Tech")\:

> If you’re here, that likely means a project linked you here.
> 
> Thanks so much for being interested in that project!
> 
> Open Source is rewarding- but it can also be exhausting.
> 
> The linking project’s code is provided as-is, and is not actively maintained.
> 
> The author(s) of that project invite you to peruse their code and even use it in your next project, provided you follow the included license!
> 
> No guarantee of support for the code is provided, and there is no promise that pull requests will be reviewed or merged.
> 
> It’s open source, so forking is allowed; just be sure to give credit where it’s due! 
