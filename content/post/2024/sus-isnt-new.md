---
title: "“Sus” isn’t new!"
date: "2024-02-29T08:23:58+11:00"
year: "2024"
category: Thoughts
tag:
- australia
- language
- united-kingdom
- united-states
location: Sydney
---
America's *Today Show* put together an article last month [about the word *sus*](https://www.today.com/parents/teens/sus-meaning-slang-rcna134122 "What is ‘sus’? Decoding the latest slang word"). It's taken on a new life since the advent of *Among Us*, a brilliantly fun game that's also popular with streamers and vtubers.

The article gets into the latest generations' use of the word:

> It was the No. 1 slang word used by teens in 2023, according to a survey of more than 600 parents by the language learning platform Preply. In the survey, 62% of parents said "sus" is the most common word they hear from their teens and 65% of all parents surveyed said they understand what it actually means.

And what does it mean?

> "Sus" is short for "suspicious," according to Urban Dictionary, and it represents a distrust of something. "Sus" as a noun also means "suspect" and is "usually used to define someone or something that looks suspicious or untrustworthy," says the website.

That's pretty close. Those of us in Australia and the UK have used it for decades to describe something being a bit *off*. Do they acknowledge this earlier, well established use of the term?

> Adam Cooper, a teaching professor of linguistics at Northeastern University, held a discussion about the word “sus” in his history of English class in 2023, after inviting students to conduct mini-research projects on words of interest. Although Cooper wasn't aware of "sus" at the time, he was struck by its familiarity to students. 

I know it's light reading for a breakfast show, but I'm surprised that's as far as it goes. They could have included a stock image of an English person with a Union Jack top hat drinking tea as he observes a *sus* kangaroo with shifty eyes. *G'day mate, innit!?* Breakfast TV viewers love that stuff.

I guess it's the same as Americans rediscovering the term *fortnight*, made popular by a game that misspelled it. It's been part of Commonwealth lexicon for as long as we've been keeping redundant letters in our *favourite* words, and thinking kettles are essential kitchen equipment.

English is fascinating because of how close its two largest branches have stayed mutually intelligable despite hundreds of years of history and entire oceans separating us. But sometimes little differences like this crop up. I'd say it's *gotten* interesting, but that's not a word in our branch.
