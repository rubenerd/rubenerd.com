---
title: "The scale difference between prod and homelabs"
date: "2024-10-05T09:06:49+10:00"
year: "2024"
category: Hardware
tag:
- homelab
location: Sydney
---
It's fascinating to me how scale affects perceptions.

At work we regularly buy dozens of 2U servers, loaded to the gills with DDR5 memory, dual CPUs with more cores than all the computers in my home combined, and terabytes of SSD storage. When a client asks for a hybrid cloud, or a private bare metal fleet for their own hypervisor, I spec out similar machines and provide quotes.

Occasionally I have moments of introspection, normally after we've received a shipment and I'm standing in the data centre surrounded by cardboard boxes, pallets of servers, and kilometres of power, Ethernet, and InfiniBand cables splayed out across all the tables. At that point I think *huh, that's a lot*. But then we get right back to work.

By comparison, is a phrase with two words. For Clara's and my homelab upgrade, we'll pour over specification sheets, individual drives, and look to second-hand markets for deals for months. We always buy previous-generation kit so we're not beta testers, but also to save money.

And boy does everything change at that scale! For example:

* I can't justify the price of hot spares like we have at work, so I'll spend more time digging into exactly what will get us the best balance of performance, cost, and data integrity (ala OpenZFS).

* I'll check if it's easily upgradeable and expandable, and how much I can get away with deferring.

* Certain considerations like noise aren't a factor in data centre kit, but definitely at home. Sure this 4U chassis doesn't hold anything more than this 2U Supermicro box, but the Noctua fans in the former are far preferable sitting next to me!

* We know exactly what each server in a fleet will be running for its operational life at work, but at home I hedge my bets by sourcing components with the widest compatibility possible. Could it boot FreeBSD? Would this 10G card work with NetBSD? What about if Debian or Alpine ends up on it?

* I'll forgo niceties like backplanes and drive sleds if it means I can put more money into the drives themselves (then cross my fingers I don't have to open up the machine to remove a failed drive that often).

It's obvious why; this stuff gets expensive *fast*. But it's still funny how my brain compartmentalises these sorts of things.
