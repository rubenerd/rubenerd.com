---
title: "Diederick discusses tape storage and cartridges"
date: "2024-02-11T11:18:55+11:00"
year: "2024"
category: Hardware
tag:
- cassettes
- retrocomputing
- storage
location: Sydney
---
Diedrick responded to my post about [audio cassettes](https://rubenerd.com/is-it-worth-collecting-cassettes-in-2024/ "Is it worth collecting cassettes in 2024?") with a [story of his own adventures with these carts](https://thefoggiest.dev/2024/01/30/cassette-tapes-rightly-obsolete/ "Cassette tapes: rightly obsolete"), this time in retrocomputers.

> When the cassette player was replaced with a dedicated data recorder the situation improved somewhat, but fact of the matter is tapes were slow (almost 5 minutes to load a full game of 48K) and they couldn’t seek, which meant that for every recording of software or data file we had to write down the position, indicated by a counter that we had to reset after fully rewinding the tape. Forgetting that cost more time.

I've played audio into my Apple //e and Commodore machines for the thrill of doing something new and unusual to me, but I can absolutely see where this would become tedious and painful quickly. Tapes as a storage medium were clearly a stopgap until disk drives could become more affordable. I agree with him, best to skip the tapes for anything other than idle curiosity.

He also raises cartridges, and the ephemeral state of modern software:

> The thing is, apart from the worst type of data storage in the history of consumer computing, these MSX machines also used the absolute most convenient way of software storage, to this day: the game cartridge. Just put it in a slot, turn on the computer and play. That’s it. As far as I’m aware, no console today can do that. Neither can Steam. Even better, my kids have a lot of fun with those cartridges, the very same I enjoyed forty years ago. No console today will be able to do that, forty years from now, simply because their content is online and will no longer be available.

I do count my lucky stars that I grew up in a time where the games I ran came on DOS disks. There are probably entire classes of mobile and web games now that today's kids will never be able to experience again when they're older. That's kinda sad.

As for carts being a convenient mechanism, I think he's hit the nail on the head. Aside from also being something tangible and more permanent, they're also fun! It's why I *love* that the [Foenix F256K](https://c256foenix.com/) has such a socket. When finances make it possible, I can't wait to get my hands on one.
