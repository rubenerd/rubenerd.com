---
title: "The Drop CSTM80 barebones board"
date: "2024-03-02T17:48:48+11:00"
year: "2024"
category: Hardware
tag:
- keyboards
location: Sydney
---
After Clara and I built our entry-level NovelKeys keyboards, I've been in the market for another tenkeyless design for work. A colleague linked me to a Drop (ne. Massdrop) unit called the [CSTM80](https://drop.com/buy/drop-cstm80-barebones-mechanical-keyboard/)\:

<figure><p><img src="https://rubenerd.com/files/2024/drop-keyboard@1x.jpg" alt="Exploded view of the CSTM80, showing the various components you can configure and add" srcset="https://rubenerd.com/files/2024/drop-keyboard@2x.jpg 2x" style="width:500px; height:334px;" /></p></figure>

It does have RGB like the NovelKeys boards, which is added complexity I'll never use. But the layout is exactly what I'm after, and I love that you can get replacement colour bezels for it to match whatever caps you end up with.

I don't have the budget for a build for a while, but I'm already researching what switches and caps to use with it (cough). I love how smooth my [Gateron North Pole 2.0s](https://rubenerd.com/typing-on-my-new-gateron-north-pole-2-keyboard/ "Typing on my new Gateron North Pole 2 keyboard") are, but Clara's Gateron Milky Yellows have a crispness to them I like, and are significantly cheaper. Maybe I need something in-between.
