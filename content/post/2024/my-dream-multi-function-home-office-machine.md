---
title: "My dream multi-function home office machine"
date: "2024-11-16T15:43:24+11:00"
year: "2024"
category: Hardware
tag:
- coffee
- netbsd
- printers
- scanners
location: Sydney
---
We've all seen those multi-function devices people use in small offices and when working from home. They've been invaluable to me over the years, but they've always lacked a certain *Je ne sais quoi*.

This would be my dream device:

* **Monochrome laser printer**, for expediency and legibility for things like tickets and reservation forms.

* **Colour inkjet printer**, for when we want to print and frame photos on semi-gloss paper.

* **Label printer**, for printing labels. Great work on that description Ruben, you're really selling it. The first label printed would always be for the device itself.

* **Separate paper storage**, for A4 (the standard) and Japanese B5 (the best paper size), in both plain and semi-gloss.

* **Document feeder scanner**, with a built-in guillotine for slicing the spine off books.

* **Flatbed scanner**, for those oddly-shaped scannables that won't fit in the document feeder. "Scannables"?

* **Document copier**, for scanning and printing in one step, with automatic monochrome laser or colour inkjet selection depending on the type of media being copied.

* **Fax machine**, for sending and receiving important correspondence to government departments in 2024, which can also import/export from PDF.

* **Built-in VoIP handset**, for use with the aforementioned fax machine, and for those forms that expect a different number to your mobile in 2024 for reasons nobody can explain to me.

* **Cross-cut document shredder**, with support for automatic document shredding after scanning if one is feeling adventurous.

* **Conical steel burr coffee grinder**, to deliver fresh grounds to the brewing chamber, and a timer for automatic morning brews. This could use the same motor as the aforementioned shredder if sufficient torque is available.

* **Coffee and tea brewing chamber**, with large shower-head water dispersion for even extractions, compatibility with Hario V60 filter papers, and a chute for expired coffee and tea leaves. This could [also talk HTCPCP](https://en.wikipedia.org/wiki/Hyper_Text_Coffee_Pot_Control_Protocol), and implement [CUPS](https://en.wikipedia.org/wiki/CUPS).

* **Automatic clothing press**, for those difficult mornings where you have to commute to the office in a collared shirt instead of working from home in an AsiaBSDCon 2019 shirt. Between this, the automatic coffee, and the document feeder for those papers you needed to present, your morning would be set.

* **Built-in toaster**, that could also double as a CD/DVD/BD burner for exporting and sharing scanned documents and/or received faxes. Clara suggested this addition, so the machine could also operate NetBSD as its firmware. This is why Clara is the best.
