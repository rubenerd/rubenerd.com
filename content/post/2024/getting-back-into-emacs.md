---
title: "Getting back into Emacs"
date: "2024-01-19T08:36:08+11:00"
year: "2024"
category: Software
tag:
- editors
- emacs
- vim
location: Sydney
---
My post about colour schemes mentioned that I'm writing in Vim and Emacs concurrently again. It's even worse than that; I still also use the excellent Kate editor from the KDE project.

A few of you messaged happy to see Emacs specifically being called out, though asked how I'm running both at the same time. Jonathan also mentioned I could use Emacs's Evil mode as a bridge if I wanted Vim keybindings.

I've talked before how I'm most familiar with Vim, for all the same reasons a lot of people are. I started using it in high school when it was the only editor available on remote machines, then again at university. I have muscle memory associated with a lot of its commands, though I wouldn't call myself an expert. I know even less about nvi, though I use it to edit config files on FreeBSD and NetBSD.

Meanwhile, I'll admit I *like* Emacs and microemacs. I like its inline help, command tab completion, built-in package manager, all the superfluous goodies, the fact it's mode-less, and elisp. Vim can do a lot of this, but Emacs feels more natural? It's hard to describe. But I'm nowhere near as proficient.

Interestingly, I don't find myself accidentally typing commands for one editor into the other. My brain doesn't reach for my remapped Meta key in Vim, just as I don't reach for my remapped ESC in Emacs. What my brain can't do is remember how to perform certain things. Need to get to the end of the line? That's `ESC+$`, obviously! But what is it in Emacs?

I probably should jump ship entirely and write only in Emacs to force myself to learn.
