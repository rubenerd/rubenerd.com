---
title: "Taking the XPT from Sydney to Melbourne"
date: "2024-03-25T08:04:42+11:00"
thumb: "https://rubenerd.com/files/2024/xpt-front-nerd@1x.jpg"
year: "2024"
category: Travel
tag:
- australia
- colour
- landscapes
- melbourne
- sydney
- trains
location: Melbourne
---
Clara and I are in Melbourne for leave and a few other things, so we decided to do something a bit different and take one of Australia's remaining intercity passenger services, the XPT. Trains have lower carbon emissions, and it would be a way to see some of the Australian landscape we normally fly over, or never see in our urban jungle.

We arrived at Sydney Central Station at the crack of dawn, and checked in our luggage on platform 1. Central has dozens of platforms, so they reserve this one for trains that are a bit more special. Naturally, Clara got a photo of me doing my usual awkward pose:

<figure><p><img src="https://rubenerd.com/files/2024/xpt-front-nerd@1x.jpg" alt="Photo of an awkward person standing alongside an XPT power car in Sydney Central" srcset="https://rubenerd.com/files/2024/xpt-front-nerd@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

My English readers may immediately recognise the streamlined diesel-electric locos as being modelled on the [InterCity 125](https://en.wikipedia.org/wiki/InterCity_125 "InterCity 125 article on Wikipedia"), albeit modified for Australian conditions. The trains were introduced from 1982, and work in a push/pull configuration with 5 carriages in between.

We took our assigned seats, and left the platform on the dot at 07:42 for our 11 hour, 720-ish kilometre (447 mile) journey to Melbourne. This is about the distance from Kyiv to Kraków, Tōkyō to Hiroshima, or just short of Boston to Richmond. Though as we were about to remember, Australia is substantially emptier!

<figure><p><img src="https://rubenerd.com/files/2024/xpt-seats@1x.jpg" alt="Photo showing the 2-2 seating layout of the first-class carriage." srcset="https://rubenerd.com/files/2024/xpt-seats@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

The XPT offers three levels of service. You can book a private sleeper cabin similar to Amtrak, though they were fully booked for reasons we'll get back to. There's also economy seating, and first-class seating that isn't much more expensive. There's a buffet car for ordering hot food and drinks, aircraft-style bathrooms, and generously-spaced reclining seats.

After passing through suburban Sydney for an hour, the train began to pick up speed. The XPT is rated for 160 km/h (100 mph) service, though like parts of the American Northeast Corridor, it rarely goes that fast on the winding track. And as we were about to be told, we wouldn't be hitting the regular speed owing to track upgrades over much of the line.

We weren't in a hurry though. While it's fun watching the cars go backwards on the highways next to you, Australia's vast distances mean you're not on a train to get anywhere quickly. *Mate, this isn't the Shinkansen!* The real reason is to relax and take in the landscape. 

And it delivered!

<figure><p><img src="https://rubenerd.com/files/2024/xpt-trees@1x.jpg" alt="Some more Australian trees and colour alongside the road." srcset="https://rubenerd.com/files/2024/xpt-trees@2x.jpg 2x" style="width:500px; height:375px;" /><br /><img src="https://rubenerd.com/files/2024/xpt-outside-goulburn@1x.jpg" alt="Photo of a plain, river, some scattered trees, and some parting clouds in the late morning." srcset="https://rubenerd.com/files/2024/xpt-outside-goulburn@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

[Jeb Brooks](https://greenergrass.com) talks about that feeling that sets in when you watch the scenery on long train journeys. It's not everyone's cup of tea, but seeing the landscape roll by in a comfy seat is one of the most relaxing things one can do. He talks in reference to North America which has some stunning landscapes. Ours are more subdued, but they're just as beautiful and unique in their own way; the colours, trees, and skies couldn't be mistaken for anywhere else.

At lunch time the buffet car opened, and we got some chicken mac and cheese. It could have used some Tabasco or Thai sweet chili sauce, but was surprisingly good for something that was clearly microwaved. A bit later we got some tea, coffee, and a bit of cheeky chocolate, and watched some more scenery.

Australia's big cities are well spread apart from each other, with only small settlements in between along the coast. We passed through Goulburn, Yass Junction, Cootamundra, and Junee on the New South Wales side, though Mossvale looked the prettiest with its old buildings.

<figure><p><img src="https://rubenerd.com/files/2024/xpt-telegraph@1x.jpg" alt="A disused telegraph pole among the brush." srcset="https://rubenerd.com/files/2024/xpt-telegraph@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

One of the unexpected highlights of the trip was seeing all the overgrown old telegraph poles that are still standing. Some of them had their original wires and insulators, others had been cut or had succumbed to the elements. I imagined people at the turn of last century connecting thousands of kilometers of these cables to let isolated people across the continent communicate, and maybe then onto repeating stations and across the world. Some of them have been retrofitted for modern purposes, but most puncture the view at regular intervals like relics of a bygone era.

I was born in Sydney, but spent most of my early formative years in Melbourne when our family started moving around. When we started our run along the Hume Highway (below), I remembered all those road trips we used to take as a family between the two cities to visit relatives. I *hated* being stuck in a stuffy, boring car for hours at a time, but it was so pleasant watching that same view from the window of a train where I could get up, stretch my legs, and read without getting motion sick. Trains are the way to go!

<figure><p><img src="https://rubenerd.com/files/2024/xpt-hume@1x.jpg" alt="The Hume Highway running alongside the train." srcset="https://rubenerd.com/files/2024/xpt-hume@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

Just passed halfway we went through Albury, then crossed the state border into Victoria. We passed through Wangaratta, Benalla, Seymour, then Broadmeadows. Finally, an hour late due to that aforementioned trackwork, we pulled into Southern Cross station in Melbourne at around 19:20.

I mentioned above that the sleeper cabins were booked out. The XPT trainsets are being retired after more than four decades in service, to be replaced with new carriages that are seating only. It's ridiculous, especially considering the clear and growing demand for rail again. Overnight trains with sleeper cabins would be a great way to travel between Australia's two biggest cities, and with much lower carbon emissions than flying. I suspect people wanted to experience it before they disappeared.

<figure><p><img src="https://rubenerd.com/files/2024/xpt-southern-x@1x.jpg" alt="View inside Southern Cross station in Melbourne" srcset="https://rubenerd.com/files/2024/xpt-southern-x@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

I loved that I got to experience this. I'm usually under a time crunch when going to Melbourne for work, but I could see myself taking this again if I need some time to decompress. I'll be interested to see what the new trains are like when they finally enter service.

