---
title: "I now have a lexicon.pls file"
date: "2024-11-27T08:48:35+11:00"
abstract: "A Pronounciation Lexicon file can be used by text-to-speech engines to assist with pronounciation. Maybe some still recognise them?"
year: "2024"
category: Thoughts
tag:
- accessibility
- language
- xml
location: Sydney
---
In my continuing quest to implement *All The XML Things*, this morning I implemented a [lexicon.pls](https://rubenerd.com/xml/lexicon.pls) file. Specifically, a Pronunciation Lexicon file, as recommended by the W3C. [From the introduction](https://www.w3.org/TR/pronunciation-lexicon/)\:

> The Pronunciation Lexicon Specification (PLS) is designed to enable interoperable specification of pronunciation information for both Automatic Speech Recognition (ASR) and Text-To-Speech (TTS) engines. The language is intended to be easy to use by developers while supporting the accurate specification of pronunciation information for international use.

The format is fairly simple. There's some basic boilerplate for the XML file, where you define the language, namespace, schema, and your chosen phonetic alphabet:

    <lexicon version="1.0" 
	    xml:lang="en-AU"
	    xmlns="http://www.w3.org/2005/01/pronunciation-lexicon"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
	    xsi:schemaLocation="http://www.w3.org/2005/01/pronunciation-lexicon
            http://www.w3.org/TR/2007/CR-pronunciation-lexicon-20071212/pls.xsd"
	    alphabet="ipa">

And then there's the `<lexeme>` pronunciation itself. The spec defines what each of these elements mean, but this gives an example of their use:

    <lexeme>
	    <grapheme>NetBSD</grapheme>
	    <alias>Net B S D</alias>
	    <phoneme><![CDATA[nɛt biː ɛs diː]]></phoneme>
	    <example>Ruben loves NetBSD on his laptops.</example>
	</lexeme>

Here we can see a `<grapheme>` term defined, an `<alias>` used to indicate the pronunciation using other terms, a `<phoneme>` using the International Phonetic Alphabet we defined above in the `alphabet` attribute, and an `<example>`. Not all of these are mandatory, and you might not need to use `CDATA` for your elements, but I did just in case.

Here's another example with the correct pronunciation of OpenZFS:

    <lexeme xml:id="openzfs">
	    <grapheme>OpenZFS</grapheme>
	    <alias>Open Z F S</alias>
	    <phoneme><![CDATA[ˈəʊpᵊn zɛd ɛf ɛs]]></phoneme>
	    <example>Ruben trusts OpenZFS with his data.</example>
	</lexeme>

Then you can link to the resulting file in your website `<head>`:

    <link rel="lexicon" type="application/pls+xml" 
        href="https://rubenerd.com/xml/lexicon.pls"
        title="Pronounciation Lexicon" />

I also updated my [nginx mime.types](https://github.com/nginx/nginx/blob/master/conf/mime.types) file to recognise `pls` as an XML file.

My plan is to include a bunch of the jargon, acronyms, and other abbreviations I write about on this blog there. I already (ab)use XSLT to view my [Omake](https://rubenerd.com/omake.xml), [Blogroll](https://rubenerd.com/blogroll.opml), and [RSS feeds](https://rubenerd.com/feed/), so maybe I'll do one of those too.

My IPA is a bit rusty, so I might be refining these. But I *think* I got them right, and used tools like [toPhonetics](https://tophonetics.com/) to give me hints. Let me know if you see any obvious issues.

I expect modern software packages that can detect and use these files is *vanishingly* small. But if even one person with a screen reader or other similar tool is helped out by my inclusion of this, it was worth it.

