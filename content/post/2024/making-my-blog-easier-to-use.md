---
title: "Making my blog easier to use"
date: "2024-02-28T20:08:02+11:00"
year: "2024"
category: Internet
tag:
- design
- weblog
location: Sydney
---
[Michał Sapka](https://michal.sapka.me/ "Michał's website and blog") emailed a comment about my own blog here, though I think it equally applies to the format more broadly:

> I love discovering sites, and blogs are the least traversable of all. It’s impossible to find stuff at rubenerd without a search engine. I’m not saying it’s a bad thing (as I visit often and see new cool stuff),  but it was not what I wanted to host. 

I agree, and I've been thinking for a while how to improve things.

Blogs were an important development because they lowered the barrier to entry for regular writers, especially if you were in the habit of maintaining a journal or diary. They also standardised on a structure for a specific type of site, such that new readers already knew what to expect.

It's interesting to me that the groundwork for this was all laid by the mid-2000s, and we've only slipped backwards since. Modern blogs aren't as fun as old ones because fewer people are doing it, but also because modern web design trends have stripped away a lot of what made blogs interesting, discoverable, and accessible. Superficial minimalism has meant people don't include archive links, metadata, blogrolls, or even RSS icons. And you know what? I'm as guilty of this as anyone.
 
Maybe we struck a winning formula back then. Maybe the modern web's insistence on funneling people into social networks for *engagement* sucked the oxygen out of the proverbial room. Either way, I've talked before about how I kinda miss what old blogs looked like.

So I brought mine back :). Kinda. My new theme is a riff on one I tried out back in 2004, complete with a sidebar containing all my archives and silly buttons of yore, and metadata under each post. The Miller/Georgia font is also a bit of nostalgia, though I may change it back.

Let me know if I've broken anything, and thanks again for reading. It means a lot.


