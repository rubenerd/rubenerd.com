---
title: "Differences in health approaches, via Patreon"
date: "2024-08-02T20:27:59+10:00"
year: "2024"
category: Thoughts
tag:
- health
- life-work-balance
location: Sydney
---
I donate to enough independent people on Patreon that I've noticed a pattern, especially when things aren't going well for someone.

Invariably, if they're in Australia, Canada, New Zealand, or Europe:

> I've had a serious health issue. I will be taking a break while I recover. Thank you for your support.

And in Singapore or the US:

> I've had a serious health issue... but don't worry! I'm hard at work catching up/will still be working!

Interestingly enough, the UK *mostly* falls into the first category, though a couple do fall into the second as well. My sample size is large, but certainly not enough to draw a meaningful conclusion from this. I just thought it was interesting.

For the record, I think having a solid work ethic is a mark of maturity. But so too is taking care of yourself. What's the line, you either choose to take downtime, or life will choose for you?
