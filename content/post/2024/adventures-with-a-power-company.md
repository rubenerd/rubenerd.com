---
title: "Adventures with a power company"
date: "2024-12-14T09:21:45+11:00"
abstract: "Being told an invoice is overdue, when I had direct debit setup, is certainly a new one."
year: "2024"
category: Thoughts
tag:
- australia
- finance
- systems
location: Sydney
---
I've talked about this a few times on Mastodon, but thought it was worth sharing the saga here too.

Australia has a partially privatised power grid, meaning there are companies between customers and producers. These middlemen speculate on the wholesale price of electricity to generate profit; or what often happens, go out of business. Clara and I have been with *four* of these "energy retailers" since we moved in together. It's all a bit of a farce.

Our current retailer is one of Australia's largest, after our last company bailed us over to them in a very familiar story. As part of signing up to their service, we had the option of setting up direct debit. This involves providing a credit card or bank details, so bills are automatically paid each quarter when due. No muss, no fuss.

Those of you who read my financial posts a decade ago know I'm a *massive* fan of automation. It was a struggle at the start of my career, but since then I've always maintained a buffer of a few paycheques in my primary account. This means I could have rent, credit cards, utilities, and all life's other regular expenses autopay, and I wouldn't have to think about it (it also meant we're essentially spending our own money on the credit cards, and are just using them for points, insurance, indemnity, and travel credit).


### The messages begin

This time was no different. I set up a direct debit with the "energy retailer", and didn't think twice when a PDF was sent with a bill a month later. Then I got this SMS:

> FROM `$RETAILER`: Your Electricity bill (a/c `$NUMBER`) is due `$DATE`. To pay `$AMOUNT` (incl. GST), go to `$URL`. If you need a payment extension of 21 days, reply '1'. Paid recently - Thanks, you can ignore this message. P.S. If you haven't already, switched to eBills through our app or My Account to save time and paper.

I remember laughing a bit about the eBills comment. Wouldn't they have it in their records that I had elected not to have dead trees sent to me? I also had direct debit set up, so clearly this didn't apply.

Things got a bit more serious a week later:

> FROM `$RETAILER`: `$AMOUNT` (incl. GST), is overdue on your Electricity account `$NUMBER`. Please pay now at `$URL`. If you'll pay in 14 days reply '1'. If you'd like to&nbsp; pay your outstanding amount and your future 12 months of usage in instalments we can offer you a fortnightly Regular Payment Option to hear more reply '2'. If you've paid in the last three days reply '3'. Thank you if paid.

A couple of things here. First, it's no wonder so many people fall for scams when legitimate messages like this are formatted with run-on sentences that would make an LLM blush! This a serious issue: how are we supposed to educate people about how to spot fake messages when companies couldn't be bothered learning how to employ the `RETURN` key and correct punctuation?

But secondly, and this was the kicker, notice they said we were "overdue?" This was news to me. **How could we be "overdue" on an account with direct debit?** Direct debit means they should have debited us automatically before payment was due! Therefore, if the account was overdue, that's their fault.


### "Jumping on a call"

I'll spare quoting the rest of these messages, but after the fourth one came in threatening to disconnect our service, I got fed up. I can't stand wasting time on calls to resolve issues that aren't my fault, but I spent the better part of an afternoon calling their overworked support line, with all the usual platitudes that "my call was important to them" and so on. I've been on the other side of that phone in a technical capacity before, so I do empathise how thankless a job it can be.

When I was finally put on with an overworked agent, this was roughly what transpired (I took notes):

> **Them**: Yes, I can see your last payment was missed, that's why you received those messages about payment being overdue.
>
> **Me**: Forgive me, but I don't understand. How can an account be "overdue" if I have direct debit set up?
>
> **Them**: Overdue in this case means payment was missed.
>
> **Me**: Yes, but I have direct debit set up. I'm logged into my portal now, and I can see my card is on file, and direct debit is enabled.
>
> **Them:** If you do, that's great, it means future payments will be automatically deducted from that card.
>
> **Me**: Okay, but the key word there is "future". I set up direct debit months before this "overdue" invoice was generated. This "overdue" invoice should have been automatically paid.
>
> **Them:** Direct debit will deduct from your nominated payment method, correct.
>
> **Me**: Right, but it wasn't. That's the issue. This should never have been overdue. It should have been paid automatically.
>
> **Them**: (long pause as a light bulb glows)... Oh. Ok, that's weird. Did you have the funds in your account to cover it?
>
> **Me**: Yes, always. I only use this card for utilities. Can you confirm direct debit is set up on your side?
>
> **Them**: One moment... yes, I can see it is on your account.
>
> **Me**: Awesome. So to make sure we're on the same page, I had direct debit set up before that invoice was generated. Your company's billing system didn't use it. So the invoice became overdue. It wasn't me who was late with payment, it was your company. Do you agree?
>
> **Them**: Yes, I understand.
> 
> **Me:** Thank you. Can you initiate payment from your side now?
>
> **Them**: Yes, one moment... done. You will receive an invoice shortly.
>
> **Me:** Brilliant, thank you! Are you able to follow up with your team to confirm why this didn't get done automatically? Because you can understand I'd be worried about this happening again in three months.
>
> **Them**: Yes this was an issue on our side, we'll sort out your account.

Three months went by, and another invoice was generated. Do you think the issue was sorted out? Do you think I got more threatening messages, and had to have another phone call, which somehow took even longer?


### Goodbye agency, my old friend

There's a unique form of desperation and despair that manifests from interacting with a system in which you have no visibility, access, or control. When you realise you're an edge case, or have fallen through the cracks, and you know that you have little to no recourse. The only option is to switch to another provider, and cross your fingers that they don't file you away incorrectly as well.

Good businesses have ways of dealing with these issues. I know a few people who work in dispute resolution (including my sister), and the key is a clear path to escalation, with a human who has the access and/or authority to resolve issues manually. Because edge cases and errors do exist. They are an absolute certainly.

I expect AI to make this much, much worse, as bad businesses convince themselves they can *make number go up* by replacing more people with bots. Already this energy retailer has a chatbot they expect you to use first before contacting their phone system, and I went in so many circles interacting with it, I felt as though I was circling the proverbial drain.

Clara and I will be switching providers, again. Will the new one be any better? Well, they can't be worse... right? R-right?
