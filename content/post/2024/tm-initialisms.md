---
title: "TM initialisms"
date: "2024-08-17T20:32:26+10:00"
year: "2024"
category: Thoughts
tag:
- lists
- pointless
location: Sydney
---
Off the top of my head:

* Tape monitor

* Telekom Malaysia

* Ticklish marsupial

* Tōkyō Metro

* Trade mark

* Turing machine

* TYPE-MOON (desu)
