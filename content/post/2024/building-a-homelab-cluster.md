---
title: "Building a small, dense homelab cluster"
date: "2024-10-08T10:31:52+11:00"
year: "2024"
category: Hardware
tag:
- arm
- cases
- homelabs
- raspberry-pi
- risc-v
- servers
location: Sydney
---
Speaking of [homelabs](https://rubenerd.com/the-difference-between-prod-and-homelabs/ "The scale difference between prod and homelabs"), I'm looking forward to building a small cluster in Clara's and my upcoming 25 RU home server rack. I've got used to Xen and Bhyve for one-off or mirrored hypervisors, but I've only ever used large clusters at work. The plan would be to use this to test distributed storage, RDMA, hypervisor failovers, and tooling like Proxmox, XCP-NG, and K8s that clients at work are migrating from, as well as more Xen, Bhyve, and NVMM testing.

I learn from tinkering, so this would be first and foremost an educational tool I could spin up, tinker with, and tear down without worrying about breaking something. It would be discrete and separate from our FreeBSD and NetBSD homelab "prod" workloads, such as Jellyfin, Minecraft, PostgreSQL, OpenZFS, and our Wireguard VPN that I can't afford to break.

There are a few options, with pros and cons for each.


### Option 1: 2U servers

<figure><p><img src="https://rubenerd.com/files/2024/cluster-silverstone@1x.jpg" alt="Front and back profile views of the 2U SilverStone RM23-502-MINI" srcset="https://rubenerd.com/files/2024/cluster-silverstone@2x.jpg 2x" style="width:500px; height:114px;" /></p></figure>

As mentioned previously, rack-mountable servers aren't ideal for a homelab owing to their depth and noise; attributes that often miss those people who offer blanket recommendations of "just get an R620!" You can often hack them with PWM fans and separate controllers, but their length also extends farther than the space Clara and I have designated for the depth-adjustable rack.

But I have a few spare boards lying around I could use inside a  [svelte 2U SilverStone cases](https://www.silverstonetek.com/en/product/info/computer-chassis/RM23-502-MINI/) and some quiet low-profile Noctua fans. While they'd probably use the most power of all the options here, I could always slowly upgrade the power supplies to more efficient units, and be more aggresive with suspend/resume.


### Option B: “Tiny” PCs

<figure><p><img src="https://rubenerd.com/files/2024/cluster-thinkcentre@1x.jpg" alt="A pair of ThinkCentres in a 1U chassis from MyElectronics" srcset="https://rubenerd.com/files/2024/cluster-thinkcentre@2x.jpg 2x" style="width:500px; height:181px;" /></p></figure>

Various manufacturers make PCs in a "tiny" form factor (like Clara, cough). While they can't hold a candle to a modern Apple Silicon Mac Mini in performance, they can be picked up for less than AU$50. Some examples include the Lenovo ThinkCentre, Dell OptiPlex, and HP ProDesk. I've also seen Fujitsu and Panasonic units in Japan.

You can mount them in [3D-printed cases](https://www.thingiverse.com/thing:6375095), or Jeff Geerling has recommended [MyElectronics](https://www.myelectronics.nl/us/19-inch-1u-rackmount-for-2x-lenovo-tiny.html) in the Netherlands (whether they come with [stroopwafels](https://en.wikipedia.org/wiki/Stroopwafel) is unknown). Upgrading might be a pain, but they'd be small, quiet, and relatively energy efficient.


### Option 三: Pi cluster or equivalent

<figure><p><img src="https://rubenerd.com/files/2024/cluster-sbc@1x.jpg" alt="A MyElectronics 1 mount for five Raspberry Pis" srcset="https://rubenerd.com/files/2024/cluster-sbc@2x.jpg 2x" style="width:500px; height:286px;" /></p></figure>

The [Raspberry Pi](https://www.raspberrypi.com/products/) has spawned a world of single-board computers in the same form factor, often with the same standoffs. For example, the [Pine Rock64](https://pine64.org/devices/rock64/) for an alternative ARM option, the [UP 7000](https://up-board.org/up-7000/) for Intel, and the [Milk-V Mars](https://milkv.io/mars) for RISC-V. I'm *extremely* keen to try all these out.

Like the Clara-sized PCs (cough), these SBCs can be mounted in a rack with a 3D-printed chassis, or you can fit more stacked vertically in a 2U unit. Pair these with a PoE switch in the RU above or below it, and you'd have an efficient, small, dense cluster for testing. Though it's interesting that the tiny PCs have a lower upfront cost.

Again, [Jeff Geerling recommended](https://www.jeffgeerling.com/blog/2021/review-myelectronics-raspberry-pi-hot-swap-rack-system) a fine rack from [MyElectronics](https://www.myelectronics.nl/us/raspberry-pi-rack-mounts/), though he's also since recommended some fancier units with a full enclosure and active cooling.


### Option δ: Dense backplane of compute modules

<figure><p><img src="https://rubenerd.com/files/2024/cluster-turingpi@1x.jpg" alt="Press image of the Turing Pi 2" srcset="https://rubenerd.com/files/2024/cluster-turingpi@2x.jpg 2x" style="width:250px; height:269px;" /></p></figure>

This is the option I'm most intrigued by, because I've never used them before. The [Turing Pi CM3](https://turingpi.com/) is probably one of the more well-known units, which is tiny when paired with a 9V barrel jack and/or a [picoPSU](https://mini-box.com.au/product/picopsu-160-xt/). I'd just need to find a chassis to mount it into a server rack.

As for the modules, there's the popular [Raspberry Pi Compute Module](https://www.raspberrypi.com/products/compute-module-4s/), though friends of mine speak highly of the [SOPINE](https://pine64.org/devices/sopine/) among others. My concern would be getting something similar for x64 too, if such a thing exists.


### Conclusion

I'd probably be leaning towards the tiny PCs, just because I have a local source going for *extremely* cheap right now. The idea of a SBC RISC-V cluster does excite me in a way that's hard to describe though.

Who'd have thought The VM Guy would be sharding out stuff to multiple physical machines again? It's HA!
