---
title: "My dmesg(1) reports a booting mind again!"
date: "2024-02-10T08:15:53+11:00"
year: "2024"
category: Thoughts
tag:
- health
- personal
location: Sydney
---
**UPDATE:** *Evidently my brain still isn't completely functional yet, on account of dmesg being in section 8, not section 1! Derp. Thanks to Simon Wheeler for pointing this out!*

Completely out of the blue, these last couple of weeks have been some of the worst of my life! But as cliché as it sounds, things only being icky today feels wonderful. There's something about perspective there.

Even family and mental health issues aside, I was struck down this week by the worst flu I've ever had; my first illness that topped COVID. My mum often talked about her chemo sore throats keeping her from sleeping, and now I know how. To put it into perspective, I'd *rather* have testicular surgery again.

Here comes the proverbial posterior prognostication: *but...* I managed to turn a corner last night, and today I feel fantastic. It's as though the `swallowfood` service has started, and the `breathe` service got unstuck after having unmet boot dependencies. Being able to do these two things again without blinding pain is like appreciating a gift I always had, but took for granted.

I still don't have resolution on a bunch of things that were taking up my mental health and time last week, and I'm still sick on the level of a bad cold, but weirdly enough having had this recent experience I feel like I can *take the world on* again. Take on... me?

<figure><p><a target=_BLANK href="https://www.youtube.com/watch?v=djV11Xbc914" title="Play a-ha - Take On Me (Official Video) [Remastered in 4K]"><img src="https://rubenerd.com/files/2024/yt-djV11Xbc914@1x.jpg" srcset="https://rubenerd.com/files/2024/yt-djV11Xbc914@1x.jpg 1x, https://rubenerd.com/files/2024/yt-djV11Xbc914@2x.jpg 2x" alt="Play a-ha - Take On Me (Official Video) [Remastered in 4K]" style="width:500px;height:281px;" /></a></p></figure>

I have two groups of people I want to thank! First, to the amazing people at the Chatswood Medical Practice, you kept my spirits up as much as you helped me with medicine, tests, and treatments. And a big thanks and a hug to those of you who sent messages. I appreciate some of you aren't exactly doing well yourselves, and surprising nobody, I've yet to follow up with all of you for your kind words. But know that they meant a lot. ♡

Also, make sure you're up to date with your flu shots. I can't imagine how much worse this would have been if Clara and I didn't keep obsessive records of what we've had and whether we're due for another.
