---
title: "SD to IDE adaptors, and revisiting retrocomputer storage"
date: "2024-08-03T09:01:30+10:00"
thumb: ""
year: "2024"
category: Hardware
tag:
- retrocomputers
- storage
location: Sydney
---
I talked a *lot* about adding bulk IDE storage to retrocomputers here a few years ago. It's one of those aspects of building, upgrading, and repairing old machines that can be delightfully simple, or devilishly frustrating, depending on luck and what basket of parts you have.

And it's getting trickier! For the longest time, the advice was to use a **CompactFlash card** with a passive IDE adaptor, but they're becoming rarer and more expensive. Finding the ones with the right UDMA support is also a crapshoot: I have a small stack of these cards that claim to be "industrial" units that simply don't work, and consumer SanDisk cards that do. 

My "solution" has been to buy **Transcend IDE SSDs**. These eye-wateringly expensive units work in every machine I've tried, and I'm glad I have it in my "daily driver" machines. But it's financially infeasible to equip *every* machine I have with these, especially if earlier devices wouldn't see beyond a 504 MiB or 1024 cylinder boundary anyway.

Another option are industrial **Disk on Module** units, which integrate storage directly onto an IDE header. These have worked fine on my early 386 and 486 machines, but the quality and performance of units varies wildly, and most available online seem to be second-hand units with unknown histories. [DosLab Electronics](https://doslabelectronics.com/shop) make modern DoM units that look like they could work great; I might need to test.

Which leads us to **SD to IDE** adaptors. These "Sintech" clone devices are everywhere on eBay and AliExpress, and you'll often see them recommended by retro bloggers and YouTubers. SD cards aren't an ideal bulk storage medium for a host of reasons, and I wouldn't trust them with irreplaceable data, but they're the only affordable solution on this entire page! I'd hoped I could get one working for tinkering.

<figure><p><img src="https://rubenerd.com/files/2024/sd-to-ide@1x.jpg" alt="An SD to IDE adaptor." srcset="https://rubenerd.com/files/2024/sd-to-ide@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

I ordered a few back in 2019, but I could never get any of them working. I subsequently sent them to eWaste, but recently I was interested to see if I could give them another try. I ordered a new unit and put it through its paces with a couple of SanDisk Ultra 16 GB cards I had spare.

Weirdly enough, my [NEC APEX 486](http://retro.rubenerd.com/necapex.htm) and [Dell 4100 Pentium III](http://retro.rubenerd.com/dell4100.htm) both detected the unit this time! It had an interesting BIOS POST message, suggesting the origins of the firmware or controller:

    Fixed Disk 0: FC-1307 SD to CF adaptor V1.4
        (UDMA=100MHz)

I was able to boot my trusty PC DOS 5 rescue disk, `fdisk` the card, `format` it with the `/s` flag to copy across system files, and boot. It... just worked? **HOW!?** Even more surprisingly, it completed an install of Windows 95 in less time than *any* of my CF cards, and almost as fast as my IDE SSD. I should probably run some more specific benchmarks.

I'm not sure if I got a bad batch of adaptors last time, or whether I had another variable at play. I recall swapping the IDE cables, changing the connectors, using different SD cards, and giving them a pensive stare to intimidate them, and nothing worked. This unit was plug and play.

Alas, it's not all good news. These SD to IDE card adaptors still have the frustrating limitation of taking over both channels of an IDE interface, so you can't have any other device on the same ribbon cable. This isn't a problem on a machine with two IDE interfaces, but my 386 and 486 machines only have one which needs to be shared with a CD-ROM.

My plan is to try and offload the CD-ROM to the sound card IDE header on these old machines again, otherwise I might need to go down the SCSI route after all, or finally fulfill my dream of using one of those MFM emulators. Except, now we're back into expensive territory!

Sometimes I wonder why I do this to myself.
