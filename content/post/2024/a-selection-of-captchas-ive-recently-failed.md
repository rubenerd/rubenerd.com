---
title: "A selection of CAPTCHAs I’ve recently failed"
date: "2024-03-26T08:27:27+11:00"
year: "2024"
category: Internet
tag:
- accessibility
- ergonomics
- design
- dark-patterns
location: Melbourne
---
I posted on Mastodon that I'd failed a CAPTCHA, but it reminded me it's been happening a lot lately. Here are some more:

* Failed to select a motor scooter when it asked for motorbikes.

* Failed to select a crosswalk. We don't use that term here, so that's an i17n failure. I assumed it referred specifically to level crossings in the middle of a road, not at an intersection.

* Failed to select the pole upon which a traffic light was mounted. I only selected the light fixture itself, because the pole was part of a lamp post. 

* Failed to select a gentle slope when it asked for mountains.

We've now come full circle! Proponents claimed CAPTCHAs were supposed to help disambiguate humans from machines (a claim I always thought was dubious). Now CAPTCHAs are expecting us to think like bots to pass the same tests.

Mechanical turks&mdash;pardon, AI&mdash;can already solve these at scale, so it's time to put this accessibility nightmare out to pasture. Just don't ask them to *solve* for pastures; they'll probably select a pond with green algae.
