---
title: "Matching toolkits and desktop environments"
date: "2024-11-02T08:26:40+11:00"
year: "2024"
category: Software
tag:
- bsd
- freebsd
- kde
- linux
- netbsd
- xfce
location: Sydney
---
You know that bell curve meme where the amateaur starts with something simple, the genius does something complicated, and the wise sage returns to something simple? It's an oversimplication for most situations to which its applied, but it tracks perfectly with my desktop environment and window manager experience.

As I'm sure many of us did, we started on whatever desktop our \*nix of choice shipped. That meant Gnome on the first Red Hat Linux I ran, then KDE with Mandrake, and Xfce with my beloved Cobind Desktop. I probably would have started on CDE had my first \*nix been something commercial, or maybe OPENSTEP if I'd even heard of NeXT as a kid. But I digress. 

For these desktops, it was less about making a deliberate choice, and thinking "well, that's what Red Hat looks like". While classic Mac OS and Windows 95 had customisable skins and themes&mdash;back when computing was personal&mdash;the idea of swapping out the *entire GUI* for something else was a foreign concept; beyond the likes of Norton Desktop or Calmira of course.

Over time, I figured out these Linux distros had *chosen* a desktop, much as my family had chosen Brown Bag Software PowerMenu, then Windows 3.x for our 486SX running DOS. But this raised the question about what *other* desktops are available. What do they look like? How do they work? Are they cool? Naturally you'd want to try them all.

I think it's a rite of passage to have built your own graphical environment when you're entering the world of \*nix; it's why Linux distros like Arch remain popular in spite of the existence of Mint and Fedora. There's a sense of accomplishment having cobbled together something cohesive and usable from disparate components that fit your exact requirements. Do you prefer overlapping or tiled window managers? What about Finder-style or orthodox file managers? Or menus or keyboard launchers?

I tended towards minimalistic window managers like Fluxbox and Xmonad, before trending back towards Xfce and eventually KDE Plasma. I realised how much work it was maintaining my own set of desktop applications, especially when I'd hit an edge case, and in the end I had better things I wanted to do.

Or at least... *so I thought!* Something that has persisted since is my desire to use applications written with the same graphical toolkit as the active desktop. That means Qt for KDE, and GTK for Gnome and Xfce. Back when I was using something like Openbox, I'd still prefer to be consistent and use all Qt or all GTK. What toolkit something was written in would often be the deciding factor in choosing certain software.

It's not a *completely* silly thing to do. The entire founding premise of KDE back in the day was bringing consistency and predictability to the desktop, after years of programs doing their own thing. KDE and KDE-adjacent applications written in Qt have similar menu layouts, and their settings screens look and work the same. Xfce tools all have a similar minimal yet functional design that I still think is a high-water mark.

Certain technical people would critique being worried about such fluffy stuff, but I use these machines every day, and I'd like the experience to be seamless and pleasant (it's honestly the main reason I lament the state of current macOS, but that's a separate discussion).

In the end though, is a phrase with four words. Despite one's best efforts to run what looks like a cohesive desktop, you'll always have exceptions. I daily drive LibreOffice, Firefox, and Inkscape in KDE, and I do prefer running Kwrite/Kate and Krita even in Xfce. Turns out, you should use the right tool for the job, not the one that happens to be written in a specific toolkit (I'm also running Ranger as my "file manager" as well, which warrants its own post).

It does make me wonder if we'll ever get a unifying toolkit and appearance. Red Hat tried years ago with Bluecurve, but the result wasn't great. That's the problem with open source people, it's like herding a field of intelligent cats with specific opinions.

