---
title: "Accessibility is still an uphill battle"
date: "2024-11-16T15:01:31+11:00"
year: "2024"
category: Ethics
tag:
- accessibility
location: Sydney
---
I overheard a few techbros having a conversation next to me at this coffee shop. It was easy, they weren't using their indoor voices:

> **Dude A:** Day one seems to all be about accessibility. They have workshops and...
> 
> **Dude B:** Well that's a massive wank. Day two?

Being an older millennial, I can still remember when nerds were on the side of the marginalised, different, and weird. We were accepting, because we felt like society and regular social circles weren't accepting of us either.

Now the shoe is on the other foot, and many in our community are no better than those who used to mock and chastise us. We used to be better than that. Or at least, I thought so.

Dare to be the person who still is. 🖖
