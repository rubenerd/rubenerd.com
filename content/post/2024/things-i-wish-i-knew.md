---
title: "Things I wish I knew"
date: "2024-09-19T15:19:56+10:00"
year: "2024"
category: Thoughts
tag:
- emacs
- geography
- lists
- music
- personal
- swimming
location: Sydney
---
This list is non-normative, non-exclusive, and non-useful. Wait.

* **African and South American geography**. I can pinpoint every country on a map of Europe, Asia Pacific, and North/Central America, right down to Moldova, Tuvalu, and Martinique, but there are swaths of the world I don't know as well as I should.

* **The drums**. For someone who loves music as much as I do, and who taps his fingers on the table to the drum line and beat of every jazz, blues, and pop song, this seems like such a wasted opportunity and potential creative outlet. *Cue epic Karen Carpenter solo... rest in peace ♡.*

* **Japanese, Latvian, and German.** Das paldies, desu.

* **Emacs**. I keep [dipping my toes](https://dictionary.cambridge.org/dictionary/english/dip-a-your-toe-in-the-water), but then "life" happens and I fall back onto Vim and Kate again. Which are excellent editors in their own way, but Orgmode and its plugin architecture look amazing.

* **Diving**. Speaking of toe dipping, swimming is great exercise, but I've never been able to overcome my weird hangup about *jumping* into water, as opposed to climbing in and pushing off the wall. Weird, I know.

* **PHP**. Yeah, *that* PHP! It's still ubiquitous, and alongside Python, is the language in which most of the stuff I care about is written. Heck, as a Perl hacker, I'm halfway there.

* **How not to be anxious, or overthink things**. Am I overthinking this? *Who knows!*

