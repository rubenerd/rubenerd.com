---
title: "My A-Z toolbox: aria2"
date: "2024-08-24T07:11:19+10:00"
year: "2024"
category: Software
tag:
- az-toolbox
- tools
location: Sydney
---
I haven’t done a post series in a long time! This is the first in one I’m dubbing my [A-Z Toolbox](https://rubenerd.com/tag/az-toolbox), in which I list tools I use down the alphabet for no logical reason.

The letter A has a lot of great options, from the ubiquitous [awk](https://github.com/onetrueawk/awk) text processor, to the [ack](https://beyondgrep.com/) search tool, and Thomas Habets’ [arping](https://github.com/ThomasHabets/arping) which helps diagnose which MAC address has been assigned what IP. 

But **[aria2](https://aria2.github.io/)** by Tatsuhiro Tsujikawa is the clear winner for me. This download tool supports HTTP, SFTP, Metalink,  BitTorrent, and even classic FTP. It can also resume broken or incomplete downloads, making it *indispensable* when you're on bad connections or spotty servers.

As an example of its versatility, torrents can be downloaded with a magnet link, or a torrent file:

    $ aria2c https://cdn.netbsd.org/pub/NetBSD/images/10.0/NetBSD-10.0-sparc.iso.torrent

I use `fetch` if I'm starting out on a fresh BSD box, and `wget` on Linux. But I very quickly install aria2. It's just that useful.

