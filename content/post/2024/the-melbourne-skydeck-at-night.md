---
title: "The Melbourne SkyDeck at night"
date: "2024-03-26T09:19:17+11:00"
year: "2024"
category: Travel
tag:
- melbourne
- photos
- observation-decks
location: Melbourne
---
Clara and I have a thing for observation decks. They're usually among the first places we visit in any new city we go to; they're such a unique and chill way to spend an evening. I dedicate an entire tab under Travel on my [Omake page](https://rubenerd.com/omake.xml) to them!

Alas, I was bitterly disappointed to learn that my [favourite Rialto Towers](https://rubenerd.com/the-rialto-towers/ "Visiting the Rialto Towers again") observation deck had been closed way back in 2009, with all its 1980s splendour. But there's now a public observation floor on the 88th floor of the Eureka Tower in Southbank, so we headed down after Clara had finished her business trip work for the day.

<figure><p><img src="https://rubenerd.com/files/2024/melbourne-skydeck@1x.jpg" alt="Photo of the Eureka Tower at dusk." srcset="https://rubenerd.com/files/2024/melbourne-skydeck@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

This building was finished when I started studying in Adelaide in the late 2000s, and I remember struggling to decide whether I liked it or not. With the melted cheese look of most modern buildings in the city, I've since warmed to its simple geometric lines and bizarre inclusion on its uppermost floors. It was briefly the tallest residential tower in the world, until Dubai does what Dubai does.

After you buy your tickets and walk in, you're greeted with this impressive 3D-printed model of Melbourne's CBD, which I was told is updated when something else gets built. You can see the Eureka at the far right, and the Rialto left of centre.

<figure><p><img src="https://rubenerd.com/files/2024/melbourne-3d-print@1x.jpg" alt="Photo looking over a 3D-printed model of the Melbourne CBD downstairs." srcset="https://rubenerd.com/files/2024/melbourne-3d-print@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

We then proceeded upstairs into what they claimed was *the fastest lift in the Southern Hemisphere*. Melbourne reminds me a lot of KL in this way; they *really* want to prove themselves. The tallest, the fastest, the biggest, the [Daft Punkiest 📺](https://www.youtube.com/watch?v=gAjR4_CbPpQ "Daft Punk - Harder, Better, Faster, Stronger"). Comparisons are a core part of the city's identity, especially with Sydney which it recently surpassed in population again. Maybe its the weather.

With that amateur psychoanalysis out of the way, we got out at the 88th floor, and the view was stunning. The deck is certainly higher than the Rialto's, and being across the river means you get a better view of the whole city. *Including the Rialto!*

<figure><p><img src="https://rubenerd.com/files/2024/melbourne-skydeck-rialto@1x.jpg" alt="Close-up of the Rialto Tower roof at night from the SkyDeck." srcset="https://rubenerd.com/files/2024/melbourne-skydeck-rialto@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

You can see a bit of what Melburnians call the *Hoddle Grid* when you look north. This is the original grid alignment of the city streets that were put down in the early 1800s. The rest of the city radiates out from here at a slightly different angle, which you can see in the trailing lights in the distance.

<figure><p><img src="https://rubenerd.com/files/2024/melbourne-skydeck-east@1x.jpg" alt="Photo of the eastern side of the Melbourne CBD." srcset="https://rubenerd.com/files/2024/melbourne-skydeck-east@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

Whomever was standing behind me in this second one looks like he's a giant attempting to crush a building like a kid with an empty can: 

<figure><p><img src="https://rubenerd.com/files/2024/melbourne-skydeck-west@1x.jpg" alt="Photo of the western side of the Melbourne CBD." srcset="https://rubenerd.com/files/2024/melbourne-skydeck-west@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

We spent probably an hour up there walking around, talking, and taking photos. It was lovely! If you have a spare evening, I can recommend it. Maybe sneak in your own snacks though, the bar up there has tourist prices. May I suggest a HappyLemon bubble tea, or some excellent Melbourne coffee?
