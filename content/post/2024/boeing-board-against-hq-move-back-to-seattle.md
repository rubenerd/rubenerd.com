---
title: "Boeing board against HQ move back to Seattle"
date: "2024-03-13T10:07:26+11:00"
year: "2024"
category: Thoughts
tag:
- aviation
- boeing
- business
- news
location: Sydney
---
*I wrote this before reading about former quality manager and whistleblower John Barnett being found dead prior to giving testimony. Good grief, what's going on?*

[Dominic Gates in the Seattle Times](https://www.seattletimes.com/business/boeing-aerospace/boeing-board-blocks-shareholder-push-to-bring-hq-back-to-seattle/)\:

> Boeing’s board has killed a longshot shareholder proposal that the company move its headquarters back to Seattle [citing interference].
>
> In a statement supporting his proposal, [investor Walkter Ryan] wrote that “Boeing became an industry leader in commercial aviation because of the close working relationship between manufacturing, engineering, and management.”

> He added that “the most significant factor” in the company’s decline “was relocating Boeing’s headquarters from Seattle and separating executives from Boeing’s core commercial manufacturing business.”

Melvin Conway [famously asserted](https://en.wikipedia.org/wiki/Conway's_law "Conway's Law on Wikipedia") that the design of software inextricably reflects the structure of the organisation that developed it. I can't help but see parallels between this and Boeing's issues since management moved to Chicago. This isn't a unique insight; spend five minutes reading anything from actual former engineers and you'll see the same concerns.

Boeing's interviews and press releases make it clear that management want to see this storm through, and want to take the required steps to make this happen. But talk is cheap. Boeing moving their HQ back to Seattle (and axing those former McDonnell Douglas managers) would send an unequivocal message to the travelling public, airline customers, regulators, and investors that they're taking the situation they currently face seriously. That they're regrouping, renewing their focus, and going back to the formula that made them a trusted, respected, and reliable manufacturer in the first place. A reputation they built over decades, and then lost in the blink of an eye.

Whether they deserve all the current PR flak they're facing right now is one thing, but seemingly ignoring it is, frankly, baffling. Though I'm sure Airbus and Embraer's sales teams appreciate it.
