---
title: "Introducing my new 8-bit Software Fun series"
date: "2024-02-17T08:58:46+11:00"
year: "2024"
category: Hardware
tag:
- 8-bit-software-fun
- retrocomputing
location: Sydney
---
I write technical documents and engineer cloud infrastructure for my day job, but I've made no secret of my love for 8-bit and other [retrocomputer](https://rubenerd.com/tag/retrocomputing) hardware here. But in a recent pseudo-hallucinatory flu state where I didn't sleep for two days, as many things struck me:

* I've never really talked about 8-bit **software** before; either the system software, games, or other stuff.

* Most retrocomputer sites and YouTube channels assume a **base level of knowledge** or history with these machines. What if your first computer was a 32-bit 486 or newer like me, and you're starting basically from first principles?

In this imaginatively-titled [8-bit Software Fun](https://rubenerd.com/tag/8-bit-software-fun/) series, I intend to explore some of these old systems and their software as though you've booted one for the first time. I'll use tools like [VICE](https://vice-emu.sourceforge.io/) until I can get a capture setup going, but either way I think it'll be fun!

<figure><p><img src="https://rubenerd.com/files/2024/8bit-screenshot-jaffydos.gif" alt="Screenshot of JaffyDOS showing the output of a PRINT command saying HELLO RUBENERD.COM READERS!" style="width:384px; image-rendering: pixelated;" /></p></figure>

Video sites like YouTube have been wonderful for learning about this old hardware, but I'm a writer first, and I do tend to prefer reading over watching something if I can. It's easier to refer to code and text in written form, and I seem to comprehend it quicker. I've read (hah!) a few of you are in a similar boat, so this series will be for you.

In terms of specific hardware, I love the Commodore 128 the most, though fortunately most of the commands and syntax are broadly compatible across their machines. My goal is to give general information, but drill down into specifics in their own posts where it's warranted. I'm also looking at potentially the Apple \]\[ which I know much less about, and maybe some British kit if I can get my hands on some (I'm still kicking myself that I lost an Acorn Electron auction by $2. I spend more on coffee). 

I'm putting the finishing touches on the first proper post in this series, and should have it out soon.
