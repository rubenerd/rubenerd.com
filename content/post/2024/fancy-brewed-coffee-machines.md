---
title: "Brewed coffee over espresso"
date: "2024-09-10T07:04:01+10:00"
thumb: "https://rubenerd.com/files/2024/cawfee-machines@1x.jpg"
year: "2024"
category: Hardware
tag:
- coffee
location: Sydney
---
I'd say most coffee people like both espresso *and* brewed coffee, but we all have a preference towards one or the other. Espresso is made from water forced through coffee at high pressure, whereas a brewed coffee is allowed to steep and filter its way to your cup. Both have their pros and cons, and have such diverse taste that I classify them as different drinks, just as I do boba and green tea, or my beloved Pilsner or a Weißbier.

Weirdly for me, my coffee preference tends to be time dependent. I love a light, brewed coffee first thing in the morning, especially when I can sit in the balcony with a mug and watch the sun rise. This is peak life, and is the thing that gets me up in the morning. By lunchtime or the early afternoon, there's nothing better than a crisp salad and a long black or espresso to taste great and keep me going.

Coupled with the fact brewed coffee isn't as popular in Australia (I miss Blue Bottle and Philz in San Francisco!), I've settled into a routine where I go to local coffee shops when I crave espresso or a long black, and brew coffee at home. This has worked out well for me; espresso tends to require more expensive, complicated kit, whereas a decent brewed coffee can be made with something as simple as an AeroPress.

Actually, that's worth expanding on briefly. I absolutely, positively *love* my AeroPress. It's one of the most important, wonderful pieces of modern technology I've ever owned. I brewed *thousands* of cups over a decade through my original unit, especially during Covid lockdowns where I was making anywhere from two to six cups a day for both Clara and I. When the plastic split last year after a decade of use, it was replaced... with another AeroPress! I've not looked at a French press ever since.

<figure><p><a target="_BLANK" href="https://www.youtube.com/watch?v=t8eYs2vxT-8" title="Play The Best Home Coffee Brewing Machine"><img src="https://rubenerd.com/files/2024/yt-t8eYs2vxT-8@1x.jpg" srcset="https://rubenerd.com/files/2024/yt-t8eYs2vxT-8@1x.jpg 1x, https://rubenerd.com/files/2024/yt-t8eYs2vxT-8@2x.jpg 2x" alt="Play The Best Home Coffee Brewing Machine" style="width:500px;height:281px;" /></a></p></figure>

But this has led me to wonder what else is available in the space for hobbyist home coffee brewers. Clara clued me back into James Hoffmann (for whom I have a massive crush, he's gorgeous), and in particular his brewed coffee machine review from a few years ago.

Two machines immediately stood out to me: the *Moccamaster*, and the *Wilfa Performance*. Both have basic feature sets and clean designs, which is what I'm after. They come with the option for glass carafes, which I prefer because I'd be drinking the coffee immediately.

They're also, much like James Hoffman, gorgeous:

<figure><p><img src="https://rubenerd.com/files/2024/cawfee-machines@1x.jpg" alt="Photo of the Moccamaster (left) and the Wilfa Performance." srcset="https://rubenerd.com/files/2024/cawfee-machines@2x.jpg 2x" style="width:500px; height:274px;" /></p></figure>

This will firmly sit in the *aspirational* camp for now! But they were still fun to research.
