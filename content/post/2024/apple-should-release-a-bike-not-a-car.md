---
title: "Apple should release a bike or scooter, not a car"
date: "2024-01-03T08:48:07+11:00"
year: "2024"
category: Hardware
tag:
- mobility
- urbanism
location: Sydney
---
Love them or hate them, but Apple sets trends that others are quick to copy. They should use that to advance their self-described sustainability goals, rather than contribute to problems.

Screw Apple's rumoured smart, self-driving, and/or electric car. They should release a smart bike, scooter, or another personal mobility device. Something premium, beautiful, and most importantly, *desirable*.

This would have four effects:

* It'd make the market segment more approachable. Apple rarely have the best device, but they tend to make ones people can grok.

* The fashion-concious&mdash;who'd otherwise have shunned or ignored such things&mdash;will suddenly be clambering to be seen riding the *Pro Max Ultra Titanium* SKU of the Apple Bike or iScooter. This would expand the profile and visibility of these devices.

* This in turn would open the flood gates to imitation, and put the spotlight on other manufactures making better devices now, and in the future. A rising tide would lift all boats.

* It would also put pressure on urban infrastructure to improve. We saw this when mobile operators had to adjust to the data requirements of the iPhone, after years of WAP.

An Apple car *solves nothing*. A move like this would signal to everyone they're interested in being part of the solution, and to their shareholders that they want to expand into an underserved market segment instead. That's always where they've had the most success.

*Fsck cars*, we need innovation. Literally; we don't have a choice. Do it, Apple. You know you want to.
