---
title: "Goodbye, Amelia Watson 🔎 💛"
date: "2024-09-21T08:23:54+10:00"
thumb: "https://rubenerd.com/files/2024/clara-ame.jpg"
year: "2024"
category: Media
tag:
- amelia-watson
- goodbye
- hololive
location: Sydney
---
<figure><p><img src="https://rubenerd.com/files/2024/watson-goodbye@1x.jpg" alt="Watching her stream this morning" srcset="https://rubenerd.com/files/2024/watson-goodbye@2x.jpg 2x" style="width:500px; height:280px;" /></p></figure>

[Amelia Watson](https://www.youtube.com/@WatsonAmelia/videos) has [announced her soft retirement](https://hololivepro.com/news_en/20240920-01-115/) from Hololive EN Myth:

> Watson Amelia, a member of the VTuber group hololive English, will conclude all of her channel activities on September 30th, 2024. [...] Though her channel activities will end, Watson Amelia will remain an affiliate of hololive production moving forward.

I can't put too fine a point on this. Ame's streams were [how Clara and I got through 2020](https://rubenerd.com/live-lessons-from-hololives-amelia/). Her adventures in Minecraft got us to (finally!) play the game, which we've now strung together with our massive *Watson Rail* line of our own. Her [collaboration with Pavolia Reine](https://rubenerd.com/the-ame-and-reine/) was how I really got to know her. I *still* watch her [Eurotruck Simulator](https://www.youtube.com/watch?v=Q5TRfy_zyNY) episode when I'm tired or depressed. Ame struck that balance of chaos and compassion, and silly and smart, in a way nobody else did. Her mannarisms are firmly entrenched in Clara's and my family folklore. I could keep going! **HIC!**

I couldn't put it better than [Clara did this morning](https://x.com/kirisviel/status/1837150101086552116):

> I actually rarely drew Ame since I didn’t think I could get her hair and hairpin quite right…. asdgwj
> 
> My first oshi and the one who led me and @Rubenerd down the vtuber/hololive rabbithole! 💛
> 
> The little ways you have influenced us remain in the fabric of our lives.

<figure><p><img src="https://rubenerd.com/files/2024/clara-ame.jpg" alt="Clara's drawing of Ame this morning" style="width:500px;" /></p></figure>
