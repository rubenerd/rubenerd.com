---
title: "Stopping MacBook screens from sleeping"
date: "2024-08-27T09:26:53+10:00"
year: "2024"
category: Software
tag:
- apple
- guides
- macos
location: Sydney
---
Say you're at a data centre and tailing logs on your work MacBook while you do other things. How do you stop the display from sleeping after a few minutes of inactivity?

If it was possible graphically before, I haven't been able to find it. Every guide online either confidently references a part of System Preferences or Settings that no longer exists, or the feature doesn't work as advertised.

Enter the `caffeinate(1)` command:

    $ caffeinate -d

This prevents your MacBook screen from sleeping. *YAY!*

There are a few other options you can set, such as `-i` and `-m` that prevent the system and disks from sleeping respectfully. You can also specify a command after those options to only stop the machine from sleeping while that process executes.

I've been using Unix-based macOS since the original Mac OS X 10.0 betas, and this was the first time I learned of it. I guess there's a person born every minute who hasn't watched Pavolia Reine, tried durian ice cream, or knew there was such a command.

As an aside, it's interesting how caffeine affects people in different ways. In the evenings it doesn't make me feel more awake, just anxious. Though I suppose it does trigger insomnia later in the night, so it *does* keep me awake. But then I knew people in uni who could drink three Red Bulls and coffee, take an exam, then fall asleep as soon as they got home. I was... not one of those people.
