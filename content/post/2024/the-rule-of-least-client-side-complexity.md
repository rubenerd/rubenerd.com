---
title: "The rule of least client-side complexity"
date: "2024-12-03T08:12:54+11:00"
abstract: "Code that runs on the server can be fully costed."
year: "2024"
category: Internet
tag:
- design
- externalities
- webdev
location: Sydney
---
I read this section on Alex Russell's [recent post on web "frameworkism"](https://infrequently.org/2024/11/if-not-react-then-what), and thought it was brilliant:

> Code that runs on the server can be fully costed. Performance and availability of server-side systems are under the control of the provisioning organisation, and latency can be actively managed by developers and DevOps engineers.
>
> Code that runs on the client, by contrast, is running on The Devil's Computer. Nothing about the experienced latency, client resources, or even available APIs are under the developer's control.

Alex's footnote explaining [The Devil's Computer](https://infrequently.org/2024/11/if-not-react-then-what#fn-the-devils-computer-2) is also worth reading. I don't envy the monumental task web developers face presenting consistency across a multitude of endpoints, though they don't do themselves any favours when they "clog those narrow pipes with amounts of heavyweight JavaScript that are incompatible with consistently delivering good user experience".

There's another practical concern related to that first point as well. The costs of client-side code are borne by the client, whether it be in data usage charges, battery life, or even with our privacy thanks to the prevalence of third-party frameworks, embeds, and assets. It's the same with AI output and blockchains; those who implement them don't pay these downstream costs.

We have a term for this: sillyness! Also, externalities.

