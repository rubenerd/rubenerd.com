---
title: "Stack Overflow and LLM licencing"
date: "2024-05-11T09:25:37+10:00"
year: "2024"
category: Ethics
tag:
- generative-ai
- law
location: Sydney
---
*(I took this post back down after I started copping some abuse. I've put it back in part with encouragement from Michael Wheeler and [Techrights](http://techrights.org/n/2024/05/12/Online_Bullying_Trying_to_Make_People_Unhappy.shtml). Bad people don't get to dictate how I talk. Thanks for your understanding).*

Last year a German professor reached out to ask if he could train a large-language model on my words for a class. I replied that while I found these sorts of LLMs distasteful, my licences didn't preclude him from doing it. The only caveat was that the 3-Clause BSD Licence, and Creative Commons Attribution, require derivative works to adequately attribute sources.

Sure enough, he used it in his class, and he provided screenshots demonstrating that my site, among many others, were attributed on output pages. He even did it properly, by referencing the URL, my name, and the date. See, it's not hard!

I was tempted to write a quick post about it at the time, but decided against it because I considered such a position self-evident and uninteresting. This professor had the professional courtesy to still ask me permission, which is something most people and companies don't bother with. But ultimately it boiled down to having a licence requirement with which someone complied. *Yay!*

And then I saw the news that Stack Overflow has partnered with a large LLM company to train their data against people's work. Unlike that professor, the announcement was coached in marketing buzzwords in lieu of seeking the permission of those who'd contributed to the site.

This was an... interesting tactic, for a few reasons.

First, credit to them for being transparent and upfront, something that's been sorely lacking elsewhere. [Automattic were caught](https://www.404media.co/tumblr-and-wordpress-to-sell-users-data-to-train-ai-tools/) selling access to their customer data for training without disclosure. They later buried a checkbox permitting people to opt-out, because they knew almost nobody would voluntarily opt-in. That's a bit of a theme, isn't it?

Likewise, fatalists were quick to point out that the web has already been trawled by LLMs, so SO were doing nothing different. Not holding companies to standards because others have set an irresponsible precedent doesn't strike me as a compelling ethical or legal stance, so I dismiss it.

But that gets us to some really interesting points. SO have demonstrated <span style="text-decoration:line-through">three</span> four things with this announcement:

* They think technical answers from an LLM will be accurate, timely, and reliable. I don't know if they will be, though admittedly I don't really care. If the quality goes down, it'll be Experts-Exchanged by something else.

* They think they're legally in the clear.

* They think the community response will be positive (I don't believe this for a second), or they can ride out any negativity.

The output of an LLM is unambiguously a derivative work, at least to me. I don't buy the argument that LLMs are somehow "inspired" the same way artists and writers are, because that's not how entropy works. Writers generate it, LLMs consume it. One need only see how LLMs fall apart from ingesting output from other generative AIs to see this in action.

But I digress. At their core, these models wouldn't exist without the inputs upon which they were trained. Therefore, use of such data would need to comply with the licences under which such words were released. It's... interesting to think that there are LLMs out there today that have violated our terms, and are charging people for the privilege.

In the case of SO, writers contributed their works under Creative Commons Attribution Share-Alike. This places further restrictions and requirements on derivative works, something people defending SO attempt to dance around (we're nerds, of course we don't have any sick moves). This would have to be tested in court though, so even if SO considered their legal obligations and responsibilities, they probably suspect such action is unlikely.

But all of this gets to the core point I want to address here. This cavalier attitude towards those who's works made their site a success is what I find most disappointing. Even if SO were in the clear&mdash;and evidently enough people think so&mdash;is it an appropriate and ethical thing to do?

I guess it's a numbers game. Will a sufficient number of people be put off and stop contributing to the site, leading to stale models that can't answer questions as well as SO expects them to? Maybe, maybe not. Enough people may continue to see value in the site that they'll keep contributing. Maybe the LLM tools really will usher in a new era of disruptive, nuanced paradigm-synergistic disruption... synergies. Nuance! Paradigms!

I stopped logging into the Stack Exchange network years ago when my question about IPMI was deemed out of scope for Server Fault. Really! But I'll be following how the story develops; it's another fascinating chapter in the struggle between platforms and people. At the risk of putting too fine a point on it, the future is being decided now.

*Update:* [David](https://eigenmagic.net/@DDsD/112426582437793938) and [Max](https://toet.dnzm.nl/@max/statuses/01HXNWVJXBH3EVPSZ52V8AY702) discussed the another reason behind this move: the site has seen a drop in traffic, and they think this will shore up their finances.
