---
title: "Spam calls in Australia on the rise"
date: "2024-06-15T09:26:55+10:00"
year: "2024"
category: Thoughts
tag:
- scams
- spam
location: Sydney
---
I don't have any concrete evidence for this, only my own personal experience. But this year has been *ridiculous* for spam calls and messages.

The calls usually go like this:

1. You get a call from a spoofed landline number, usually Victoria for some reason. They tend to call after 17:00, which is frustrating because I have to assume it's a client with an urgent request if I'm on pager duty (we have a strict validation process where I call them back on the number registered on their account).

2. You answer the call, and aren't greeted immediately. Instead, it's delayed while someone in the automated call centre realises a mark has responded to their automated dialling, and have it delegated to one of the scammers. This makes them trivial to detect.

3. When the person finally answers, it's almost always for a service or company for which I don't have an account. They ask vague questions, and become hostile in ways a professional call centre operator wouldn't when you mess with them.

4. If you hang up on them immediately, they call back, sometimes upwards of three times. I add their number to my ever-increasing and futile block list, and move on.

The government's energy rebate has been a popular topic to bring up. The scammers will claim eligibility depends on signing up to a new energy plan&hellip; and by the way, what credit card should they use? And so on. I guarantee this playbook must be working for them, because I've had it told to me a few times. A bit grim to think about.

The scam messages seem to mostly regard toll roads, which is amusing for someone who doesn't drive, or know what an "Eastlink" is:

> Eastlink: Avoid incurring fines. Your toll debt should always be paid off by the due date. $OBVIOUS-FAKE-URL

The perverse thing is, I almost empathise with people doing this. Not the operators themselves, but the mules working those scam call centres aren't living their best lives. Being in a *regular* call centre is a tough, unforgiving gig where you're ripe for abuse from some of the worst people. Likewise, you don't become a scammer when things are going well for you. When people like me say robust social security is financially responsible, this is one of the exact scenarios I talk about. Society always pays, it's just a question of how.

Of course, an excuse isn't a (good) reason. These scammers disproportionately rope in older people and the less tech literate, who also skew towards having less money. If they're being scammers to *take down the system*, going after other victims is completely arse backwards. Even my dad, a scientist whom society assumes must therefore be rational, was almost taken by one of these people. It can affect anyone.

I've read all the usual arguments that education is key, but this clearly hasn't worked after decades. I dunno, do we need to start charging for all outbound calls, irrespective of whether they're picked up? I bet phone companies would *love* that!
