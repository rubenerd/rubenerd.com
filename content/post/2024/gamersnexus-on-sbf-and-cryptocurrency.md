---
title: "GamersNexus on SBF and cryptocurrency"
date: "2024-04-05T15:57:49+11:00"
thumb: "https://rubenerd.com/files/2024/gamersnexus-sbf-03@1x.jpg"
year: "2024"
category: Ethics
tag:
- blockchain
- gamersnexus
- cryptocurrency
- quotes
- scams
- video
location: Sydney
---
From their [latest Hardware News recap](https://www.youtube.com/watch?v=5ghgTIBzuRA "HW News - Intel Battlemage Appears, Open Source GPU, Xbox Handheld Rumors, $1400 Monitor") of the week.

<figure><p><img src="https://rubenerd.com/files/2024/gamersnexus-sbf-01@1x.jpg" alt="The judge stated the defence was misleading, logically flawed, and speculative." srcset="https://rubenerd.com/files/2024/gamersnexus-sbf-01@2x.jpg 2x" style="width:500px; height:281px;" /><br /><img src="https://rubenerd.com/files/2024/gamersnexus-sbf-02@1x.jpg" alt="..." srcset="https://rubenerd.com/files/2024/gamersnexus-sbf-02@2x.jpg 2x" style="width:500px; height:281px;" /><br /><img src="https://rubenerd.com/files/2024/gamersnexus-sbf-03@1x.jpg" alt="Is he talking about crypto[currency]..." srcset="https://rubenerd.com/files/2024/gamersnexus-sbf-03@2x.jpg 2x" style="width:500px; height:281px;" /><br /><img src="https://rubenerd.com/files/2024/gamersnexus-sbf-04@1x.jpg" alt="Oh, he was talking about the defence." srcset="https://rubenerd.com/files/2024/gamersnexus-sbf-04@2x.jpg 2x" style="width:500px; height:281px;" /></figure>
