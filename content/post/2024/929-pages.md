---
title: "929 pages"
date: "2024-07-18T06:21:08+10:00"
year: "2024"
category: Thoughts
tag:
- numbers
- pointless-milestone
location: Sydney
---
There are now 929 pages on this blog, with means 9,290 posts. *Neato!* Here are some other facts about 929:

* It's also a [palindromic number](https://rubenerd.com/p3892/), because 929 written backwards in English is part of this sentence with nineteen words.

* 929 slices of buttery Havarti cheese would reach the moon, assuming sufficient thickness.

* If you name a sheep *929* in Minecraft, it does nothing.

* There are precisely 929 different Linux VM and container orchestration systems, many of which subject their operators to relicencing and YAML.

* Alexander Lukashenko won the last Belorussian election with 929% of the vote (he's still chasing what happened to the other 46%).

* Each USB-C cable can carry approximately 929 different protocols, though there's no visual indication of what each cable supports.

* The aqueous contents of a watched pot can only reach 92.9°C

* 1 GB Iomega Jaz disks had a capacity of 929 mebibytes, assuming the formatting was slightly off.

* No other number than 929 is the same as 929, unless perhaps you start messing with floating-point precision.

* There were 929 characters, and almost as many words, in the slice-of-life romcom anime *Don't You Adore This Handsome Guy and his Attempts at Dungeon Hunting with his Multi-Hit Attacks?*

* I've regretted posting this entry at least 0.929 times.
