---
title: "Using git rm on files you already deleted"
date: "2024-11-05T09:42:39+11:00"
year: "2024"
category: Software
tag:
- git
- things-you-already-know-unless-you-dont
location: Sydney
---
In today's installment of *[things you already know, unless you don’t](https://rubenerd.com/tag/things-you-already-know-unless-you-dont)*, we're looking at dealing with deleted files in git.

Say you've deleted a file in a repository, either on purpose or by mistake, then run `git status`:

    On branch trunk
    Your branch is up to date with 'origin/trunk'.
       
    Changes not staged for commit:
        deleted: DELETEME.TXT

Whoops! How do you deal with this?

For the longest time I'd restore the file, either with a manual backup, or using `git checkout`. Because you can't tell git to delete a file that was already deleted, right?

Wrong! Turns out you don't need to do this before removing it from the repository; git can still delete it just fine:

    $ git rm DELETEME.TXT

git will issue this delete in the repository, and not return any feedback. But you can tell this worked by trying to delete a file that *never* existed in the repo:

    $ git rm DOES_NOT_EXIST.TXT  
    ==> fatal: pathspec 'DOES_NOT_EXIST.TXT' did not match any files

I'd still recommend deleting the files within git, but this works in a pinch.
