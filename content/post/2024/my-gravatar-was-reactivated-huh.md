---
title: "My Gravatar was reactivated… huh"
date: "2024-08-10T09:20:47+10:00"
year: "2024"
category: Ethics
tag:
- blockchain
- scams
- spam
location: Sydney
---
A week ago I encouraged everyone to [cancel their Gravatars](https://rubenerd.com/cancel-your-gravatar-now/), on account of Automattic's footgun pivot to blockchain.

Well I just got this email:

> **Welcome to Gravatar.com**
>
> Your email now has a profile!

Wait, what? But then I got this when trying to log in:

> **Enable Account**
> 
> Your Gravatar account is currently disabled.
> 
> This page allows you to reenable your account in order to continue to use it.

That means I have a profile again, but on a disabled account? Clear as mud!

I have no idea what's going on. Maybe someone at Automattic saw my post, and thought they'd have a bit of fun. I don't blame them, given where they work.
