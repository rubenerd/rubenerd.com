---
title: "Revisiting self-driving cars"
date: "2024-04-05T13:18:34+11:00"
year: "2024"
category: Thoughts
tag:
- cars
- public-transport
- video
location: Sydney
---
Self-driving cars are one of those technologies that flew under my [LIDAR](https://en.wikipedia.org/wiki/Lidar#Autonomous_vehicles) (thank you) until I started seeing their social effects. I'd previously lumped them in with hyperloops, cryptocurrency, and generative AI as mediocre tech in search of problems. Or at least, gullible investors with short memories.

It was a [Veritasium video 📺](https://www.youtube.com/watch?v=yjztvddhZmI "Why you should want driverless cars on roads now") that first got me interested in the technology, though more for its surprisingly-lopsided approach to discussing it. I've shared Derek's videos before; they're usually excellent. But three things seemed off:

* His comparison between self-driving cars with lifts and autopilot. Both these systems operate in controlled environments with far fewer parameters than a self-driving car. Even as a form of social commentary, the argument seemed weak.

* His argument that self-driving cars won't make the mistakes of a human driver, without discussing the mistakes self-driving cars make (we'll come back to that).

* His conclusion that self-driving cars could reclaim "lost value" and solve traffic, without mentioning they all require the same expensive and damaging road infrastructure that's hostile to anyone outside a car.

I had a long draft where I sought to debunk these points, but then I saw an excellent [Tom Nicolas video](https://www.youtube.com/watch?v=CM0aohBfUTc "Tom Nicholas: When Science Meets Misinformation") doing exactly this. Tom provided the missing context to the stats Derek used, advocated for public transport, and explained how the advertising relationship with Waymo twisted Veritasim's video into an advertorial.

Such discussions aren't limited to science educators, as I talked about in August last year about [autonomous vehicles causing traffic](https://rubenerd.com/ride-sharing-causing-not-solving-traffic/ "Ride sharing and autonomous vehicles cause traffic") rather than reducing it. There's a concerted campaign to convince the public that self-driving cars are safe by drawing comparisons to flawed humans, as though this absolves the cars themselves.

This worries me as a systems engineer, because we're being primed to accept undefined behavior. I mentioned last year all the [new issues they're introducing in LA](https://rubenerd.com/driverless-cars/ "Driverless cars"), but it took a recent episode of The Urbanist Agenda to really [crystallise my concerns](https://www.youtube.com/watch?v=htPRgC-dua0)\:

> **Jason:** The whole pitch is they're safer than human drivers, and there's evidence to suggest that. But what I find interesting [...] is there's a whole bunch of situations where these cars get into that humans would never do either.
>
> **Jon:** It's not that they're perfect, they're different. [...] They're going to bring in a bunch of new problems. I'm a software developer! Things go wrong, and these AI systems are extremely difficult to debug.

They also touch on how the public are being trained to be beta testers without realising it, and how these autonomous systems are fooled by [coning](https://www.npr.org/2023/08/26/1195695051/driverless-cars-san-francisco-waymo-cruise), where protestors immobilise cars by placing traffic cones on their bonnets.

All this is to say, is a phrase with five words. This is by no means a solved problem, despite what some technical enthusiasts and advertisers may choose to assert. I also agree with Jason that, again, the real answer to urban traffic problems is to provide compelling alternatives to driving. *One more lane bro* doesn't cut it, whether it's a smoke machine, an EV, or a self-driving ambulance blocker.
