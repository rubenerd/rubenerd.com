---
title: "Copy/paste plain text should be the default"
date: "2024-03-10T12:14:18+11:00"
year: "2024"
category: Software
tag:
- comments
- design
- empathy
- ergonomics
location: Sydney
---
I love (abusing) the term *Litmus Test* as a metaphor for evaluating reactions in other contexts. Though technically my favourite indicator in chemistry was Bromothymol Blue, to the point where I did one of my papers on it, and even nearly *named this blog for it* in 2004! I still regret that; it's such a supremely satisfying name, and the gorgeous colour palette literally makes itself, as [GregorTrefalt](https://commons.wikimedia.org/wiki/File:Bromothymol_blue_colors_at_different_pH.png) demonstrates:

<figure><p><img src="https://rubenerd.com/files/2024/Bromothymol_blue_colors_at_different_pH.jpg" alt="Different colors of bromothymol blue at marked pH conditions." style="width:294px;" /></p></figure>

But I digress. A *Bromothymol Blue Test* I like performing is seeing is how people react to the idea of copy/paste being plain text by default, which it should be. I have an intelligent, charismatic, and above all modest audience of readers here, so I can only assume you all agree with this sentiment.

Pasting with formatting is fraught with complications, errors, and additional cleanup work that combine to negate any residual benefit pasting with formatting provides in all but rare circumstances. If I wanted pasted text to render like a 5G flat earth conspiracy leaflet, I'd fire up Creative Writer. That reminds me, I need to install that on my 486.

Mention this however, and you'll receive an interesting range of responses that fall under different shades of colour on our indicator. We have:

* People who **agree**, and may make references to providing their weapons *Lord of the Rings* style. These are my people, and are the deepest shade of blue on account of being the most based. Pardon, being a strong base.

* People who say it's a **non-issue** because you can invoke an alternative shortcut command, change something, install some random tool, or spin a rubber chicken above your head. Even if these didn't make huge and self-defeating assumptions about someone's operating environment, it willfully misses the point of these actions being the *default*. These responses both frustrate and *deeply* fascinate me.

* People who **Tone Police**, or otherwise attack the person making the point. Rich text paste isn't the worst thing ever, haven't you heard of COVID-19? In my weaker moments I've been tempted to ask why they're not thinking about heart disease, problem gambling, or Kubernetes.

* People who **pretend not to know what the issue is**. They may ask why it's frustrating under the guise of *just asking questions*. I say *pretend* not to know, because I value their intelligence more than they do.

* People who **disagree** on its utility. I find these perspectives the only other useful ones, oddly enough. For example, someone reminded me that preserving a table from a document to a spreadsheet *is* useful. I maintain that this shouldn't be the default though.

There are probably [Dan Hon Response Cards](https://rubenerd.com/dan-hons-resources-for-managing-replies/ "Dan Hon’s resources for managing replies") for each of these. But the *predictability* of these responses still interest me. You could almost make a Bingo card for it.
