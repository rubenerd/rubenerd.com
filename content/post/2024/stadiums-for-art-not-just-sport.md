---
title: "Stadiums for art, not just sport"
date: "2024-02-23T13:06:23+11:00"
year: "2024"
category: Media
tag:
- art
- economics
- music
- paul-mccartney
- sport
location: Sydney
---
I'll admit to being one of those people who roll their eyes at billion dollar stadium upgrades, especially given the steep opportunity cost. But [Jack Snape may have proved me wrong](https://www.theguardian.com/sport/2024/feb/23/taylor-swift-australian-eras-tour-crowds-sport-supremacy-mcg "Universal appeal of Taylor Swift and other superstars threaten sport’s supremacy at Australian stadiums ")\:

> A total of 96,000 people turned up on Friday. Then 96,000 on Saturday. And 96,000 on Sunday, highlighting the increasing popularity of arena spectaculars. Globally, shows in the top 100 stadiums grossed US$3.6bn ($5.5bn) in 2023 according to live music trade publication Pollstar, up 35% on the year before and more than double 2019, the year before the pandemic set in.
> 
> With every passing concert – and every glowing wristband handed out – the role of stadiums in the community is slowly being transformed, according to those working in the sector. From exorbitant athletic indulgences, or worse, white elephants, large venues are aspiring to be cultural hubs and economic engine rooms for cosmopolitan cities.

Jack is referring to Taylor Swift's current Australian tour here, though Clara and I had a similar experience [seeing Paul McCartney in Sydney](https://rubenerd.com/seeing-paul-mccartney-live-in-sydney/ "Seeing Paul McCartney live in Sydney! ♡") last year. **AAAAAAAAA we saw Paul McCartney!** But I digress.

Seeing these as entertainment and cultural venues puts their functions and societal role in a completely different light. These aren't just catherdrals to sport, they're for everyone.

This raises some interesting questions. What can stadium desginers do to better accomodate a range of cultural activities, whether it be someone projecting a ball or their voice? Do we even call these *stadiums* anymore? How do we designate priorities, or schedule conflicting uses?

The Romans had this figured out thousands of years ago, along with concrete and numbers. Wait, scratch number III.
