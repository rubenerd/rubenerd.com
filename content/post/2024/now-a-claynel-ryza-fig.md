---
title: "Now a 1:7 Claynel Ryza fig exists! Also acrylic stands"
date: "2024-04-12T09:32:34+10:00"
thumb: "https://rubenerd.com/files/2024/FIGURE-167600_12@1x.jpg"
year: "2024"
category: Anime
tag:
- acrylic-stands
- anime-figs
- atelier-ryza
location: Sydney
---
I've never heard of Claynel before, but they've already manufactured some impressive figs, including Clara's favourite *Hololive JP* character [Kanata](https://myfigurecollection.net/item/1522987), and the legendary [Takina](https://myfigurecollection.net/item/1781835) and [Chisato](https://myfigurecollection.net/item/1781834) duo from *Lycrois Recoil*.

This rendition of Ryza from her namesake Atelier franchise is adorable, and a reminder that I still, **still** haven't blogged about those games yet.

<figure><p><img src="https://rubenerd.com/files/2024/FIGURE-167600_12@1x.jpg" alt="" srcset="https://rubenerd.com/files/2024/FIGURE-167600_12@2x.jpg 2x" style="width:400px;" /><br /><img src="https://rubenerd.com/files/2024/FIGURE-167600_11@1x.jpg" alt="" srcset="https://rubenerd.com/files/2024/FIGURE-167600_11@2x.jpg 2x" style="width:285px;" /></p></figure>

She was scuplted by ZP1 whom I'm also unfamiliar with, but they designed a [garage kit of Chloe](https://myfigurecollection.net/item/1562043), my favourite Holo X character. I'm going to close all these tabs before I'm tempted to do something financially irresponsible.

The good news for people with little space and finances, Gust also have a line of those acrylic stands. Problem is, these also then end up taking up an entire shelf themselves. Though for fans of Lent too (right), this is likely all we're going to get alas.

<figure><p><img src="https://rubenerd.com/files/2024/00043933962@1x.jpg" alt="Lent and Ryza as acrylic stands" srcset="https://rubenerd.com/files/2024/00043933962@2x.jpg 2x" style="width:500px;" /></p></figure>
