---
title: "Visiting Titanic: The Artefact Exhibition!"
date: "2024-03-29T08:46:14+11:00"
thumb: "melbourne-titanic-telegraph@1x.jpg"
year: "2024"
category: Travel
tag:
- ocean-liners
- titanic
location: Melbourne
---
I've been fascinated with [ocean liners](https://rubenerd.com/tag/ocean-liners/ "View posts tagged with ocean liner") and early twentieth century history since I was in primary school, as I've posted again multiple times recently. Clara and I had penciled in a day to wander the [Melbourne Museum](https://museumsvictoria.com.au/melbournemuseum/), but when we heard there was a Titanic exhibit!? *Get out of here!*

<figure><p><img src="https://rubenerd.com/files/2024/melbourne-titanic-flags@1x.jpg" alt="Banners outside the museum for the Titanic exhibition" srcset="https://rubenerd.com/files/2024/melbourne-titanic-flags@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

The banners outside the museum say the exhibit had been extended to the 20th of April, and we soon discovered why. We entered the museum and went to the ticket counter, and were told we were lucky enough to *just* make their 12:15 viewing. They've since completely sold out of their Easter long weekend day tickets, though you can still book evening viewing as of writing. Clearly there's a wider interest in the history of this liner and its people than I would have thought.

<figure><p><img src="https://rubenerd.com/files/2024/melbourne-titanic-boarding@1x.jpg" alt="" srcset="https://rubenerd.com/files/2024/melbourne-titanic-boarding@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

Once we'd signed in through the large RMS *Titanic* archway to the right of the museum entrance, we were given these cute White Star boarding passes, and a passenger's details where we could learn more about the people who sailed on this ship in 1912. I thought it was a lovely touch, and a way to humanise what's often viewed as merely disaster porn (ask me what I think of the *Lusitania* in the same way).

<figure><p><img src="https://rubenerd.com/files/2024/melbourne-titanic-artefacts@1x.jpg" alt="A selection of artefacts, described below." srcset="https://rubenerd.com/files/2024/melbourne-titanic-artefacts@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

The exhibit is arranged into a few different halls, with the first depicting the conception and construction of the ship. You're shown photos from the huge Harland and Wolff shipyards, portraits of Lord Pirrie and J Bruce Ismay, and deck-by-deck schematics of the liner. But the stars are the recovered artefacts themselves: a set of real hull plate rivets, a deck lamp, gears, a barometer, and a boiler thermometer.

My surprise favourite was the coffee urn setup, which was used to serve the world's greatest beverage to passengers!

<figure><p><img src="https://rubenerd.com/files/2024/melbourne-titanic-coffee@1x.jpg" alt="A coffee urn and large, partially reconstructed vessel." srcset="https://rubenerd.com/files/2024/melbourne-titanic-coffee@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

In addition to the artefacts themselves, various rooms and hallways were mocked up to give you a sense of how it would have felt wandering the ship. They were clearly simplified from the originals, but I thought they were executed well! There was a first class hallway, a stateroom, even the distinctive green and white walls of the famed Café Parisian.

<figure><p><img src="https://rubenerd.com/files/2024/melbourne-titanic-stairs@1x.jpg" alt="An awkward gentleman walking down a mock Grand Staircase" srcset="https://rubenerd.com/files/2024/melbourne-titanic-stairs@2x.jpg 2x" style="width:375px; height:500px;" /></p></figure>

But of course the *Grand Staircase* stole the show. I'm putting one of my usual awkward expressions here because dozens of people were looking on! But even on this stage you could feel the gravity of the event. There were people who graced these real stairs who never got to know anywhere else, to say nothing of all the third class passengers below decks who didn't stand a chance.

<figure><p><img src="https://rubenerd.com/files/2024/melbourne-titanic-crockery@1x.jpg" alt="A selection of third class crocerky with the White Star logo" srcset="https://rubenerd.com/files/2024/melbourne-titanic-crockery@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

There were so many more recovered artefacts on show than I could have dreamed of; everything from watches and personal papers, to a literal block of boiler coal. But it was the tableware and cooking vessels that hit the closest to home for me. The exhibit showcased the finery of first class, some of the beautiful second class china, and the pedestrian crockery for third class (above) stamped with the White Star logo to "deter theft". Maybe it's the seeming impossibility that so much fragile table wear could survive such a disaster, or the thoughts of people using these in their daily lives without so much a second thought. All of this crashed down and sat at the bottom of the Atlantic for close to a hundred years.

<figure><p><img src="https://rubenerd.com/files/2024/melbourne-titanic-telegraph@1x.jpg" alt="Photo showing one of Titanic's bridge telegraph" srcset="https://rubenerd.com/files/2024/melbourne-titanic-telegraph@2x.jpg 2x" style="width:375px; height:500px;" /></p></figure>

At the other end of the scale we had the other piece of Edwardian engineering I was really hoping they'd have: an original bridge telegraph. These were used to send speed instructions to the engine rooms, which would have included that last command for full reverse on the fateful night of the sinking. This one was pictured in the third hall that described the events leading up to the iceberg encounter.

<figure><p><img src="https://rubenerd.com/files/2024/melbourne-titanic-tag@1x.jpg" alt="An original luggage tag." srcset="https://rubenerd.com/files/2024/melbourne-titanic-tag@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

It's strange, I've been reading about ships like Titanic for decades at this stage. Clara and I even got to go see [the *Queen Mary* in Long Beach](https://rubenerd.com/visiting-the-rms-queen-mary/ "Visiting the RMS Queen Mary"), which was another childhood dream. But seeing these real items from *Titanic* added a whole other dimension to my understanding and feelings around it. It was a privilege to see them all.

