---
title: "Avoid using pie charts"
date: "2024-01-09T11:07:57+11:00"
year: "2024"
category: Media
tag:
- data
- design
- journalism
- mathematics
- statistics
location: Sydney
---
I love the aesthetic appeal of pie charts, especially when they're [meme-related](https://duckduckgo.com/?q=pyramid+pie+chart "DuckDuckGo search for the infamous Pyramid Pie Chart"). But Adrian Barnett and Victor Oguoma at The Conversation reminds us all that they're [rarely useful, and usually misleading](https://theconversation.com/heres-why-you-should-almost-never-use-a-pie-chart-for-your-data-214576 "Here’s why you should (almost) never use a pie chart for your data")\:

> Overall, it is best to use pie charts sparingly, especially when there is a more “digestible” alternative – the bar chart.
> 
> Whenever we see pie charts, we think one of two things: their creators don’t know what they’re doing, or they know what they are doing and are deliberately trying to mislead.

They link to this example by [User:Schutz on Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Piecharts.svg "Pie chart versus bar graph example by User:Schutz"). Try this for yourself: are you able to quickly make out which segment is the second-largest in these pie charts, or does it only become apparent when looking at the bar graph?

<figure><p><img src="https://rubenerd.com/files/2024/barpie.svg" alt="Pie chart versus bar graph example by Schutz." style="width:500px;" /></p></figure>

Pie charts place the burden of comparison on the reader. I find myself trying to rearrange and overlap the segments in my head to figure out how large each is in relation to the others. Bar graphs do all this work for us.

I agree with their conclusion: pie charts are best left for limited datasets where the delta between points are easily discernible when presented visually. But even then, I'm thinking I'd just stick with bar graphs.

This is also further proof why technical accuracy alone is insufficient when writing and having discussions. As the authors conclude:

> A graphical summary aims to easily and quickly communicate the data.

If your method of communication fails to do this, your accurate and attractive pie chart lacks meaning, and is therefore pointless. Like a circle, now that I think about it.
