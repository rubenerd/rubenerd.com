---
title: "Another quick break"
date: "2024-05-30T08:00:15+10:00"
year: "2024"
category: Thoughts
tag:
- personal
location: Sydney
---
Taking leave again for a bit to sort out some big things, and smell the flowers. Thanks everyone, and catch you soon. 👋 💐
