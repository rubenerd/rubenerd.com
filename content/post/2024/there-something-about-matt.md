---
title: "There’s something about Matt"
date: "2024-09-26T08:42:02+10:00"
year: "2024"
category: Ethics
tag:
- wordpress
location: Sydney
---
**2013:**

* [Me!](https://rubenerd.com/my-own-wordpress-to-jekyll-adventure/ "My own WordPress to Jekyll adventure")\: "To me, Matt Mullenweg represents the good of my generation; the antithesis of Mark Zuckerberg in so many ways"

**2024:**

* [TechCrunch](https://techcrunch.com/2024/02/22/tumblr-ceo-publicly-spars-with-trans-user-over-account-ban-revealing-private-account-names-in-the-process/)\: "Tumblr CEO publicly spars with trans user over account ban, revealing private account names in the process"

* [404 Media](https://www.404media.co/tumblr-and-wordpress-to-sell-users-data-to-train-ai-tools/)\: "Tumblr and WordPress to Sell Users’ Data to Train AI Tools"

* [The Register](https://www.theregister.com/2024/09/24/wp_engine_claims_automattic_ceo/)\: "WP Engine hits back after Automattic CEO calls it 'cancer'" [classy –ed]

* [Baldur Bjarnason](https://www.baldurbjarnason.com/2024/rotten-wordpress/)\: "There’s something rotten in the kingdom of Wordpress"

Well, I got that wrong! What a difference a decade makes.
