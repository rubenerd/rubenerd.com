---
title: "Farewell to my first coffee grinder"
date: "2024-12-25T08:51:12+11:00"
abstract: "It got us through COVID lockdowns, and was my intro to freshly-ground coffee. What a difference it makes!"
year: "2024"
category: Hardware
tag:
- coffee
- farewell
location: Sydney
---
Clara and I bought a white [Baratza Encore ESP](https://www.baratza.com/en-au/product/encoretm-esp-zcg495) grinder last month on special, to celebrate moving into our new home. It replaced this no-name blade grinder that had been my daily driver for more than a decade:

<figure><p><img src="https://rubenerd.com/files/2024/first-grinder@1x.jpg" alt="" srcset="https://rubenerd.com/files/2024/first-grinder@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

I bought this for $10 at a big box store back in the early 2010s, around the time I was developing an interest in homebrew coffee. I poured beans into it, pushed the large silver button, and got ground coffee out the other side (later I got a basic set of scales from the same store also pictured above, which has since become our general kitchen scales).

Coffee aficionados aren't fans of these blade grinders because they generate uneven output. Larger pieces are not extracted to their full potential when subjected to hot water, and "fines" add bitterness and grit, which can clog filter papers. This is all true, and I've noticed both a marked increase in sweetness *and* ease of AeroPress plunging since moving to the Baratza.

Here comes the proverbial posterior prognostication: *but...* everything is relative. Coming from pre-ground coffee, the freshly-ground beans coming from this little machine was *night and day* in difference. Fresh beans, even ones from mass-produced roasteries, smelled *incredible*. Much like a low-end SLR can shine with the right glass, even a budget coffee brewing setup can be made *so* much more wonderful by just grinding the beans yourself. Taste is mostly smell, and you add so many aromatics just by making this one change.

Using this budget grinder was fun, but just like my original AeroPress, it was during COVID lockdowns from 2020 and 2021 that it stepped up to the herculean task of grinding between two to five cups a day for us. Those cups of coffee Clara and I shared on the balcony all came through this thing. It sounds silly, but I almost wanted to get a 3D frame for this and the AeroPress; something along the lines of *IN CASE OF EMERGENCY!!*

The good news is, it's staying in the family. My brother in law has shown an interest in upgrading his Nespresso machine to homebrew coffee, so we gave him this alongside a new Clever Coffee Dripper. I hope he gets as much joy out of it as we did.
