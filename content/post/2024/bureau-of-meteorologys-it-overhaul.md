---
title: "Australian Bureau of Meteorology’s IT overhaul"
date: "2024-03-22T10:14:24+11:00"
year: "2024"
category: Software
tag:
- australia
- development
location: Sydney
---
[Peter Hannam in The Guardian Australia](https://www.theguardian.com/australia-news/2024/mar/04/bom-chief-indicates-cost-of-it-overhaul-to-staff-after-refusing-to-disclose-to-senators "‘Nearly a billion dollars’: BoM chief indicates cost of IT overhaul to staff after refusing to disclose to senators")\:

> The CEO and director of the Bureau of Meteorology, Andrew Johnson, revealed to staff the cost of its delayed IT overhaul – one of Australia’s most expensive ever – despite repeatedly telling senators such details must be kept under wraps for cabinet secrecy reasons.
>
> “We’ve heard unconfirmed reports that the amount is around the $1bn mark, which is said to be three times the original budget for this project,” [South Australian senator Barbara Pocock] said. “Why on earth can’t we find out how much an IT project at the weather bureau will cost?

They must be rebuilding on Kubernetes! *Haiyo!*

I kid, but going three times over budget isn't unbelievable at all, if such reports are true. The BoM is a critical government service, and it needs to get things right. Those angry about government expenditure would blanch at what the private sector burns on similarly-sized projects (another reason why I don't buy the idea that running government agencies as "businesses" makes any sense).

The secrecy does raise eyebrows though. It suggests to me that it wasn't scoped properly at the start, something foundational had to be changed mid-build, or someone discovered a flaw that wasn't trivial to change. But why hide it? Is it to avoid naming parties involved?

There's a joke about *forecasting* here somewhere, but I'll avoid it for your benefit and mine.
