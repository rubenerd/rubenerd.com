---
title: "Dark Angel, as discussed by Adrift"
date: "2024-08-02T11:13:19+11:00"
year: "2024"
category: Media
tag:
- shows
location: Sydney
---
I rediscovered Adrift's blog recently, and specifically [this post](https://nambulous.wordpress.com/2020/04/02/if-you-come-to-think-of-it-66/ "If you come to think of it (66)") about an early 2000s TV show I hadn't thought about for *years!* Emphasis added:

> After finishing Star Trek: Picard, I watched all 42 episodes of Dark Angel (yeah yeah, embarrassing – WHAT ISN’T?!?). What’s interesting/funny about this 20 year old show (and I forgot all about that), is **that it takes place in 2020**. I was positively surprised by that and it’s definitely one of the reasons why it felt like the right time to watch it right now.

I never saw the show either, though somehow I remember ads for it being plasted everywhere in Singapore, and on cable TV. It's weird how our childhood brains absorb so much chaff among the wheat, so to speak.

Adrift mentioned one of the big themes of our time that did come to pass, in true metaphorical fashion:

> It got a lot of things right, like surveillance drones flying around and whatnot.

And on world-building, a personal interest of mine too:

> What I like so much about this show, is that it does a solid amount of world-building. Max doesn’t just go off on her crazy adventures (every episode), there are also all these other “normal” people, who have jobs and apartments and so on. Sometimes they hang out after work together. I, as a viewer, understand how this world works and what’s going on. There are streets I recognize. This is the opposite of what STP does, where I never knew where Starfleet (or whomever else) was or what it was doing until it shows up once at the end of the season. It’s great when a fictional world feels and works like a real place anyway. And shitty if it doesn’t. 

