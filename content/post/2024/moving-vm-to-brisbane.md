---
title: "Moving my cloud VM to Brisbane"
date: "2024-01-06T16:12:46+11:00"
year: "2024"
category: Internet
tag:
- architecture
- weblog
- work
location: Sydney
---
Work launched a new data centre PoP in [Brisbane](https://en.wikipedia.org/wiki/Brisbane "Wikipedia article on Brisbane") late last year. Free hosting is a perk of the job, and one I definitely abuse (cough).

As a thank you, I thought I'd take the opportunity to move my stuff up there as an [eating one's dog food](https://en.wikipedia.org/wiki/Eating_your_own_dog_food "Wikipedia article on dogfooding") exercise. My family and I lived up there for about 18 months when I was a kid, so it's also kinda fun knowing I'll soon have some bits and bytes kicking around up there again.

There may some funkiness over the coming day while I cut over. I have a bunch of redundancy when it comes to storage and backups, but I don't bother with a CDN or anything when it comes to delivering the site. For all my overengineering, all I use is a RAM disk and bunch of reverse proxying to a bunch of FreeBSD jails. Maybe one day I should use a proper CDN, but I like to keep things scrappy and in my control :).

Just a heads up if you see some issues over the coming 24-48 hours. The local police have been advised to send out search parties after this time.
