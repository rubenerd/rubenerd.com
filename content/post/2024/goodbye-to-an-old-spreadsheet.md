---
title: "Goodbye to an old spreadsheet"
date: "2024-10-12T10:32:44+10:00"
thumb: "https://rubenerd.com/files/2024/rsp-envelopes.png"
year: "2024"
category: Thoughts
tag:
- finances
- personal
- spreadsheets
location: Sydney
---
This is a silly story to tell for one lowly spreadsheet workbook, but it's been such a dependable and significant part of my life for so many years that I feel like it deserves a send off.

The year was 2015, and I'd come into some money. Technically it was mine to start with; payroll had withheld too much tax, so I got a lump-sum payment. Rather than spend it, I decided to start a proper budget spreadsheet to track savings, investments, and the travel I wanted to do, starting with a nice buffer.

Before this, I'd become enamoured with the envelope method of budgeting popularised by *You Need a Budget*. Along with building and maintaining a buffer, the system involves divvying up your income into virtual envelopes, in which you track every expense. Some of these are paid into and spent each month, like groceries or rent. Others like travel or house deposits may be left to accumulate, so you can spend it in the future.

<figure><p><img src="https://rubenerd.com/files/2024/rsp-envelopes.png" alt="Screenshot showing envelopes"  style="width:640px; height:320px;" /></p></figure>

I ended up creating a [LibreOffice Calc](https://www.libreoffice.org/discover/calc/) workbook analogue of YNAB with a few additional features, and spent a few days sanitising and importing data into this new format. Everything is manually entered, so no expense ever surprised me.

In one tab I tracked every expense, including clearing date, payee, account, and envelope. Another tab held "off-budget" accounts like investments, travel wallets, and superannuation. And finally, the budget tab tracked each month's spending against what I allocated to each envelope. If I overspent in one envelope, I'd make up the shortfall by pulling from a different one. Any remaining funds could then be allocated to savings, or rolled over to the next month for larger expenses.

I've now tracked every expense, payee, and income source since October 2015 in it. In a weird way, it's fun going through years of expenses and seeing payees like "Statue of Liberty Pedestal, NYC", "Pret a Manger, Hong Kong", and "Daily Yamazaki, Osaka" among the regular groceries and rent. I can also see how my spending and savings have evolved over time.

With Clara's and my new home loan, I decided to rebuild it from scratch in another tool which maybe one day I'll share. This includes our shared offset account, merged cards, savings, investment accounts, and travel wallets.

<figure><p><img src="https://rubenerd.com/files/2024/rsp@2x.png" alt="Screenshot from the workbook showing row 17485" style="width:640px; height:162px;" /></p></figure>

Thus, September 2024 was the last month I used this budget workbook. In ten years it has accumulated:

* 17,469 individual budget transactions
* 1,133 off-budget transactions
* in 3,451 envelopes over 119 months

My M1 MacBook Air took about 11 seconds to load and save it each week! Though curiously if I save it as a classic Excel file, it loads on my Dell Pentium III in... wait, only 14 seconds? That's probably worth its own post.

At the risk of being exposed as the sentimental fool I am&mdash;*fool* being the operative word&mdash;I owe a lot to this lowly workbook. I was able to save for a home and travel for a decade thanks to knowing what my financial state was each month. It also made tax time ludecrously simple, and saved my bacon when I was involved in a few financial disputes. It was an anxious income earner's best friend. Thanks ♡
