---
title: "The xz backdoor"
date: "2024-04-02T08:18:12+11:00"
year: "2024"
category: Software
tag:
- compression
- linux
- security
- systemd
location: Sydney
---
I haven't seen this many spooked infosec engineers since Spectre. I was on leave when the news broke, but many of the same lessons the industry didn't take to heart during the OpenSSL debacle have resurfaced. Namely, the security risks posed by:

* Small, overworked, underpaid, underappreciated teams of developers who wield a disproportionate influence on the security of the entire industry (whether by their choice or otherwise).

* Insufficient testing and review processes for code, packages, and dependencies in repositories and operating systems.

* Social engineering, which remains utterly effective regardless of mitigations.

And somewhat, sort-of related:

* Poorly-conceived init software with huge attack surfaces, and security concerns none of us are happy to see validated.

* The relative complexity of xz itself, and why people like me have been [advocating for lzip](https://www.nongnu.org/lzip/xz_inadequate.html) instead for years.

All of these are easy to say, but the solutions to them will be hard. But I'd argue we don't have a choice.

In terms of operational lessons, this is exactly why isolation, permissions, testing, and dependency management are so important, and why we should push back on the business temptations for scope creep. But again, this will require a rethink on how we structure and finance software, which is tricker than throwing `capsicum(4)` on everything and calling it a day. Or a salad.

