---
title: "2× HiDPI > 1× > 1.5×"
date: "2024-08-13T20:36:58+10:00"
year: "2024"
category: Hardware
tag:
- ergonomics
- hidpi
- pc-screen-syndrome
location: Sydney
---
I've talking a lot here about *[PC Screen Syndrome](https://rubenerd.com/tag/pc-screen-syndrome/)*, the phenomena where modern PCs can't compete with displays Apple were shipping more than a decade ago. It's akin to a 486 owner looking at an Amiga and saying *nah, let them eat Hercules! Nobody will ever need more than 640 KiB! The mouse will never catch on!*

Weirder still, the PC industry have slowly responded with something that's somehow even worse. Rather than doing a clean `2:1` doubled upscale which is easy for both vector and raster images, Windows and most OEMs default to interpolating at an awkward `1.5:1` ratio that looks... comically bad. Lines shimmer when you move them around the display as their thickness changes, photos look blocky, and you burn more GPU time for the privilege.

Which gets me to a recent observation. Messing with retrocomputers on my lovely `1024×768` NEC panel, I realised... how much I kind of like the old font smoothing and sharp pixels of old systems. Sure the shapes aren't as buttery smooth or wonderful as my `2:1` HiDPI screen on my FreeBSD workstation, or the display on my MacBook Air, but they're crisp and legible. There's something kinda nostalgic and wonderful about seeing those blocky pixels, especially running something like dvtm on a NetBSD partition on an ancient system.

And on retro VMs, or remote desktops? *Nearest neighbour* is your friend! A `2:1` scale on your host means it's *trivial* to run your old OSs, or a VNC session in a window and have it look as good as it did on its original monitor, because its a simple horizontal and vertical double.

Therefore, I'm calling it. 2× HiDPI is the best, but I prefer 1× over 1.5×.
