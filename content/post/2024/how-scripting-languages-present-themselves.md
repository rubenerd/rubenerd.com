---
title: "How scripting languages present themselves"
date: "2024-12-18T19:28:37+11:00"
abstract: "Going to the sites of Perl, PHP, Python, Ruby, and tcl for the first time in many years."
year: "2024"
category: Internet
tag:
- software
location: Sydney
---
I just realised I haven't been to the landing pages of these languages for *years*. I thought it was interesting how they present themselves.

[Groovy](https://www.groovy-lang.org/)\:

> Apache Groovy is a powerful, optionally typed and dynamic language, with static-typing and static compilation capabilities, for the Java platform aimed at improving developer productivity thanks to a concise, familiar and easy to learn syntax.

[Lua](https://www.lua.org/about.html)\:

> Lua is a powerful, efficient, lightweight, embeddable scripting language. It supports procedural programming, object-oriented programming, functional programming, data-driven programming, and data description.

[Perl](https://www.perl.org)\:

> That's why we love Perl. Flexible & Powerful. Perl is a highly capable, feature-rich programming language with over 36 years of development.

[PHP](https://www.php.net)\:

> A popular general-purpose scripting language that is especially suited to web development. Fast, flexible and pragmatic, PHP powers everything from your blog to the most popular websites in the world.

[Python](https://www.python.org)\:

> Python is a programming language that lets you work quickly and integrate systems more effectively.

[REXX](https://www.ibm.com/docs/en/zos/3.1.0?topic=guide-learning-rexx-language)\:

> The REXX language is a versatile general-purpose programming language that can be used by new and experienced programmers. 

[Rubenerd](#)\:

> Rubenerd is a verbose scripting language with an inelegant but functional design. It achieves this with syntactic coffee instead of sugar, like a gentleman. Redundant concurrency is achieved with redundancy, lah, です.

[Ruby](https://www.ruby-lang.org/en/)\:

> Ruby is... A dynamic, open source programming language with a focus on simplicity and productivity. It has an elegant syntax that is natural to read and easy to write.

[Tcl](https://www.tcl-lang.org)\:

> Tcl (Tool Command Language) is a very powerful but easy to learn dynamic programming language, suitable for a very wide range of uses, including web and desktop applications, networking, administration, testing and many more.

I'd say Groovy is most appealing based purely on its description. I think it's very on brand that Python is so succinct.
