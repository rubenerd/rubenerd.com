---
title: "MariaDB versus PostgreSQL, and dual-stacking"
date: "2024-01-11T17:25:57+11:00"
year: "2024"
category: Software
tag:
- databases
- mariadb
- mysql
- postgres
location: Sydney
---
Two heavyweights have long stood out in the battle for the world's preeminent open-source database. That would be Berkeley DB and SQLite. Or is it Vim and Emacs? To select one for a task isn't to evaluate two systems against your requirements, but to commit your system to The Ordained One. Flame wars of the kind that say one sucks over the other, with all the subtlety of a dropped table in a china shop, are all but inevitable when the discussion comes up on social media, forums, or technical Q&A sites.

I embellish, but in 2023 the two real contenders are Postgres, and the MySQL fork MariaDB. I won't get into the history of how we ended up here, but for the sake of the argument you could probably substitute MariaDB for MySQL where appropriate.

Which one do *you* choose when starting a new project?

We'll get to why that's a loaded question in a moment, but first I'll preface my rambling here by saying that while I moonlighted as a DBA early in my career, I wouldn't consider myself a subject expert. I can Boyce Codd normalise data, suggest ways to optimise indexes, and perform migrations, ingestions, and slightly hairier than normal SQL queries. But what I have real experience in is how they run on systems I maintain.

Traditionally, if you wanted performance, broad compatibility, and easy replication *out of the box*, you'd go with MariaDB. If you wanted reliability, absolute data integrity, something resembling Oracle's PL/SQL, and you were an expert at tuning and optimisation, you went with Postgres. I've read reasonable people in each camp broadly concede these points over the years, with caveats.

These points still (mostly) hold true after fifteen years of running both in a variety of settings; though I've only lost data on a Postgres system, ironically thanks to its replication.

But back to the question about which to use if given the choice. The reality is, the answer is usually provided! Run more than a few trivial systems, and you're bound to encounter software that requires one over the other. You might get away with finding software that all agrees on one, but even then they likely have differing requirements for optimisation, performance, and access (aka, a geospatial database versus a WordPress blog).

Moreover, some software may *claim* to support both MariaDB and Postgres, but it's invariably evident from its architecture and history that one will offer better support than the other. They might use shims (insert ghost reference about ODBC), but it's clear you'll have an easier time using their preferred platform. I learned this the hard way after many years of running both WordPress and MediaWiki on Postgres. Yes, it's possible! No, you shouldn't!

I belabour all this because I mentioned on Mastodon a few months ago that I used to prefer one over the other, but these days I'm forced to dual-stack regardless. ZFS makes this *way* easier, thanks to its ability to define metadata and block sizes so easily. Check out the excellent [OpenZFS wiki](https://openzfs.github.io/openzfs-docs/Performance%20and%20Tuning/Workload%20Tuning.html#database-workloads "Database workload tuning") for performance and tuning tips.

Which one do *I* personally prefer? I feel like I should prefer Postgres, but I do like MariaDB. Interpret that how you will.
