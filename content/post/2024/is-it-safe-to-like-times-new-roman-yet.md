---
title: "Is it safe to like Times and Times New Roman yet?"
date: "2024-02-19T17:12:00+11:00"
year: "2024"
category: Thoughts
tag:
- design
- fonts
- times-new-roman
- typeography
location: Sydney
---
<div style="font-family:'Times New Roman','Liberation Serif',Times;">

<p><em>The Tyranny of the Default</em> is among my favourite infosec lessons. It doesn't matter if you can disable a destructive feature, or enable a protection, or fix a bug. The majority don't change their operating environments, so anything that's not the default isn't used. People who should know better fall afoul 🐓 of this truth <em>constantly</em>, and we all live with the consequences.</p>

<p>That should be its own post, but it’s worth remembering this manifests in other ways too; not least design!</p>

<p>For decades we were trained to think of <em>Times</em>, <em>Times New Roman</em>, <em>Liberation Serif</em>, and other such typefaces as boring. There was a simple reason for this: their use in a document signalled that the writer or designer was too lazy to change it. Being the default saddled it with baggage that had nothing to do with the merits of its design.</p>

<p>There’s a bit of an irony to this. Early printers and word processor packages were keen to demonstrate they'd moved on from the limitations of earlier systems, and <em>Times</em> was a font that demonstrated their extra fidelity and flexibility. But then everyone used it, and the opposite perception endured.</p>

<p>This sucked if you were a fan of <em>Times</em> as a font. What if you thought it was elegant, timeless, maybe even classy? I suspect the late-2000s web design obsession with any serif font that wasn’t <em>Times</em> was born in part due to wanting it, but not its associated stigma. Sure, <em>Comic Sans</em> was bad, but at least the budding typesetter <em>changed it</em>.</p>

<p>But the world has changed. Almost no website uses the browser’s default fonts, not even proverbial <a href="https://dictionary.cambridge.org/dictionary/english/stick-in-the-mud" title="Cambridge dictionary definition">sticks-in-the-mud</a> like me. Tools like Microsoft Word have also since switched to Calibri, so we’ve had milquetoast documents for a decade or so now that don’t use <em>Times</em>.</p>

<p>Is it <em>Time(s)</em> to bring it back? Are we&hellip; are we allowed to?</p>

</div>
