---
title: "How not to leak customer data"
date: "2024-07-14T10:25:02+10:00"
year: "2024"
category: Internet
tag:
- privacy
- security
location: Sydney
---
[Dave Rahardja](https://sfba.social/@drahardja/112775200437415365)\:

> Don’t collect them.
