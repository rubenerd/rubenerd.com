---
title: "An unintentional LED night light"
date: "2024-03-11T09:49:34+11:00"
year: "2024"
category: Hardware
tag:
- ergonomics
- leds
- phones
location: Sydney
---
I got a new wireless charger for the bedside table, so I could stop fiddling with USB cables before going to sleep. The wireless charger on my computer desk works great, so I figured why not?

Well, it was about to give me a reason. I turned off the lights and placed my phone on the pad… and an LED flooded the dark room with a flashing blue glow, thereby proving itself *completely* useless in its intended purpose.

It was so bright, I could make out words on the spines of the books on my shelf. It was so bright, pedestrians ten floors down were blinded as they had a late night stroll. It was so bright, people in Yerevan on the other side of the world asked us to cut it out. At least two of those were false, but I’ll let you decide which.

I tried positioning the side of the device with the LED against the wall to hide it, but it was too powerful to be contained. *I'm a status LED, and I'm going to show you what this device is doing no matter what, damn it!*

I rolled my eyes, unplugged it, and plugged the phone directly to the cable. The device itself then became a momentary projectile as I threw out out of frustration against a bookshelf. Illuminate *this!*

I’ve blogged about this a few times, but why does every single device need such bright status LEDs? Are we all destined to live our lives now with thick black electrical tape stuck over every appliance, charger, and power board light? I don't think I've been to a single person's home lately&mdash;and even a few offices&mdash;that hasn't had such a hack on a device.

Do industrial designers not grok that status lights are orders of magnitude brighter than those pithy red LEDs we all had before? Do they think it makes their devices appear more modern or cool having lights so bright that they interfere with light aircraft, bird migration, and low-Earth orbit satellites?

Maybe this is all a ploy by mental health professionals to get phones out of the bedroom, so we go back to using traditional clock radios. *Touché*.
