---
title: "James’ new book on technical writing"
date: "2024-01-09T07:50:08+11:00"
year: "2024"
category: Software
tag:
- documentation
- writing
location: Sydney
---
[James of jamesg.blog](https://jamesg.blog/) has a [new book on technical writing](https://jamesg.blog/book.pdf) you can download today, itself an expansion of his *Advent of Technical Writers* blog post series from last year.

From page 6:

> This book is positioned toward people who want to start writing technical documents, or who are early in their careers and are looking to refine their skills. 
>
> This book will be focused on what I have learned from writing software documentation, code documentation, and technical tutorials. The world of technical writing is vast, encompassing everything from writing instruction manuals for hardware to producing online reference material for programming languages. The words herein may not be as directly applicable to some fields of technical writing as my core areas of expertise,
but much of what I cover has broad relevance.

I've always appreciated James' clear and compelling writing style on his blog, and he's carried it forward here too. He, [Wouter from Brain Baking](https://brainbaking.com/), and [Michael W. Lucas](https://mwl.io/) are breaths of fresh air from the nonsense I have to read on a daily basis as part of my own job.

Good technical writing is a skill, and one precious few seem to appreciate or do well. James nails it here.
