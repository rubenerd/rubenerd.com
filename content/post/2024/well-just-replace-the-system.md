---
title: "We’ll just replace the system!"
date: "2024-09-07T07:44:19+10:00"
year: "2024"
category: Software
tag:
- ai
- people
- systems
location: Sydney
---
I've been talking a lot with a friend recently who's been helping us out with something, and the story he's been telling us about his work is both ridiculous and believable. Some of you may have... let's just say, *intimate* experience with such a process! Maybe go grab a coffee, tea, or something stronger, then sit down as I tell you a story.


### The problem

Their small-ish firm did a survey a few years ago to determine what employees thought of the various admin systems they used for their jobs. None of the staff especially liked how any of the piecemeal systems worked or interacted. They'd all learned to work around the limitations, but reported being frustrated when dealing with even simple tasks, especially when it came to moving data from one of the internal systems to another. My friend estimated that it added hours of busywork to any job, though in his words "at least it worked in the end".

Management reviewed a few options, and decided to bring in one of the [Big Four](https://en.wikipedia.org/wiki/Big_Four_accounting_firms) to audit how their systems worked, and to propose a solution. The auditors spent a few months reviewing internal processes, interviewing staff, building user stories, analysing data, conducting meetings, and so on. It was "full on" in the words of my friend, to the point where some of the work being done for this review subsumed his daily responsibilities on the internal IT team. This pushed back internal project delivery dates by months, but he took what management said on board about this being important.

He mentioned being impressed with his own interview. After explaining his role, responsibilities, and how he interacted with the various internal systems at the firm, the auditor went as far as to ask for a "wish list" of features that would make his life easier. He came up with no fewer than 30 items, which the auditor seemingly took to heart with detailed notes, and even a follow-up call to understand some of the specifics. For all the horror stories he'd heard about such massive IT projects, he admitted to feeling informed, involved, and a part of the solution. Huh!


### The solution

The auditing firm took these points, and a few months later came back with a comprehensive proposal for the firm. They recommended a wholesale replacement of *all* the disparate systems with a new, overarching platform that would do **All The Things&trade;** instead.

Now, what do *you* think when you read this? Because when my friend first told me about this plan, alarm bells rang in my head louder than a fire engine speeding down the street to attend to an apartment building with a tenant burning toast! It's not that these legacy systems didn't need replacement, but in my experience, entirely new platforms are disruptive, introduce their own problems, and take far longer to implement than anyone *ever* claims. Existing systems, for all their problems, have been hammered into place. Newer systems often expect the business to adapt to them, rather than the other way round.

Unfortunately, though perhaps not surprisingly, our fears were realised.

The first problem was the proposed solution offered but a fraction of the features and capabilities of the systems for which it was designed to replace. When my friend's team had their initial induction and training, he was baffled that his questions regarding basic functionality were answered with a curt "it doesn't do that", or "you'll need to introduce a new workflow", or "this was a deliberate choice". He said that during one video call he noticed a colleague rolling their eyes and slowly shaking their heads on camera, and he found himself doing the same.

Remember when I talked about my friend being asked about features that would make his life easier or more productive? Shortly after the initial training sessions, he received an email listing everything he'd requested or wished for, and that almost all of them were infeasible. Many of these included the ones that the auditor had originally requested further detail from him for, leading him to wonder what the entire point of the song and dance was. If they knew the functionality of the system in the first place, why the charade?

Subsequent training sessions haven't gone smoothly either. The auditing firm's engineers seemingly didn't understand how their own systems worked, or entire sections of demos couldn't be performed because that part of the system hadn't yet been implemented, installed, or tested. During a two hour seminar (that stretched into lunch, a sore point for my friend!), the auditors fell back to showing the same part of the system twice, presumably to provide cover for their engineers behind the scenes scrambling to fix what they were supposed to be showing. What they *did* show was slower and more difficult to use than the clunky but reliable tech they'd used before, and again, *without half the features they needed to do their jobs!*

As one example, my friend has to compile a specific report each quarter. In the old system, this was logged as a recurring item. The new system doesn't have a concept of an item that's completed *and* recurring, so he has to generate a new one each time, from scratch (I'm glossing over a lot of the mechanics for obvious reasons, but it's still mind blowing). He says the time to complete this task has gone from less than an hour, to the better part of a day. One new AI feature (of course) excitedly touted as "saving time" leaves such a mess in its wake than he's had to work overtime on four consecutive Friday afternoons to clean it up. His favourite though is how the UI takes longer to load than their old system, despite it having a quarter of the features.

The training and features left my friend with the impression that the auditor wasn't just ill prepared, they fundamentally didn't understand the very basics of what their firm did, how they operated, and how their staff did their jobs. In other words, the *exact opposite* impression he got during those initial interviews.


### The impact (... crater?)

But of course, the *pièce de résistance* was the price. Because the implementation of the new system has been slower than expected (shocker), the cost has spiralled to far more than what was originally expected. Their firm only has a few hundred staff, and there's growing frustration that the exorbitant cost of this new system has eaten into budgets that would have been spent on staff pay and office improvements.

The end result has been staff actively working *around* the new system to do their jobs, resentment at the misplaced priorities, and more than a few on my friend's team either quitting or actively looking for work elsewhere. This has left large gaps in competency and institutional knowledge, which has further added to his workload. Perhaps worst of all though, the hit to morale has left it, to use his word, "terminal".

**Now!** Normally I'd try to offer some balance to a story like this. I'd describe how new systems are often required for regulatory, compliance or security reasons. People are often resistant to moving to new systems, because they have to learn a new way of doing things. Change is hard, and all that. While I believe everything my friend says, I'm sure there's a side to this I'm not seeing.

*But...*, and this is the real caveat here, that's why this company hired an external auditor to do this work in the first place! These companies specifically advertise and propose solutions based on their supposed pedigree and industry experience. They were the ones who conducted the initial feasibility studies. They're supposed to know the potential pitfalls, and either how to avoid or mitigate them. If they couldn't manage a project for a small company like theirs, it makes you wonder how they deal with enterprises.

I guess the conclusion here is: I'm not surprised. At all. I also feel bad for my friend, who until recently talked about liking his job and colleagues. It sucks when something like this comes along and pulls the rug out from under your feet.

Computer systems are used by people. One day these auditing firms, and these companies, will see case studies like this and realise what they're doing wrong at a fundamental level. Maybe. Possibly?
