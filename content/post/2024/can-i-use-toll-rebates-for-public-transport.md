---
title: "Can I use toll rebates for public transport?"
date: "2024-11-26T15:45:42+11:00"
abstract: "A state government scheme to rebate drivers for tolls completely misses the point, and incentivises driving over sustainable forms of transport."
year: "2024"
category: Thoughts
tag:
- australia
- cars
- public-transport
- sydney
- urbanism
location: Sydney
---
A few of you have said you enjoy it when I get stuck into car culture and public transport, so here's one for you! Last week I received a newsletter from the New South Wales state government, with the following subject line:

> Claim a toll rebate up to $340 a week

Oh I definitely have some thoughts already. But let's look at the substance of this email:

> We’ve received more than 234,000 claims for the $60 weekly cap rebate so far this year, with the average rebate being $284.
> 
> Eligible motorists can claim a rebate of up to $340 a week for personal toll trips in a vehicle registered for private use.
> 
> If you’ve spent more than $60 in any week on tolls between 1 July and 30 September, go online now to claim your rebate – it’s fast, easy and only takes a few minutes. Claim your rebate now.

There's a lot going on here, as video essayists say. But first, let's appreciate how absurd and redundant this is. A person who drives a car for "private use", and pays a toll, is able to claim some of it back. What's the point of collecting it in the first place then? And why benefit drivers over non-drivers?

Tolls are collected for various reasons, but the most common are:

1. To disincentivise driving over other forms of transit if they're available.

2. To more equitably fund road maintenance by charging the people who use them.

3. To provide a return on investment for the large capital cost of private motorway construction; or for the motorway operators to turn massive profits, depending on your perspective.

The idea of a "cap" on tolls flies in the face of point (1) and (2). It's cheaper per kilometre to drive *more* once you reach that cap, because the cost is diffused over a further distance. This *incentivises* more driving, which increases road wear, which results in more unfunded maintenance.

This gets us to the more perverse result from point (3). A rebate funnels taxpayer money&mdash;our money&mdash;into private road operators. The people driving on these privately-operated toll roads are still being charged, but the money to pay for the tolls ends up being rebated from the public purse. Brilliant!

*(As I finished writing this, I read that you can only claim up to $340 a week. Having an upper limit is good, but it still encourages more driving when you're between $60 to $340. The incentive is there to claim as many free tolls as you can. Which is precisely what we <strong>don’t</strong> want)!*

There's also a social element. Rebates disproportionately affect lower-income earners, who may not have the cash or credit to pay for the tolls in the first place. Think about those old computer systems that came with mail-in "rebate" coupons. If you couldn't afford that component at the start, you wouldn't buy it. But if you can, you get a bit back. In other words, rebates are claimed by people who *can* pay for them. This makes rebates a regressive tax on mobility, which especially sucks if all you can afford to rent is a shoebox in a car-centric hellscape away from Sydney's above-average but patchy train and metro system.

And speaking of trains, why is this rebate only for people who take privately-funded toll roads? Where's the rebate for people who can, or choose, to commute by train, bus, tram, ferry, penny farthing, or foot? It's not like public transport costs haven't also become expensive, and added to cost of living pressures. Instead, tax dollars are being given to motorists, some of whom may need it, but others could have easily taken a sustainable form of transport instead. As mentioned, I'm now helping to pay for the worst form of urban transport, to private operators, while I stand on a crowded train.

I was about to say that words can't express my level of contempt for this scheme, and for the shortsighted, populist bureaucrats who came up with it. But apparently I *can* express it with words, as evidenced by this blog post.

    # pkgin install life
       
    ==> calculating dependencies... done
    ==> 2 packages to install:
    ==> life cars
       
    # echo "Wait no, fsck(8) car dependency!"

