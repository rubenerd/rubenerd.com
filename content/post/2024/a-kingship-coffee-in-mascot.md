---
title: "A Kingship Coffee in Mascot"
date: "2024-04-17T16:09:38+10:00"
thumb: "https://rubenerd.com/files/2024/kingship-coffee-cup@1x.jpg"
year: "2024"
category: Thoughts
tag:
- australia
- coffee
- sydney
location: Sydney
---
Kingship Coffee opened in Mascot, near Sydney Airport. It's pretty great, and just what I needed :).

<figure><p><img src="https://rubenerd.com/files/2024/kingship-coffee-cup@1x.jpg" alt="A coffee cup on a table, on a nice day." srcset="https://rubenerd.com/files/2024/kingship-coffee-cup@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>
