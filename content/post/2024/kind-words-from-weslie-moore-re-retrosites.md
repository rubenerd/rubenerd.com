---
title: "Launching my Retro Corner, thanks to Wesley Moore"
date: "2024-06-26T09:12:01+10:00"
year: "2024"
category: Internet
tag:
- retrocomputing
location: Sydney
---
I woke up to this Mastodon post from [Wesley Moore](https://mastodon.decentralised.social/@wezm) up in Queensland:

> This is a fun idea: a retro corner to your current website that is accessible to vintage machines as well as retro themed http://retro.rubenerd.com/ by @rubenerd

Thanks! You should also [check out his blog](https://www.wezm.net/v2/), including a fun recent post about his [Tech Stack](https://www.wezm.net/v2/posts/2024/tech-stack/) from earlier this month. His post made me realise that I linked to my retro site from a [recent post about nostalgia](https://rubenerd.com/the-other-side-of-nostalgia-and-the-indieweb/ "The other side of nostalgia, and the IndieWeb?"), but never actually formally "launched" it. I guess that's what this is!

**[Ruben's Retro Corner](http://retro.rubenerd.com/)** is what it says on the tin. The plan was to use it for tracking my progress restoring retrocomputers, linking to drivers, keeping an inventory of parts, and documenting stuff, but I've decided to use it as a general "catch all" for nostalgia.

The site itself is also a bit of retro fun. I wrote it in [HTML 3.2](https://www.w3.org/TR/2018/SPSD-html32-20180315/) without a CMS, using most of the same assets my original GeoCities site had back in the day. It's also delivered with 256 colour files, and without SSL/TLS, so legacy machines can access it.

It's very much *Under Construction* as we used to say, so it's a bit of a mess. But it's been so much fun getting stuck into old school web design again (if you can call that resulting site "designed"). I liked Wes calling it a *Retro Corner*, so I also renamed it to that.

