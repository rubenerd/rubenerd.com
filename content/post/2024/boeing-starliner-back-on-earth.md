---
title: "Boeing Starliner back on Earth"
date: "2024-09-13T17:59:22+10:00"
year: "2024"
category: Thoughts
tag:
- aviation
- science
location: Sydney
---
The [BBC's Science News desk reported](https://www.bbc.com/news/articles/cx29wzk4r19o) on the returned craft a week ago:

> The capsule, which suffered technical problems after it launched with Nasa's Butch Wilmore and Suni Williams on board, was deemed too risky to take the astronauts home.
>
> [...] it was plagued with problems soon after it blasted off from Cape Canaveral in Florida on 5 June.
>
> The capsule experienced leaks of helium, which pushes fuel into the propulsion system, and several of its thrusters did not work properly.
> 
> Engineers at Boeing and Nasa spent months trying to understand these technical issues, but in late August the US space agency decided that Starliner was not safe enough to bring the astronauts home.

I know Boeing the defence and aerospace contractor isn't (entirely) the same as Boeing's commercial aircraft division. But it's still not a great look for the already beleaguered company that's trying to regain the trust of its customers, the flying public, and governments.

I don't envy their PR department, to say nothing about the teams internally who are trying to rebuild the company's culture. Boeing used to stand for trustworthiness and engineering excellence, but I instinctively wince when I see their name appear in my feed reader.
