---
title: "In search of the perfect soap"
date: "2024-11-17T10:13:49+11:00"
thumb: "https://rubenerd.com/files/2019/declutter-mofo-soap@1x.jpg"
year: "2024"
category: Thoughts
tag:
- cleaning
- jazz
- michael-franks
- soap
location: Sydney
---
Don't worry, this isn't a post about an overengineered access protocol I last talked about [a decade ago](https://rubenerd.com/remember-soap/ "Remember SOAP?"). Or... is it?

In 1978, Michael Franks began his [search for the perfect shampoo](https://www.youtube.com/watch?v=nAyhdeGEhf4 "In Search of the Perfect Shampoo"). My mum heard a friend play that song on their turntable in the 1980s, and it began a lifelong obsession with the singer/songwriter and his works. She passed this onto me, both in musical tastes, and in my middle name. While my favourite song remains *[Tiger in the Rain](https://www.youtube.com/watch?v=4a9WwntkrWU)*, this song about shampoo continues to tickle me all these years later. Michael penned this, a producer gave it the green light, and it was released to the world. Wonderful.

That said, my shower and hygeine habits belie an interest in a different&mdash;though related&mdash;substance. One may say *obsession*.

Soap. I love soap. I love the tactile experience of holding a bar of soap, lathering it up in my hands, and spreading it around where required. Soap is approachable, grokkable. I'll never be fabriciating my own integrated circuits, but I love that I can buy the essential ingredients to make my own soap if I ever wanted. I love going to markets and seeing all the weird and wonderful fragrences and shapes people have made with soap. Soap bars don't need plastic packaging, or complicated bottle pumps that get gross and can't be recycled. You can mix oats into soap, or other natural scrubs. Soap!

I've written about [shower soap](https://rubenerd.com/mofo-soap/ "Mofo soap"), [hand soap](https://rubenerd.com/a-simple-bar-of-soap/ "A simple bar of soap"), and even [soap holders](https://rubenerd.com/soap-holder-language/ "Soap holder language")\:

<figure><p><img src="https://rubenerd.com/files/2019/declutter-mofo-soap@1x.jpg" srcset="https://rubenerd.com/files/2019/declutter-mofo-soap@1x.jpg 1x, https://rubenerd.com/files/2019/declutter-mofo-soap@2x.jpg 2x" alt="The Mofo bar of soap next to its box, a bar of Cussons Prize Medal Oatmeal soap, and for no reason, Saber from Fate." style="width:500px; height:333px;" /></p></figure>

And yet, soap doesn't always like me. My main challenge with soap lies in its *aggresively* effective surfactant abilities. Soap allows water to bond to the natural oils and other crud on our skin, which leaves us feeling clean and refreshed. But disturbing that delicate equilibrium between clean and dry can lead to chapped and creaky skin, with our bodies naturally producing more oil to compensate.

This is a shame, because many of my favourite soaps are simply too much hassle to deal with now. The smell of Cussons Imperial Leather takes me right back to my childhood and memories of my parents, but it's too drying. Same for most craft soaps. I can use a dedicated moisturiser afterwards like I do with my face, but speaking honestly, that's another step I'm likely to skip when it's early in the morning and I have to catch a train.

My solution thus far has been to use soaps like Dove which include a moisturiser. Obsessively washing our hands with Dove got Clara and I through Covid without destroying our skin, and it works great in the shower. But years of using the same soap is boring, especially for someone *who loves soap!*

My most recent adventure has been with *[Dr Squatch](https://www.drsquatch.com/)* scrubs on advice from a friend, which has been interesting. It's far less drying than most soap I've used, and the *Fresh Falls* fragrence have been nice to wake up to. But it's still not as effective at moisturising as the Dove. If Pears soap is a 1, and Dove is a 10, this would be around a 7.

It seems I'm still in search of the perfect soap. If and when I find it, you'll all be the first to know. Soap.
