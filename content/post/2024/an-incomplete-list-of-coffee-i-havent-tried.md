---
title: "An incomplete list of coffee I haven’t tried"
date: "2024-10-29T07:08:12+11:00"
year: "2024"
category: Thoughts
tag:
- coffee
location: Sydney
---
As much as a [coffee fan](http://ruben.coffee/) as I am, there's still a lot I haven't tried, and plenty of experiences to be had.

* **Home-made coffee ground with burrs**. I've always used bladed coffee grinders on account of cost, though I hope to rectify this soon.

* **Coffee specifically with James**. Clara has never been to Scotland, and we especially want to make the trip up there to say hi and meet the IndieWeb's [premier coffee blogger](https://jamesg.blog/) himself.

* **Espresso in Italy**. One day!

* **Coffee with creamer**. I'm not a snob, I've just never felt compelled to add it. I don't think I even knew what coffee creamer was until I saw James Hoffman review various flavours. Not to be confused with *crema*.

* **Tim Hortons, in Canada**. Clara and I had it in New York, but I was told it was better up north. Though I've also since been told that's also gone downhill since they were bought. Oh well, time machine anyone?

* **Coffee with butter**. As above, though I know people like Dan Benjamin swore by it back in the day.
