---
title: "Nick Jordan reviews Australian instant coffees"
date: "2024-04-10T08:54:48+10:00"
thumb: "https://rubenerd.com/files/2024/alcafe@1x.jpg"
year: "2024"
category: Thoughts
tag:
- aldi
- coffee
- reviews
- girls-und-panzer
location: Sydney
---
Nicholas Jordan and his taste testing team of taste testers [tasted and tested various Australian instant coffees](https://www.theguardian.com/food/2024/apr/10/best-instant-coffee-brands-supermarket-taste-test-australia). Taste test!

Their results confirmed what Clara and I have long suspected, and were equally surprised about: [Aldi](https://www.aldi.com.au/) instant is shockingly decent, especially for the price. Clara has taken a small jar of it to work, and I have a few different jars which I've caught Darjeeling from *Girls und Panzer* grabbing a cup of sometimes between her namesake teas.

<figure><p><img src="https://rubenerd.com/files/2024/alcafe@1x.jpg" alt="Darjeeling sitting with a few different instant coffee jars." srcset="https://rubenerd.com/files/2024/alcafe@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

I don't rank things I hate, so Aldi's Alcafe rates a solid 1/5. Palatable, inoffensive, and completely reasonable. *But enough about me.*

I agree with Nick's analysis of its slight burnt cereal taste, and how it actually adds to the flavour slightly. Then again, I love the taste of cereal, so I'm biased. I can't speak to how it tastes with milk.

Europe's resident coffee expert [James](https://jamesg.blog/) may have well-founded reservations about me praising instant (cough!), but it's decent in a pinch, or when I'm in too much of a hurry to pull out the coffee grinder, Aeropress, or the Hario V60.
