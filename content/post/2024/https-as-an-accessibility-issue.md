---
title: "HTTPS as an accessibility issue"
date: "2024-10-03T08:26:57+10:00"
year: "2024"
category: Internet
tag:
- accessibility
- security
location: Sydney
---
Like many of you, I was quick to install Let's Encrypt once it was available. It reduced the barrier to entry significantly for HTTPS by rendering the experience of buying, installing, and renewing certificates affordable and easy. I let other commercial certs lapse because Certbot and the various other ACME clients were that much nicer.

The benefits were clear(text). **HAH!** Privacy for your visitors, integrity of pages and files in transit, secured authentication (remember Firesheep?), and the warm fuzzies that you were doing your bit to make the Web more secure. Over time, search engines preferenced sites delivered over HTTPS, and browsers began warning users of sites that weren't, both of which I didn't think too much about at the time.

Two things have happened recently to shake this perception.

The first was the launch of my silly *[Ruben's Retro Corner](http://retro.rubenerd.com/)*, which has taken on more of a life than I expected. Several of you have told me you use the site to test retrocomputers, which makes me incredibly happy. More surprising though were the number of people saying they use it as a way to test other machines in a range of different scenarios and use cases. It's a relatively memorable address, and they know it's one of the few remaining sites they know will be delivered over plain HTTP.

In the words of moral philosopher Curtis Stigers, it made me *[wonder why](https://www.youtube.com/watch?v=VS2Hp9Ck9mQ "Curtis Stigers: I Wonder Why")*. Granted, I've written enough scripts to know that including the requisite libraries to handle HTTPS traffic is an extra step, but I didn't think it was especially onerous.

It wasn't until I wrote my post about the [promise of HTML and CSS](https://rubenerd.com/the-promise-of-html-and-css/) last month that I drew the obvious connection. *Not every endpoint supports HTTPS*. And we're shutting them out.

This is a bit sad at a philosophical level. HTML, as we use it today, is largely compatible with the original markup that Tim Berners-Lee and his team developed in the 1990s. My Retro Corner can be read on a 1994 Macintosh Quadra, and a 2022 Ryzen 5700X PC. So much software, and so many websites, have come and gone during that time, but the *lingua franca* of the Web remains. It might not be rendered the same, or it may be missing certain features (remember when webdevs thought graceful degradation was important?), but it's still readable. That's unusual, and beautiful, and I've begun to treasure it.

But this is a pragmatic issue. Many endpoints are old and lack support for modern ciphers, or never had the feature to begin with. Implementing HTTPS everywhere introduces a limitation on the machines, operating systems, and therefore *people* who can view this. [Terence Eden's 2021 post about this topic](https://shkspr.mobi/blog/2021/01/the-unreasonable-effectiveness-of-simple-html/) is worth a read if you don't think this is an issue.

This gets us back to the value proposition of HTTPS. Are they... really necessary for a blog without a web-facing admin portal, software downloads, or mission-critical features? Have I shut people out for benefits that don't really make sense in this context? Have a lot of us?

It might be too late to reconsider reverting back to plain old HTTP for this blog, because I assume endpoints would see a former HTTPS site rendering as HTTP as a security risk. Redirects would also be tricky. But it's making me re-evaluate my use of it elsewhere.
