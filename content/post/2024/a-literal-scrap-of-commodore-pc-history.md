---
title: "Buying a literal scrap of Commodore PC history"
date: "2024-03-02T12:10:03+11:00"
year: "2024"
category: Hardware
tag:
- commodore
- commodore-pc
- retrocomputing
location: Sydney
---
Last May I wrote a post about [Commodore's PC line](https://rubenerd.com/researching-if-commodore-pcs-was-profitable/ "Researching if Commodore PCs were profitable"). The famed C64 and Amiga manufacturer also hedged their bets in Europe with a line of PC clones that had some interesting quirks, including a PET startup jingle, some unique graphics hardware, and an unusual level of motherboard IO integration for the time.

I grew up on DOS machines, so the Commodore PC line has intruiged me for years. But I could never justify buying one, given I already have PC kit of a similar vintage. The eye-watering prices they command by collectors also puts them well out of my league, even if I could afford the astronomical shipping from Europe to Australia.

This changed last month, sort of. A chap in Germany was selling an intersting piece of Commodore PC history, for less than 25 € shipped to Australia. It arrived last week (cough), and here it is:

<figure><p><img src="https://rubenerd.com/files/2024/commodore-pc45-scrap@1x.jpg" alt="Photo showing the scrap piece of the bezel, with the indicator lights, computer name plate, and badge." srcset="https://rubenerd.com/files/2024/commodore-pc45-scrap@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

*How cool is this!?* I hear you asking. Very, I'd say!

This is a broken piece of a Commodore PC 45-III bezel, with all the interesting visual parts of it intact. It was billed as "scrap" that could be harvested for its unique Commodore logo badge. That broke my heart a bit, so I had to save it for a personal project!

My plan is to give it a deep clean, sand the broken pieces in the lower right to match the left, retr0bright it just enough to hide that missing sticker mark, and mount it to a little wooden board that could either be free-standing, or placed on a shelf.

The next step would be to mount a small single-board computer in the large gap; maybe a Raspberry Pi or equivilent to run an emulator. The headers and cables for the LEDs are still intact, so my hope is I can wire them up for some *Das Blinkin Lights* too.

The Commodore PC 45-III was an 80286-bsaed PC with 1 MiB of RAM, a 3.5-inch disk drive, and VGA graphics. Unlike the Colt, it also retained that darker Commodore grey bezel which was a nice nod to the Commodore 64.
