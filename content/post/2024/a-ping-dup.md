---
title: "A ping DUP!"
date: "2024-10-24T08:56:28+11:00"
year: "2024"
category: Internet
tag:
- networking
location: Sydney
---
I was struggling with some flaky, tethered Optus Internet earlier this week, so I tried to ping [Quad9](https://www.quad9.net/)\:

	64 bytes from 9.9.9.9: icmp_seq=0 ttl=55 time=115.004 ms
	64 bytes from 9.9.9.9: icmp_seq=1 ttl=55 time=199.378 ms
	64 bytes from 9.9.9.9: icmp_seq=2 ttl=55 time=71.115 ms
	64 bytes from 9.9.9.9: icmp_seq=3 ttl=55 time=271.379 ms
	64 bytes from 9.9.9.9: icmp_seq=4 ttl=55 time=445.797 ms
	64 bytes from 9.9.9.9: icmp_seq=5 ttl=55 time=95.156 ms
	64 bytes from 9.9.9.9: icmp_seq=5 ttl=55 time=98.663 ms (DUP!)
	64 bytes from 9.9.9.9: icmp_seq=6 ttl=55 time=19.403 ms
	64 bytes from 9.9.9.9: icmp_seq=7 ttl=55 time=153.812 ms
	64 bytes from 9.9.9.9: icmp_seq=8 ttl=55 time=209.375 ms
	64 bytes from 9.9.9.9: icmp_seq=8 ttl=55 time=201.306 ms (DUP!)
	64 bytes from 9.9.9.9: icmp_seq=9 ttl=55 time=92.014 ms

There's some output I hadn't seen since I used [TM Streamyx in Malaysia](https://rubenerd.com/we-have-internet-and-telephone-again/) back in the 2000s! I still remember what a shock it was after having spent years using Singaporean cable.

As always, the manpage for [ping(8)](https://man.freebsd.org/cgi/man.cgi?query=ping) had more details:

> The ping utility will report duplicate and damaged packets. Duplicate packets should never occur when pinging a unicast address, and seem to be caused by inappropriate link-level retransmissions.  Duplicates may occur in many situations and are rarely (if ever) a good sign, although the presence of low levels of duplicates may not always be cause for alarm. Duplicates are expected when pinging a broadcast or multicast address, since they are not really duplicates but replies from different hosts to the same request.

