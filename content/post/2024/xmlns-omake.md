---
title: "The omake namespace"
date: "2024-05-27T13:54:46+10:00"
year: "2024"
category: Internet
tag:
- xml
location: Sydney
---
Introducing **Omake**, my simple cardfile format written in XML. It's designed to be readable by humans, and easy to present with a simple XSL transform.

*This is version 1.0, last updated October 2023.*


### Etymology

The format is named for Japanese *omake*, describing bonus or extra content. The silly bacronym is *Outline Markup and (Zettle)Kasten Enumerator*, for the [personal German cardfile system](https://en.wikipedia.org/wiki/Zettelkasten) of paper slips.


### Structure

**Omake** consists of a root `omake` element, one or more `card` elements that can be nested, and text `note` elements.

* The `omake` element can have Dublin Core elements describing the file, or whatever other namespace you wish to import.

* `card` elements are collections of notes under a topic or idea. They can have Dublin Core attributes. When rendered, `dc:title` attributes could be used as the heading one could click on to expand or flip to that card.

* `note` elements are plain text by default, though they can be rendered differently based on attributes. For example, an `xlink:href` could be rendered as a link to click, and `dc:description` could be used to create key/value pairs. A `note` must be in a `card`.


### Examples

This is the simplest **Omake**, with a single card and note:

    <?xml version="1.0" encoding="UTF-8"?>
	<omake xmlns="https://rubenerd.com/omake-xmlns/">
	    <card dc:title="Are you worth it?">
	        <note>Yes</note>
	    </card>
	</omake>

Here's an Omake with a couple of `card` elements, `note` elements, and a bit of metadata:

	<?xml version="1.0" encoding="UTF-8"?>
	<omake xmlns="https://rubenerd.com/omake-xmlns/"
	    xmlns:dc="http://purl.org/dc/elements/1.1/"
	    xmlns:xlink="http://www.w3.org/1999/xlink">
	    
	    <dc:title>Ruben’s Favourites</dc:title>
	    <dc:description>A collection of nice things</dc:description>
	    <dc:date>2023-10-08T15:35:00+10:00</dc:date>
	    
	    <card dc:title="Favourite things">
	        <note dc:description="City">Singapore</note>
	        <note dc:description="OS">FreeBSD</note>
	    </card>
           
	    <card dc:title="Favourite feeds">
	        <note dc:type="application/rss+xml" 
	            xml:lang="en-SG"
	            xlink:href="rss.xml">Favourites, lah</note>
        </card>
	</omake>


### Metadata

**Omake** uses [standard XML](https://www.w3.org/2001/xml.xsd) attributes, [Dublin Core](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#section-3), and [XLinks](https://www.w3.org/TR/xlink11/) to provide metadata, because they've done the work to define them for us. The recommended attributes are `xml:id`, `xml:lang`, `dc:date`, `dc:description`, `dc:title`, `dc:type`, and `xlink:href`.

All attributes and data are expected to be UTF-8.


### Comparing Omake to OPML

Omake is partly inspired by many years of using the Outline Process Markup Language. This is how it differs:

* Omake is philosophically a digital zettelkasten of named cards, not an outline. That said, you could probably serialise one as the other.

* Data is stored in elements. This negates the need for nested parsing, and permites the use of CDATA without escaping. This is closer to how RSS works, and improves interoperability with standard XML tooling.

* Omake uses modern date formats, thanks to Dublin Core.

* Omake notes don't need a type attribute. Instead, type is inferred by its attributes. A note with an `xlink` is a link, for example.

* <abbr title="internationalisation">I17n</abbr> is baked-in, thanks to the use of `xml:lang`, and UTF-8 by default. This is important to me.

* Like OPML however, Omake isn't RDF. Life is too short.


### Validation
 
I'm planning to implement a schema at some point. Dublin Core and XLink attributes can be validated against their specifications.


### Conclusion

My [omake.xml](/omake.xml) file is now live, if you'd like a real example.

I doubt anyone else will use this, owing to howls of "why would you use XML?" and so on. But it was a fun afternoon exercise.


### Related posts

* [The housekeeping namespace](https://rubenerd.com/xmlns-housekeeping/)

