---
title: "Australian trees in the Artarmon Reserve"
date: "2024-08-11T14:00:06+10:00"
thumb: "https://rubenerd.com/files/2024/artarmon-reserve@1x.jpg"
year: "2024"
category: Travel
tag:
- hiking
- nature
location: Sydney
---
Speaking of trees, Clara and I wandered back to Chatswood this afternoon via the Artarmon Reserve. There were some stunning Australian trees that looked (and smelled!) amazing, and only about six kilometres from the Sydney CBD.

<figure><p><img src="https://rubenerd.com/files/2024/artarmon-reserve@1x.jpg" alt="Photo showing Australin trees against a mostly-clear winter sky." srcset="https://rubenerd.com/files/2024/artarmon-reserve@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

Thanks to some cycling trails, it's possible to get from St Leonards to Chatswood almost entirely without walking along a road. I'd give serious consideration to giving up the train and biking or scooting along a large part of this if I were staying in the area.
