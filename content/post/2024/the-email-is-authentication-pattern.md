---
title: "The “email is authentication” pattern"
date: "2024-09-06T10:24:39+10:00"
year: "2024"
category: Internet
tag:
- design
- privacy
- security
location: Sydney
---
I'm the first to admit that I don't live in the real (electronic) world. As the late Jim Kloss pointed out during one of his broadcasts, we (and probably you) live in a part of the Web with ad blockers (as the [FBI recommends](https://www.ic3.gov/Media/Y2022/PSA221221)), limited JavaScript, password managers, and a (mostly) finely-tuned sense of what is a scam and what is legitimate (that was a lot of brackets).

Most people don't live like this. I'd posit the *vast majority* don't. And it's worth a reality check sometimes.

Here's a shockingly-common login process I witness:

1. Get to a login page
2. Click "I forgot my password"
3. Go to their email
4. Click the recovery link
5. Type a throwaway password they won't retain
6. Rinse, and repeat

When I ask people why they do this, they either don't have an answer, or respond with "huh, I never thought about why". And that's interesting to me.

Enough has been written (including here) about the need for password managers, the risks of identity theft, two-factor and multi-factor authentication, and whether the entire concept of a username/password is antiquated and in bad need of replacement. If you're a reader of my silly blog here, you likely already know all this.

What I'm interested in here is the fact people have come up with that above process *in the first place*. How do you decide that using "I forgot my password" as authentication makes sense to you? Or more specifically, the *most* sense to you, out of all possible options?

I think people can't answer why they do this because it's not a concious decision. They don't wake up in the morning and decide *yes, this is how I'm going to interact with online accounts today!* Instead, this is a process that has coalesced over time and become rote. It offers a guaranteed, repeatable, low-effort solution (of sorts) to passphrases they don't need to think about (there's those brackets again).

It makes me wonder if we're looking at a bunch of these issues backwards, and whether we can take advantage of people's tendencies towards learned behaviour like this. What if we could somehow design systems so that the people who use them evolve to use them in *better* ways? Because I do empathise with people that often improved security comes with more barriers and friction, not fewer.
