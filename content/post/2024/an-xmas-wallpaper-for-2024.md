---
title: "An Xmas wallpaper for 2024"
date: "2024-12-01T16:38:45+11:00"
abstract: "We put up the family tree at my sister's this year, and I think it turned out great! "
year: "2024"
category: Media
tag:
- holidays
- xmas
location: Sydney
---
We put up the family tree at my sister's this year, and I think it turned out great! If you look *very* closely at a couple of the baubles, you might see us smiling and waving.

Feel free to use if it will help bring your desktop some cheer. 🎄

<figure><p><a title="Family Xmas tree wallpaper" href="https://rubenerd.com/files/2024/xmas-2024@original.jpg"><img src="https://rubenerd.com/files/2024/xmas-2024@1x.jpg" alt="Family Xmas tree wallpaper" srcset="https://rubenerd.com/files/2024/xmas-2024@2x.jpg 2x" style="width:500px; height:281px;" /></a></p></figure>
