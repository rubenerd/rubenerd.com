---
title: "Bidding farewell to Mini-ITX and SFF, maybe"
date: "2024-12-31T06:34:36+11:00"
abstract: "The limitations of the platform might have finally got to me, maybe."
year: "2024"
category: Hardware
tag:
- builds
- mini-itx
location: Sydney
---
I like small things. I like efficient things. I like small things that are efficient. Brute-forcing a solution by making it bigger, hotter, or louder is one thing, but performing the same work with less, in a smaller space? Or taking an existing system and shrinking it down? *That's* impressive. The fact Clara is that much shorter than me and twice as intelligent is another example (cough).

I knew about Mini-ITX going back to the early days of VIA; I saw one demo'd at a COMDEX/Asia in Singapore in the early 2000s and thought it was unfathomably cool. *How did they do it!?* But when I read that you could get standard, desktop-class components in a tiny board, wed it to a decent graphics card, and put in in a small-form factor case, I knew that I wanted to give it a try.

Fifteen years on, and all my desktop PCs have used these form factor components. It was never a question! I told myself it was because I've always lived in tight spaces, save for the McMansion my family lived in when we were in Kuala Lumpur. My bedrooms in Singapore were tiny, my dorm in Adelaide even more so, and Clara and I lived in tiny apartments in Sydney until recently.

<figure><p><img src="https://rubenerd.com/files/2024/ncase-m1-sliver@1x.jpg" alt="Press image of the NCASE M1 in silver" srcset="https://rubenerd.com/files/2024/ncase-m1-sliver@2x.jpg 2x" style="width:500px; height:336px;" /></p></figure>

The truth was, I loved my little computers. The NCASE M1 remains my single favourite case I've ever bought, and even with its thermal compromises, I love my svelte Fractal Ridge. Sure, they're not as small as a Mac Mini, but I can build, upgrade, fix, and replace everything myself. They have this satisfying heft to them when you pick them up: they're small, but boy are they feature packed. And they're cute!

But *compromise* was the operative word above. Mini-ITX can be a juggling act when it comes to selecting and installing components, some of which can be more difficult to deal with than others. For example:

* You only have one real slot, which you'll almost certainly be using for a graphics card. You can [hack the Wi-Fi slot](https://rubenerd.com/using-m2-wifi-cards-for-other-things/) into a port or an additional PCIe slot, but where would you put the card you attach to it? This limits the potential use cases for the machine.

* The board may have a generous slew of drive ports and other connectors, but then your case will be the limiting factor. You won't be installing a bevy of 2.5 or 3.5-inch drives, and optical drives are out of the question without a mess of external units and cables. You *could* put the motherboard in a larger case, but that negates the point.

* Mini-ITX cases have more limited thermal ceilings, as I've noticed with the Ridge. Careful and optimal component selection will reduce this problem, but a Mini-ITX case will likely never be as quiet or have the cooling potential as a regular case with more/larger fans, longer radiators, and wider external surface area.

* Unsurprisingly, Mini-ITX is more expensive than other form factors for the same components. You need more expensive DIMMs because you have fewer slots, you probably need a smaller power supply, and so on.

I was able to wrestle with these limitations by outsourcing many of my day-to-day requirements to an external homelab server. This sits in an old Antec 300, which was considered a medium-small case in the day, but is a monster compared to the Mini-ITX cases Clara and I use. I would not have been able to sling this had I needed to live in just one computer.

I'm not ready to throw in the towel and get a monster, but I'm thinking a Micro-ATX board will be in my next build. I could get a higher-spec CPU, and pair it with a decent-sized cooling fan that doesn't sound like a jet taking off under load. It would have the physical space and slots for a discrete sound card, 10G NIC, and maybe even an optical drive? I'll admit, I'm kind of excited thinking about what I could do without these self-imposed limitations on what is my primary desktop. I've not researched larger cases in a long time.

Who knows, maybe the novelty of that will wear off too, and I'll go back to Mini-ITX and SFF. I'd say I'm indecisive, but I'm not sure.
