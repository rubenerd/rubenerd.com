---
title: "Modern C64 boards compared to original prices"
date: "2024-01-23T08:55:05+11:00"
abstract: ""
thumb: ""
year: "2024"
category: Thoughts
tag:
-
location: Sydney
---
A couple of weeks ago I posted about [reproduction Commodore 64C motherboards](https://rubenerd.com/modern-commodore-64-motherboards/ "Modern Commodore 64 motherboards"). Two things I didn't mention were availability and price. I'm thankful we can get them *at all*, but these points were still glaring omissions with hindsight.

Fellow BSD.network Mastodon poster @Freezr [summarises](https://bsd.network/@freezr/111800220258829470
 "Post on BSD.network by @Freezr") most of the comments I've received:

> And they cost even more that the original C64 price!

I wondered if this was true. The short answer is: probably! But it's an interesting mental exercise.


### How expensive was the original Commodore 64?

Wikipedia references an article in [IEEE Spectrum from 1985](https://web.archive.org/web/20120513181613/https://spectrum.ieee.org/ns/pdfs/commodore64_mar1985.pdf) that quotes the Commodore 64 as being **US $595** at launch, and **$149** by the mid 1980s. I'm sure they got even lower by the early 1990s as it evolved from a multimedia powerhouse to budget-concious home computer.

Commodore Australia also [advertised 📺](https://www.youtube.com/watch?v=b4iwtzJf9zI "Commodore 64 - Family Pack - 1983 - Australian TV Commercial") a *Family Pack* which included an official Commodore datasette, joystick, and bundled BASIC software for **AU $499** as early as 1983. The AUD was close to parity with the USD at the time, so it shows the price already came down within a year.

Using the [US Inflation Calculator](https://www.usinflationcalculator.com/), prices in today's money roughly correspond to **$470** at the low end, and **$1879** at launch. That's still lower than I expected, and proves just how aggressive Commodore were able to punch out these machines at scale.

I wonder how much the [Commodore 64 sold at Aldi](https://rubenerd.com/my-new-aldi-commodore-64/ "My new Aldi Commodore 64") in West Germany by the late 1980s? Any German readers have some old magazines lying around? :).


### Is this cheaper than modern boards?

In dollar terms from the time, yes. But it becomes murkier when one factors in inflation, as [@ClaudioM points out](https://bsd.network/@claudiom/111800240245594070 "Such is the case with inflation after 40 years or so").

The [bwack's C64 250407 replica](https://www.pcbway.com/project/shareproject/The_C64_250407_replica.html) is probably the most afforadble board on this list, because the Gerber source files are freely available and can be fabricated by anyone. [DIY Chris](https://diychris.com/product/commodore-64-250407-replica/) also sells his replica for a very reasonable $35, as does [Bob's Bits SixtyClone](https://www.tindie.com/products/bobsbits/sixtyclone-commodore-64-replica-pcbs/).

But we're forgetting something! While the PCB is integral to any C64 build, we still need sockets, the ICs themselves, capacitors, resistors, switches, a case, a keyboard, and a power supply. Almost all of these can be sourced from reproductions or modern versions of the original components, but they each add to the total price of a build (we'll also probably need an upscaler or converter for use on modern displays, though an original C64 would also need this. More on this in a moment).

At a high level:

* [Bob's Bits](https://www.tindie.com/products/bobsbits/sixtyclone-commodore-64-replica-pcbs/) will sell you a complete kit with most of these components for $93.50, including a SID replacement

* [Retro8BitShop](https://retro8bitshop.com/product/j-cia64-the-modern-alternative-for-the-mos6526-8521-cia-chips-for-the-commodore-64-128/ "
J-CIA64 6526/8521 Replacement for Commodore 64/128/1570/1571") sell the J-CIA64s for 40 €, of which you'll need two.

* <a title="MOS CPU Replacer (6510/8501)" href="https://monotech.fwscart.com/product/mos-cpu-replacer-(6510-8501)">Monotech</a> has a populated 6510 to 6502 in a socket for $35

* [Vitnoshop](https://www.etsy.com/au/listing/721885870/commodore-64-dust-cover-brand-new) has a gorgeous dust cover for AU $46. Okay, this isn't strictly necessary, but still.

This doesn't include the EEPROMS, licences for things like JiffyDOS, solder/flux/etc, or what your time is worth; something those cooking channels on YouTube forget as well. *Here's a 10 minute recipe that involves two hours of preparation!*

I'm going down this avenue as we speak. But if the cost of procuring (and shipping!) all these disparate components sounds frustrating, you can go with the partly-populated [C64 Reloaded Mk2](https://icomp.de/shop-icomp/en/produkt-details/product/c64-reloaded-mk2.html) for 210 €, or the [Ultimate 64](https://ultimate64.com) for 121 € that requires no additional components at all. The latter uses an FPGA, but it's significantly cheaper for similar functionality, which is impressive.

Either way, you can see how these costs add up. I have $600 as a goal in my budget spreadsheet for my Aldi 64 build, including the case and keyboard from eBay; a goal I won't achieve for a while for a *host* of reasons, but it's good to have dreams!

Unless you go all-out on a gorgeous [EVO64](https://retrospective.shop/) with a [tube preamp](https://retrospective.shop/triode64-audio-preamp-toroidal-psu-combo/), modern C64 builds today are placed squarely between the C64's launch price, and that latter price quoted on the IEEE article.


### Other things to consider

Commodore&mdash;rather infamously&mdash;were cheap. They used whatever components they could get their hands on, as evidenced by the mishmash of IC manufacturers and dates on new machines. They were implementing Jack Tramel's vision of *computers for the masses, not the classes* after all.

<figure><p><img src="https://rubenerd.com/files/2023/commodoreplus4-plankton@1x.jpg" alt="View of the open Plus/4 showing the PLAnkton chip replacement installed." style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2023/commodoreplus4-plankton@2x.jpg 2x" /></p></figure>

*(Yes, this is a photo of my [gorgeous Plus/4](https://rubenerd.com/i-fixed-my-commodore-plus4/ "I fixed my beautiful little Commodore Plus/4!"); it was the only internal photo I had on hand. Note the new PLAnkton chip that replaced a dead PLA).*

Today's Commodore fans have access to better stuff. We use dual wipe, round hole, or even ZIF sockets in lieu of the single-wipe monstrosities Commodore deployed... if they even socketed certain chips in the first place. This price difference would have been huge at the scale Commodore manufactured these machines, but in small batches the quality of life improvement is worth it.

These parts also had to be painstakingly reverse-engineered, reproduced, tested, and distributed by fans. This can't have been easy, especially given the broad range of boards Commodore produced over the lifetime of the machine, all of which have to maintain compatibility. I'm frankly in awe of what the community has been able to achieve, especially in the last decade. While I begrungingly pay for hardware from soulless megacorporations to live and work, paying a premium for *these* parts is worth it to show my appreciation. Thank you for all you do!

I said at the start that I'm happy to even have access to these at all, which is also worth repeating. It's incredible that schmucks like you and I have access to equipment to produce small batches of components that only companies the size of Commodore could have done in the 1980s.

The fact these components are within the upper and lower bounds of the original C64's selling price is impressive, especially when one considers the *orders of magnitude* more machines Commodore were selling compared to someone with a Tindie store. 


### Conclusion

*SO... ing machine*, was the C64 cheaper than these modern reproductions? At one point no, but at another point yes.

It might still be cheaper to buy a second-hand Commodore 64 in certain circumstances, either in working condition or for refurbishment with modern parts. I'm building a modern C64 for the opportunity, but I also have an original kit.

But just as a self-hosted compiler is desirable to language designers, it's also good to know we can [build modern versions](https://rubenerd.com/thoughts-on-an-entirely-new-commodore-64/ "Thoughts on an entirely new Commodore 64") without old or decaying hardware as well. That's priceless.
