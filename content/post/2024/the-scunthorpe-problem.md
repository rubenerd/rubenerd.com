---
title: "The Scunthorpe Problem"
date: "2024-11-21T09:26:59+11:00"
year: "2024"
category: Thoughts
tag:
- language
location: Sydney
---
I was talking with a friend recently about an email of theirs running afoul (🐔) of another aggressive filter system, because they dared to to talk with someone called *Dickson*. I know right, they're the absolute worst.

For those unfamiliar, this is [The Scunthorpe Problem](https://en.wikipedia.org/wiki/Scunthorpe_problem). As Wikipedia describes:

> The Scunthorpe problem is the unintentional blocking of online content by a spam filter or search engine because their text contains a string (or substring) of letters that appear to have an obscene or otherwise unacceptable meaning.

It then describes an issue that could apply to plenty more technical problems, not least this one:

> The problem arises since computers can easily identify strings of text within a document, but interpreting words of this kind requires considerable ability to interpret a wide range of contexts, possibly across many cultures, which is an extremely difficult task.

I'm actually surprised cross-cultural issues like this don't come more often, given the diversity and range of people online (and yes, I expect AIs to make this much worse, not better).
