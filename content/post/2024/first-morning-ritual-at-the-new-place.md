---
title: "First morning ritual at the new place"
date: "2024-12-07T08:52:35+11:00"
abstract: "Today was the first time properly sitting here doing my early morning ritual of coffee and RSS."
year: "2024"
category: Thoughts
tag:
- personal
location: Sydney
---
This is the first post written from the balcony of our new apartment. One of the criteria Clara and I used while house hunting was whether the unit had a balcony to start with, had a nice view, was shaded, and if it avoided direct sunlight first thing in the morning.

Sitting outside is important to me. I wake up faster with fresh air, and I'm more productive working from home outdoors (unless it's a heatwave or the dead of winter). I can trick myself into thinking I'm sitting at an open-air coffee shop, which is my other happy place. That reminds me, Clara needs to paint a cafe sign for out here.

Today was the first time properly sitting here doing my early morning ritual of coffee and RSS. The views of the national park, the sound of birds, and the complete lack of road noise is stunning. Trains roll by sometimes, but the "quality" of that sound (for want of a better word) is far preferable to the constant drone of traffic, horns, and brakes. It's silly, but I like that I can wave at people, and occasionally a small kid will wave back :).

I grew up in apartments in Singapore that had what I'd call Frasier-esque views. We almost paid for a place in the Melbourne CBD that would have had the same thing. There's something to be said about feeling alive in the centre of town. But at the same time, half the appeal of Australia is the option to live closer to nature that is either difficult or impossible to back home.

Our new apartment has flaws, some of which we're only just discovering which is always fun. If we had more money we would have chosen something else. But for the price, and to stay in Sydney, I think we really lucked out. I feel the same sense of calm out here that I did when we stayed in the Blue Mountains. Just with more trains.

It's just started drizzling, and you can see fog rolling over the hills of the forest outside. Wow.
