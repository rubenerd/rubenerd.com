---
title: "RSS clients on Windows"
date: "2024-01-05T14:37:11+11:00"
year: "2024"
category: Internet
tag:
- rss
- web-feeds
location: Sydney
---
A friend on Mastodon asked what RSS clients I use or recommend on Windows, though he was quick to point out I use Feedland on the web as well.

My favourite used to be [RSSOwl](http://www.rssowl.org/), and most recently it's still being maintained as [RSSOwlnix](https://github.com/Xyrio/RSSOwlnix/releases). It's a Java application, which was never a problem for me because I was a Java developer back in the day.

In truth though, I don't use a version of Windows newer than 2000 anymore (because retrocomputing), so I'm probably not the best person to ask. Thesedays I tend towards cross platform tools, which RSSOwl and [Thunderbird](https://blog.thunderbird.net/2022/05/thunderbird-rss-feeds-guide-favorite-content-to-the-inbox/ "Thunderbird + RSS: How To Bring Your Favorite Content To The Inbox") still are.

Unfortuantely, desktop applications don't seem to support my preferred RSS reading experience, which is a *river of news* format. This is how sites like Sky Karen and Mastodon display updates, as opposed to treating them as an inbox with an unread counter. My experience is that I'm discouraged from reading when I see that counter tick up to infinity.

If anyone has an idea for a native, cross-platform river of news RSS reader, I'd love to hear. Though I suppose the web will always be the easist.
