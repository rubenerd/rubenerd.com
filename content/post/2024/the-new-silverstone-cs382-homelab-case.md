---
title: "The new SilverStone CS382 homelab case"
date: "2024-07-16T13:17:33+10:00"
thumb: ""
year: "2024"
category: Hardware
tag:
- bsd
- cases
- freebsd
- homelab
- netbsd
location: Sydney
---
SilverStone announced the [CS382](https://www.silverstonetek.com/en/product/info/computer-chassis/cs382/) MicroATX case at the recent Computex in Taiwan, and it looks like an efficient box of wonders:

<figure><p><img src="https://rubenerd.com/files/2024/silverstone_304578@1x.jpg" alt="Offset view of the front of the case, showing the 8 drive bays, 5.25-inch bay, and 9.5mm optical drive bay. The door is swung open." srcset="https://rubenerd.com/files/2024/silverstone_304578@2x.jpg 2x" style="width:420px; height:329px;" /></p></figure>

It has breathable panels, dust filters, a power supply shroud, mounting options for MicroATX and MiniITX boards, and a smaller footprint than my venerable homelab Antec 300. It can support a 240mm AIO, but it's also efficiently laid out for a 168mm tower air cooler.

But the standout feature is its 8× tool-less drive sleds, which have mounting options for 3.5 and 2.5 SATA or SAS drives, and cooling mounted to the back of the cage. Most NAS cases top out at five drives, which is useless for using mirrored drives, and not really sufficient for ZFS RAID-Z2 or RAID6. Anyone using RAID5 today is getting the best bang for their buck, until it goes bang.

Other interesting features include a 5.25 drive bay (yay!), and even a separate 9.5mm optical drive slot! I could install my BD writer and a LTO drive, or use both for additional SSDs. The 5× PCI slots could accommodate beefy graphics cards, or plenty of expansion for a small bracket-mounted Raspberry Pi cluster, SAS controllers, and so on.

<figure><p><img src="https://rubenerd.com/files/2024/silverstone_304579@1x.jpg" alt="Side-view of the case, showing its efficient layout." srcset="https://rubenerd.com/files/2024/silverstone_304579@2x.jpg 2x" style="width:420px; height:369px;" /></p></figure>

This wasn't sponsored by SilverStone! I'm just glad people are still making hardware like this. Consumer-grade cases basically only target gamers now, who only want M.2 storage and maybe a 2.5 SSD. This would be a perfect homelab box for running FreeBSD with bhyve and OpenZFS, or NetBSD with nvmm, or a Penguin of some description.

Our Antec 300 homelab server is still rocking a Supermicro X11SAE-M MicroATX board with a Xeon E3-1240 v6. That... would fit in this. Uh oh.
