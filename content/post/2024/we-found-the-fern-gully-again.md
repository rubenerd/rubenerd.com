---
title: "We found the Fern Gully again"
date: "2024-04-01T09:22:11+11:00"
year: "2024"
category: Travel
tag:
- gardens
- melbourne
- nature
location: Melbourne
---
Clara and I went to the [Melbourne Botanic Gardens](https://rubenerd.com/wandering-around-the-melbourne-botanic-gardens/ "Wandering around the Melbourne Botanic Gardens") last week, but we didn't get to the [Fern Gully](https://rbg.vic.gov.au/melbourne-gardens/discover-melbourne-gardens/melbourne-gardens-living-collections/fern-gully/) in time. But over the weekend we went back to wander, and we *did* find it this time.

This is easily my favourite part of the park, and it even has a bench where you can sit and have a cheeky snack. It's cool, quiet, and shady, which is rather lovely.

<figure><p><img src="https://rubenerd.com/files/2024/melbourne-botanic-fern-1@1x.jpg" alt="" srcset="https://rubenerd.com/files/2024/melbourne-botanic-fern-1@2x.jpg 2x" style="width:500px; height:375px;" /><br /><img src="https://rubenerd.com/files/2024/melbourne-botanic-fern-2@1x.jpg" alt="" srcset="https://rubenerd.com/files/2024/melbourne-botanic-fern-2@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>
