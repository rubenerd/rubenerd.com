---
title: "Complexity, complexity everywhere"
date: "2024-02-22T17:23:57+11:00"
year: "2024"
category: Internet
tag:
- complexity
- design
- html
- rant
location: Sydney
---
So the story goes, is a phrase with four words. Woz's creativity and engineering prowess are the stuff of legend, in particular his *ruthless* ability for optimisation. He could look at a printed circuit board, rewire it, and remove half the components while maintaining the same functionality.

I do not possess such skill, at least not when it comes to electronics. But every now and then I look at the page source on a random website, and am bowled over at how much *cruft* there is. I then feel this irrational and unproductive urge to clear it all out, and recreate it with basic HTML tags, just to remind myself it's still possible.

Someone shared a link to a CNN article with me recently. It was 9,667 lines of HTML, for 30 paragraphs of text. Even with all the extra things pages are expected to serve now... *9,667 lines*. And they weren't even word-wrapped.

The web hasn't been crafted by human hands for a long time, but it still surprises me just how deep the rabbit hole goes on these pages now. Do you really need twenty nested divs to render a sentence, or an entire block of JavaScript to show a code sample? Evidently the answer is yes.

My Design and Tech teacher in high school often said she admired people who were so committed to their craft, they'd put effort into things people wouldn't see. Her example was a tradie who smoothed out a perfect layer of adhesive before placing a tile, where a blob or two would suffice. The only people who'd likely ever notice were the wreckers or renovators who'd attack the wall decades later.

HTML is blobs now. Which is probably fine, and I should get over it. I should also exercise more, eat more salads, and spend less on coffee.
