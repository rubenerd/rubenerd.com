---
title: "Do you run more than one BSD?"
date: "2024-09-28T23:01:41+10:00"
year: "2024"
category: Software
tag:
- bsd
- drangonfly-bsd
- freebsd
- netbsd
- openbsd
location: Sydney
---
I'm running a bit of an experiment. I've [got a poll on Mastodon](https://bsd.network/@rubenerd/113214053884352198) asking BSD people how many of the OSs they run. I've noticed I'm not the only one who run two or more stacks, depending on the requirements or my whims that day.

It's live for the next week, after which I'll tabulate the results and post about it here. If you don't have a Mastodon account, feel free to [ping me directly](https://rubenerd.com/about/#contact).
