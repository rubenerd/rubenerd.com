---
title: "A 2024 Yellow Pages monitor stand"
date: "2024-01-20T18:44:23+11:00"
year: "2024"
category: Hardware
tag:
- books
- family
- retrocomputing
- traditions
location: Sydney
---
I'd like to tell you a tale. It's a story of suspense, romance, intrigue, espionage, and a single sentence that's full of lies and baseless assertions. It also involves a phonebook, a paper-based device that, despite the name, only dedicates a *small* part of its exhaustive listings to selling you phones. Or more specifically, listing *vendors* who sell phones, or fix them. I'm already starting to lose the plot, and I'm only a paragraph into this post.

I had a hulking big 19-inch Dell CRT when I was a kid. Monitors today still don't match the colour depth, contrast, and sheer weight of that beast, and it even had a vastly superior 4:3 aspect ratio. It was great as a display, and a tremendous way to get exercise when one had to move house.

Problem was, the stand upon which it pivoted and moved around lacked sufficient height for my ergonomic requirements. I had a short desk, and by the time I was into my last growth spurt as a kid, I found myself looking down onto it from my advanced height. I needed something to prop it up to avoid neck strain. But what with?

My first experiments involved using textbooks, but I soon ran into the issue that I needed to *take them to classes* sometimes. I tried a tissue box, but it flattened like a pancake the moment it bore the full weight of the CRT. I even used a small set of IKEA tabletop drawers, but again, the weight of the CRT pushed down on the frame with *just* enough force that one couldn't open the drawers.

Something else was needed, and it was delivered one fateful school holiday. *The Yellow Pages!*

*The Yellow Pages* had all the hallmarks of a great monitor stand. It was thick, dense, rigid, and printed a cheerful colour. It got to the point where my mum would deliver new editions to me when it arrived, and we'd perform a little ritual of replacing the previous year's edition under my monitor. It was *completely* pointless, and it was wonderful.

<figure><p><img src="https://rubenerd.com/files/2024/yellow-pages@1x.jpg" alt="Copy of the most recent Yellow Pages for Sydney" srcset="https://rubenerd.com/files/2024/yellow-pages@2x.jpg 2x" style="width:500px; height:333px;" /></p></figure>

Fast forward to 2024, and I couldn't help but notice a copy of our local *Yellow Pages* thrown by the side of the road. It was clear from the poor condition of the book's plastic bag that nobody had come to collect it for a while. Frankly, I was shocked that these were even still being printed. I picked it up and  rescued it from its plastic confides.

Modern copies of the *Yellow Pages* are but a shadow of their former chunky glory of thicc proportions, for reasons we all know. Still, there was something oddly comforting about opening it up and being able to find listings for various tradespeople and other services, right there in black and white. No popups, no newsletter signups, no tracking, no disingenuous cookie notices, no autoplaying videos. Just a name and a number. What a concept!

<figure><p><img src="https://rubenerd.com/files/2024/yellow-pages-stand@1x.jpg" alt="Copy of the most recent Yellow Pages for Sydney" srcset="https://rubenerd.com/files/2024/yellow-pages-stand@2x.jpg 2x" style="width:500px; height:333px;" /></p></figure>

But I had a different use case in mind. The LCD on my retrocomputer KVM desk was just *ever* so slightly too low for my tastes; enough so that I'd been evaluating getting a VESA stand of some description. And wouldn't you know it, the diminutive size of the current *’Pages* was a perfect fit!

I like being able to keep silly traditions like this alive again after all this time, even if the person you shared it with has long since gone. Maybe somehow she left it out for me, hoping I'd find it. 🥹
