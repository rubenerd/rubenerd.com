---
title: "Boeing and Spirit Aerosystems"
date: "2024-07-11T08:53:36+10:00"
thumb: "https://rubenerd.com/files/2024/alaska-737max-kirkxwb@1x.jpg"
year: "2024"
category: Travel
tag:
- aviation
- boeing
- news
location: Sydney
---
Aviation nerds like me have been watching reports of Boeing buying out Spirit Aerosystems with a mix of interest and confusion. How would it even work? Is it the right thing to do? What does such a move represent for the beleaguered US aircraft manufacturer?

Spirit Aerosystems is a contractor for Boeing, spun off from the parent company in 2005. It was one of many eyebrow-raising decisions made after McDonnell Douglas [bought Boeing with Boeing's money](https://qz.com/1776080/how-the-mcdonnell-douglas-boeing-merger-led-to-the-737-max-crisis "QZ: The 1997 merger that paved the way for the Boeing 737 Max crisis"). Spirit is responsible for manufacturing the fuselage and other components for the 737 narrow-body family, by far the most important aircraft on Boeing's balance sheet.

The 737 has obviously been in the news for all the wrong reasons. The MAX family landed Boeing in court for the installation of a control system called [MCAS without the knowledge of pilots](https://rubenerd.com/pbs-frontline-documentary-on-the-737-max/ "PBS Frontline documentary on the 737-MAX"), which lead to two fatal crashes. More recently, a [door-plug blowout](https://www.bbc.com/news/world-us-canada-67915771 "BBC: Alaskan Airlines flight 1282: Key questions behind door plug blowout") caused decompression and an emergency landing that by sheer luck didn't take further lives.

<figure><p><img src="https://rubenerd.com/files/2024/alaska-737max-kirkxwb@1x.jpg" alt="Alaskan Airlines Boeing 737 Max photo by KirkXWB" srcset="https://rubenerd.com/files/2024/alaska-737max-kirkxwb@2x.jpg 2x" style="width:500px;" /></p></figure>

*[Photo by KirkXWB](https://en.wikipedia.org/wiki/File:Alaska_737_Max_9.jpg) on Wikimedia Commons.*

I've mentioned on Mastodon how sad it is that Boeing torched decades of public trust, seemingly overnight. The company and its aircraft have gone from *[if it's not Boeing, I'm not going](https://www.thecut.com/article/boeing-flight-anxiety-travel-safety.html)*, to being the punchline on late night TV. Based on demand, online booking services have rolled out features to allow travellers to avoid booking passage on their aircraft. Meanwhile, Airbus can't produce the A320 family fast enough, with an order backlog stretching into the 2030s.

This gets us back to Spirit Aerosystems. [Spirit were responsible](https://www.abc.net.au/news/2024-01-08/alaska-airlines-boeing-737-max-9-door-plug-search-blowout/103292708 "ABC: A Boeing's door 'plug' has been found in a backyard after a mid-air blowout. Here's what we know") for building, installing, verifying, and delivering the aforementioned door plug and fuselage to Boeing. While Boeing are ultimately responsible for the aircraft they sell, buying Spirit would help Boeing address these quality control issues by bringing the teams back in-house. At least, so the logic goes.

But there are a few problems. The biggest is that Boeing simply doesn't have the cash. As AviationPros reported, their quality control issues have resulted in a [737 MAX production cap being imposed](https://www.aviationpros.com/aircraft/news/55055320/faa-keeps-cap-on-boeing-737-max-production-as-safety-concerns-persist "AviationPros: FAA Keeps Cap on Boeing 737 Max Production as Safety Concerns Persist"), and as SimpleFlying reminds us, their certification of other airframes has been [repeatedly delayed](https://simpleflying.com/boeing-777x-737-max-7-10-certification-update/ "Simple Flying: What's The Latest With Boeing's Certification Efforts For The 777X And 737 MAX 7 & 10 Models?"). These, along with a litany of other issues, have deprived Boeing of sorely-needed capital. Leeham News reported they're [looking at a stock swap](https://leehamnews.com/2024/06/25/boeing-simply-cant-afford-a-cash-deal-for-spirit-aerosystems/ "Leeham News: Boeing simply can’t afford a cash deal for Spirit AeroSystems"), but even that seems shaky.

Spirit have also diversified since being spun off from Boeing, with their factories producing parts for companies like... *Airbus!* Petter over on [Mentour Pilot](https://www.youtube.com/channel/UCwpHKudUkP5tNgmMdexB3ow "Mentour Pilot's YouTube channel") has done several videos discussing the complexity that would result in demerging and splitting Spirit along these lines, and how Airbus wouldn't be in a hurry to help their arch rivals carve up Spirit to make it easier and cheaper for them.

There's also the broader question about whether buying Spirit would solve Boeing's issues. Airbus famously uses a network of disparate suppliers across Europe and North America to build their aircraft, which last I checked were flying off the proverbial shelves. Boeing also canned the reasonable idea of [moving their HQ back](https://rubenerd.com/boeing-board-against-hq-move-back-to-seattle/ "Boeing board against HQ move back to Seattle") from Chicago, which will only make repairing the rift between business and engineering that much harder.

I hope they can turn this plane around. But management have to want to.
