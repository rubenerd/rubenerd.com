---
title: "“You’re not smiling enough”"
date: "2024-09-13T14:04:20+10:00"
year: "2024"
category: Internet
tag:
- authentication
- biometrics
- security
- verification
location: Sydney
---
You know that Treehouse of Horror episode of *The Simpsons* where the family is bundled off for some "Re-Neducation", and everyone's faces are forced into a smile with tenterhooks? It's *such* a visceral image I can still remember it decades later. And weirdly, I feel as though I just experienced something a bit similar myself.

I had to verify my identity for some documents, and the online form helpfully offered to film my face in various poses for... reasons. A bit weird, but I guess that's the brave new future in which we find ourselves.

As I positioned myself for the photo, an outline of a human head appeared on the screen for me to direct how I should be posing. I wheeled the chair closer, looked into the lifeless circle of glass that was attempting to capture my humanity, and waited.

Nothing happened.

I moved my head around slightly, as though I were training a new fingerprint scanner. Some still require you to move your thumb slightly to capture as much ridge detail as possible, especially from the sides and at weird angles. Maybe my head was...

... wait no, I was being given an instruction:

> Now, give a **BIG** smile!

I... pardon?

I've posed for enough passport and other document photos to know the drill. Look neutral, have no personality, take off hats and glasses, stare directly at the camera, don't think too much about security theatre. Except, this time, I was being asked to *smile*.

Whooaaaoo~ *[this time](https://www.youtube.com/watch?v=v0GqXRpNc9I)* ♫.

It wasn't the instruction itself that was so unsettling, but rather the way in which it was delivered. A chipper, Ned Flandian voice bellowed through the speaker, animated by a stylised head&mdash;without eyes&mdash;showing what a smile looks like.

> Just relax, and let the hooks do the work!

I tried multiple times. No matter how enthusiastic and animated I tried to look, it refused to accept what I was doing. I subsequently failed the test. It was creepy as all hell, and I left reassured with the knowledge that I'm not me *or* human.


### There are a few things here

Online identity is hard. An animated face showing a range of emotions is harder&mdash;but not impossible&mdash;to spoof than a static image. I reckon you could pull it off with a sufficiently-detailed mask and some theatre makeup, but I concede it does introduce a higher barrier to entry for wannabe scammers and identity thieves.

Here comes the proverbial posterior prognostication: *but...* when we deal with people in the real world, we risk entrenching stereotypes and ableism. And as the saying goes, if you *assume* something, you're making an arse out of a molehill. Wait, what?

Two things make these sorts of tests difficult for me. Well, three, if you account for me not entirely trusting an opaque identity system for which I can't interrogate its operation.

First, I don't smile in the stereotypical Western way. I naturally express joy by smiling with my eyes, not my mouth. My eyes crease, and I do what my late mum referred to as [Michael Franks Mouth](https://michaelfranks.com/). It's a bit like a restrained, upcurled pout? I know, it's weird, but I'll bet you know others who do this too.

This used to *really* shit me when I was a kid. I'd be at a party, or a group photo among Australians, and people would say "come on Ruben, smile!" I could be having a great time, and expressing happiness at being there, but because I wasn't smiling with my mouth like Mr Happy, people would assume I was being a buzzkill. Curiously, this never happened when I was hanging out with my Chinese, or *especially* my Japanese friends in Singapore, because they also smile more with their eyes. Hey look, cultural considerations in tech!

The second issue is more mental. I've made huge progress in my social anxiety in the last decade, but there are a few things I still can't stand. Selfie cameras are one of them. Mirrors are tolerable, but there's something about seeing a (mostly) live image of myself coming back on a phone that's *deeply* uncomfortable. It's not shyness or revulsion; more an escalating fear without a known cause. It's why I'd love to do videos about travel and retrocomputers one day, but I'll be *behind* the camera instead.

These auth systems don't accommodate for how *everyone's* faces or minds work. Which I suppose shouldn't be surprising.

