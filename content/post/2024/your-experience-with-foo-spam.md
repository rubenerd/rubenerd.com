---
title: "“Your experience with $FOO” spam"
date: "2024-08-21T09:25:33+10:00"
year: "2024"
category: Internet
tag:
- spam
location: Sydney
---
I've noticed more people trying to *engage* by sending spam in the form of a question. Then, when they inevitably don't get a response, they'll send a follow-up with another question:

> It would be great to chat about your pipeline. We work with many game studios, maybe we can learn from each other?
> 
> > Are you using Perforce? What do you like about it, what not?
> >
> > > Hey Ruben,
> > >
> > > We are using Git in game development. I am very interested in your experience. How about a quick call to discuss our experiences?

I'm intrigued about what they'll send next! A question about transitioning to hg or cvs, perhaps?

