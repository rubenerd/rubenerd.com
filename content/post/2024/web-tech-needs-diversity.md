---
title: "Web tech needs diversity"
date: "2024-10-05T11:36:41+10:00"
year: "2024"
category: Hardware
tag:
- cpus
- design
- security
location: Sydney
---
Hey, [remember Crowdstrike](https://rubenerd.com/the-day-the-crowdstrike-died/)? That update to Windows security software that affected airports, hospitals, supermarkets, schools... and clients who called me desperate to know why their VMs had vanished? It was fun! By which I mean it was an unmitigated disaster, and the ultimate example of a technical externality bourne by everyone.

I haven't seen much of a postmortem discussion on this; certainly not to the same extent as the wall-to-wall coverage those BSODs garnered. I suspect the press got their story, social media got their memes, and we all went on with our lives. Well, until the next one hits.

<figure><p><img src="https://rubenerd.com/files/2024/crowdstrike-bsod@1x.jpg" alt="A BSOD on a terminal in a local supermarket" srcset="https://rubenerd.com/files/2024/crowdstrike-bsod@2x.jpg 2x" style="width:500px; height:333px;" /></p></figure>

Of the people who *have* been talking about it, the discussion has centred around the companies involved, the processes that lead to the faulty patch being released, the economic and social fallout that such a massive, global issue can cause, and some ironic comparisons being made between malicious actors and security vendors. Why take your actor down with an evil ploy, when they'll do it themselves by accident?

I'm surprised though that the biggest issue was largely unacknowledged (save for some commentary about how appropriate it is to run Windows in an airport). The *scale* of the problem could only occur **because of how homogeneous our global IT infrastructure has become**. The ubiquity of one vendor's tech all but guarantees such issues are global in scope and impact. That fact alone should be ringing alarm bells.

Take this example of the TOP500 supercomputers by processor family, as visualised by [Moxfyre on Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Processor_families_in_TOP500_supercomputers.svg). Granted we can't directly extrapolate this to general web servers or end-user devices like workstations and phones, let alone software. Here comes the proverbial posterior prognostication, *but*... we can broadly see similar trends everywhere.

<figure><p><img src="https://rubenerd.com/files/2024/top500@1x.png" alt="" srcset="https://rubenerd.com/files/2024/top500@2x.png 2x" style="width:500px; height:355px;" /></p></figure>

I talked again last week about [browser monocultures](https://rubenerd.com/an-october-2024-browser-monoculture-update/), but the same issue is happening across the industry, from consumer operating systems and software to industrial control equipment and servers. Granted we have ARM (and to a far lesser extent RISC-V) offering an alternative, but even then we're typically running similar software and platforms across these architectures.

I miss the days when there was more technical diversity in the industry purely from the silly perspectives of fun and novelty. But it's also a security issue. We intuitively understand how important genetic diversity is in nature for the viability and robustness of ecosystems, and I'd argue it's just as important here. It's ridiculous that a single software patch can bring down that many systems.

Standards are great, but we need more implementations! Icanhaz a modern SPARC64 server running NetBSD?
