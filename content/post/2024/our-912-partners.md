---
title: "Our 912 partners"
date: "2024-12-18T13:38:38+11:00"
abstract: "“Responsible use of your data”... wow."
year: "2024"
category: Internet
tag:
- advertising
- privacy
location: Sydney
---
I used a fresh browser install this morning without an adblocker, and saw this incredible popup:

> **Responsible use of your data**. We and our 912 partners process your personal data, e.g. your IP-number, using technology such as cookies to store and access information on your device in order to serve personalized ads and content, ad and content measurement, audience research and services development. You have a choice in who uses your data and for what purposes. You can change or withdraw your consent any time from the Cookie Declaration or by clicking on the Privacy trigger icon.

912 partners? Responsible?

People give the EU crap for mandating these sorts of notices, ignoring the fact that if you didn't have "912 partners" you wouldn't need to notify us of said. At least they're being forced to disclose this.
