---
title: "The Zed editor"
date: "2024-03-08T10:04:55+11:00"
year: "2024"
category: Software
tag:
- editors
- electron
location: Sydney
---
There's a [new editor on the block](https://zed.dev/) with a [familiar](https://texteditors.org/cgi-bin/wiki.pl?ZED "ZED Editor") name:

> Zed is a high-performance, multiplayer code editor from the creators of Atom and Tree-sitter. It's also open source.
>
> Zed efficiently leverages every CPU core and your GPU to start instantly, load files in a blink, and respond to your keystrokes on the next display refresh. Unrelenting performance keeps you in flow and makes other tools feel slow.

It's ironic that the team responsible for Electron claim their tool is fast because it uses Rust instead. It'd be akin to Bjarne Stroustrup saying his new tool is simpler because it's written in C.

Still, this is great news at a meta level! As I said about [1Password's rewrite](https://rubenerd.com/1password-electron-flareup-is-good-news/ "1Password Electron flareup is good news for the industry"), we now have another signal for software quality, performance, security, efficiency, and accessibility. Not using Electron is a *feature*.

**Update:** A few of you have pointed out it has AI integration. Ew. I wasn't intending on running before, but now I think it's best avoided.
