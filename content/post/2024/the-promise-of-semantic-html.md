---
title: "The promise of semantic HTML"
date: "2024-12-11T23:36:29+11:00"
abstract: "Or when people swapped b for strong, without thinking why."
year: "2024"
category: Internet
tag:
- accessibility
- design
location: Sydney
---
There may be a better term for this, but I'm going with it! But first, one of my shaggy-dog introductions.

As a tragic late-thirty-something, I'm old enough to remember when CSS was being debated and introduced. Source code for web pages in the 1990s was a hot mess, with content intertwined with presentation like pasta with so much sauce ("content" in this case referring to the contents of a page, not [creative works](https://rubenerd.com/patrick-willems-discusses-content/ "Patrick H Willems discusses “content”")).

Elements were used inappropriately all over the place, but poor old tables were the most egregious example of this pasta-based sauce. Spaghetti code, you could say. Without a robust, reliable mechanism to format pages into columns&mdash;something people very quickly wanted to do as screen resolutions increased&mdash;tables elements were abused from containing tabular data into displaying the contents of entire pages. That is, unless you went down the avenue of using frames.

I helped with the school's website back in the day, and I remember table-based layouts being one of those technical solutions that work incredibly well, until they don't. It was a nightmare tweaking the layout across one page, let alone consistently across every page on even a small site. I really wish I knew `awk(1)` back then! 

CSS was designed to alleviate both these problems. It promised to decouple content from presentation, allowing a site's design to be defined in a separate file that could be referenced everywhere. `id` and `class` attributes could be used to provide additional "hooks" for when styling all elements of a specific type wasn't desired (say, every `h1`). I still think you should be able to style based purely on position most of the time (an `h1` in `header` is different from an `h1` in `article`), but the functionality is there if necessary.

Most importantly to me: CSS allowed elements to return to their **original semantic meaning**. Or, so they were supposed to. Headings could be headings again, not just big text. Abbreviations could be abbreviations, not massive footnotes. Tables could contain tabular data. And so on.

I've argued the success of this was... mixed. Today, sites contain hundreds of nested `div` elements with machine-generated names that have no purpose other than to achieve a specific look. Put another way, that same mess of spaghetti code translated onto another site would make no visual sense. The use of the elements to define a document structure is gone; it's all for design. This, coupled with the trend away from standards like XHTML mean that pages are the end-rendered product, not a base from which you can easily transform to whatever you need. Tragics like me consider that a missed opportunity, but that's the way the wind blows... and we get our revenge with silly things like [Omake](https://rubenerd.com/omake.xml) and [ruben.coffee](http://ruben.coffee/)!

Wow, we're really getting into the weeds here. Watch where you step; this is written in Australia and I suspect there are dozens of poisonous snakes and spiders here.

The other side effect of this change was a bit of a regression back to semantic irrelevance. After being told for decades that table-based layouts are a bad idea, there are now guides online about how to render a `div` like a `table`, even if the `div` is to contain tabular data! Webdevs were told that `b` for bold text, and `i` for italics were inline style that should be avoided, so everyone swapped them for `strong` and `em` for emphasis, because they rendered the same. Is all bold and italic text supposed to be emphasised? Probably not.

Perhaps it was too much to expect the Web to be written in a way that makes semantic sense. I still encounter technical Word documents written by people that have manually-formatted headings, so a table of contents can't be automatically generated. In the words of the Pet Shop Boys via a 1996 page formatted with tables, *se a vida é*.
