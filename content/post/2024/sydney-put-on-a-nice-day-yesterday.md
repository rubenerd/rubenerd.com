---
title: "Sydney put on a nice day yesterday"
date: "2024-12-30T10:35:28+11:00"
abstract: "Warm but not too hot, and great for strolling around in the CBD."
thumb: "https://rubenerd.com/files/2024/sydney-nice-day@2x.jpg"
year: "2024"
category: Travel
tag:
- australia
- sydney
location: Sydney
---
<figure><p><img src="https://rubenerd.com/files/2024/nice-day-sydney@1x.jpg" alt="View looking down George Street in the Sydney CBD with a clear blue sky and people wandering about. Also, no cars!" srcset="https://rubenerd.com/files/2024/nice-day-sydney@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>
