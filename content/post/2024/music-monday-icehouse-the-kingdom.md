---
title: "Music Monday: Icehouse, The Kingdom"
date: "2024-03-11T16:46:13+11:00"
thumb: "https://rubenerd.com/files/2024/yt-Y8sdIjcYdH8@1x.jpg"
year: "2024"
category: Media
tag:
- 1980s
- australia
- icehouse
- music
- music-monday
location: Sydney
---
This week's *[Music Monday](https://rubenerd.com/tag/music-monday/)* takes us back to the mid 1980s, and one my favourite Australian bands of the era. *[Electric Blue](https://rubenerd.com/icehouse-electric-blue/ "Icehouse, Electric Blue")* might have been the breakout song from this album, but this is the real keeper for me.

I know the *Rubenerd Law of Music* says that everything sounds better with brass, but a silky 80s pop song with prominant keyboards is pretty great too.

<figure><p><a target="_BLANK" href="https://www.youtube.com/watch?v=Y8sdIjcYdH8" title="Play The Kingdom"><img src="https://rubenerd.com/files/2024/yt-Y8sdIjcYdH8@1x.jpg" srcset="https://rubenerd.com/files/2024/yt-Y8sdIjcYdH8@1x.jpg 1x, https://rubenerd.com/files/2024/yt-Y8sdIjcYdH8@2x.jpg 2x" alt="Play The Kingdom" style="width:500px;height:281px;" /></a></p></figure>

