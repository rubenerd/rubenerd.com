---
title: "Gymnastics New Zealand’s new attire rules"
date: "2024-04-07T05:29:25+10:00"
thumb: ""
year: "2024"
category: Thoughts
tag:
- gymnastics
- new-zealand
- sport
location: Sydney
---
New Zealand's new attire... New (Zealand, attire)? Damn my compulsive need to factorise *all the things*.

I saw a few outlets reporting on this, so I [went to the source](https://www.gymnasticsnz.com/attire/ "What can you wear to compete?")\:

> Gymnastics NZ Regulations – All Codes [..] Athletes may choose to wear gymnastics shorts or leggings over their leotard.

Funny story, one of [my first posts in 2005](https://rubenerd.com/murphy-brown-windows-xp-and-cold-gymnists/ "Murphy Brown, Windows XP, and cold gymnasts") touched on how cold female gymnists [sic!] must be. I often found myself shivering while waiting for my turn in high school PE, because our teacher liked having the air con in that Singaporean gymnasium set to *Antarctic*. And I was wearing far more.

These updated rules by Gymnastics New Zealand make sense to me, and represent a long-overdue change that the <abbr title="International Gymnastics Federation">FIG</abbr> should adopt globally. Frankly, I never understood why female aerobic gymnasts couldn't wear the same skirts that their rhythmic team mates could if they wanted either. It should be their choice either way.

<figure><p><img src="https://rubenerd.com/files/2024/erika-akiyama@1x.jpg" alt="Erika Akiyama performing a rhythmic routine, by David Thévenot" srcset="https://rubenerd.com/files/2024/erika-akiyama@2x.jpg 2x" style="width:500px; height:305px;" /></p></figure>

*Photo of Erika Akiyama performing in 1988 by [David Thévenot on Wikimedia Commons](https://en.wikipedia.org/wiki/File:Erika_Akiyama.jpg), still one of my favourites. Erica I mean, though David might be an upstanding gentleman as well.*

Gymnastics is still one of the few sports I watch with any sort of frequency, especially since I lost interest in Formula 1. I love that it's a personal effort with artistic freedom and creativity that still accrues points for a team. It tickles something in my introvert brain.

As an aside, maybe I need to give Formula E a try.
