---
title: "When Tramiel left Commodore for Atari"
date: "2024-05-20T14:18:15+10:00"
year: "2024"
category: Hardware
tag:
- atari
- commodore
- history
- retrocomputers
location: Sydney
---
Commodore CEO Jack Tramiel leaving Commodore for rival Atari was one of the more surreal events of the 1980s home computer scene. Jack resigned after a disagreement with Irving Gould, Commodore's chairman at the time.

[Chip Loder wrote](https://appleinsider.com/inside/macos/tips/how-to-use-classic-atari-commodore-and-sinclair-software-on-your-mac) for Apple Insider, emphasis added:

> In 1994, largely due to competition from Apple and Microsoft, Commodore closed its doors forever. 
>
> Strangely, **after** Commodore closed, its original founder Jack Tramiel and several key employees bought the remnants of Atari after the video game crash of 1983 and formed a new company, Atari Corporation. Many of the original Commodore employees were instrumental in the development of Atari ST and 7800.

That may have been a typo, because the rest of the paragraph tracks. Jack Tramiel bought Atari more than a decade earlier, *before* Commodore closed.

It's interesting to think about what Commodore may have done if Tramiel remained. Would a Commodore ST have come out, and Atari bought Amiga instead? Would Commodore Germany have come out with their PC clone line at all?
