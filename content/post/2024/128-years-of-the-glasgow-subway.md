---
title: "128 Years of the Glasgow Subway!"
date: "2024-12-17T15:05:16+11:00"
abstract: "I’m fascinated by it, and hope to travel on one day :)."
year: "2024"
category: Travel
tag:
- public-transport
- scotland
- trains
location: Sydney
---
[Via Wikipedia](https://en.wikipedia.org/wiki/Glasgow_Subway)\:

> The Glasgow Subway is an underground light metro system in Glasgow, Scotland. Opened on 14 December 1896, it is the third-oldest underground rail transit system in the world after the London Underground and the Budapest Metro.

The Subway operates as a loop line, with some of the coolest looking rolling stock in the world. They're even narrower than the London Underground's famous tube lines, as <a href="https://commons.wikimedia.org/wiki/File:Glasgow_Subway_Stadler_unit_at_West_Street_160624_(53794657872).jpg">Foulger Rail Photos photographed</a>:

<figure><p><img src="https://rubenerd.com/files/2024/glasgow_subway_stadler_unit@1x.jpg" alt="Glasgow Subway Stadler unit at West Street, by Foulger Rail Photos" srcset="https://rubenerd.com/files/2024/glasgow_subway_stadler_unit@2x.jpg 2x" style="width:500px; height:333px;" /></p></figure>

I'm fascinated by it, and hope to travel on one day :).

*(For the record, my mum's side of the family are Scottish, though from the highlands. I suspect the population density, geography, and terrain would make building such a railway there difficult. You could say they Pict a different place).*
