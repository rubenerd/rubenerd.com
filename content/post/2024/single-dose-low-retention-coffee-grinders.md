---
title: "Single dose, low retention coffee grinders"
date: "2024-10-09T09:32:14+11:00"
thumb: "https://rubenerd.com/files/2024/fellow-ode@1x.jpg"
year: "2024"
category: Hardware
tag:
- coffee
location: Sydney
---
This is going to sound silly, but I'm *so relieved* and oddly validated that these things exist! I have no idea how the industry tolerated how this was done before.

I'll explain what these grinders are, but first let me wind back the clock to the early 2000s. The short-lived *swing revival* was in the rearview mirror. Palm had released their m100-series PDAs. Tragic nerds like me had the *Love Hina* OP on a loop in our heads. *Dah dah da-da dah! Dah dah da-da dah!* And my parents started buying what James Hoffman referred to as "bean to cup" coffee machines, starting with this exact model of *DeLonghi Magnifica*.

<figure><p><img src="https://rubenerd.com/files/2024/delongi-magnifica@1x.jpg" alt="The DeLonghi Magnifica" srcset="https://rubenerd.com/files/2024/delongi-magnifica@2x.jpg 2x" style="width:375px;" /></p></figure>

Compared to a traditional espresso or filter machines that require human intervention and manual steps for some of the coffee making process, "bean to cup" machines boil, grind, tamp, and brew for you with a few button presses. They have large tanks for water, chambers for expelled grounds, and a large hopper you fill with fresh bag of coffee beans. You could think of them as domestic versions of commercial machines you might find in an office, hotel buffet, or those IKEA meatball restaurants.

*(Whenever I walk past Council Chambers in our suburb, I wonder how many expelled coffee grounds are in it, and I chuckle. Then people walking past notice I'm chuckling, and it's awkward. "No I'm not weird, I'm chuckling because these coffee machines have chambers for coffee grounds and... wait, why are you running away)?!*

Such machines cop flack from aficionados&mdash;perhaps rightly so under certain circumstances&mdash;but they were my formative experience with brewing coffee at home. I was interested to learn how beans in one end resulted in a tasty liquid out the other. They were massive black boxes that made weird mechanical sounds, and I wanted to learn how and why they worked the way they did. Maybe one day I'll do a post about that, including how I might have broken one machine digging around. Whoops.

But leaving aside how much of a pain these machines were to clean and maintain, they came with a massive caveat: those fresh coffee beans expired *quickly*... and *especially* in tropical Southeast Asian climates. Even before I really knew what "good" or "bad" coffee was, I could easily taste the difference between coffee made with fresh beans, and beans that had sat in the hopper for a couple of weeks. It was thinner, harsher, and bordered on damp cardboard undertones. After one fateful holiday we came back to find some green fuzz, which was fun.

The mere existence of this coffee bean hopper was strange to me, because all the fresh coffee bags we bought had *very* prescriptive advice for storage that such a device violated. Don't leave beans in the open exposed to sunlight, keep them in airtight containers, don't look at the beans the wrong way... oh but ignore that if you use a bean to cup machine? It made no sense.

As I learned more about the brewing process, and how much freshness played a role in coffee's flavour (who knew!?), I started abusing the hoppers in these machines. When I'd make a coffee for myself or my mum, I'd only pour in as many beans as I needed to use, and store the rest away. Just that one change resulted in *consistently* tastier coffee, to the point where my mum asked if we were buying different beans. I even experimented with filling the machine with more or less coffee with the same quantity of water. I was learning about coffee dosing without even realising it!

While it was an improvement, this hack wasn't perfect. One problem was that there'd always be grounds left over after each brew, so I got into the habit of "purging" the machine by making a long black without grounds before use. This resulted in a murky cup of water each time which I had to throw away, but it was proof that there was still coffee in the machine that would have otherwise been mixed into the fresh stuff.

I'm sure my abuse of these machines contributed to their eventual failure, as I mentioned above. But it meant that when I moved out and started buying cheap brewers of my own, I had a slightly better idea of ratios and what flavours I liked.

<figure><p><img src="https://rubenerd.com/files/2024/fellow-ode@1x.jpg" alt="Photo of the Fellow Ode 2 coffee grinder" srcset="https://rubenerd.com/files/2024/fellow-ode@2x.jpg 2x" style="width:300px;" /></p></figure>

This leads us to today, and my *very* late realisation that there are modern coffee grinders that have two key features:

* **Single dose** is what it sounds like: you're only grinding enough coffee for one cup or serving of coffee. Because you're not leaving the beans in a hopper to go stale, you're getting a fresh grind each time. This is what I was abusing those machines above to do.

* **Low retention** is a design that reduces the amount of coffee left in the machine after a grind, to prevent the mixing of stale coffee with fresh that I used to experience above. Some expensive machines advertise zero retention, though no machine will ever avoid this entirely.

There's an entire world of these grinders online, which coupled with their consistent output and adjustable grind settings would be an amazing upgrade from the glorified spice grinder I've been using for a decade with my AeroPress.

But as I said at the start, I'm also just happy that such devices now even exist in the first place. If you quickly go through the beans in a large "bean to cup" machine, it's probably not an issue. But I won't be getting one again when better solutions (for me!) like this exist.
