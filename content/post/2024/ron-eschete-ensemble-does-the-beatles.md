---
title: "Ron Escheté Ensemble does The Beatles"
date: "2024-03-04T20:30:52+11:00"
thumb: "https://rubenerd.com/files/2024/yt-UmvBzRV7fL0@1x.jpg"
year: "2024"
category: Media
tag:
- jazz
- music
- music-monday
- the-beatles
location: Sydney
---
This *[Music Monday](https://rubenerd.com/tag/music-monday/ "View all posts in Music Monday")* is so smooth, I risked melting out of my chair.

<figure><p><a target="_BLANK" href="https://www.youtube.com/watch?v=UmvBzRV7fL0" title="Play Can't Buy Me Love"><img src="https://rubenerd.com/files/2024/yt-UmvBzRV7fL0@1x.jpg" srcset="https://rubenerd.com/files/2024/yt-UmvBzRV7fL0@1x.jpg 1x, https://rubenerd.com/files/2024/yt-UmvBzRV7fL0@2x.jpg 2x" alt="Play Can't Buy Me Love" style="width:500px;height:281px;" /></a></p></figure>

I submit this as further evidence for the *Rubenerd Music Law*, namely that everything sounds better with brass.
