---
title: "Sunaookami Shiroko by Good Smile Company"
date: "2024-06-27T08:35:45+10:00"
thumb: "https://rubenerd.com/files/2024/shiroko-box@1x.jpg"
year: "2024"
category: Anime
tag:
- anime-figs
- blue-archive
location: Sydney
---
Clara and I put in the preorder for this *Blue Archive* character a long time ago, and she just arrived from AmiAmi! She's in a bathing suit, so I'm putting behind a [read more](https://rubenerd.com/our-shiroko-blue-archive-fig-arrived/) link for those on the site.

<figure><p><img src="https://rubenerd.com/files/2024/shiroko-box@1x.jpg" alt="Photo showing the AmiAmi box with Shiroko, and the art that AmiAmi includes of their mascot." srcset="https://rubenerd.com/files/2024/shiroko-box@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

<!--more-->


### Her character, and Blue Archive

Elizabeth Rose Bloodflame mentioned [in her debut stream](https://rubenerd.com/erbloodflame-hololive-debut/ "Elizabeth Rose Bloodflame’s Hololive debut!") that while she's not a fan of tea ("soz, innit?"), she loves the aesthetic of it. That's been my impression of *Blue Archive*. I played the mobile game briefly, but while it wasn't exactly what I was after, I absolutely *adored* the world building, character designs, and visuals. It gives me *Railgun* vibes, but more blue. And presumably, with more archives.

Shiroko appears to be the prototypical quiet blue/grey haired character from the franchise, though she's as much an athlete as a strategist. Most aloof, reserved characters in anime and games are cast as bookworms, so her divergent interests and personality traits are an interesting subversion of the trope, or *git* as we'd use now. Thank you.

<figure><p><img src="https://rubenerd.com/files/2024/blue-archive-official-artworks-vol-1@1x.jpg" alt="Cover of the Blue Archive Official Artworks" srcset="https://rubenerd.com/files/2024/blue-archive-official-artworks-vol-1@2x.jpg 2x" style="width:500px; height:355px;" /></p></figure>

Interestingly, while confirming the spelling of her name, I noticed the [Indie Wiki Buddy](https://rubenerd.com/the-indie-wiki-buddy-browser-extension/) extension helped me out by directing me to the independent [Blue Archive wiki](https://bluearchive.wiki/wiki/Shiroko). Thanks again!



### Opening and unpacking

This is a 1/7 scale figure of Shiroko, released by the evil geniuses at Good Smile Company with whom I've surprsingly had little experience. Her sculpting is credited to [Masaaki Oka](https://myfigurecollection.net/entry/155009), with paintwork by [Pan Daisuki Shufu](https://myfigurecollection.net/entry/295920), or the "Bread-Loving Housewife" which is an incredible name. Masaaki Oka isn't to be confused with the [Japanese Supreme Court](https://www.courts.go.jp/english/about/justice/OKA/index.html) justice, unless he has a side gig designing anime figures based on mobile games. Please don't sue me.

The box is fairly non-descript. There's no perspex cutout for viewing the fig on a shelf while she's still in the box, but Clara and I don't do that. There isn't much going on inside either, save for a brief instruction sheet for attaching her divine *Halo*. Some figs are so complicated they require multiple trays of parts, but I suppose this is a pretty basic outfit.

<figure><p><img src="https://rubenerd.com/files/2024/shiroko-out-of-box@1x.jpg" alt="Shiroko in her protective plastic shell, with the box to the side." srcset="https://rubenerd.com/files/2024/shiroko-out-of-box@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>


### Looking at the fig

As you can see from the box, Shiroko is depicted in her <a href="https://bluearchive.wiki/wiki/Shiroko_(Swimsuit)">swimsuit variant</a> from the game. The practical nature of her summer costume compared to her peers reinforces her no-nonsense attitude. This is another *exceedingly* common anime and anime-adjacent trope: bikinis and Speedos are for the bubbly, outgoing characters, while one-pieces and [tight *Free* shorts](https://myfigurecollection.net/item/166705 "Free! - Nanase Haruka - ALTAiR - 1/8") mean serious business!

I knew she was a 1/7 scale, but she was still far taller than I expected. Here she is with another famous blue/purple/grey hair character from the mid-2000s:

<figure><p><img src="https://rubenerd.com/files/2024/shiroko-yuki@1x.jpg" alt="Shiroko posing with Yuki from the Haruhi franchise, and a little stand of Rubi." srcset="https://rubenerd.com/files/2024/shiroko-yuki@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

Based on the press photos, I wasn't sure if her bag and draped jumper would be detachable or not, but they're affixed to the fig. I'm actually relieved; based on how thin the bag strap is, and my penchant for walking into shelves when I have a migraine, accessories like this are falling off figs all the time. *But I digress!* Ow.

I know shouldn't be shocked at the level of detail and the quality of the paintwork on figs like this; Japanese manufacturers don't cut corners the way Western action figure producers do! But it still blows my mind how faithfully her artwork was reproduced, and the level of detail in everything from the zip on the bag, to the folds in the fabric of her costume. I thought the shading in her eyes, *especially* in her contrasting irises (irii?) was especially well done:

<figure><p><img src="https://rubenerd.com/files/2024/shiroko-face@1x.jpg" alt="Close-up of Shiroko's eyes." srcset="https://rubenerd.com/files/2024/shiroko-face@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

Her pose looks natural, the tone of her skin and hair all look great, and the tiny detail in her earrings, hair clip, and sandals are nice touches that other fig makers don't always get right. Her bathing suit also has depth in both detail and colour that make it look a little more as though it's clothing being *worn*, not just glorified spray paint (certain *Evangelion* figs are notorious for not getting this balance right). 

<figure><p><img src="https://rubenerd.com/files/2024/shiroko-side@1x.jpg" alt="Side view showing the folds in her suit, and on the bag" srcset="https://rubenerd.com/files/2024/shiroko-side@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

If I had but one criticism, it'd be the sculpting behind her jaw which looks a bit strange. Some fig manufacturers do a better job of covering seams than others, but poor Shiroko here has been saddled with an awkward line that runs from her neck up to behind her ear. Fortunately she'll be standing on a shelf facing us, so this will be pointing away.

<figure><p><img src="https://rubenerd.com/files/2024/shiroko-back@1x.jpg" alt="The back of the fig, showing the noticible seam between her neck and the back of her ear. Sailor Mercury is standing in the background." srcset="https://rubenerd.com/files/2024/shiroko-back@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>


### Conclusion

It's been a long time since I've bought a fig, because tiny apartment and saving money, but we think she's adorable! Eventually Clara and I will have a dedicated shelf where we can have these, maybe with some artbooks and other related stuff from our favourite franchises.

I also realise I really miss having a proper camera and a macro lens for small things like this. Also some studio lighting, so I'm not at the mercy of overcast days. One can dream.

<figure><p><img src="https://rubenerd.com/files/2024/shiroko-final@1x.jpg" alt="Parting photo of Shiroko looking off to the side, with Rubi in the background." srcset="https://rubenerd.com/files/2024/shiroko-final@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>
