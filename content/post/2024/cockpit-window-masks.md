---
title: "Cockpit window masks, and anti-dazzle paint"
date: "2024-03-09T10:26:35+11:00"
year: "2024"
category: Travel
tag:
- aviation
- colour
- design
location: Sydney
---
Speaking of [planes](https://rubenerd.com/why-the-757-was-retired/ "Ideas on why the 757 was retired"), Simple Flying posted an article about the trend of [cockpit masks](https://simpleflying.com/which-airlines-have-mask-style-cockpit-windows/). This seems to be the default for modern Airbus aircraft, but they also cite airlines like Air Canada and Northern Pacific that have retroactively added the schemes to their fleets.

<figure><p><img src="https://rubenerd.com/files/2024/air-canada-nose@1x.jpg" alt="Photo by Mackenzie Cole of an Air Canada A220." srcset="https://rubenerd.com/files/2024/air-canada-nose@2x.jpg 2x" style="width:500px; height:333px;" /></p></figure>

*Photo of an Air Canada A220 by [Mackenzie Cole](https://www.flickr.com/photos/193032273@N08/51171146574/).*

They don't mention in their article, but black paint around cockpit windows isn't new. Before the advent of white anti-dazzle paint, aircraft routinely had black areas painted below the cockpit to [reduce glare](https://aviation.stackexchange.com/questions/35058/what-is-the-purpose-of-the-black-paint-below-the-cockpit-windows "Aviation StackExchange question about black paint below cockpit windows") and improve visibility for the pilots.

Some liveries included a pronounced black *V* shape under the windows which ended at the nose cone. Others incorporated them into their cheatlines, like Delta did with their DC-8 fleet. Cheatlines need to come back.

<figure><p><img src="https://rubenerd.com/files/2024/dc8-delta@1x.jpg" alt="Delta DC-8 on approach by RuthAS" srcset="https://rubenerd.com/files/2024/dc8-delta@2x.jpg 2x" style="width:500px; height:334px;" /></p></figure>

*Photo of a Delta DC-8 by [RuthAS](https://commons.wikimedia.org/wiki/File:Douglas_DC-8-51_N821E_DL_MIA_07.02.71_edited-5.jpg).*

As my late granddad was fond of saying, *now you can add this to your vast store of useless information*.


