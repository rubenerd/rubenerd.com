---
title: "Melbourne now getting an airport train… maybe?"
date: "2024-07-08T08:06:00+10:00"
year: "2024"
category: Travel
tag:
- australia
- melbourne
- public-transport
- trains
location: Sydney
---
Melbourne is a wonderful city, but the first impressions travellers get leave a lot to be desired. While many capitals have a train service, including smaller ones, one of Australia's largest cities has the dreaded, much-maligned *SkyBus*. They suck, for a few reasons:

* They're expensive. Sydney's airport station tariff is too, but you're getting a *train*.

* They're always deliberately filled to seating capacity, which makes absolutely no sense given people also have, you know, *bags!* You're crammed in tighter than a Ryanair economy seat, because you're also sharing your seat with the massive backpack of the person next to you. It's clear the *SkyBus* operators have never travelled before, or get cabs.

* They depart and arrive from the dingy basement of Southern Cross Station, which makes New York City's Port Authority Bus Terminal feel light and full of fresh air.

Melbournites have been asking for an airport train for decades at this point. The latest delay has come down to a dispute between the private (of course) airport operators and the Victorian state government. The airport wants an underground station, but the government doesn't want to fund the additional cost.

But [according to Rachel Eddie and Kieran Rooney](https://www.theage.com.au/politics/victoria/melbourne-airport-agrees-to-have-overground-train-station-clearing-path-for-rail-link-20240707-p5jrno.html) in The Age, that might be changing?

> Melbourne Airport has backed down from its demand for an underground station for the airport rail link, ending a stalemate with the Victorian government and clearing the path for the overdue project to proceed.
> 
> The state government has insisted building a station above ground in Tullamarine would be cheaper and faster, and accused the airport of deliberately trying to sink the project, while the airport had argued that an underground station would future-proof the connection.

[@hamhammer27 on Twitter](https://x.com/hamhammer27/status/1810069111365677329) says the operator realised they overplayed their hand, which sounds likely:

> I can’t help but think Melbourne Airport panicked because they've pissed off the federal government over this… who are also sitting on the final approval of the 3rd runway which the airport desperately wants to get up.

I think I speak for all Melbournites and regular travellers: get it built!
