---
title: "“The road less travelled”"
date: "2024-08-28T08:35:50+10:00"
year: "2024"
category: Thoughts
tag:
- philosophy
location: Sydney
---
Maybe there was a reason? Sometimes?

I seemed to instinctively choose the road less travelled when I was in my twenties. It lead to amazing results: I wouldn't have moved to where I am now, met the person I'm with, got the job at the startup instead of the big tech company, or even stuck with FreeBSD for as long as I have. But I also see a lot of missed opportunities, and potential avenues for personal growth.

There's value in not following the crowd on everything. But sometimes, the crowd is going that way because it's the better option. I suppose wisdom comes from deciding *when* that is.
