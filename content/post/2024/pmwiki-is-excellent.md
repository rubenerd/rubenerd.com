---
title: "PmWiki is excellent"
date: "2024-04-22T08:41:49+10:00"
year: "2024"
category: Internet
tag:
- pmwiki
- wikis
location: Sydney
---
This is just a short post to remind everyone that [PmWiki is excellent](https://www.pmwiki.org/), and you should consider it. I've been running a local install of it for my own note-taking uses for two decades, and I recently flipped another public MediaWiki install to it... <a href="https://sasara.moe/">ours</a>!

I maintain, and have maintained, a bunch of CMSs and wiki systems for various people, including DokuWiki, Moin, and TWiki, along with MediaWiki and its various offshoots. PmWiki is by far the easiest, both to use and operate. It's tiny, it's written in bog standard PHP you can deliver with php-fpm, and its markup is easy to grok.

I'm at the point now where I default to PmWiki unless someone has a specific feature they need from something heavier. This rarely happens in practice.

Clara and I [wrote a stylesheet](https://codeberg.org/rubenerd/lunchbox/src/branch/trunk/kiriben-pmwiki.css "Our CSS on Codeberg") to make it look a bit more like MediaWiki, but otherwise we run it stock. If you do decide to give it a try, [don't forget to donate](https://www.pmwiki.org/wiki/PmWiki/PayForPmWiki).

Huge thanks to Patrick R. Michaud and Petko Yotov :). Your software is one of the regular highlights of my day.
