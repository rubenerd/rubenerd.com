---
title: "A National RC-6060 clock radio from 1985"
date: "2024-04-09T12:13:52+11:00"
year: "2024"
category: Hardware
tag:
- 1980s
- anxiety
- clocks
- nostalgia
location: Sydney
---
The subject of smartphone <a href="https://rubenerd.com/an-unintentional-led-night-light/" title="An unintentional LED night light">frustration</a> and <a href="https://rubenerd.com/three-months-into-my-smartphone-awareness-exercise/" title="Six months into my smartphone awareness exercise">dependence</a> has become an unintentional theme here of late. Whether my work and personal phones are the root cause, or <a href="https://rubenerd.com/smartphones-and-teenage-mental-illness/" title="The science (or not) behind smartphones and anxiety">exacerbating existing anxiety</a>, I've decided I'd rather not have them in the bedroom before I sleep. I now reach for a book instead, which has been wonderful!

There was just one issue with that approach. Like I imagine many (most?) of you do as well, my primary phone is also my alarm clock. *Whoops*. I was lucky that I woke up naturally again yesterday, otherwise I likely wouldn't have heard it beeping from the other room.

This presented a bit of a quandary. How did we used to wake up before the advent of smartphones? Or feature phones? Or PDAs? I thought back to roosters, but our current landlord probably wouldn't appreciate him scratching up the walls. We have our house deposit together, but even then our future neighbours likely wouldn't be waking up at the same time, and wouldn't appreciate the audible gesture.

Then I remembered these things called *alarm clocks*. These dedicated devices were designed to sit on our bedside tables, and alert us to wake up at a preprogrammed time. I had vague memories of using them as a little kid without a phone in the *Before Times*, so I thought it made sense to find one to use again.

I could have been **rational** and bought something compact, affordable, and functional. Instead, I went **National.** *AAAAAAAAAAAAAAH!* I present to you this stunning piece of 1980s engineering, the National RC-6060:

<figure><p><img src="https://rubenerd.com/files/2024/national-clock-radio@1x.jpg" alt="" srcset="https://rubenerd.com/files/2024/national-clock-radio@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

No, your eyes do not deceive you, that's fake wood grain. The digital clock belies its true vintage, but you'd be forgiven for thinking it's much older. This would have looked dated in the 1980s, let alone now.

The unit comes with an alarm beeper, an AM/FM radio tuner with shockingly clear reception despite the lack of an external antenna cable, a mono speaker, and a power cable. It also features that aforementioned 1980s display which is dim by daylight, but just the right brightness at night to be read without blanketing the room in a sickly glow. Modern LED manufactures, [take note of this](https://rubenerd.com/an-unintentional-led-night-light/ "An unintentional LED night light").

It now takes pride of place on my bedside table, alongside the stack of books I'm working through. <span lang="jp" class="jp">ども ありがと</span>, 1980s Matsushita :).
