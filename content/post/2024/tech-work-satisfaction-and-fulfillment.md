---
title: "Tech work satisfaction and fulfillment"
date: "2024-10-29T08:29:28+10:00"
year: "2024"
category: Thoughts
tag:
- psychology
- work
location: Sydney
---
This is not a autobiographical post... or is it?! What this post *is* supposed to be is addressing the issue of work satisfaction more broadly; or at least, what I've been reading about it.

I keep seeing articles in newspapers and journals that Zoomers and Millennials are wanting more from their employment than previous generations. I'm not sure that's broadly true; nobody wants to get nothing from what they spend a significant amount of their lives on. But these generations do seem more vocal about it, at least based on the recruiters and managers I know in Singapore and Australia.

It's manifesting as "The Great Resignation" across the world, at least based on how reporters are discussing it. People are realising there are opportunities elsewhere, so why put in the proverbial 110% into somewhere inflexible, or for a hapless manager that makes your work miserable? The great lie that large swaths of humanity need to be permanently based in open-plan offices with long commutes and helicopter managers have been exposed with Covid work-from-home arrangements.

This is a key piece of advice I see: follow your passion, and don't stick around with a job that doesn't respect you. I can see something to that.

Here comes the proverbial posterior prognostication: *but...* I've been seeing the angle that fulfillment at work isn't the issue, its wanting it in the first place. The argument goes that people tie too much of their self worth and energy into their jobs, which results in disappointment and stress when you internalise and onboard too much of it. Advocates of this view will use terms like "work/life balance" and "healthy separation" to encourage workers to distance the two. Professional detachment is a skill, but it extends far beyond how you talk with clients.

Even in my early twenties when I worked at a job I didn't especially enjoy, I did derive satisfaction and security from knowing I was pulling in money I could save and spend, or to take the family out to dinner, etc. There's something alluring about reframing work in that context again, rather than introducing myself as "someone who works at X" before saying who I am.

It reminds of that US-centric advice of the "side hustle", which someone I read recently responded with "mine is spending time with family". Maybe that's the key: what is it in life you want to prioritise? In the words of Scotty from *Star Trek*, if something is important, you make the time.

