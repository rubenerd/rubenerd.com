---
title: "Another of those random question lists"
date: "2024-11-07T18:32:00+11:00"
year: "2024"
category: Thoughts
tag:
- lists
- personal
- pointless
location: Sydney
---
I saw this one [float by on Mastodon](https://masto.es/@bolverkr/113389665312076151), and thought it'd be a fun distraction.

As a PSA, if you're not like me and use keysmashes stored in a password manager for "secret questions", you probably shouldn't answer things like this.

**What's your nickname?**
: Based on the email I receive when this blog is mentioned on Hacker News, it seems to be "idiot". They never spell it right on coffee cups.<p></p>

**Age?**
: I do, yes. It's preferable to the alternative.<p></p>

**Height?**
: At least 0.0018 km, depending on posture.<p></p>

**Sexuality?**
: 🩷💜💙<p></p>

**Favourite sound?**
: Crunchy autumn leaves underfoot, followed by the vibraphone.<p></p>

**Favourite smell?**
: Freshly-ground coffee. No contest.<p></p>

**Any siblings?**
: Yes, an incredible sister who got all the genes for talent, charisma, and looks. That said, I could write way more YAML than her. Is that a good thing?<p></p>

**Do you drink?**
: Only a bit. I hate feeling drunk, and hangovers remind me of migraines. Whisky, sake, and pilsner are lovely.<p></p>

**Crush?**
: When I was a kid, <a title="Ami's article on Wikipedia" href="https://en.wikipedia.org/wiki/Sailor_Mercury">Ami</a> from the *Sailor Moon* franchise, and <a title="Kes's article on Wikipedia" href="https://en.wikipedia.org/wiki/Kes_(Star_Trek)">Kes</a> from *Star Trek: Voyager.* Right now, [James Hoffman](https://www.jameshoffmann.co.uk/ "James Hoffman's coffee website").<p></p>

**Any phobias?**
: Being taken too seriously. Also large social gatherings, though I'm working on it.<p></p>

**Cats or dogs?**
: I was the only one in the family who loved cats. But I love dogs more. We don't deserve dogs.<p></p>

**Any scars?**
: One on my right hand where I gashed myself opening a Sony Vaio PCG-C1VM laptop as a teenager. I love retrocomputers from my childhood, but I ewasted that worthless pile of scrap!<p></p>

**Books you would recommend?**
: Right now, any of the fine fiction and non-fiction by [Michael W Lucas](https://mwl.io/). I've still barely made a dent, but wow. I'm also a huge fan of the <a href="https://en.wikipedia.org/wiki/Monogatari_(series)">Monogatari</a> light novels, which I'm still going through. I've also read a lot of John Grisham.<p></p>

**TV shows you would recommend?**
: [Barakamon](https://myanimelist.net/anime/22789/Barakamon), in a heartbeat. I don't watch much TV anymore, it's all indie YouTubers and the occasional anime.<p></p>

**Piercings/tattoos?**
: I don't judge, but they're not for me.<p></p>

**Which concerts have you been to?**
: Most recently was [Paul McCartney last year](https://rubenerd.com/seeing-paul-mccartney-live-in-sydney/). To see a living Beatle was one thing, but Clara and I are *massive* fans of his solo work and Wings.<p></p>

**What would you name your kids?**
: We'd love to foster one day. We'd name them what they're called.<p></p>

**If you could instantly learn any language, which one would it be?**
: Assembler, for every processor architecture. If that doesn't count, Kansai-ben. まいど 👋<p></p>

**Moon or stars?**
: We need both, but Van Gough was onto something.<p></p>

**Favourite song right now?**
: It'll always be *[Tiger in the Rain](https://www.youtube.com/watch?v=4a9WwntkrWU)* by Michael Franks, followed closely by *[Wave](https://www.youtube.com/watch?v=a6KDpB6skA4)* by Antônio Carlos Jobim.<p></p>

**Best friends?**
: I'm lucky to have several, but Clara is number one.<p></p>

**Favourite sport?**
: To play, badminton. To watch, badminton. I used to watch F1, but lost interest. Gymnastics and table tennis are amazing to watch. I like swimming, even if I'm no good at it.<p></p>

**What do you like about yourself?**
: I like to think I give a shit. My mentour Jim Kloss proved you could be earnest in IT with kindness and a conscience, and I try to live that too.<p></p>

**What do you dislike about yourself?**
: Anxiety.<p></p>

**Someone you miss?**
: My mum. Every day. It guts me she never got to meet Clara.<p></p>

**Places you'd like to visit.**
: Too many! Right now I want to take Clara to Europe. I've never been to Poland or the Baltic states, and would love to visit Ukraine and Armenia. We've only stayed in 4 Japanese prefectures, and have yet to visit Taiwan or Korea.<p></p>

**What was one of the best parties you've been to?**
: The ones with a few close friends or family. Big parties aren't for me.<p></p>

**Do you want kids?**
: Per above.<p></p>

**Hobbies?**
: I submit this blog as a record of these!<p></p>

**Favourite subject?**
: Computer history, especially around the 1980s and 90s. So much stuff hadn't been figured out yet, and there was so much diversity in platforms, architectures, and software.<p></p>

**Least favourite subject?**
: In the words of a RØDE mic, I'm not *Shure*. Thank you.<p></p>

**Favourite flower?**
: Cherry blossoms, for the most cliché of reasons. I also love tulips and jacaranda, even if the latter pose a slipping hazard in autumn.<p></p>

**What are you currently looking forward to?**
: Moving into our new home. Also one day meeting more of my friends and readers around the world.<p></p>

**Dream vacation?**
: One day I want to walk the Nakasendō Trail from Kyōto to Tōkyō, and the Camino de Santiago that ends in Galicia. Or just drop us off at a train station somewhere, and let us explore.<p></p>

**Where do you see yourself 5 years from now?**
: Older, and hopefully a bit wiser. But not too much more.<p></p>

**Why did you join Mastodon?**
: Curiosity, and to [meet other BSD people](https://bsd.network/@Rubenerd). Now with the collapse of the only social network I ever grokked, it's my new home.<p></p>
