---
title: "Can’t use /home on macOS"
date: "2024-08-01T09:02:24+10:00"
year: "2024"
category: Software
tag:
- apple
- bsd
- macos
- unix
location: Sydney
---
macOS is a [certified UNIX](https://www.opengroup.org/openbrand/register/) platform, and at times it has enough in common with other Unix-like OS to be useful. Every time I use a Windows machine, I get frustrated that I don't have the same shell and utilities that macOS supports natively (and no, Cygwin and WSL don't count).

<p><img src="https://rubenerd.com/files/2024/tango-home.svg" alt="Home icon from the Tango Desktop Project" style="width:128px; height:128px; float:right; margin: 0 0 1em 2em;"></p>

*But...* occasionally you're reminded things are a bit different. The location of `/home` is the one that has always frustrated me, because it breaks certain config files, nor can I type them quickly.

Here's my home directory on FreeBSD, which conveniently is even its own ZFS dataset by default:

    /home/ruben (previously aliased to /usr/home/ruben)

And on NetBSD, and Linux:

    /home/ruben

And macOS:

    /Users/ruben

I suspect Apple/NeXT wanted the directory to be Capitalised, which would have been fine on a case-insensitive file system. But early versions of macOS supported being installed on alternative file systems, which could present issues referencing distinct `/home` and `/Home` folders. Using a different name negated this issue.

The thing is, a stub `/home` folder does exist on macOS, albeit as a symbolic link:

    # ls / *home*
    ==> home@ -> /System/Volumes/Data/home

In the early days of Mac OS X, I'd do this on new machines:

    # ln -s /Users/ruben /home/ruben

Alas, this no longer works, perhaps owing to it being a link to an APFS volume with additional restrictions. 

    # ln -s /Users/ruben /home/ruben
    ==> ln: /home/ruben: Operation not supported

Oh well!

Now I know what you're all thinking; invoke `$HOME` and be done with it. That's the Technically Correct Answer&trade; to this issue, though that doesn't always work for the two reasons:

* Certain (badly-written!) config files require hardcoded paths, so I have to maintain two copies where one should suffice.

* Muscle memory, alas, doesn't respect environment variables either.

Maybe the solution is to create a `/Users` directory on my other Unix-like machines, and symlink stuff there!

    \^__^\ /^__^/ \^__^/ ... #shudders

Apple, give us a link to our `/Users` directory from `/home` please! You're scraping my pages to feed your AI without my attribution, consent, or compensation, so I know you're reading this.
