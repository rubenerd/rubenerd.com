---
title: "Cataloguing some Melbourne trams"
date: "2024-04-02T09:24:45+11:00"
thumb: "https://rubenerd.com/files/2024/melbourne-tram-w@1x.jpg"
year: "2024"
category: Travel
tag:
- australia
- melbourne
- public-transport
- trams
location: Sydney
---
Intercity travellers to Melbourne might talk about the coffee and views, but the real reason transport nerds like me go back there is to explore the world's largest tram system, known in other parts of the world as light rail, trolleys, street cars, anything but *bus*. Because buses suck.

I tasked myself with getting photos of every generation of tram I could while we wandered around, because again, I'm a massive nerd. None of them were especially good, but it was still fun.


### W-class

These trams were built from 1923–56, and serve as a bit of heritage novelty today. A unit has also been exported to San Francisco, which Clara and I rode back in 2018. The remaining units in operation were partially rebuilt to improve safety, but retain the original aesthetic.

<figure><p><img src="https://rubenerd.com/files/2024/melbourne-tram-w@1x.jpg" alt="A heritage W-class tram picking up passengers." srcset="https://rubenerd.com/files/2024/melbourne-tram-w@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>


### Z3-class

Pronounced the *Zed* class because we're civilised down here, these were commissioned from 1979–83. They were a refinement of the original Z-class from 1975, and were refurbished in 2013. They're about the length of a small bus, and I think also have a certain retro charm to them.

<figure><p><img src="https://rubenerd.com/files/2024/melbourne-tram-z3@1x.jpg" alt="Photo of a Z3-class tram at the end of a line near Flinders Street Station." srcset="https://rubenerd.com/files/2024/melbourne-tram-z3@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>


### A-class and B-class

These trams were introduced from 1983–94, and look the part with their angled lines and flat sides. The A-class consists of a single unit, while the B-class were longer and introduced articulation, interestingly with flat panels in lieu of concertina-style curtains.

<figure><p><img src="https://rubenerd.com/files/2024/melbourne-tram-b@1x.jpg" alt="Photo of a B-class tram in the distance behind some trees." srcset="https://rubenerd.com/files/2024/melbourne-tram-b@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>


### C-class

These trams were built by Alstom, and introduced from 2001–02. They're the first I'd describe as modern looking, though it's wild to think they're more than two decades old now. They're longer than the B-class, and articulated in two sections.

Personally, they also hold the dubious honour of being the only trams I've ever travelled on (in Sydney, Melbourne, Adelaide, Plzeň, and San Francisco) that made me motion sick. This might explain why I forgot to take a picture after the fact. Check out the [Wikipedia article](https://en.wikipedia.org/wiki/C-class_Melbourne_tram "C-class Melbourne tram") for a photo.


### D-class

The D-class were built by Siemens, and introduced from 2002–04. The D1 is articulated in two sections, and D2 in four. They remind me a bit in design and layout of the Adelaide Metro Flexity trams, which were built by Bombardier shortly after.

<figure><p><img src="https://rubenerd.com/files/2024/melbourne-tram-c@1x.jpg" alt="Photo of a D-Class train with horrible backlighting" srcset="https://rubenerd.com/files/2024/melbourne-tram-c@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>


### C2-class

The five C2s in operation were originally built for the Mulhouse tram system in France, but were bought by Yarra Trams in 2008. They're nicknamed *bumble bees* for their weird cab design, but I think their five articulated sections [look more like a centipede](https://en.wikipedia.org/wiki/C2-class_Melbourne_tram "C2-class Melbourne tram").

They only operate on Route 96, which we didn't end up taking.


### E-class

The most recent introduction to the fleet, built in Melbourne by Bombardier from 2013–21. They're the longest trams in the system, and difficult to take a picture of in a single shot without an extreme angle or with compression!

This was my attempt one late morning outside Parliament House:

<figure><p><img src="https://rubenerd.com/files/2024/melbourne-tram-e@1x.jpg" alt="A wide view of an E-class tram navigating a corner." srcset="https://rubenerd.com/files/2024/melbourne-tram-e@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>
