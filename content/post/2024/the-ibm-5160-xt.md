---
title: "The IBM 5160 XT"
date: "2024-06-06T08:30:45+10:00"
year: "2024"
category: Hardware
tag:
- commodore-pc
- ibm
- ibm-xt
- retrocomputing
location: Sydney
---
*This post is dedicated to Daniel O'Connell, who's words of encouragement over the last few months have helped me a lot. Thank you!*

My decades-long interest in retrocomputers has lead me down so many rabbit holes, including 8-bit Commodore and Apple machines. But I never spent much time thinking about the original IBM PC and its offshoots. I long figured the PC architectures represented by my [Pentium 1](http://retro.rubenerd.com/x86.htm#childhood-mmx) and [Am386 SX](http://retro.rubenerd.com/x86.htm#dfi-am386) were sufficiently old to experience that era of computing. It's DOS, CP/M, and OS/2 all the way down, right?

But surprising nobody, recently I've been diving into the world of the original IBM PCs, and specifically the 8-bit 5150 PC, and 5160 XT. They set so many of the standards that almost all of us use in our machine today, right down to the card slots of the latter.

I mean, just *look* at [this photo by Rama & Musée Bolo](https://commons.wikimedia.org/wiki/File:IBM_PC-IMG_7271_(transparent).png)\! What's not to love about this industrial design? The two-tone beige and grey, the contrast of the monitor and full-height disk drives, the chonky power switch!

<figure><p><img src="https://rubenerd.com/files/2024/ibm5160@1x.jpg" alt="Photo of an IBM 5150" srcset="https://rubenerd.com/files/2024/ibm5160@2x.jpg 2x" style="width:500px; height:333px;" /></p></figure>

This is the exact setup my dad had at his job in the 1980s, and I was since told he even had the wood-veneer table and avaiator glasses to match. It's a shame it was owned by The Company, and so it was probably binned when it was deemed surplus to requirements. Years later, and the Texas Instruments, Toshiba, and ThinkPad laptops he got with work met a similar fate, despite his insistence it'd be a waste not to let his son tinker with them.

But I digress. IBM's architecture and design are a large part of the reason why I'm able to type this blog post out, despite nothing in this homebrew FreeBSD tower having any discrete IBM components.

While IBM originally treated clones of their PCs the way Apple did, their begrudging acceptance of clean-room, reverse-engineered designs lead to a proliferation of manufacturers and OEMs selling everything from mirror copies, to more advanced versions with better silicon and more features. Think NEC V20 CPUs, turbo modes, additional card slots, more memory, exotic drive controllers, consolidated boards, and so on. When IBM attempted to assert control of the standard they created with the MCA slot, the industry ignored them, and IBM eventually found themselves having to comply with connectors like PCI (did IBM ever ship a VLB machine?).

<figure><p><img src="https://rubenerd.com/files/2024/ibm-diagnostic@1x.png" alt="IBM Diagnostic screen on an emulated 5060" srcset="https://rubenerd.com/files/2024/ibm-diagnostic@2x.png 2x" style="width:500px; height:300px;" /></p></figure>

I find these clones just as interesting as the original IBM. The fact [Commodore were making PCs](https://rubenerd.com/researching-if-commodore-pcs-was-profitable/) especially tickles me, and makes me want to build a small-form factor PC out of [this scrap of Commodore PC history](https://rubenerd.com/a-literal-scrap-of-commodore-pc-history/ "Buying a literal scrap of Commodore PC history") somehow. Maybe I could build a little single-board computer into it running [86box](https://86box.net/) or [MartyPC](https://github.com/dbalsom/martypc).

I wasn't around for most of the 1980s, and when I was, I wasn't old enough to read, let alone comprehend the home computer scene! Part of me wishes I'd been born a decade earlier to have grown up when all these machines were being established. The 1990s and onwards simply weren't as interesting in the same way. I guess at least we have some competition in the form of ARM and RISC-V.
