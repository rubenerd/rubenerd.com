---
title: "Germany’s sovereign tech fund invests in FreeBSD"
date: "2024-09-05T09:21:26+10:00"
year: "2024"
category: Software
tag:
- bsd
- freebsd
- news
location: Sydney
---
I read this last week, but it's awesome news! [The FreeBSD Foundation](https://freebsdfoundation.org/blog/sovereign-tech-fund-to-invest-e686400-in-freebsd-infrastructure-modernization/)\:

> Boulder, CO – August 26, 2024—The FreeBSD Foundation, dedicated to advancing the open source FreeBSD operating system and supporting the community, announced that Germany’s Sovereign Tech Fund (STF) has agreed to invest €686,400 in the FreeBSD project to drive improvements in infrastructure, security, regulatory compliance, and developer experience.
