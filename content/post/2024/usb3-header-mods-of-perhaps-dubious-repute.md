---
title: "USB 3.0 header mods of perhaps dubious repute"
date: "2024-12-01T14:58:09+11:00"
abstract: "Using and abusing internal headers for an internal optical drive."
year: "2024"
category: Hardware
tag:
- homelab
- storage
location: Sydney
---
My older FreeBSD homelab server runs on a [Supermicro X11SAE-M](https://www.supermicro.com/en/products/motherboard/X11SAE-M) workstation board with a Xeon E3-1275 v6. This is a photo of the motherboard:

<figure><p><img src="https://rubenerd.com/files/2024/x11sae-m@1x.jpg" alt="Photo of the aforementioned motherboard." srcset="https://rubenerd.com/files/2024/x11sae-m@2x.jpg 2x" style="width:300px; height:338px;" /></p></figure>

This board has two USB 3.0 headers, seen in the photo as small cyan rectangles at the bottom. These headers are generally used to attach to the USB connectors on the front of a case or chassis. They're a massive upgrade from the pins used in earlier versions of USB, the layout of which was never formally standardised. Ask me how I found out the hard way!

This machine is mostly used for bulk hard drive storage, though two of the eight SATA ports are used for slimline optical drives. While this does come in useful for ripping media and burning the odd M-Disc, it seems like a waste of ports that could otherwise have hard drives or SSDs attached instead.

This lead me to a dangerous thought. Could I offload the optical drives to those internal USB headers? These are the same slimline drives used in external USB enclosures without an additional power source, so theoretically I could route an internal cable from one of these headers to the back of the slimline drive. Then I'd have two more SATA ports for hard drives.

Alas, and perhaps unsurprisingly, it's not that simple. Slimline internal drives use SATA... obviously, because its how they're currently connected! External drives typically have a daughterboard that translates SATA (or IDE, etc) into USB, which then is carried across the external cable. An internally-connected slimline drive would need an active adaptor to convert SATA to USB. This would then need to be connected to the header somehow.

Fortunately, this adaptor is everywhere on the usual sites, and converts the USB 3.0 header to two USB 3.0 ports:

<figure><p><img src="https://rubenerd.com/files/2024/usb-header-ports@1x.jpg" alt="Photo of the aforementioned connector." srcset="https://rubenerd.com/files/2024/usb-header-ports@2x.jpg 2x" style="width:300px; height:264px;" /></p></figure>

To recap, the chain would be:

1. Slimline drive
2. SATA-to-USB adaptor
3. USB cable
4. USB connector
5. USB-to-USB header connector
6. USB header connector

This sounds like a lot of steps, but really it's no different electrically from what those external drives are doing. So why do I still feel weird about contemplating it?

The *professional* answer would be to get a PCIe drive controller to run in JBOD mode. But it still feels like a waste not using those juicy USB 3.0 headers for anything. If I wanted to be really janky, I'd get a pair of USB keys and connect them to the USB 3.0 headers directly, then run a ZFS mirror across them. I'd call the pool `LivingDangerously`.

Have any of you found a novel or alternative use for those headers?
