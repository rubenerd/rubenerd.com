---
title: "Using mkdir -p for multiple subdirectories"
date: "2024-05-07T07:36:15+10:00"
year: "2024"
category: Software
tag:
- bsd
- linux
- things-you-already-know-unless-you-dont
location: Sydney
---
In today's installment of *[things you already know, unless you don't](https://rubenerd.com/tag/things-you-already-know-unless-you-dont/)*, did you know you can use the `-p` option to create multiple levels of subdirectories at once, even if the intermediate directories don't exist?

For example, say you want to make this new tree for a new FreeBSD box:

    # mkdir /usr/local/etc/pkg/repos

If any of those subdirectories don't exist, you'll be told:

    mkdir: /usr/local/etc/pkg/repos: No such file or directory

One way to do this is to make the intermediate folders manually:

    # cd /usr/local
    # mkdir etc etc/pkg etc/pkg/repos

Or easier, use `-p`:

    # mkdir -p /usr/local/etc/pkg/repos
    $ tree
       
    [..]
    .
    └── usr
        └── local
            └── etc
                └── pkg
                    └── repos
    [..]

Donezo!

I've had a bunch of you say these random things are helpful, especially if you're starting out with \*nix. Let me know if there are any other little things you want me to cover.
