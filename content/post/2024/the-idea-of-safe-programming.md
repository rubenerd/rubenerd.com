---
title: "The idea of memory-safe programming in context"
date: "2024-04-20T20:16:33+10:00"
year: "2024"
category: Software
tag:
- security
location: Sydney
---
I've been watching the current debate over memory-safe programming with interest. On the one have you have Rust and Go advocates talking about the need to replace C with its buffer overflows and other security problems, and on the other you have different fingers.

And people say I'm not serious enough here, can you believe it!? Don't answer that.

Bruce Schneier quoted C++ expert Herb Sutter about how [his language can be made safer](https://herbsutter.com/2024/03/11/safety-in-context/). The whole article is an interesting read, even if you're like me and last touched C++ at university. But this meta line was my favourite:

> But if we focus on programming language safety alone, we may find ourselves fighting yesterday’s war and missing larger past and future security dangers that affect software written in any language.

I haven't read anyone claim that languages like Rust are a panacea, especially not Rustacians themselves. Here comes the proverbial posterior prognostication: *but...* I hope this interest in memory-safe languages by governments also come with a mature discussion on wider security as well.

The recent xz adventures prove that the scope of issues we have to worry about is far greater than memory safety, especially when imperfect meatspace is involved. I still feel like my own brain buffer has been dropping thoughts off the stack this week. I'm pretty sure I mixed some metaphors there again. *Rubenerd, over and `<< cout`*.
