---
title: "“Writing Month” so far"
date: "2024-11-26T16:45:28+11:00"
abstract: "15,474 words on my blog this month isn’t a novel, but it’s better than a poke in the eye with a sharp stick."
year: "2024"
category: Media
tag:
- numbers
- pointless
- weblog
location: Sydney
---
[I posted this on Mastodon](https://bsd.network/@rubenerd/113547661030766042), but thought it was fun to share. I'm not doing Writing Month, or the AI-infested original, but according to my blog repo, I've written:
 
* 177,555 words this year
* 15,474 words in November

It ain't no book that's for sure. But better than a poke in the eye with a sharp stick?

I'll publish one of my actual silly books one day. But as David Pflug pointed out in a recent email, my editor sucks. He's where good spell chequers go to dye!
