---
title: "Michael Twitty on history, food, and family"
date: "2024-01-01T23:40:00+11:00"
thumb: "https://rubenerd.com/files/2024/michael-twitty@1x.jpg"
year: "2024"
category: Media
tag:
- food
- politics
- united-states
- video
- youtube
location: Sydney
---
Max Miller of [Tasting History](https://www.tastinghistory.com/) recently did a video with chef, historian, and author Michael Twitty about [making Hoppin’ John 📺](https://www.youtube.com/watch?v=ff_gUzdzED4 "Hoppin' John for New Year's with Michael Twitty"), a New Year food I'd never heard of, having grown up on the other side of the planet.

<figure><p><img src="https://rubenerd.com/files/2024/michael-twitty@1x.jpg" alt="Michael Twitty (left) talking with Max Miller" srcset="https://rubenerd.com/files/2024/michael-twitty@2x.jpg 2x" style="width:500px; height:281px;" /></p></figure>

I expected it to be interesting, but not to end up with a lump in my throat. Here's just a short snippet from the start:

> There was a respite between Christmas Eve and New Year's Day. And so it was an opportunity for the enslaved community to come together and celebrate each other.
>
> The average enslaved person was sold two to three times in their lifetime, which meant there was an inability to connect successfully with your kin. So anytime you have the opportunity to do that, and cement those relationships, that was important.
>
> When you talk about [culinary history] it goes hand in hand with the socio-economic and cultural realities of the United States and beyond. It's inescapable.
> 
> But what I love about this: this food connects us with our ancestors going back millennia. Not just the Antebellum South, the colonials, millennia. Back into ancient Africa.
>
> And it says: we survived.
