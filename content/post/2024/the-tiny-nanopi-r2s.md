---
title: "The tiny NanoPi R2S"
date: "2024-03-10T23:12:25+11:00"
year: "2024"
category: Hardware
tag:
- cute
- sbc
location: Sydney
---
I can't remember where I first read about this, but it looks like a [cool little box](https://www.friendlyelec.com/index.php?route=common/home "Website for Friendly Elec") for a router or VPN endpoint:

> The NanoPi R2S uses Rockchip's quad-core A53 RK3328 SoC with powerful performance. Its default frequency is 1.2GHz.
The NanoPi R2S has 1GB (or optional 2GB) RAM, dual Gbps Ethernet ports. It uses RK805 PMU chip and supports dynamic frequency scaling. It has one USB 2.0 port that can interface with 4G modules, USB HD cameras, USB WiFi modules etc.

Below is their store photo. The Ethernet jacks give you a visual indiciation just how small this box is:

<figure><p><img src="https://rubenerd.com/files/2024/nanopi-r2s@1x.jpg" alt="Images of the motherboard and case for the tiny NanoPi 2RS" srcset="https://rubenerd.com/files/2024/nanopi-r2s@2x.jpg 2x" style="width:350px;" /></p></figure>

It comes with Ubuntu Core, which makes me think it could run FreeBSD and NetBSD too? The machined metal case is pretty swish.
