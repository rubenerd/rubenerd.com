---
title: "Some Melbourne detail at night"
date: "2024-04-04T13:11:44+11:00"
thumb: "https://rubenerd.com/files/2024/melbourne-night-1@1x.jpg"
year: "2024"
category: Media
tag:
- architecture
- austrlaia
- light
- melbourne
location: Sydney
---
This is the penultimate post in my Melbourne 2024 series! Today we have the results of a small trip Clara and I took the day before we flew back to Sydney. I haven't had telephoto capability for a long time, so thought it would be fun to capture some small details on various buildings with interesting lighting. With hindsight I should have noted *what* the buildings were&hellip; *whoops!*

<figure><p><img src="https://rubenerd.com/files/2024/melbourne-night-1@1x.jpg" alt="" srcset="https://rubenerd.com/files/2024/melbourne-night-1@2x.jpg 2x" style="width:500px; height:375px;" /><br /><img src="https://rubenerd.com/files/2024/melbourne-night-4@1x.jpg" alt="" srcset="https://rubenerd.com/files/2024/melbourne-night-4@2x.jpg 2x" style="width:500px; height:375px;" /><br /><img src="https://rubenerd.com/files/2024/melbourne-night-3@1x.jpg" alt="" srcset="https://rubenerd.com/files/2024/melbourne-night-3@2x.jpg 2x" style="width:500px; height:375px;" /><br /><img src="https://rubenerd.com/files/2024/melbourne-night-2@1x.jpg" alt="" srcset="https://rubenerd.com/files/2024/melbourne-night-2@2x.jpg 2x" style="width:500px; height:375px;" /></figure>
