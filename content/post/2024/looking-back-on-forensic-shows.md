---
title: "Looking back on forensic shows"
date: "2024-12-14T10:39:46+11:00"
abstract: "Forensic odontology isn’t, and forensic experts fabricating evidence."
year: "2024"
category: Thoughts
tag:
- science
location: Sydney
---
I grew up watching shows like *CSI*, and true crime like *Forensic Files* and *The New Detectives*. It's interesting now with retrospect seeing what the people on these shows got wrong, or where the science was overstated, or what we've learned since (I also think white collar crime is underrepresented on such programming too, but that's a separate issue).

For example, I had no idea how shaky the entire field of [forensic odontology](https://www.nist.gov/news-events/news/2022/10/forensic-bitemark-analysis-not-supported-sufficient-data-nist-draft-review) was, as NIST published:

> The draft review finds that “forensic bitemark analysis lacks a sufficient scientific foundation because the three key premises of the field are not supported by the data. First, human anterior dental patterns have not been shown to be unique at the individual level. Second, those patterns are not accurately transferred to human skin consistently. Third, it has not been shown that defining characteristics of those patterns can be accurately analyzed to exclude or not exclude individuals as the source of a bitemark.” 

I was also looking up an unrelated person, but stumbled on this article from the AP about an expert these shows regularly featured. *Turns out*, he [fabricated evidence](https://apnews.com/article/henry-lee-fabricated-murder-evidence-ef08de1e15148b3d48129ead10924009) that sent two innocent people to gaol for decades:

> “Other than stating that he performed the test, however, the record contains no evidence that any such test was performed,” the judge wrote. “In fact, as plaintiffs noted, Dr. Lee’s own experts concluded that there is no ‘written documentation or photographic’ evidence that Dr. Lee performed the TMB blood test. And there is evidence in this record that the tests actually conducted did not indicate the presence of blood.”

This is less a mark against the science, and more on its execution. But the two can't be decoupled: as long as humans are the ones performing and presenting these tests, there will be opportunity for oversights or abuse. It's why effective defence representation and third-party verification are so critical.

There was this sense of incredulity and fear in these forensic shows that crimes used to be solved with vague and unscientific procedures, much like a modern surgeon would baulk at what passed as medicine in the past. I feel like now we'll be looking back at some of these early attempts at forensic science in a similar light. In a way that's a good thing: science only gets better as we learn more.

Clara and I picked up the original <a href="https://en.wikipedia.org/wiki/CSI:_Crime_Scene_Investigation_(video_game)">Xbox version of CSI</a> for the princely sum of $1.50 at our local CEX. We might be firing that up in an emulator soon to see how that's aged! Oh Grissom, you work in the lab, why are you interviewing people?
