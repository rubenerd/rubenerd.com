---
title: "I love Linux! ♡"
date: "2024-06-12T09:32:06+10:00"
thumb: "https://rubenerd.com/files/2024/fedora-40@1x.jpg"
year: "2024"
category: Software
tag:
- bsd
- freebsd
- netbsd
- linux
location: Sydney
---
It’s no secret that I love FreeBSD and NetBSD, and use both wherever I can for work and fun. Their operation makes the most sense to me, they have the tooling I trust and prefer to use, I’m sympathetic to their permissive licensing, and I like being a part of their communities.

This has lead more than a few people email asking why I don’t like Linux, or going as far to claim I must hate it. The TL;DR is, I don’t! I literally run it every day, at work and at home.

<figure><p><img src="https://rubenerd.com/files/2024/fedora-40@1x.jpg" alt="Background by Sosuke on a Fedora 40 desktop" srcset="https://rubenerd.com/files/2024/fedora-40@2x.jpg 2x" style="width:500px;" /></p></figure>

*(With thanks to [sosuke](https://www.pixiv.net/en/users/798322) for the amazing wallpaper. Pavolia Reine and Ceres Fauna are my favourite vtubers. Yes, I'm one of those people! Their original Pixiv account is gone alas).*

I love Linux for a bunch of reasons.

On the server, it just runs (usually). I’d estimate half the customer VMs we run at work are Linux, but they’re responsible for less than 10% of our support tickets. When I *am* on pager duty for the evening and I get an alert from a Linux box, it’s almost always for something external affecting them. I wish I could say the same thing for Windows Server, which seems to spit the dummy at the slightest provocation.

Young people starting out in IT can get a Linux VPS or cloud server and learn to build amazing things. Every Linux server is another that would have previously been an expensive commercial system, or may not have even existed at all. This social good that comes from having access to this, alongside high-quality software a package manager command away cannot be understated; not to mention sticking it to convicted monopolists.

Moving to the desktop, modern Linux has made it feasible for me to not even need Windows for games, something I could have scarcely believed a decade ago. Red Hat Linux, Mandrake, and Cobind were my introduction to \*nix before I even knew BSD was a thing, and I still run a lot of the software I got used to back then (even on the Mac).

This gets to the other fundamental reason I appreciate Linux. Just as Linux users and developers benefit from the tooling and code that comes from BSD, likewise we benefit from Linux existing. I run desktops and applications on my FreeBSD workstation that were mostly built for Linux. When something doesn't have a native BSD build, it’s significantly easier to port or recompile a Linux application for BSD than, say, a Windows program (or even some old school UNIX). BSD users also tend to benefit from vendors realising they need to support Linux; *it's one step closer!*

I don't agree with some of the directions commercial vendors have forced the broader Linux ecosystem to take, though some Linux people don't either!

Point is, even though I defer to BSD where possible, I'd still prefer to run Linux than almost anything else. I'm thankful it exists. 🐧
