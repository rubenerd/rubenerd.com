---
title: "Blocking someone sending comments"
date: "2024-01-02T14:19:28+11:00"
year: "2024"
category: Internet
tag:
- comments
- weblog
location: Sydney
---
This is a bit *Inside Baseball* as my American friends would say, but it's something that happened again that I'm posting about here, rather than responding to the person directly.

Over Christmas I received a long email from someone containing an itemised list of introductory questions about FreeBSD and macOS. It was cordial, though it rubbed me the wrong way for a few reasons:

* Half the questions were easily answered from reading a sentence or two from Wikipedia, or doing a web search. I don't like the blanket advice of *read the fscking manual*, because we all learn in different ways. But these were very rudimentary.

* The volume of questions would have taken hours to answer with the requisite level of detail to be helpful. I'll be brutally honest, I have better things to do, and better people on whom to spend my time.

* The tone of the email was one of a client issuing a support request, not someone reaching out to comment on my writing, or to have a conversation.

These sorts of emails aren't new. Over the years I've come to expect people see you writing about a topic, and they take that as an invitation to ask for the world. There are cultural and language differences as well, but ultimately it's a question of respect. Is the person asking in good faith, or are they taking the piss?

Normally I respond to such queries by directing them to the various online forums, mailing lists, and social networks full of people who could offer documentation and advice, but it was Christmas day, and I didn't. Suffice to say, this person didn't take it kindly, and sent me some inappropriate follow up. I was about to respond with either a Singaporean "wah lao eh" or an Australian-style "mate, what are you doing?", but thought better of it and deleted the message.

I love receiving email from people, and sometimes I even remember and get the time to respond! But I want to be crystal clear here: you just got blocked on my mail server, and I suspect you'll get the same treatment elsewhere with that kind of behaviour. Learn some manners and respect, and you might get some help from someone next time.

*(I know a bunch of people who use Proton Mail, but unfortunately it's become a leading indicator that I'm about to receive a message like this, more than any other email provider. I'm not sure why).*
