---
title: "Your life matters"
date: "2024-06-19T22:10:16+10:00"
year: "2024"
category: Thoughts
tag:
- mental-health
- personal
location: Sydney
---
This is a weird topic to discuss, and I feel unqualified. But I'm going to give it the old college try, as my American friends say. As much as I might write for others, the truth is most of my posts are a way for me to explore an issue or idea myself.

This blog flew under the radar for most of its almost two-decade history, but something changed in the last few years. I get regular comments from wonderful people all over the place, to whom I've mostly failed to respond! Most thank me for giving them a solution to a problem, or say they like what I do here. I've had frowns literally turned upside down from such email.

I get a *lot* of rude, crass, or obtuse messages, usually when I've been linked to from sites like the Orange Peanut Gallery. I usually ignore them, though now I can also direct them to a specific point on my [comment rules post](https://rubenerd.com/comment-rules-and-advice/).

Some of the most touching feedback I've received has been from readers who've said I've helped them in a way beyond just solving a technical problem. They mention my conversational (aka, informal) writing style feels like they're talking with someone every other day. I'm happy my words have been a source of comfort in that capacity, even if I still feel the compliment is unearned, coincidental, or entirely unintentional.

<figure><p><img src="https://rubenerd.com/files/2024/cloudy-day@1x.jpg" alt="A cloudy day in suburban Sydney last week." srcset="https://rubenerd.com/files/2024/cloudy-day@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

In the same vein however, is a phrase with four words. I’ve also been fielding comments from people who've now read my words for a while, and are willing to express more personal details about their views and circumstances. These can venture into rough territory.

Some of these come in the form of a rant or mere venting, which I can appreciate. Some ask for advice, like how did I get a job, girlfriend, uni degree, etc? The heavier ones venture into outright calls for help, with more than a few threatening self-harm.

A lot of this I don't feel qualified to answer, in a few different senses.

First, I'm *literally* not a mental health professional! I'm not qualified to discuss the ins and outs of your circumstances, prescribe actions, or offer meaningful advice. You deserve more.

Second, I feel like I'm doing a decent enough job keeping my own lights on, but that's about the limit of my own personal capacity right now.

And third, it comes down to simple luck. Just like there's no such thing a self-made billionaire, I'm not a self-made technical writer or system architect. I'm here because of hard work, but also by being in the right place, at the right time, with the attention of the right people. Luck favours the prepared, but it still requires luck make an appearance. I had a rubbish childhood, but I also had a lot going for me too.

I'm not sure what I'm trying to say here. I suppose I'm posting about it publicly in the hopes it helps others understand a bit of my own perspective too. I hear you if you're having trouble. I absolutely can empathise with that feeling of despair, awkwardness, loneliness, and fear.

In the words of Merlin Mann, whom I basically quote in every post thesedays, *live your life as though it matters*. Because it does.

