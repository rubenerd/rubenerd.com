---
title: "Peerless Assassin 120 versus 120 SE cooler"
date: "2024-05-02T08:25:19+10:00"
thumb: "https://rubenerd.com/files/2024/peerless-coolers@1x.jpg"
year: "2024"
category: Hardware
tag:
- cooling
- diy
location: Sydney
---
*<strong>TL;DR</strong>: They're about the same. Get whichever is available!*

I've read several reviewers praise the Thermalright Peerless Assassin CPU air cooler, not for its awful name (all modern gaming tech does this), but for the performance it delivers for comparatively little money. I've since seen stores selling two variants; the [120](https://www.thermalright.com/product/peerless-assassin-120/) (left) and the [120 SE](https://www.thermalright.com/product/peerless-assassin-120-se/) (right). What's the difference?

<figure><p><img src="https://rubenerd.com/files/2024/peerless-coolers@1x.jpg" alt="" srcset="https://rubenerd.com/files/2024/peerless-coolers@2x.jpg 2x" style="width:500px; height:270px;" /></p></figure>

According to the manufacturer, they're about the same. Both are dual-tower coolers with six 6 mm copper heatpipes, and two 120 mm Thermalright fans. Reviewers like [PC Analytics 📺](https://www.youtube.com/watch?v=M4AAqbv4m1A) have also concluded their performance is about comparable, which is probably all you need to know.

The 120 uses grey [TL-C12B](https://www.thermalright.com/product/tl-c12-b/) fans, whereas the 120 SE uses black [TL-C12C](https://www.thermalright.com/product/tl-c12c/) fans. Both are advertised as having the same speed, noise levels, air flow, and air pressure stats, with the same 9-bladed design. The former weighs 15g more, if that's at all a measure of quality.

As for the coolers themselves, the 120 has flat, Thermalright-branded caps on the fin stacks, whereas the 120 SE has exposed heatpipes. This does contribute to slightly different dimensions:

* **120:** L125 mm x W135 mm x H157 mm 
* **120 SE:** L125 mm x W110 mm x H155 mm

Based on these tiny differences in fans and size, I'd lean towards the 120 if given the choice. But if there's a price or availability gap, they're probably interchangeable.

This will be my first non-Noctua air cooler I've bought in more than a decade. I'm moving my main desktop back into the CoolerMaster NR200P after being underwhelmed with the CPU cooling ceiling of the otherwise gorgeous Fractal Ridge. I'll post about this soon.
