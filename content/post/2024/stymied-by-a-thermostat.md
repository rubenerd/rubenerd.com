---
title: "Stymied by a 1990s thermostat"
date: "2024-05-28T08:11:23+10:00"
thumb: "https://rubenerd.com/files/2024/thermostat@1x.jpg"
year: "2024"
category: Hardware
tag:
- accessibility
- design
location: Sydney
---
Spend time in an Australian apartment or hotel room built during a specific time in the late 1990s, and chances are you'll come across a thermostat panel that looks, a little something, like this:

<figure><p><img src="https://rubenerd.com/files/2024/thermostat@1x.jpg" alt="A minimal thermostat showing a tiny 2-digit LCD and a few buttons." srcset="https://rubenerd.com/files/2024/thermostat@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

*(The bezel hasn't yellowed, the whole apartment has that beige/cream accent everywhere).*

This is the unit that controls our reverse cycle air conditioner, also known as a *heat pump* in North America. It's been a continual source of frustration since we first moved in, to the point where it still surprises me two years later.

So it's time to play a game; I know you all love these! Wait, where are you going?

For your first question, take a look at the panel. Say I've tasked you with turning it on, and setting the heater to 23 degrees. What buttons would you press? Take your time, look at the buttons and have a proper think.

I know my readers are intelligent, rational, and above all attractive people, so I can anticipate your response. You *probably* said that you should press the **On** button to turn it on, followed by pressing the **Up** arrow to increase the temperature. Right?

*Wrong!* Don't fret though, Clara and I made the same mistake.

It's a fair assumption. Every other air conditioner remote control and thermostat I've ever used operates this way. It also makes intuitive sense: you press the **Up** arrow on a thermostat for more warmth, and the **Down** arrow to make the room cooler. Except, no!

This leads to the inevitable second question. If pressing the **On** button, then the **Up** arrow doesn't increase the temperature... what *does* it do? These were the responses from a straw poll at work:

* Increase the fan speed

* Toggle the mode from auto (the default), heat, or cooling

* Spring out a rubber chicken

None of these were correct either.

Give up?

*It tells you the current temperature of the room.*

I want everyone to take a moment to appreciate this. You press the **On** button, followed by an **Up** arrow, on a *thermostat*, and it... tells you how hot or cold the room is.

If that sounds ridiculous, it gets even worse. Pressing the **Up** button doesn't just tell you the current temperature of the room, but it blinks *every other LED on the entire panel* along with it. You're being told the aircon is operating in cool mode, warm mode, and auto mode, and the fan is set to low, mid, and high. Concurrently! It's inscrutable to a layperson, and looks like a crash or a bug to an engineer, because it *looks like it's broken*. At what point would it make sense for a fan to be spinning at three speeds at the same time!?

Consulting the horribly-written manual from reception downstairs however, and this is expected behavior. There's a truth table that indicates the temperature sensor output is being displayed if all the LEDs are on. If another nonsensical subset of the LEDs are on, it means certain other things. And so on.

This gets to the core of the issue here. Because the people who designed, built, and sold the panel were too cheap to include a *single* extra button called "sensor" or "room temp", they had to have the feature invoked by pressing a sequence of other buttons. A sequence, mind you, that not only makes *no sense*, but is apparently so integral to the system's operation that they were willing to replace a *core feature* with it. Namely, setting the temperature on a thermostat. It boggles the mind.

If you've made it this far, you know what the third question is going to be. If **On** and **Up** doesn't turn the aircon on and turn the heat up, and instead shows the temperature of the room... how *do* you turn the aircon on and turn the heat up? Any guesses?

The answer is only slightly less ridiculous. You have to press the **Up** button and *hold it down for more than a second*. Again, you don't just press it like you would any other switch panel or remote control made across the ages, you have to keep it pressed. This is not written anywhere on the device.

Why is this bad, besides it being completely unintuitive, and subservient to another feature that's almost completely useless? Well, if you have a range of temperatures to scroll through, you can't just rapidly press the button. No, you have to hold it down and wait for it to count up or down. Slowly. Eventually! *21... 22... 23...*

This panel has a few other fun tricks. There's a secret admin mode you can easily invoke by accident with another combination of buttons that can be used to set the upper and lower temperature bounds, which can lead to situations where it's the dead of winter and you can't go above 18 degrees, or summer where you can't go below 24, requiring a tech to come out to reset it. Always fun!

We're in the process of moving, so we won't have to put up with this silly thing for much longer. Maybe. We hope!
