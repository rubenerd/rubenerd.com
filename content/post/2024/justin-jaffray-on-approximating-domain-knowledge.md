---
title: "Justin Jaffray on approximating domain knowledge"
date: "2024-06-26T09:27:37+10:00"
year: "2024"
category: Software
tag:
- data
- databases
location: Sydney
---
Among the biggest challenges in IT hiring is finding people who have domain knowledge in your industry. You're rarely doing IT for IT's sake, you're probably designing, architecting, building, maintaining, upgrading, and/or replacing a system that interfaces with, or serves, a specific use case. A firm might not need a Perl programmer, they need a Perl programmer who understands geospacial data. Or a C# developer who knows about logistics. Or a Rust developer who understands hypervisors.

I suppose that discusses the supply-side issue, but what about development itself? Database expert Justin Jaffray gave me food for thought in an [August 2023 post](https://justinjaffray.com/hard-and-soft-statistics/ "Hard and Soft Statistics") that I'm still mulling over now:

> One lens I've found satisfying for "what is a query planner" is that it's a tool for "approximating domain knowledge."
> 
> By that I mean, someone who knows Python could probably write a pretty solid program to generate an end-of-month report for an airline company. But to write a **good** version of that program probably requires both knowledge of the distribution of flights, passengers, and the internal IDs used by the company. Such things (how many of each there are, how they're related to each other, etc.) influence lots of programming decisions, like how to order lookups or what data structures to use in various scenarios.

This is where the query planner comes in:

> One of the big wins we get when we move queries from an imperative language like Python to a declarative language like SQL is that we give the database a chance to express its opinion on things in [domain-specific knowledge].

I haven't been a DBA for many years now, but I found his points compelling. Give the post a read, and [subscribe to his newsletter](https://buttondown.email/jaffray).
