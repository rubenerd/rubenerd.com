---
title: "Dave Farquhar on CD-ROMs in early PCs"
date: "2024-05-24T08:22:15+10:00"
year: "2024"
category: Hardware
tag:
- am386
- cd-roms
- retrocomputing
location: Sydney
---
Dave over at the Silicon Underground wrote a [great historical post]( https://dfarq.homeip.net/cd-rom-drives-in-286-and-386-pcs/ "https://dfarq.homeip.net/cd-rom-drives-in-286-and-386-pcs/") about the Multimedia PC standard, and the existence of CD-ROMs in 386 and 486-era machines.

> The Golden Age of CD-ROM titles definitely came along a generation or two after the 286 and 386, during the 486 and Pentium era. CD-ROM drives were standard equipment by the time the Pentium was mainstream, and while low-end 486s didn’t necessarily come from the factory with a CD-ROM drive, they were a very popular upgrade. Having sold computers at retail in 1994 and 1995, I would estimate at least half of the computers I sold came with CD-ROM drives. And if a day went by with me not selling a CD-ROM upgrade kit, it must have been a slow day.

This is exactly what our family did with our 486 SX. It originally had a Sound Blaster 2.0 card, but one Christmas I came downstairs and noticed it had a shiny new hex-speed Creative CD-ROM drive, and the sound had been upgraded to an SB16. It was one of those Creative CD bundles that came with a *tonne* of multimedia and game discs, most of which I still have in my classic CD binder today.

Dave goes into more detail about installing CD-ROMs into earlier machines, like 386-based PCs:

> So if you have a 386 today, and installing a CD-ROM drive makes it easier for you to install software on it, or if the case simply has two open 5.25-inch drive bays and you don’t have the blanking plate for one of them, just go for it. It’s not like people in the 386 era didn’t want a CD-ROM drive.

And even earlier:

> Installing a CD-ROM drive in a 286 generation machine is more controversial than a 386-era machine. But it also wasn’t unheard of. The very first CD-ROM drives, like the Philips CM-100 and Hitachi CDR-1503, came with interface cards that only required an 8-bit ISA slot, so they even worked on an 8088-based PC, let alone a 286.

I also loved his comment that a PC with Microsoft Bookshelf was the closest to having Google in the 1991. Part of the reason I love reading multimedia CD-ROMs today is precisely because they're snapshots in time.

<figure><img src="https://rubenerd.com/files/2023/ess-cd-ide@1x.jpg" alt="Photo showing the optical drive, the ESS IDE CD-ROM controller loaded, and the VIDE-CDD driver loaded." style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2023/ess-cd-ide@2x.jpg 2x" /></figure>

As for running the CD-ROMs themselves, I've written posts about bootstrapping them off an [ESS AudioDrive](https://rubenerd.com/the-ess-audiodrive-es1868-for-sound-and-ide/ "The ESS AudioDrive ES1868F for sound and IDE") and a [Sound Blaster card](https://rubenerd.com/using-an-ide-cdrom-with-a-sound-blaster-32/ "Connecting a CD-ROM to a Sound Blaster 32 IDE header in DOS") with integrated IDE on my own 386. They're fiddly to get working, but it's possible if your multi-IO card only has one IDE connector, or your PC has none.

Recently though I've decided that the memory their drivers take up, and the speed of the drives, doesn't really make sense for how I use these earlier PCs. I got a [cute new case for my 386](http://retro.sasara.moe/#dfi-am386) that only had one 5.25-inch drive bay, so I dedicated that to a 5.25 drive instead. A 3Com NIC also lets lets me transfer files I need from the home network, and my [486](http://retro.sasara.moe/#nec-apex) and [Pentium MMX](http://retro.sasara.moe/#childhood-mmx) machines make better Multimedia PCs. But it was still a fun experiment seeing how far back I could push a CD-ROM.

