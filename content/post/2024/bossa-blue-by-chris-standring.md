---
title: "Bossa Blue by Chris Standring"
date: "2024-11-18T17:10:05+11:00"
thumb: "https://www.youtube.com/watch?v=vyGiKR7IzPs"
year: "2024"
category: Media
tag:
- jazz
- music
- music-monday
location: Sydney
---
It's [Music Monday](https://rubenerd.com/tag/music-monday/) time! Each and every Monday, except when I forget, I regale everyone with tales of a specific tune, album, artist, genre, or combination of all of the above. Except when I forget.

Today we have a bit of a treat. English jazz extraordinaire Chris Standring's *[Bossa Blue](https://www.youtube.com/watch?v=oV5Rr1ZMy6s)* has been in my playlists for years, but I only just heard him [perform it live](https://www.youtube.com/watch?v=vyGiKR7IzPs) in 2017. Which I suppose isn't *live* per se, given it was recorded seven years ago. It was live when it was recorded. But then... isn't *everything* live when it's recorded? I guess mixing and post-production would alter that, as opposed to a live performance. A live performance would be live to the people watching the performance, and live in the sense that someone was listening to a live performance. How did we get onto this?

<figure><p><a target="_BLANK" href="https://www.youtube.com/watch?v=vyGiKR7IzPs" title="Play Chris Standring - Bossa Blue (Live in London)"><img src="https://rubenerd.com/files/2024/yt-vyGiKR7IzPs@1x.jpg" srcset="https://rubenerd.com/files/2024/yt-vyGiKR7IzPs@1x.jpg 1x, https://rubenerd.com/files/2024/yt-vyGiKR7IzPs@2x.jpg 2x" alt="Play Chris Standring - Bossa Blue (Live in London)" style="width:500px;height:281px;" /></a></p></figure>

For all the concerts I've been to, I've only been to a few jazz ones. I *really* need to address this. To myself. With tickets to a jazz concert. Live.
