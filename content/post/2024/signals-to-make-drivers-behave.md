---
title: "Signals to make drivers behave"
date: "2024-06-07T21:29:28+10:00"
year: "2024"
category: Thoughts
tag:
- cars
- urbanism
location: Sydney
---
*This post is dedicated to Rebecca, who's insistence that Clara and I exercise to help our mental states have helped tremendously... and also lead me to finally shed all my COVID-19 lockdown weight! You could say we could all benefit from getting out of cars.*

Operators of four-wheeled, motorised conveyances have copped some flack here of late, and I'll admit it's not entirely justified. Most Australian drivers I interact with as a pedestrian are courteous and responsible, to the point where some will even stop on an empty street to let me pass, even when the law says they don't have to. They smile and nod at me, I smile and wave back, life is good.

No seriously, kind interactions like that make my day!

Unfortunately, this isn't universal. Certain drivers have this entitlement complex that says they own the road, and their convenience is more important than your safety. Talk with everyone from interstate truck drivers to acoustic cyclists, and you'll hear stories of drivers doing reckless things. Even other motorists probably do, now that I think about it.

This trend would be bad enough, if not for the fact that cars themselves are getting heavier, bigger, and faster. Simple physics dictates that a pedestrian stands less of a chance if hit by an imported American light truck or suburban SUV than they would a small hatchback, and the former are selling in far greater numbers.

This has lead to some creative approaches to citizen enforcement of road rules and responsible driving. The [Grab a Brick](https://globalnews.ca/news/10395423/vancouver-pedestrian-safety-brick-campaign/) campaign in Vancouver is a famous recent example that started as a joke, but took on greater significance:

> Over the long weekend, the group installed bins of foam rubber “bricks” at crosswalks in Granville Island and the West End. Signage is posted encouraging pedestrians to “be seen” by grabbing a brick, looking for traffic, waving at vehicles and crossing.
>
> “It’s to poke fun at the idea that it should be the obligation of a pedestrian … to actually beg for safe passage across the road. We can do better than that.”

Closely-spaced bollards have demonstrated bad drivers who don't care about safety suddenly *do* if there's a chance of scratching their paint, so of course they slow down at the sight of someone carrying a potential projectile. It's a shame cyclists and pedestrians aren't accorded that level of respect automatically.

[Marc Hedlund shared a story](https://xoxo.zone/@marcprecipice/112542373791346194) recently in which they noticed drivers behaved more responsibly after a simple change:

> I’ve experienced this, too: when I have a camera or anything that looks like a camera on top of my bike helmet, drivers are *much* more cautious and respectful around me. 
> 
> It really lays bare that they can see me—can even see a small camera on top of me—but don’t care to be cautious around me unless they fear being caught.

I've been doing a variation on that theme, but with my phone. There's an intersection in Chatswood that motorists constantly queue across, blocking it for pedestrians and drivers coming from the side street. I take my phone out, put it in landscape, and make a deliberate showing of recording them, paying close attention to their licence plates, then panning back up to the red light. I've done this to one local Maserati SUV driver a few times now, and he must recognise me, because he now stops behind the white line where he's supposed to.

<figure><p><img src="https://rubenerd.com/files/2024/badcars@1x.jpg" alt="Two bad cars queued across an intersection on red, which is illegal and dangerous." srcset="https://rubenerd.com/files/2024/badcars@2x.jpg 2x" style="width:500px; height:333px;" /></p></figure>

It's also not just car drivers. Irrespective of why it got this bad, Sydney bus drivers are some of the worst I've ever interacted with, as a passenger and a pedestrian. I've done my phone filming trick around more than a few of them that queue across this same intersection, and the look of apoplectic rage their drivers give me could melt the metal frame of the bus around them. Pity they don't have as much concern for the dad pushing a pram, or the woman in a wheelchair, or the kids with their schoolbags, who now all have to walk *into the middle of the intersection* because their bad driving blocked the pedestrian crossing... for the third light cycle.

It shouldn't take the threat of physical damage or enforcement to slow these people down and to obey the rules. Here comes the proverbial posterior prognostication: *but...* as long as this stark power balance exists in their favour, I harbour no reservations about pedestrians, motorcyclists, and other road users doing these things.

