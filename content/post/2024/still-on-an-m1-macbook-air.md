---
title: "Still on an M1 MacBook Air"
date: "2024-09-03T15:48:25+10:00"
year: "2024"
category: Hardware
tag:
- apple
- macbook-air
location: Sydney
---
My primary portable machine is a first-generation M1 MacBook Air, as I've posted here a few times. I need to edit Microsoft Office documents for work, and I don't want to deal with the hassle of Windows in a VM, or dedicated hardware. I've also been a Mac user (albeit not exclusively) for more than 20 years, and have all the usual tricks, scripts, and muscle memory one accumulates.

Apple's software quality has famously nosedived in the last few years, with the die-hard fans its most vocal critics. UI regressions, dropped features, bad design, flaky performance, and abandonment of wonderful tools like Aperture don't put macOS at the level of Windows, though it seems Cupertino are hell-bent at making this convergence happen.

I'm more conflicted on the hardware side. Apple's kit is more sealed, locked down, and hostile to repair than it's ever been; so much so that I use my 1985 [Apple //e Platinum](http://retro.rubenerd.com/iie.htm) or 2001 [Power Mac G4](http://retro.rubenerd.com/powermac.htm) and am shocked I'm using the same company's kit! On notebooks we've gone from having user-serviceable batteries, memory, drives, and power inverters, to... none of those things. I've likened it to feeling like a guest in my own home, and completely at the mercy of limited warranty periods.

Which is shame, because the hardware is *good*. Every PC laptop I use/fix/test feels like a massive downgrade. They're twice the weight, have half the screen resolution, get warm during extended use, have rubbish plastic construction, and aside from standouts like the ThinkPad, have mediocre keyboards. The fact PC manufacturers generally can't match screens Apple released *more than a decade ago* used to be silly; now it's just [embarrassing](https://rubenerd.com/tag/pc-screen-syndrome/).

<figure><p><img src="https://rubenerd.com/files/2024/macbookair@1x.jpg" alt="Photo of the MacBook Air M1 in front of my Power Mac G4, and Dell 4100." srcset="https://rubenerd.com/files/2024/macbookair@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

This MacBook Air is three years old. Work keeps offering to upgrade it for me, but I see no need. Aside from maybe some more memory, there isn't anything better or more compelling in the newer models for my use case. I'm not left wanting with anything I do on this machine, whether it's firing up a few VMs, editing an unholy Excel or LibreOffice sheet, writing blog posts or code, or using Ansible to install or maintain deployments for myself or customers. This machine comes out of sleep instantly, and runs all I want without breaking a sweat. From a financial, ewaste, and logistical perspective, I'll be happy using this thing until the day macOS is no longer supported… and maybe even then, there's [Asahi Linux](https://asahilinux.org/).

I've kind of reached this happy point where I have a day-to-day MacBook Air, a FreeBSD/Fedora workstation/game machine at home, a tiny Japanese Let's Note running NetBSD as my “on call” laptop, and a mini home server cluster for odd jobs, backups, and media delivery. I hold out hope that maybe one day the upgrade for the work machine will be something like the Framework, but for all its positives, I'm not sure it'd feel like an upgrade right now.

It does make me wonder what Apple would have to release for me to want to get something new. It's a weird way of thinking for a guy who used to obsess over MacWorld and WWDC, dreaming about the PowerBook that would be so much more epic than my lowly iBook G3. I'd love a celluar modem to stop the need to constantly tether when I'm out on the (rail)road, but that's honestly about it.
