---
title: "Pseudoephedrine in Australia"
date: "2024-02-12T19:08:58+11:00"
year: "2024"
category: Ethics
tag:
- health
location: Sydney
---
**DISCLAIMER:** I am not a doctor or pharmacist, and this post is not medical advice. Speak to a qualified health care professional if you have any questions or concerns.

🏥

Now *here's* a niche topic! But it's something I had to deal with today.

The genes that gifted me blogging prowess, rugged attractiveness, and self-delusion also saddled me with unusually-narrow nasal passages. It's not life threatening, but it makes anything but mild congestion excruciating, like I'm continuously snorting wasabi, and without the benefit of some nice sashimi on the side.

Pseudoephedrine works for this, *extremely well*. With Clara and my sister as witnesses, I've cried from relief after taking it during especially acute episodes. *K-On!* had a lot of very a-cute episodes. That reminds me, I'm long overdue talking about *BOCCHI*. But I digress.

Pseudoephedrine used to be the active ingredient in most off-the-shelf decongestants in Australia, until we were told certain parties discovered they could make narcotics from it. This lead to it being a [controlled substance](https://www.guild.org.au/guild-branches/nsw/professional-services/health-services/pseudoephedrine-recording "Pharmacists Guild: Pseudoephedrine Recording"), meaning there are certain hoops one must jump through to get access. Gymnastics moves are difficult enough are they are now, let alone when one is ill.

To get around the restriction, decongestant tablets sold in Australia made the switch to *phenylephrine*, a drug that sounds like something you'd make bottles from. Wait no, that's polypropylene. I may as well have chewed plastic though, because boxes from three manufacturers did absolutely nothing. They even failed as placebos; I was fully primed to accept relief when I saw the familiar packaging, not knowing at the time it was a reformulation.

An American Food and Drug Administration panel since confirmed that *phenylephrine* taken orally [does nothing](https://edition.cnn.com/2023/09/12/health/phenylephrine-tablets-ineffective-fda-panel-says/index.html "CNN: Popular OTC medicines for colds and allergies don’t work, FDA panel says"). Specifically, the active ingredient *is metabolised before it reaches your nose*. This was validating; no wonder it did sweet FA. Not to be confused with <a href="https://en.wikipedia.org/wiki/Fa_(philosophy)" title="Wikipedia article on the philosophy of Fa">Fa</a>... or should it?

If that's the case, is a phrase with four words. Why is an ineffective substitute being pushed on people without their knowledge? The packaging of these tablets, the shelves, and pharmacy staff either don't mention the difference, or aren't allowed to. People are going to pharmacies in their hour of need, presumably in a lot of pain and stress, and are leaving with expensive salt tablets. 

I understand there are risks associated with certain medicines. Here comes the proverbial posterior prognostication: *but...* the key word is *informed* consent. Substituting a substandard medicine is one thing, but an entirely ineffective one strikes me as especially cynical. And while we're on the subject of legality, whatever happened to false advertising, and duty of care? *The stuff doesn't work.*

I knew from experience and research that I could go to a counter and ask for a pseudoephedrine-based decongestant with my [government ID and a proof of address](https://www.health.nsw.gov.au/pharmaceutical/Documents/guide-supplyS3.pdf "NSW Health PDF on the supply of schedule 3 substances"). It helps when you're muffling coughs and sniffles under a mask too, though presumably that's optional. But what if you *don't* know this? There are no signs anywhere telling you.

My heart goes out to people suffering at home with those crappy tablets. It's not just you, they're really not doing anything.

