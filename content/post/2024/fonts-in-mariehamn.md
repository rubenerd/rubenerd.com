---
title: "Fonts in Mariehamn"
date: "2024-03-05T08:35:59+11:00"
year: "2024"
category: Media
tag:
- europe
- fonts
location: Sydney
---
Speaking of Åland in that [previous Cloudflare post](https://rubenerd.com/cloudflares-ipv6-map-is-missing-something "Cloudflare's IPv6 map is missing something"), this font on the side of a Bank of Åland branch is *amazing*, and I want it.

<figure><p><img src="https://rubenerd.com/files/2024/aland-bank@1x.jpg" alt="Photo of a lovely street in Mariehamn in Åland, showing an art deco style font spelling BANK." srcset="https://rubenerd.com/files/2024/aland-bank@2x.jpg 2x" style="width:500px; height:281px;" /></p></figure>

Photo by <a href="https://commons.wikimedia.org/wiki/File:%C3%85landsbanken,_Mariehamn,_2019_(01).jpg">Bahnfrend on Wikimedia Commons</a>.
