---
title: "Coffee shop under new management"
date: "2024-02-27T08:29:50+11:00"
year: "2024"
category: Thoughts
tag:
- coffee-shops
location: Sydney
---
One of my regular haunts has new staff and music, but everything else is the same. Feels weird.

I wonder where the old people went?
