---
title: "The Duke of Edinburgh Award"
date: "2024-10-30T08:57:30+11:00"
year: "2024"
category: Thoughts
tag:
- family
- memories
location: Sydney
---
*According to the timestamp, I wrote this post in December 2018 but never published. I only just found it in a configuration folder labelled rc.conf; clearly I saved the wrong buffer!*

When I was in high school in the 2000s, a bunch of my cohort participated in the Duke of Edinburgh Award programme. I can't remember much of what it entailed, though there was a social outreach and community service element.

One morning I remember several of the students entering the oncology ward at the hospital to do a day of volunteering, ostensibly to earn credits for the award. I knew at least one of them had boasted it'd be "easy", and that she'd get out as soon as her logbook was signed. Others were a bit more altruistic.

I know they were there, because I was too. I had been every week for most of my high school life, taking care of my mum who was going through cancer treatment. I was there so regularly the head of our year Ms Samuels referred to me as a “part-time student”. It's funny how little I remember of my school life now, but random little phrases like that stick around.

Anyway, some of the other kids were really giving their all. You could tell they were out of their element, but their heart was in the right place. The others I discussed… not so much.

The experience did raise a thought though. If these people could earn credits towards something for doing a day of social good, surely my experience doing it *hundreds* of times would be worth a *lot!* Heck, I even helped out other people in the ward sometimes too! Dr Tan even said he'd vouch!

The teacher and representative of the award at the school could not have been more blunt, after he laughed at what he thought was a joke. I know right; cancer, hilarious! "Nah, doesn't count." "But what if I…" "Nah, you wouldn't qualify."

Oh well! All I knew was I was there the following week. None of the other kids were.

A year later, and they got their awards in a large assembly, with many a speech delivered about their transformative, life-changing experiences helping people and improving their lives. I could feel my face getting hot at the sheer volume and scale of the bullshit, so I stood up and walked out. I still remember my maths teacher and confidant Ms Harris catching my eye as I left, and nodding in approval. She was a legend.

I forgot about this for years, but recently I overheard some people at a coffee shop wax lyrical about their time doing the award and how, to use their words, it "changed their lives". They couldn't understand why their own kids didn't want to do it, and made mention of the fact they lacked conviction, or something. A few minutes later, they were haranguing the staff for their "extra hot" coffee not being hot enough.

My experience is that good people aren't motivated by glory or attention; they act because they care. Still, I'm sure they'd appreciate the gesture.
