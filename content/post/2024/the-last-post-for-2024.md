---
title: "The last post for 2024"
date: "2024-12-31T08:04:48+11:00"
abstract: "There were 560 posts, 203,900 words, and only a singular mention of creamed corn."
year: "2024"
category: Internet
tag:
- pointless-milestone
- weblog
location: Sydney
---
[This year](https://rubenerd.com/year/2024/ "Year archive for 2024") the [blog turned twenty](https://rubenerd.com/this-site-turns-20-today/)! There were 560 posts, 204,081 words, and only a singular mention of creamed corn&hellip; right there.

This was a hard year personally, but also one with lots of milestones. Next year I plan to spend less time fixated on news, more time reading people, and investing into fixing things that I've left on the back-burner while we spent  our time and money on buying a place.

Happy New Year to you, and thanks for reading. ♡
