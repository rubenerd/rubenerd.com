---
title: "Evaluating book summary services"
date: "2024-11-09T14:20:02+11:00"
year: "2024"
category: Internet
tag:
- books
- reading
- reviews
location: Sydney
---
Last year I signed up for one of those online book summary services. I'd been curious for a while, and they were offering a steep discount for the first year. I figured I could give it a shot, and cancel it before renewal if I didn't find it useful.

This service promised to "distill" books to their "essence", and deliver a summary in a half hour audio programme. I thought it'd be a fun way to read summaries while getting some of my 10,000 steps. I used to read a *lot* of audiobooks.

While billed as a way to save time (we'll get back to that), I was more interested in using it as a way to gauge my interest in a topic before diving further into it. I get by with recommendations for fiction and light novels, but non-fiction is tricker. There's a lot of hype around some of these titles, especially when we get into self-help and adjacent books. I tend to only discover I don't like one of these books after a couple of chapters, but the free samples offered by ebook stores don't quite cover that.

The summaries were useful to that end. I bought a few books after I heard the summary, because I was intrigued. Likewise, I avoided other famous tomes based on the summaries.

You can likely spot the problem though. Just to see how accurate and useful these summaries were, I listened to their programmes about three books I knew very well. Two I'd consider fine, but one could best be described as a misrepresentation. I wouldn't consider it malicious, but the writer had missed key points in the book, and the conclusion was nothing like what the original author intended. This lead me to wonder if I'd discounted other books based on their summaries, some of which may have been inaccurate. Once I started worrying about this, any enjoyment or interest I got out of the service evaporated.

Which leads us to the idea that these summaries save time. It makes sense on the surface; a half-hour audio programme is faster than several hours reading a book. But reading non-fiction isn't just recognising words in your head, it's about ruminating on ideas and building understanding. Short snippets save you time in the same way a fast food burger saves you cooking. It's *true*, but you miss out on a lot.

I'm glad I tried one of these services out, but I can't say I'd recommend them, either as a book "preview" service, or a way to cram more books into your day.
