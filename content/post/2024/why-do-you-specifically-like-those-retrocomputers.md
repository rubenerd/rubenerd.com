---
title: "Why do you specifically like those retrocomputers?"
date: "2024-09-24T08:13:46+10:00"
thumb: "https://rubenerd.com/files/2024/iieplat@1x.jpg"
year: "2024"
category: Hardware
tag:
- feedback
- retrocomputing
location: Sydney
---
This is a question that comes up a lot in the context of retrocomputer discussions. There were so many, from so many different companies, why did I end up settling on the ones that I ended up on? What is it about them that intrigues me so much?

I'm interested in so many old systems, but the four main ones I spend the most time on are:

* 8-bit Commodore machines, such as the [VIC-20](http://retro.rubenerd.com/vc-20.htm), [Plus/4](http://retro.rubenerd.com/plus4.htm), and the legendary [C64](http://retro.rubenerd.com/64c.htm) and [C128](http://retro.rubenerd.com/c128.htm).

* The Apple II line, in particular the later models like the [Apple IIe](http://retro.rubenerd.com/iie.htm), IIc, and IIGS.

* Early 32-bit machines of specific architectures, such as the [486](http://retro.rubenerd.com/necapex.htm), [SPARC](http://retro.rubenerd.com/sparc5.htm), and [PowerPC](http://retro.rubenerd.com/powermac.htm).

* A grab bag of old expansion cards and modern reproductions built around the Z80, usually to run CP/M. These overlap with the other machines above, too.

Some of this is easy to explain. I grew up on early 32-bit machines like the Pentium, Apple’s G3 Macs, and went to university on Solaris kit, so I naturally hold an affinity for them. Likewise, my primary school *still* had plenty of Apple II hardware even when I started in the mid-1990s, so seeing and hearing them takes me back to a simple, happy time part of my childhood.

This I think gets to the core of a lot of retrocomputing interest: a rekindling of that unbridled joy and wonder you first felt staring at that glowing screen. I’ve been chasing that high ever since.

<figure><p><img src="https://rubenerd.com/files/2024/iieplat@1x.jpg" alt="Photo of my Apple IIe Platinum" srcset="https://rubenerd.com/files/2024/iieplat@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

Okay, but what about the Z80? My CP/M adventures weren't borne of direct experience, but a more recent desire to learn what came before the first OS I grew up on as a kid: DOS. In the process I learned about Digital Research, Gary Kildall, and this wonderful ecosystem of interesting software that scratches that specific itch I have. I tend not to game on my old machines, instead preferring to run and explore what people ran at the time to do their jobs and be creative. Think obscure utilities, office productivity, paint tools, networking, that sort of thing.

Related to this, I hold a specific fascination with [Zilog Z80 coprocessor boards](https://rubenerd.com/z8o-cards-for-the-apple-ii/) like the ones made available on the Commodore and Apple II platforms, perhaps in part because the idea seems so novel to a modern computer user today. Imagine booting your Ryzen PC with an ARM board connected over the PCIe bus, or a RISC-V board jumping to a daughter board with an Intel N100. It’s weird, wonderful, and I’m all for it!

Which leads us to Commodore, the undisputed king of early 1980s computing. In a world of Atari, Apple, Commodore, and Radio Shack in the US; Acorn, Amstrad, BBC, and Sinclair in the UK; and Dick Smith and Microbee locally in Australia; what is it specifically about the Chicken Lips company that captivates me so much? This is harder to answer than I first thought.

My maternal grandfather worked at Rank Xerox and had access to tonnes of cool kit, but he harboured the most love and interest for his PET and C64. Their nostalgic designs certainly imprinted on me when I used to visit as a kid, but I don’t have any specific memories using them. But that might have been enough to pique my interest as a teenager many years later, and my parents buying me my first C64, C16, Plus/4, and 1541 disk drive off eBay for my birthday in 2004.

Ever since, I've continued to be *floored* at the capabilities of these systems. The BASIC and gorgeous colour palette of the Plus/4. The sprite and sound capabilities of the C64. And later down the line, the C128 courtesy of [Screenbeard](https://the.geekorium.com.au/), which is the single most capable 8-bit machine ever made, thanks in no small part to its Z80 for running CP/M!

Why people like things is a complicated question, and I clearly don't feel as though I fully understand them here either. But if you'll forgive the modern Apple marketing speak, that mystery is what makes such a hobby so magical.
