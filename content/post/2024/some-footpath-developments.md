---
title: "Some footpath developments"
date: "2024-04-11T13:17:32+10:00"
thumb: "https://rubenerd.com/files/2024/footpath-1@1x.jpg"
year: "2024"
category: Thoughts
tag:
- australia
- design
- photos
- sydney
location: Sydney
---
Here are some footpath-related observations I've made recently (or *sidewalk* for my North American friends), that have caused me considerable concern.

The first is a farewell to an Australia Post and Express Post mailing box, which sat on the side of this footpath for decades before finally being removed this month. This is a busy thoroughfare opposite a large shopping centre in Sydney, so I'm a bit surprised. I guess we send parcels to each other from online commerce, but not as many letters anymore?

<figure><p><img src="https://rubenerd.com/files/2024/footpath-1@1x.jpg" alt="Photo of a footpath at night showing a red and white platform where the post boxes used to be." srcset="https://rubenerd.com/files/2024/footpath-1@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

But it's the state of the footpath in this second photo that was truly horrifying. The telco workers or council didn't match the tiles at all! What's going on!?

<figure><p><img src="https://rubenerd.com/files/2024/footpath-2@1x.jpg" alt="A couple of utility covers with mismatched tiles." srcset="https://rubenerd.com/files/2024/footpath-2@2x.jpg 2x" style="width:500px; height:375px;" /></p></figure>

I have intimate experience with utility covers of this nature, having [fallen into one](https://rubenerd.com/i-broke-ground-on-sunday-with-my-ankle/) in 2013. But at least that cracked one had a consistent surface around it, not whatever is going on here. Hey, an attempt was made.

That concludes my in-depth reporting on the worrying state of this street in Sydney, though technically the state is New South Wales. Sometimes I worry that I'm too funny, but the feeling rapidly subsides.
