---
title: "I like Gemini and Gopher… but also HTML"
date: "2024-07-17T15:14:49+10:00"
year: "2024"
category: Internet
tag:
- accessibility
- design
- gemini
- gopher
- html
location: Sydney
---
Gemini and Gopher sites are fantastic. Their pages are simple to write, serve, and read. But my [Retro Corner](http://retro.rubenerd.com/) has also reminded me that I specifically love HTML. I love the organic process of laying out site structures and writing pages in this markup format.

I'm realising now I love Gemini and Gopher as much for what they represent. When I open the [Lagrange](https://gmi.skyjake.fi/lagrange/) browser, I know I won't be battling all the dystopian dark patterns and excesses that have come to define the modern Web. It's a breath of fresh air in a world choking on fumes.

Maybe we need a signal to say your HTML page is written in the same spirit. Our browsers are capable of rendering minimal pages that respect people. It almost seems defeatist throwing in the towel and starting again somewhere else.
