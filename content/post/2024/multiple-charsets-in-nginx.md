---
title: "Multiple charsets in nginx"
date: "2024-01-10T09:13:30+11:00"
year: "2024"
category: Internet
tag:
- nginx
- retrocomputing
- servers
location: Sydney
---
You know my post last October about [why we should use UTF-8](https://rubenerd.com/iso-8851-and-english-arent-the-default/ "ISO-8859-1 and English aren’t the default") everywhere? Well, Clara and I cheat with [Sasara.moe](http://www.sasara.moe/), our retro themed web server. I wanted these pages readable on our retrocomputers, so I deliver them over standard HTTP with ISO-8859-1.

Problem is, my nginx reverse-proxy stamps all my proxied FreeBSD jails with UTF-8, as it should:

    ## /usr/local/etc/nginx.conf
    http {
        charset utf8;
    }

Today I learned this can also be overridden in individual server blocks, and even within locations:

    ## /usr/local/etc/conf.d/www.sasara.moe.conf
    server {
        listen 80;
        server_name www.sasara.moe;
        charset ISO-8859-1; 
           
        location /folder/ {
            charset utf8;
        }
    }

You can confirm these with curl:

    $ curl -I www.sasara.moe | grep charset
    ==> Content-Type: text/html; charset=ISO-8859-1

