---
title: "Useful cross-platform desktop emulators"
date: "2024-07-13T09:44:45+10:00"
year: "2024"
category: Software
tag:
- atari-st
- commodore
- games
- pdp-11
- retrocomputers
- virtualisation
location: Sydney
---
I haven't done a virtualisation post in a while, so thought it'd be fun to list what's in my current toolbox.

<figure><p><img src="https://rubenerd.com/files/2024/emulators2024.png" alt="Icons for the various emaultors on my MacBook Air" style="width:486px; height:226px;" /></p></figure>

**[86Box](https://86box.net/)**
: <p>A low-level x86 emulator to run software and OSs for the original IBM PC, up until the Pentium era. It can emulate sound cards, NICs, even Zip drives. I spend too much time in this.</p>

**[DOSBox-X](https://dosbox-x.com/)**
: <p>I mostly run DOS-era stuff in 86Box now, but this DOSBox fork is great for rapidly launching software or a game with all the goodies. It just works.</p>

**[Hatari](http://hatari.tuxfamily.org/)**
: <p>Emulator for the Atari ST, my favourite 16-bit computer line from the late 1980s. It’s a <strong>GEM!</strong> No, I’m not apologising for that.</p>

**[Open SIMH](https://opensimh.org/)**
: <p>This incredible emulation framework can run dozens of classic minicomputers. I use it for tinkering with the PDP-8 and PDP-11.</p>

**[QEMU](https://www.qemu.org/)**
: <p>Still the gold standard for hardware emulators. I use it for messing with x86, RISC-V, and SPARC on my M1 MacBook Air.</p>

**[ScummVM](https://www.scummvm.org/)**
: <p>Not strictly an emaultor, it lets me play the classic Humongous Entertainment RPGs written in the SCUMM system I grew up with.</p>

**[UTM](https://mac.getutm.app/)**
: <p>MacOS only (whoops), but is the best GUI for Apple's Hypervisor. Can also be used in conjunction with QEMU.</p>

**[VICE](https://vice-emu.sourceforge.io/)**
* <p>The Versitile Commodore Emulator, which can emulate all those amazing 8-bit Commodore machines.</p>
