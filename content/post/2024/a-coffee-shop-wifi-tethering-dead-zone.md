---
title: "A coffee shop Wi-Fi tethering dead zone"
date: "2024-03-09T16:05:42+11:00"
year: "2024"
category: Internet
tag:
- networking
- wifi
location: Sydney
---
This is the sort of story that if a non-technical person told it to me, my instinct would be to assume they're doing something wrong. Because networks *don't work like this*. Or do they?

One of the coffee shops I love going to around here has this bizarre effect on my electronics. I'll go to tether from my phone to my laptop, and the connection will never establish. I'm not embellishing for comedic effect (for once), it *never* connects. It's like I'm drinking coffee in a Faraday cage.

*Faraday Café.* **HARRISON FORD.** *Get me out of this cage*. One day I'll do a post about *Air Force One*, and how so many quotes I say come from it even though I barely remember anything about it. But I digress.

It doesn't matter if it's my recent iPhone and my MacBook Air, or my older work phone with `wpa_supplicant` running on my Panasonic Let's Note with FreeBSD. It simply times out attempting to establish the Wi-Fi connection. Every. Time.

It's the same issue with Bluetooth. Even though Bluetooth sucks, I thought maybe it'd suck less than Wi-Fi that doesn't work at all. No dice. But that just might be Bluehooth sucking, so it might be a red herring. Blue herring?

The phone is able to access 4G while I sit here, so my solution has been to carry a USB cable with me, and connect the phone to the laptop directly. This makes it act as a USB modem, and it works flawlessly.

The area has dozens of Wi-Fi access points, two of which are public to the shopping centre that I can connect to without problems. I *think* this rules out any active jamming, unless they've tuned it in such a way to avoid them (can you even do that)?

Maybe there are too many networks on the frequency on which both my phones are trying to broadcast? But then, I've been in rooms with triple the available networks without this *consistent* issue.

I'm not a network engineer, and I'm definitely not an expert on wireless. But this has me scratching my head. Again, if someone else told me this was happening to them, I'd scarcely believe it. Anyone know what could be going on?
