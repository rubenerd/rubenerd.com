---
title: "A tech marketing test"
date: "2024-04-23T11:40:17+10:00"
year: "2024"
category: Thoughts
tag:
- chatbots
- marketing
location: Sydney
---
Let's do an experiment! Everyone likes experiments. It's what separates people who engage in experiments from people who don't. As they say, there are ten kinds of people in the world: those that understand binary, and those sick of this joke.

If you'll stop interrupting me, I'd like you to take a look at this word salad. This smorgasbord of syllables.

> $COMPANY is the world’s digital infrastructure company® [sic]. Digital leaders harness $COMPANY’s trusted platform to bring together and interconnect foundational infrastructure at software speed. $COMPANY enables organizations to access all the right places, partners and possibilities to scale with agility, speed the launch of digital services, deliver world-class experiences and multiply their value, while supporting their sustainability goals.

As an aside, I love that they localised their entire site for Australia, but they retained the US spelling in that blurb. Makes everything else seem a bit like a wasted effort.

Anyway, based on a reading of just that paragraph, what do you think they do? What service do you think they provide? What are you *buying?* Their landing page also says <span style="text-decoration:line-through">blockchain</span>, wait no, <span style="text-decoration:line-through">augmented reality</span>, wait no, AI too, if that helps.

Give up?

They're a data centre company. They run data centres. Though I think if they were a telco, a dev house, a managed service provider, an OEM, a systems integrator, or a hot dog stand, I'd have still believed it. I could slap it on the website of the company I work for, and it'd still work.

Marketing being outsourced to chatbots was perhaps inevitable, but I'm surprised a billion dollar company would let one loose on their landing page copy. Maybe I don't understand because I don't think at "software speed". I wonder how you quantify that?

*Sorry sir, you'll have to forgive Ruben, he only operates at seventeen softwares a second*.
